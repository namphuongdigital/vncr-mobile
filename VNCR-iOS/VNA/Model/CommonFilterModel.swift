//
//  CommonFilterModel.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 16/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import SwiftyJSON
import Foundation

class CommonFilterModel: NSObject {
    
    var Id: Int = 0
    var Code: String = ""
    var Name: String = ""
    var Description: String = ""
    var DisplayOrder: Int = 0
    
    var IsSelected: Bool = false
    
    override init() {
        super.init()
    }
    
    public init(_ id: Int, _ code: String, _ name: String, _ isSelected: Bool) {
        self.Id = id
        self.Code = code
        self.Name = name
        self.IsSelected = isSelected
    }
    
    init(json: JSON) {
        self.Id = json["ID"].int ?? 0
        self.Code = json["Code"].string ?? ""
        self.Name = json["Fullname"].string ?? ""
        self.Description = json["Description"].string ?? ""
        self.DisplayOrder = json["DisplayOrder"].int ?? 0
    }
}
