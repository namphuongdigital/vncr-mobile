//
//  DeviceModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/17/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class DeviceModel: NSObject {
    
    var id: Int = 0
    var textColor: String = ""
    var displayName: String = ""
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.id = json["ID"].int ?? 0
        self.textColor = json["TextColor"].string ?? ""
        self.displayName = json["DisplayName"].string ?? ""
    }
    

}
