//
//  DynamicModel.swift
//  VNA
//
//  Created by Dai Pham on 16/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation

@dynamicMemberLookup
public struct DynamicModel {
    let dictionary:[String:Any]
    public subscript(dynamicMember member: String) -> Any? {
        return dictionary[member]
    }
}

@dynamicMemberLookup
public struct DynamicModelString {
    let dictionary:[String:String]
    public subscript(dynamicMember member: String) -> String? {
        return dictionary[member]
    }
}
