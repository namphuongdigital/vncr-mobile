//
//  SupportModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 4/6/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

enum StatusSupportType: Int {
    case pending = 1
    case completed = 2
    case taken = 3
    case removed = 4
    case hide = 5
}

class SupportModel: NSObject {
    /*
     "SupportID": 1,
     "ParentID": 0,
     "Sender": "CONG",
     "Assignee": "",
     "Message": "Test support 1",
     "Date": "23:17 03/04",
     "Status": 1,
     "StatusText": "Pending",
     "AttachmentId": 0,
     "Attachments": null
    */
    var parentID: Int = 0
    var supportID: Int = 0
    var avatar: String = ""
    var sender: String = ""
    var assignee: String = ""
    var date: String = ""
    var status: StatusSupportType = .pending
    var statusText: String = ""
    var message: String = ""
    var attachmentId: Int = 0
    var attachments = Array<AttachmentModel>()
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.parentID = json["ParentID"].int ?? 0
        self.supportID = json["SupportID"].int ?? 0
        self.avatar = json["Avatar"].string ?? ""
        self.sender = json["Sender"].string ?? ""
        self.assignee = json["Assignee"].string ?? ""
        self.date = json["Date"].string ?? ""
        self.status = StatusSupportType(rawValue: json["Status"].int ?? 0)!
        self.statusText = json["StatusText"].string ?? ""
        self.message = json["Message"].string?.removingPercentEncoding ?? ""
        self.attachmentId = json["AttachmentId"].int ?? 0
     
        if let dic = json["Attachments"].dictionary {
            attachments.append(AttachmentModel(json: JSON(dic)))
        }
        
        
        
    }
    

}
