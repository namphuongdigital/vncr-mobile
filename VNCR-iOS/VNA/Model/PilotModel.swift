//
//  PilotModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

public enum PositionType: String {
    case main = "Main"
    case second = "Second"
}


class PilotModel: NSObject {
    
    /**
     "ID": 1,
     "Name": "Pilot 1", "FullName": "Pilot 1", "Position": "Main", "Version": 1
    */
    var id: Int = 0
    var name: String = ""
    var fullName: String = ""
    var position: PositionType = .second
    var version: Int = 1
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.id = json["ID"].int ?? 0
        self.name = json["Name"].string ?? ""
        self.fullName = json["FullName"].string ?? ""
        self.position = .main//PositionType.init(rawValue: json["Position"].string ?? "Second")!
        version = json["Version"].int ?? 1
    }

}
