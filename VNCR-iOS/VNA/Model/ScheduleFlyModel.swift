//
//  ScheduleFlyModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/29/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import Foundation
import SwiftyJSON


enum AircraftCategoryType: String {
    case A350 = "A350", A321_8 = "A321-8", A321_16 = "A321-16", B787 = "B787", none = ""
    
    // new
    case A32 = "32A", B32 = "32B", C32 = "32C", D32 = "32D", E32 = "32E", N32 = "32N"
    case A35 = "35A", B35 = "35B"
    case A78 = "78A", B78 = "78B", C78 = "78C"
    case AT7 = "AT7", ATR = "ATR72"
}

class ScheduleFlyModel: NSObject {
    
    
    /*
     "Icon1_Top" : "",
     "TotalDEPU" : 0,
     "Icon1_Bot" : "",
     "TotalSTCR" : 0,
     "FlightDate" : "2016-11-01 00:00:00",
     "TypeApl" : null,
     "FlightNo" : "VN1954",
     "TotalUM" : 0,
     "Status" : null,
     "Parking" : null,
     "Arrived" : "2016-11-01 01:00:00",
     "TotalBSCT" : 0,
     "Version" : 0,
     "Gate" : null,
     "TotalBLND" : 0,
     "TotalSM" : 0,
     "Icon3_Top" : "",
     "TotalEXST" : 0,
     "Acf" : null,
     "TotalWchr" : 0,
     "TotalINF" : 0,
     "UTC" : "",
     "SelectTripYN" : true,
     "TotalPaxY" : 0,
     "Trips" : {
     "Routing" : "BMV-DAD-DLI-DAD-DAD-BMV",
     "TripID" : 10397,
     "Details" : [
     "VN1910 01\/11 15:00 - 18:00",
     "VN1954 01\/11 00:00 - 01:00",
     "VN1911 01\/11 08:00 - 11:00"
     ]
     },
     "TotalPaxC" : 0,
     "PaxRemark" : null,
     "Icon2_State" : "#C0C0C0",
     "Icon1_State" : "#C0C0C0",
     "FlightID" : 22877,
     "Note" : null,
     "Carry" : null,
     "Routing" : "DLI-DAD",
     "TotalPax" : 0,
     "Icon3_State" : "#C0C0C0",
     "Icon2_Bot" : "",
     "Aircraft" : "321",
     "Capacity" : 0,
     "TotalDEAF" : 0,
     "RegisterNo" : "321",
     "TotalVIP" : 0,
     "Departed" : "2016-11-01 00:00:00",
     "AcfNo" : null,
     "Icon2_Top" : "",
     "TotalPaxCKI" : 0,
     "Icon3_Bot" : "",
     "TotalCIP" : 0
     
     */
    var flightID: Int = 0
    var timeStart: String = ""
    var c: Int = 0
    var VIP: Int = 0
    var inf: Int = 0
    var um: Int = 0
    var y: Int = 0
    var CIP: Int = 0
    var BSCT: Int = 0
    var WCHC: Int = 0
    var totalDEPU: Int = 0
    var specialMeal: Int = 0
    var totalPax: Int = 0
    var capacity: Int = 0
    var aircraft: String = ""
    var delay: String = ""
    var classify: String = ""
    var totalCI: String = ""
    var totalY: String = ""
    var selectTripYN: Bool = false
    var seatmap: AircraftCategoryType = .none
    
    var routing: String = ""
    var flightNo: String = ""
    var fColor: String = ""
    
    var icon1State: String = ""
    var icon2State: String = ""
    var icon3State: String = ""
    
    var icon1TextColor: String = ""
    var icon2TextColor: String = ""
    var icon3TextColor: String = ""
    
    var icon1Top: String = ""
    var icon2Top: String = ""
    var icon3Top: String = ""
    
    var icon1StateBot: String = ""
    var icon2StateBot: String = ""
    var icon3StateBot: String = ""
    
    var icon1TextColorBot: String = ""
    var icon2TextColorBot: String = ""
    var icon3TextColorBot: String = ""
    
    var icon1Bot: String = ""
    var icon2Bot: String = ""
    var icon3Bot: String = ""
    
    var trips: TripModel?
    var flightDate: Date = Date()
    var departed: Date = Date()
    var arrived: Date = Date()
    var departedDate: String = ""
    var departedTime: String = ""
    
    var badgeReportFly: String = ""
    var badgeReportMultiRate: String = ""
    var badgeReportPosition: String = ""
    
    var timeColor: String = ""
    var note: String = ""
    
    var fullName: String = ""
    
    var fullName1: String = ""
    var numberPlane1: String = ""
    var numberPlane2: String = ""
    var numberPlane3: String = ""
    var numberPlane4: String = ""
    
    
    var heightRow: Float = 80.0
    var isExpanded: Bool = false {
        didSet {
            if(self.isExpanded == true) {
                heightRow = 140.0
            } else {
                heightRow = 80.0
            }
        }
    }
    var isCheck: Bool = false
    
    var message: String = ""
    
    override init() {
        super.init()
        self.c = 12
        self.VIP = 5
        self.y = 177
        self.CIP = 2
        self.inf = 1
        self.um = 1
        self.BSCT = 1
        self.WCHC = 1
        self.specialMeal = 1
        self.aircraft = "777"
        self.delay = "Delay"
        
        self.badgeReportFly = "5"
        self.badgeReportPosition = "!"
        self.badgeReportMultiRate = "3"
        
        
    }
    
    public init(json: JSON) {
        self.flightID = json["FlightID"].int ?? 0
        self.c = json["TotalPaxC"].int ?? 0
        self.VIP = json["TotalVIP"].int ?? 0
        self.y = json["TotalPaxY"].int ?? 0
        self.CIP = json["TotalCIP"].int ?? 0
        self.inf = json["TotalINF"].int ?? 0
        self.um = json["TotalUM"].int ?? 0
        self.BSCT = json["TotalBSCT"].int ?? 0
        self.WCHC = json["TotalWchr"].int ?? 0
        self.totalPax = json["TotalPax"].int ?? 0
        self.capacity = json["Capacity"].int ?? 0
        self.classify = json["Classify"].string ?? ""
        self.totalCI = json["TotalCI"].string ?? ""
        self.totalY = json["TotalY"].string ?? ""
        
        self.routing = json["Routing"].string ?? ""
        self.flightNo = json["FlightNo"].string ?? ""
        self.fColor = json["Fcolor"].string ?? "#166880"
        self.flightDate = json["FlightDate"].dateTime ?? Date()
        self.arrived = json["Arrived"].dateTime ?? Date()
        self.departed = json["Departed"].dateTime ?? Date()
        self.departedDate = json["DepartedDate"].string ?? ""
        self.departedTime = json["DepartedTime"].string ?? ""
        self.selectTripYN = json["SelectTripYN"].bool ?? false
        
        self.aircraft = json["Aircraft"].string ?? ""
        
        self.icon1State = json["Icon1_State"].string ?? ""
        self.icon2State = json["Icon2_State"].string ?? ""
        self.icon3State = json["Icon3_State"].string ?? ""
        
        icon1TextColor = json["Icon1_TextColor"].string ?? ""
        icon2TextColor = json["Icon2_TextColor"].string ?? ""
        icon3TextColor = json["Icon3_TextColor"].string ?? ""
        
        self.icon1Top = json["Icon1_Top"].string ?? ""
        self.icon2Top = json["Icon2_Top"].string ?? ""
        self.icon3Top = json["Icon3_Top"].string ?? ""
        
        icon1StateBot = json["Icon1_State_Bot"].string ?? ""
        icon2StateBot = json["Icon2_State_Bot"].string ?? ""
        icon3StateBot = json["Icon3_State_Bot"].string ?? ""
        
        icon1TextColorBot = json["Icon1_TextColor_Bot"].string ?? ""
        icon2TextColorBot = json["Icon2_TextColor_Bot"].string ?? ""
        icon3TextColorBot = json["Icon3_TextColor_Bot"].string ?? ""
        
        self.icon1Bot = json["Icon1_Bot"].string ?? ""
        self.icon2Bot = json["Icon2_Bot"].string ?? ""
        self.icon3Bot = json["Icon3_Bot"].string ?? ""
        
        self.timeColor = json["TimeColor"].stringValue
        self.note = json["Note"].stringValue
        
        self.trips = TripModel(json: json["Trips"])
        self.seatmap = AircraftCategoryType(rawValue: json["Seatmap"].stringValue) ?? .none
        
        
    }
   

}
