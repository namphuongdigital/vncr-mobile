// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let surveyPageModel = try? newJSONDecoder().decode(SurveyPageModel.self, from: jsonData)

import Foundation

// MARK: - SurveyPageModel
public class SurveyPageModel: Codable {
    public var id: Int?
    public var idToken, cid: String?
    public var questions: [SurveyQuestion]?
    public var title, titleColor, surveyPageModelDescription, result: String?
    public var creatorid: Int?
    public var creator, created: String?
    public var modifierid: Int?
    public var modifier, modified: String?
    public var countdown:Int

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case idToken = "IdToken"
        case cid = "CID"
        case questions = "Questions"
        case title = "Title"
        case titleColor = "TitleColor"
        case surveyPageModelDescription = "Description"
        case result = "Result"
        case creatorid = "Creatorid"
        case creator = "Creator"
        case created = "Created"
        case modifierid = "Modifierid"
        case modifier = "Modifier"
        case modified = "Modified"
        case countdown = "Countdown"
    }

    public init(id: Int?, idToken: String?, cid: String?, questions: [SurveyQuestion]?, title: String?, titleColor: String?, surveyPageModelDescription: String?, result: String?, creatorid: Int?, creator: String?, created: String?, modifierid: Int?, modifier: String?, modified: String?, countdown:Int) {
        self.id = id
        self.idToken = idToken
        self.cid = cid
        self.questions = questions
        self.title = title
        self.titleColor = titleColor
        self.surveyPageModelDescription = surveyPageModelDescription
        self.result = result
        self.creatorid = creatorid
        self.creator = creator
        self.created = created
        self.modifierid = modifierid
        self.modifier = modifier
        self.modified = modified
        self.countdown = countdown
    }
    
    func toDict() -> [String:Any] {
        return [
            CodingKeys.id.stringValue : id ?? NSNull(),
            CodingKeys.idToken.stringValue : idToken ?? NSNull(),
            CodingKeys.cid.stringValue : cid ?? NSNull(),
            CodingKeys.questions.stringValue : questions?.compactMap({$0.toDict()}) ?? NSNull(),
            CodingKeys.title.stringValue : title ?? NSNull(),
            CodingKeys.titleColor.stringValue : titleColor ?? NSNull(),
            CodingKeys.surveyPageModelDescription.stringValue : surveyPageModelDescription ?? NSNull(),
            CodingKeys.result.stringValue : result ?? NSNull(),
            CodingKeys.creatorid.stringValue : creatorid ?? NSNull(),
            CodingKeys.creator.stringValue : creator ?? NSNull(),
            CodingKeys.created.stringValue : created ?? NSNull(),
            CodingKeys.modifierid.stringValue : modifierid ?? NSNull(),
            CodingKeys.modifier.stringValue : modifier ?? NSNull(),
            CodingKeys.modified.stringValue : modified ?? NSNull(),
            CodingKeys.countdown.stringValue : countdown
        ]
    }
}

// MARK: - Question
public class SurveyQuestion: Codable, Equatable {
    
    func copy() -> SurveyQuestion {
        do {
            let encoded = try JSONEncoder().encode(self)
            let decoded = try JSONDecoder().decode(SurveyQuestion.self, from: encoded)
            return decoded
        } catch {
            fatalError(error.localizedDescription)
        }
    }
    
    public static func == (lhs: SurveyQuestion, rhs: SurveyQuestion) -> Bool {
        
        let extra = lhs.ansExtra == rhs.ansExtra
        
        var ansNumber = true
        lhs.ansNumbers?.enumerated().forEach({(i,a) in
            if rhs.ansNumbers?[i] != a {
                ansNumber = false
            }
        })
        
        var ansText = true
        lhs.ansTexts?.enumerated().forEach({(i,a) in
            if rhs.ansTexts?[i] != a {
                ansText = false
            }
        })
        
        var ansRadio = true
        lhs.ansRadios?.enumerated().forEach({(i,a) in
            if rhs.ansRadios?[i] != a {
                ansRadio = false
            }
        })
        
        var ansCheckbox = true
        lhs.ansCheckboxes?.enumerated().forEach({(i,a) in
            if rhs.ansCheckboxes?[i] != a {
                ansCheckbox = false
            }
        })
        
        var ansDateTime = true
        lhs.ansDateTimes?.enumerated().forEach({(i,a) in
            if rhs.ansDateTimes?[i] != a {
                ansDateTime = false
            }
        })
        
        return extra &&
        (ansNumber &&
        ansText &&
        ansRadio &&
        ansCheckbox &&
        ansDateTime)
    }
    
    public var id, sid, type: Int?
    public var title, content: String?
    public var ansNumbers: [AnsNumber]?
    public var ansRadios, ansCheckboxes: [AnsBoolean]?
    public var ansTexts: [AnsText]?
    public var ansExtra: AnsText?
    public var ansDateTimes: [AnsDateTime]?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case sid = "SID"
        case type = "Type"
        case title = "Title"
        case content = "Content"
        case ansTexts = "AnsTexts"
        case ansNumbers = "AnsNumbers"
        case ansRadios = "AnsRadios"
        case ansCheckboxes = "AnsCheckboxes"
        case ansDateTimes = "AnsDateTimes"
        case ansExtra = "AnsExtra"
    }

    public init(id: Int?, sid: Int?, type: Int?, title: String?, content: String?, ansTexts: [AnsText]?, ansNumbers: [AnsNumber]?, ansRadios: [AnsBoolean]?, ansCheckboxes: [AnsBoolean]?, ansDateTimes: [AnsDateTime]?, ansExtra: AnsText?) {
        self.id = id
        self.sid = sid
        self.type = type
        self.title = title
        self.content = content
        self.ansTexts = ansTexts
        self.ansNumbers = ansNumbers
        self.ansRadios = ansRadios
        self.ansCheckboxes = ansCheckboxes
        self.ansDateTimes = ansDateTimes
        self.ansExtra = ansExtra
    }
    
    func toDict() -> [String:Any] {
        return [
            CodingKeys.id.stringValue : id ?? NSNull(),
            CodingKeys.sid.stringValue : sid ?? NSNull(),
            CodingKeys.type.stringValue : type ?? NSNull(),
            CodingKeys.title.stringValue : title ?? NSNull(),
            CodingKeys.content.stringValue : content ?? NSNull(),
            CodingKeys.ansTexts.stringValue : ansTexts?.compactMap({$0.toDict()}) ?? NSNull(),
            CodingKeys.ansNumbers.stringValue : ansNumbers?.compactMap({$0.toDict()}) ?? NSNull(),
            CodingKeys.ansRadios.stringValue : ansRadios?.compactMap({$0.toDict()}) ?? NSNull(),
            CodingKeys.ansCheckboxes.stringValue : ansCheckboxes?.compactMap({$0.toDict()}) ?? NSNull(),
            CodingKeys.ansDateTimes.stringValue : ansDateTimes?.compactMap({$0.toDict()}) ?? NSNull(),
            CodingKeys.ansExtra.stringValue : ansExtra?.toDict() ?? NSNull()
        ]
    }
    
    static var empty:SurveyQuestion {SurveyQuestion(id: nil, sid: nil, type: nil, title: nil, content: nil, ansTexts: nil, ansNumbers: nil, ansRadios: nil, ansCheckboxes: nil, ansDateTimes: nil, ansExtra: nil)}
    
    var haveAns:Bool {
        return ansTexts?.count ?? 0 > 0 ||
        ansNumbers?.count ?? 0 > 0 ||
        ansRadios?.count ?? 0 > 0 ||
        ansCheckboxes?.count ?? 0 > 0 ||
        ansDateTimes?.count ?? 0 > 0 ||
        ansExtra != nil
    }
    
    var totalQuestions:Int {
        var total = 0
        total += (ansNumbers?.count ?? 0)
        total += (ansTexts?.count ?? 0)
        total += (ansRadios != nil ? 1 : 0)
        total += (ansCheckboxes != nil ? 1 : 0)
        total += (ansDateTimes?.count ?? 0)
        return total
    }
    
    var totalAnswer:Int {
        var total = 0
        total += (ansNumbers?.filter({$0.val ?? -1 != -1}).count ?? 0)
        total += (ansTexts?.filter({$0.val?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 != 0}).count ?? 0)
        total += (ansRadios?.first(where: {$0.val ?? false == true}) != nil ? 1 : 0)
        total += (ansCheckboxes?.first(where: {$0.val ?? false == true}) != nil ? 1 : 0)
        total += (ansDateTimes?.filter({ a in
            var haveTime = true
            var haveDate = true
            if a.dateVisible {
                if let v = a.valDate {
                    let check = Date(timeIntervalSinceReferenceDate: TimeInterval(v)).dateTimeToddMMyyyy
                    haveDate = check?.count ?? 0 > 0
                } else {
                    haveDate = false
                }
            }
            // kiem tra neu hien time va value time <= 0 then value time chua co
            if a.timeVisible {
                if let v = a.valTime {
                    let check = Date(timeIntervalSinceReferenceDate: TimeInterval(v)).dateTimeToTimeHHmm
                    haveTime = check?.count ?? 0 > 0
                } else {
                    haveTime = false
                }
            }
            return haveTime && haveDate
        }).count ?? 0)
        return total
    }
    
    var hasAnswer:Bool {

        var ansNumber = ansNumbers == nil ? true : true
        ansNumbers?.forEach({a in
            if a.val ?? -1 == -1 {
                ansNumber = false
            }
        })
        
        var ansText = ansTexts == nil ? true : true
        ansTexts?.forEach({a in
            if a.val?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 == 0 {
                ansText = false
            }
        })
        
        var ansRadio = ansRadios == nil ? true : false
        ansRadios?.forEach({a in
            if a.val ?? false == true {
                ansRadio = true
            }
        })
        
        var ansCheckbox = ansCheckboxes == nil ? true : false
        ansCheckboxes?.forEach({a in
            if a.val ?? false == true {
                ansCheckbox = true
            }
        })
        
        let ansDateTime = ansDateTimes == nil ? true : false
        var haveTime = true
        var haveDate = true
        ansDateTimes?.forEach({a in
            if a.dateVisible {
                if let v = a.valDate {
                    let check = Date(timeIntervalSinceReferenceDate: TimeInterval(v)).dateTimeToddMMyyyy
                    haveDate = check?.count ?? 0 > 0
                } else {
                    haveDate = false
                }
            }
            // kiem tra neu hien time va value time <= 0 then value time chua co
            if a.timeVisible {
                if let v = a.valTime {
                    let check = Date(timeIntervalSinceReferenceDate: TimeInterval(v)).dateTimeToTimeHHmm
                    haveTime = check?.count ?? 0 > 0
                } else {
                    haveTime = false
                }
            }
        })
        
        let extra = ansExtra != nil ? ansExtra?.val?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0 : true
        if ansExtra != nil {
            return extra ||
            (ansText && ansRadio && ansCheckbox && ansNumber && ansDateTime && haveTime && haveDate)
        } else {
            return (ansText && ansRadio && ansCheckbox && ansNumber && ansDateTime && haveTime && haveDate)
        }
    }
}

// MARK: - AnsText
public class AnsText: Codable, Equatable {
    public static func == (lhs: AnsText, rhs: AnsText) -> Bool {
        let valhs = lhs.val ?? ""
        let varhs = rhs.val ?? ""
        return valhs == varhs
    }
    
    var idString:String? {
        return id == nil ? nil : "\(id!)"
    }
    
    public var id: Int?
    public var caption: String?
    public var captionColor: String?
    public var val: String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case caption = "Caption"
        case captionColor = "CaptionColor"
        case val = "Val"
    }

    public init(id: Int?, caption: String?, captionColor: String?, val: String?) {
        self.id = id
        self.caption = caption
        self.captionColor = captionColor
        self.val = val
    }
    
    func toDict() -> [String:Any] {
        return [
            CodingKeys.id.stringValue : id ?? NSNull(),
            CodingKeys.caption.stringValue : caption ?? NSNull(),
            CodingKeys.captionColor.stringValue : captionColor ?? NSNull(),
            CodingKeys.val.stringValue : val ?? NSNull()
        ]
    }
}

// MARK: - AnsBoolean
public class AnsBoolean: Codable, Equatable {
    public static func == (lhs: AnsBoolean, rhs: AnsBoolean) -> Bool {
        let valhs = lhs.val ?? false
        let varhs = rhs.val ?? false
        return valhs == varhs
    }
    
    public var id: Int?
    public var caption: String?
    public var captionColor: String?
    public var val: Bool?

    var idString:String? {
        return id == nil ? nil : "\(id!)"
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case caption = "Caption"
        case captionColor = "CaptionColor"
        case val = "Val"
    }

    public init(id: Int?, caption: String?, captionColor: String?, val: Bool?) {
        self.id = id
        self.caption = caption
        self.captionColor = captionColor
        self.val = val
    }
    
    func toDict() -> [String:Any] {
        return [
            CodingKeys.id.stringValue : id ?? NSNull(),
            CodingKeys.caption.stringValue : caption ?? NSNull(),
            CodingKeys.captionColor.stringValue : captionColor ?? NSNull(),
            CodingKeys.val.stringValue : val ?? NSNull()
        ]
    }
}

// MARK: - AnsNumber
public class AnsNumber: Codable, Equatable {
    public static func == (lhs: AnsNumber, rhs: AnsNumber) -> Bool {
        let valhs = lhs.val ?? -1
        let varhs = rhs.val ?? -1
        return valhs == varhs
    }
    
    var idString:String? {
        return id == nil ? nil : "\(id!)"
    }
    
    public var id: Int?
    public var caption: String?
    public var captionColor: String?
    public var val: Double?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case caption = "Caption"
        case captionColor = "CaptionColor"
        case val = "Val"
    }

    public init(id: Int?, caption: String?, captionColor: String?, val: Double?) {
        self.id = id
        self.caption = caption
        self.captionColor = captionColor
        self.val = val
    }
    
    func toDict() -> [String:Any] {
        return [
            CodingKeys.id.stringValue : id ?? NSNull(),
            CodingKeys.caption.stringValue : caption ?? NSNull(),
            CodingKeys.captionColor.stringValue : captionColor ?? NSNull(),
            CodingKeys.val.stringValue : val ?? NSNull()
        ]
    }
}

// MARK: - AnsDateTime
public class AnsDateTime: Codable , Equatable {
    public static func == (lhs: AnsDateTime, rhs: AnsDateTime) -> Bool {
        let valTimehs = lhs.valTime ?? -1
        let varTimehs = rhs.valTime ?? -1
        let valDatehs = lhs.valDate ?? -1
        let varDatehs = rhs.valDate ?? -1
        return valDatehs == varDatehs && valTimehs == varTimehs
    }
    
    var idString:String? {
        return id == nil ? nil : "\(id!)"
    }
    
    public var id: Int?
    public var caption: String?
    public var captionColor: String?
    public var valDate, valTime: Int64?
    public var dateVisible, timeVisible:Bool

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case caption = "Caption"
        case captionColor = "CaptionColor"
        case valDate = "ValDate"
        case valTime = "ValTime"
        case dateVisible = "DateVisible"
        case timeVisible = "TimeVisible"
        
    }

    public init(id: Int?, caption: String?, captionColor: String?, valDate: Int64?, valTime:Int64?,dateVisible:Bool, timeVisible:Bool) {
        self.id = id
        self.caption = caption
        self.captionColor = captionColor
        self.valDate = valDate
        self.valTime = valTime
        self.dateVisible = dateVisible
        self.timeVisible = timeVisible
    }
    
    func toDict() -> [String:Any] {
        return [
            CodingKeys.id.stringValue : id ?? NSNull(),
            CodingKeys.caption.stringValue : caption ?? NSNull(),
            CodingKeys.captionColor.stringValue : captionColor ?? NSNull(),
            CodingKeys.valDate.stringValue : valDate ?? NSNull(),
            CodingKeys.valTime.stringValue : valTime ?? NSNull(),
            CodingKeys.dateVisible.stringValue : dateVisible,
            CodingKeys.timeVisible.stringValue : timeVisible
        ]
    }
}
