//
//  CommandModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 3/12/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class CommandModel: NSObject {
    
    var id: Int = 0
    var color: String = ""
    var display: String = ""
    var imageUrl: String = ""
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.id = json["ID"].int ?? 0
        self.color = json["Color"].string ?? ""
        self.display = json["Display"].string ?? ""
        self.imageUrl = json["ImageUrl"].string ?? ""
    }
    
}
