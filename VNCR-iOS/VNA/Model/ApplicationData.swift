//
//  ApplicationData.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/21/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import BoltsSwift
import RealmSwift

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        
        return array
    }
}

class ApplicationData: NSObject {
    
    private var realm: Realm?
    
    public static let sharedInstance: ApplicationData = {
        let instance = ApplicationData()
        instance.initRealm()
        return instance
    }()
    
    func initRealm() {
        let configuration = Realm.Configuration(
            schemaVersion: 17,
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 17) {
                    // The enumerateObjects(ofType:_:) method iterates
                    // over every Person object stored in the Realm file
                    migration.enumerateObjects(ofType: UserModel.className()) { oldObject, newObject in
                        // Nothing to do!
                        // Realm will automatically detect new properties and removed properties
                        // And will update the schema on disk automatically
                    }
                    
                    migration.enumerateObjects(ofType: MessageModel.className()) { oldObject, newObject in
                        // Nothing to do!
                        // Realm will automatically detect new properties and removed properties
                        // And will update the schema on disk automatically
                    }
                }
        })
        Realm.Configuration.defaultConfiguration = configuration
        self.realm = try! Realm()
    }
    
    
    func getBlockRealmWrite(block: () -> Void) {
        do {
            try realm?.write {
                block()
            }
        }
        catch {
            print("Error: \(error)")
        }
    }
    
    func getLoginUserModel() -> UserModel? {
        let results = self.realm?.objects(UserModel.self)
        let array = results?.toArray(ofType: UserModel.self)
        if let result = array?.first {
            UserDefaults.standard.set(result.userId, forKey: "userId")
            UserDefaults.standard.set(result.token, forKey: "token")
            UserDefaults.standard.set(result.crewID, forKey: "crewID")
            return result
        }
        return nil
        
    }
    
    func updateUserLogin(userModel: UserModel) {
        UserDefaults.standard.set(userModel.userId, forKey: "userId")
        UserDefaults.standard.set(userModel.token, forKey: "token")
        UserDefaults.standard.set(userModel.crewID, forKey: "crewID")
        do {
            try realm?.write {
                if let _ = realm?.object(ofType: UserModel.self, forPrimaryKey: userModel.userId as AnyObject) {
                    realm?.add(userModel, update: .all)
                } else {
                    realm?.add(userModel, update: .all)
                }
                
            }
        }
        catch {
            print("Error: \(error)")
        }
    }
    
    func deleteAllUserLogin() {
        UserDefaults.standard.set(nil, forKey: "userId")
        UserDefaults.standard.set(nil, forKey: "token")
        UserDefaults.standard.set(nil, forKey: "crewID")
        if let userLogin = self.getLoginUserModel() {
            do {
                try realm?.write {
                    realm?.delete(userLogin)
                }
            }
            catch {
                print("Error: \(error)")
            }
        }
    }
    
    //MARK - MessageModel
    
    func getParentMessageModels() -> [MessageModel]? {
        let results = self.realm?.objects(MessageModel.self).filter("isParents == %@", NSNumber(booleanLiteral: true))
        var array = results?.toArray(ofType: MessageModel.self)
        array?.sort(by: { $0.date.compare($1.date) == .orderedAscending})
        return array
    }
    
    func getMessageModels() -> [MessageModel]? {
        let results = self.realm?.objects(MessageModel.self)
        var array = results?.toArray(ofType: MessageModel.self)
        array?.sort(by: { $0.date.compare($1.date) == .orderedAscending})
        return array
    }
    
    func deleteAllMessageModel() {
        if let array = self.getMessageModels() {
            do {
                try realm?.write {
                    for model in array {
                        for item in model.childrens {
                            realm?.delete(item)
                        }
                    }
                    realm?.delete(array)
                }
            }
            catch {
                print("Error: \(error)")
            }
        }
    }
    
    func updateMessageModel(message: MessageModel) {
        do {
            try realm?.write {
                if let _ = realm?.object(ofType: MessageModel.self, forPrimaryKey: message.id as AnyObject) {
                    realm?.add(message, update: .all)
                } else {
                    realm?.add(message, update: .all)
                }
                
            }
        }
        catch {
            print("Error: \(error)")
        }
    }
    
    func deleteMessageModel(id: Int64) {
        do {
            try realm?.write {
                if let model = realm?.object(ofType: MessageModel.self, forPrimaryKey: id as AnyObject) {
                    for item in model.childrens {
                        realm?.delete(item)
                    }
                    realm?.delete(model)
                } else {
                    print("")
                }
                
            }
        }
        catch {
            print("Error: \(error)")
        }
    }
    
    //MARK - SurverModel
    
    func checkSurveyModel(id: Int) -> Bool {
        if let model = realm?.object(ofType: SurveyModel.self, forPrimaryKey: id as AnyObject) {
            return true
        } else {
            return false
        }
    }
    
    func getSurveyModel(id: Int) -> SurveyModel? {
        if let model = realm?.object(ofType: SurveyModel.self, forPrimaryKey: id as AnyObject) {
            return model
        } else {
            return nil
        }
    }
    
    func getSurveyModels() -> [SurveyModel]? {
        let results = self.realm?.objects(SurveyModel.self)
        var array = results?.toArray(ofType: SurveyModel.self)
        //array?.sort(by: { $0.date.compare($1.date) == .orderedAscending})
        return array
    }
    
    func deleteAllSurveyModel() {
        if let array = self.getSurveyModels() {
            do {
                try realm?.write {
                    for model in array {
                        for item in model.surveyItems {
                            realm?.delete(item)
                        }
                    }
                    realm?.delete(array)
                }
            }
            catch {
                print("Error: \(error)")
            }
        }
    }
    
    func updateSurveyModel(survey: SurveyModel) {
        do {
            try realm?.write {
                if let _ = realm?.object(ofType: SurveyModel.self, forPrimaryKey: survey.id as AnyObject) {
                    realm?.add(survey, update: .all)
                } else {
                    realm?.add(survey, update: .all)
                }
                
            }
        }
        catch {
            print("Error: \(error)")
        }
    }
    
    func deleteSurveyModel(id: Int) {
        do {
            try realm?.write {
                if let model = realm?.object(ofType: SurveyModel.self, forPrimaryKey: id as AnyObject) {
                    for item in model.surveyItems {
                        realm?.delete(item)
                    }
                    realm?.delete(model)
                } else {
                    print("")
                }
                
            }
        }
        catch {
            print("Error: \(error)")
        }
    }
    
    
    //MARK - OJTModel
    
    func checkOJTModel(id: Int) -> Bool {
        if let model = realm?.object(ofType: OJTModel.self, forPrimaryKey: id as AnyObject) {
            return true
        } else {
            return false
        }
    }
    
    func getOJTModel(id: Int) -> OJTModel? {
        if let model = realm?.object(ofType: OJTModel.self, forPrimaryKey: id as AnyObject) {
            return model
        } else {
            return nil
        }
    }
    
    func getOJTModels() -> [OJTModel]? {
        let results = self.realm?.objects(OJTModel.self)
        var array = results?.toArray(ofType: OJTModel.self)
        //array?.sort(by: { $0.date.compare($1.date) == .orderedAscending})
        return array
    }
    
    func deleteAllOJTModel() {
        if let array = self.getOJTModels() {
            do {
                try realm?.write {
                    for model in array {
                        for item in model.ojtItems {
                            realm?.delete(item)
                        }
                    }
                    realm?.delete(array)
                }
            }
            catch {
                print("Error: \(error)")
            }
        }
    }
    
    func updateOJTModel(ojt: OJTModel) {
        do {
            try realm?.write {
                if let _ = realm?.object(ofType: OJTModel.self, forPrimaryKey: ojt.id as AnyObject) {
                    realm?.add(ojt, update: .all)
                } else {
                    realm?.add(ojt, update: .all)
                }
                
            }
        }
        catch {
            print("Error: \(error)")
        }
    }
    
    func deleteOJTModel(id: Int) {
        do {
            try realm?.write {
                if let model = realm?.object(ofType: OJTModel.self, forPrimaryKey: id as AnyObject) {
                    for item in model.ojtItems {
                        realm?.delete(item)
                    }
                    realm?.delete(model)
                } else {
                    print("")
                }
                
            }
        }
        catch {
            print("Error: \(error)")
        }
    }

}
