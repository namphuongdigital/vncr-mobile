//
//  CommonPopoverModel.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 16/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

import Foundation

class CommonPopoverModel: NSObject {
    
    var Id: String = ""
    var Code: String = ""
    var Name: String = ""
    var Description: String = ""
    var DisplayOrder: Int = 0
    
    var IsSelected: Bool = false
    
    override init() {
        super.init()
    }
    
    public init(_ id: String, _ code: String, _ name: String, _ isSelected: Bool) {
        self.Id = id
        self.Code = code
        self.Name = name
        self.IsSelected = isSelected
    }
    
    init(json: [String: Any]) {
        self.Id = json["Id"] as? String ?? ""
        self.Code = json["Code"] as? String ?? ""
        self.Name = json["Name"] as? String ?? ""
        self.Description = json["Description"] as? String ?? ""
        self.DisplayOrder = json["DisplayOrder"] as? Int ?? 0
    }
}
