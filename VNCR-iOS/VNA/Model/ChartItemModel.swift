//
//  ChartItemModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 7/15/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON


class ChartItemModel: NSObject {
    
    var id: Int = 0
    var val: Double = 0.0
    var title: String = ""
    var color: String = ""
    var textColor: String = ""
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.id = json["ID"].int ?? 0
        self.val = json["Val"].doubleValue
        self.title = json["Title"].string ?? ""
        self.color = json["Color"].string ?? ""
        self.textColor = json["TextColor"].string ?? ""
    }

}
