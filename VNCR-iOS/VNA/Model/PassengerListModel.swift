//
//  PassengerListModel.swift
//  VNA
//
//  Created by Dai Pham on 22/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

//   let passengerListResponse = try? newJSONDecoder().decode(PassengerListResponse.self, from: jsonData)

//
// To parse values from Alamofire responses:
//
//   Alamofire.request(url).responsePassengerListResponseElement { response in
//     if let passengerListResponseElement = response.result.value {
//       ...
//     }
//   }

import Foundation
import Alamofire

// MARK: - PassengerListResponseElement
public class PassengerSeat: Codable {
    public let id: Int?
    public let flightID: Int?
    public let seat: String?
    public let fullName: String?
    public let avatarURL: String?
    public let title: String?
    public let desc: String?
    public let note: String?
    public let pnr: String?
    public let bookingClass: String?
    public let cabin: String?
    public let passengerType: String?
    public let bagCount: String?
    public let indicators: String?
    public let checkInNumber: String?
    public let checkInDate: String?
    public let boardStatus: String?
    public let lotusShopKey:String?
    public let lotusShopItems:[CommonItem]
    public let lotusReportItems:[CommonItem]
    public let lotusShopNotes:[CommonItem]
    public var lotusShopStatus:LotusShopStatus?
    
    public let TitleTextColor: String?
    public let DescriptionTextColor: String?
    public let IconTextColor: String?
    public let IconBackgroundColor:String?
    public let IconBorderColor:String?

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case flightID = "FlightId"
        case seat = "Seat"
        case fullName = "FullName"
        case avatarURL = "AvatarUrl"
        case title = "Title"
        case desc = "Desc"
        case note = "Note"
        case pnr = "PNR"
        case bookingClass = "BookingClass"
        case cabin = "Cabin"
        case passengerType = "PassengerType"
        case bagCount = "BagCount"
        case indicators = "Indicators"
        case checkInNumber = "CheckInNumber"
        case checkInDate = "CheckInDate"
        case boardStatus = "BoardStatus"
        case lotusShopKey = "LotusShopKey"
        case lotusShopItems = "LotusShopItems"
        case lotusReportItems = "LotusReportItems"
        case TitleTextColor,DescriptionTextColor,IconTextColor,IconBackgroundColor,IconBorderColor
        case lotusShopStatus = "LotusShopStatus"
        case lotusShopNotes = "LotusShopNotes"
    }

    public init(id: Int?, flightID: Int?, seat: String?, fullName: String?, avatarURL: String?, title: String?, desc: String?, note: String?, pnr: String?, bookingClass: String?, cabin: String?, passengerType: String?, bagCount: String?, indicators: String?, checkInNumber: String?, checkInDate: String?, boardStatus: String?,TitleTextColor: String?,DescriptionTextColor: String?,IconTextColor: String?,IconBackgroundColor:String?,IconBorderColor:String?,
                lotusShopKey:String?, lotusShopItems:[CommonItem],lotusReportItems:[CommonItem],lotusShopStatus:LotusShopStatus?,
                lotusShopNotes:[CommonItem]) {
        self.id = id
        self.flightID = flightID
        self.seat = seat
        self.fullName = fullName
        self.avatarURL = avatarURL
        self.title = title
        self.desc = desc
        self.note = note
        self.pnr = pnr
        self.bookingClass = bookingClass
        self.cabin = cabin
        self.passengerType = passengerType
        self.bagCount = bagCount
        self.indicators = indicators
        self.checkInNumber = checkInNumber
        self.checkInDate = checkInDate
        self.boardStatus = boardStatus
        self.TitleTextColor = TitleTextColor
        self.DescriptionTextColor = DescriptionTextColor
        self.IconTextColor = IconTextColor
        self.IconBackgroundColor = IconBackgroundColor
        self.IconBorderColor = IconBorderColor
        self.lotusShopKey = lotusShopKey
        self.lotusShopItems = lotusShopItems
        self.lotusReportItems = lotusReportItems
        self.lotusShopStatus = lotusShopStatus
        self.lotusShopNotes = lotusShopNotes
    }
    
    required public  init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? nil
        flightID = try values.decodeIfPresent(Int.self, forKey: .flightID) ?? nil
        seat = try values.decodeIfPresent(String.self, forKey: .seat) ?? nil
        fullName = try values.decodeIfPresent(String.self, forKey: .fullName) ?? nil
        avatarURL = try values.decodeIfPresent(String.self, forKey: .avatarURL) ?? nil
        title = try values.decodeIfPresent(String.self, forKey: .title) ?? nil
        desc = try values.decodeIfPresent(String.self, forKey: .desc) ?? nil
        note = try values.decodeIfPresent(String.self, forKey: .note) ?? nil
        pnr = try values.decodeIfPresent(String.self, forKey: .pnr) ?? nil
        bookingClass = try values.decodeIfPresent(String.self, forKey: .bookingClass) ?? nil
        cabin = try values.decodeIfPresent(String.self, forKey: .cabin) ?? nil
        passengerType = try values.decodeIfPresent(String.self, forKey: .passengerType) ?? nil
        bagCount = try values.decodeIfPresent(String.self, forKey: .bagCount) ?? nil
        indicators = try values.decodeIfPresent(String.self, forKey: .indicators) ?? nil
        checkInNumber = try values.decodeIfPresent(String.self, forKey: .checkInNumber) ?? nil
        checkInDate = try values.decodeIfPresent(String.self, forKey: .checkInDate) ?? nil
        boardStatus = try values.decodeIfPresent(String.self, forKey: .boardStatus) ?? nil
        TitleTextColor = try values.decodeIfPresent(String.self, forKey: .TitleTextColor) ?? nil
        DescriptionTextColor = try values.decodeIfPresent(String.self, forKey: .DescriptionTextColor) ?? nil
        IconTextColor = try values.decodeIfPresent(String.self, forKey: .IconTextColor) ?? nil
        IconBackgroundColor = try values.decodeIfPresent(String.self, forKey: .IconBackgroundColor) ?? nil
        IconBorderColor = try values.decodeIfPresent(String.self, forKey: .IconBorderColor) ?? nil
        lotusShopKey = try values.decodeIfPresent(String.self, forKey: .lotusShopKey) ?? nil
        lotusShopItems = try values.decodeIfPresent([CommonItem].self, forKey: .lotusShopItems) ?? []
        lotusReportItems = try values.decodeIfPresent([CommonItem].self, forKey: .lotusReportItems) ?? []
        lotusShopStatus = try values.decodeIfPresent(LotusShopStatus.self, forKey: .lotusShopStatus) ?? nil
        lotusShopNotes = try values.decodeIfPresent([CommonItem].self, forKey: .lotusShopNotes) ?? []
    }
    
    var titleColor:UIColor {
        var titleColor:UIColor = .black
        if let v = self.TitleTextColor {
            titleColor = UIColor(v)
        }
        return titleColor
    }
    
    var descColor:UIColor {
        var descriptionColor:UIColor = .gray
        
        if let v = DescriptionTextColor {
            descriptionColor = UIColor(v)
        }
        return descriptionColor
    }
    
    var iconColor:UIColor  {
        var seatColor:UIColor = .black
        
        if let v = IconTextColor {
            seatColor = UIColor(v)
        }
        return seatColor
    }
    
    var iconBorderColor:UIColor  {
        var seatColor:UIColor = .gray
        
        if let v = IconBorderColor {
            seatColor = UIColor(v)
        }
        return seatColor
    }
    
    var iconBackgroundColor:UIColor  {
        var seatColor:UIColor = .white
        
        if let v = IconBackgroundColor {
            seatColor = UIColor(v)
        }
        return seatColor
    }
    
    var haveLotusitems:Bool  {
        return lotusShopItems.count > 0
    }
}

public class LotusShopStatus: Codable {
    public var id, flightID: Int
    public var lotusShopKey: String?
    public var selectedItemID, remark: String?
    public var attachments:[AttachmentCommonModel]

    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case flightID = "FlightID"
        case lotusShopKey = "LotusShopKey"
        case selectedItemID = "ReportOptionItemId"
        case remark = "Remark"
        case attachments = "Attachments"
    }

    public init(id: Int, flightID: Int, lotusShopKey: String, selectedItemID: String?, remark: String?,attachments:[AttachmentCommonModel]) {
        self.id = id
        self.flightID = flightID
        self.lotusShopKey = lotusShopKey
        self.selectedItemID = selectedItemID
        self.remark = remark
        self.attachments = attachments
    }
    
    required public  init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        flightID = try values.decodeIfPresent(Int.self, forKey: .flightID) ?? 0
        lotusShopKey = try values.decodeIfPresent(String.self, forKey: .lotusShopKey) ?? ""
        selectedItemID = try values.decodeIfPresent(String.self, forKey: .selectedItemID) ?? nil
        remark = try values.decodeIfPresent(String.self, forKey: .remark) ?? nil
        attachments = try values.decodeIfPresent([AttachmentCommonModel].self, forKey: .attachments) ?? []
    }
}


public typealias PassengerListModel = [PassengerSeat]

// MARK: - Alamofire response handlers
public extension DataRequest {
    fileprivate func decodableResponseSerializer<T: Decodable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }

            guard let data = data else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }

            return Result { try newJSONDecoder().decode(T.self, from: data) }
        }
    }

    @discardableResult
    fileprivate func responseDecodable<T: Decodable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }

    @discardableResult
    func responsePassengerListResponse(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<PassengerListModel>) -> Void) -> Self {
        return responseDecodable(queue: queue, completionHandler: completionHandler)
    }
}

