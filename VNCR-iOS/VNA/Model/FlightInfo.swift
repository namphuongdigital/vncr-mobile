//
//  FlightInfo.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/7/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

public class FlightInfo: NSObject, Codable {

    /**
     "Routing": "SGN-HUI",
     "FlightNo": "VN1376",
     "Departed": "2016-11-01 21:00:00",
     "Arrived": "2016-11-01 22:00:00"
     */
    
    public var departedTime, arrived, departed, arrivedDate: String
    public var flightNo, arrivedTime, departedDate, routing: String

    enum CodingKeys: String, CodingKey {
        case departedTime = "DepartedTime"
        case arrived = "Arrived"
        case departed = "Departed"
        case arrivedDate = "ArrivedDate"
        case flightNo = "FlightNo"
        case arrivedTime = "ArrivedTime"
        case departedDate = "DepartedDate"
        case routing = "Routing"
    }

    public init(departedTime: String, arrived: String, departed: String, arrivedDate: String, flightNo: String, arrivedTime: String, departedDate: String, routing: String) {
        self.departedTime = departedTime
        self.arrived = arrived
        self.departed = departed
        self.arrivedDate = arrivedDate
        self.flightNo = flightNo
        self.arrivedTime = arrivedTime
        self.departedDate = departedDate
        self.routing = routing
    }
    
    public init(json: JSON) {
        self.routing = json["Routing"].string ?? ""
        self.flightNo = json["FlightNo"].string ?? ""
        self.departed = json["Departed"].string ?? ""
        self.departedDate = json["DepartedDate"].string ?? ""
        self.departedTime = json["DepartedTime"].string ?? ""
        self.arrived = json["Arrived"].string ?? ""
        self.arrivedDate = json["ArrivedDate"].string ?? ""
        self.arrivedTime = json["ArrivedTime"].string ?? ""
        
    }
    
    var getArriveDate:Date? {
        let showTime = Formatter.jsonDateTimeFormatter.date(from: arrivedDate)
        let finalTime = showTime
        return finalTime
    }
    
    var getDepartedDate:Date? {
        let showTime = Formatter.jsonDateTimeFormatter.date(from: departedDate)
        let finalTime = showTime
        return finalTime
    }
    
    required public  init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        routing = try values.decodeIfPresent(String.self, forKey: .routing) ?? ""
        flightNo = try values.decodeIfPresent(String.self, forKey: .flightNo) ?? ""
        departed = try values.decodeIfPresent(String.self, forKey: .departed) ?? ""
        departedDate = try values.decodeIfPresent(String.self, forKey: .departedDate) ?? ""
        departedTime = try values.decodeIfPresent(String.self, forKey: .departedTime) ?? ""
        arrived = try values.decodeIfPresent(String.self, forKey: .arrived) ?? ""
        arrivedDate = try values.decodeIfPresent(String.self, forKey: .arrivedDate) ?? ""
        arrivedTime = try values.decodeIfPresent(String.self, forKey: .arrivedTime) ?? ""
    }
}
