//
//  BaseObject.swift
//  MBN-ePaperSmart-iOS
//
//  Created by Van Trieu Phu Huy on 10/25/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import Foundation

public class BaseObject: NSObject, IObjectId {
    public var objectId: Int?

}
