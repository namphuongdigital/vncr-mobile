//
//  SeatModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 3/21/19.
//  Copyright © 2019 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON



class SeatModel: ZFSeatModel {
    
    /*
     {
     "Avatar" : "https:\/\/api.crew.vn:443\/images\/VIP0.PNG",
     "R" : 0,
     "PaxName" : "NHAN\/DO QUYNH VI 1-1",
     "SeatColor" : "#008000",
     "Title" : "TONG GIAM DOC SKY",
     "Desc" : "",
     "ID" : 0,
     "C" : 0
     }
    */
    var id: Int = 0
    var r: Int = 0
    var c: Int = 0
    var avatar: String = ""
    var title: String = ""
    var paxName: String = ""
    var seatColor: String = ""
    var desc: String = ""
    
    
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        super.init()
        self.id = json["ID"].intValue
        self.r = json["R"].intValue
        self.c = json["C"].intValue
        self.seatColor = json["SeatColor"].stringValue
        super.colorHex = json["SeatColor"].stringValue
        self.avatar = json["Avatar"].stringValue
        self.title = json["Title"].stringValue
        self.paxName = json["PaxName"].stringValue
        self.seatColor = json["SeatColor"].stringValue
        self.desc = json["Desc"].stringValue
    }
    

}
