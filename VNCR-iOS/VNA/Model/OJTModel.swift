//
//  OJTModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Realm


class OJTModel: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var isReadOnly: Bool = false
    @objc dynamic var flightID: Int = 0
    @objc dynamic var fltInfo: String = ""
    @objc dynamic var cid: String = ""
    @objc dynamic var fullName: String = ""
    @objc dynamic var date: String = ""
    @objc dynamic var seatNo: String = ""
    @objc dynamic var purserID: String = ""
    @objc dynamic var purserName: String = ""
    @objc dynamic var leaderID: String = ""
    @objc dynamic var leaderName: String = ""
    @objc dynamic var crOJTLessonID: Int = 0
    @objc dynamic var crOJTLesson: String = ""
    @objc dynamic var crOJTCategory: String = ""
    @objc dynamic var totalScore: Int = 0
    @objc dynamic var comment: String = ""
    @objc dynamic var suggestion: String = ""
    @objc dynamic var signed: String = ""
    @objc dynamic var isDeleted: Bool = false
    @objc dynamic var created: Bool = false
    @objc dynamic var modified: Bool = false
    @objc dynamic var signature: String = ""
    @objc dynamic var textColor: String = ""
    @objc dynamic var statusUrl: String = ""
    public var IsShowConfirm:Bool = true
    
    var ojtItems = RealmSwift.List<OJTItemModel>()
    
    /*
     "ReadOnly": false,
     "Signature": null,
     "TextColor": "#1414cc",
     "ID": 0,
     "FlightID": null,
     "FltInfo": null,
     "ExaminerID": null,
     "ExaminerName": null,
     "ExamineeID": "DEV2",
     "ExamineeName": "Nguyen Dev 2",
     "Date": null,
     "CR_OJT_Lesson_ID": 1,
     "CR_OJT_Lesson": "Chuyển loại - Bài 1",
     "CR_OJT_Category": "Chuyển loại / Khác biệt máy bay",
     "TotalScore": 0,
     "Comment": null,
     "Suggestion": null,
     "Signed": null,
     "IsDeleted": null,
     "Created": null,
     "Modified": null,
     "Creator": null,
     "Modifier": null,
     "Creatorid": null,
     "Modifierid": null,
     "CR_OJT_Item": []
    */
    
    override public class func primaryKey() -> String {
        return "id"
    }
    
    override public static func ignoredProperties() -> [String] {
        return []
    }
    
//    required public init(realm: RLMRealm, schema: RLMObjectSchema) {
//        super.init(realm: realm, schema: schema)
//    }
    
//    required public init() {
//        super.init()
//        //fatalError("init() has not been implemented")
//    }
    
    public override init() {
        super.init()
    }
    
    required public init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
    
    public init(json: JSON) {
        super.init()
        self.id = json["ID"].int ?? 0
        self.isReadOnly = json["ReadOnly"].bool ?? false
        self.flightID = json["FlightID"].int ?? 0
        self.fltInfo = json["FltInfo"].string ?? ""
        self.fullName = json["FullName"].string ?? ""
        self.textColor = json["TextColor"].string ?? ""
        self.cid = json["CID"].string ?? ""
        self.seatNo = json["SeatNo"].string ?? ""
        self.purserID = json["PurserID"].string ?? ""
        self.purserName = json["PurserName"].string ?? ""
        self.signature = json["Signature"].string ?? ""
        self.leaderID = json["LeaderID"].string ?? ""
        self.leaderName = json["LeaderName"].string ?? ""
        self.crOJTCategory = json["CR_OJT_Category"].string ?? ""
        self.crOJTLesson = json["CR_OJT_Lesson"].string ?? ""
        self.crOJTLessonID = json["CR_OJT_Lesson_ID"].int ?? 0
        self.totalScore = json["TotalScore"].int ?? 0
        self.comment = json["Comment"].string ?? ""
        self.suggestion = json["Suggestion"].string ?? ""
        self.signed = json["Signed"].string ?? ""
        self.isDeleted = json["IsDeleted"].bool ?? false
        self.created = json["Created"].bool ?? false
        self.modified = json["Modified"].bool ?? false
        self.statusUrl = json["StatusUrl"].string ?? ""
        self.IsShowConfirm = json["IsShowConfirm"].bool ?? true
        if let arrayJson = json["CR_OJT_Item"].array {
            for item in arrayJson {
                ojtItems.append(OJTItemModel(json: item))
            }
        }
        
        
    }
    
    func indexOfOJTItem(itemId: Int) -> Int {
        for index in 0..<self.ojtItems.count {
            if(itemId == self.ojtItems[index].id) {
                return index
            }
        }
        return -1
    }

}
