//
//  SectionModel.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 3/14/20.
//  Copyright © 2020 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class SectionModel: NSObject {
    
    var BarTitle: String = ""
    var BarItems: [ChartItemModel] = []
    var PieTitle: String = ""
    var PieItems: [ChartItemModel] = []
    var ListItems: [SectionListItemModel] = []
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.BarTitle = json["BarTitle"].string ?? ""
        if let arrayJson = json["Bar"].array {
            for item in arrayJson {
                let model = ChartItemModel(json: item)
                BarItems.append(model)
            }
        }
        
        self.PieTitle = json["PieTitle"].string ?? ""
        if let arrayJson = json["Pie"].array {
            for item in arrayJson {
                let model = ChartItemModel(json: item)
                PieItems.append(model)
            }
        }
        
        if let arrayJson = json["Sections"].array {
            for item in arrayJson {
                let model = SectionListItemModel(json: item)
                ListItems.append(model)
            }
        }
    }
    
}

class SectionListItemModel: NSObject {
    
    var Title: String = ""
    var Items: [ContentModel] = []
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.Title = json["Title"].string ?? ""
        if let arrayJson = json["Items"].array {
            for item in arrayJson {
                let model = ContentModel(json: item)
                Items.append(model)
            }
        }
    }
    
}

