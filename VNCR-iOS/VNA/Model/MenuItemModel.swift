//
//  MenuItemModel.swift
//  CAVA
//
//  Created by Van Trieu Phu Huy on 9/13/18.
//  Copyright © 2018 Van Trieu Phu Huy. All rights reserved.
//

import UIKit
import SwiftyJSON


class MenuItemModel: NSObject {
    
    /*
     public int ID { get; set; }
     public int Category { get; set; }
     public string ImageUrl { get; set; }
     public string TitleVN { get; set; }
     public string TitleEN { get; set; }
     public string Description { get; set; }
     public string Color { get; set; } //Mau cua title
     public int Badges { get; set; } //Chi so tin nhan
    */
    var id: Int = 0
    var category: Int = 0
    var titleVN: String = ""
    var titleEN: String = ""
    var color: String = ""
    var descriptionContent: String = ""
    var imageUrl: String = ""
    var badges: Int = 0
    var BadgeValue: String = ""
    
    var ActionCategory: String = ""
    var ActionObjectId: String = ""
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        
        self.id = json["ID"].intValue
        self.color = json["Color"].stringValue
        self.descriptionContent = json["Description"].stringValue
        self.imageUrl = json["ImageUrl"].stringValue
        self.titleVN = json["TitleVN"].stringValue
        self.titleEN = json["TitleEN"].stringValue
        self.badges = json["Badges"].intValue
        self.category = json["Category"].intValue
        self.BadgeValue = json["BadgeValue"].string ?? ""
        self.ActionCategory = json["ActionCategory"].string ?? ""
        self.ActionObjectId = json["ActionObjectId"].string ?? ""
    }


}
