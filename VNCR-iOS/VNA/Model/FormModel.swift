//
//  FormModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/12/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

public class FormModel: NSObject {
    /*
     "CategoryName": "Lịch bay",
     "Title": "15 Nov 2017 - 15 Nov 2017",
     "Description": "Xin đề nghị lịch đi Úc vì có người thân đi trên chuyến bay VN... ngày 15/11/2017 (Đính kèm vé máy bay)",
     "StatusUrl": "http://api.doantiepvien.com.vn:8090/profiles/avatar_default.png",
     "AttachmentUrls": null,
     "Categories": null,
     "ID": 3,
     "CID": "0672",
     "CName": null,
     "Category_ID": 3,
     "From_Date": null,
     "To_Date": null,
     "Route": null,
     "Content": null,
     "Attachments": null,
     "Date": null,
     "Status": null,
     "Replierid": null,
     "Replier": null,
     "Replied": null,
     "ReplyDept": null,
     "ReplyInfo": null,
     "IsDeleted": null,
     "Created": null,
     "Modified": null,
     "Creator": null,
     "Modifier": null,
     "Creatorid": null,
     "Modifierid": null
    */
    var id: Int = 0
    var categoryName: String = ""
    var title: String = ""
    var descriptionContent: String = ""
    var statusUrl: String = ""
    var categories = Array<FormCategoryModel>()
    var attachmentUrls = Array<String>()
    var attachments = Array<AttachmentModel>()
    var active: Date = Date()
    var cid: String = ""
    var cName: String = ""
    var categoryID: Int = 0
    var attachmentGroupID: Int = 0
    
    var isReadonly: Bool = false
    
    var fromDate: Date?
    var toDate: Date?
    
    var remarks: String = ""
    var isNeedApprove: Bool = false
    var workFlows: [FormWorkflowModel] = []
    var actionStatus: Common.FormStatus = Common.FormStatus.None
    var AvatarUrl: String = ""
    
    var approvalFromDate: Date? = nil
    var approvalToDate: Date? = nil
    var approvalRemark: String = ""

    override init() {
        super.init()
    }
    
    public init(json: JSON) {
        
        
        self.id = json["ID"].int ?? 0
        self.categoryName = json["CategoryName"].string ?? ""
        self.title = json["Title"].string ?? ""
        self.descriptionContent = json["Description"].string ?? ""
        self.statusUrl = json["StatusUrl"].string ?? ""
        self.cid = json["CID"].string ?? ""
        self.cName = json["CName"].string ?? ""
        self.categoryID = json["Category_ID"].int ?? 0
        //self.attachmentId = json["AttachmentId"].int ?? 0
        self.isReadonly = json["Readonly"].bool ?? false
        
        self.fromDate = json["From_Date"].stringValue.stringToDateYYYYMMdd
        self.toDate = json["To_Date"].stringValue.stringToDateYYYYMMdd
        
        self.attachmentGroupID = json["Attachments"].int ?? 0
        self.AvatarUrl = json["AvatarUrl"].string ?? ""

        if let arrayJson = json["AttachmentUrls"].array {
            for item in arrayJson {
                attachments.append(AttachmentModel(json: item))
            }
        }
        
        if let arrayJson = json["Categories"].array {
            for item in arrayJson {
                categories.append(FormCategoryModel(json: item))
            }
        }
        
        self.remarks = json["Remarks"].string ?? ""
        self.isNeedApprove = json["IsNeedApprove"].bool ?? false
        if let arrayJson = json["WorkFlows"].array {
            for item in arrayJson {
                workFlows.append(FormWorkflowModel(json: item))
            }
        }
        
        self.approvalFromDate = json["ApprovalFromDate"].stringValue.stringToDateYYYYMMdd
        self.approvalToDate = json["ApprovalToDate"].stringValue.stringToDateYYYYMMdd
        self.approvalRemark = json["ApprovalRemark"].string ?? ""

    }
    
}
