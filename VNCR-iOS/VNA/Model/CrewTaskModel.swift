//
//  CrewTaskModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/7/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON


public enum PermissionCrewTaskType: Int {
    case read = 0
    case write = 1
    case full = 2
}

public class CrewTaskModel:NSObject, Codable {
    
    public var flightInfo: [FlightInfo]
    public var trainerList, crewDutyList: [CrewDutyModelNew]
    public var permissionInt: Int

    enum CodingKeys: String, CodingKey {
        case flightInfo = "FlightInfo"
        case trainerList = "TrainerList"
        case crewDutyList = "CrewDutyList"
        case permissionInt = "Permission"
    }
    
    required public init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        flightInfo = try values.decodeIfPresent([FlightInfo].self, forKey: .flightInfo) ?? []
        trainerList = try values.decodeIfPresent([CrewDutyModelNew].self, forKey: .trainerList) ?? []
        crewDutyList = try values.decodeIfPresent([CrewDutyModelNew].self, forKey: .crewDutyList) ?? []
        permissionInt = try values.decodeIfPresent(Int.self, forKey: .permissionInt) ?? 0
    }
    
    public init(flightInfo: [FlightInfo], trainerList: [CrewDutyModelNew], crewDutyList: [CrewDutyModelNew], permission: Int) {
        self.flightInfo = flightInfo
        self.trainerList = trainerList
        self.crewDutyList = crewDutyList
        self.permissionInt = permission
    }
    
    var permission:PermissionCrewTaskType {
        PermissionCrewTaskType(rawValue: permissionInt)!
    }
    var listFlightInfo:Array<FlightInfo> {
        flightInfo
    }
    var listCrewDutyModel:Array<CrewDutyModel> {
        crewDutyList.compactMap({$0.old})
    }
    var listTrainer:Array<CrewDutyModel> {
        trainerList.compactMap({$0.old})
    }
}
