//
//  OJTItemModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Realm

class OJTItemModel: Object {
    
    /*
     "ID": 1,
     "CR_OJT_ID": 1,
     "Question": "1. Kiến thức ATB về loại máy bay thực tập, quy trình ATB thông thường và khẩn cấp",
     "Factor": 2,
     "Score": "",
     "ScoreValue": null,
     "Remark": null,
     "Comment": null,
     "IsDeleted": null,
     "Created": null,
     "Modified": null,
     "Creator": null,
     "Modifier": null,
     "Creatorid": null,
     "Modifierid": null,
     "Input": true,
     "TextColor": "#000000",
     "ScoreType": "1;2;3;4;5;6;7;8;9;10",
     "CR_OJT": null
    */

    @objc dynamic var id: Int = 0
    @objc dynamic var crOJTID: Int = 0
    @objc dynamic var question: String = ""
    @objc dynamic var factor: Int = 0
    @objc dynamic var scoreValue: Int = 0
    @objc dynamic var totalScore: Int = 0
    @objc dynamic var remark: String = ""
    @objc dynamic var comment: String = ""
    @objc dynamic var isDeleted: Bool = false
    @objc dynamic var created: Bool = false
    @objc dynamic var modified: Bool = false
    @objc dynamic var isInput: Bool = false
    @objc dynamic var scoreTypeString: String = ""
    @objc dynamic var score: String = ""
    @objc dynamic var textColor: String = ""
    var scoreType: [String] {
        get {
            return scoreTypeString.components(separatedBy: ";")
        }
    }
    
    override public class func primaryKey() -> String {
        return "id"
    }
    
    override public static func ignoredProperties() -> [String] {
        return []
    }
    
    override init() {
        super.init()
    }
//    required public init(realm: RLMRealm, schema: RLMObjectSchema) {
//        super.init(realm: realm, schema: schema)
//    }
    
//    required public init() {
//        super.init()
        //fatalError("init() has not been implemented")
//    }
    
    required public init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
    public init(json: JSON) {
        super.init()
        self.id = json["ID"].int ?? 0
        self.factor = json["Factor"].int ?? 0
        self.score = json["Score"].string ?? ""
        self.scoreValue = json["ScoreValue"].int ?? 0
        self.scoreTypeString = json["ScoreType"].string ?? ""
        self.textColor = json["TextColor"].string ?? "#000000"
        self.remark = json["Remark"].string ?? ""
        self.comment = json["Comment"].string ?? ""
        self.question = json["Question"].string ?? ""
        self.crOJTID = json["CR_OJT_ID"].int ?? 0
        self.comment = json["Comment"].string ?? ""
        self.isDeleted = json["IsDeleted"].bool ?? false
        self.created = json["Created"].bool ?? false
        self.modified = json["Modified"].bool ?? false
        self.isInput = json["Input"].bool ?? false
        
    }
    
}
