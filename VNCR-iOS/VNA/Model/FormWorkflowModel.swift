//
//  FormWorkflowModel.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 06/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

public class FormWorkflowModel: NSObject {

    var ID: Int64 = 0
    var Step: Int = 0
    var Title: String = ""
    var ApprovedBy: String = ""
    var Remark: String = ""
    var AvatarUrl: String = ""
    var Status: String = ""
    var IsCompleted: Bool = false
    var TextColor: String = ""
    var ApprovedByTextColor: String = ""
    var RemarkTextColor: String = ""

    override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.ID = json["ID"].int64 ?? 0
        self.Step = json["Step"].int ?? 0
        self.Title = json["Title"].string ?? ""
        self.ApprovedBy = json["ApprovedBy"].string ?? ""
        self.Remark = json["Remark"].string ?? ""
        self.AvatarUrl = json["AvatarUrl"].string ?? ""
        self.Status = json["Status"].string ?? ""
        self.IsCompleted = json["IsCompleted"].bool ?? false
        self.TextColor = json["TextColor"].string ?? ""
        self.ApprovedByTextColor = json["ApprovedByTextColor"].string ?? ""
        self.RemarkTextColor = json["RemarkTextColor"].string ?? ""

    }
    
}
