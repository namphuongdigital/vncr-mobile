//
//  ReportModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

public enum EmergencyType: Int {
    case slow = 0
    case medium = 1
    case high = 2
}

public class ReportModel: NSObject {
    /**
     "ID": 34,
     "FlightID": 65651,
     "CrewID": "3165",
     "AvatarURL": "http://api.doantiepvien.com.vn:8090/profiles/avatar_default.png",
     "FullName": "MEYER CHIEN REINHARD",
     "Created": "10/02/2017 00:02",
     "Categories": "Sub Category 1, Sub Category 6, Sub Category 7, Sub Category 8",
     "Content": "Content test report",
     "Pilot": "Pilot 1, Pilot 2",
     "Emergency": "Important",
     "CanUpdate": true,
     "CanDelete": true,
     "Attchments": []
    */
    var id: Int = 0
    var flightID: Int = 0
    var crewID: Int = 0
    var avatarURL: String = ""
    var fullName: String = ""
    var created: String = ""
    var mainPilot: String = ""
    var secondPilot: String = ""
    var pilot: String = ""
    var isCanUpdate: Bool = false
    var isCanDelete: Bool = false
    var emergency: EmergencyType = .slow
    var content: String = ""
    var isReadOnly: Bool = false
    var attachments = Array<AttachmentModel>()
    var attachmentGroupID: Int = 0
    var categories = Array<CategoryModel>()
    var comment: String = ""
    var isCollapsed: Bool = true
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.id = json["ID"].int ?? 0
        self.flightID = json["FlightID"].int ?? 0
        self.crewID = json["CrewID"].int ?? 0
        self.fullName = json["FullName"].string ?? ""
        self.emergency = EmergencyType.init(rawValue: json["Emergency"].intValue) ?? .slow
        self.content = json["Content"].string?.removingPercentEncoding ?? ""
        self.isReadOnly = json["IsReadOnly"].bool ?? false
        self.comment = json["Comment"].string ?? ""
        self.created = json["Created"].string ?? ""
        self.avatarURL = json["AvatarURL"].string ?? ""
        self.fullName = json["FullName"].string ?? ""
        //self.pilot = json["Pilot"].string ?? ""
        self.isCanUpdate = json["CanUpdate"].bool ?? false
        self.isCanDelete = json["CanDelete"].bool ?? false
        
        
        self.mainPilot = json["MainPilot"].string ?? ""
        self.secondPilot = json["SecondPilot"].string ?? ""
        if(mainPilot.count > 0) {
            self.pilot = String(format: "%@ : %@", "Pilot".localizedString(), mainPilot)
        }
        /*
        if(mainPilot.characters.count > 0) {
            if(secondPilot.characters.count == 0) {
                self.pilot = String(format: "1: %@", mainPilot)
                //String(format: "%@ : %@", "Pilot".localizedString(), reportModel.mainPilot)
            } else {
                self.pilot = String(format: "1: %@ - 2: %@", mainPilot, secondPilot)
            }
        } else {
            if(secondPilot.characters.count > 0) {
                self.pilot = String(format: "2: %@", secondPilot)
            }
        }
        */
        self.attachmentGroupID = json["AttachmentGroupID"].int ?? 0
        
        if let arrayJson = json["Catetories"].array {
            for item in arrayJson {
                categories.append(CategoryModel(json: item))
            }
            
        }
        
        if let arrayJson = json["Attachments"].array {
            for item in arrayJson {
                attachments.append(AttachmentModel(json: item))
            }
            
        }
        
        
        
    }

}
