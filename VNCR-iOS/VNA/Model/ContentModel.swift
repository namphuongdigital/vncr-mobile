//
//  ContentModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 7/15/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContentModel: NSObject {

    var id: Int = 0
    var title: String = ""
    var imageUrl: String = ""
    var descriptionContent: String = ""
    var color: String = ""
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.id = json["ID"].int ?? 0
        self.title = json["Title"].string ?? ""
        self.imageUrl = json["ImageUrl"].string ?? ""
        self.descriptionContent = json["Description"].string ?? ""
        self.color = json["Color"].string ?? ""
    }
    
}
