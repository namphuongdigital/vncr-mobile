//
//  DepartmentModel.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 11/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import SwiftyJSON

public class DepartmentModel: NSObject {
    
    var ID: Int = 0
    var Key: String = ""
    var Code: String = ""
    var DepartmentName: String = ""
    var Description: String = ""
    
    override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.ID = json["ID"].int ?? 0
        self.Key = json["Key"].string ?? ""
        self.Code = json["Code"].string ?? ""
        self.DepartmentName = json["DepartmentName"].string ?? ""
        self.Description = json["Description"].string ?? ""
    }
    
}
