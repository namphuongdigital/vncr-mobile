// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let scheduleFlightModel = try? newJSONDecoder().decode(ScheduleFlightModel.self, from: jsonData)

import Foundation

public typealias ScheduleFlightModels = [ScheduleFlightModel]

// MARK: - ScheduleFlightModel
public class ScheduleFlightModel: Codable {
    public var gate: String
    public var totalSTCR: Int
    public var fcolor, icon3TextColor: String
    public var totalBLND: Int
    public var briefings: [Briefing]
    public var icon2Bot, arrivalBay: String
    public var totalINF: Int
    public var icon2TextColorBot: String
    public var blink: Bool
    public var totalCI: String
    public var totalBSCT: Int
    public var registerNo, arrivalCodeTextColor, icon3TextColorBot, departedDate: String
    public var icon3State, arrived, departureTimeValue: String
    public var ckinC: Int
    public var routing, arrivalNameValue: String
    public var totalPax: Int
    public var trips: ScheduleTrips
    public var icon3StateBot: String
    public var version: Int
    public var aircraft: String
    public var totalPaxI: Int
    public var departsAirport, icon3Bot: String
    public var totalEXST, ckinY: Int
    public var totalY, notice, arrivalTerminal, flightDate: String
    public var icon1Top: String
    public var capacity: Int
    public var arrivalTimeTextColor, departureTimeTextColor, departureGate: String
    public var totalDEPU: Int
    public var icon1State, departureUTCValue: String
    public var flightID: Int
    public var arrivalBelt, icon2State, status: String
    public var ckinI, totalDEAF: Int
    public var departureTerminal, icon1Bot, departedTime, classify: String
    public var flightNo, arrivalTimeValue, icon2StateBot, icon1TextColor: String
    public var totalSM: Int
    public var arrivalUTCValue, departureBay, note, icon2TextColor: String
    public var timeColor, icon2Top: String
    public var totalPaxC: Int
    public var icon3Top: String
    public var totalUM: Int
    public var departureCodeTextColor: String
    public var tripItems: [String]
    public var departed, departureNameValue, icon1StateBot, realFlightNo: String
    public var selectTripYN: Bool
    public var departureNameTextColor, carry, utc: String
    public var totalWchr: Int
    public var seatmap, departs, departureCodeValue: String
    public var totalPaxY, totalVIP: Int
    public var parking, arrivalNameTextColor, icon1TextColorBot: String
    public var totalPaxCKI: Int
    public var acfNo: String
    public var totalCIP: Int
    public var paxRemark, realDate: String
    public var ssrItems: [CommonItem]
    public var arrivalCodeValue, typeAPL, acf: String
    public var cnl: Bool
    
    public var flightDistanceTime: String
    public var capacityC, capacityW, capacityY, bookedC, bookedW, bookedY, checkedInC, checkedInW, checkedInY: Int
    
    enum CodingKeys: String, CodingKey {
        
        case flightDistanceTime = "FlightDistanceTime"
        case capacityC = "CapacityC"
        case capacityW = "CapacityW"
        case capacityY = "CapacityY"
        case bookedC = "BookedC"
        case bookedW = "BookedW"
        case bookedY = "BookedY"
        case checkedInC = "CheckedInC"
        case checkedInW = "CheckedInW"
        case checkedInY = "CheckedInY"
        
        case gate = "Gate"
        case totalSTCR = "TotalSTCR"
        case fcolor = "Fcolor"
        case icon3TextColor = "Icon3_TextColor"
        case totalBLND = "TotalBLND"
        case briefings = "Briefings"
        case icon2Bot = "Icon2_Bot"
        case arrivalBay = "ArrivalBay"
        case totalINF = "TotalINF"
        case icon2TextColorBot = "Icon2_TextColor_Bot"
        case blink = "Blink"
        case totalCI = "TotalCI"
        case totalBSCT = "TotalBSCT"
        case registerNo = "RegisterNo"
        case arrivalCodeTextColor = "ArrivalCodeTextColor"
        case icon3TextColorBot = "Icon3_TextColor_Bot"
        case departedDate = "DepartedDate"
        case icon3State = "Icon3_State"
        case arrived = "Arrived"
        case departureTimeValue = "DepartureTimeValue"
        case ckinC = "CkinC"
        case routing = "Routing"
        case arrivalNameValue = "ArrivalNameValue"
        case totalPax = "TotalPax"
        case trips = "Trips"
        case icon3StateBot = "Icon3_State_Bot"
        case version = "Version"
        case aircraft = "Aircraft"
        case totalPaxI = "TotalPaxI"
        case departsAirport = "DepartsAirport"
        case icon3Bot = "Icon3_Bot"
        case totalEXST = "TotalEXST"
        case ckinY = "CkinY"
        case totalY = "TotalY"
        case notice = "Notice"
        case arrivalTerminal = "ArrivalTerminal"
        case flightDate = "FlightDate"
        case icon1Top = "Icon1_Top"
        case capacity = "Capacity"
        case arrivalTimeTextColor = "ArrivalTimeTextColor"
        case departureTimeTextColor = "DepartureTimeTextColor"
        case departureGate = "DepartureGate"
        case totalDEPU = "TotalDEPU"
        case icon1State = "Icon1_State"
        case departureUTCValue = "DepartureUTCValue"
        case flightID = "FlightID"
        case arrivalBelt = "ArrivalBelt"
        case icon2State = "Icon2_State"
        case status = "Status"
        case ckinI = "CkinI"
        case totalDEAF = "TotalDEAF"
        case departureTerminal = "DepartureTerminal"
        case icon1Bot = "Icon1_Bot"
        case departedTime = "DepartedTime"
        case classify = "Classify"
        case flightNo = "FlightNo"
        case arrivalTimeValue = "ArrivalTimeValue"
        case icon2StateBot = "Icon2_State_Bot"
        case icon1TextColor = "Icon1_TextColor"
        case totalSM = "TotalSM"
        case arrivalUTCValue = "ArrivalUTCValue"
        case departureBay = "DepartureBay"
        case note = "Note"
        case icon2TextColor = "Icon2_TextColor"
        case timeColor = "TimeColor"
        case icon2Top = "Icon2_Top"
        case totalPaxC = "TotalPaxC"
        case icon3Top = "Icon3_Top"
        case totalUM = "TotalUM"
        case departureCodeTextColor = "DepartureCodeTextColor"
        case tripItems = "TripItems"
        case departed = "Departed"
        case departureNameValue = "DepartureNameValue"
        case icon1StateBot = "Icon1_State_Bot"
        case realFlightNo = "RealFlightNo"
        case selectTripYN = "SelectTripYN"
        case departureNameTextColor = "DepartureNameTextColor"
        case carry = "Carry"
        case utc = "UTC"
        case totalWchr = "TotalWchr"
        case seatmap = "Seatmap"
        case departs = "Departs"
        case departureCodeValue = "DepartureCodeValue"
        case totalPaxY = "TotalPaxY"
        case totalVIP = "TotalVIP"
        case parking = "Parking"
        case arrivalNameTextColor = "ArrivalNameTextColor"
        case icon1TextColorBot = "Icon1_TextColor_Bot"
        case totalPaxCKI = "TotalPaxCKI"
        case acfNo = "AcfNo"
        case totalCIP = "TotalCIP"
        case paxRemark = "PaxRemark"
        case realDate = "RealDate"
        case ssrItems = "SSRItems"
        case arrivalCodeValue = "ArrivalCodeValue"
        case typeAPL = "TypeApl"
        case acf = "Acf"
        case cnl = "CNL"
    }
    
    public init(gate: String, totalSTCR: Int, fcolor: String, icon3TextColor: String, totalBLND: Int, briefings: [Briefing], icon2Bot: String, arrivalBay: String, totalINF: Int, icon2TextColorBot: String, blink: Bool, totalCI: String, totalBSCT: Int, registerNo: String, arrivalCodeTextColor: String, icon3TextColorBot: String, departedDate: String, icon3State: String, arrived: String, departureTimeValue: String, ckinC: Int, routing: String, arrivalNameValue: String, totalPax: Int, trips: ScheduleTrips, icon3StateBot: String, version: Int, aircraft: String, totalPaxI: Int, departsAirport: String, icon3Bot: String, totalEXST: Int, ckinY: Int, totalY: String, notice: String, arrivalTerminal: String, flightDate: String, icon1Top: String, capacity: Int, arrivalTimeTextColor: String, departureTimeTextColor: String, departureGate: String, totalDEPU: Int, icon1State: String, departureUTCValue: String, flightID: Int, arrivalBelt: String, icon2State: String, status: String, ckinI: Int, totalDEAF: Int, departureTerminal: String, icon1Bot: String, departedTime: String, classify: String, flightNo: String, arrivalTimeValue: String, icon2StateBot: String, icon1TextColor: String, totalSM: Int, arrivalUTCValue: String, departureBay: String, note: String, icon2TextColor: String, timeColor: String, icon2Top: String, totalPaxC: Int, icon3Top: String, totalUM: Int, departureCodeTextColor: String, tripItems: [String], departed: String, departureNameValue: String, icon1StateBot: String, realFlightNo: String, selectTripYN: Bool, departureNameTextColor: String, carry: String, utc: String, totalWchr: Int, seatmap: String, departs: String, departureCodeValue: String, totalPaxY: Int, totalVIP: Int, parking: String, arrivalNameTextColor: String, icon1TextColorBot: String, totalPaxCKI: Int, acfNo: String, totalCIP: Int, paxRemark: String, realDate: String, ssrItems: [CommonItem], arrivalCodeValue: String, typeAPL: String, acf: String, cnl: Bool,
                flightDistanceTime: String,
                capacityC: Int, capacityW: Int, capacityY: Int, bookedC: Int, bookedW: Int, bookedY: Int, checkedInC: Int, checkedInW: Int, checkedInY: Int
                ) {
        self.gate = gate
        self.totalSTCR = totalSTCR
        self.fcolor = fcolor
        self.icon3TextColor = icon3TextColor
        self.totalBLND = totalBLND
        self.briefings = briefings
        self.icon2Bot = icon2Bot
        self.arrivalBay = arrivalBay
        self.totalINF = totalINF
        self.icon2TextColorBot = icon2TextColorBot
        self.blink = blink
        self.totalCI = totalCI
        self.totalBSCT = totalBSCT
        self.registerNo = registerNo
        self.arrivalCodeTextColor = arrivalCodeTextColor
        self.icon3TextColorBot = icon3TextColorBot
        self.departedDate = departedDate
        self.icon3State = icon3State
        self.arrived = arrived
        self.departureTimeValue = departureTimeValue
        self.ckinC = ckinC
        self.routing = routing
        self.arrivalNameValue = arrivalNameValue
        self.totalPax = totalPax
        self.trips = trips
        self.icon3StateBot = icon3StateBot
        self.version = version
        self.aircraft = aircraft
        self.totalPaxI = totalPaxI
        self.departsAirport = departsAirport
        self.icon3Bot = icon3Bot
        self.totalEXST = totalEXST
        self.ckinY = ckinY
        self.totalY = totalY
        self.notice = notice
        self.arrivalTerminal = arrivalTerminal
        self.flightDate = flightDate
        self.icon1Top = icon1Top
        self.capacity = capacity
        self.arrivalTimeTextColor = arrivalTimeTextColor
        self.departureTimeTextColor = departureTimeTextColor
        self.departureGate = departureGate
        self.totalDEPU = totalDEPU
        self.icon1State = icon1State
        self.departureUTCValue = departureUTCValue
        self.flightID = flightID
        self.arrivalBelt = arrivalBelt
        self.icon2State = icon2State
        self.status = status
        self.ckinI = ckinI
        self.totalDEAF = totalDEAF
        self.departureTerminal = departureTerminal
        self.icon1Bot = icon1Bot
        self.departedTime = departedTime
        self.classify = classify
        self.flightNo = flightNo
        self.arrivalTimeValue = arrivalTimeValue
        self.icon2StateBot = icon2StateBot
        self.icon1TextColor = icon1TextColor
        self.totalSM = totalSM
        self.arrivalUTCValue = arrivalUTCValue
        self.departureBay = departureBay
        self.note = note
        self.icon2TextColor = icon2TextColor
        self.timeColor = timeColor
        self.icon2Top = icon2Top
        self.totalPaxC = totalPaxC
        self.icon3Top = icon3Top
        self.totalUM = totalUM
        self.departureCodeTextColor = departureCodeTextColor
        self.tripItems = tripItems
        self.departed = departed
        self.departureNameValue = departureNameValue
        self.icon1StateBot = icon1StateBot
        self.realFlightNo = realFlightNo
        self.selectTripYN = selectTripYN
        self.departureNameTextColor = departureNameTextColor
        self.carry = carry
        self.utc = utc
        self.totalWchr = totalWchr
        self.seatmap = seatmap
        self.departs = departs
        self.departureCodeValue = departureCodeValue
        self.totalPaxY = totalPaxY
        self.totalVIP = totalVIP
        self.parking = parking
        self.arrivalNameTextColor = arrivalNameTextColor
        self.icon1TextColorBot = icon1TextColorBot
        self.totalPaxCKI = totalPaxCKI
        self.acfNo = acfNo
        self.totalCIP = totalCIP
        self.paxRemark = paxRemark
        self.realDate = realDate
        self.ssrItems = ssrItems
        self.arrivalCodeValue = arrivalCodeValue
        self.typeAPL = typeAPL
        self.acf = acf
        self.cnl = cnl
        self.flightDistanceTime = flightDistanceTime
        self.capacityC = capacityC
        self.capacityW = capacityW
        self.capacityY = capacityY
        self.bookedC = bookedC
        self.bookedW = bookedW
        self.bookedY = bookedY
        self.checkedInY = checkedInY
        self.checkedInC = checkedInC
        self.checkedInW = checkedInW
    }
    
    var heightRow: Float = 80.0
    var isExpanded: Bool = false {
        didSet {
            if(self.isExpanded == true) {
                heightRow = 140.0
            } else {
                heightRow = 80.0
            }
        }
    }
    var isCheck: Bool = false
    
    var message: String = ""
    
    static var empty:ScheduleFlightModel {
        //        self.c = 12
        //        self.VIP = 5
        //        self.y = 177
        //        self.CIP = 2
        //        self.inf = 1
        //        self.um = 1
        //        self.BSCT = 1
        //        self.WCHC = 1
        //        self.specialMeal = 1
        //        self.aircraft = "777"
        //        self.delay = "Delay"
        //
        //        self.badgeReportFly = "5"
        //        self.badgeReportPosition = "!"
        //        self.badgeReportMultiRate = "3"
        ScheduleFlightModel(gate: "", totalSTCR: 0, fcolor: "", icon3TextColor: "", totalBLND: 0, briefings: [], icon2Bot: "", arrivalBay: "", totalINF: 0, icon2TextColorBot: "", blink: false, totalCI: "", totalBSCT: 0, registerNo: "", arrivalCodeTextColor: "", icon3TextColorBot: "", departedDate: "", icon3State: "", arrived: "", departureTimeValue: "", ckinC: 0, routing: "", arrivalNameValue: "", totalPax: 0, trips: ScheduleTrips(tripID: 0, routing: "", details: []), icon3StateBot: "", version: 0, aircraft:"", totalPaxI: 0, departsAirport: "", icon3Bot: "", totalEXST: 0, ckinY: 0, totalY: "", notice: "", arrivalTerminal: "", flightDate: "", icon1Top: "", capacity: 0, arrivalTimeTextColor: "", departureTimeTextColor: "", departureGate: "", totalDEPU: 0, icon1State: "", departureUTCValue: "", flightID: 0, arrivalBelt: "", icon2State: "", status: "", ckinI: 0, totalDEAF: 0, departureTerminal: "", icon1Bot: "", departedTime: "", classify: "", flightNo: "", arrivalTimeValue: "", icon2StateBot: "", icon1TextColor: "", totalSM: 0, arrivalUTCValue: "", departureBay: "", note: "", icon2TextColor: "", timeColor: "", icon2Top: "", totalPaxC: 0, icon3Top: "", totalUM: 0, departureCodeTextColor: "", tripItems: [], departed: "", departureNameValue: "", icon1StateBot: "", realFlightNo: "", selectTripYN: false, departureNameTextColor: "", carry: "", utc: "", totalWchr: 0, seatmap: "", departs: "", departureCodeValue: "", totalPaxY: 0, totalVIP: 0, parking: "", arrivalNameTextColor: "", icon1TextColorBot: "", totalPaxCKI: 0, acfNo: "", totalCIP: 0, paxRemark: "", realDate: "", ssrItems: [], arrivalCodeValue: "", typeAPL: "", acf: "", cnl: false,flightDistanceTime: "",capacityC: 0, capacityW: 0, capacityY: 0, bookedC: 0, bookedW: 0, bookedY: 0, checkedInC: 0, checkedInW: 0, checkedInY: 0)
    }
    
    required public init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.departed = try values.decodeIfPresent(String.self, forKey: .departed) ?? ""
        self.arrivalNameTextColor = try values.decodeIfPresent(String.self, forKey: .arrivalNameTextColor) ?? ""
        self.flightDate = try values.decodeIfPresent(String.self, forKey: .flightDate) ?? ""
        self.icon2Bot = try values.decodeIfPresent(String.self, forKey: .icon2Bot) ?? ""
        self.totalEXST = try values.decodeIfPresent(Int.self, forKey: .totalEXST) ?? 0
        self.departureNameTextColor = try values.decodeIfPresent(String.self, forKey: .departureNameTextColor) ?? ""
        self.totalDEPU = try values.decodeIfPresent(Int.self, forKey: .totalDEPU) ?? 0
        self.ckinC = try values.decodeIfPresent(Int.self, forKey: .ckinC) ?? 0
        self.arrivalNameValue = try values.decodeIfPresent(String.self, forKey: .arrivalNameValue) ?? ""
        self.icon2StateBot = try values.decodeIfPresent(String.self, forKey: .icon2StateBot) ?? ""
        self.arrivalCodeTextColor = try values.decodeIfPresent(String.self, forKey: .arrivalCodeTextColor) ?? ""
        self.acf = try values.decodeIfPresent(String.self, forKey: .acf) ?? ""
        self.ckinI = try values.decodeIfPresent(Int.self, forKey: .ckinI) ?? 0
        self.departedTime = try values.decodeIfPresent(String.self, forKey: .departedTime) ?? ""
        self.totalY = try values.decodeIfPresent(String.self, forKey: .totalY) ?? ""
        self.departureTimeTextColor = try values.decodeIfPresent(String.self, forKey: .departureTimeTextColor) ?? ""
        self.totalSM = try values.decodeIfPresent(Int.self, forKey: .totalSM) ?? 0
        self.cnl = try values.decodeIfPresent(Bool.self, forKey: .cnl) ?? false
        self.departedDate = try values.decodeIfPresent(String.self, forKey: .departedDate) ?? ""
        self.departs = try values.decodeIfPresent(String.self, forKey: .departs) ?? ""
        self.totalCIP = try values.decodeIfPresent(Int.self, forKey: .totalCIP) ?? 0
        self.icon1StateBot = try values.decodeIfPresent(String.self, forKey: .icon1StateBot) ?? ""
        self.departureCodeValue = try values.decodeIfPresent(String.self, forKey: .departureCodeValue) ?? ""
        self.totalPaxCKI = try values.decodeIfPresent(Int.self, forKey: .totalPaxCKI) ?? 0
        self.icon2State = try values.decodeIfPresent(String.self, forKey: .icon2State) ?? ""
        self.flightNo = try values.decodeIfPresent(String.self, forKey: .flightNo) ?? ""
        self.totalUM = try values.decodeIfPresent(Int.self, forKey: .totalUM) ?? 0
        self.departsAirport = try values.decodeIfPresent(String.self, forKey: .departsAirport) ?? ""
        self.arrivalTimeValue = try values.decodeIfPresent(String.self, forKey: .arrivalTimeValue) ?? ""
        self.parking = try values.decodeIfPresent(String.self, forKey: .parking) ?? ""
        self.totalVIP = try values.decodeIfPresent(Int.self, forKey: .totalVIP) ?? 0
        self.icon3Top = try values.decodeIfPresent(String.self, forKey: .icon3Top) ?? ""
        self.seatmap = try values.decodeIfPresent(String.self, forKey: .seatmap) ?? ""
        self.totalWchr = try values.decodeIfPresent(Int.self, forKey: .totalWchr) ?? 0
        self.icon1Bot = try values.decodeIfPresent(String.self, forKey: .icon1Bot) ?? ""
        self.totalPaxC = try values.decodeIfPresent(Int.self, forKey: .totalPaxC) ?? 0
        self.gate = try values.decodeIfPresent(String.self, forKey: .gate) ?? ""
        self.acfNo = try values.decodeIfPresent(String.self, forKey: .acfNo) ?? ""
        self.paxRemark = try values.decodeIfPresent(String.self, forKey: .paxRemark) ?? ""
        self.arrivalTimeTextColor = try values.decodeIfPresent(String.self, forKey: .arrivalTimeTextColor) ?? ""
        self.status = try values.decodeIfPresent(String.self, forKey: .status) ?? ""
        self.icon2TextColor = try values.decodeIfPresent(String.self, forKey: .icon2TextColor) ?? ""
        self.note = try values.decodeIfPresent(String.self, forKey: .note) ?? ""
        self.aircraft = try values.decodeIfPresent(String.self, forKey: .aircraft) ?? ""
        self.totalDEAF = try values.decodeIfPresent(Int.self, forKey: .totalDEAF) ?? 0
        self.fcolor = try values.decodeIfPresent(String.self, forKey: .fcolor) ?? ""
        self.icon1State = try values.decodeIfPresent(String.self, forKey: .icon1State) ?? ""
        self.totalPax = try values.decodeIfPresent(Int.self, forKey: .totalPax) ?? 0
        self.totalBSCT = try values.decodeIfPresent(Int.self, forKey: .totalBSCT) ?? 0
        self.realDate = try values.decodeIfPresent(String.self, forKey: .realDate) ?? ""
        self.arrived = try values.decodeIfPresent(String.self, forKey: .arrived) ?? ""
        self.departureTimeValue = try values.decodeIfPresent(String.self, forKey: .departureTimeValue) ?? ""
        self.routing = try values.decodeIfPresent(String.self, forKey: .routing) ?? ""
        self.registerNo = try values.decodeIfPresent(String.self, forKey: .registerNo) ?? ""
        self.totalPaxY = try values.decodeIfPresent(Int.self, forKey: .totalPaxY) ?? 0
        self.blink = try values.decodeIfPresent(Bool.self, forKey: .blink) ?? false
        self.icon2TextColorBot = try values.decodeIfPresent(String.self, forKey: .icon2TextColorBot) ?? ""
        self.departureCodeTextColor = try values.decodeIfPresent(String.self, forKey: .departureCodeTextColor) ?? ""
        self.typeAPL = try values.decodeIfPresent(String.self, forKey: .typeAPL) ?? ""
        self.icon3TextColorBot = try values.decodeIfPresent(String.self, forKey: .icon3TextColorBot) ?? ""
        self.totalBLND = try values.decodeIfPresent(Int.self, forKey: .totalBLND) ?? 0
        self.ckinY = try values.decodeIfPresent(Int.self, forKey: .ckinY) ?? 0
        self.totalCI = try values.decodeIfPresent(String.self, forKey: .totalCI) ?? ""
        self.utc = try values.decodeIfPresent(String.self, forKey: .utc) ?? ""
        self.icon3State = try values.decodeIfPresent(String.self, forKey: .icon3State) ?? ""
        self.notice = try values.decodeIfPresent(String.self, forKey: .notice) ?? ""
        self.icon3StateBot = try values.decodeIfPresent(String.self, forKey: .icon3StateBot) ?? ""
        self.icon3TextColor = try values.decodeIfPresent(String.self, forKey: .icon3TextColor) ?? ""
        self.trips = try values.decodeIfPresent(ScheduleTrips.self, forKey: .trips) ?? ScheduleTrips(tripID: 0, routing: "", details: [])
        self.version = try values.decodeIfPresent(Int.self, forKey: .version) ?? 0
        self.totalINF = try values.decodeIfPresent(Int.self, forKey: .totalINF) ?? 0
        self.icon3Bot = try values.decodeIfPresent(String.self, forKey: .icon3Bot) ?? ""
        self.icon2Top = try values.decodeIfPresent(String.self, forKey: .icon2Top) ?? ""
        self.classify = try values.decodeIfPresent(String.self, forKey: .classify) ?? ""
        self.arrivalCodeValue = try values.decodeIfPresent(String.self, forKey: .arrivalCodeValue) ?? ""
        self.carry = try values.decodeIfPresent(String.self, forKey: .carry) ?? ""
        self.departureNameValue = try values.decodeIfPresent(String.self, forKey: .departureNameValue) ?? ""
        self.capacity = try values.decodeIfPresent(Int.self, forKey: .capacity) ?? 0
        self.realFlightNo = try values.decodeIfPresent(String.self, forKey: .realFlightNo) ?? ""
        self.icon1TextColor = try values.decodeIfPresent(String.self, forKey: .icon1TextColor) ?? ""
        self.icon1TextColorBot = try values.decodeIfPresent(String.self, forKey: .icon1TextColorBot) ?? ""
        self.icon1Top = try values.decodeIfPresent(String.self, forKey: .icon1Top) ?? ""
        self.selectTripYN = try values.decodeIfPresent(Bool.self, forKey: .selectTripYN) ?? false
        self.totalSTCR = try values.decodeIfPresent(Int.self, forKey: .totalSTCR) ?? 0
        self.timeColor = try values.decodeIfPresent(String.self, forKey: .timeColor) ?? ""
        self.totalPaxI = try values.decodeIfPresent(Int.self, forKey: .totalPaxI) ?? 0
        self.flightID = try values.decodeIfPresent(Int.self, forKey: .flightID) ?? 0
        self.arrivalTerminal = try values.decodeIfPresent(String.self, forKey: .arrivalTerminal) ?? ""
        self.departureGate = try values.decodeIfPresent(String.self, forKey: .departureGate) ?? ""
        self.departureUTCValue = try values.decodeIfPresent(String.self, forKey: .departureUTCValue) ?? ""
        self.arrivalBelt = try values.decodeIfPresent(String.self, forKey: .arrivalBelt) ?? ""
        self.departureTerminal = try values.decodeIfPresent(String.self, forKey: .departureTerminal) ?? ""
        self.arrivalUTCValue = try values.decodeIfPresent(String.self, forKey: .arrivalUTCValue) ?? ""
        self.tripItems = try values.decodeIfPresent([String].self, forKey: .tripItems) ?? []
        self.ssrItems = try values.decodeIfPresent([CommonItem].self, forKey: .ssrItems) ?? []
        self.briefings = try values.decodeIfPresent([Briefing].self, forKey: .briefings) ?? []
        self.arrivalBay = try values.decodeIfPresent(String.self, forKey: .arrivalBay) ?? ""
        self.departureBay = try values.decodeIfPresent(String.self, forKey: .departureBay) ?? ""
        self.flightDistanceTime = try values.decodeIfPresent(String.self, forKey: .flightDistanceTime) ?? ""
        
        self.capacityC = try values.decodeIfPresent(Int.self, forKey: .capacityC) ?? 0
        self.capacityW = try values.decodeIfPresent(Int.self, forKey: .capacityW) ?? 0
        self.capacityY = try values.decodeIfPresent(Int.self, forKey: .capacityY) ?? 0
        self.bookedC = try values.decodeIfPresent(Int.self, forKey: .bookedC) ?? 0
        self.bookedW = try values.decodeIfPresent(Int.self, forKey: .bookedW) ?? 0
        self.bookedY = try values.decodeIfPresent(Int.self, forKey: .bookedY) ?? 0
        self.checkedInY = try values.decodeIfPresent(Int.self, forKey: .checkedInY) ?? 0
        self.checkedInC = try values.decodeIfPresent(Int.self, forKey: .checkedInC) ?? 0
        self.checkedInW = try values.decodeIfPresent(Int.self, forKey: .checkedInW) ?? 0
    }
}

// MARK: - Trips
public class ScheduleTrips: Codable {
    public var tripID: Int
    public var routing: String
    public var details: [String]
    
    enum CodingKeys: String, CodingKey {
        case tripID = "TripID"
        case routing = "Routing"
        case details = "Details"
    }
    
    public init(tripID: Int, routing: String, details: [String]) {
        self.tripID = tripID
        self.routing = routing
        self.details = details
    }
    
    required public init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.tripID = try values.decodeIfPresent(Int.self, forKey: .tripID) ?? 0
        self.routing = try values.decodeIfPresent(String.self, forKey: .routing) ?? ""
        self.details = try values.decodeIfPresent([String].self, forKey: .details) ?? []
    }
}

// MARK: - Briefing
public class Briefing: Codable {
    var avatarURL: String
    var sendTimeValue: String
    var id, flightID: Int
    var senderID, content: String
    var attachmentURL: String
    var originalFileName, displayFileName: String
    var fileSize, replyID: Int
    var sendTime, senderCode, senderName: String
    var isDeleted: Bool
    var updatedDate: String
    
    enum CodingKeys: String, CodingKey {
        case avatarURL = "AvatarUrl"
        case sendTimeValue = "SendTimeValue"
        case id = "Id"
        case flightID = "FlightID"
        case senderID = "SenderId"
        case content = "Content"
        case attachmentURL = "AttachmentUrl"
        case originalFileName = "OriginalFileName"
        case displayFileName = "DisplayFileName"
        case fileSize = "FileSize"
        case replyID = "ReplyId"
        case sendTime = "SendTime"
        case senderCode = "SenderCode"
        case senderName = "SenderName"
        case isDeleted = "IsDeleted"
        case updatedDate = "UpdatedDate"
    }
    
    init(avatarURL: String, sendTimeValue: String, id: Int, flightID: Int, senderID: String, content: String, attachmentURL: String, originalFileName: String, displayFileName: String, fileSize: Int, replyID: Int, sendTime: String, senderCode: String, senderName: String, isDeleted: Bool, updatedDate: String) {
        self.avatarURL = avatarURL
        self.sendTimeValue = sendTimeValue
        self.id = id
        self.flightID = flightID
        self.senderID = senderID
        self.content = content
        self.attachmentURL = attachmentURL
        self.originalFileName = originalFileName
        self.displayFileName = displayFileName
        self.fileSize = fileSize
        self.replyID = replyID
        self.sendTime = sendTime
        self.senderCode = senderCode
        self.senderName = senderName
        self.isDeleted = isDeleted
        self.updatedDate = updatedDate
    }
    
    required public init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.avatarURL = try values.decodeIfPresent(String.self, forKey: .avatarURL) ?? ""
        self.sendTimeValue = try values.decodeIfPresent(String.self, forKey: .sendTimeValue) ?? ""
        self.id = try values.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.flightID = try values.decodeIfPresent(Int.self, forKey: .flightID) ?? 0
        self.senderID = try values.decodeIfPresent(String.self, forKey: .senderID) ?? ""
        self.content = try values.decodeIfPresent(String.self, forKey: .content) ?? ""
        self.attachmentURL = try values.decodeIfPresent(String.self, forKey: .attachmentURL) ?? ""
        self.originalFileName = try values.decodeIfPresent(String.self, forKey: .originalFileName) ?? ""
        self.displayFileName = try values.decodeIfPresent(String.self, forKey: .displayFileName) ?? ""
        self.fileSize = try values.decodeIfPresent(Int.self, forKey: .fileSize) ?? 0
        self.replyID = try values.decodeIfPresent(Int.self, forKey: .replyID) ?? 0
        self.sendTime = try values.decodeIfPresent(String.self, forKey: .sendTime) ?? ""
        self.senderCode = try values.decodeIfPresent(String.self, forKey: .senderCode) ?? ""
        self.senderName = try values.decodeIfPresent(String.self, forKey: .senderName) ?? ""
        self.isDeleted = try values.decodeIfPresent(Bool.self, forKey: .isDeleted) ?? false
        self.updatedDate = try values.decodeIfPresent(String.self, forKey: .updatedDate) ?? ""
    }
}

// MARK: - SSRItem
public class CommonItem: Codable {
    var id: String
    var imageURL: String
    var title, Description: String
    var titleColor, descriptionColor: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case imageURL = "ImageUrl"
        case title = "Title"
        case Description = "Description"
        case titleColor = "TitleColor"
        case descriptionColor = "DescriptionColor"
    }
    
    init(id: String, imageURL: String, title: String, Description: String, titleColor: String?, descriptionColor: String?) {
        self.id = id
        self.imageURL = imageURL
        self.title = title
        self.Description = Description
        self.titleColor = titleColor
        self.descriptionColor = descriptionColor
    }
    
    required public init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try values.decodeIfPresent(String.self, forKey: .id) ?? ""
        self.imageURL = try values.decodeIfPresent(String.self, forKey: .imageURL) ?? ""
        self.title = try values.decodeIfPresent(String.self, forKey: .title) ?? ""
        self.Description = try values.decodeIfPresent(String.self, forKey: .Description) ?? ""
        self.titleColor = try values.decodeIfPresent(String.self, forKey: .titleColor) ?? nil
        self.descriptionColor = try values.decodeIfPresent(String.self, forKey: .descriptionColor) ?? nil
    }
    
    var getDescriptionColor:UIColor?  {
        if let v = descriptionColor {
            return UIColor(v)
        }
        return nil
    }
    
    var getTitleColor:UIColor?  {
        if let v = titleColor {
            return UIColor(v)
        }
        return nil
    }
}
