//
//  WeeklyReportModel.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 11/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import SwiftyJSON

public class WeeklyReportModel: NSObject {
    
    var ID: Int = 0
    var Title: String = ""
    var Content: String = ""
    var ImageUrl: String = ""
    var DateString: String = ""
    
    var TextColor: String = ""
    var DescriptionTextColor: String = ""
    var RemarkTextColor: String = ""
    var Attachments:[AttachmentCommonModel] = []
    
    override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.ID = json["Id"].int ?? 0
        self.Title = json["Title"].string ?? ""
        self.Content = json["Content"].string ?? ""
        self.ImageUrl = json["ImageUrl"].string ?? ""
        self.DateString = json["DateString"].string ?? ""
        self.TextColor = json["TextColor"].string ?? ""
        self.DescriptionTextColor = json["DescriptionTextColor"].string ?? ""
        self.RemarkTextColor = json["RemarkTextColor"].string ?? ""
        if let list = json["Attachments"].array {
            self.Attachments = list.compactMap({AttachmentCommonModel(json: $0)})
        }
    }
    
}

public class AttachmentCommonModel: NSObject,Codable {
    
    var FileID: Int = 0
    var OriginalFileName: String = ""
    var FoFileUrl: String = ""
    var FoFileSource: String = ""
    var FilePath: String = ""
    
    var IsDeleted: Bool = false
    var DisplayOrder: Int = 0
    var base64File:String?
    
    override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.FileID = json["FileID"].int ?? 0
        self.OriginalFileName = json["OriginalFileName"].string ?? ""
        self.FoFileUrl = json["FoFileUrl"].string ?? ""
        self.FoFileSource = json["FoFileSource"].string ?? ""
        self.FilePath = json["FilePath"].string ?? ""
        self.IsDeleted = json["IsDeleted"].bool ?? false
        self.DisplayOrder = json["DisplayOrder"].int ?? 0
    }
 
    public init(FileID: Int, OriginalFileName: String, FoFileSource: String, FoFileURL: String, DisplayOrder: Int, FilePath: String, IsDeleted: Bool) {
        self.FileID = FileID
        self.OriginalFileName = OriginalFileName
        self.FoFileSource = FoFileSource
        self.FoFileUrl = FoFileURL
        self.DisplayOrder = DisplayOrder
        self.FilePath = FilePath
        self.IsDeleted = IsDeleted
    }
    
    required public  init(from decoder:Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        FileID = try values.decodeIfPresent(Int.self, forKey: .FileID) ?? 0
        OriginalFileName = try values.decodeIfPresent(String.self, forKey: .OriginalFileName) ?? ""
        FoFileSource = try values.decodeIfPresent(String.self, forKey: .FoFileSource) ?? ""
        FoFileUrl = try values.decodeIfPresent(String.self, forKey: .FoFileUrl) ?? ""
        DisplayOrder = try values.decodeIfPresent(Int.self, forKey: .DisplayOrder) ?? 0
        FilePath = try values.decodeIfPresent(String.self, forKey: .FilePath) ?? ""
        IsDeleted = try values.decodeIfPresent(Bool.self, forKey: .IsDeleted) ?? false
    }
}
