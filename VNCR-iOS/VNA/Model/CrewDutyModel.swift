//
//  CrewDutyModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/7/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

// MARK: - CrewDutyModelNew
public class CrewDutyModelNew: Codable {
    public var id: Int
    public var foMaDV: String
    public var tripID: Int
    public var crewID: String
    public var foNight: Int
    public var crewFirstName: String
    public var isReadOnly: Bool
    public var foRpt: String
    public var isPurser: Bool
    public var foIntTime, autoJob, foLoai, foFromPlace: String
    public var foRptime, crewLastName, dutyFree, textColor: String
    public var foCham, foFlyTime, ca, foNote: String
    public var foCh: Int
    public var foOffSplit, foTimeOff, foEndTime, foStartTime: String
    public var trainingCrewName: String
    public var flightID: Int
    public var vip, training: String
    public var foTimeFor: Int
    public var ability, foCFG, foUser, foDutyTime: String
    public var autoCA, ann, job, foStatus: String
    public var foJob: String
    public var avatarURL: String
    public var isTrainee: Bool
    public var foTimeMin: Int
    public var foEndPlace, other, foEOD: String
    public var userID, version: Int

    enum CodingKeys: String, CodingKey {
        case id = "ID"
        case foMaDV = "FO_Ma_DV"
        case tripID = "TripID"
        case crewID = "CrewID"
        case foNight = "FO_Night"
        case crewFirstName = "CrewFirstName"
        case isReadOnly = "IsReadOnly"
        case foRpt = "FO_Rpt"
        case isPurser = "IsPurser"
        case foIntTime = "FO_Int_Time"
        case autoJob = "AutoJob"
        case foLoai = "FO_Loai"
        case foFromPlace = "FO_From_Place"
        case foRptime = "FO_Rptime"
        case crewLastName = "CrewLastName"
        case dutyFree = "DutyFree"
        case textColor = "TextColor"
        case foCham = "FO_Cham"
        case foFlyTime = "FO_Fly_Time"
        case ca = "CA"
        case foNote = "FO_Note"
        case foCh = "FO_Ch"
        case foOffSplit = "FO_Off_Split"
        case foTimeOff = "FO_Time_Off"
        case foEndTime = "FO_End_Time"
        case foStartTime = "FO_Start_Time"
        case trainingCrewName = "TrainingCrewName"
        case flightID = "FlightID"
        case vip = "VIP"
        case training = "Training"
        case foTimeFor = "FO_Time_For"
        case ability = "Ability"
        case foCFG = "FO_Cfg"
        case foUser = "FO_User"
        case foDutyTime = "FO_Duty_Time"
        case autoCA = "AutoCA"
        case ann = "ANN"
        case job = "Job"
        case foStatus = "FO_Status"
        case foJob = "FO_Job"
        case avatarURL = "AvatarURL"
        case isTrainee = "IsTrainee"
        case foTimeMin = "FO_Time_Min"
        case foEndPlace = "FO_End_Place"
        case other = "Other"
        case foEOD = "FO_Eod"
        case userID = "UserId"
        case version = "Version"
    }

    public init(id: Int, foMaDV: String, tripID: Int, crewID: String, foNight: Int, crewFirstName: String, isReadOnly: Bool, foRpt: String, isPurser: Bool, foIntTime: String, autoJob: String, foLoai: String, foFromPlace: String, foRptime: String, crewLastName: String, dutyFree: String, textColor: String, foCham: String, foFlyTime: String, ca: String, foNote: String, foCh: Int, foOffSplit: String, foTimeOff: String, foEndTime: String, foStartTime: String, trainingCrewName: String, flightID: Int, vip: String, training: String, foTimeFor: Int, ability: String, foCFG: String, foUser: String, foDutyTime: String, autoCA: String, ann: String, job: String, foStatus: String, foJob: String, avatarURL: String, isTrainee: Bool, foTimeMin: Int, foEndPlace: String, other: String, foEOD: String, userID: Int, version: Int) {
        self.id = id
        self.foMaDV = foMaDV
        self.tripID = tripID
        self.crewID = crewID
        self.foNight = foNight
        self.crewFirstName = crewFirstName
        self.isReadOnly = isReadOnly
        self.foRpt = foRpt
        self.isPurser = isPurser
        self.foIntTime = foIntTime
        self.autoJob = autoJob
        self.foLoai = foLoai
        self.foFromPlace = foFromPlace
        self.foRptime = foRptime
        self.crewLastName = crewLastName
        self.dutyFree = dutyFree
        self.textColor = textColor
        self.foCham = foCham
        self.foFlyTime = foFlyTime
        self.ca = ca
        self.foNote = foNote
        self.foCh = foCh
        self.foOffSplit = foOffSplit
        self.foTimeOff = foTimeOff
        self.foEndTime = foEndTime
        self.foStartTime = foStartTime
        self.trainingCrewName = trainingCrewName
        self.flightID = flightID
        self.vip = vip
        self.training = training
        self.foTimeFor = foTimeFor
        self.ability = ability
        self.foCFG = foCFG
        self.foUser = foUser
        self.foDutyTime = foDutyTime
        self.autoCA = autoCA
        self.ann = ann
        self.job = job
        self.foStatus = foStatus
        self.foJob = foJob
        self.avatarURL = avatarURL
        self.isTrainee = isTrainee
        self.foTimeMin = foTimeMin
        self.foEndPlace = foEndPlace
        self.other = other
        self.foEOD = foEOD
        self.userID = userID
        self.version = version
    }
    
    var isPilot:Bool{crewID.lowercased().contains("pi")}
    
    var trainingCrewDutyModel: CrewDutyModel? {
        didSet {
            self.training = trainingCrewDutyModel?.crewID ?? ""
            self.trainingCrewName = trainingCrewDutyModel?.crewFirstName ?? ""
        }
    }
    
    public var old:CrewDutyModel? {
        if let data = try? self.jsonData(),
           let result = try? JSON(data: data) {
            return CrewDutyModel(json: result)
        }
        return nil
    }
}


public class CrewDutyModel: NSObject {
    
    
    var id: Int = 0
    var tripID: Int = 0
    var flightID: Int = 0
    var crewID: String = ""
    var userId: Int = 0
    var crewFirstName: String = ""
    var crewLastName: String = ""
    var pos: String = ""
    var isReadOnly: Bool = false
    var isPurser: Bool = false
    var totalScore: Int = 0
    var textColor: String = ""
    var ability: String = ""
    var fromPlace: String = ""
    var endPlace: String = ""
    var startTime: Date = Date()
    var endTime: Date = Date()
    var timeFor: Int = 0
    var timeMin: Int = 0
    var foJob: String = ""
    var autoJob: String = ""
    var autoCA: String = ""
    var job: String = ""
    var ca: String = ""
    var ann: String = ""
    var training: String = ""
    var trainingCrewName: String = ""
    var trainingCrewDutyModel: CrewDutyModel? {
        didSet {
            self.training = trainingCrewDutyModel?.crewID ?? ""
            self.trainingCrewName = trainingCrewDutyModel?.crewFirstName ?? ""
        }
    }
    var vip: String = ""
    var dutyFree = ""
    var other: String = ""
    var version: Int = 0
    var avatarURL: String = ""
    
    var isTrainee: Bool = false
    
    var isPilot:Bool{crewID.lowercased().contains("pi")}
    
    var assessmentModel:AssessmentModel?
    
    /**

     "ID": 85433,
     "TripID": 8787,
     "FlightID": 23017,
     "CrewID": "1766",
     "CrewFirstName": null,
     "CrewLastName": null,
     "FO_Job": "Y",
     "Ability": ""
     "FO_From_Place": "SGN",
     "FO_End_Place": "HUI",
     "FO_Start_Time": "2016-11-01 00:00:00",
     "FO_End_Time": "2016-11-01 00:00:00",
     "FO_Time_For": 0,
     "FO_Time_Min": 0,
     "FO_Time_Off": null,
     "FO_Fly_Time": null,
     "FO_Int_Time": null,
     "FO_Duty_Time": null,
     "FO_Cfg": null,
     "FO_Cham": null,
     "FO_Night": 0,
     "FO_Off_Split": null,
     "FO_Rpt": null,
     "FO_Rptime": null,
     "FO_Eod": null,
     "FO_Ch": 0,
     "FO_Loai": null,
     "FO_Status": null,
     "FO_Note": null,
     "FO_Ma_DV": null,
     "FO_User": "0",
     "Job": "P",
     "CA": null,
     "ANN": null,
     "Training": null,
     "VIP": null,
     "Other": null,
     "Version": 0
     */
    override init() {
        super.init()
    }
    
    public init(crewDutyModel: CrewDutyModel) {
        super.init()
        self.id = crewDutyModel.id
        self.tripID = crewDutyModel.tripID
        self.flightID = crewDutyModel.flightID
        self.crewID = crewDutyModel.crewID
        self.crewFirstName = crewDutyModel.crewFirstName
        self.crewLastName = crewDutyModel.crewLastName
        self.ability = crewDutyModel.ability
        self.fromPlace = crewDutyModel.fromPlace
        self.endPlace = crewDutyModel.endPlace
        
        self.startTime = crewDutyModel.startTime
        self.endTime = crewDutyModel.endTime
        self.timeFor = crewDutyModel.timeFor
        self.timeMin = crewDutyModel.timeMin
        self.pos = crewDutyModel.pos
        
        self.foJob = crewDutyModel.foJob
        
        self.job = crewDutyModel.job
        self.ca = crewDutyModel.ca
        self.ann = crewDutyModel.ann
        self.training = crewDutyModel.training
        self.trainingCrewName = crewDutyModel.trainingCrewName
        self.vip = crewDutyModel.vip
        self.dutyFree = crewDutyModel.dutyFree
        self.other = crewDutyModel.other
        self.version = crewDutyModel.version
        
        self.avatarURL = crewDutyModel.avatarURL
    }
    
    
    
    public init(json: JSON) {
  
        self.id = json["ID"].int ?? 0
        self.tripID = json["TripID"].int ?? 0
        self.flightID = json["FlightID"].int ?? 0
        self.crewID = json["CrewID"].string ?? ""
        self.crewFirstName = json["CrewFirstName"].string ?? ""
        self.crewLastName = json["CrewLastName"].string ?? ""
        self.ability = json["Ability"].string ?? ""
        self.fromPlace = json["FO_From_Place"].string ?? ""
        self.endPlace = json["FO_End_Place"].string ?? ""
        self.isReadOnly = json["IsReadOnly"].bool ?? false
        self.isPurser =  json["IsPurser"].bool ?? false
        self.totalScore =  json["TotalScore"].int ?? 0
        self.textColor =  json["TextColor"].string ?? ""
        self.userId =  json["UserId"].int ?? 0

        self.startTime = json["FO_Start_Time"].dateTime ?? Date()
        self.endTime = json["FO_End_Time"].dateTime ?? Date()
        self.timeFor = json["FO_Time_For"].int ?? 0
        self.timeMin = json["FO_Time_Min"].int ?? 0
        self.pos = json["FO_Cfg"].string ?? ""
        
        self.foJob = json["FO_Job"].string ?? ""
        
        self.job = json["Job"].string ?? ""
        self.ca = json["CA"].string ?? ""
        self.ann = json["ANN"].string ?? ""
        self.training = json["Training"].string ?? ""
        self.trainingCrewName = json["TrainingCrewName"].string ?? ""
        self.vip = json["VIP"].string ?? ""
        self.dutyFree = json["DutyFree"].string ?? ""
        self.other = json["Other"].string ?? ""
        self.version = json["Version"].int ?? 0
        
        self.autoJob = json["AutoJob"].string ?? ""
        self.autoCA = json["AutoCA"].string ?? ""
        
        self.avatarURL = json["AvatarURL"].string ?? ""
        self.isTrainee = json["IsTrainee"].bool ?? false
    }
    
    func loadAssessment(group:DispatchGroup) {
        group.enter()
        ServiceData.sharedInstance.taskFlightAssessmentgetinfo(flightId: flightID, destinationCrewID: crewID, destinationJob: foJob)
            .continueOnSuccessWith(continuation: { task in
//            #if DEBUG
//            print("\(task) \(#function)")
//            #endif
            if let result = task as? JSON {
                self.assessmentModel = AssessmentModel(json: result)
                group.leave()
            }
        }).continueOnErrorWith(continuation: { error in
            group.leave()
        })
    }

}
