//
//  AssessmentItemModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON


public class AssessmentItemModel: NSObject {
    /**
     "ID": 8,
     "QuestionID": 52,
     "Question": "P Đánh giá S 4",
     "Factor": 1,
     "Score": 5
    */
    var id: Int64 = 0
    var questionID: Int = 0
    var question: String = ""
    var factor: Int = 0
    var score: Int = 0
    var textColor: String = ""
    var isInput: Bool = false
    var Categories: [AssessmentItemCategoryModel] = []
    var rate:String = ""

    override init() {
        super.init()
    }
    
    public init(json: JSON) {
        
        self.id = json["ID"].int64 ?? 0
        self.questionID = json["QuestionID"].int ?? 0
        self.question = json["Question"].string ?? ""
        self.factor =  json["Factor"].int ?? 0
        self.score =  json["Score"].int ?? 0
        self.textColor = json["TextColor"].string ?? "#000000"
        self.isInput = json["Input"].bool ?? true
        self.rate = json["Rate"].string ?? ""
        if let arrayJson = json["Categories"].array {
            self.Categories = arrayJson.compactMap({AssessmentItemCategoryModel(json: $0)})
        }
    }
    
}

public class AssessmentItemCategoryModel: NSObject {
    
    var id: Int64 = 0
    var CategoryID: String = ""
    var Description: String = ""
    var Name: String = ""
    
    override init() {
        super.init()
    }
    
    public init(json: JSON) {
        
        self.id = json["ID"].int64 ?? 0
        self.CategoryID = json["CategoryID"].string ?? ""
        self.Description = json["Description"].string ?? ""
        self.Name =  json["Name"].string ?? ""
    }
}
