//
//  NotificationProtocol.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/13/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

protocol UnreadMessageCountDelegate: NSObjectProtocol {
    
    func unreadMessageCountUpdate(unreadMessageCount: Int)
}

protocol PushNotifyDelegate: NSObjectProtocol {
    func notifyReceiver(item: NotifyModel)
    func notifyOpened(item: NotifyModel)
}

protocol ReportPositionDelegate: NSObjectProtocol {
    func updateReportPosition(item: ScheduleFlightModel)
    
}

protocol UpdateFormDelegate: NSObjectProtocol {
    func addForm(formModel: FormModel)
    func refreshForm(formModel: FormModel)
    
}

protocol UpdateFlightReportDelegate: NSObjectProtocol {
    func addFlightReporte(report: ReportModel)
    func refreshFlightReporte(report: ReportModel)
    
}

protocol CrewDutyModelDelete: NSObjectProtocol {
    func refreshCrewDuty(_ crewDutyModel: CrewDutyModel)
}

protocol LeadReportPositionDelegate: NSObjectProtocol {
    func refreshAuthorizeLeadReportPosition()
}
