//
//  SubCategoryModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class SubCategoryModel: NSObject {
    /**
     "SubCategoryID": 1, "CategoryID": 1, "CategoryName": "Category 1", "SubCategoryName": "Sub
     Category 1", "IsSelected": false
    */
    var subCategoryID: Int = 0
    var subCategoryName: String = ""
    var categoryID: Int = 0
    var categoryName: String = ""
    var isSelected: Bool = false
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.subCategoryID = json["SubCategoryID"].int ?? 0
        self.subCategoryName = json["SubCategoryName"].string ?? ""
        self.categoryID = json["CategoryID"].int ?? 0
        self.categoryName = json["CategoryName"].string ?? ""
        self.isSelected = json["IsSelected"].bool ?? false
    }
    
    public init(subCategoryModel: SubCategoryModel) {
        self.subCategoryID = subCategoryModel.subCategoryID
        self.subCategoryName = subCategoryModel.subCategoryName
        self.categoryID = subCategoryModel.categoryID
        self.categoryName = subCategoryModel.categoryName
        self.isSelected = subCategoryModel.isSelected
    }

}
