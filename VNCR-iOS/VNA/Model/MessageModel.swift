//
//  MessageModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/18/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import Realm

public enum MessageStatusType: Int {
    case unsend = 0, //chưa gửi
    delivered = 1, // đã gửi
    received = 2, //đã nhận
    unread = 4, // chưa đọc
    read = 8, //đã đọc
    replied = 16, //đã trả lời
    repliedbyYes = 32, //đồng ý
    repliedbyNo = 64, // Từ chối
    repliedbyText = 128 //trả lời bằng text
}


public class MessageModel: Object {
    /*
     "Receiver" : null,
     "ID" : 3390,
     "Status" : 16,
     "Message" : "OK",
     "Sender" : "guest",
     "Poweredby" : " GUEST",
     "Group" : 0,
     "Children" : null,
     "AvatarUrl" : "http:\/\/api.doantiepvien.com.vn:8090\/images\/formstatus\/20.png",
     "ParentID" : 0,
     "Date" : "0001-01-01T00:00:00",
     "Note" : null
    */
    @objc dynamic var id: Int64 = 0
    @objc dynamic var parentID: Int64 = 0
    @objc dynamic var sender: String = ""
    @objc dynamic var receiver: String = ""
    @objc dynamic var group: Int = 0
    @objc dynamic var message: String = ""
    @objc dynamic var rawStatus: Int = MessageStatusType.unsend.rawValue
    var status: MessageStatusType {
        get {
            return MessageStatusType.init(rawValue: rawStatus) ?? MessageStatusType.unsend
        }
        set {
            rawStatus = newValue.rawValue
        }
    }
    
    @objc dynamic var title: String = ""
    @objc dynamic var date: String = ""
    @objc dynamic var note: String = ""
    @objc dynamic var children: String = ""
    @objc dynamic var poweredby: String = ""
    @objc dynamic var avatarUrl: String = ""
    @objc dynamic var isParents: Bool = true
    @objc dynamic var isHtml: Bool = false
    
    @objc dynamic var imageUrl: String = ""
    @objc dynamic var dateRecord: Date? = nil
    @objc dynamic var totalView: Int = 0
    @objc dynamic var totalLike: Int = 0
    @objc dynamic var totalComm: Int = 0
    
    @objc dynamic var objectId: String = ""
    @objc dynamic var navi: Int = 0
    
    var childrens = RealmSwift.List<MessageModel>()
    
    override public class func primaryKey() -> String {
        return "id"
    }
    
    override public static func ignoredProperties() -> [String] {
        return []
    }
    
//    required public init(realm: RLMRealm, schema: RLMObjectSchema) {
//        super.init(realm: realm, schema: schema)
//    }
    
//    required public init() {
//        super.init()
//        //fatalError("init() has not been implemented")
//    }
    
    public override init() {
        super.init()
    }
    
    required public init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
    public init(json: JSON) {
        super.init()
        self.id = json["ID"].int64 ?? 0
        self.parentID = json["ParentID"].int64 ?? 0
        self.sender = json["Sender"].string ?? ""
        self.receiver = json["Receiver"].string ?? ""
        self.group = json["Group"].int ?? 0
        self.message = json["Message"].string ?? ""
        self.isHtml = json["isHtml"].boolValue
        self.status = MessageStatusType.init(rawValue: json["Status"].intValue) ?? .unsend
        self.note = json["Note"].string ?? ""
        self.date = json["Date"].string ?? ""//.dateTime ?? Date()
        self.children = json["children"].string ?? ""
        self.poweredby = json["Poweredby"].string ?? ""
        self.avatarUrl = json["AvatarUrl"].string ?? ""
        
        self.dateRecord = json["DateString"].dateTime ?? Date()
        self.title = json["Title"].string ?? ""
        self.imageUrl = json["ImageUrl"].string ?? ""
        self.totalView = json["TotalView"].int ?? 0
        self.totalLike = json["TotalLike"].int ?? 0
        self.totalComm = json["TotalComm"].int ?? 0
        self.navi = json["Navi"].int ?? 0
        self.objectId = json["ObjectId"].string ?? ""
        
        if let arrayJson = json["Children"].array {
            for item in arrayJson {
                let model = MessageModel(json: item)
                model.isParents = false
                childrens.append(model)
            }
            
        }
        
    }

}
