//
//  ImagePostModel.swift
//  MBN-iOS-App
//
//  Created by Van Trieu Phu Huy on 11/9/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import UIKit

class ImagePost: NSObject {
    
    public var isHiddenButtonDelete: Bool = false
    
    public var index: Int = 0
    
    public var imageUrl: String = ""
    
    public var imageFullUrl: String = ""
    
    public var isImagePost: Bool = false {
        didSet {
            if(self.isImagePost == false) {
                self.image = UIImage(named: "photo-camera.png")
                self.imageUrl = ""
            }
        }
    }
    
    public var image: UIImage?
    
    init(named name: String, isImagePost: Bool = false) {
        super.init()
        self.image = UIImage(named: name)
        self.isImagePost = isImagePost
        if(self.isImagePost == false) {
            self.image = UIImage(named: "photo-camera.png")
        }
        
    }
    
    init(image: UIImage, isImagePost: Bool = true) {
        super.init()
        self.image = image
        self.isImagePost = isImagePost
        if(self.isImagePost == false) {
            self.image = UIImage(named: "image.png")
        }
    }
    

}
