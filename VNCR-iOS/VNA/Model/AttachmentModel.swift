//
//  AttachmentModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/9/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class AttachmentModel: NSObject {
    /*
     "Attachments" : [
     {
     "FileName" : "image-test.jpg",
     "DownloadPath" : "http:\/\/api.doantiepvien.com.vn:8090\/FilesUpload\/14\/f135c541-9980-4997-a018-23737f51f0eb.jpg",
     "FileID" : 30
     }
     ]
    */
    var fileName: String = ""
    var downloadPath: String = ""
    var fileID: Int = 0
    var attachmentID: Int = 0
    var thumbnailPath: String = ""
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.fileName = json["FileName"].string ?? ""
        self.downloadPath = json["DownloadPath"].string ?? ""
        self.fileID = json["FileID"].int ?? 0
        self.thumbnailPath = json["ThumbnailPath"].string ?? ""
    }
    
}
