//
//  FilterModel.swift
//  ReportForBoss
//
//  Created by Van Trieu Phu Huy on 3/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

public class FilterModel: NSObject {
    public var id: Int = 0
    public var filterName: String = "Tất cả"
    public var isSelected: Bool = false
    
    public override init() {
        super.init()
    }

}
