//
//  IconModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/30/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit

enum IconStateType: Int {
    case none = 0
}

class IconModel: NSObject {
    /*
    var tripID: Int = 0
    var tripName: String = ""
    var routing: String = ""
    
    Icon1
    Top
    {
    None = 0,
    Warning = 1,
    OK = 2,
    Comment = 3
    }
    Icon2
    Top
    {
    None = 0,
    VIP = 1,
    Cancel = 2,
    Delay = 3
    }
    
    Icon3
    Top
    {
    None = 0,
    Warning = 1,
    Reported = 2
    }
    Bot
    {
    Normal = 0,
    Important = 1,
    VeryImportant = 2,
    }
    
    "Icon1_State": 0,
    "Icon1_Top": 1,
    "Icon1_Bot": 2,
    "Icon2_State": 0,
    "Icon2_Top": 2,
    "Icon2_Bot": 0,
    "Icon3_State": 0,
    "Icon3_Top": 2,
    "Icon3_Bot": 2
    */

}
