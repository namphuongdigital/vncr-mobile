//
//  UserExamineeModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON


class UserExamineeModel: NSObject {
    
    var id: Int = 0
    var crewID: String = ""
    var fullName: String = ""
    var avatar: String = ""
    var userId: Int = 0
    var task: String = ""
    /*
    "Avatar": "http://api.doantiepvien.com.vn:8090/profiles/2798.JPG",
    "ID": 1510027,
    "CrewID": "2798",
    "FullName": "VU 7 PHAN MINH",
    "UserId": 1831,
    "Task": "P/"
    */
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.id = json["ID"].int ?? 0
        self.crewID = json["CrewID"].string ?? ""
        self.avatar = json["Avatar"].string ?? ""
        self.fullName = json["FullName"].string ?? ""
        self.userId = json["UserId"].int ?? 0
        self.task = json["Task"].string ?? ""
    }
}
