//
//  UserModel.swift
//  MBN-ePaperSmart-iOS
//
//  Created by Van Trieu Phu Huy on 10/25/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//
import Foundation
import SwiftyJSON
import RealmSwift
import Realm

public enum GenderType: Int {
    case female = 0
    case male = 1
}

public class UserModel: Object {
    
    /*
     Account = congnb;
     CodeTV = 4802;
     CrewID = 4802;
     FirstNameVn = Kong;
     Group = "";
     ImageBase64 = "image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEwAACxMBAJqcGAAABJdJREFUeJzt3UtsFXUUx/HvbcFiWyIEeQgLEeWxwEow4isu1OpGUYkh7NiwYUNYEOPenQsSo3HFa0WCLsC4Iz5QY/AVS8AYg0iBhgTExIqSUtJWXPznKt70cmdu73/OnJnfJzlpSJvL+f/PuTO585/5XxAREREREREREREREREREREREREREfdq1gnkqBsYAB4EVgGLgP7kd9eAK8DPwKkk/jbIUTpsNrAReA8YBW6mjN+BQ8CLyWuIM73ATuAi6YveLEaAHcCduY5A2vYScJ6ZF74xhoEX8huGZNUHHKDzhW+MPehoUDhLgZPEL349hoAluYxMWlpBnEN+mlPCvfGHJ7ezGPiF/Itfj9PA3dFHKdOaDRzHrvj1+ByYFXmsMo03sS9+Pd6IPFZpsB6Ywr7w9ZgE1kYdsfyrRjEO/Y3xacxBy38GsS92s3gq4rglcRT7QjeLDyOOW4BlhFU660I3i0nCKqMbXdYJZLSZYi9hdwOvWieRhbcGeNY6gRSesU6grGpkW9O3isuxJiAGT0eARcA86yRSWAzMt04iLU8NcL91Ahk8YJ1AWp4awMO7v85Nrp4aoM86gQz6W/9JMXhqgBvWCWQwbp1AWp4a4C/rBDJwk6unBhixTiCDC9YJpFXkq2qNuoEx4A7rRFoYA+bi5MEST0eAKcLNmEX3LU6KD74aAHysuR+zTqDMHsH+Um+rGIg2eqEG/IR9kZvFyXhDj8PbKeAm8I51ErfxtnUCVTAHuIT9u70xRij+J5TS2Ip9wRtjS9QRy//UCJ8IrItej6P4uqZSCsuA37Av/mX0oKiZpwmLRFbFH0e3gpvbjM1TQpPAphzGJym8DFwnv+KPod1CCmcDcI74xT8LPJzTmCSjecB+4hV/D3BXbqORtj1JWJTpVOE/Bh7LdQTSEU8Qjgh/kr3oV4F9lLzwVbl4MQd4nPCxcQBYDSwk3LgB4RauK4QtX34gXGT6Gkf39omIiIiIiIiIiIg0V5VLwT2ErVv6kui95WcXMJHEGPBHEr8m/y61sjXASuBRYA1wH7A8+bmE9sZ6lfC9Q2cI6wQ/AicID6dMzTxde54boIuwwDNIKPoGYEFO//d14BvgC+Aj4CtK0hBF10P4Kre9hEN0XreAtYpR4CDwSpKjdNhq4C187BM4CrwLPBRlJipmI/AJ9kVtNz4jHLEko+cJGy1YF7BTMYTuIk5lgPCusS5YrDiG9g+YVi/hO4AmsC9S7JgEdidjFsJHubPYFybvOEPJbzhtpQa8RjXe9c1iAngd39dj2tIHfIB9AYoSh/G1Fe6M3AN8j/2kFy1OUIHHzK2+59dLDBPWLkppBWEfHetJLnqMUMImWErYP9d6cr3EMCU6HfQTzm/Wk+othijJtYIj2E+m13i/jfkulF3YT6L32Jl51gtiHbYbOJUlxnH4zeTdhHOY9eSVJb7D2ba+27GftLLFtkwVSClWVw1Get0qey7Gi7o6rEjnqQEqTg1QcWqAilMDVJwaoOLUABWnBqg4NUDFzYr0ul8S7n+XzjlunYCIiIiIiIiIiIiIiIiIiIiIiIiIiBTUP1rFj6/SZdtoAAAAAElFTkSuQmCC";
     IsCrew = 1;
     LastNameVn = "NHAN BA";
     Name = Kong;
     NameTV = "NHAN BA CONG";
     Note = "";
     Phone = "";
     Token = "NDc3MDYzNjE2NDM3OTM0NzI0ODkwMw==";
     UserId = 3778;
     ann = 0;
     baclg = "";
     "class_tv" = " ";
     course = "";
     dob = "1982-12-12 00:00:00";
     dubi = 0;
     "duty_time" = 0;
     "end_date" = "1900-01-01 00:00:00";
     "fly_time" = 0;
     "from_place" = "";
     home = "";
     "int_time" = 0;
     "kn_khac" = "";
     knbgoc = "";
     lc = " ";
     lg = "";
     "ma_tt" = 0;
     "main_base" = "";
     man = 1;
     ngaynhan = "1900-01-01 00:00:00";
     ngayve = "1900-01-01 00:00:00";
     night = 0;
     noicap = "";
     noisinh = "";
     "on_plan" = "";
     "on_route" = "";
     "pag_no" = "";
     "pport_no" = "";
     quoctich = "";
     sochbay = 0;
     "start_date" = "1900-01-01 00:00:00";
     status = "  ";
     "term_tv" = "";
     ttut = 0;
     "type_tv" = " ";
     vip = "";
     */
 
    
    @objc public dynamic var userId: Int = 0
    @objc public dynamic var account: String = ""
    @objc public dynamic var codeTV: String = ""
    @objc public dynamic var crewID: String = ""
    @objc public dynamic var firstNameVn: String = ""
    @objc public dynamic var group: String = ""
    @objc public dynamic var pPortNo: String = ""
    @objc public dynamic var imageBase64: String = ""
    @objc public dynamic var isCrew: Int = 0
    @objc public dynamic var name: String = ""
    @objc public dynamic var nameTV: String = ""
    @objc public dynamic var note: String = ""
    @objc public dynamic var phone: String = ""
    @objc public dynamic var token: String = ""
    @objc public dynamic var job: String = "P"
    @objc public dynamic var mainBase: String = ""
    @objc public dynamic var isInfoConfirmed: Bool = true
    @objc public dynamic var dob: String = ""
    @objc dynamic var rawGender: Int = GenderType.male.rawValue
    public var isGuest:Bool = false
    public var gender: GenderType {
        get {
            return GenderType.init(rawValue: rawGender) ?? GenderType.male
        }
        set {
            rawGender = newValue.rawValue
        }
    }
    
    public init(json: JSON) {
        super.init()
        self.userId = json["UserId"].int ?? 0
        self.account = json["Account"].string ?? ""
        self.codeTV = json["CodeTV"].string ?? ""
        self.crewID = json["CrewID"].string ?? ""
        self.group = json["Group"].string ?? ""
        self.pPortNo = json["pport_no"].string ?? ""
        self.imageBase64 = json["ImageBase64"].string ?? ""
        self.isCrew = json["IsCrew"].int ?? 0
        self.firstNameVn = json["FirstNameVn"].string ?? ""
        self.name = json["Name"].string ?? ""
        self.nameTV = json["NameTV"].string ?? ""
        self.note = json["Note"].string ?? ""
        self.phone = json["Phone"].string ?? ""
        self.token = json["Token"].string ?? ""
        self.mainBase = json["main_base"].string ?? ""
        self.isInfoConfirmed = json["InfoConfirmed"].bool ?? true
        self.dob = json["dob"].string ?? ""
        self.gender = GenderType(rawValue: json["man"].int ?? 1)!
    }
    
    override public class func primaryKey() -> String {
        return "userId"
    }
    
    override public static func ignoredProperties() -> [String] {
        return []
    }
    
    public override init() {
        super.init()
    }
    
//    required public init(realm: RLMRealm, schema: RLMObjectSchema) {
//        super.init(realm: realm, schema: schema)
//    }
    
//    required public init() {
//        super.init()
//        //fatalError("init() has not been implemented")
//    }
    
    required public init(value: Any, schema: RLMSchema) {
        fatalError("init(value:schema:) has not been implemented")
    }
    
}
