//
//  FormCategoryModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/12/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class FormCategoryModel: NSObject, ICategory {
    
    /*
     "ID": 1,
     "Code": null,
     "Name": "Nghỉ phép",
     "Description": "Xin nghỉ phép",
     "Active": null,
     "Created": null,
     "Modified": null,
     "Creator": null,
     "Modifier": null,
     "Creatorid": null,
     "Modifierid": null

    */
    var id: Int = 0
    var code: String = ""
    var name: String = ""
    var descriptionContent: String = ""
    var active: Date = Date()
    var created: String = ""
    var modified: String = ""
    var creator: String = ""
    var modifier: String = ""
    var creatorid: String = ""
    var modifierid: String = ""
    var isSelected: Bool = false
    
    override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.id = json["ID"].int ?? 0
        self.code = json["Code"].string ?? ""
        self.name = json["Name"].string ?? ""
        self.descriptionContent = json["Description"].string ?? ""
        self.active = json["Active"].dateTime ?? Date()
        
    }

    public init(categoryModel: FormCategoryModel) {
        super.init()
        self.id = categoryModel.id
        self.code = categoryModel.code
        self.name = categoryModel.name
        self.descriptionContent = categoryModel.descriptionContent
        self.active = categoryModel.active
        self.isSelected = categoryModel.isSelected
    }
    
}
