//
//  AssessmentModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON


class AssessmentModel: NSObject {
    
    /**
     "ID": 2,
     "FlightID": 46888,
     "SourceCrewID": "1694",
     "SourceFullName": "TRAN HOAI THANH 24",
     "DestinationCrewID": "0976",
     "DestinationFullName": "NGUYEN THI MINH 9",
     "LessonID": 13,
     "Lesson": "P Đánh giá S",
     "TotalScore": 3,
     "Strength": "Điểm mạnh là....",
     "Weakness": "Điểm yếu là....",
     "AssessmentItems": [
     {
     "ID": 5,
     "QuestionID": 49,
     "Question": "P Đánh giá S 1",
     "Factor": 1,
     "Score": 2
     },
     {
     "ID": 6,
     "QuestionID": 50,
     "Question": "P Đánh giá S 2",
     "Factor": 1,
     "Score": 3
     },
     {
     "ID": 7,
     "QuestionID": 51,
     "Question": "P Đánh giá S 3",
     "Factor": 1,
     "Score": 2
     },
     {
     "ID": 8,
     "QuestionID": 52,
     "Question": "P Đánh giá S 4",
     "Factor": 1,
     "Score": 5
     }
     ]
    */
    var id: Int = 0
    var flightID: Int = 0
    var sourceCrewID: String = ""
    var sourceFullName: String = ""
    var destinationCrewID: String = ""
    var destinationFullName: String = ""
    var lessonID: Int = 0
    var lesson: String = ""
    var totalScore: Int = 0
    var strength: String = ""
    var weakness: String = ""
    var assessmentItems = Array<AssessmentItemModel>()
    
    override init() {
        super.init()
    }
    
    public init(json: JSON) {
        
        self.id = json["ID"].int ?? 0
        self.flightID = json["FlightID"].int ?? 0
        self.sourceCrewID = json["SourceCrewID"].string ?? ""
        self.sourceFullName = json["SourceFullName"].string ?? ""
        self.destinationCrewID = json["DestinationCrewID"].string ?? ""
        self.destinationFullName = json["DestinationFullName"].string ?? ""
        self.lessonID =  json["LessonID"].int ?? 0
        self.lesson = json["Lesson"].string ?? ""
        
        self.totalScore =  json["TotalScore"].int ?? 0
        self.strength = json["Strength"].string ?? ""
        self.weakness = json["Weakness"].string ?? ""
        
        if let arrayJson = json["AssessmentItems"].array {
            for item in arrayJson {
                assessmentItems.append(AssessmentItemModel(json: item))
            }
            
        }
        
    }
    

}
