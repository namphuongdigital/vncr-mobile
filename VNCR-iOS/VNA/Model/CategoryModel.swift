//
//  CatetoryModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

public protocol ICategory {
    var id: Int {get set}
    var name: String {get set}
    
}

class CategoryModel: NSObject, ICategory {
    /**
     "CategoryID": 1, "CategoryName": "Category 1", "SubCatetories"
    */
    var id: Int = 0
    var name: String = ""
    
    var categoryID: Int = 0 {
        didSet {
            self.id = categoryID
        }
    }
    var categoryName: String = "" {
        didSet {
            self.name = categoryName
        }
    }
    var subCatetories = Array<SubCategoryModel>()
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.categoryID = json["CategoryID"].int ?? 0
        self.categoryName = json["CategoryName"].string ?? ""
        if let arrayJson = json["SubCatetories"].array {
            for item in arrayJson {
                subCatetories.append(SubCategoryModel(json: item))
            }
            
        }
        
    }
    
    public init(categoryModel: CategoryModel) {
        super.init()
        self.categoryID = categoryModel.categoryID
        self.categoryName = categoryModel.categoryName
        for item in categoryModel.subCatetories {
            self.subCatetories.append(SubCategoryModel(subCategoryModel: item))
        }
        
        
    }

}
