//
//  LeaderPersonModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/30/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import Foundation


enum JobType: String {
    case s = "S"
    case p = "P"
    case y = "Y"
    case b = "B"
    
    
}

enum PositonType: String {
    case cn = "CN"
    case vn = "VN"
    case en = "EN"
    case jp = "JP"
    case kr = "KR"
}



class AirStewardModel: NSObject {
    
    var pos: String = ""
    var exInfo: String = ""
    var workId: String = ""
    var numberName: String = ""
    var fistName: String = ""
    var lastName: String = ""
    var keySign: String = ""
    var job: JobType? = nil
    var postion: Int? = nil
    var ann: PositonType? = nil
    var training: String = ""
    var vip: Int? = nil
    var dutyFree: Int? = nil
    
    override init() {
        super.init()
        
    }
    
    func checkTraining(training: String) -> Bool {
        if(self.training == training) {
            return true
        }
        return false
    }
    
}
