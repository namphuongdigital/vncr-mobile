//
//  SelectorTypeModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/7/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON



public enum CategoryType: String {
    /*
     All = 0    Job = 1,    CA = 2,    ANN = 3,    VIP = 4,    DutyFree = 5
     */
    case all = "0"
    case job = "1"
    case ca = "2"
    case ann = "3"
    case vip = "4"
    case dutyFree = "5"
    case training = "6"
    case totalScore = "7"
}

public typealias SelectorTypeModels = [SelectorTypeModel]

public class SelectorTypeModel: Codable {
    public var name: String
    public var id: Int
    public var categoryIDInt, selectorTypeModelDescription: String

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case id = "ID"
        case categoryIDInt = "CategoryID"
        case selectorTypeModelDescription = "Description"
    }

    public init(name: String, id: Int, categoryID: String, selectorTypeModelDescription: String) {
        self.name = name
        self.id = id
        self.categoryIDInt = categoryID
        self.selectorTypeModelDescription = selectorTypeModelDescription
    }
    
    var categoryId:CategoryType {
        CategoryType.init(rawValue: categoryIDInt)!
    }
}
