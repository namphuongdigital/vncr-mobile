//
//  TripModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/30/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class TripModel: NSObject {
    
    var tripID: Int = 0
    var tripName: String = ""
    var routing: String = ""
    var group: Int = 0
    var type: Int = 0
    var startdate: Date = Date()
    var finishdate: Date = Date()
    var status: Int = 0
    var details = Array<String>()
    
    
    
    public init(json: JSON) {
        /*
         TripID": 2,
         "TripName": "VKG-SGN-VKG-SGN",
         "Routing": "VKG-SGN",
         "Group": "0",
         "Type": "0",
         "Startdate": "2016-08-30 04:00:00",
         "Finishdate": "2016-08-31 04:00:00",
         "Status": "0"
        */
        self.tripID = json["TripID"].int ?? 0
        self.tripName = json["TripName"].string ?? ""
        self.routing = json["Routing"].string ?? ""
        self.group = json["Group"].int ?? 0
        self.type = json["Type"].int ?? 0
        self.startdate = json["Startdate"].dateTime ?? Date()
        self.finishdate = json["Finishdate"].dateTime ?? Date()
        self.status = json["Status"].int ?? 0

        
        if let arrayJson = json["Details"].array {
            for item in arrayJson {
                details.append(item.string ?? "")
            }
            
        }
    }

}
