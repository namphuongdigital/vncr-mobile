//
//  NotifyModel.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/16/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

public class NotifyModel: NSObject {
    
    var ui: Int = 0
    var objectId: String = ""
    var group: Int = 0
    var isHtml: Bool = false
    
    public override init() {
        super.init()
    }
    
    public init(json: JSON) {
        self.ui = json["UI"].int ?? 0
        self.objectId = json["ObjectId"].string ?? ""
        self.group = json["Group"].int ?? 0
        self.isHtml = json["isHtml"].boolValue
    }
    
    

}
