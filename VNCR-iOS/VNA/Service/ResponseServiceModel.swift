//
//  ResponseModel.swift
//  MBN-ePaperSmart-iOS
//
//  Created by Van Trieu Phu Huy on 10/21/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

enum ErrorType: Int {
    case unExpectedError = -99, accessDenied = -1, success = 0, dataInputFailed = 1, updateDataFailed = 2, accessTokenInvalid = 3, dataNotFound = 4, dataInputIsRequire = 5, dataIsLock = 6
    
}

public class ResponseServiceModel: NSObject {

    var status:Int = 0
    var message:String?
    var result:AnyObject?
    
    init(json: JSON) {
        super.init()
        self.status = json["ErrorCode"].int ?? 0
        self.message = json["Message"].string ?? ""
        self.result = json["Data"].string as AnyObject?
    }
    
    init(response: DataResponse<Any>) {
        super.init()
        switch response.result {
        case .success:
            if let value = response.result.value {
                let json = JSON(value)
                if json["ErrorCode"].int != 0 {
                    self.status = json["ErrorCode"].int!
                    self.message = json["Message"].string ?? ""
                    self.result = ServiceErrorModel.serviceError(json: json)
                    
                } else {
                    self.status = json["ErrorCode"].int ?? 0
                    self.message = json["Message"].string ?? ""
                    self.result = json.dictionaryValue["Data"] as AnyObject?
                }
                
            }
        case .failure(let error):
            self.status = 400
            self.message = ""
            let e = error as NSError
            print(e.code)
            self.result = error as AnyObject?
        }
 
    }
    
    
}
