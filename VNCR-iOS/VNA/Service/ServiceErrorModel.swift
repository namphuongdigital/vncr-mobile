//
//  ServiceErrorModel.swift
//  ChupHinhDep
//
//  Created by Van Trieu Phu Huy on 8/10/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import UIKit
import SwiftyJSON

enum ServiceError: Int {
    case ServiceErrorGraphQL = 1,
    ServiceErrorRest = 2
}

public class ServiceErrorModel {

    /*
    class func getLocationErrorDescription(status:INTULocationStatus) ->String {
        if (status == .servicesNotDetermined) {
            return "Error: User has not responded to the permissions alert."
        }
        if (status == .servicesDenied) {
            return "Error: User has denied this app permissions to access device location."
        }
        if (status == .servicesRestricted) {
            return "Error: User is restricted from using location services by a usage policy."
        }
        if (status == .servicesDisabled) {
            return "Error: Location services are turned off for all apps on this device."
        }
        return "An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)"
    }
    */
    
    class func serviceError(responseServiceModel: ResponseServiceModel) -> NSError? {
        return NSError(domain: "serviceError", code: responseServiceModel.status, userInfo: [NSLocalizedDescriptionKey: responseServiceModel.message])
        
    }
    
    class func serviceError(json: JSON) -> NSError? {
        let responseServiceModel = ResponseServiceModel(json: json)
        return NSError(domain: "serviceError", code: responseServiceModel.status, userInfo: [NSLocalizedDescriptionKey: responseServiceModel.message])
        
    }
    
    
    class func serviceErrorGraphQL(json: JSON, domain: String) -> NSError? {

        return NSError(domain: domain, code: ServiceError.ServiceErrorGraphQL.rawValue, userInfo: [NSLocalizedDescriptionKey: json["errors"][0]["message"].string!])
      
    }
    
    class func serviceError(json:JSON, domain:String) -> NSError? {
 
        return NSError(domain: domain, code: json["ErrorCode"].int!, userInfo: [NSLocalizedDescriptionKey: json["Message"].string!])

    }
    
    class func serviceError(message:String, code:Int, domain:String) -> NSError? {
        
        return NSError(domain: domain, code: code, userInfo: [NSLocalizedDescriptionKey: message])
        
    }
    
    class func serviceError(messageError:String, domain:String) -> NSError? {
        
        return NSError(domain: domain, code: -1, userInfo: [NSLocalizedDescriptionKey: messageError])
        
        
    }
    
    
}
