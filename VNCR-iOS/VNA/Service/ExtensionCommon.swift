//
//  ExtensionCommon.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 11/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import AVFoundation
import MobileCoreServices

// MARK: - TABLEVIEW
private var PullRefreshEvent:String = "PullRefreshEvent"
private var RefreshControl:String = "PullRefreshEvent"
extension UITableView {
    
    func pullResfresh(_ event:@escaping (()->Void)) {
        
        if objc_getAssociatedObject(self, &RefreshControl) == nil {
            let refreshControl = UIRefreshControl()
            objc_setAssociatedObject(self, &RefreshControl, refreshControl, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            refreshControl.attributedTitle = nil//NSAttributedString(string: "pull_to_refresh".localized())
            refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
            self.addSubview(refreshControl)
        }
        
        objc_setAssociatedObject(self, &PullRefreshEvent, event, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func endPullResfresh() {
        if let refreshControl = objc_getAssociatedObject(self, &RefreshControl) as? UIRefreshControl {
            if refreshControl.isRefreshing {
                refreshControl.endRefreshing()
            }
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        // override it
        if let event = objc_getAssociatedObject(self, &PullRefreshEvent) as? (()->Void) {
            event()
        }
    }
    
    func showNoData(_ message: String = "No data.".localizedString()) {
        let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        noDataLabel.text          = message
        noDataLabel.textColor     = UIColor.black
        noDataLabel.textAlignment = .center
        self.backgroundView  = noDataLabel
        self.separatorStyle  = .none
    }
    
    func removeNoData() {
        self.backgroundView  = nil
    }
}

extension UINavigationController {
    func makeNavigationBarTransparent() {
        
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black]
        
        self.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.barTintColor = nil
        self.navigationBar.isTranslucent = true
    }
    
    func resetNavigationBar() {
        self.navigationBar.barTintColor = UIColor("#166880")
        self.navigationBar.tintColor = UIColor.white
        self.navigationBar.isTranslucent = false
        self.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
}

extension URL {
    @discardableResult
    func getThumbnail(_ result:((UIImage?, String?)->Void)? = nil) -> UIImage? {
        DispatchQueue.global().async {
            let asset: AVAsset = AVAsset(url: self)
            let imageGenerator = AVAssetImageGenerator(asset: asset)
            imageGenerator.appliesPreferredTrackTransform = true
            do {
                let thumbnailImage = try imageGenerator.copyCGImage(at: CMTimeMake(value: 1, timescale: 60) , actualTime: nil)
                let image = UIImage(cgImage: thumbnailImage)
                DispatchQueue.main.async {
                    result?(image,self.absoluteString)
                }
            } catch let _{
                DispatchQueue.main.async {
                    result?(nil,self.absoluteString)
                }
            }
        }
        return nil
    }

    func mimeType() -> String {
        let pathExtension = self.pathExtension
        if let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, pathExtension as NSString, nil)?.takeRetainedValue() {
            if let mimetype = UTTypeCopyPreferredTagWithClass(uti, kUTTagClassMIMEType)?.takeRetainedValue() {
                return mimetype as String
            }
        }
        return "application/octet-stream"
    }
    var containsImage: Bool {
        let mimeType = self.mimeType()
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return false
        }
        return UTTypeConformsTo(uti, kUTTypeImage)
    }
    var containsAudio: Bool {
        let mimeType = self.mimeType()
        guard let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return false
        }
        return UTTypeConformsTo(uti, kUTTypeAudio)
    }
    var containsVideo: Bool {
        let mimeType = self.mimeType()
        guard  let uti = UTTypeCreatePreferredIdentifierForTag(kUTTagClassMIMEType, mimeType as CFString, nil)?.takeRetainedValue() else {
            return false
        }
        return UTTypeConformsTo(uti, kUTTypeMovie)
    }

}
