//
//  Common.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 11/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class Common: NSObject {
    static let shared : Common = {
        
        let instance = Common()
        
        return instance
        
    }()
    
    let APP_BLUE_COLOR = #colorLiteral(red: 0.09803921569, green: 0.462745098, blue: 0.8235294118, alpha: 1)  //UIColor.init("#1976D2")//01579B
    let APP_GREEN_COLOR = #colorLiteral(red: 0.2196078431, green: 0.5568627451, blue: 0.2352941176, alpha: 1) //UIColor.init("#388E3C")//2ECC71
    let APP_ORANGE_COLOR = #colorLiteral(red: 0.9607843137, green: 0.4862745098, blue: 0, alpha: 1) //UIColor.init("#F57C00")//EF7400
    let APP_BLUE_VS_GREEN = #colorLiteral(red: 0.1843137255, green: 0.3803921569, blue: 0.5019607843, alpha: 1)
    let APP_YELLOW_COLOR = #colorLiteral(red: 0.9921568627, green: 0.8470588235, blue: 0.2078431373, alpha: 1) //UIColor.init("#FDD835")
    let APP_BLUE_WORKING_COLOR = #colorLiteral(red: 0.09803921569, green: 0.7098039216, blue: 0.9960784314, alpha: 1)  //UIColor.init("#19B5FE")//2196F3
    let APP_RED_COLOR = #colorLiteral(red: 0.8549019608, green: 0, blue: 0.09803921569, alpha: 1) //#DA0019
    
    public enum FormStatus: Int {
        case None = 0,
             Waiting = 1,
             Processing = 2,
             Accepted = 3,
             Rejected = 4
    }
}
