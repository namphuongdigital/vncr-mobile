//
//  ExtensionBool.swift
//  ChupHinhDep
//
//  Created by Van Trieu Phu Huy on 9/8/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import UIKit

public extension Bool {
    
    public func toString() -> String {
        if(self == true){
            return "true"
        } else {
            return "false"
        }
    }
}
