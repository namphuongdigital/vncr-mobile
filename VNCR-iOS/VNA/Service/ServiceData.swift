//
//  ServiceData.swift
//  MBN-ePaperSmart-iOS
//
//  Created by Van Trieu Phu Huy on 10/21/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import BoltsSwift

extension Alamofire.DataRequest {
    public func LogRequest() -> Self {
        //Your logic for logging
        #if DEBUG
        print("\(#function): " + (request?.url?.absoluteString ?? ""))
        #endif
        return self
    }
}

public extension Data {
    func load<T:Decodable>() throws -> T? {
        do {
            return try newJSONDecoder().decode(T.self, from: self)
        } catch let er {
            throw er
        }
    }
}

public extension Encodable {
    func jsonData() throws -> Data {
        return try newJSONEncoder().encode(self)
    }
}

func newJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .iso8601
    }
    return decoder
}

func newJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .iso8601
    }
    return encoder
}

extension Dictionary {
    
    var jsonString: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }
    
    func printJson() {
        print(jsonString)
    }
    
}

class Formatter {
    
    private static var internalJsonDateFormatter: DateFormatter?
    private static var internalJsonDateTimeFormatter: DateFormatter?
    private static var internalNodeJSDateTimeFormatter: DateFormatter?
    
    static var jsonDateFormatter: DateFormatter {
        if (internalJsonDateFormatter == nil) {
            internalJsonDateFormatter = DateFormatter()
            internalJsonDateFormatter!.locale = NSLocale.current
            internalJsonDateFormatter!.dateFormat = "yyyy-MM-dd"//2016-06-21 14:11:36
        }
        return internalJsonDateFormatter!
    }
    
    static var jsonDateTimeFormatter: DateFormatter {
        if (internalJsonDateTimeFormatter == nil) {
            internalJsonDateTimeFormatter = DateFormatter()
            internalJsonDateTimeFormatter!.dateFormat = "yyyy-MM-dd HH:mm:ss"//2016-09-01 05:00:00
        }
        return internalJsonDateTimeFormatter!
    }
    
    
}
extension JSON {
    
    public var date: Date? {
        get {
            switch self.type {
            case .string:
                let showTime = Formatter.jsonDateFormatter.date(from: self.object as! String)
                let finalTime = showTime
                return finalTime
            default:
                return nil
            }
        }
    }
    
    public var dateTime: Date? {
        get {
            switch self.type {
            case .string:
                let showTime = Formatter.jsonDateTimeFormatter.date(from: self.object as! String)
                let finalTime = showTime
                return finalTime
            default:
                return nil
            }
        }
    }
    
    
    
}
extension Date {
    
    
    public var dateTimeToString: String? {
        get {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd"
            let stringFromDate = df.string(from: self)
            return stringFromDate
            
        }
    }
    
    public var dateTimeToddMMyyyy: String? {
        get {
            let df = DateFormatter()
            df.dateFormat = "dd/MM/yyyy"
            let stringFromDate = df.string(from: self)
            return stringFromDate
            
        }
    }
    public var dateTimeToTimeHHmm: String? {
        get {
            let df = DateFormatter()
            df.dateFormat = "HH:mm"
            let stringFromDate = df.string(from: self)
            return stringFromDate
            
        }
    }
    
    public var dateTimeToDateTimeHHmm: String? {
        get {
            let df = DateFormatter()
            df.dateFormat = "ddMMM HH:mm"
            let stringFromDate = df.string(from: self)
            return stringFromDate
            
        }
    }
    
    public var dateTimeToHHmmddMMyyyy: String? {
        get {
            let df = DateFormatter()
            df.dateFormat = "HH:mm dd/MM/yyyy"
            let stringFromDate = df.string(from: self)
            return stringFromDate
            
        }
    }
    
    public var dateTimeToddMMM: String {
        get {
            let df = DateFormatter()
            df.dateFormat = "dd\nMMM"
            let stringFromDate = df.string(from: self)
            return stringFromDate
            
        }
    }
    
    public var dateTimeTodMMM: String {
        get {
            let df = DateFormatter()
            df.dateFormat = "d MMM yyyy"
            df.locale = Locale(identifier: "en_US")
            let stringFromDate = df.string(from: self)
            return stringFromDate.replacingOccurrences(of: " ", with: "")
            
        }
    }
    
    
    /**
     "yyyy-MM-dd"
    */
    public var dateTimeToYYYYMMdd: String {
        get {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd"
            let stringFromDate = df.string(from: self)
            return stringFromDate
            
        }
    }
    
    public var dateTimeToArticle: String {
        get {
            let df = DateFormatter()
            df.dateFormat = "EEE, dd MMM HH:mm"
            let stringFromDate = df.string(from: self)
            return stringFromDate
            
        }
    }
}

extension String {
    /**
     "dd/MM/yyyy"
     */
    
    public var stringToDateYYYYMMdd: Date? {
        get {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
            let dateFromString = df.date(from: self)
            return dateFromString
            
        }
    }
    
    public var stringToDate: Date? {
        get {
            let df = DateFormatter()
            df.dateFormat = "dd/MM/yyyy"
            let dateFromString = df.date(from: self)
            return dateFromString
            
        }
    }
    
    public func localizedString() -> String {
        return NSLocalizedString(self, comment: "")
    }
    
}



public enum SortBy: String {
    case desc = "DESC"
    case asc = "ASC"
}

public enum StatusArticle: String {
    case active = "active"
    case inactive = "inactive"
    case removed = "removed"
    case rejected = "rejected"
}

public enum ActionUpdateSupportType: Int {
    
    case add = 1
    case complete = 2
    case take = 3
    case remove = 4
    case hide = 5

    
}


enum ReponseReturnType : Int {
    case status = 0, message = 1, result = 2, reponse = 3
}


public class ServiceData : NSObject {
    
    public static var username: String = ""
    
    public static var password: String = ""
    
    public var userModel: UserModel?
    
    public var userId:Int? {
        UserDefaults.standard.integer(forKey: "userId")
    }
    
    public var token:String? {
        UserDefaults.standard.string(forKey: "token")
    }
    
    public var crewID:Int? {
        UserDefaults.standard.integer(forKey: "crewID")
    }
    
    var listMainPilot: Array<PilotModel>?
    
    var listSecondPilot: Array<PilotModel>?
    
    public var listFilterModel = Array<Array<FilterModel>>()
    
    public var listFilterSelected = [Array<NSObject>]()
    
    private var dataRequestTaskFlightInfoList: DataRequest?
    
    var serviceHeaders: [String : String]!
    
    public var hostAPI: String {
        get {
//            var url = "http://doantiepvien.vn/api"
//            var url = "http://doantiepvien.vn/apis"
            var url = "https://api.crew.vn/api"
            if(UserDefaults.standard.value(forKey: "hostAPI") != nil) {
                url = UserDefaults.standard.value(forKey: "hostAPI") as! String
            }
            return url
        }
    }
    
    private let userLogin = "/login"
    
    // MARK: -  FLIGHT
    private let flightInfoList = "/flight/GetGeneralScheduleFlights" //replace for infolistexcludetrip"
    
    private let flightInfoDetail = "/flight/GetFlightInfoDetail" // get detail a flight
    
    private let flightSaveBriefing = "/flight/SaveBriefing" // add new briefing
    
    private let flightReloadInfo = "/Flight/ReloadFlightInfo" // synchronize with third services
    
    private let flightDeleteBriefing = "/Flight/DeleteBriefing" // delete a briefing
    
    private let flightCrewtaskgetlist = "/flight/crewtaskgetlist"
    
    private let flightCrewtaskgetlistsupporttrip = "/flight/crewtaskgetlistsupporttrip"
    
    private let flightCrewtaskcategorylist = "/flight/crewtaskcategorylist"
    
    private let flightCrewtaskUpdate = "/flight/crewtaskupdate"
    
    private let flightCrewtaskupdatesupporttrip = "/flight/crewtaskupdatesupporttrip"
    
    private let flightPilotGetList = "/flight/pilotgetlist"
    
    private let flightFinalReportGetInfo = "/flight/finalreportgetinfo"
    
    private let flightFinalReportCategoryGetList = "/flight/finalreportcategorygetlist"
    
    private let flightFinalReportUpdateInfo = "/flight/finalreportupdateinfo"
    
    private let filemanagerFlightreportuploadfiles = "/filemanager/flightreportuploadfiles"
    
    private let filemanagerDeletefiles = "/filemanager/deletefiles"
    
    private let flightFinalreportgetlist = "/flight/finalreportgetlist"
    
    private let flightFinalReportGetAll = "/Flight/FinalReportGetAll"
    
    private let flightPersonalschedule = "/flight/GetPersonalScheduleFlights" //replace for PersonalSchedule
    
    private let flightFinalReportDelete = "/flight/finalreportdelete"
    
    private let flightAssessmentcrewdutylist = "/flight/assessmentcrewdutylist"
    
    private let flightAssessmentgetinfo = "/flight/assessmentgetinfo"
    
    private let flightAssessmentupdateinfo = "/flight/assessmentupdateinfo"
    
    private let getPassengerList = "/Flight/GetPassengerList" // Dai: url lấy danh sách ghế ngồi
    
    private let lotusShopSubmitReport = "/Flight/LotusShopSubmitReport" // report a lotusshop's passenger
    
    private let flightCrewtaskauthorize = "/flight/crewtaskauthorize"
    
    private let userGetinfobycrewid = "/user/getinfobycrewid"
    
    private let userConfirminfo = "/user/confirminfo"
    
    private let userCheckaccesstoken = "/user/checkaccesstoken"
    
    private let supportList = "/support/list"
    
    private let supportDetail = "/support/detail"
    
    private let supportUpdate = "/support/update"
    
    private let filemanagerSupportuploadfiles = "/filemanager/supportuploadfiles"
    
    private let register4vnc = "/Device/Register4vnc"
    
    private let OTP4VNC = "/Device/OTP4VNC"
    
    private let formCategory = "/Form/Category"
    
    private let formList = "/Form/List"
    
    private let formCreate = "/Form/Create"
    
    private let formDetail = "/Form/Detail"
    
    private let formUpdate = "/Form/Update"
    
    private let formRemove = "/Form/Remove"
    
    private let formGetTermsAndConditions = "/Form/GetTermsAndConditions"
    
    private let formUploadFiles = "/filemanager/formuploadfiles"
    
    private let incomeMarkAsRead = "/income/MarkAsRead"
    
    private let incomeDone = "/income/Done"
    
    private let incomeAdd = "/income/Add"
    
    private let incomeDetailWebview = "/income/Webview"
    
    private let incomeDetail = "/income/Detail"
    
    private let incomeList = "/income/List"
    
    private let newsMarkAsRead = "/news/MarkAsRead"
    
    private let newsDone = "/news/Done"
    
    private let newsAdd = "/news/Add"
    
    private let newsDetailWebview = "/news/Webview"
    
    private let newsDetail = "/news/Detail"
    
    private let newsList = "/news/List"
    
    private let messageList = "/Message/List"
    
    private let messageListArchives = "/Message/ListArchives"
    
    private let messageListNewMessages = "/Message/ListNewMessages"
    
    private let messageCount = "/Message/Count"
    
    private let messageMarkAsRead = "/Message/MarkAsRead"
    
    private let messageDone = "/Message/Done"
    
    private let messageAdd = "/Message/Add"
    
    private let messageDetail = "/Message/Detail"
    
    private let messageDetailWebview = "/Message/Webview"
    
    private let deviceVerifyQR = "/device/verifyQR"
    
    private let surveyList = "/Survey/List"
    
    private let surveyDetail = "/Survey/Detail"
    
    private let surveyUpdate = "/Survey/Update"
    
    private let surveyRemove = "/Survey/Remove"
    
    private let surveySignature = "/Survey/Signature"
    
    private let ojtExaminees = "/OJT/Examinees"
    
    private let ojtList = "/OJT/List"
    
    private let ojtDetail = "/OJT/Detail"
    
    private let ojtUpdate = "/OJT/Update"
    
    private let ojtSignature = "/OJT/Signature"
    
    private let deviceMydevices = "/Device/Mydevices"
    
    private let deviceDeActivate = "/Device/DeActivate"
    
    private let deviceSetMaster = "/Device/SetMaster"
    
    private let deviceCommands = "/Device/Commands"
    
    private let userGetEmployees = "/User/GetEmployees"
    
    private let userGetEmployeeInfo = "/User/GetEmployeeInfo"
    
    private let statisticFlight = "/Statistic/flight"
    
    private let statisticFlightDetails = "/Statistic/FlightDetails"
    
    private let statisticFlightDetailPie = "/Statistic/FlightDetailPie"
    
    private let statisticFlightDetailBar = "/Statistic/FlightDetailBar"
    
    private let statisticDashboard = "/Statistic/Dashboard"
    
    private let statisticDiscover = "/Statistic/Discover"
    
    private let cavaGetcava = "/Dashboard/GetInfo" // Dai: Cava/GetCava => Dashboard/GetInfo
    
    private let cavaGetBanInfo = "/Dashboard/GetBanInfo" // Dai: Cava/* => Dashboard/*
    
    private let cavaGetlist = "/Dashboard/Getlist" // Dai: Cava/* => Dashboard/*
    
    private let cavaGetDetails = "/Dashboard/GetDetails" // Dai: Cava/* => Dashboard/*
    
    private let flightGetSeatmap = "/flight/GetSeatmap"
    
    private let cavaGetSectionList = "/Dashboard/GetSections" // Dai: Cava/* => Dashboard/*
    
    private let cavaGetSectionDetails = "/Dashboard/GetSectionDetails" // Dai: Cava/* => Dashboard/*
    
    private let formDoAction = "/Form/DoAction"
    
    private let weeklyReportList = "/Report/List"
    
    private let weeklyReportDetail = "/Report/Detail"
    
    private let sendWeeklyReportMail = "/Report/SendMail"
    
    private let weeklyReportGetDepartments = "/Report/GetDepartments"
    
    private let formHistory = "/Form/GetHistory"
    
    private let newsGetCategory = "/News/Category"

    private let formGetFilterList = "/Form/GetFilterList"
    
    private let checkAwaitingApproval = "/User/CheckAwaitingApproval"
    
    private let getSurveyPage = "/set/list"
    
    private let updateSurveyPage = "/set/update"
    
    public var oneSignalUserId: String = "3b6f5618-5806-4420-83d6-a295f44e5f64"
    
    public var unreadMessageCount: Int = 0
    
    public var isShowAlertNotificationPermission: Bool  = false
    
    var _locationLatLong: String = ""
    public var locationLatLong: String {
        get{
            if(self._locationLatLong.count == 0) {
                self.taskGetLocation().continueOnSuccessWith(continuation: { task in
                    let currentLocation = task as! CLLocation
                    self._locationLatLong = String(format: "%f;%f", currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
                }).continueOnErrorWith(continuation: { error in
                    print("")
                })
            }
            return self._locationLatLong
        }
    }
    
    //MARK: Shared Instance
    
    public static let sharedInstance : ServiceData = {
        let instance = ServiceData()
        instance.loadFilter()
        let version = Bundle.main.infoDictionary?["CFBundleVersion"] as? String
        
        // Dai: Update for header on API:
        // X_REQUEST_API_VERSION: 2.0 => 3.0 (for validation token in next version)
        
        instance.serviceHeaders = ["Content-Type": "application/x-www-form-urlencoded", "X_API_ID": "VN_CREW_2017", "X_API_KEY": "KE4Sc6zqaaHHlpkzStfdpwcmnkvposK6", "X_REQUEST_API_VERSION": "3.0", "X_APP_LANGUAGE": "en-US", "X_TOKEN": instance.userModel?.token ?? "", "X_REQUEST_UDID": UIDevice.current.uuidForKeychain(), "X_REQUEST_OS_VERSION": UIDevice.current.systemVersion, "X_REQUEST_PLATFORM": "iOS", "X_LOCATION": instance.locationLatLong, "X_REQUEST_DEVICE_TYPE": DeviceTypes.deviceModelName(), "X_REQUEST_DEVICE_NAME": UIDevice.current.name, "X_REQUEST_DESCRIPTION": "", "Build": version ?? ""]
        //
        return instance
    }()
    
    
  
    func loadFilter() {
        
        /*
         
         enum StatusSupportType: Int {
         case pending = 1
         case completed = 2
         case taken = 3
         case removed = 4
         case hide = 5
         }
         
         "Filter": "0" => All
         "Filter": "1,2,3" => for filter task status (Completed, InProgress, Late)
         "Department": "0" => All
         "Department": "DepId1,DepId2,DepId3..." => for filter by department
        = 0 : get only message with status pending
        = 1 : get all
        */
        
        var arraySection1 = Array<FilterModel>()
        var filterModel = FilterModel()
        filterModel.id = 1
        filterModel.filterName = "All".localizedString()
        arraySection1.append(filterModel)
        filterModel = FilterModel()
        filterModel.id = 0
        filterModel.filterName = "For me!".localizedString()
        arraySection1.append(filterModel)
        listFilterModel.append(arraySection1)
        
        var arraySection2 = Array<FilterModel>()
        filterModel = FilterModel()
        filterModel.id = 0
        filterModel.filterName = "All".localizedString()
        arraySection2.append(filterModel)
        filterModel = FilterModel()
        filterModel.id = 1
        filterModel.filterName = "Pending".localizedString()
        arraySection2.append(filterModel)
        filterModel = FilterModel()
        filterModel.id = 2
        filterModel.filterName = "Completed".localizedString()
        arraySection2.append(filterModel)
        filterModel = FilterModel()
        filterModel.id = 3
        filterModel.filterName = "Taken".localizedString()
        arraySection2.append(filterModel)
        listFilterModel.append(arraySection2)
        
        listFilterSelected.append([listFilterModel[0][0]])
        listFilterSelected.append([listFilterModel[1][0]])
    }
    
    public func taskShowFormUserLogin() -> Task<AnyObject> {
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if let storyboard = UIApplication.topViewController()?.storyboard {
            let viewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
            viewController.taskCompletionSource = TaskCompletionSource<AnyObject>()
            viewController.taskCompletionSource?.task.continueOnSuccessWith(continuation: { task in
                let token = self.userModel?.token ?? ""
                viewController.dismiss(animated: true, completion: {
                    taskCompletionSource.trySet(result: token as AnyObject)
                })
            }).continueOnErrorWith(continuation: { error in
                taskCompletionSource.set(error: error)
            }).continueWith(continuation: { task in
                if(task.cancelled) {
                    taskCompletionSource.tryCancel()
                }
            })
            UIApplication.topViewController()?.present(viewController, animated: true, completion: nil)
            
        }
        return taskCompletionSource.task
        
    }
  
    func getAlertNotificationsPermission() -> PermissionScope? {
        
        if(isShowAlertNotificationPermission == true) {
            return nil
        }
        isShowAlertNotificationPermission = true
        
        let singlePscope = PermissionScope()
        singlePscope.addPermission(NotificationsPermission(), message: "Notification")
        singlePscope.headerLabel.text = "VNCrew"
        singlePscope.bodyLabel.text = "Allow notification"
        singlePscope.closeButton.setTitle("Close", for: UIControl.State())
        return singlePscope
    }
    
    public func taskShowFormRegisterDevice() -> Task<AnyObject> {
        ApplicationData.sharedInstance.deleteAllUserLogin()
        self.userModel = UserModel()
        self.userModel!.token = "0"
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if let storyboard = UIApplication.topViewController()?.storyboard {
            
            let viewController = storyboard.instantiateViewController(withIdentifier: "RegisterDeviceViewController") as! RegisterDeviceViewController
            viewController.taskCompletionSource = TaskCompletionSource<AnyObject>()
            viewController.taskCompletionSource?.task.continueOnSuccessWith(continuation: { task in
                let token = self.userModel?.token ?? ""
                viewController.dismiss(animated: true, completion: {
                    taskCompletionSource.trySet(result: token as AnyObject)
                })
            }).continueOnErrorWith(continuation: { error in
                taskCompletionSource.set(error: error)
            }).continueWith(continuation: { task in
                if(task.cancelled) {
                    taskCompletionSource.tryCancel()
                }
            })
            let navigation = UINavigationController(rootViewController: viewController)
            UIApplication.topViewController()?.present(navigation, animated: true, completion: nil)
            
        }
        return taskCompletionSource.task
        
    }
   
    public func taskGetUserLogin(username: String = username, password: String = password) -> Task<AnyObject> {
        
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, userLogin)
        
        let parameters = ["username": username, "password": password]
        Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
            let responseServiceModel = ResponseServiceModel(response: response)
            if let error = responseServiceModel.result as? NSError {
                taskCompletionSource.set(error: error)
            } else {
                if let result = responseServiceModel.result as? JSON {
                    ServiceData.username = username
                    ServiceData.password = password
                    
                    let userModel = UserModel(json: result)
                    self.userModel = userModel
                    ApplicationData.sharedInstance.updateUserLogin(userModel: userModel)
                    taskCompletionSource.set(result: userModel)
                }
                
            }
            taskCompletionSource.tryCancel()
            
        }
        
        return taskCompletionSource.task
        
    }
    
    //
    public func taskFormCategory() -> Task<AnyObject> {
      
        let stringPath = String(format:"%@%@", hostAPI, formCategory)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: nil, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    //
    public func taskFormList(keyword: String, categoriesId: Int, pageIndex: Int, filter: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, formList)
        let parameters: [String : Any] = ["category": categoriesId, "keyword": keyword, "pageIndex": pageIndex, "Filter": filter]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    //
    public func taskFormCreate() -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, formCreate)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: nil, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    //Form/Detail
    public func taskFormDetail(id: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, formDetail)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    //Form/Update
    public func taskFormUpdate(formModel: FormModel) -> Task<AnyObject> {
        /*
        {
        "ID": 0,
        "Category_ID": 1,
        "From_Date": "2017-11-10T00:00:00",
        "To_Date": "2017-11-14T00:00:00",
        "Route": null,
        "Content": "Xin nghỉ phép đi nghỉ mát chế độ",
        "Attachments": 1,
        "Date": "2017-11-10T00:00:00"
        }
        */
        
        let stringFormModelInfo = String(format: "{\"ID\":%d,\"Category_ID\":%d,\"From_Date\":\"%@\",\"To_Date\":\"%@\",\"Route\":\"%@\",\"Content\":\"%@\",\"Title\":\"%@\",\"Attachments\":%d,\"Date\":\"%@\"}", formModel.id, formModel.categoryID, formModel.fromDate?.dateTimeToYYYYMMdd ?? "", formModel.toDate?.dateTimeToYYYYMMdd ?? "", "", formModel.descriptionContent, formModel.title, formModel.attachmentGroupID, "")
        
        let stringPath = String(format:"%@%@", hostAPI, formUpdate)
        let parameters: [String : Any] = ["FormModel": stringFormModelInfo]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    //Form/Remove
    public func taskFormRemove(id: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, formRemove)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    
    /**
     - FormID
     - AttachmentGroupID
     */
    public func taskFormUploadFile(userId: Int,
                                   token: String,
                                   formID: Int,
                                   fileName: String,
                                   image: UIImage,
                                   attachmentGroupId: Int) -> Task<AnyObject> {
        
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, formUploadFiles)
        if token.count > 0, userId > 0 {
            Alamofire.upload(multipartFormData: {
                multipartFormData in
                
                multipartFormData.append(String(format: "%d", userId).data(using: .utf8, allowLossyConversion: false)!, withName: "UserID")
                multipartFormData.append(token.data(using: .utf8, allowLossyConversion: false)!, withName: "Token")
                multipartFormData.append(String(format: "%d", formID).data(using: .utf8, allowLossyConversion: false)!, withName: "FormID")
                multipartFormData.append(String(format: "%d", attachmentGroupId).data(using: .utf8, allowLossyConversion: false)!, withName: "AttachmentGroupID")
                multipartFormData.append(image.jpegData(compressionQuality: 1.0)!, withName: "File_Attach", fileName: fileName, mimeType: "image/jpeg")
                
            }, to: stringPath, headers: self.serviceHeaders) {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _ ):
                    upload.validate().LogRequest().responseJSON(completionHandler: { response in
                        let responseServiceModel = ResponseServiceModel(response: response)
                        if let error = responseServiceModel.result as? NSError {
                            taskCompletionSource.set(error: error)
                        } else {
                            if let result = responseServiceModel.result as? JSON {
                                if let array = result["AttachmentList"].array {
                                    if(array.count > 0) {
                                        let attachmentModel = AttachmentModel(json: array[0])
                                        attachmentModel.attachmentID = result["AttachmentID"].int ?? 0
                                        taskCompletionSource.set(result: attachmentModel as AnyObject)
                                    }
                                }
                                
                            }
                            
                        }
                        taskCompletionSource.tryCancel()
                    })
                    
                case .failure(let encodingError):
                    taskCompletionSource.set(error: encodingError)
                    print(encodingError)
                }
            }
        }
        
        return taskCompletionSource.task
        
    }
    
    func taskSurveySignature(id: Int, comment: String) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id, "Comment": comment]
        let stringPath = String(format:"%@%@", hostAPI, surveySignature)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .reponse)
        
    }
    
    func taskSurveyRemove(id: Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id]
        let stringPath = String(format:"%@%@", hostAPI, surveyRemove)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskSurveyUpdate(surveyModel: SurveyModel) -> Task<AnyObject> {
        
        
        var stringItems = ""
        for index in 0..<surveyModel.surveyItems.count {
            if(index == (surveyModel.surveyItems.count - 1)) {
                stringItems = String(format: "%@%@", stringItems, "{\"ID\":\"\(surveyModel.surveyItems[index].id)\",\"Score\":\"\(surveyModel.surveyItems[index].score)\",\"Comment\":\"\(surveyModel.surveyItems[index].comment)\"}")
            } else {
                stringItems = String(format: "%@,%@", "{\"ID\":\"\(surveyModel.surveyItems[index].id)\",\"Score\":\"\(surveyModel.surveyItems[index].score)\",\"Comment\":\"\(surveyModel.surveyItems[index].comment)\"}", stringItems)
                
            }
        }
        let stringModelInfo = "{\"ID\":\(surveyModel.id),\"FlightID\":\(surveyModel.flightID),\"FltInfo\":\"\(surveyModel.fltInfo)\",\"CID\":\"\(surveyModel.cid)\",\"FullName\":\"\(surveyModel.fullName)\",\"Date\":\"\",\"SeatNo\": \"\(surveyModel.seatNo)\",\"PurserName\":\"\(surveyModel.purserName)\",\"Comment\":\"\(surveyModel.comment)\",\"Suggestion\": \"\(surveyModel.suggestion)\",\"CR_Survey_Category_ID\":\"\(surveyModel.crSurveyCategoryID)\",\"CR_Survey_Item\":[\(stringItems)]}"
        
        let parameters: [String : Any] = ["SurveyModel": stringModelInfo]
        
        
        let stringPath = String(format:"%@%@", hostAPI, surveyUpdate)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .reponse)
        
    }
    
    /**
     - Id
     - TemplateId
    */
    func taskSurveyDetail(id: Int, templateId: Int, flightId: Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id, "TemplateId": templateId, "FlightId": flightId]
        let stringPath = String(format:"%@%@", hostAPI, surveyDetail)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    
    /**
     */
    public func taskSurveyList(flightID: Int, keyword: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, surveyList)
        let parameters: [String : Any] = ["FlightID": flightID, "Keyword": keyword]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskOJTUpdate(ojtModel: OJTModel) -> Task<AnyObject> {

        var stringItems = ""
        for index in 0..<ojtModel.ojtItems.count {
            if(index == (ojtModel.ojtItems.count - 1)) {
                stringItems = String(format: "%@%@", stringItems, "{\"ID\":\"\(ojtModel.ojtItems[index].id)\",\"Score\":\"\(ojtModel.ojtItems[index].score)\",\"Comment\":\"\(ojtModel.ojtItems[index].comment)\"}")
            } else {
                stringItems = String(format: "%@,%@", "{\"ID\":\"\(ojtModel.ojtItems[index].id)\",\"Score\":\"\(ojtModel.ojtItems[index].score)\",\"Comment\":\"\(ojtModel.ojtItems[index].comment)\"}", stringItems)
                
            }
        }
        let stringModelInfo = "{\"ID\":\(ojtModel.id),\"FlightID\":\(ojtModel.flightID),\"FltInfo\":\"\(ojtModel.fltInfo)\",\"Date\":\"\",\"Comment\":\"\(ojtModel.comment)\",\"Suggestion\": \"\(ojtModel.suggestion)\",\"CR_OJT_Lesson_ID\":\"\(ojtModel.crOJTLessonID)\",\"CR_OJT_Item\":[\(stringItems)]}"
        
        let parameters: [String : Any] = ["OJTModel": stringModelInfo]
        
        
        let stringPath = String(format:"%@%@", hostAPI, ojtUpdate)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .reponse)
        
    }
    
    func taskOJTDetail(id: Int, templateId: Int, flightId: Int, cid: String) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id, "TemplateId": templateId, "FlightId": flightId, "cid": cid]
        let stringPath = String(format:"%@%@", hostAPI, ojtDetail)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    public func taskOJTExaminees(flightID: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, ojtExaminees)
        let parameters: [String : Any] = ["FlightID": flightID]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    public func taskOJTList(flightID: Int, cid: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, ojtList)
        let parameters: [String : Any] = ["FlightID": flightID, "CID": cid]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskOJTSignature(id: Int, comment: String) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id, "Comment": comment]
        let stringPath = String(format:"%@%@", hostAPI, ojtSignature)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .reponse)
        
    }
    
    func taskDeviceMydevices(keyword: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, deviceMydevices)
        let parameters: [String : Any] = ["Keyword": keyword]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskDeviceDeActivate(id: Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id]
        let stringPath = String(format:"%@%@", hostAPI, deviceDeActivate)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .reponse)
        
    }
    
    func taskDeviceSetMaster(id: Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id]
        let stringPath = String(format:"%@%@", hostAPI, deviceSetMaster)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .reponse)
        
    }
    
    func taskDeviceCommands(keyword: String, pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Keyword": keyword, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, deviceCommands)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskUserGetEmployees(keyword: String, pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Keyword": keyword, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, userGetEmployees)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskUserGetEmployeeInfo(id: Int, keyword: String, pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id, "Keyword": keyword, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, userGetEmployeeInfo)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskStatisticFlight(date: Date, keyword: String, pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Date": date.dateTimeTodMMM, "Keyword": keyword, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, statisticFlight)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskStatisticDetails(id: Int, keyword: String, pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id, "Keyword": keyword, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, statisticFlightDetails)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskStatisticDetailPie(id: Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id]
        let stringPath = String(format:"%@%@", hostAPI, statisticFlightDetailPie)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskStatisticDetailBar(id: Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id]
        let stringPath = String(format:"%@%@", hostAPI, statisticFlightDetailBar)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskStatisticDashboard() -> Task<AnyObject> {
        let parameters: [String : Any] = [:]
        let stringPath = String(format:"%@%@", hostAPI, statisticDashboard)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskStatisticDiscover() -> Task<AnyObject> {
        let parameters: [String : Any] = [:]
        let stringPath = String(format:"%@%@", hostAPI, statisticDiscover)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskGetBannerInfo(id: Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id]
        let stringPath = String(format:"%@%@", hostAPI, cavaGetBanInfo)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskGetcava() -> Task<AnyObject> {
        let parameters: [String : Any] = [:]
        let stringPath = String(format:"%@%@", hostAPI, cavaGetcava)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskCavaGetlist(id: Int, date: Date, keyword: String, pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id, "Date": date.dateTimeTodMMM, "Keyword": keyword, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, cavaGetlist)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    func taskCavaGetDetails(id: Int, pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, cavaGetDetails)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func taskFlightGetSeatmap(flightId: Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["FlightId": flightId]
        let stringPath = String(format:"%@%@", hostAPI, flightGetSeatmap)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    
    /**
     - Id (Message Id)
     */
    public func taskIncomeMarkAsRead(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, incomeMarkAsRead)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /**
     - Id (Message Id)
     */
    public func taskIncomeDone(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, incomeDone)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /**
     - MessageModel
     {
     "ParentID": 1,   -- Khi reply, ParentID là ID of Message chọn reply.
     "Status": 16,
     "Message": "OK"
     }
     */
    
    public func taskIncomeAdd(parentID: Int64, message: String, messageStatusType: MessageStatusType) -> Task<AnyObject> {
        let stringMessageModel = String(format: "{\"ParentID\":%d,\"Status\":%d,\"Message\":\"%@\"}", parentID, messageStatusType.rawValue, message)
        let stringPath = String(format:"%@%@", hostAPI, incomeAdd)
        let parameters: [String : Any] = ["MessageModel": stringMessageModel]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    public func taskIncomeWebView(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, incomeDetailWebview)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    public func taskIncomeDetail(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, incomeDetail)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /*
     - Keyword
     - PageIndex
     */
    public func taskIncomeList(keyword: String, pageIndex: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, incomeList)
        let parameters: [String : Any] = ["Keyword": keyword, "Pageindex": pageIndex]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    
    /**
     - Id (Message Id)
     */
    public func taskNewsMarkAsRead(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, newsMarkAsRead)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /**
     - Id (Message Id)
     */
    public func taskNewsDone(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, newsDone)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /**
     - MessageModel
     {
     "ParentID": 1,   -- Khi reply, ParentID là ID of Message chọn reply.
     "Status": 16,
     "Message": "OK"
     }
     */
    
    public func taskNewsAdd(parentID: Int64, message: String, messageStatusType: MessageStatusType) -> Task<AnyObject> {
        let stringMessageModel = String(format: "{\"ParentID\":%d,\"Status\":%d,\"Message\":\"%@\"}", parentID, messageStatusType.rawValue, message)
        let stringPath = String(format:"%@%@", hostAPI, newsAdd)
        let parameters: [String : Any] = ["MessageModel": stringMessageModel]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    public func taskNewsWebView(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, newsDetailWebview)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    
    public func taskNewsDetail(id: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, newsDetail)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /*
     - Keyword
     - PageIndex
     */
    public func taskNewsList(keyword: String, listFilter: String, pageIndex: Int) -> Task<AnyObject> { // [Array<NSObject>]
        let stringPath = String(format:"%@%@", hostAPI, newsList)
        let stringFilter = ""
//        for item in listFilter[0] {
//            let filter = item as! FilterModel
//            stringFilter = String(format: "%d,%@", filter.id, stringFilter)
//        }
//        if(!stringFilter.isEmpty) {
//            stringFilter = String(stringFilter.dropLast())
//        }
        let parameters: [String : Any] = ["Keyword": keyword, "Category": listFilter, "Pageindex": pageIndex]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    ///
    public func taskMessageWebView(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, messageDetailWebview)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    public func taskMessageDetail(id: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, messageDetail)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    public func taskMessageListNew(keyword: String, pageIndex: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, messageListNewMessages)
        let parameters: [String : Any] = ["keyword": keyword, "pageIndex": pageIndex]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /*
     - Keyword
     - PageIndex
     */
    public func taskMessageListArchives(keyword: String, pageIndex: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, messageListArchives)
        let parameters: [String : Any] = ["keyword": keyword, "pageIndex": pageIndex]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /*
     - Keyword
     - PageIndex
    */
    public func taskMessageList(keyword: String, pageIndex: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, messageList)
        let parameters: [String : Any] = ["Keyword": keyword, "Pageindex": pageIndex]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /**
     - PhoneNo
     */
    public func taskMesageCount(phoneNo: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, messageCount)
        let parameters: [String : Any] = ["PhoneNo": phoneNo]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    func deincrementUnreadMessageCount() -> Int  {
        if(unreadMessageCount > 0) {
            unreadMessageCount = unreadMessageCount - 1
        }
        Broadcaster.notify(UnreadMessageCountDelegate.self) {
            $0.unreadMessageCountUpdate(unreadMessageCount: unreadMessageCount)
        }
        return unreadMessageCount
    }
    
    func incrementUnreadMessageCount() -> Int  {
        unreadMessageCount = unreadMessageCount + 1
        Broadcaster.notify(UnreadMessageCountDelegate.self) {
            $0.unreadMessageCountUpdate(unreadMessageCount: unreadMessageCount)
        }
        return unreadMessageCount
    }
    
    /**
     - Id (Message Id)
     */
    public func taskMessageMarkAsRead(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, messageMarkAsRead)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /**
     - Id (Message Id)
     */
    public func taskMessageDone(id: Int64) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, messageDone)
        let parameters: [String : Any] = ["Id": id]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /**
     - MessageModel
     {
     "ParentID": 1,   -- Khi reply, ParentID là ID of Message chọn reply.
     "Status": 16,
     "Message": "OK"
     }
     */
    
    public func taskMessageAdd(parentID: Int64, message: String, messageStatusType: MessageStatusType) -> Task<AnyObject> {
        let stringMessageModel = String(format: "{\"ParentID\":%d,\"Status\":%d,\"Message\":\"%@\"}", parentID, messageStatusType.rawValue, message)
        let stringPath = String(format:"%@%@", hostAPI, messageAdd)
        let parameters: [String : Any] = ["MessageModel": stringMessageModel]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    public func taskDeviceVerifyQR(qrCode: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, deviceVerifyQR)
        let parameters: [String : Any] = ["VerifyQR": qrCode]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .reponse)
        
    }
    
    
    public func taskOTP4VNC(otpCode: String) -> Task<AnyObject> {
        /*
        if let result = responseServiceModel.result as? JSON {
            let userModel = UserModel(json: result)
            self.userModel = userModel
            ApplicationData.sharedInstance.updateUserLogin(userModel: userModel)
            taskCompletionSource.set(result: userModel)
            
        }
        */
        let stringPath = String(format:"%@%@", hostAPI, OTP4VNC)
        
        let parameters: [String : Any] = ["OTP_CODE": otpCode]
        
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
        
    }
    
    /**
     "- PhoneNo (Số điện thoại người dùng nhập)
     - PushToken (Token push notification 1Signal)"
    */
    public func taskRegister4vnc(phoneNo: String, pushToken: String) -> Task<AnyObject> {
        /*
        if let result = responseServiceModel.result as? JSON {
            taskCompletionSource.set(result: result as AnyObject)
        }
        */
        let stringPath = String(format:"%@%@", hostAPI, register4vnc)
        
        let parameters: [String : Any] = ["PhoneNo": phoneNo, "PushToken": pushToken]
        
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    /**
     "- UserID
     - Token
     - From (yyyy-MM-dd)
     - To (yyyy-MM-dd)
     - Keyword
     - IsGetAll (0 or 1)
     - PageSize (default: 30 if empty)
     - PageIndex (default: 1 if empty)"
    */
    
    public func taskFlightInfoList(userID: Int = 0, fromDate: Date, toDate: Date, keyword: String = "", isGetAll: Bool = false, pageSize: Int = 20, pageIndex: Int = 1) -> Task<AnyObject> {
      
        let stringPath = String(format:"%@%@", hostAPI, flightInfoList)
        let parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "CrewID": self.userModel!.crewID, "Token": self.userModel!.token, "From": fromDate.dateTimeToString!, "To": toDate.dateTimeToString!, "Keyword": keyword, "IsGetAll": isGetAll ? "1" : "0", "PageSize": pageSize, "PageIndex": pageIndex] as [String : Any]
        print(parameters)
        if(dataRequestTaskFlightInfoList != nil) {
            dataRequestTaskFlightInfoList?.cancel()
        }
        return self.taskAlamorefireRequestFlightInfoList(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightInfoList)
            
            let parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "CrewID": self.userModel!.crewID, "Token": self.userModel!.token, "From": "", "To": toDate.dateTimeToString!, "Keyword": keyword, "IsGetAll": isGetAll ? "1" : "0", "PageSize": pageSize, "PageIndex": pageIndex] as [String : Any]
            if(dataRequestTaskFlightInfoList != nil) {
                dataRequestTaskFlightInfoList?.cancel()
            }
            dataRequestTaskFlightInfoList = Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    if(error.code == ErrorType.accessTokenInvalid.rawValue) {
                        
                    }
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        var listFlightInfo = Array<ScheduleFlightModel>()
                        for item in result.array! {
                            let scheduleFlyModel = ScheduleFlightModel(json: item)
                            listFlightInfo.append(scheduleFlyModel)
                        }
                        taskCompletionSource.set(result: listFlightInfo as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    /**
     "- UserID
     - Token
     - CrewID
     - Month (1 ~ 12)
     - PageSize (default: 30 if empty)
     - PageIndex (default: 1 if empty)"
    */
    public func taskFlightPersonalschedule(userID: Int = 0, month: Int, pageSize: Int = 20, pageIndex: Int = 1) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightPersonalschedule)
        
        let parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "CrewID": self.userModel!.crewID, "Token": self.userModel!.token, "Month": month, "PageSize": pageSize, "PageIndex": pageIndex] as [String : Any]
        
        #if DEBUG
        print("\(parameters) \(#function)")
        #endif
        
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightPersonalschedule)
            
            let parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "CrewID": self.userModel!.crewID, "Token": self.userModel!.token, "Month": month, "PageSize": pageSize, "PageIndex": pageIndex] as [String : Any]
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        var listFlightInfo = Array<ScheduleFlightModel>()
                        for item in result.array! {
                            let scheduleFlyModel = ScheduleFlightModel(json: item)
                            listFlightInfo.append(scheduleFlyModel)
                        }
                        taskCompletionSource.set(result: listFlightInfo as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    
    /**
     - UserID
     - Token
     - FlightReportInfo
     format
     {
     {"ID":0,"FlightID":1232,"CrewID":"1234","FullName":"Ng V A","MainPilot":"Pilot 1","SecondPilot":"Pilot 2","Emergency":1,"Category":"1,2,3,4,5","Content":"Test report."}
     }
     */
    
    public func taskFlightFinalReportUpdateInfo(finalReport: ReportModel) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightFinalReportUpdateInfo)
        
        var categorySting = ""
        for item in finalReport.categories {
            for sub in item.subCatetories {
                if(sub.isSelected) {
                    categorySting = String(format: "%d,%@", sub.subCategoryID, categorySting)
                }
                
            }
            
        }
        if(!categorySting.isEmpty) {
            categorySting = String(categorySting.dropLast())
        }
        if(categorySting.count == 0) {
            categorySting = "-1"
        }
        let stringFlightReportInfo = String(format: "{\"ID\":%d,\"FlightID\":%d,\"CrewID\":\"%d\",\"FullName\":\"%@\",\"MainPilot\":\"%@\",\"SecondPilot\":\"%@\",\"Emergency\":%d,\"Category\":\"%@\",\"Content\":\"%@\"}", finalReport.id, finalReport.flightID, self.userModel?.crewID ?? "", self.userModel?.name ?? "", finalReport.mainPilot, finalReport.secondPilot, finalReport.emergency.rawValue, categorySting, finalReport.content.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
        
        let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightReportInfo": stringFlightReportInfo, "AttachmentGroupID": finalReport.attachmentGroupID] as [String : Any]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: headers, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightFinalReportUpdateInfo)
            
            var categorySting = ""
            for item in finalReport.categories {
                for sub in item.subCatetories {
                    if(sub.isSelected) {
                        categorySting = String(format: "%d,%@", sub.subCategoryID, categorySting)
                    }
                    
                }
                
            }
            if(!categorySting.isEmpty) {
                categorySting = String(categorySting.characters.dropLast())
            }
            
            let stringFlightReportInfo = String(format: "{\"ID\":%d,\"FlightID\":%d,\"CrewID\":\"%d\",\"FullName\":\"%@\",\"MainPilot\":\"%@\",\"SecondPilot\":\"%@\",\"Emergency\":%d,\"Category\":\"%@\",\"Content\":\"%@\"}", finalReport.id, finalReport.flightID, self.userModel?.crewID ?? "", self.userModel?.name ?? "", finalReport.mainPilot, finalReport.secondPilot, finalReport.emergency.rawValue, categorySting, finalReport.content.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!)
            
            let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightReportInfo": stringFlightReportInfo, "AttachmentGroupID": finalReport.attachmentGroupID] as [String : Any]
            
            let headers = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(stringPath, method: .post, parameters: parameters, headers: headers).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        let reportModel = ReportModel(json: result)
                        taskCompletionSource.set(result: reportModel)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
         */
    }
    
    
    
    public func taskFlightFinalReportCategoryGetList() -> Task<AnyObject> {
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightFinalReportCategoryGetList)
            
            Alamofire.request(stringPath, method: .post, parameters: nil).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        var listCategoryModel = Array<CategoryModel>()
                        for item in result.array! {
                            let categoryModel = CategoryModel(json: item)
                            listCategoryModel.append(categoryModel)
                        }
                        taskCompletionSource.set(result: listCategoryModel as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        
    }
    /**
     
    */
    public func taskFlightPilotGetList(position: PositionType) -> Task<AnyObject> {
        
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            if(self.listMainPilot != nil) {
                if(position == .main) {
                    taskCompletionSource.set(result: self.listMainPilot as AnyObject)
                } else {
                    taskCompletionSource.set(result: self.listSecondPilot as AnyObject)
                }
            } else {
                let stringPath = String(format:"%@%@", hostAPI, flightPilotGetList)
                
                let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token] as [String : Any]
                
                Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { [weak self]  response in
                    let responseServiceModel = ResponseServiceModel(response: response)
                    if let error = responseServiceModel.result as? NSError {
                        taskCompletionSource.set(error: error)
                    } else {
                        self?.listMainPilot = Array<PilotModel>()
                        self?.listSecondPilot = Array<PilotModel>()
                        self?.listSecondPilot?.removeAll()
                        if let result = responseServiceModel.result as? JSON {
                            for item in result.array! {
                                let pilotModel = PilotModel(json: item)
                                if(pilotModel.position == .main) {
                                    self?.listMainPilot!.append(pilotModel)
                                } else {
                                    self?.listSecondPilot!.append(pilotModel)
                                }
                            }
                            if(position == .main) {
                                taskCompletionSource.set(result: self?.listMainPilot as AnyObject)
                            } else {
                                taskCompletionSource.set(result: self?.listSecondPilot as AnyObject)
                            }
                            
                        }
                        
                    }
                    taskCompletionSource.tryCancel()
                    
                }
            }
            
        }
        
        
        return taskCompletionSource.task
        
    }
    
    /**
     
    */
    
    public func taskFlightFinalReportGetInfo(flightId: Int, finalReportID: Int = 0) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightFinalReportGetInfo)
        let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightId": flightId, "CrewID": self.userModel!.crewID, "FinalReportID": finalReportID] as [String : Any]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: headers, reponseReturnType: .result)
        
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightFinalReportGetInfo)
            let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightId": flightId, "CrewID": self.userModel!.crewID, "FinalReportID": finalReportID] as [String : Any]
            
            let headers = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(stringPath, method: .post, parameters: parameters, headers: headers).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        let reportModel = ReportModel(json: result)
                        taskCompletionSource.set(result: reportModel)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserID
     - Token
     - FlightID (ex: 46888)
     - SourceCrewID (ex: 1694) - User đánh giá
     - SourceJob (ex: P) - User đánh giá
     - DestinationCrewID (ex: 0976) - User được đánh giá
     - DestinationJob (ex: S) - User được đánh giá
     - LessonID
     - Strength (Điểm mạnh)
     - Weakness (Điểm yếu)
     - AssessmentItems
    */
    public func taskFlightAssessmentUpdateInfo(flightId: Int, destinationCrewID: String, destinationJob: String, lessonID: Int, assessmentItems: Array<AssessmentItemModel>, strength: String, weaknes: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightAssessmentupdateinfo)
        var assessmentItemString = "["
        for item in assessmentItems {
            let parameters:[String:Any] = ["QuestionID": item.questionID, "Rate": item.rate]
            assessmentItemString.append(parameters.jsonString.replacingOccurrences(of: " ", with: ""))
            assessmentItemString.append(",")
        }
        assessmentItemString.append("]")
        
        let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightID": flightId, "SourceCrewID": self.userModel!.crewID, "SourceJob": self.userModel!.job, "DestinationCrewID": destinationCrewID, "DestinationJob": destinationJob, "LessonID": lessonID, "Strength": strength, "Weakness": weaknes, "AssessmentItems": assessmentItemString] as [String : Any]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: headers, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightAssessmentupdateinfo)
            
            var assessmentItemString = "["
            for item in assessmentItems {
                let parameters = ["QuestionID": item.questionID, "Score": item.score]
                assessmentItemString.append(parameters.jsonString.replacingOccurrences(of: " ", with: ""))
                assessmentItemString.append(",")
            }
            
            assessmentItemString.append("]")
            
            let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightID": flightId, "SourceCrewID": self.userModel!.crewID, "SourceJob": self.userModel!.job, "DestinationCrewID": destinationCrewID, "DestinationJob": destinationJob, "LessonID": lessonID, "Strength": strength, "Weakness": weaknes, "AssessmentItems": assessmentItemString] as [String : Any]
            
            let headers = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(stringPath, method: .post, parameters: parameters, headers: headers).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        let crewDutyModel = CrewDutyModel(json: result)
                        taskCompletionSource.set(result: crewDutyModel)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserName
     - Token
    */
    public func taskUserCheckaccesstoken(userName: String, token: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, userCheckaccesstoken)
        let parameters = ["UserName": userName, "Token": token] as [String : Any]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        
    }
    
    /**
     - UserID
     - Token
    */
    
    public func taskUserConfirminfo() -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, userConfirminfo)
        let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token] as [String : Any]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, userConfirminfo)
            
            let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token] as [String : Any]
            
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        taskCompletionSource.set(result: true as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        return taskCompletionSource.task
        */
    }
    
    
    
    
    /**
     - UserID
     - Token
     - FlightId
     - SourceCrewID (người ủy quyền)
     - DestinationCrewID (người được ủy quyền)
     */
    public func taskFlightCrewtaskauthorize(flightId: Int, sourceCrewID: String, destinationCrewID: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskauthorize)
        let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightId": flightId, "SourceCrewID": sourceCrewID, "DestinationCrewID": destinationCrewID] as [String : Any]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .message)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskauthorize)
            
            let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightId": flightId, "SourceCrewID": sourceCrewID, "DestinationCrewID": destinationCrewID] as [String : Any]
            
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        taskCompletionSource.set(result: (responseServiceModel.message ?? "") as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserID
     - Token
     - CrewID
     */
    public func taskUserGetinfobycrewid(crewId: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, userGetinfobycrewid)
        
        let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "CrewID": crewId] as [String : Any]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, userGetinfobycrewid)
            
            let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "CrewID": crewId] as [String : Any]
            
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        let userModel = UserModel(json: result)
                        taskCompletionSource.set(result: userModel)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserID
     - Token
     - FlightID (ex: 46888)
     - SourceCrewID (ex: 1694) - User đánh giá
     - SourceJob (ex: P) - User đánh giá
     - DestinationCrewID (ex: 0976) - User được đánh giá
     - DestinationJob (ex: S) - User được đánh giá
     
    */
    public func taskFlightAssessmentgetinfo(flightId: Int, destinationCrewID: String, destinationJob: String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightAssessmentgetinfo)
        let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightId": flightId, "SourceCrewID": self.userModel!.crewID, "SourceJob": self.userModel!.job, "DestinationCrewID": destinationCrewID, "DestinationJob": destinationJob] as [String : Any]
        
        let headers = [
            "Content-Type": "application/x-www-form-urlencoded"
        ]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: headers, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightAssessmentgetinfo)
            let parameters = ["UserID": self.userModel!.userId, "Token": self.userModel!.token, "FlightId": flightId, "SourceCrewID": self.userModel!.crewID, "SourceJob": self.userModel!.job, "DestinationCrewID": destinationCrewID, "DestinationJob": destinationJob] as [String : Any]
            
            let headers = [
                "Content-Type": "application/x-www-form-urlencoded"
            ]
            
            Alamofire.request(stringPath, method: .post, parameters: parameters, headers: headers).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        let assessmentModel = AssessmentModel(json: result)
                        taskCompletionSource.set(result: assessmentModel)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    
    
    /**
     - UserID
     - Token
     - FlightId (required)
     - IsGetTrip
     */
    public func taskGetFlightCrewtaskgetlistsupporttrip(userID: Int = 0, flightId: Int, isGetTrip: Bool = false) -> Task<AnyObject> {
        
        let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskgetlistsupporttrip)
        var parameters: [String : Any]
        if(isGetTrip == true) {
            parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "IsGetTrip": 1]
        } else {
            parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "IsGetTrip": 0]
        }
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskgetlistsupporttrip)
            var parameters: [String : Any]
            if(isGetTrip == true) {
                parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "IsGetTrip": 1]
            } else {
                parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "IsGetTrip": 0]
            }
            
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        let crewTaskModel = CrewTaskModel(json: result)
                        taskCompletionSource.set(result: crewTaskModel)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    
    /**
     - UserID
     - Token
     - FlightId (required)
     - TripId (optional)
    */
    /*
    public func taskGetFlightCrewtaskgetlist(userID: Int = 0, flightId: Int, tripId: Int = 0) -> Task<AnyObject> {
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskgetlist)
            var parameters: [String : Any]
            if(tripId == 0) {
                parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId]
            } else {
                parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "TripId": tripId]
            }
            
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        let crewTaskModel = CrewTaskModel(json: result)
                        taskCompletionSource.set(result: crewTaskModel)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        
    }
    */
    /**
     
    */
    public func taskFlightCrewtaskcategorylist(userID: Int = 0, categoryId: CategoryType = CategoryType.all) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskcategorylist)
        let parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "CategoryID": categoryId.rawValue] as [String : Any]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskcategorylist)
            let parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "CategoryID": categoryId.rawValue] as [String : Any]
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        var listSelectorTypeModel = Array<SelectorTypeModel>()
                        listSelectorTypeModel.append(SelectorTypeModel())
                        for item in result.array! {
                            let selectorTypeModel = SelectorTypeModel(json: item)
                            listSelectorTypeModel.append(selectorTypeModel)
                        }
                        taskCompletionSource.set(result: listSelectorTypeModel as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserID
     - Token
     - FlightId (required)
     - TripId (optional)
     - CrewDutyList (string)
     
     - CrewDutyList (Training: chọn CrewName (và hiển thị Name), khi save xuống thì save CrewID => @Huy: available?)
     Sample CrewDutyList:
     [{"CrewID": "3493","CrewFirstName": "LAN 28","CrewLastName": "TRAN THI NGOC","FO_Job": "Y","Ability": "","Job": "B","CA": "","ANN": "","Training": "1766","VIP": "","Other": "","DutyFree": 1},{"CrewID": "1766","CrewFirstName": "LIEN 17","CrewLastName": "NGUYEN THI BICH","FO_Job": "Y","Ability": "","Job": "Y","CA": "","ANN": "","Training": "","VIP": "","Other": "","DutyFree": 1}]
    */
    public func taskFlightCrewtaskUpdate(userID: Int = 0, flightId: Int, tripId: Int = 0, crewDutyList: Array<[String: String]>) -> Task<AnyObject> {
        var crewDutyListString = "["
        for item in crewDutyList {
            crewDutyListString.append(item.jsonString.replacingOccurrences(of: " ", with: ""))
            crewDutyListString.append(",")
        }
        crewDutyListString.append("]")
        
        let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskUpdate)
        var parameters: [String : Any]
        if(tripId == 0) {
            parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "CrewDutyList" : crewDutyListString]
        } else {
            parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "TripId": tripId, "CrewDutyList" : crewDutyListString]
        }
        
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .reponse)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        var crewDutyListString = "["
        for item in crewDutyList {
            crewDutyListString.append(item.jsonString.replacingOccurrences(of: " ", with: ""))
            crewDutyListString.append(",")
        }
        crewDutyListString.append("]")
        
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskUpdate)
            var parameters: [String : Any]
            if(tripId == 0) {
                parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "CrewDutyList" : crewDutyListString]
            } else {
                parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "TripId": tripId, "CrewDutyList" : crewDutyListString]
            }
      
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        let scheduleFlyModel = ScheduleFlightModel(json: result)
                        scheduleFlyModel.message = responseServiceModel.message ?? "Your changes have been saved.".localizedString()
                        taskCompletionSource.set(result: scheduleFlyModel as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    public func taskFlightCrewtaskupdatesupporttrip(userID: Int = 0, flightId: Int, isUpdateTrip: Int = 0, crewDutyList: Array<[String: String]>) -> Task<AnyObject> {
        var crewDutyListString = "["
        for item in crewDutyList {
            crewDutyListString.append(item.jsonString.replacingOccurrences(of: " ", with: ""))
            crewDutyListString.append(",")
        }
        crewDutyListString.append("]")
        let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskupdatesupporttrip)
        var parameters: [String : Any]
        if(isUpdateTrip == 0) {
            parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "CrewDutyList" : crewDutyListString]
        } else {
            parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "IsUpdateTrip": isUpdateTrip, "CrewDutyList" : crewDutyListString]
        }
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .reponse)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        var crewDutyListString = "["
        for item in crewDutyList {
            crewDutyListString.append(item.jsonString.replacingOccurrences(of: " ", with: ""))
            crewDutyListString.append(",")
        }
        crewDutyListString.append("]")
        
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightCrewtaskupdatesupporttrip)
            var parameters: [String : Any]
            if(isUpdateTrip == 0) {
                parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "CrewDutyList" : crewDutyListString]
            } else {
                parameters = ["UserID": userID == 0 ? self.userModel!.userId : userID, "Token": self.userModel!.token, "FlightId": flightId, "IsUpdateTrip": isUpdateTrip, "CrewDutyList" : crewDutyListString]
            }
            
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        let scheduleFlyModel = ScheduleFlightModel(json: result)
                        scheduleFlyModel.message = responseServiceModel.message ?? "Your changes have been saved.".localizedString()
                        taskCompletionSource.set(result: scheduleFlyModel as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        
        
        return taskCompletionSource.task
        */
    }
    
    
    public func taskBackgroundFilemanagerFlightreportuploadfiles(finalReportID: Int, flightId: Int, attachmentGroupID: Int, image: UIImage, imageName: String) -> Task<AnyObject> {
        
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, filemanagerFlightreportuploadfiles)
        
        if(self.userModel != nil) {
            NetworkBackgroundkManager.sharedManager.backgroundManager.upload(multipartFormData: {
                multipartFormData in
                
                multipartFormData.append(String(format: "%d", self.userModel!.userId).data(using: .utf8, allowLossyConversion: false)!, withName: "UserID")
                multipartFormData.append(self.userModel!.token.data(using: .utf8, allowLossyConversion: false)!, withName: "Token")
                multipartFormData.append(self.userModel!.crewID.data(using: .utf8, allowLossyConversion: false)!, withName: "CrewID")
                multipartFormData.append(String(format: "%d", flightId).data(using: .utf8, allowLossyConversion: false)!, withName: "FlightID")
                multipartFormData.append(String(format: "%d", finalReportID).data(using: .utf8, allowLossyConversion: false)!, withName: "FinalReportID")
                multipartFormData.append(String(format: "%d", attachmentGroupID).data(using: .utf8, allowLossyConversion: false)!, withName: "AttachmentGroupID")
                multipartFormData.append(image.jpegData(compressionQuality: 1.0)!, withName: "file", fileName: imageName, mimeType: "image/jpeg")
                
                
                
            }, to: stringPath, headers: self.serviceHeaders) {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _ ):
                    upload.validate().LogRequest().responseJSON(completionHandler: { response in
                        let responseServiceModel = ResponseServiceModel(response: response)
                        if let error = responseServiceModel.result as? NSError {
                            taskCompletionSource.set(error: error)
                        } else {
                            if let result = responseServiceModel.result as? JSON {
                                if let array = result["AttachmentList"].array {
                                    if(array.count > 0) {
                                        let attachmentModel = AttachmentModel(json: array[0])
                                        attachmentModel.attachmentID = result["AttachmentID"].int ?? 0
                                        taskCompletionSource.set(result: attachmentModel as AnyObject)
                                    }
                                }
                                
                            }
                            
                        }
                        taskCompletionSource.tryCancel()
                    })
                    
                case .failure(let encodingError):
                    taskCompletionSource.set(error: encodingError)
                    print(encodingError)
                }
            }
        }
        
        return taskCompletionSource.task
        
    }
    
    /**
     - UserID
     - Token
     - CrewID
     - FlightID
     - AttachmentGroupID
     - FinalReportID
     - File 1
     - File 2
     - ...
     - File n
    */
    public func taskFilemanagerFlightreportuploadfiles(finalReportID: Int, flightId: Int, attachmentGroupID: Int, image: UIImage, imageName: String) -> Task<AnyObject> {
        
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, filemanagerFlightreportuploadfiles)

        if(self.userModel != nil) {
            let token = self.userModel!.token
            let crewID = self.userModel!.crewID
            let userId = self.userModel!.userId
            Alamofire.upload(multipartFormData: {
                multipartFormData in
                
                multipartFormData.append(String(format: "%d", userId).data(using: .utf8, allowLossyConversion: false)!, withName: "UserID")
                multipartFormData.append(token.data(using: .utf8, allowLossyConversion: false)!, withName: "Token")
                multipartFormData.append(crewID.data(using: .utf8, allowLossyConversion: false)!, withName: "CrewID")
                multipartFormData.append(String(format: "%d", flightId).data(using: .utf8, allowLossyConversion: false)!, withName: "FlightID")
                multipartFormData.append(String(format: "%d", finalReportID).data(using: .utf8, allowLossyConversion: false)!, withName: "FinalReportID")
                multipartFormData.append(String(format: "%d", attachmentGroupID).data(using: .utf8, allowLossyConversion: false)!, withName: "AttachmentGroupID")
                multipartFormData.append(image.jpegData(compressionQuality: 1.0)!, withName: "file", fileName: imageName, mimeType: "image/jpeg")
                
                
            }, to: stringPath, headers: self.serviceHeaders) {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _ ):
                    upload.validate().LogRequest().responseJSON(completionHandler: { response in
                        let responseServiceModel = ResponseServiceModel(response: response)
                        if let error = responseServiceModel.result as? NSError {
                            taskCompletionSource.set(error: error)
                        } else {
                            if let result = responseServiceModel.result as? JSON {
                                if let array = result["AttachmentList"].array {
                                    if(array.count > 0) {
                                        let attachmentModel = AttachmentModel(json: array[0])
                                        attachmentModel.attachmentID = result["AttachmentID"].int ?? 0
                                        taskCompletionSource.set(result: attachmentModel as AnyObject)
                                    }
                                }
                                
                            }
                            
                        }
                        taskCompletionSource.tryCancel()
                    })
                    
                case .failure(let encodingError):
                    taskCompletionSource.set(error: encodingError)
                    print(encodingError)
                }
            }
        }
        
        return taskCompletionSource.task
        
    }
    
    /**
     - UserID
     - Token
     - FinalReportID
     */
    func taskFlightFinalReportDelete(reportID: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightFinalReportDelete)
        var parameters: [String : Any]
        parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "FinalReportID": reportID]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .message)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, flightFinalReportDelete)
            var parameters: [String : Any]
            parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "FinalReportID": reportID]
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if(responseServiceModel.status == 0) {
                        taskCompletionSource.set(result: responseServiceModel.message as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        return taskCompletionSource.task
         */
    }
    
    func taskFilemanagerDeletefiles(fileId: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, filemanagerDeletefiles)
        var parameters: [String : Any]
        parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "FileIds": fileId]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        if(self.userModel != nil) {
            let stringPath = String(format:"%@%@", hostAPI, filemanagerDeletefiles)
            var parameters: [String : Any]
            parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "FileIds": fileId]
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        taskCompletionSource.set(result: true as AnyObject)
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        return taskCompletionSource.task
         */
    }
    
    
    
    public func taskFlightFinalReportGetAll(date: Date, keyword: String = "", pageIndex: Int = 1) -> Task<AnyObject> {
        /*
         Date //(ddMMMyyyy) 12MAR2018
         Keyword
         PageIndex (1,2,3…)

        */
        let stringPath = String(format:"%@%@", hostAPI, flightFinalReportGetAll)
        var parameters: [String : Any]
        parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "CrewID": self.userModel?.crewID ?? "", "Date": date.dateTimeTodMMM, "Keyword": keyword, "PageIndex": pageIndex]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        
    }
    
    //flightFinalreportgetlist
    /**
     "- UserID
     - Token
     - FlightID
     - CrewID"
     */
    public func taskFlightFinalreportgetlist(flightId: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightFinalreportgetlist)
        var parameters: [String : Any]
        parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "FlightId": flightId, "CrewID": self.userModel?.crewID ?? ""]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, flightFinalreportgetlist)
        if(self.userModel != nil) {
            var parameters: [String : Any]
            parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "FlightId": flightId, "CrewID": self.userModel?.crewID ?? ""]
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    
                    if let result = responseServiceModel.result as? JSON {
                        if let array = result.array {
                            var arrayReportModel = Array<ReportModel>()
                            for item in array {
                                let reportModel = ReportModel(json: item)
                                arrayReportModel.append(reportModel)
                            }
                            taskCompletionSource.set(result: arrayReportModel as AnyObject)
                        }
                        
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserID
     - Token
     - FlightID (ex: 46888)
    */
    public func taskFlightAssessmentcrewdutylist(flightId: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightAssessmentcrewdutylist)
        var parameters: [String : Any]
        parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "FlightId": flightId]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, flightAssessmentcrewdutylist)
        if(self.userModel != nil) {
            var parameters: [String : Any]
            parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "FlightId": flightId]
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    
                    if let result = responseServiceModel.result as? JSON {
                        if let array = result.array {
                            var arrayCrewDutyModel = Array<CrewDutyModel>()
                            for item in array {
                                let crewDutyModel = CrewDutyModel(json: item)
                                arrayCrewDutyModel.append(crewDutyModel)
                            }
                            taskCompletionSource.set(result: arrayCrewDutyModel as AnyObject)
                        }
                        
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserID
     - Token
     - IsAll (optional): 0 | 1
     - Filter (optional): 1,2,3...
     - PageSize (optional)
     - PageIndex (optional)
     */
    public func taskGetSupportList(isAll: Int = 0, keyword: String = "", filter: Int = 0, pageSize: Int = 20, pageIndex: Int = 1) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, supportList)
        var parameters: [String : Any]
        parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "IsAll": isAll, "Filter": filter, "PageSize": pageSize, "PageIndex": pageIndex]
        if(keyword != "") {
            parameters["Keyword"] = keyword
        }
        
        parameters["IsAll"] = (self.listFilterSelected[0][0] as! FilterModel).id
        
        var stringFilter = ""
        for item in self.listFilterSelected[1] {
            let filter = item as! FilterModel
            stringFilter = String(format: "%d,%@", filter.id, stringFilter)
        }
        if(!stringFilter.isEmpty) {
            stringFilter = String(stringFilter.dropLast())
            parameters["Filter"] = stringFilter
        }
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, supportList)
        if(self.userModel != nil) {
            var parameters: [String : Any]
            parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "IsAll": isAll, "Filter": filter, "PageSize": pageSize, "PageIndex": pageIndex]
            if(keyword != "") {
                parameters["Keyword"] = keyword
            }
            
            parameters["IsAll"] = (self.listFilterSelected[0][0] as! FilterModel).id
            
            var stringFilter = ""
            for item in self.listFilterSelected[1] {
                let filter = item as! FilterModel
                stringFilter = String(format: "%d,%@", filter.id, stringFilter)
            }
            if(!stringFilter.isEmpty) {
                stringFilter = String(stringFilter.characters.dropLast())
                parameters["Filter"] = stringFilter
            }
            
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    
                    if let result = responseServiceModel.result as? JSON {
                        if let array = result.array {
                            var arraySupport = Array<SupportModel>()
                            for item in array {
                                let supportModel = SupportModel(json: item)
                                arraySupport.append(supportModel)
                            }
                            taskCompletionSource.set(result: arraySupport as AnyObject)
                        }
                        
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserID
     - Token
     - SupportID (required)
    */
    
    public func taskGetSupportDetail(supportID: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, supportDetail)
        var parameters: [String : Any]
        parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "SupportID": supportID]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, supportDetail)
        if(self.userModel != nil) {
            var parameters: [String : Any]
            parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "SupportID": supportID]
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    if let result = responseServiceModel.result as? JSON {
                        if let array = result.array {
                            var arraySupport = Array<SupportModel>()
                            for item in array {
                                let supportModel = SupportModel(json: item)
                                arraySupport.append(supportModel)
                            }
                            taskCompletionSource.set(result: arraySupport as AnyObject)
                        }
                        
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserID
     - Token
     - Action (required)
     - SupportID (optional)
     - Message (optional)
     - Assignee (optional) (set CrewID - select from list user same as Task for Boss)
    */
    
    public func taskSupportUpdate(action: ActionUpdateSupportType, supportID: Int, message: String = "") -> Task<AnyObject> {
        
        let stringPath = String(format:"%@%@", hostAPI, supportUpdate)
        var parameters: [String : Any]
        parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "SupportID": supportID, "Action": action.rawValue, "Message": message.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
        /*
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, supportUpdate)
        if(self.userModel != nil) {
            var parameters: [String : Any]
            parameters = ["UserID": self.userModel?.userId ?? 0, "Token": self.userModel?.token ?? "", "SupportID": supportID, "Action": action.rawValue, "Message": message.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!]
            Alamofire.request(stringPath, method: .post, parameters: parameters).validate().LogRequest().responseJSON { response in
                let responseServiceModel = ResponseServiceModel(response: response)
                if let error = responseServiceModel.result as? NSError {
                    taskCompletionSource.set(error: error)
                } else {
                    
                    if let result = responseServiceModel.result as? JSON {
                        let supportModel = SupportModel(json: result)
                        taskCompletionSource.set(result: supportModel as AnyObject)
                        
                    }
                    
                }
                taskCompletionSource.tryCancel()
                
            }
        }
        return taskCompletionSource.task
        */
    }
    
    /**
     - UserID
     - Token
     - SupportID
     - File_Attach
    */
    public func taskFilemanagerSupportuploadfiles(supportID: Int, fileName: String, image: UIImage) -> Task<AnyObject> {

        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        let stringPath = String(format:"%@%@", hostAPI, filemanagerSupportuploadfiles)
        
        if(self.userModel != nil) {
            let userId = UserDefaults.standard.integer(forKey: "userId")
            let token = UserDefaults.standard.string(forKey: "token") ?? ""
            Alamofire.upload(multipartFormData: {
                multipartFormData in
                
                multipartFormData.append(String(format: "%d", userId).data(using: .utf8, allowLossyConversion: false)!, withName: "UserID")
                multipartFormData.append(token.data(using: .utf8, allowLossyConversion: false)!, withName: "Token")
                multipartFormData.append(String(format: "%d", supportID).data(using: .utf8, allowLossyConversion: false)!, withName: "SupportID")
                
                
                multipartFormData.append(image.jpegData(compressionQuality: 1.0)!, withName: "File_Attach", fileName: fileName, mimeType: "image/jpeg")
                
                
            }, to: stringPath, headers: self.serviceHeaders) {
                encodingResult in
                switch encodingResult {
                case .success(let upload, _, _ ):
                    upload.validate().LogRequest().responseJSON(completionHandler: { response in
                        let responseServiceModel = ResponseServiceModel(response: response)
                        if let error = responseServiceModel.result as? NSError {
                            taskCompletionSource.set(error: error)
                        } else {
                            if let result = responseServiceModel.result as? JSON {
                                let supportModel = SupportModel(json: result)
                                taskCompletionSource.set(result: supportModel as AnyObject)
                                
                            }
                            
                        }
                        taskCompletionSource.tryCancel()
                    })
                    
                case .failure(let encodingError):
                    taskCompletionSource.set(error: encodingError)
                    print(encodingError)
                }
            }
        }
        
        return taskCompletionSource.task
        
    }
    
    
    func taskAlamorefireRequest(stringPath: String, method: HTTPMethod, parameters: Parameters?, headers: HTTPHeaders? = nil, reponseReturnType: ReponseReturnType) -> Task<AnyObject> {
        #if DEBUG
        print("\(parameters) \(#function)")
        #endif
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        var parameters = parameters
        let token = UserDefaults.standard.string(forKey: "token") ?? ""
        self.serviceHeaders["X_TOKEN"] = token
        self.serviceHeaders["X_LOCATION"] = self.locationLatLong
        Alamofire.request(stringPath, method: method, parameters: parameters, headers: self.serviceHeaders).validate().LogRequest().responseJSON { response in
            let responseServiceModel = ResponseServiceModel(response: response)
            if let error = responseServiceModel.result as? NSError {
                if(responseServiceModel.status == ErrorType.accessTokenInvalid.rawValue) {
                    self.taskShowFormRegisterDevice().continueOnSuccessWith(continuation: { task in
                        parameters?["Token"] = token
                        parameters?["CrewID"] = UserDefaults.standard.integer(forKey: "crewID")
                        parameters?["UserID"] = UserDefaults.standard.integer(forKey: "userId")
                        let token = UserDefaults.standard.string(forKey: "token") ?? ""
                        self.serviceHeaders["X_TOKEN"] = token
                        self.taskAlamorefireRequest(stringPath: stringPath, method: method, parameters: parameters, reponseReturnType: reponseReturnType).continueOnSuccessWith(continuation: {
                            task in
                            taskCompletionSource.set(result: task)
                        })
                    }).continueOnErrorWith(continuation: { error in
                        taskCompletionSource.set(error: error)
                    })
                    return
                } else {
                    taskCompletionSource.set(error: error)
                }
            } else {
                if let result = responseServiceModel.result as? JSON {
                    if(reponseReturnType == ReponseReturnType.result) {
                        taskCompletionSource.set(result: result as AnyObject)
                    } else if(reponseReturnType == ReponseReturnType.message) {
                        let message = responseServiceModel.message ?? ""
                        taskCompletionSource.set(result: message as AnyObject)
                    } else if(reponseReturnType == ReponseReturnType.status) {
                        taskCompletionSource.set(result: responseServiceModel.status as AnyObject)
                    } else if(reponseReturnType == ReponseReturnType.reponse) {
                        taskCompletionSource.set(result: responseServiceModel as AnyObject)
                    }
                }
                
            }
            taskCompletionSource.tryCancel()
            
        }
        return taskCompletionSource.task
    }
    
    func taskAlamorefireRequestFlightInfoList(stringPath: String, method: HTTPMethod, parameters: Parameters?, headers: HTTPHeaders? = nil, reponseReturnType: ReponseReturnType) -> Task<AnyObject> {
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        var parameters = parameters
        let token = UserDefaults.standard.string(forKey: "token") ?? ""
        self.serviceHeaders["X_TOKEN"] = token
        self.serviceHeaders["X_LOCATION"] = self.locationLatLong
        dataRequestTaskFlightInfoList = Alamofire.request(stringPath, method: method, parameters: parameters, headers: self.serviceHeaders).validate().LogRequest().responseJSON { response in
            let responseServiceModel = ResponseServiceModel(response: response)
            if let error = responseServiceModel.result as? NSError {
                if(responseServiceModel.status == ErrorType.accessTokenInvalid.rawValue) {
                    self.taskShowFormRegisterDevice().continueOnSuccessWith(continuation: { task in
                        parameters?["Token"] = token
                        parameters?["CrewID"] = UserDefaults.standard.integer(forKey: "crewID")
                        parameters?["UserID"] = UserDefaults.standard.integer(forKey: "userId")
                        let token = UserDefaults.standard.string(forKey: "token") ?? ""
                        self.serviceHeaders["X_TOKEN"] = token
                        self.taskAlamorefireRequestFlightInfoList(stringPath: stringPath, method: method, parameters: parameters, reponseReturnType: reponseReturnType).continueOnSuccessWith(continuation: {
                            task in
                            taskCompletionSource.set(result: task)
                        })
                    }).continueOnErrorWith(continuation: { error in
                        taskCompletionSource.set(error: error)
                    })
                    return
                } else {
                    taskCompletionSource.set(error: error)
                }
            } else {
                if let result = responseServiceModel.result as? JSON {
                    if(reponseReturnType == ReponseReturnType.result) {
                        taskCompletionSource.set(result: result as AnyObject)
                    } else if(reponseReturnType == ReponseReturnType.message) {
                        let message = responseServiceModel.message ?? ""
                        taskCompletionSource.set(result: message as AnyObject)
                    } else if(reponseReturnType == ReponseReturnType.status) {
                        taskCompletionSource.set(result: responseServiceModel.status as AnyObject)
                    } else if(reponseReturnType == ReponseReturnType.status) {
                        taskCompletionSource.set(result: responseServiceModel as AnyObject)
                    }
                }
                
            }
            taskCompletionSource.tryCancel()
            
        }
        return taskCompletionSource.task
    }
    
    
    func taskGetLocation() -> Task<AnyObject> {
        let taskCompletionSource = TaskCompletionSource<AnyObject>()
        
        _ = INTULocationManager.sharedInstance().requestLocation(withDesiredAccuracy: .room, timeout: 1.5, delayUntilAuthorized: true) { (currentLocation, achievedAccuracy, status) in
            if (status == .success || status == .timedOut) {
                if(currentLocation != nil){
                    taskCompletionSource.set(result: currentLocation!)
                } else {
                    let serviceError = ServiceErrorModel.serviceError(messageError: "You may wish to inspect achievedAccuracy here to see if it is acceptable, if you plan to use currentLocation", domain: "TimedOut")
                    taskCompletionSource.set(error: serviceError!)
                    
                }
                
            } else if (status == .servicesDenied){
                
                let serviceError = ServiceErrorModel.serviceError(messageError: NSLocalizedString("You has explicitly denied this app permission to access location services.", comment: ""), domain: NSLocalizedString("taskGetAddress", comment: ""))
                taskCompletionSource.set(error: serviceError!)
            } else if (status == .servicesDisabled){
                /** User has turned off location services device-wide (for all apps) from the system Settings app. */
                let serviceError = ServiceErrorModel.serviceError(messageError: NSLocalizedString("You has turned off location services device-wide (for all apps) from the system Settings app.", comment: ""), domain: NSLocalizedString("taskGetAddress", comment: ""))
                taskCompletionSource.set(error: serviceError!)
            }
            else {
                // An error occurred
                let serviceError = ServiceErrorModel.serviceError(messageError: " An error occurred", domain: NSLocalizedString("taskGetAddress", comment: ""))
                taskCompletionSource.set(error: serviceError!)
            }
        }
        
        return taskCompletionSource.task
    }
    
    //New Changes - Harry Nhan
    func taskCavaGetSectionList(id: Int, date: Date, keyword: String, pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Id": id, "Date": date.dateTimeTodMMM, "Keyword": keyword, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, cavaGetSectionList)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    func taskCavaGetSectionDetails(menuId: Int, id: Int = -1, date: Date = Date(), keyword: String = "", pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["MenuId": menuId, "Id": id, "Date": date.dateTimeTodMMM, "Keyword": keyword, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, cavaGetSectionDetails)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    //
    public func taskFormDoAction(id: Int, status: Int, remark: String, approvalFromDate: Date, approvalToDate: Date) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, formDoAction)
        let parameters: [String : Any] = ["Id": id, "Status": status, "Remark": remark, "ApprovalFromDate": approvalFromDate.dateTimeToYYYYMMdd, "ApprovalToDate": approvalToDate.dateTimeToYYYYMMdd]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    /*
     - Keyword
     - Filter
     - PageIndex
     */
    public func taskWeeklyReportList(keyword: String, stringFilter: String, pageIndex: Int) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, weeklyReportList)
        let stringFilter = ""
        //        for item in listFilter[0] {
        //            let filter = item as! FilterModel
        //            stringFilter = String(format: "%d,%@", filter.id, stringFilter)
        //        }
        //        if(!stringFilter.isEmpty) {
        //            stringFilter = String(stringFilter.dropLast())
        //        }
        let parameters: [String : Any] = ["Keyword": keyword, "Filter": stringFilter, "Pageindex": pageIndex]
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
        
    }
    
    public func taskWeeklyReportDetail(id: Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["ID": id]
        let stringPath = String(format:"%@%@", hostAPI, weeklyReportDetail)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func taskWeeklyReportGetDepartments() -> Task<AnyObject> {
        let parameters: [String : Any] = [:]
        let stringPath = String(format:"%@%@", hostAPI, weeklyReportGetDepartments)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }

    func taskGetFormHistory(cid: String, keyword: String = "", pageIndex: Int = 1) -> Task<AnyObject> {
        let parameters: [String : Any] = ["CID": cid, "Keyword": keyword, "PageIndex": pageIndex]
        let stringPath = String(format:"%@%@", hostAPI, formHistory)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func taskNewsGetCategory() -> Task<AnyObject> {
        let parameters: [String : Any] = [:]
        let stringPath = String(format:"%@%@", hostAPI, newsGetCategory)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func taskFormGetFilter() -> Task<AnyObject> {
        let parameters: [String : Any] = [:]
        let stringPath = String(format:"%@%@", hostAPI, formGetFilterList)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    // Dai
    //
    public func taskGetPassengerList(flightId:Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["FlightId":flightId]
        let stringPath = String(format:"%@%@", hostAPI, getPassengerList)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    
    /// synchronize with third services
    /// - Parameter flightId: current schedule flight
    /// - Returns: synchronize with third services ex: Lotus
    public func taskSyncFlightInfo(flightId:Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["FlightId":flightId]
        let stringPath = String(format:"%@%@", hostAPI, flightReloadInfo)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func taskGetFlightScheduleDetail(flightId: Int, userId:Int? = nil) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, flightInfoDetail)
        
        let parameters = ["UserID": (userId != nil) ? userId! : self.userModel!.userId, "CrewID": self.userModel!.crewID, "Token": self.userModel!.token, "flightId":flightId] as [String : Any]
        
        #if DEBUG
        print("\(parameters) \(#function)")
        #endif
        
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, reponseReturnType: .result)
    }
    
    public func taskSaveBriefing(flightId:Int, content:String, fileName:String = "filenam.jpg", base64Image:String = "", replyId:Int) -> Task<AnyObject> {
        
        var dict:[String:Any] = ["Content":content,"ReplyId":replyId]
        
        if base64Image.count > 0 {
            dict["OriginalFileName"] = fileName
            dict["ContentBase64"] = base64Image
        }
        
        let parameters: [String : Any] = ["FlightID":flightId,"Data":toStringJSon(dict: dict)]
        
        let stringPath = String(format:"%@%@", hostAPI, flightSaveBriefing)
        #if DEBUG
        print("\(parameters) \(#function)")
        #endif
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    //
    public func taskFormGetTermsAndConditions() -> Task<AnyObject> {
        let parameters: [String : Any] = [:]
        let stringPath = String(format:"%@%@", hostAPI, formGetTermsAndConditions)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func taskSendEmailWeeklyReport(emails:String, ID:Int) -> Task<AnyObject> {
        let parameters: [String : Any] = ["Emails":emails,"ID":ID]
        let stringPath = String(format:"%@%@", hostAPI, sendWeeklyReportMail)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func taskDeleteBriefing(flightId:Int,id:Int) -> Task<AnyObject> {
        
        let parameters: [String : Any] = ["FlightID":flightId,"Id":id]
        
        let stringPath = String(format:"%@%@", hostAPI, flightDeleteBriefing)
        #if DEBUG
        print("\(parameters) \(#function)")
        #endif
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    /// report lotus shop state
    /// - Parameters:
    ///   - flightId: flight id
    ///   - selectedItemId: lotus shop report item id
    ///   - lotusShopKey: lotusShopKey
    ///   - remark: note nếu có
    ///   - attachments: ["OriginalFileName":"<filename>.ext","ContentBase64":"<base64String>"]
    /// - Returns: Task
    public func taskLotusShopSubmitReport(flightId:Int,
                                          selectedItemId:String,
                                          lotusShopKey:String,
                                          remark:String,
                                          deletedAttachment:[Int],
                                          attachments:[DynamicModelString]) -> Task<AnyObject> {
        
        let parameters: [String : Any] = ["FlightID": flightId,
                                          "LotusShopKey": lotusShopKey,
                                          "SelectedItemId": selectedItemId,
                                          "Remark": remark,
                                          "DeletedAttachment":deletedAttachment,
                                          "Attachment":toStringArrayJSon(array: attachments.compactMap({$0.dictionary}))]
        
        let stringPath = String(format:"%@%@", hostAPI, lotusShopSubmitReport)
        #if DEBUG
        print("\(parameters) \(#function)")
        #endif
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func taskLotusShopSubmitReportSync(parameters: [String : Any]) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, lotusShopSubmitReport)
        #if DEBUG
        print("\(parameters) \(#function)")
        #endif
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func getSurveyPage(id:String) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, getSurveyPage)
        
        let parameters: [String : Any] = ["Tid":id]
        
        #if DEBUG
        print("\(parameters) \(#function)")
        #endif
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func updateSurveyPage(surveyPage:SurveyPageModel) -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, updateSurveyPage)
        
        let parameters: [String : Any] = surveyPage.toDict()
        
        #if DEBUG
        print("\(parameters) \(#function)")
        #endif
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: parameters, headers: serviceHeaders, reponseReturnType: .result)
    }
    
    public func taskCheckingWaitingApproval() -> Task<AnyObject> {
        let stringPath = String(format:"%@%@", hostAPI, checkAwaitingApproval)
        return self.taskAlamorefireRequest(stringPath: stringPath, method: .post, parameters: nil, headers: serviceHeaders, reponseReturnType: .result)
    }
}

// MARK: -  support
extension ServiceData {
    func toStringJSon(dict:[String:Any]) -> String {
        if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: dict,
            options: .prettyPrinted
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.utf8) {
            return theJSONText
        }
        return ""
    }
    
    func toStringArrayJSon(array:[[String:Any]]) -> String {
        if let theJSONData = try?  JSONSerialization.data(
            withJSONObject: array,
            options: .fragmentsAllowed
            ),
            let theJSONText = String(data: theJSONData,
                                     encoding: String.Encoding.utf8) {
            return theJSONText
        }
        return ""
    }
    
    func toArrayJSon(string:String?) -> [[String:Any]]? {
        guard let string = string else {
             return nil
        }
        
        if let data = string.data(using: .utf8),
            let object = try? JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed) as? [[String:Any]] {
            return object
        }
        return nil
    }
    
    func toJSon(string:String?) -> [String:Any]? {
        guard let string = string else {
             return nil
        }
        
        if let data = string.data(using: .utf8),
            let object = try? JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed) as? [String:Any] {
            return object
        }
        return nil
    }
}
