//
//  AppDelegate.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/27/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import OneSignal
import SwiftyJSON

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
#if DEBUG
do {
    try FileManager.default.removeItem(atPath: NSHomeDirectory()+"/Library/SplashBoard")
} catch {
    #if DEBUG
    print("Failed to delete launch screen cache: \(error)")
    #endif
}
#endif
        
        
        //coredata
        CoreData.sharedInstance.applicationDocumentsDirectory()
        
        // check internet
        NetworkObserver.shared.startNotifierNetwork()
        
        // Override point for customization after application launch.
        //Bundle.setLanguage("vi")
//        var storyboard: UIStoryboard
//        if(UIDevice.current.userInterfaceIdiom == .phone) {
//            storyboard = UIStoryboard.init(name: "Main_iphone", bundle: nil)
//        } else {
//            storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//        }
        
        _ = UIStoryboard.init(name: "Main_iphone", bundle: nil)
        self.setupOnsignal(launchOptions: launchOptions)
        
        /*
        let viewController = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        self.window!.rootViewController = viewController
        self.window!.makeKeyAndVisible()
        */
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        // This is the workaround for Xcode 11.2
        // UITextViewWorkaround.unique.executeWorkaround()
        
        return true
    }

    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        NetworkBackgroundkManager.sharedManager.backgroundCompletionHandler = completionHandler
        print("NetworkBackgroundkManager.sharedManager.backgroundCompletionHandler")
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        NetworkObserver.shared.stopNotifierNetwork()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        NetworkObserver.shared.startNotifierNetwork()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

// MARK: - OSPermissionObserver, OSSubscriptionObserver
extension AppDelegate: OSPermissionObserver, OSSubscriptionObserver {
    
    
    func setupOnsignal(launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        // For debugging
        //OneSignal.setLogLevel(.LL_VERBOSE, visualLevel: .LL_NONE)
        OneSignal.setSubscription(true)
        let notificationReceivedBlock: OSHandleNotificationReceivedBlock = { notification in
            UIApplication.shared.applicationIconBadgeNumber = 0
            if let additionalData = notification?.payload?.additionalData {
                OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
                let json = JSON(additionalData)
                let item = NotifyModel(json: json)
                if(item.ui == 1) {
                    Broadcaster.notify(PushNotifyDelegate.self) {
                        $0.notifyReceiver(item: item)
                    }
                }
                
                
            }
            /*
             print("Received Notification: \(notification!.payload.notificationID)")
             print("launchURL = \(notification?.payload.launchURL)")
             print("content_available = \(notification?.payload.contentAvailable)")
             */
            /*
             if let additionalData = notification?.payload.additionalData {
             if let roomId = additionalData["roomId"] as? String {
             Broadcaster.notify(ChatNotificationOpenedDelegate.self) {
             $0.chatNotificationOpened(roomId: roomId)
             }
             
             }
             }
             */
        }
        
        let notificationOpenedBlock: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            /*
             let payload: OSNotificationPayload? = result?.notification.payload
             print("Message = \(payload!.body)")
             print("badge number = \(payload?.badge)")
             print("notification sound = \(payload?.sound)")
             */
            if let additionalData = result?.notification.payload?.additionalData {
                let json = JSON(additionalData)
                let item = NotifyModel(json: json)
                MessageListViewController.notifyModelFromNotify = item
                Broadcaster.notify(PushNotifyDelegate.self) {
                    $0.notifyOpened(item: item)
                }
                
                
            }
        }
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppAlerts: true, kOSSettingsKeyInAppLaunchURL: true, ]
        
        OneSignal.initWithLaunchOptions(launchOptions, appId: "8737cece-332c-4001-910a-d571ecedc725", handleNotificationReceived: notificationReceivedBlock, handleNotificationAction: notificationOpenedBlock, settings: onesignalInitSettings)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification
        
        // Add your AppDelegate as an obsserver
        OneSignal.add(self as OSPermissionObserver)
        OneSignal.add(self as OSSubscriptionObserver)
        self.onRegisterForPushNotifications()
    }
    
    // Add this new method
    func onOSPermissionChanged(_ stateChanges: OSPermissionStateChanges!) {
        
        // Example of detecting answering the permission prompt
        if stateChanges.from.status == OSNotificationPermission.notDetermined {
            if stateChanges.to.status == OSNotificationPermission.authorized {
                print("Thanks for accepting notifications!")
                self.onRegisterForPushNotifications()
            } else if stateChanges.to.status == OSNotificationPermission.denied {
                print("Notifications not accepted. You can turn them on later under your iOS settings.")
            }
        }
        // prints out all properties
        print("PermissionStateChanges: \n\(stateChanges)")
    }
    
    // Output:
    
    // TODO: update docs to change method name
    // Add this new method
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
        if !stateChanges.from.subscribed && stateChanges.to.subscribed {
            print("Subscribed for OneSignal push notifications!")
            self.onRegisterForPushNotifications()
        }
        print("SubscriptionStateChange: \n\(stateChanges)")
    }
    
    // Output:
    
    
    func onRegisterForPushNotifications() {
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        let hasPrompted = status.permissionStatus.hasPrompted
        if hasPrompted == false {
            // Call when you want to prompt the user to accept push notifications.
            // Only call once and only if you set kOSSettingsKeyAutoPrompt in AppDelegate to false.
            OneSignal.promptForPushNotifications(userResponse: { accepted in
                if accepted == true {
                    print("User accepted notifications: \(accepted)")
                } else {
                    print("User accepted notifications: \(accepted)")
                }
            })
        } else {
            if(status.permissionStatus.status == .authorized) {
                
                if let onesignalUserId = status.subscriptionStatus.userId {
                    
                    ServiceData.sharedInstance.oneSignalUserId = onesignalUserId
                }
                
            } else {
                displaySettingsNotification()
            }
        }
    }
    
    func displaySettingsNotification() {
        
    }
    
    
}

