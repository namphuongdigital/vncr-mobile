//
//  VaccineController.swift
//  ACV
//
//  Created by Pham Dai on 05/10/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class VaccineController: BaseViewController {

    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    
    @IBOutlet weak var lblCertificate: UILabel!
    @IBOutlet weak var imageQRCode: UIImageView!
    @IBOutlet weak var iconGuard: UIImageView!
    //information
    @IBOutlet weak var vwInformation: Corner5View!
    @IBOutlet weak var labelFullname: TextLabel!
    @IBOutlet weak var labelDob: TextLabel!
    @IBOutlet weak var labelPhone: TextLabel!
    @IBOutlet weak var labelAddress: TextLabel!
    
    // vaccines
    @IBOutlet weak var vwVaccines: Corner5View!
    @IBOutlet weak var stackVaccines: UIStackView!
    @IBOutlet weak var lblVaccineTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        config()
        loadData()
    }
    
    private func config() {
        
        setBarCloseButton()
        
        iconGuard.image = UIImage(named: "ic_guard")?.tint(with: .white)
        
        lblCertificate.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        lblCertificate.textColor = .white
        lblCertificate.adjustsFontSizeToFitWidth = true
        
        labelFullname.title = "Full name".localizedString()
        labelDob.title = "Birthday".localizedString()
        labelPhone.title = "Phone".localizedString()
        labelAddress.title = "Address".localizedString()
        title = "Vaccine Passport".localizedString().uppercased()
    }

    func loadData() {
        /*
        guard let personACV = Client.shared.user else {return}
        
        var backgroundColor:UIColor = .red
        let numerVaccineInjected = personACV.vacxin?.count ?? 0
        if numerVaccineInjected == 1 {
            backgroundColor = UIColor("#F8D959")
        } else if numerVaccineInjected > 1 {
            backgroundColor = UIColor("#4D9938")
        }
        view.backgroundColor = backgroundColor
        vwVaccines.isHidden = numerVaccineInjected == 0
        
        if numerVaccineInjected > 0 {
            lblCertificate.text = "have".localizedString().uppercased() + " \(numerVaccineInjected) \("Vaccinations".localizedString().uppercased())"
        } else {
            lblCertificate.text = "Not Vaccinated yet".localizedString().uppercased()
        }
        
        labelFullname.content = personACV.person?.fullName
        labelDob.content = personACV.person?.birthday?.stringToDateYYYYMMdd?.dateTimeToddMMyyyy
        labelAddress.content = personACV.personACV?.pAddress
        labelPhone.content = personACV.person?.phone
        
        stackVaccines.arrangedSubviews.forEach({$0.removeFromSuperview()})
        stackVaccines.spacing = 10
        personACV.vacxin?.enumerated().forEach({ i in
            let view = VaccineBlockView(frame: .zero)
            view.show(vaccine: i.element, index: i.offset + 1)
            stackVaccines.addArrangedSubview(view)
        })
        
        genateQRCode(content: personACV.qrstring ?? "")
        
        if #available(iOS 13.0, *) {
            let appearNavigationBar = UINavigationBarAppearance()
            appearNavigationBar.backgroundColor = backgroundColor
            appearNavigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            appearNavigationBar.shadowColor = backgroundColor
            appearNavigationBar.backgroundImage = UIImage()
            self.navigationController?.navigationBar.standardAppearance = appearNavigationBar
            self.navigationController?.navigationBar.scrollEdgeAppearance = appearNavigationBar
        } else {
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            self.navigationController?.navigationBar.barTintColor = backgroundColor
            self.navigationController?.navigationBar.shadowImage = UIImage()
        }
        self.navigationController?.navigationBar.isTranslucent = false
         */
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.view.safeAreaInsets.bottom + 20, right: 0)
        } else {
            scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        }
    }
    
    func genateQRCode(content:String?) {
        let inputCorrectionLevel = EFInputCorrectionLevel.h
        let content = content ?? ""//String(format: "#1%@", UIDevice.current.uuidForKeychain())
        let generator = EFQRCodeGenerator(content: content, size: EFIntSize(width: 1024, height: 1024))
        generator.setInputCorrectionLevel(inputCorrectionLevel: inputCorrectionLevel)
        generator.setMode(mode: EFQRCodeMode.none)
        generator.setMagnification(magnification: EFIntSize(width: 9, height: 9))
        generator.setColors(backgroundColor: CIColor(color: UIColor.white), foregroundColor: CIColor(color: UIColor.black))
        generator.setIcon(icon: UIImage2CGimage(nil), size: nil)
        generator.setForegroundPointOffset(foregroundPointOffset: 0.0)
        generator.setAllowTransparent(allowTransparent: true)
        generator.setBinarizationThreshold(binarizationThreshold: 0.5)
        generator.setPointShape(pointShape: EFPointShape.square)
        
        if let tryCGImage = generator.generate() {
            let tryImage = UIImage(cgImage: tryCGImage)
            imageQRCode.image = tryImage
            print("")
        } else {
            let alert = UIAlertController(title: "Warning", message: "Create QRCode failed!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
