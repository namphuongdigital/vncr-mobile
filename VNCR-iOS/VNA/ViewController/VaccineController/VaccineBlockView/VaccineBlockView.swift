//
//  VaccineBlockView.swift
//  ACV
//
//  Created by Pham Dai on 05/10/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class VaccineBlockView: BaseView {

    @IBOutlet weak var lblDateInjected: UILabel!
    @IBOutlet weak var lblvaccine: UILabel!
    @IBOutlet weak var lblAddressInject: UILabel!
    @IBOutlet weak var lblInjectNumber: UILabel!
    @IBOutlet weak var vwNumber: RoundCornerView!
    @IBOutlet weak var lblNumber: UILabel!
    
    override func config() {
        lblDateInjected.textColor = .black
        lblDateInjected.textAlignment = .center
        
        lblvaccine.textColor = .black
        lblAddressInject.textColor = .gray
        lblAddressInject.textColor = .gray
        lblInjectNumber.textColor = .gray
        
        vwNumber.backgroundColor = .systemBlue
        
        lblNumber.textColor = .white
        lblNumber.font = .systemFont(ofSize: 20, weight: .bold)
        
        lblInjectNumber.text = "Mũi số:".localizedString()
    }
    
    /*
    func show(vaccine:Vacxin?, index:Int) {
        lblDateInjected.setAttribute(elements: [(vaccine?.dateInject?.stringToDateYYYYMMdd?.dateTimeTodd ?? "",nil,UIFont.systemFont(ofSize: 30, weight: .bold)),("\n" + (vaccine?.dateInject?.stringToDateYYYYMMdd?.dateTimeToMMyyyy ?? ""),nil,UIFont.systemFont(ofSize: 16, weight: .bold))])
        lblvaccine.text = vaccine?.vacxinTypeName
        lblAddressInject.text = vaccine?.location
        lblNumber.text = "\(index)"
    }
     */
}
