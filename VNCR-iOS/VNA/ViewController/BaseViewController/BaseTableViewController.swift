//
//  BaseTableViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 4/6/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class BaseTableViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    
    override open func viewWillAppear(_ animated: Bool) {
        // Always adopt a dark interface style.
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
