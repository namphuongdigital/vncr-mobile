//
//  BaseSplitController.swift
//  VNA
//
//  Created by Dai Pham on 22/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class BaseSplitController: UISplitViewController {
    
    var rootController:UIViewController
    var detailController:UIViewController?
    
    init(root:UIViewController, detail:UIViewController?) {
        rootController = root
        detailController = detail
        super.init(nibName: nil, bundle: nil)
        viewControllers = [rootController]
        if #available(iOS 13.0, *){
            self.isModalInPopover = true
        }
        self.providesPresentationContextTransitionStyle = true
        self.modalPresentationStyle=UIModalPresentationStyle.fullScreen
    }
    
    required init?(coder: NSCoder) {
        rootController = UIViewController()
        super.init(coder: coder)
        viewControllers = [rootController]
        if #available(iOS 13.0, *){
            self.isModalInPopover = true
        }
        self.providesPresentationContextTransitionStyle = true
        self.modalPresentationStyle=UIModalPresentationStyle.fullScreen
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preferredDisplayMode = .allVisible
        if let vc = detailController {
            self.showDetailViewController(vc, sender: self)
        }
        
        extendedLayoutIncludesOpaqueBars = true
    }
    
    deinit {
        #if DEBUG
        print("\(NSStringFromClass(self.classForCoder)) \(#function)")
        #endif
    }
}
