//
//  BaseViewController.swift
//  MBN-iOS-App
//
//  Created by Van Trieu Phu Huy on 10/27/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//
import UIKit
import Foundation
import MBProgressHUD
import SwiftMessages
import DZNEmptyDataSet
import BoltsSwift
import QRCodeReader
import LocalAuthentication

open class BaseViewController: UIViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    public var progressHUD:MBProgressHUD!

    var isOfflineMode:Bool = false {
        didSet {
            NotificationCenter.default.post(name: NSNotification.Name("kChangeDataMode"), object: NSNumber(booleanLiteral: isOfflineMode))
        }
    }
    var isConnectedInternet:Bool = true
    
    // Dai custom affect present popup
    // ti le width so voi parent view
    public var multiplierWidth:CGFloat{get{return 1.0}}
    
    /// handle percent complete transition
    var interactionController: UIPercentDrivenInteractiveTransition?
    // custom transaction
    var customTransitionDelegate:UIViewControllerTransitioningDelegate?
    
    let success = MessageView.viewFromNib(layout: .statusLine)
    var successConfig = SwiftMessages.defaultConfig
    var stringButtonTitleTryAgain = ""
    var stringTitleTryAgain = "No items".localizedString()
    
    private var labelTitle = UILabel()
    public var textTitle: String {
        get {
            return labelTitle.text ?? ""
        } set {
            if(self.isViewLoaded) {
                if(labelTitle.superview == nil) {
                    self.navigationItem.titleView = labelTitle
                    
                }
            }
            labelTitle.text = newValue
            labelTitle.sizeToFit()
            print("")
            
        }
    }
    
    deinit {
        #if DEBUG
        print("\(NSStringFromClass(type(of: self))) \(#function)")
        #endif
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.reachabilityChanged, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("kChangeDataMode"), object: nil)
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        // Always adopt a dark interface style.
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    @objc func observerNetwork(object: Notification) {
        if let reachability = object.object as? Reachability {
            updateInterfaceWithReachability(reachability: reachability)
        }
    }
    
    @objc func observerDataMode(object: Notification) {
        if let isOff = (object.object as? NSNumber)?.boolValue {
            if isOfflineMode != isOff {
                isOfflineMode = isOff
            }
        }
    }
    
    @objc func updateInterfaceWithReachability(reachability:Reachability) {
        let netStatus = reachability.currentReachabilityStatus()
        switch netStatus {
        case NotReachable:
            // not internet connection
            #if DEBUG
            print("\("Internet Not Available") \(#function)")
            #endif
            networkDidChanged(connectedInternet: false, reachability: reachability)
        case ReachableViaWWAN, ReachableViaWiFi:
            #if DEBUG
            print("\("Reachable WWAN") \(#function)")
            #endif
            networkDidChanged(connectedInternet: true, reachability: reachability)
        default:
            break
        }
    }
    
    @objc func networkDidChanged(connectedInternet:Bool, reachability:Reachability) {
        // override to handle
        self.isConnectedInternet = connectedInternet
    }
    
    @objc func changeDataMode(_ sender:UIButton) {
        isOfflineMode = sender.isSelected
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        isConnectedInternet = NetworkObserver.shared.isConnectedInternet
        isOfflineMode = NetworkObserver.shared.internetReachability.currentReachabilityStatus() == NotReachable
        
        // config for data mode
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerNetwork(object:)), name: NSNotification.Name.reachabilityChanged, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerDataMode(object:)), name: NSNotification.Name(rawValue: "kChangeDataMode"), object: nil)
        
        success.configureTheme(.success)
        //success.configureDropShadow()
        success.configureContent(title: "Success", body: "Something good happened!")
        success.button?.isHidden = true
        successConfig.presentationStyle = .top
        successConfig.duration = .seconds(seconds: 0.5)
        successConfig.presentationContext = .window(windowLevel: UIWindow.Level(rawValue: UIWindow.Level.statusBar.rawValue))
        
        //self.navigationController?.navigationBar.topItem?.title = ""
        if #available(iOS 13.0, *) {
            let appearNavigationBar = UINavigationBarAppearance()
            appearNavigationBar.backgroundColor = UIColor("#166880")
            appearNavigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            self.navigationController?.navigationBar.standardAppearance = appearNavigationBar
            self.navigationController?.navigationBar.scrollEdgeAppearance = appearNavigationBar
        } else {
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            self.navigationController?.navigationBar.barTintColor = UIColor("#166880")
        }
        
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        if(self.navigationController != nil){
            progressHUD = MBProgressHUD.showAdded(to: self.navigationController!.view, animated: true)
        } else {
            progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        }
        
        progressHUD.removeFromSuperViewOnHide = false
        progressHUD.hide(animated: false)
        
        labelTitle.font = UIFont.boldSystemFont(ofSize: 14)
        labelTitle.text = ""
        labelTitle.textColor = UIColor.white
        labelTitle.sizeToFit()
        //self.navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func showMessageSuccess(content: String) {
        //SwiftMessages.hideAll()
        success.configureContent(title: "", body: content)
        SwiftMessages.show(config: successConfig, view: success)
        
    }
    
    
    func showMBProgressHUDInWindow(_ message:String, animated:Bool) {
        progressHUD?.removeFromSuperview()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        progressHUD = MBProgressHUD.showAdded(to: appDelegate.window!, animated: true)
        progressHUD.label.text = message
        progressHUD.show(animated: animated)
    }
    
    func showMBProgressHUD(_ message:String, animated:Bool) {
        progressHUD.label.text = message
        progressHUD.show(animated: animated)
        
    }
    
    func hideMBProgressHUD(_ animated:Bool) {
        progressHUD.hide(animated: animated)
    }
    
   
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func gotoMainApp() {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        
        let mainTabBarController = self.storyboard?.instantiateViewController(withIdentifier: "MainTabBarController")
        appDelegate?.window?.rootViewController = mainTabBarController
        appDelegate?.window?.makeKeyAndVisible()
        mainTabBarController?.view.alpha = 0.0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            mainTabBarController?.view.alpha = 1.0
        })
    }
    
    func deviceOwnerAuthentication(cancelTitle:String = "Cancel".localizedString(),
                                   context:LAContext,
                                   onNext:@escaping ()->Void,
                                   onError:@escaping (Int?)->Void,
                                   onNotAvailable:@escaping (_ isNotSupported:Bool)->Void) {

        if #available(iOS 10.0, *) {
            context.localizedCancelTitle = cancelTitle
        }
        
        // remove enter password
        context.localizedFallbackTitle = "Enter Passcode".localizedString()

        // First check if we have the needed hardware support.
        var error: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Verify you are the account holder".localizedString()
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in

                if success {
                    UserDefaults.standard.setValue(true, forKey: .keyEnableFaceID)
                    UserDefaults.standard.synchronize()
                    // Move to the main thread because a state update triggers UI changes.
                    DispatchQueue.main.async {
                        onNext()
                    }
                } else {
                    DispatchQueue.main.async {
                        if #available(iOS 11.0, *) {
                            if error?._code == LAError.biometryNotAvailable.rawValue {
                                onNotAvailable(error?.localizedDescription.lowercased().contains("not available") == true)
                                return
                            }
                        } else {
                            if error?._code == LAError.touchIDNotAvailable.rawValue {
                                onNotAvailable(error?.localizedDescription.lowercased().contains("not available") == true)
                                return
                            }
                        }
                        onError(error?._code)
                    }
                }
            }
        } else {
            DispatchQueue.main.async {
                if #available(iOS 11.0, *) {
                    if error?.code == LAError.biometryNotAvailable.rawValue {
                        onNotAvailable(error?.localizedDescription.lowercased().contains("not available") == true)
                        return
                    }
                } else {
                    if error?.code == LAError.touchIDNotAvailable.rawValue {
                        onNotAvailable(error?.localizedDescription.lowercased().contains("not available") == true)
                        return
                    }
                }
                onError(error?.code)
            }
        }
    }
    
    func pushReportFlightViewcontroller(scheduleFlyModel: ScheduleFlightModel, reportModel: ReportModel? = nil, isSameSchedule: Bool) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "ReportFlightViewcontroller") as! ReportFlightViewcontroller
        viewcontroller.scheduleFlyModel = scheduleFlyModel
        viewcontroller.finalReportModel = reportModel
        if splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController,isSameSchedule {
                if let current = nv.find(viewController: ReportFlightViewcontroller.self) {
                    nv.popToViewController(current, animated: true)
                } else {
                    nv.pushViewController(viewcontroller, animated: true)
                }
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewcontroller), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func synchronizeThirdServices(scheduleFlyModel: ScheduleFlightModel,_ completed:(()->Void)?) {
        self.showMBProgressHUD("Syncing...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskSyncFlightInfo(flightId: scheduleFlyModel.flightID)
            .continueOnSuccessWith(continuation: {[weak self] task in
                #if DEBUG
                print("\(task) \(#function)")
                #endif
            self?.hideMBProgressHUD(true)
                completed?()
        }).continueOnErrorWith(continuation: { error in
            self.hideMBProgressHUD(true)
            self.showAlert("Alert".localizedString(), message: (error as NSError).localizedDescription, buttons: ["Ok".localizedString()]) { action, pos in
               
            }
            completed?()
        })
    }
    
    func pushCreateFormViewController(formModel: FormModel? = nil) {
//        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "CreateFormViewController") as! CreateFormViewController
//        viewcontroller.formModel = formModel
        let vc = CreateFormAndHistoryController(formModel: formModel)
        if splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController {
                nv.pushViewController(vc, animated: true)
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: vc), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func pushFormWorkflowViewController(formModel: FormModel? = nil) {
        let viewcontroller = FormWorkflowViewController(nibName: "FormWorkflowViewController", bundle: Bundle.main)
        viewcontroller.formModel = formModel
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushFlightReportListViewController(scheduleFlyModel: ScheduleFlightModel, isSameSchedule: Bool) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "FlightReportListViewController") as! FlightReportListViewController
        viewcontroller.scheduleFlyModel = scheduleFlyModel
        if splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController,isSameSchedule {
                if let current = nv.find(viewController: FlightReportListViewController.self) {
                    nv.popToViewController(current, animated: true)
                } else {
                    nv.pushViewController(viewcontroller, animated: true)
                }
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewcontroller), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func pushAllFlightReportListViewController() {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "AllFlightReportListViewController") as! AllFlightReportListViewController
        if splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController {
                nv.pushViewController(viewcontroller, animated: true)
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewcontroller), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func pushAssessmentCrewDutyListViewController(scheduleFlyModel: ScheduleFlightModel, isSameSchedule:Bool) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "AssessmentCrewDutyListViewController") as! AssessmentCrewDutyListViewController
        viewcontroller.scheduleFlyModel = scheduleFlyModel
        if splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController, isSameSchedule {
                if let current = nv.find(viewController: AssessmentCrewDutyListViewController.self) {
                    nv.popToViewController(current, animated: true)
                } else {
                    nv.pushViewController(viewcontroller, animated: true)
                }
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewcontroller), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func pushAssessmentCrewDutyViewController(crewDutyModel : CrewDutyModel) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "AssessmentCrewDutyViewController") as! AssessmentCrewDutyViewController
        viewcontroller.crewDutyModel = crewDutyModel
        if splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController {
                nv.pushViewController(viewcontroller, animated: true)
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewcontroller), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func pushProfileViewController(crewId: String, permission: PermissionCrewTaskType = PermissionCrewTaskType.read, flightId: Int = 0, sourceCrewID: String = "", destinationCrewID: String = "") {
        
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        viewcontroller.crewId = crewId
        viewcontroller.permission = permission
        viewcontroller.flightId = flightId
        viewcontroller.sourceCrewID = sourceCrewID
        viewcontroller.destinationCrewID = destinationCrewID
        if splitViewController != nil, parent == nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController {
                nv.pushViewController(viewcontroller, animated: true)
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewcontroller), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func pushMoreViewController(crewId: String, permission: PermissionCrewTaskType = PermissionCrewTaskType.read, flightId: Int = 0, sourceCrewID: String = "", destinationCrewID: String = "") {
        
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "MoreViewController") as! MoreViewController
        viewcontroller.crewId = crewId
        viewcontroller.permission = permission
        viewcontroller.flightId = flightId
        viewcontroller.sourceCrewID = sourceCrewID
        viewcontroller.destinationCrewID = destinationCrewID
        if splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController {
                nv.pushViewController(viewcontroller, animated: true)
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewcontroller), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func pushSupportDetailViewController(supportModel: SupportModel) {
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "SupportDetailViewController") as! SupportDetailViewController
        viewController.supportModel = supportModel
        if splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController {
                nv.pushViewController(viewController, animated: true)
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewController), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewController, animated: true)
        }
         
    }
    
    func callNumber(phoneNumber: String?) {
        guard let phoneNumber = phoneNumber else {
            return
        }
        if let phoneCallURL: URL = URL(string:"tel://\(phoneNumber.replacingOccurrences(of: " ", with: ""))") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.openURL(phoneCallURL)
            }
        }
    }
    
    func pushEditContentViewController(title: String, content: String, delegate: EditContentViewControllerDelegate? = nil) {
        
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "EditContentViewController") as! EditContentViewController
        viewcontroller.titleString = title
        viewcontroller.content = content
        viewcontroller.delegate = delegate
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushMessageListViewController() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let mainTabBarController = appDelegate?.window?.rootViewController as? UITabBarController {
            mainTabBarController.selectedIndex = 1
        }
        /*
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "MessageListViewController") as! MessageListViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        */
    }
    
    func pushSettingViewController() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        if let mainTabBarController = appDelegate?.window?.rootViewController as? UITabBarController {
            mainTabBarController.selectedIndex = 4
        }
        
    }
    
    
    
    func pushNewListViewController() {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "NewListViewController") as! NewListViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushNewDetailViewController(messageModel: MessageModel) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "NewDetailViewController") as! NewDetailViewController
        viewcontroller.messageModel = messageModel
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushWebViewNewsViewController(id: Int64, title: String) {
        let viewcontroller = WebViewNewsViewController()
        viewcontroller.id = id
        viewcontroller.textTitle = title.isEmpty ? "News".localizedString() : title
        viewcontroller.title = title.isEmpty ? "News".localizedString() : title
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushIncomeListViewController() {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "IncomeListViewController") as! IncomeListViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushIncomeDetailViewController(messageModel: MessageModel) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "IncomeDetailViewController") as! IncomeDetailViewController
        viewcontroller.messageModel = messageModel
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushWebViewIncomeViewController(id: Int64, title: String) {
        let viewcontroller = WebViewIncomeViewController()
        viewcontroller.id = id
        viewcontroller.textTitle = title
        viewcontroller.title = title
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
        
    func pushItemMenuViewController(menuItem: MenuItemModel) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "ItemMenuViewController") as! ItemMenuViewController
        viewcontroller.menuItem = menuItem
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    //Harry Nhan Added
    func pushFormDetailViewController(cid: String) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "FormHistoryViewController") as! FormHistoryViewController
        viewcontroller.cid = cid
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushMenuItemDetailViewController(menuItem: MenuItemModel, id: Int) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "ItemMenuDetailViewController") as! ItemMenuDetailViewController
        viewcontroller.menuItem = menuItem
        viewcontroller.id = id
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushFormListViewController() {
        // Dai
        // request permission camera, photos truoc khi push view
        var allowedCamera = false
        var allowedPhoto = false
        let group = DispatchGroup()
        group.enter()
        EasyPermission.requestCameraPermission { (status) in
            if(status == EasyAuthorityStatus.authorizationStatusDenied) {
                DispatchQueue.main.async {
                    EasyPermission.alertTitle("Alert".localizedString(), message: "Press Setting for config Camera".localizedString())
                }
                group.leave()
                //
            } else {
                allowedCamera = true
                group.leave()
            }
        }
        group.enter()
        EasyPermission.requestPhotoLibrayPermission { (status) in
            if(status == EasyAuthorityStatus.authorizationStatusDenied) {
                DispatchQueue.main.async {
                    EasyPermission.alertTitle("Alert".localizedString(), message: "Press Setting for config Photos".localizedString())
                }
                group.leave()
            } else {
                allowedPhoto = true
                group.leave()
            }
        }
        
        group.notify(queue: .main) {
            if !allowedPhoto || !allowedCamera {return}
            let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "FormListViewController") as! FormListViewController
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func presentSeatmapViewController(seatmap: AircraftCategoryType, title: String, flightId: Int) {
        //let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "SetmapViewController") as! SetmapViewController
        
        // dung splitview cho ipad
        if splitViewController != nil {
            let nv = BasePresentNavigationController(root: FlightSeatMapListViewController(titleText: title, flightId: flightId, seatmap: seatmap))
            let vc = BaseSplitController(root: nv, detail: nil)
//            self.splitViewController?.showDetailViewController(vc, sender: nil)
            // do present tiep tren tabbar nen cho cho popover dismissed
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(300)) {
                self.present(vc, animated: true, completion: nil)
            }
        } else {
            let vc = FlightSeatMapListViewController(titleText: title, flightId: flightId, seatmap: seatmap)
            vc.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func pushWebViewDetailBannerViewController(id: Int, title: String) {
        let viewcontroller = WebViewDetailBannerViewController()
        viewcontroller.id = id
        viewcontroller.textTitle = title
        viewcontroller.title = title
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushWebViewMessageViewController(id: Int64, title: String) {
        let viewcontroller = WebViewMessageViewController()
        viewcontroller.id = id
        viewcontroller.textTitle = title
        viewcontroller.title = title
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushMessageDetailViewController(messageModel: MessageModel) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "MessageDetailViewController") as! MessageDetailViewController
        viewcontroller.messageModel = messageModel
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushVerifyCodeViewController(taskCompletionSource: TaskCompletionSource<AnyObject>? = nil) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "VerifyCodeViewController") as! VerifyCodeViewController
        viewcontroller.taskCompletionSource = taskCompletionSource
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushSurveyListViewController(scheduleFlyModel: ScheduleFlightModel?, isListSaved: Bool, isSameSchedule: Bool) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "SurveyListViewController") as! SurveyListViewController
        viewcontroller.scheduleFlyModel = scheduleFlyModel
        viewcontroller.isListSaved = isListSaved
        if splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController,isSameSchedule {
                if let current = nv.find(viewController: SurveyListViewController.self) {
                    nv.popToViewController(current, animated: true)
                } else {
                    nv.pushViewController(viewcontroller, animated: true)
                }
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewcontroller), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func pushLeadreportPosition(scheduleFly: ScheduleFlightModel?, isSameSchedule: Bool) {
        let leadReportPositionViewController = self.storyboard?.instantiateViewController(withIdentifier: "LeadReportPositionViewController") as! LeadReportPositionViewController
        leadReportPositionViewController.scheduleFlyModel = scheduleFly
        if self.splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController, isSameSchedule {
                if let current = nv.find(viewController: LeadReportPositionViewController.self) {
                    nv.popToViewController(current, animated: true)
                } else {
                    nv.pushViewController(leadReportPositionViewController, animated: true)
                }
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: leadReportPositionViewController), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(leadReportPositionViewController, animated: true)
        }
    }
    
    func pushOJTListExaminees(scheduleFly: ScheduleFlightModel?, isSameSchedule: Bool) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "OJTListExamineesViewController") as! OJTListExamineesViewController
        viewcontroller.scheduleFlyModel = scheduleFly
        if self.splitViewController != nil {
            if splitViewController?.viewControllers.count ?? 0 > 1,
               let nv = splitViewController?.viewControllers.last as? UINavigationController, isSameSchedule {
                if let current = nv.find(viewController: OJTListExamineesViewController.self) {
                    nv.popToViewController(current, animated: true)
                } else {
                    nv.pushViewController(viewcontroller, animated: true)
                }
            } else {
                self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: viewcontroller), sender: nil)
            }
        } else {
            self.navigationController?.pushViewController(viewcontroller, animated: true)
        }
    }
    
    func pushSurveyDetailViewController(surveyModel: SurveyModel, scheduleFlyModel: ScheduleFlightModel?, isModelSaved: Bool = false) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "SurveyDetailViewController") as! SurveyDetailViewController
        viewcontroller.surveyModel = surveyModel
        viewcontroller.scheduleFlyModel = scheduleFlyModel
        viewcontroller.isModelSaved = isModelSaved
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }
    
    func pushOJTListViewController(scheduleFlyModel: ScheduleFlightModel?, isListSaved: Bool, cid: String) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "OJTListViewController") as! OJTListViewController
        viewcontroller.scheduleFlyModel = scheduleFlyModel
        viewcontroller.isListSaved = isListSaved
        viewcontroller.cid = cid
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushOJTDetailViewController(ojtModel: OJTModel, scheduleFlyModel: ScheduleFlightModel?, isModelSaved: Bool = false, cid: String) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "OJTDetailViewController") as! OJTDetailViewController
        viewcontroller.ojtModel = ojtModel
        viewcontroller.scheduleFlyModel = scheduleFlyModel
        viewcontroller.isModelSaved = isModelSaved
        viewcontroller.cid = cid
        self.navigationController?.pushViewController(viewcontroller, animated: true)
        
    }
    
    func pushOJTListExamineesViewController(scheduleFlyModel: ScheduleFlightModel) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "OJTListExamineesViewController") as! OJTListExamineesViewController
        viewcontroller.scheduleFlyModel = scheduleFlyModel
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    @available(iOS 10.0,*)
    func saveScheduleFlight(scheduleFlyModel: ScheduleFlightModel,
                            isPersonal:Bool,
                            isGetFull:Bool,
                            completed:COMPLETEDERRORRESULT) {
        let userId = ServiceData.sharedInstance.userId ?? 0
        ServiceDataCD.saveFlight(flightId: scheduleFlyModel.flightID,
                                 userId: userId,
                                 model: scheduleFlyModel,
                                 isPersonal: isPersonal,
                                 isGetFull: isGetFull,
                                 completed: completed)
    }
    
    @available(iOS 10.0,*)
    func deleteScheduleFlightLocal(flightId: Int,
                                    completed:COMPLETEDERRORRESULT) {
        ServiceDataCD.deleteSavedFlight(flightId: flightId, completed: completed)
    }
    
    func pushListDeviceViewController() {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "ListDeviceViewController") as! ListDeviceViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushListCommandViewController() {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "ListCommandViewController") as! ListCommandViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushEmployeesViewController() {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "EmployeesViewController") as! EmployeesViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushDetailEmployeesViewController(id: Int) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "DetailEmployeesViewController") as! DetailEmployeesViewController
        viewcontroller.id = id
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushStatisticViewController() {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "StatisticViewController") as! StatisticViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushStatisticDetailViewController(id: Int) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "StatisticDetailViewController") as! StatisticDetailViewController
        viewcontroller.id = id
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushWebViewDashboardViewController() {
        let viewcontroller = WebViewDashboardViewController()
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushWebViewDiscoverViewController() {
        let viewcontroller = WebViewDiscoverViewController()
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushSupportListViewController() {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "SupportListViewController") as! SupportListViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushWeeklyReportListViewController() {
        let viewcontroller = WeeklyReportListViewController(nibName: "WeeklyReportListViewController", bundle: Bundle.main)
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    func pushWeeklyReportDetailViewController(id: Int, title: String) {
        let viewcontroller = WebViewWeeklyReportViewController()
        viewcontroller.id = id
        viewcontroller.textTitle = title
        viewcontroller.title = title
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    //AlertView
    
    func showAlert(_ stringTitle:String, stringContent:String)  {
        self.hideMBProgressHUD(false)
        let alert = UIAlertController(title: stringTitle, message: stringContent, preferredStyle: UIAlertController.Style.alert)
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Close".localizedString(), style: UIAlertAction.Style.cancel, handler: nil))
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    @discardableResult
    func showAlert(_ title:String, message:String, buttons:[String], tapBlock:((UIAlertAction,Int) -> Void)?) -> UIAlertController? {
        self.hideMBProgressHUD(false)
        return EZAlertController.alert(title, message: message, buttons: buttons, tapBlock: tapBlock)
    }
        
    //ActionSheet
    func showActionSheet(_ title:String, message:String, buttons:[String], sourceView:UIView? = nil, tapBlock:((UIAlertAction,Int) -> Void)?) -> UIAlertController? {
        self.hideMBProgressHUD(false)
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            return self.showAlert(title, message: message, buttons: buttons, tapBlock: tapBlock)
        } else {
            return EZAlertController.actionSheet(title, message: message, sourceView: sourceView ?? self.view, buttons: buttons, tapBlock: tapBlock)
        }
    }
    
    func showAlertWithAction(_ title:String?, message:String?, actions:[UIAlertAction]) {
        self.hideMBProgressHUD(false)
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.showAlertWithActionIpad(title, message, actions: actions)
        } else {
            EZAlertController.actionSheet(title ?? "Alert".localizedString(), message: message ?? "", sourceView: self.view, actions: actions)
        }
    }
    
    func showActionSheetWithAction(_ title:String?, message:String?, sourceView:UIView? = nil, actions:[UIAlertAction]) {
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.showAlertWithAction(title, message: message, actions: actions)
        } else {
            let _ = EZAlertController.actionSheet(title ?? "Alert".localizedString(), message: message ?? "", sourceView: self.view, actions: actions)
        }
    }
    
    func showAlertWithActionIpad(_ title: String?, _ message: String?, actions: [UIAlertAction]) {
        DispatchQueue.main.async {
            self.hideMBProgressHUD(false)
            let actionSheet = UIAlertController(title: title ?? "Alert".localizedString(), message: message, preferredStyle: UIAlertController.Style.alert)
            // add the actions (buttons)
            if (actions.count == 0) {
                actionSheet.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.cancel, handler: nil))
            } else {
                for action in actions {
                    actionSheet.addAction(action)
                }
            }
            // show the alert
            // actionSheet.popoverPresentationController?.sourceView = self.view
            // actionSheet.popoverPresentationController?.permittedArrowDirections = UIPopoverArrowDirection.any
            self.present(actionSheet, animated: true, completion: nil)
        }
    }
    
    //MARK - DZNEmptyDataSetSource
    public func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: stringTitleTryAgain, attributes: nil)
    }
    public func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "", attributes: nil)
    }
    
    // MARK: - Actions
    
    func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            UIApplication.shared.openURL(settingsURL)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
}

// MARK: - IMAGE
public extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func resizeImageWith(newSize: CGSize) -> UIImage {
        let horizontalRatio = newSize.width / size.width
        let verticalRatio = newSize.height / size.height
        let ratio = max(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func resizeImage(newSize: CGSize) -> UIImage {
        let size = self.size
        let widthRatio  = newSize.width  / size.width
        let heightRatio = newSize.height / size.height
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(origin: CGPoint(x: 0, y: 0), size: newSize)
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        self.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func maskRoundedImage(radius: CGFloat? = nil) -> UIImage {
        let width = min(self.size.width,self.size.height)
        let imageView: UIImageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: width)))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        let layer = imageView.layer
        layer.masksToBounds = true
        layer.cornerRadius = radius == nil ? self.size.width/2 : radius!
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size,false, UIScreen.main.scale)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage!
    }
    
    func maskCornerRadiusImage(radius: CGFloat) -> UIImage {
        let width = min(self.size.width,self.size.height)
        let imageView: UIImageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: width)))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        let layer = imageView.layer
        layer.masksToBounds = true
        layer.cornerRadius = radius
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size,false, UIScreen.main.scale)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage!
    }
    
    func tint(with color: UIColor) -> UIImage {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    static func textToImage(drawText text: String, inImage image: UIImage, atPoint point: CGPoint? = nil) -> UIImage {
        let textColor = UIColor.red
        let textFont = UIFont.boldSystemFont(ofSize: 12)
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes:[NSAttributedString.Key:Any] = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
        ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        var p = CGPoint.zero
        if let pp = point {
            p = pp
        }
        let rect = CGRect(origin: p, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func isEqualToImage(image: UIImage) -> Bool {
        guard let data1 = self.pngData(),
              let data2 = image.pngData() else {return false}
        let dt1 = data1 as NSData
        let dt2 = data2 as NSData
        return dt1.isEqual(dt2)
    }
    
}

// MARK: -  Dai: một số function khác
extension BaseViewController {
    func present(vc: UIViewController,
                 transitioning: UIViewControllerTransitioningDelegate?,
                 completion:(()->Void)?) {
        
        if vc.isKind(of: UISplitViewController.self) == false,
           let transition = transitioning {
            customTransitionDelegate = transition
            vc.transitioningDelegate = customTransitionDelegate
        }
        
        self.present(vc, animated: true, completion: completion)
    }
    
    func setBarCloseButton(title:String? = nil,
                           icon:UIImage = #imageLiteral(resourceName: "ic_close_128"),
                           color:UIColor = .white) {
        
        let barbutton = UIBarButtonItem(image: icon.resizeImageWith(newSize: CGSize(width: 30, height: 30)), style: .done, target: self, action: #selector(close))
        self.navigationItem.setLeftBarButton(barbutton, animated: false)
        navigationItem.leftBarButtonItem?.tintColor = color
        navigationItem.title = title
    }
    
    func setBarRightButton(image:UIImage? = nil,
                          title:String? = nil,
                          color:UIColor = .white,
                          bag:Int = 0) {
        navigationItem.rightBarButtonItem = nil
        
        if image == nil && title == nil {
            return
        }
        
        if let image = image {
            let barbutton = UIBarButtonItem(image: image.resizeImageWith(newSize: CGSize(width: 20, height: 20)).tint(with: color),
                                            style: .plain,
                                            target: self,
                                            action: #selector(actionBarRightButton))
            navigationItem.setRightBarButton(barbutton,
                                             animated: true)
            navigationItem.rightBarButtonItem?.tintColor = color
        }
        
        if let title = title {
            let btn = UIBarButtonItem(image: nil,
                                      title: title,
                                      color: color,
                                      font: UIFont.systemFont(ofSize: 17, weight: .medium),
                                      isLeft: false,
                                      target: self,
                                      action: #selector(actionBarRightButton))
//            btn.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : color], for: UIControlState())
            navigationItem.setRightBarButton(btn,animated: true)
        }
    }
    
    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func actionBarRightButton() {
        self.dismiss(animated: true, completion: nil)
    }
}
