//
//  BasePresentNavigationController.swift
//  VNA
//
//  Created by Dai Pham on 23/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class BasePresentNavigationController: UINavigationController {

    init(root: UIViewController) {
        super.init(nibName: String(describing: BasePresentNavigationController.self), bundle: .main)
        self.viewControllers = [root]
        self.setCustomPresentationStyle()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setCustomPresentationStyle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
