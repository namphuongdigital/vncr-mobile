//
//  ProfileViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/12/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import MessageUI
import SwiftyJSON
import QRCodeReader
import AVFoundation

class ProfileViewController: FormViewController, MFMailComposeViewControllerDelegate, QRCodeReaderViewControllerDelegate {
    
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode], captureDevicePosition: .back)
            $0.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    var flightId: Int = 0
    
    var sourceCrewID: String = ""
    
    var destinationCrewID: String = ""
    
    var permission: PermissionType = .read
    
    var crewId: String = ""
    
    var userModel: UserModel!
    
    var textFieldFullnameRowFormer:RowFormer!
    
    var textFieldUsernameRowFormer:RowFormer!
    
    var textFieldEmailRowFormer:RowFormer!
    
    var textFieldDOBRowFormer:RowFormer!
    
    var textFieldGenderRowFormer: RowFormer!
    
    var textFieldCodeRowFormer:RowFormer!
    
    var textFieldPPRowFormer:RowFormer!
    
    var textFieldVersionRowFormer:RowFormer!
    
    var textFieldUrlAPIRowFormer:RowFormer!//TextFieldRowFormer<FormTextFieldCell>!
    
    var updateAuthorizeTableViewCell: CustomRowFormer<UpdateReportTableViewCell>!
    
    var updateConfirmInfoTableViewCell: CustomRowFormer<UpdateReportTableViewCell>!
    
    fileprivate var avatarHeader:CustomViewFormer<FormHeaderFooterView>!
    
    var heightHeaderAvatar: CGFloat = 170
    
    var textUrlAPI: String = ""
    
    var textVersion: String = "Unknown".localizedString()
    
    var isChangeUrlAPI: Bool = false
    
    var logoutBarButtonItem: UIBarButtonItem!
    
    var refreshControl: UIRefreshControl!
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView.addSubview(refreshControl)
            }
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.navigationItem.title = "PROFILE".localizedString()
        if(crewId == "") {
            self.userModel = ServiceData.sharedInstance.userModel!
            self.loadViewData()
        } else {
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskUserGetinfobycrewid(crewId: self.crewId).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    let userModel = UserModel(json: result)
                    self.navigationItem.title = userModel.account
                    self.userModel = userModel
                    DispatchQueue.main.async {
                        self.loadViewData()
                        self.tableView.reloadData()
                    }
                }
                
                self.hideMBProgressHUD(true)
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
    }
    
    func refreshData() {
        if(crewId == "") {
            if(self.userModel.userId == 0) {
                ServiceData.sharedInstance.taskShowFormRegisterDevice().continueOnSuccessWith(continuation: { task in
                    self.userModel = ServiceData.sharedInstance.userModel!
                    self.former.removeAll()
                    self.loadViewData()
                    self.former.reload()
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                    
                    
                }).continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    self.hideMBProgressHUD(true)
                    self.refreshControl.endRefreshing()
                })
            } else {
                self.hideMBProgressHUD(true)
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
            
        } else {
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskUserGetinfobycrewid(crewId: self.crewId).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    let userModel = UserModel(json: result)
                    self.navigationItem.title = userModel.account
                    self.userModel = userModel
                    DispatchQueue.main.async {
                        self.loadViewData()
                        self.tableView.reloadData()
                    }
                }
                
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            })
        }
    }
    
    func loadViewData() {
        //
        let createMenu: ((String, _ textColor:UIColor?, _ isDisclosureIndicator:Bool, (() -> Void)?) -> RowFormer) = { text, color, isDisclosure,  onSelected in
            return LabelRowFormer<FormLabelCell>() {
                if(color == nil){
                    $0.titleLabel.textColor = UIColor.black
                } else {
                    $0.titleLabel.textColor = color
                }
                
                $0.titleLabel.font = UIFont.systemFont(ofSize: 15)
                
                if(isDisclosure){
                    $0.accessoryType = .disclosureIndicator
                } else {
                    $0.accessoryType = .none
                }
                
                }.configure {
                    $0.text = text
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        var nibNameUpdateAuthorizeTableViewCell = "UpdateReportTableViewCell"
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            heightHeaderAvatar = 140
            nibNameUpdateAuthorizeTableViewCell = String(format: "%@_iPhone", nibNameUpdateAuthorizeTableViewCell)
        }
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            textVersion = String(format: "%@ : %@", "Version".localizedString(), version)
        }
        
        if(self.userModel.account == "kerberos") {
            isChangeUrlAPI = true
            
        }
        /*
        logoutBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.logoutBarButtonItemPressed(_:)))
        logoutBarButtonItem.setFAIcon(icon: .FAPowerOff, iconSize: 20)
        if(self.userModel.userId == ServiceData.sharedInstance.userModel?.userId) {
            self.navigationItem.rightBarButtonItem = self.logoutBarButtonItem
        }
        */
        self.tableView.separatorInset = UIEdgeInsetsMake(self.tableView.separatorInset.top, 0, self.tableView.separatorInset.bottom, 0)
        self.tableView.backgroundColor = UIColor("#fafafa")
        //self.tableView.frame = CGRect(x: (self.tableView.frame.size.width - 320)/2.0, y: 0, width: 320, height: self.tableView.frame.size.height)
        
        
        textFieldFullnameRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldFullnameRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldFullnameRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAUser, postfixText: String(format: "  %@", self.userModel.nameTV), size: 14, iconSize: 16)
        
        textFieldUsernameRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldUsernameRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldUsernameRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAAt, postfixText: String(format: "  %@", self.userModel.account), size: 14, iconSize: 16)
        
        
        textFieldCodeRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldCodeRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldCodeRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FACode, postfixText: String(format: "  %@", self.userModel.codeTV), size: 14, iconSize: 16)
        
        textFieldEmailRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldEmailRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldEmailRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAAsterisk, postfixText: String(format: "  %@", self.userModel.mainBase), size: 14, iconSize: 16)
        
        textFieldDOBRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldDOBRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldDOBRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAGift, postfixText: String(format: "  %@", self.userModel.dob), size: 14, iconSize: 16)
        
        textFieldGenderRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldGenderRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldGenderRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FATransgender, postfixText: String(format: "  %@", self.userModel.gender == .male ? "Male".localizedString() : "Female".localizedString()), size: 14, iconSize: 16)
        
        
        textFieldPPRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        
        textFieldPPRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldPPRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldPPRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FACreditCard, postfixText: String(format: "  %@", self.userModel.pPortNo), size: 14, iconSize: 16)
        
     
        let textFieldPhoneRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
            if(self?.userModel.userId != ServiceData.sharedInstance.userModel?.userId) {
                self?.callNumber(phoneNumber: self?.userModel.phone)
            }
            
        }
        (textFieldPhoneRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (textFieldPhoneRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAPhoneSquare, postfixText: String(format: "  %@", self.userModel.phone), size: 14, iconSize: 16)
        
        
        
        textFieldVersionRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldVersionRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldVersionRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAInfoCircle, postfixText: String(format: "  %@", self.textVersion), size: 14, iconSize: 16)
        
        
        textFieldUrlAPIRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
            self?.saveAPIURL()
        }
        (textFieldUrlAPIRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (textFieldUrlAPIRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FACompress, postfixText: String(format: "  %@", ServiceData.sharedInstance.hostAPI), size: 14, iconSize: 16)
        
        // Create Headers
        
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor("#fafafa")
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
  
                }.configure {
                    $0.viewHeight = 50
                    $0.text = text
                    
            }
        }
        
        
        avatarHeader = CustomViewFormer<FormHeaderFooterView>() { [weak self] (formHeaderFooterView) in
            guard let `self` = self else {
                return
            }
            let imageView = UIImageViewProgress(isProgressBar: false)
            imageView.contentMode = .scaleAspectFit
            imageView.tag = 10
            imageView.frame =  CGRect(x: (UIScreen.main.bounds.width - 120) / 2.0, y: 10, width: 120, height: 120)
            //imageView.layer.backgroundColor = UIColor.init(white: 0.886, alpha: 1.000).cgColor
            imageView.layer.cornerRadius = 2.5//imageView.frame.size.height / CGFloat(2.0)
            imageView.layer.borderWidth = 0.5
            imageView.layer.borderColor = UIColor.init(white: 0.837, alpha: 1.000).cgColor
            imageView.clipsToBounds = true
            imageView.loadImageNoProgressBar(url: URL(string: self.userModel.imageBase64 ))
            imageView.isUserInteractionEnabled = true
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageAvatarTapped(tapGestureRecognizer:)))
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(tapGestureRecognizer)
            
            formHeaderFooterView.contentView.addSubview(imageView)
            
            formHeaderFooterView.contentView.backgroundColor = UIColor("#fafafa")
            }.configure {
                $0.viewHeight = heightHeaderAvatar
        }
        
        let feedbackEmailRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            
            self?.sendMailTo(email: "congnb@vietnamairlines.com")
        }
        (feedbackEmailRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (feedbackEmailRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAEnvelopeO, postfixText: String(format: "  %@", "Send feedback".localizedString()), size: 14, iconSize: 16)
        
        let helpLinkRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.gotoLink(link: "http://doantiepvien.com.vn/vi/tin-tuc/vn-crew-user-giude/1/81")
        }
        (helpLinkRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (helpLinkRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAQuestion, postfixText: String(format: "  %@", "Support".localizedString()), size: 14, iconSize: 16)
        
        var logoutRow: RowFormer?
        
        if(self.userModel.userId == ServiceData.sharedInstance.userModel?.userId) {
            logoutRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
                self?.former.deselect(animated: true)
                self?.logoutBarButtonItemPressed()
            }
            (logoutRow!.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
            (logoutRow!.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAPowerOff, postfixText: String(format: "  %@", "Deactive".localizedString()), size: 14, iconSize: 16)
        }
        
        updateAuthorizeTableViewCell = CustomRowFormer<UpdateReportTableViewCell>(instantiateType: .Nib(nibName: nibNameUpdateAuthorizeTableViewCell)) {[weak self] in
            print($0.textLabel?.text)
            $0.buttonUpdate.setTitle("Set Authorize".localizedString(), for: UIControlState())
            $0.buttonUpdate.addTarget(self, action: #selector(self?.buttonUpdateAuthorizePress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        updateConfirmInfoTableViewCell = CustomRowFormer<UpdateReportTableViewCell>(instantiateType: .Nib(nibName: nibNameUpdateAuthorizeTableViewCell)) {[weak self] in
            print($0.textLabel?.text)
            $0.buttonUpdate.setTitle("Confirm Info".localizedString(), for: UIControlState())
            $0.buttonUpdate.addTarget(self, action: #selector(self?.buttonUpdateConfirmInfoPress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        let scanQRCodeRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.scanInModalAction(self!)
        }
        (scanQRCodeRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (scanQRCodeRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAQrcode, postfixText: String(format: "  %@", "Scan QR code".localizedString()), size: 14, iconSize: 16)
        
        let showSurveyListArchiveRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.showSurveyListArchivePress(self!)
        }
        showSurveyListArchiveRow.cellInstance.accessoryType = .disclosureIndicator
        (showSurveyListArchiveRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showSurveyListArchiveRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "Survey archive list".localizedString()), size: 14, iconSize: 16)
        
        var imageSection: SectionFormer
        var listRowFormer = [RowFormer]()
        listRowFormer.append(textFieldFullnameRowFormer)
        listRowFormer.append(textFieldUsernameRowFormer)
        listRowFormer.append(textFieldCodeRowFormer)
        listRowFormer.append(textFieldEmailRowFormer)
        listRowFormer.append(textFieldDOBRowFormer)
        listRowFormer.append(textFieldGenderRowFormer)
        listRowFormer.append(textFieldPPRowFormer)
        listRowFormer.append(textFieldPhoneRowFormer)
        
        if(ServiceData.sharedInstance.userModel?.userId == self.userModel.userId) {
            if(self.userModel.isInfoConfirmed == false) {
                listRowFormer.append(updateConfirmInfoTableViewCell)
            }
            listRowFormer.append(scanQRCodeRow)
            listRowFormer.append(showSurveyListArchiveRow)
        } else {
            if(permission == .read) {
                
            } else {
                listRowFormer.append(updateAuthorizeTableViewCell)
            }
        }
        imageSection = SectionFormer(rowFormers: listRowFormer).set(headerViewFormer: avatarHeader)
        
        var listPrivateRowFormer = [RowFormer]()
        listPrivateRowFormer.append(textFieldVersionRowFormer)
        if(self.userModel.account == "kerberos") {
            listPrivateRowFormer.append(textFieldUrlAPIRowFormer)
        }
        listPrivateRowFormer.append(feedbackEmailRow)
        listPrivateRowFormer.append(helpLinkRow)
        if(logoutRow != nil) {
            listPrivateRowFormer.append(logoutRow!)
        }
        
        let privateInfo = SectionFormer(rowFormers: listPrivateRowFormer)
            .set(headerViewFormer: createHeader("Others".localizedString()))
        
        if(self.userModel.userId == ServiceData.sharedInstance.userModel?.userId) {
            former.append(sectionFormer: imageSection, privateInfo)
                .onCellSelected { [weak self] _ in
                    //self?.formerInputAccessoryView.update()
            }
        } else {
            former.append(sectionFormer: imageSection)
                .onCellSelected { [weak self] _ in
                    //self?.formerInputAccessoryView.update()
            }
        }
        
        
        
        UITextField.appearance().tintColor = UIColor.black
        UITextView.appearance().tintColor = UIColor.black
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func gotoLink(link: String) {
        UIApplication.shared.openURL(URL(string: link)!)
    }
    
    func sendMailTo(email: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody("<p>Send feedback !</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    
    func logoutBarButtonItemPressed() {
        
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertActionStyle.destructive, handler: { (UIAlertAction) -> Void in
            
            ServiceData.sharedInstance.userModel = nil
            UserDefaults.standard.setValue("", forKey: "token")
            ApplicationData.sharedInstance.deleteAllUserLogin()
            ApplicationData.sharedInstance.deleteAllMessageModel()
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            
            //let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
            let navigationRegisterDeviceViewController = self.storyboard?.instantiateViewController(withIdentifier: "NavigationRegisterDeviceViewController")
            appDelegate?.window?.rootViewController = navigationRegisterDeviceViewController
            appDelegate?.window?.makeKeyAndVisible()
            navigationRegisterDeviceViewController?.view.alpha = 0.0
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                
                navigationRegisterDeviceViewController?.view.alpha = 1.0
            })
            
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you want to logout ?".localizedString(), message: "", actions: [yesAction,cancelAction])
    }
    
    func saveAPIURL() {
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Edit", message: "Change API URL", preferredStyle: .alert)
        //2. Add the text field. You can configure it however you need.
        alert.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = ""
            textField.text = ServiceData.sharedInstance.hostAPI
        })
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Save", style: .destructive, handler: { [weak alert] (_) in
            if let textField = alert?.textFields![0] {
                UserDefaults.standard.setValue(textField.text, forKey: "hostAPI")
                (self.textFieldUrlAPIRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FACompress, postfixText: String(format: "  %@", ServiceData.sharedInstance.hostAPI), size: 14, iconSize: 16)
                print("Text field: \(textField.text)")
            }// Force unwrapping because we know it exists.
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel".localizedString(), style: UIAlertActionStyle.default, handler: {[weak self] (UIAlertAction) -> Void in
            print("")
            
        }))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
 
    }
    
    
    
    func buttonUpdateAuthorizePress(sender: UIButton) {
        
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertActionStyle.destructive, handler: {[weak self](UIAlertAction) -> Void in
            self?.updateAuthorizePress()
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        let stringTitle = String(format: "%@ %@ ?", "Would you like to set the permission for".localizedString(), userModel.account)
        self.showAlertWithAction(stringTitle, message: "", actions: [yesAction,cancelAction])
        
    }
    
    func updateAuthorizePress() {
        
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightCrewtaskauthorize(flightId: self.flightId, sourceCrewID: self.sourceCrewID, destinationCrewID: self.destinationCrewID).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            if let message = task as? String {
                
                let cancelAction = UIAlertAction(title: "Close".localizedString(), style: UIAlertActionStyle.cancel, handler: { (UIAlertAction) -> Void in
                    Broadcaster.notify(LeadReportPositionDelegate.self) {
                        $0.refreshAuthorizeLeadReportPosition()
                    }
                    let _ = self.navigationController?.popViewController(animated: true)
                })
                
                self.showAlertWithAction("Alert".localizedString(), message: message, actions: [cancelAction])
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    func buttonUpdateConfirmInfoPress(sender: UIButton) {
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskUserConfirminfo().continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            self.showAlert("Alert".localizedString(), stringContent: "Update success".localizedString())
            self.userModel.isInfoConfirmed = true
            ServiceData.sharedInstance.userModel?.isInfoConfirmed = true
            self.former.removeUpdate(rowFormer: self.updateConfirmInfoTableViewCell)
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    //MARK
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    func showAvatarProfile() {
        let photo = SKPhoto.photoWithImageURL(userModel.imageBase64)
        photo.caption = ""
        let browser = SKPhotoBrowser(originImage: UIImage.init(named: "photo-camera.png")!, photos: [photo], animatedFromView: avatarHeader.view)
        browser.initializePageIndex(0)
        present(browser, animated: true, completion: {})
    }
    
    func imageAvatarTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        self.showAvatarProfile()
    }
    
    func verifyCode(codeScan: String) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskDeviceVerifyQR(qrCode: codeScan).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            if let responseServiceModel = task as? ResponseServiceModel {
                let message = responseServiceModel.message ?? "QR code active accepted full".localizedString()
                self.showAlert("Message".localizedString(), stringContent: message)
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    //
    func showSurveyListArchivePress(_ sender: AnyObject) {
        self.pushSurveyListViewController(scheduleFlyModel: nil, isListSaved: true)
    }

    //
    @IBAction func scanInModalAction(_ sender: AnyObject) {
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate = self
        
        readerVC.completionBlock = {[weak self] (result: QRCodeReaderResult?) in
            guard let `self` = self else {
                return
            }
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
                self.verifyCode(codeScan: result.value)
            }
            
            
        }
        present(readerVC, animated: true, completion: nil)
    }
    
    //MARK - QRCodeReaderViewControllerDelegate
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) { [weak self] in
            print("")
            /*
            let alert = UIAlertController(
                title: "QRCodeReader",
                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self?.present(alert, animated: true, completion: nil)
            */
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    
}
