//
//  WebViewWeeklyReportViewController.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 11/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebKit

class WebViewWeeklyReportViewController: BaseViewController, WKUIDelegate, WKNavigationDelegate {
    
    var webView: WKWebView!
    
    var id: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_send_email").resizeImage(newSize: CGSize(width: 25, height: 25)), style: .done, target: self, action: #selector(sendEmail(_ :)))
    }
    
    @objc func sendEmail(_ sender:Any) {
        let vc = WeeklyReportEmailController(id: id)
        self.present(vc: BasePresentNavigationController(root: vc), transitioning: ScaleUpAnimateTransitionDelegate(), completion: nil)
    }
    
    override func loadView() {
        let webViewConfiguration = WKWebViewConfiguration.init()
        webViewConfiguration.allowsInlineMediaPlayback = true
        webView = WKWebView(frame: .zero, configuration: webViewConfiguration)
        webView.backgroundColor = UIColor.white
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.allowsLinkPreview = true
        webView.scrollView.layer.masksToBounds = false
        webView.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        if #available(iOS 13.0, *) {
            webView.scalesLargeContentImage = true
        } else {
            // Fallback on earlier versions
        }
        self.view = webView
    }
    
    func loadWebView() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskWeeklyReportDetail(id: self.id).continueOnSuccessWith(continuation: { task in
            #if DEBUG
            print("=====taskWeeklyReportDetail======= \n\(task)\n==================")
            #endif
            let result = task as! JSON
            let detail = WeeklyReportModel.init(json: result)
            self.webView.loadHTMLString(detail.Content, baseURL: nil)
            if detail.Attachments.count > 0 {
                let attachmentView = WeeklyReportAttachmentView(frame: .zero)
                self.view.addSubview(attachmentView)
                attachmentView.translatesAutoresizingMaskIntoConstraints = false
                attachmentView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
                self.view.trailingAnchor.constraint(equalTo: attachmentView.trailingAnchor, constant: 0).isActive = true
                if #available(iOS 11.0, *) {
                    self.view.bottomAnchor.constraint(equalTo: attachmentView.bottomAnchor, constant: self.view.safeAreaInsets.bottom).isActive = true
                } else {
                    self.view.bottomAnchor.constraint(equalTo: attachmentView.bottomAnchor, constant: 0).isActive = true
                }
                attachmentView.show(attachments: detail.Attachments)
                attachmentView.onView = {[weak self] attachment in
                    guard let `self` = self, let attachment = attachment,
                          let url = URL(string: attachment.FoFileUrl) else { return }
                    if url.pathExtension.contains("pdf") {
                        let vc = PDFViewController()
                        vc.loadUrl(url: url,title: attachment.OriginalFileName)
                        self.navigationController?.pushViewController(vc, animated: true)
                    } else {
                        let browser = SKPhotoBrowser(photos: [SKPhoto(url: url.absoluteString)], initialPageIndex: 0)
                        self.present(browser, animated: true, completion: nil)
                    }
                }
                self.view.bringSubviewToFront(attachmentView)
                self.webView.scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: attachmentView.frame.height + 80, right: 0)
            }
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideMBProgressHUD(true)
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil || navigationAction.targetFrame?.isMainFrame == false {
            if let urlToLoad = navigationAction.request.url {
                let url = URL(string: urlToLoad.absoluteString)!
                if UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                            print("Open url : \(success)")
                        })
                    } else {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
        return nil
    }
}
