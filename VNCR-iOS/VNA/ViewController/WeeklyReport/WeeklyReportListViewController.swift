//
//  MeetingListViewController.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 11/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class WeeklyReportListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var pageIndexNew: Int = 0
    
    var listOriginal = [WeeklyReportModel]()
    
    var listDepartment = [DepartmentModel]()
    
    var textSearch: String = ""
    
    var refreshControl: UIRefreshControl!
    
    var selectedFilter: String = "ALL"
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView?.addSubview(refreshControl)
            }
            
        }
    }
    
    var listFilterSelected: [FilterModel] = []
    var searchText: String = ""
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Weekly Meeting".localizedString()
        
        tableView.keyboardDismissMode = .onDrag
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        tableView.register(ArticleCell.nib, forCellReuseIdentifier: ArticleCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.delegate = self
        tableView.dataSource = self
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tableView.alwaysBounceVertical = true
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        //self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            self?.loadMoreDataNew() {
                self?.tableView!.finishInfiniteScroll()
            }
            
        }
        self.configSearchBar()
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getDepartmentData() { }
        self.getNewData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.view.backgroundColor = UIColor("#166880")
    }
    
    @objc func refreshData() {
        getNewData()
    }
    
    func getDepartmentData(completion: @escaping () -> Void) {
        ServiceData.sharedInstance.taskWeeklyReportGetDepartments().continueOnSuccessWith(continuation: { task in
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<DepartmentModel>()
                for item in array {
                    let model = DepartmentModel(json: item)
                    arrayModel.append(model)
                }
                self.listDepartment = arrayModel
            }
            completion()
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            completion()
        })
    }
    
    func getNewData() {
        pageIndexNew = 1
        ServiceData.sharedInstance.taskWeeklyReportList(keyword: self.searchText, stringFilter: self.selectedFilter, pageIndex: pageIndexNew).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<WeeklyReportModel>()
                for item in array {
                    let model = WeeklyReportModel(json: item)
                    arrayModel.append(model)
                }
                self.listOriginal = arrayModel
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    func loadMoreDataNew(_ handler: (() -> Void)?) {
        
        if(self.listOriginal.count > 1 && !self.refreshControl.isRefreshing){
            pageIndexNew += 1
            ServiceData.sharedInstance.taskWeeklyReportList(keyword: self.searchText, stringFilter: self.selectedFilter, pageIndex: pageIndexNew).continueOnSuccessWith(continuation: { task in
                let result = task as! JSON
                if let array = result.array, array.count > 0 {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in array {
                        listIndexPath.append(IndexPath(row: self.listOriginal.count, section: 0))
                        let model = WeeklyReportModel(json: item)
                        self.listOriginal.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                handler?()
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                handler?()
            })
            
            
        } else {
            handler?()
        }
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listOriginal.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell")! as! ArticleCell
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "ArticleCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! ArticleCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model: WeeklyReportModel = self.listOriginal[indexPath.row]
        
        self.pushWeeklyReportDetailViewController(id: model.ID, title: model.Title)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func configure(cell: ArticleCell, at indexPath: IndexPath) {
        
        cell.labelContent.numberOfLines = 4
        cell.labelTitle.numberOfLines = 2
        cell.loadContent(model: self.listOriginal[indexPath.row])
        
    }
    
}

extension WeeklyReportListViewController: UISearchBarDelegate {
    
    // MARK: - Config Search Bar
    private func configSearchBar() {
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.placeholder = "Search".localizedString()
        self.searchBar.delegate = self
        self.searchBar.sizeToFit()
        self.searchBar.showsBookmarkButton = true
        self.searchBar.setImage(UIImage(named: "ic_filter")?.resizeImageWith(newSize: CGSize(width: 20, height: 20)).withRenderingMode(.alwaysOriginal), for: .bookmark, state: .normal)
        //self.tableView.tableHeaderView = self.searchBar
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
    }
    
    // MARK: - UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.searchBar.sizeToFit()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        self.searchBar.sizeToFit()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
        self.searchBar.text =  self.searchText
        //filterData(filterString: self.searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if (self.searchBar.text == "" || self.searchBar.text == nil) {
            self.searchText = ""
        }
        //getData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchText = searchBar.text ?? ""
        self.view.endEditing(true)
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if self.listDepartment.count == 0 {
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            self.getDepartmentData() {
                self.didTapFilter(searchBar)
            }
        } else {
            didTapFilter(searchBar)
        }
    }
    
    @objc func didTapFilter(_ sender: UISearchBar) {
        var array = Array<CommonPopoverModel>()
        //array.append(CommonPopoverModel.init("", "All".localizedString(), "All".localizedString(), self.selectedFilter == ""))
        for item in listDepartment {
            array.append(CommonPopoverModel.init(item.Key, item.Key, item.DepartmentName, self.selectedFilter == item.Key))
        }
        
        CommonCategoriesPopover.appearFrom(originView: sender, baseViewController: self, title: "Department".localizedString(), arrayData: array, doneAction: {[weak self] array in
            for item in array {
                if item.IsSelected {
                    self?.selectedFilter = item.Id
                    self?.showMBProgressHUD("Loading...".localizedString(), animated: true)
                    self?.getNewData()
                    break
                }
            }
        }, cancelAction: { print("cancel") },
        modalStyle: .popover,
        arrowDirection: .any)
    }
}

