//
//  WeeklyReportAttachmentView.swift
//  VNA
//
//  Created by Pham Dai on 08/08/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class WeeklyReportAttachmentView: BaseView {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackContainer: UIStackView!
    
    var weeklyReportAttachments:[AttachmentCommonModel] = []
    
    var onView:((AttachmentCommonModel?) ->Void)?

    override func config() {
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    }
    
    func show(attachments:[AttachmentCommonModel]) {
        self.weeklyReportAttachments = attachments.sorted(by: {$0.DisplayOrder < $1.DisplayOrder})
        stackContainer.arrangedSubviews.forEach{
            ($0 as? UIButton)?.removeTarget(self, action: #selector(view(_ :)), for: .touchUpInside)
            ($0 as? UIButton)?.removeFromSuperview()
            
        }
        (stackContainer.arrangedSubviews.first(where: {$0.isKind(of: UILabel.self)}) as? UILabel)?.text = "Attachments(\(weeklyReportAttachments.count)): "
        weeklyReportAttachments.forEach { attachment in
            
            let button = Button10Corner(frame: .zero)
            button.setTitle(attachment.OriginalFileName.trunc(length: 50), for: UIControl.State())
            if let url  = URL(string: attachment.FoFileUrl) {
                if url.pathExtension.contains("pdf") {
                    let image = #imageLiteral(resourceName: "ic_pdf")
                    button.setImage(image, for: UIControl.State())
                } else {
                    let image = #imageLiteral(resourceName: "ic_png")
                    button.setImage(image, for: UIControl.State())
//                    UIImageView().load(urlString: attachment.FoFileUrl) { image in
//                        guard let image = image else {return}
//                        let ratio = image.size.width/image.size.height
//                        button.setImage(image.resizeImage(newSize: CGSize(width: 50*ratio, height: 50)), for: UIControl.State())
//                    }
                }
            }
            button.addTarget(self, action: #selector(view(_ :)), for: .touchUpInside)
            button.object = attachment
            button.contentMode = .scaleAspectFill
            button.setTitleColor(.white, for: UIControl.State())
            button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 15)
            button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: -5)
            stackContainer.addArrangedSubview(button)
            button.backgroundColor = .darkGray
        }
    }
    
    @objc func view(_ sender:Button10Corner) {
        onView?(sender.object as? AttachmentCommonModel)
    }
}
