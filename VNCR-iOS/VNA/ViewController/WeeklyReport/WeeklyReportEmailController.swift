//
//  WeeklyReportEmailController.swift
//  VNA
//
//  Created by Pham Dai on 08/08/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class WeeklyReportEmailController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txvEmail: UITextView10Corner!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var heightEmail: NSLayoutConstraint!
    
    let id:Int
    var tap:UITapGestureRecognizer?
    
    init(id:Int) {
        self.id = id
        super.init(nibName: String(describing: WeeklyReportEmailController.self), bundle: .main)
        self.setCustomPresentationStyle()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.font = .systemFont(ofSize: 14, weight: .regular)
        lblTitle.textColor = .gray
        lblTitle.text = "dùng dấu ';' để ngăn cách giữa các email".localizedString();
        
        txvEmail.font = .systemFont(ofSize: 16)
        txvEmail.isScrollEnabled = false
        txvEmail.placeholder1 = "Enter email";
        txvEmail.keyboardType = .emailAddress
        txvEmail.onTextViewDidChange = {[weak self] textview in
            guard let `self` = self else { return }
            
            var shouldClear = false
            if self.txvEmail.text == "" {
                shouldClear = true
                self.txvEmail.text = " "
            }
            
            let fixedWidth = self.txvEmail.frame.size.width
            let newSize = self.txvEmail.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            self.view.layoutIfNeeded()
            let height = newSize.height + self.txvEmail.contentInset.top + self.txvEmail.contentInset.bottom
            self.txvEmail.isScrollEnabled = height >= self.heightEmail.constant
            self.view.layoutIfNeeded()
            self.updatePreferredSize()
            if shouldClear {self.txvEmail.text = ""}
            
            self.checkValidContent()
        }
        
        btnSend.setTitle("Send".localizedString(), for: UIControl.State())
        btnSend.titleLabel?.font = .systemFont(ofSize: 16)
        btnSend.backgroundColor = .systemBlue
        btnSend.setTitleColor(.white, for: UIControl.State())
        btnSend.setBackgroundImage(UIColor.systemBlue.imageRepresentation, for: .normal)
        btnSend.setBackgroundImage(UIColor.lightGray.imageRepresentation, for: .disabled)
        btnSend.tintColor = .white
        btnSend.isEnabled = false
        
        if self.navigationController != nil && self.splitViewController == nil {
            setBarCloseButton(title: "Email".localizedString())
        } else {
            navigationItem.title = "Email".localizedString()
        }
        
        updatePreferredSize()
        
        tap = UITapGestureRecognizer(target: self, action: #selector(self.hideKeyboard))
        tap?.numberOfTouchesRequired = 1
        tap?.cancelsTouchesInView = true
        view.addGestureRecognizer(tap!)
    }
    
    @objc func hideKeyboard() {
        txvEmail.resignFirstResponder()
    }
    
    func checkValidContent() {
        var valid = true
        let mutil = txvEmail.text.components(separatedBy: ";")
        mutil.forEach { email in
            if email.count > 0 || mutil.count == 1 {
                if !email.isValidEmail() {
                    valid = false
                }
            }
        }
        btnSend.isEnabled = valid
    }
    
    func updatePreferredSize() {
        var size = self.view.systemLayoutSizeFitting(CGSize(width: UIScreen.bounceWindow.width * 0.9, height: UIScreen.bounceWindow.height))
        let max = UIScreen.bounceWindow.height*0.75
        if size.height > max {
            size.height = max
        }
        self.preferredContentSize = size
        self.navigationController?.preferredContentSize = self.preferredContentSize
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.navigationController?.view.roundCornersFull(corners: [.topLeft,.topRight,.bottomLeft,.bottomRight], radius: 10)
    }
    
    @IBAction func send(_ sender: Any) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        
        ServiceData.sharedInstance.taskSendEmailWeeklyReport(emails: txvEmail.text, ID: id).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            self.showAlert("Email", message: "Email sent successfully".localizedString(),
                           buttons: [
                            "OK".localizedString()
                           ]) { action, index in
                self.dismiss(animated: true, completion: nil)
            }
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
}
