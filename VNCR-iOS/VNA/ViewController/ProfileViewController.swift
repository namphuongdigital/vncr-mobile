//
//  ProfileViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/12/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProfileViewController: FormViewController {
    
    var flightId: Int = 0
    
    var sourceCrewID: String = ""
    
    var destinationCrewID: String = ""
    
    var permission: PermissionCrewTaskType = .read
    
    var crewId: String = ""
    
    var userModel: UserModel!
    
    var textFieldFullnameRowFormer:RowFormer!
    
    var textFieldUsernameRowFormer:RowFormer!
    
    var textFieldEmailRowFormer:RowFormer!
    
    var textFieldDOBRowFormer:RowFormer!
    
    var textFieldGenderRowFormer: RowFormer!
    
    var textFieldCodeRowFormer:RowFormer!
    
    var textFieldPPRowFormer:RowFormer!
    
    var updateAuthorizeTableViewCell: CustomRowFormer<UpdateReportTableViewCell>!
    
    var updateConfirmInfoTableViewCell: CustomRowFormer<UpdateReportTableViewCell>!
    
    fileprivate var avatarHeader:CustomViewFormer<FormHeaderFooterView>!
    
    var heightHeaderAvatar: CGFloat = 170
    
    var refreshControl: UIRefreshControl!
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView.addSubview(refreshControl)
            }
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.navigationItem.title = "PROFILE".localizedString()
        if(crewId == "") {
            self.userModel = ServiceData.sharedInstance.userModel!
            self.loadViewData()
        } else {
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskUserGetinfobycrewid(crewId: self.crewId).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    let userModel = UserModel(json: result)
                    self.navigationItem.title = userModel.account
                    self.userModel = userModel
                    DispatchQueue.main.async {
                        self.loadViewData()
                        self.tableView.reloadData()
                    }
                }
                
                self.hideMBProgressHUD(true)
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
    }
    
    @objc func refreshData() {
        if(crewId == "") {
            if(self.userModel.userId == 0) {
                ServiceData.sharedInstance.taskShowFormRegisterDevice().continueOnSuccessWith(continuation: { task in
                    self.userModel = ServiceData.sharedInstance.userModel!
                    self.former.removeAll()
                    self.loadViewData()
                    self.former.reload()
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                    
                    
                }).continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    self.hideMBProgressHUD(true)
                    self.refreshControl.endRefreshing()
                })
            } else {
                self.hideMBProgressHUD(true)
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
            
        } else {
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskUserGetinfobycrewid(crewId: self.crewId).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    let userModel = UserModel(json: result)
                    self.navigationItem.title = userModel.account
                    self.userModel = userModel
                    DispatchQueue.main.async {
                        self.loadViewData()
                        self.tableView.reloadData()
                    }
                }
                
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            })
        }
    }
    
    func loadViewData() {
        //
        let createMenu: ((String, _ textColor:UIColor?, _ isDisclosureIndicator:Bool, (() -> Void)?) -> RowFormer) = { text, color, isDisclosure,  onSelected in
            return LabelRowFormer<FormLabelCell>() {
                if(color == nil){
                    $0.titleLabel.textColor = UIColor.black
                } else {
                    $0.titleLabel.textColor = color
                }
                
                $0.titleLabel.font = UIFont.systemFont(ofSize: 15)
                $0.textLabel?.font = UIFont.systemFont(ofSize: 15)
                if(isDisclosure){
                    $0.accessoryType = .disclosureIndicator
                } else {
                    $0.accessoryType = .none
                }
                
                }.configure {
                    $0.text = text
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        var nibNameUpdateAuthorizeTableViewCell = "UpdateReportTableViewCell"
        
        //if(UIDevice.current.userInterfaceIdiom == .phone) {
            heightHeaderAvatar = 140
            nibNameUpdateAuthorizeTableViewCell = String(format: "%@_iPhone", nibNameUpdateAuthorizeTableViewCell)
        //}//
        
        self.tableView.separatorInset = UIEdgeInsets(top: self.tableView.separatorInset.top, left: 0, bottom: self.tableView.separatorInset.bottom, right: 0)
        self.tableView.backgroundColor = UIColor("#fafafa")
        //self.tableView.frame = CGRect(x: (self.tableView.frame.size.width - 320)/2.0, y: 0, width: 320, height: self.tableView.frame.size.height)
        
        
        textFieldFullnameRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldFullnameRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldFullnameRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAUser, postfixText: String(format: "  %@", self.userModel.nameTV), size: 14, iconSize: 16)
        
        textFieldUsernameRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldUsernameRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldUsernameRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAAt, postfixText: String(format: "  %@", self.userModel.account), size: 14, iconSize: 16)
        
        
        textFieldCodeRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldCodeRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldCodeRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FACode, postfixText: String(format: "  %@", self.userModel.codeTV), size: 14, iconSize: 16)
        
        textFieldEmailRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldEmailRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldEmailRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAAsterisk, postfixText: String(format: "  %@", self.userModel.mainBase), size: 14, iconSize: 16)
        
        textFieldDOBRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldDOBRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldDOBRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAGift, postfixText: String(format: "  %@", self.userModel.dob), size: 14, iconSize: 16)
        
        textFieldGenderRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldGenderRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldGenderRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FATransgender, postfixText: String(format: "  %@", self.userModel.gender == .male ? "Male".localizedString() : "Female".localizedString()), size: 14, iconSize: 16)
        
        
        textFieldPPRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        
        textFieldPPRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldPPRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldPPRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FACreditCard, postfixText: String(format: "  %@", self.userModel.pPortNo), size: 14, iconSize: 16)
        
     
        let textFieldPhoneRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
            if(self?.userModel.userId != ServiceData.sharedInstance.userId) {
                self?.callNumber(phoneNumber: self?.userModel.phone)
            }
            
        }
        (textFieldPhoneRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (textFieldPhoneRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAPhoneSquare, postfixText: String(format: "  %@", self.userModel.phone), size: 14, iconSize: 16)
        
        
        // Create Headers
        
        let createHeader: ((String) -> ViewFormer) = { text in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor("#fafafa")
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
  
                }.configure {
                    $0.viewHeight = 50
                    $0.text = text
                    
            }
        }
        
        
        avatarHeader = CustomViewFormer<FormHeaderFooterView>() { [weak self] (formHeaderFooterView) in
            guard let `self` = self else {
                return
            }
            let imageView = UIImageViewProgress(isProgressBar: false)
            imageView.contentMode = .scaleAspectFit
            imageView.tag = 10
            imageView.frame =  CGRect(x: (UIScreen.main.bounds.width - 120) / 2.0, y: 10, width: 120, height: 120)
            //imageView.layer.backgroundColor = UIColor.init(white: 0.886, alpha: 1.000).cgColor
            imageView.layer.cornerRadius = 2.5//imageView.frame.size.height / CGFloat(2.0)
            imageView.layer.borderWidth = 0.5
            imageView.layer.borderColor = UIColor.init(white: 0.837, alpha: 1.000).cgColor
            imageView.clipsToBounds = true
            imageView.loadImageNoProgressBar(url: URL(string: self.userModel.imageBase64 ))
            imageView.isUserInteractionEnabled = true
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageAvatarTapped(tapGestureRecognizer:)))
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(tapGestureRecognizer)
            
            formHeaderFooterView.contentView.addSubview(imageView)
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.heightAnchor.constraint(equalToConstant: 120).isActive = true
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1).isActive = true
            imageView.centerXAnchor.constraint(equalTo: formHeaderFooterView.centerXAnchor).isActive = true
            imageView.centerYAnchor.constraint(equalTo: formHeaderFooterView.centerYAnchor).isActive = true
            
            formHeaderFooterView.contentView.backgroundColor = UIColor("#fafafa")
            }.configure {
                $0.viewHeight = heightHeaderAvatar
        }
        
        
        
        updateAuthorizeTableViewCell = CustomRowFormer<UpdateReportTableViewCell>(instantiateType: .Nib(nibName: nibNameUpdateAuthorizeTableViewCell)) {[weak self] in
            print($0.textLabel?.text)
            $0.buttonUpdate.setTitle("Set Authorize".localizedString(), for: UIControl.State())
            $0.buttonUpdate.addTarget(self, action: #selector(self?.buttonUpdateAuthorizePress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        updateConfirmInfoTableViewCell = CustomRowFormer<UpdateReportTableViewCell>(instantiateType: .Nib(nibName: nibNameUpdateAuthorizeTableViewCell)) {[weak self] in
            print($0.textLabel?.text)
            $0.buttonUpdate.setTitle("Confirm Info".localizedString(), for: UIControl.State())
            $0.buttonUpdate.addTarget(self, action: #selector(self?.buttonUpdateConfirmInfoPress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        
        var imageSection: SectionFormer
        var listRowFormer = [RowFormer]()
        listRowFormer.append(textFieldPhoneRowFormer)
        listRowFormer.append(textFieldFullnameRowFormer)
        listRowFormer.append(textFieldUsernameRowFormer)
        listRowFormer.append(textFieldCodeRowFormer)
        listRowFormer.append(textFieldEmailRowFormer)
        listRowFormer.append(textFieldDOBRowFormer)
        listRowFormer.append(textFieldGenderRowFormer)
        listRowFormer.append(textFieldPPRowFormer)
        
        if(ServiceData.sharedInstance.userId == self.userModel.userId) {
            if(self.userModel.isInfoConfirmed == false) {
                listRowFormer.append(updateConfirmInfoTableViewCell)
            }
        } else {
            if(permission == .read) {
                
            } else {
                listRowFormer.append(updateAuthorizeTableViewCell)
            }
        }
        imageSection = SectionFormer(rowFormers: listRowFormer).set(headerViewFormer: avatarHeader)
        
        former.append(sectionFormer: imageSection)
            .onCellSelected { [weak self] _ in
                //self?.formerInputAccessoryView.update()
        }
        
        
        UITextField.appearance().tintColor = UIColor.black
        UITextView.appearance().tintColor = UIColor.black
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    @objc func buttonUpdateAuthorizePress(sender: UIButton) {
        
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self](UIAlertAction) -> Void in
            self?.updateAuthorizePress()
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        let stringTitle = String(format: "%@ %@ ?", "Would you like to set the permission for".localizedString(), userModel.account)
        self.showAlertWithAction(stringTitle, message: "", actions: [yesAction,cancelAction])
        
    }
    
    func updateAuthorizePress() {
        
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightCrewtaskauthorize(flightId: self.flightId, sourceCrewID: self.sourceCrewID, destinationCrewID: self.destinationCrewID).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            if let message = task as? String {
                
                let cancelAction = UIAlertAction(title: "Close".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
                    Broadcaster.notify(LeadReportPositionDelegate.self) {
                        $0.refreshAuthorizeLeadReportPosition()
                    }
                    let _ = self.navigationController?.popViewController(animated: true)
                })
                
                self.showAlertWithAction("Alert".localizedString(), message: message, actions: [cancelAction])
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    @objc func buttonUpdateConfirmInfoPress(sender: UIButton) {
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskUserConfirminfo().continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            self.showAlert("Alert".localizedString(), stringContent: "Update success".localizedString())
            self.userModel.isInfoConfirmed = true
            ServiceData.sharedInstance.userModel?.isInfoConfirmed = true
            self.former.removeUpdate(rowFormer: self.updateConfirmInfoTableViewCell)
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    func showAvatarProfile() {
        let photo = SKPhoto.photoWithImageURL(userModel.imageBase64)
        photo.caption = ""
        let browser = SKPhotoBrowser(photos: [photo], initialPageIndex: 0)
        present(browser, animated: true, completion: {})
    }
    
    @objc func imageAvatarTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        self.showAvatarProfile()
    }
    
    
}
