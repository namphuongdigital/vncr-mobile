//
//  CreateFormViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/14/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import UIImage_ImageCompress
import SwiftyJSON
import STPopup
import Photos
import PhotosUI

class CreateFormViewController: FormViewController, UITextFieldDelegate, ListImagePostTableViewCellDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, EditContentViewControllerDelegate, PopupNoteViewControllerDelagate, FormWorkflowCellDelegate, PopupApprovalViewControllerDelagate {
    
    var imagesWaitUpload:[UIImage] = [] // truong hop tao moi, stored khi create form thanh cong se upload len server
    
    let imagePicker = UIImagePickerController()
    
    var formModel: FormModel!
    
    var categoriesFormTableViewCell: CustomRowFormer<DynamicHeightCell>!
    
    var titleTableViewCell: CustomRowFormer<DynamicHeightCell>!
    
    var fromDateTableViewCell: CustomRowFormer<DynamicHeightCell>!
    
    var toDateTableViewCell: CustomRowFormer<DynamicHeightCell>!
    
    var descriptionTableViewCell: CustomRowFormer<DynamicHeightCell>!
    
    var termsOfConditionsCell: CustomViewFormer<TermsOfConditionsCell>?
    
    var sectionFormer: SectionFormer!
    
    var updateReportTableViewCell: CustomRowFormer<UpdateReportTableViewCell>!
    
    var textTitleForm: String = ""
    
    var fromDate: Date?
    
    var toDate: Date?
    
    var textDescription: String = ""
    
    var textCategoryName: String = ""
    
    var category: FormCategoryModel = FormCategoryModel()
    
    var sendBarButtonItem: UIBarButtonItem!
    var menuBarButtonItem: UIBarButtonItem!

    weak var selectedImagePost: ImagePost?
    
    var listFormCategory = [FormCategoryModel]()
    
    var isEditTitleForm: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        sendBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.sendBarButtonItemPressed(_:)))
        sendBarButtonItem.setFAIcon(icon: .FASendO, iconSize: 20)
        
        //add menu items
        //menuBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.menuBarButtonItemPressed(_:)))
        //let buttonMenuAction = UIButton(frame: CGRect(x: 0, y: 0, width: 24, height: 24))
        //buttonMenuAction.setBackgroundImage(UIImage(named: "ic_menu_verticle")!.resizeImageWith(newSize: CGSize(width: 20, height: 20)).tint(with: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)), for: .normal)
        //buttonMenuAction.addTarget(self, action: #selector(self.menuBarButtonItemPressed), for: .touchUpInside)
        //menuBarButtonItem = UIBarButtonItem(customView: buttonMenuAction)
        
        menuBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.menuBarButtonItemPressed(_:)))
        menuBarButtonItem.setFAIcon(icon: .FATasks, iconSize: 20)
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.loadData()
//        ServiceData.sharedInstance.taskFormDetail(id: formId).continueOnSuccessWith(continuation: { task in
//            if let result = task as? JSON {
//                let formModel = FormModel(json: result)
//                self.formModel = formModel
//            }
//            self.loadData()
//        }).continueOnErrorWith(continuation: { error in
//            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
//            self.hideMBProgressHUD(true)
//        })
        
        Broadcaster.register(FormWorkflowCellDelegate.self, observer: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func sendBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if(self.formModel?.id == 0) {
            if (termsOfConditionsCell?.viewInstance as? TermsOfConditionsCell)?.isAccepted == false {
                self.showAlert("Warning".localizedString(), stringContent: "Bạn chưa chấp nhận với quy định ĐTV hoặc terms and conditions\nAccept 'quy định ĐTV' & 'Terms and Conditions' are required")
                return
            }
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            buttonCreateFormPress(sender: nil)
        } else {
            self.showMBProgressHUD("Updating...".localizedString(), animated: true)
            buttonUpdateFormPress(sender: nil)
        }
    }
    
    @objc func menuBarButtonItemPressed(_ sender: UIBarButtonItem) {
        var actions: [UIAlertAction] = []
        if self.formModel.isNeedApprove {
            actions.append(UIAlertAction(title: "Approve".localizedString(), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                self.showNoteBarButtonItemPressed(model: self.formModel, status: Common.FormStatus.Accepted, buttonText: "Approve".localizedString())
            })
            actions.append(UIAlertAction(title: "Reject".localizedString(), style: UIAlertAction.Style.destructive) { (result : UIAlertAction) -> Void in
                self.showNoteBarButtonItemPressed(model: self.formModel, status: Common.FormStatus.Rejected, buttonText: "Reject".localizedString())
            })
        }
        actions.append(UIAlertAction(title: "Workflows".localizedString(), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.pushFormWorkflowViewController(formModel: self.formModel)
        })
//        actions.append(UIAlertAction(title: "History".localizedString(), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
//            self.pushFormDetailViewController(cid: self.formModel.cid)
//        })
        if !self.formModel.isNeedApprove {
            self.pushFormWorkflowViewController(formModel: self.formModel)
        } else {
            actions.append(UIAlertAction(title: "Close".localizedString(), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in })
            self.showActionSheetWithAction(self.textTitle, message: "What would you like to do?".localizedString(), sourceView: self.view, actions: actions)
        }
    }
    
    @objc func buttonAcceptPress(sender: UIButton) {
        self.showNoteBarButtonItemPressed(model: self.formModel, status: Common.FormStatus.Accepted, buttonText: "Approve".localizedString())
    }
    
    @objc func buttonRejectPress(sender: UIButton) {
        self.showNoteBarButtonItemPressed(model: self.formModel, status: Common.FormStatus.Rejected, buttonText: "Reject".localizedString())
    }
    
    func loadViewData() {
        //self.formModel.isReadonly = true
        self.textTitleForm = self.formModel.title
        self.textDescription = self.formModel.descriptionContent
        self.fromDate = self.formModel.fromDate
        self.toDate = self.formModel.toDate
        self.category.id = self.formModel.categoryID
        self.category.name = self.formModel.categoryName
        // Do any additional setup after loading the view.
        
        var formId = 0
        if(self.formModel?.id == 0) {
            self.textTitle = "CREATE FORM".localizedString()
        } else {
            formId = self.formModel.id
            //self.textTitle = "FORM".localizedString()
            self.textTitle = "@\(self.formModel.cName.isEmpty ? self.formModel.cid : self.formModel.cName)"
        }
        
        var nibNameInfoReportTableViewCell = "InfoReportTableViewCell"
        var nibNameMainPilotReportTableViewCell = "MainPilotReportTableViewCell"
        var nibNameSecondPilotReportTableViewCell = "SecondPilotReportTableViewCell"
        var nibNameEmergencyReportTableViewCell = "EmergencyReportTableViewCell"
        var nibNameCategoriesReportTableViewCell = "CategoriesReportTableViewCell"
        var nibNameContentReportTableViewCell = "ContentReportTableViewCell"
        var nibNameUpdateReportTableViewCell = "UpdateReportTableViewCell"
        
        //if(UIDevice.current.userInterfaceIdiom == .phone) {
        nibNameInfoReportTableViewCell = String(format: "%@_iPhone", nibNameInfoReportTableViewCell)
        nibNameMainPilotReportTableViewCell = String(format: "%@_iPhone", nibNameMainPilotReportTableViewCell)
        nibNameSecondPilotReportTableViewCell = String(format: "%@_iPhone", nibNameSecondPilotReportTableViewCell)
        nibNameEmergencyReportTableViewCell = String(format: "%@_iPhone", nibNameEmergencyReportTableViewCell)
        nibNameCategoriesReportTableViewCell = String(format: "%@_iPhone", nibNameCategoriesReportTableViewCell)
        nibNameContentReportTableViewCell = String(format: "%@_iPhone", nibNameContentReportTableViewCell)
        nibNameUpdateReportTableViewCell = String(format: "%@_iPhone", nibNameUpdateReportTableViewCell)
        //}
        
        categoriesFormTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {[weak self] in
            guard let `self` = self else {
                return
            }
            $0.title = "Type".localizedString()
            $0.clearButton.isHidden = true
            var stringName = self.category.name
            if(stringName.isEmpty) {
                stringName = "\n\n"
            }
            $0.body = stringName
            //$0.accessoryType = .disclosureIndicator
            }.configure {
                $0.rowHeight = UITableView.automaticDimension
            }.onSelected({[weak self] (row) in
                print("")
                self?.buttonSelectCategoriesPress(sender: row.cell)
            })
        
        fromDateTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {[weak self] in
            $0.title = "From date".localizedString()
            $0.body = self?.fromDate?.dateTimeToYYYYMMdd
            $0.clearButton.addTarget(self, action: #selector(self?.clearFromDateButton), for: UIControl.Event.touchUpInside)
            if(self?.formModel.isReadonly == true) {
                $0.clearButton.isHidden = true
            }
            //$0.accessoryType = .disclosureIndicator
            }.configure {
                $0.rowHeight = UITableView.automaticDimension
            }.onSelected({[weak self] (row) in
                print("")
                self?.selectFromDatePress()
            })
        
        toDateTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {[weak self] in
            $0.title = "To date".localizedString()
            $0.body = self?.toDate?.dateTimeToYYYYMMdd
            $0.clearButton.addTarget(self, action: #selector(self?.clearToDateButton), for: UIControl.Event.touchUpInside)
            if(self?.formModel.isReadonly == true) {
                $0.clearButton.isHidden = true
            }
            //$0.accessoryType = .disclosureIndicator
            }.configure {
                $0.rowHeight = UITableView.automaticDimension
            }.onSelected({[weak self] (row) in
                print("")
                self?.selectToDatePress()
            })
        
        titleTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {[weak self] in
            $0.title = "Title".localizedString()
            $0.body = self?.textTitleForm
            $0.clearButton.isHidden = true
            //$0.accessoryType = .disclosureIndicator
            }.configure {
                $0.rowHeight = UITableView.automaticDimension
            }.onSelected({[weak self] (row) in
                print("")
                self?.selectTitlePress()
            })
        
        descriptionTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {[weak self] in
            $0.title = "Reason".localizedString()
            $0.body = self?.textDescription
            $0.clearButton.isHidden = true
            //$0.accessoryType = .disclosureIndicator
            }.configure {
                $0.rowHeight = UITableView.automaticDimension
            }.onSelected({[weak self] (row) in
                print("")
                self?.selectContentPress()
            })
        
        var array = Array<ImagePost>()
        if let attachments = self.formModel?.attachments {
            array.append(contentsOf: attachments.compactMap({ model in
                let image = ImagePost(named: "")
                image.imageUrl = model.thumbnailPath
                image.imageFullUrl = model.downloadPath
                image.isImagePost = true
                image.isHiddenButtonDelete = self.formModel!.isReadonly
                return image
            }))
        }
        
        let listImagePostTableViewCell = CustomViewFormer<ListImagePostTableViewCell>(instantiateType: .Nib(nibName: "ListImagePostTableViewCell")) {
            $0.loadContent(listImagePost: array)
            $0.delegate = self
            }.configure { view in
                view.viewHeight = 100
        }
        
        let empety = CustomRowFormer<InfoReportTableViewCell>(instantiateType: .Nib(nibName: nibNameInfoReportTableViewCell)) {
            print($0.labelC)
        }.configure {
            $0.rowHeight = 0
        }
        
        sectionFormer = SectionFormer(rowFormer: empety).set(headerViewFormer: listImagePostTableViewCell)
        sectionFormer.remove(atIndex: 0)
        
        updateReportTableViewCell = CustomRowFormer<UpdateReportTableViewCell>(instantiateType: .Nib(nibName: nibNameUpdateReportTableViewCell)) {[weak self] in
            print($0.textLabel?.text ?? "")
            $0.buttonUpdate.addTarget(self, action: #selector(self?.buttonUpdateFormPress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        let createHeader: ((String, _ height: CGFloat) -> ViewFormer) = { text, height in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor.clear
                $0.titleLabel.font = UIFont.systemFont(ofSize: 14)
                }.configure {
                    $0.viewHeight = height
                    $0.text = text
                    
            }
        }
        
        let customRowFormerSection = SectionFormer(rowFormer: categoriesFormTableViewCell, fromDateTableViewCell, toDateTableViewCell, descriptionTableViewCell).set(headerViewFormer: createHeader("", 0))
        former.append(sectionFormer: customRowFormerSection, sectionFormer)
        
        if formId != 0 {

            let workFlows = self.formModel?.workFlows ?? []
            
            //workflows
            let listWorkflowTableViewCell = CustomRowFormer<ListFormWorkflowTableViewCell>(instantiateType: .Nib(nibName: "ListFormWorkflowTableViewCell")) {
                $0.setData(workFlows)
            }.configure {
                if workFlows.count == 0 {
                    $0.rowHeight = 50
                } else {
                    $0.rowHeight = CGFloat((workFlows.count) * 70) + 10
                }
            }
            //sectionFormer = SectionFormer(rowFormer: listWorkflowTableViewCell).set(headerViewFormer: listImagePostTableViewCell)
            let approvalSections = SectionFormer(rowFormer: listWorkflowTableViewCell)
            if self.formModel.approvalFromDate != nil {
                //approval data
                let approvalFromDateTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {[weak self] in
                    $0.title = "Approval from".localizedString()
                    $0.body = self?.formModel.approvalFromDate?.dateTimeToYYYYMMdd
                    $0.clearButton.isHidden = true
                    $0.titleFont = UIFont.systemFont(ofSize: 12, weight: .light)
                    $0.titleColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
                    $0.bodyColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                }.configure {
                    $0.rowHeight = UITableView.automaticDimension
                }.onSelected({[weak self] (row) in
                    print("")
                    //self?.selectFromDatePress()
                })
                let approvalToDateTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {[weak self] in
                    $0.title = "Approval to".localizedString()
                    $0.body = self?.formModel.approvalToDate?.dateTimeToYYYYMMdd
                    $0.clearButton.isHidden = true
                    $0.titleFont = UIFont.systemFont(ofSize: 12, weight: .light)
                    $0.titleColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
                    $0.bodyColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                }.configure {
                    $0.rowHeight = UITableView.automaticDimension
                }.onSelected({[weak self] (row) in
                    print("")
                    //self?.selectToDatePress()
                })
                
                let approvalRemarkTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {[weak self] in
                    $0.title = "Important comments".localizedString()
                    $0.body = self?.formModel.approvalRemark
                    $0.clearButton.isHidden = true
                    $0.titleFont = UIFont.systemFont(ofSize: 12, weight: .light)
                    $0.titleColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
                    $0.bodyColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                }.configure {
                    $0.rowHeight = UITableView.automaticDimension
                }.onSelected({[weak self] (row) in
                    print("")
                    //self?.selectContentPress()
                })
                
                approvalSections.append(rowFormer: approvalFromDateTableViewCell, approvalToDateTableViewCell, approvalRemarkTableViewCell)
            }
            
            let workflowFormerSection = approvalSections.set(headerViewFormer: LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor.white
                $0.titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
                $0.titleLabel.textColor = #colorLiteral(red: 0.09803921569, green: 0.7098039216, blue: 0.9960784314, alpha: 1)
            }.configure {
                $0.viewHeight = 40
                $0.text = "Manager/Supervisor Approval".localizedString()
            })//createHeader(, 40))
            former.append(sectionFormer: workflowFormerSection)
            
        }
        
        if formId != 0 && (self.formModel?.isNeedApprove ?? false) {
            let buttonAcceptRow = CustomRowFormer<ButtonActionTableViewCell>(instantiateType: .Nib(nibName: "ButtonActionTableViewCell")) {[weak self] in
                print($0.textLabel?.text ?? "")
                $0.buttonAction.backgroundColor = UIColor("#388E3C")
                $0.buttonAction.setTitle("Approve".localizedString(), for: UIControl.State())
                $0.buttonAction.addTarget(self, action: #selector(self?.buttonAcceptPress(sender:)), for: .touchUpInside)
            }.configure {
                $0.rowHeight = 60
            }
            
            let buttonRejectRow = CustomRowFormer<ButtonActionTableViewCell>(instantiateType: .Nib(nibName: "ButtonActionTableViewCell")) {[weak self] in
                print($0.textLabel?.text ?? "")
                $0.buttonAction.backgroundColor = UIColor("#DA0019")
                $0.buttonAction.setTitle("Reject".localizedString(), for: UIControl.State())
                $0.buttonAction.addTarget(self, action: #selector(self?.buttonRejectPress(sender:)), for: .touchUpInside)
            }.configure {
                $0.rowHeight = 60
            }
            let buttonRowFormerSection = SectionFormer(rowFormer: buttonAcceptRow, buttonRejectRow).set(headerViewFormer: createHeader("", 0))
            former.append(sectionFormer: buttonRowFormerSection)
        } else {
            if formId == 0 && !self.formModel.isReadonly {
                termsOfConditionsCell = CustomViewFormer<TermsOfConditionsCell>(instantiateType: .Nib(nibName: "TermsOfConditionsCell")) {[weak self] cell in
                }.configure(handler: { view in
                    view.viewHeight = isIpad ? 50 : 50
                    (view.viewInstance as? TermsOfConditionsCell)?.onPresentTermsAndConditions = {[weak self] in guard let `self` = self else { return }
                        let viewcontroller = TermsAndConditionsViewController()
                        viewcontroller.textTitle = "Terms And Conditions".localizedString()
                        viewcontroller.title = "Terms And Conditions".localizedString()
                        self.navigationController?.pushViewController(viewcontroller, animated: true)
                    }
                })
                let sectionFormer1 = SectionFormer(rowFormer: empety).set(headerViewFormer: termsOfConditionsCell!)
                former.append(sectionFormer: sectionFormer1)
            }
        }
        
        self.tableView.separatorStyle = .none
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.01))
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        
        if formId != 0 {
            sendBarButtonItem.setFAIcon(icon: .FASave, iconSize: 20)
        } else {
            sendBarButtonItem.setFAIcon(icon: .FASendO, iconSize: 20)
        }
        
        var buttonItems: [UIBarButtonItem] = []
        if formId == 0 {
            buttonItems.append(self.sendBarButtonItem)
        } else if !self.formModel.isReadonly && formId != 0 && self.formModel.cid == ServiceData.sharedInstance.userModel?.crewID ?? "" {
            buttonItems.append(self.sendBarButtonItem)
        }
        
//        if formId != 0 {
//            buttonItems.append(self.menuBarButtonItem)
//        }
        
        self.navigationItem.rightBarButtonItems = buttonItems
    }
    
    func refreshBarItems() {
        if parent != nil {
            self.parent?.navigationItem.titleView = nil
            
            var formID = 0
            if formModel != nil {
                formID = formModel.id
            } else {
                formID = 0
            }
            
            var buttonItems: [UIBarButtonItem] = []
            if formID == 0 {
                buttonItems.append(self.sendBarButtonItem)
            } else if !self.formModel.isReadonly && formID != 0 && self.formModel.cid == ServiceData.sharedInstance.userModel?.crewID ?? "" {
                buttonItems.append(self.sendBarButtonItem)
            }
            
            if formID != 0 {
//                buttonItems.append(self.menuBarButtonItem)
            }
            
            if(formID == 0) {
                self.textTitle = "CREATE FORM".localizedString()
            } else {
                //self.textTitle = "FORM".localizedString()
                self.textTitle = "\(self.formModel.cName.isEmpty ? self.formModel.cid : self.formModel.cName)"
            }
            self.parent?.navigationItem.title = textTitle
            self.parent?.navigationItem.rightBarButtonItems = buttonItems
        }
    }
    
    func loadData() {
        //self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        if(formModel == nil) {
            ServiceData.sharedInstance.taskFormCategory().continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    for item in result.array! {
                        let model = FormCategoryModel(json: item)
                        self.listFormCategory.append(model)
                    }
                    ServiceData.sharedInstance.taskFormCreate().continueOnSuccessWith(continuation: { task in
                        if let result = task as? JSON {
                            //let formModel = FormModel(json: result)
                            self.formModel = FormModel(json: result)
                            self.loadViewData()
                        }
                        self.hideMBProgressHUD(true)
                    }).continueOnErrorWith(continuation: { error in
                        self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                        self.hideMBProgressHUD(true)
                    })
                }
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
            
        } else {
            ServiceData.sharedInstance.taskFormCategory().continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    for item in result.array! {
                        let model = FormCategoryModel(json: item)
                        self.listFormCategory.append(model)
                    }
                    ServiceData.sharedInstance.taskFormDetail(id: self.formModel.id).continueOnSuccessWith(continuation: { task in
                        if let result = task as? JSON {
                            self.formModel = FormModel(json: result)
                        }
                        self.loadViewData()
                        self.hideMBProgressHUD(true)
                    }).continueOnErrorWith(continuation: { error in
                        self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                        self.hideMBProgressHUD(true)
                    })
                }
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
        }
    }
    
    func uploadImage(userId:Int,token:String,image: UIImage, attachmentGroupID: Int, id: Int,_ completed:((Any?)->Void)?) {
        // neu trong upload mutil images thi khong can show HUD
        if completed == nil {
            self.showMBProgressHUD("Uploading...".localizedString(), animated: true)
        }
        ServiceData.sharedInstance.taskFormUploadFile(userId:userId,token: token, formID: id, fileName: "image-form-\(id).jpg", image: image, attachmentGroupId: attachmentGroupID).continueOnSuccessWith(continuation: { task in
            if let attachmentModel = task as? AttachmentModel {
                self.formModel?.attachmentGroupID = attachmentModel.attachmentID
                self.formModel?.attachments.append(attachmentModel)
                if completed == nil {
                    self.hideMBProgressHUD(true)
                }
                if let cell = self.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                    if completed == nil {
                        self.selectedImagePost?.image = image
                        self.selectedImagePost?.isImagePost = true
                        cell.reloadData()
                    }
                }
            }
            completed?(nil)
        }).continueOnErrorWith(continuation: { error in
            if completed != nil {
                completed?(error)
            } else {
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            }
        })
        
    }
    
    func buttonCreateFormPress(sender: UIButton?) {
        self.createForm()
    }
    
    func createForm(isPopToRoot: Bool = true) {
        self.updateForm()
    }
    
    @objc func buttonUpdateFormPress(sender: UIButton?) {
        self.updateForm()
    }
    
    func updateForm(isPopToRoot: Bool = false) {
        guard let formModel = self.formModel else {
            return
        }
        formModel.title = self.textTitleForm
        formModel.descriptionContent = self.textDescription
        formModel.fromDate = self.fromDate
        formModel.toDate = self.toDate
        formModel.categoryID = category.id
        formModel.categoryName = category.name
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFormUpdate(formModel: formModel).continueOnSuccessWith(continuation: { task in
            
            if let result = task as? JSON {
                let formModel = FormModel(json: result)
                self.formModel = formModel
                
                // upload file neu co files can upload
                // remove file wait upload
                self.uploadImages(images: self.imagesWaitUpload)
                self.imagesWaitUpload = []
                
                self.loadData()
                self.refreshBarItems()
                Broadcaster.notify(UpdateFormDelegate.self) {
                    $0.refreshForm(formModel: formModel)
                }
                
            }
            self.hideMBProgressHUD(true)
            if(isPopToRoot == true) {
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        
    }
    
    func uploadImages(images:[UIImage]) {
        
        if images.count == 0 {return}
        
        guard let userId = ServiceData.sharedInstance.userId, let token = ServiceData.sharedInstance.token else {return}
        
        var errorUploads:Set<String> = []
        let queue = OperationQueue()
        self.showMBProgressHUD("Uploading...".localizedString(), animated: true)
        
        var operations:[AsyncBlockOperation] = []
        images.enumerated().forEach { i,pickedImage in
            let task = AsyncBlockOperation {[weak self] op in
                guard let _self = self else {
                    op.complete()
                    return
                }
                self?.uploadImage(userId:userId,token: token, image: UIImage.compressImage(pickedImage, compressRatio: 0.7), attachmentGroupID: _self.formModel.attachmentGroupID, id: self?.formModel?.id ?? 0) { error in
                    if let er = error as? NSError {
                        errorUploads.insert(er.localizedDescription)
                    }
                    op.complete()
                }
            }
            if let t = operations.last {
                task.addDependency(t)
            }
            operations.append(task)
            if i == images.count - 1 {
                task.completionBlock = {[weak self] in
                    guard let _self = self else {return}
                    DispatchQueue.main.async {
                        self?.hideMBProgressHUD(true)
                        if let cell = self?.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell,
                           let attachments = self?.formModel?.attachments {
                            cell.loadContent(listImagePost: attachments.compactMap({ model in
                                let image = ImagePost(named: "")
                                image.imageUrl = model.thumbnailPath
                                image.imageFullUrl = model.downloadPath
                                image.isImagePost = true
                                image.isHiddenButtonDelete = _self.formModel!.isReadonly
                                return image
                            }))
                        }
                        if errorUploads.count > 0 {
                            self?.showAlert("Alert".localizedString(), stringContent: errorUploads.joined(separator: "\n"))
                        }
                    }
                }
            }
        }
        
        queue.addOperations(operations, waitUntilFinished: false)
    }
    
    // MARK: - ListImagePostTableViewCellDelegate
    func addImagePostCellPress(imagePost: ImagePost) {
        if(self.formModel.isReadonly) {
            return
        }
        self.selectedImagePost = imagePost
        showActionSheetSelectImage()
    }
    
    func didSelectCellPress(viewController: UIViewController) {
        present(viewController, animated: true, completion: {})
    }
    
    func selectedImagePostCellPress(imagePost: ImagePost) {

    }
    
    func removeImagePostCellPress(imagePost: ImagePost, index: Int) {
        
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
            guard let `self` = self else {
                return
            }
            
            // neu la tao form moi thi keep upload
            if self.formModel.id == 0 {
                if index < self.imagesWaitUpload.count {
                    self.imagesWaitUpload.remove(at: index)
                }
                // show selected images
                if let cell = self.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                    cell.loadContent(listImagePost: self.imagesWaitUpload.compactMap({
                        let image = ImagePost(image: UIImage.compressImage($0, compressRatio: 0.7))
                        image.isImagePost = true
                        return image
                    }))
                }
                return
            }
            
            let attachment = self.formModel!.attachments[index]
            self.showMBProgressHUD("Deleting...", animated: true)
            ServiceData.sharedInstance.taskFilemanagerDeletefiles(fileId: attachment.fileID).continueOnSuccessWith(continuation: { task in
                self.formModel!.attachments.remove(at: index)
                self.hideMBProgressHUD(true)
                if let cell = self.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                    imagePost.isImagePost = false
                    cell.reloadData()
                }
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you want delete this file ?".localizedString(), message: "", actions: [yesAction,cancelAction])
        
        
    }
    
    func selectTitlePress() {
        if(self.formModel.isReadonly == true) {
            return
        }
        self.isEditTitleForm = true
        self.pushEditContentViewController(title: "Form title".localizedString(), content: self.textTitleForm, delegate: self)
    }
    
    @objc func clearFromDateButton() {
        self.fromDate = nil
        self.fromDateTableViewCell.update({[weak self] (row) in
            guard let `self` = self else {
                return
            }
            
            row.cellUpdate({[weak self] (cell) in
                cell.body = self?.fromDate?.dateTimeToYYYYMMdd
                self?.tableView.reloadData()
            })
            
        })
    }
    
    func selectFromDatePress() {
        if(self.formModel.isReadonly) {
            return
        }
        let initialDate = self.fromDate ?? Date()
        CalendarPickerPopover.appearFrom(
            originView: fromDateTableViewCell.cellInstance,
            baseViewController: self,
            title: "From Date".localizedString(),
            dateMode: .date,
            initialDate: initialDate,
            doneAction: {[weak self] selectedDate in
                print("selectedDate \(selectedDate)")
                self?.fromDate = selectedDate
                //self?.showStartDateInfo(date: selectedDate)
                
                self?.fromDateTableViewCell.update({[weak self] (row) in
                    guard let `self` = self else {
                        return
                    }
                    
                    row.cellUpdate({[weak self] (cell) in
                        cell.body = self?.fromDate?.dateTimeToYYYYMMdd
                        self?.tableView.reloadData()
                    })
                    
                })
                
                
            },
            cancelAction: {print("cancel")}
        )
    }
    
    @objc func clearToDateButton() {
        self.toDate = nil
        self.toDateTableViewCell.update({[weak self] (row) in
            guard let `self` = self else {
                return
            }
            
            row.cellUpdate({[weak self] (cell) in
                cell.body = self?.toDate?.dateTimeToYYYYMMdd
                self?.tableView.reloadData()
            })
            
        })
    }
    
    func selectToDatePress() {
        if(self.formModel.isReadonly) {
            return
        }
        let initialDate = self.toDate ?? Date()
        CalendarPickerPopover.appearFrom(
            originView: toDateTableViewCell.cellInstance,
            baseViewController: self,
            title: "To Date".localizedString(),
            dateMode: .date,
            initialDate: initialDate,
            doneAction: {[weak self] selectedDate in
                print("selectedDate \(selectedDate)")
                self?.toDate = selectedDate
                //self?.showStartDateInfo(date: selectedDate)
                self?.toDateTableViewCell.update({[weak self] (row) in
                    guard let `self` = self else {
                        return
                    }
                    
                    row.cellUpdate({[weak self] (cell) in
                        cell.body = self?.toDate?.dateTimeToYYYYMMdd
                        self?.tableView.reloadData()
                    })
                    
                })
            },
            cancelAction: {print("cancel")}
        )
    }
    
    func selectContentPress() {
        if(self.formModel.isReadonly == true) {
            return
        }
        self.isEditTitleForm = false
        self.pushEditContentViewController(title: "Form description".localizedString(), content: self.textDescription, delegate: self)
    }
    
    //MARK - EditContentViewControllerDelegate
    func editContentViewControllerSave(viewController: UIViewController, content: String) {
        if(self.isEditTitleForm) {
            self.textTitleForm = content
            self.titleTableViewCell.update({[weak self] (row) in
                guard let `self` = self else {
                    return
                }
                
                row.cellUpdate({[weak self] (cell) in
                    cell.body = content
                    self?.tableView.reloadData()
                })
                
            })
        } else {
            self.textDescription = content
            self.descriptionTableViewCell.update({[weak self] (row) in
                guard let `self` = self else {
                    return
                }
                
                row.cellUpdate({[weak self] (cell) in
                    cell.body = content
                    self?.tableView.reloadData()
                })
                
            })
        }
        
        self.tableView.reloadData()
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func buttonSelectCategoriesPress(sender: UIView) {
        if(self.formModel.isReadonly) {
            return
        }
        if (sender.superview?.superview) != nil {
            _ = self.tableView.convert(sender.frame, to: self.tableView.superview)
            let viewRect = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 64))
            //let viewRect = UIView(frame: rectInSuperview)
            var array = Array<FormCategoryModel>()
            for item in self.listFormCategory {
                item.isSelected = false
                if(item.id == self.category.id) {
                    item.isSelected = true
                }
                array.append(FormCategoryModel(categoryModel: item))
                
            }
            
            FormCategoriesPopover.appearFrom(originView: viewRect, baseViewController: self, title: "Select reason".localizedString(), arrayData: array, doneAction: {[weak self] array in
                for item in array {
                    if(item.isSelected){
                        self?.category.id = item.id
                        self?.category.name = item.name
                        self?.categoriesFormTableViewCell.update({[weak self] (item) in
                            guard let `self` = self else {
                                return
                            }
                            var stringName = self.category.name
                            if(stringName.isEmpty) {
                                stringName = "\n\n"
                            }
                            item.cellUpdate({[weak self] (cell) in
                                cell.body = stringName
                                self?.tableView.reloadData()
                            })
                            
                        })
                        break
                    }
                }
                
                
                }, cancelAction: {
                    print("cancel")
            })
            
        }
        
    }
    
    //
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if let cell = textField.superview?.superview as? UITableViewCell {
            if let indexPath = self.tableView.indexPath(for: cell) {
                let rectCell = self.tableView.rectForRow(at: indexPath)
                let rectOfCellInSuperview = self.tableView.convert(rectCell, to: tableView.superview)
                if let autoCompleteTextField = textField as? AutoCompleteTextField {
                    autoCompleteTextField.autoCompleteTableView?.removeFromSuperview()
                    
                    autoCompleteTextField.autoCompleteTableView?.frame = CGRect(x: autoCompleteTextField.autoCompleteTableView!.frame.origin.x, y: rectOfCellInSuperview.origin.y - autoCompleteTextField.autoCompleteTableView!.frame.size.height, width: autoCompleteTextField.autoCompleteTableView!.frame.size.width + 20, height: autoCompleteTextField.autoCompleteTableView!.frame.size.height)
                    self.view.addSubview(autoCompleteTextField.autoCompleteTableView!)
                }
                
                print(rectOfCellInSuperview)
            }
            
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

    }
    
    func takeImage() {
        
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    //
    func showActionSheetSelectImage() {
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.popover
            if let cell = self.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                imagePicker.popoverPresentationController?.sourceView = cell
            } else {
                imagePicker.popoverPresentationController?.sourceView = self.view
            }
            
        }
        
        
        var arrayAction = Array<UIAlertAction>()
        
        let takePhotoAction = UIAlertAction(title: "Camera".localizedString(), style: UIAlertAction.Style.default, handler: {[weak self] (UIAlertAction) -> Void in
            self?.takeImage()
        })
        arrayAction.append(takePhotoAction)
        
        let chooseFromLibAction = UIAlertAction(title: "Library".localizedString(), style: UIAlertAction.Style.default, handler: {[weak self] (UIAlertAction) -> Void in
            if #available(iOS 14, *) {
                var config = PHPickerConfiguration()
                var maxFiles = 4 // so luong 1 lan uploaded
                // make sure dont select over maxFiles
                if let form = self?.formModel {
                    if form.id == 0 {
                        maxFiles = 4 - min(maxFiles, self?.imagesWaitUpload.count ?? 0)
                    } else {
                        maxFiles = 4 - min(maxFiles, form.attachments.count)
                    }
                }
                config.selectionLimit = maxFiles
                config.filter = PHPickerFilter.images
                let pickerViewController = PHPickerViewController(configuration: config)
                pickerViewController.delegate = self
                self?.present(pickerViewController, animated: true, completion: nil)
            } else {
                self?.imagePicker.delegate = self
                self?.imagePicker.allowsEditing = false
                self?.imagePicker.sourceType = .photoLibrary
                self?.present(self!.imagePicker, animated: true, completion: nil)
            }
        })
        arrayAction.append(chooseFromLibAction)
        
        let cancelAction = UIAlertAction(title: "Cancel".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        arrayAction.append(cancelAction)
        
        self.showActionSheetWithAction("Upload image".localizedString(), message: nil, actions: arrayAction)
        
        
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            // neu la tao form moi thi keep upload
            if self.formModel.id == 0 {
                self.imagesWaitUpload.append(pickedImage)
                
                // show selected images
                if let cell = self.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                    cell.loadContent(listImagePost: self.imagesWaitUpload.compactMap({
                        let image = ImagePost(image: UIImage.compressImage($0, compressRatio: 0.7))
                        image.isImagePost = true
                        return image
                    }))
                }
                return
            }
            
            self.uploadImages(images: [pickedImage])
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func showNoteBarButtonItemPressed(model: FormModel, status: Common.FormStatus, buttonText: String) {
        model.actionStatus = status
//        let popupNoteViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopupNoteViewController") as! PopupNoteViewController
//        popupNoteViewController.popupNoteViewControllerDelagate = self
        let popupNoteViewController = PopupApprovalViewController(nibName: "PopupApprovalViewController", bundle: Bundle.main)
        popupNoteViewController.popupApprovalViewControllerDelagate = self
        popupNoteViewController.object = model
        popupNoteViewController.textTitlePopup = buttonText
        popupNoteViewController.textContentHint = "Type remark if any".localizedString()
        popupNoteViewController.textButtonDone = buttonText
        
        let popupController = STPopupController.init(rootViewController: popupNoteViewController)
        popupController.containerView.layer.cornerRadius = 3
        popupController.transitionStyle = .fade
        popupController.navigationBar.tintColor = UIColor.black
        popupController.present(in: self)
    }
    
    //MARK - PopupApprovalViewControllerDelagate
    func popupApprovalViewControllerSendPress(text: String, object: NSObject?) {
        let text = text
        guard let model = object as? FormModel else {
            return
        }
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFormDoAction(id: model.id, status: model.actionStatus.rawValue, remark: text, approvalFromDate: model.approvalFromDate ?? model.fromDate ?? Date(), approvalToDate: model.approvalToDate ?? model.toDate ?? Date()).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            if let result = task as? JSON {
                let formModel = FormModel(json: result)
                Broadcaster.notify(UpdateFormDelegate.self) {
                    $0.refreshForm(formModel: formModel)
                }
                self.formModel = formModel
                self.loadViewData()
                self.showAlert(self.textTitle, stringContent: "Success".localizedString())
                let _ = self.navigationController?.popViewController(animated: true)
            }
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    //MARK - PopupNoteViewControllerDelagate
    func popupNoteViewControllerSendPress(text: String, object: NSObject?) {
//        let text = text
//        guard let model = object as? FormModel else {
//            return
//        }
//        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
//        ServiceData.sharedInstance.taskFormDoAction(id: model.id, status: model.actionStatus.rawValue, remark: text).continueOnSuccessWith(continuation: { task in
//            if let result = task as? JSON {
//                let formModel = FormModel(json: result)
//                Broadcaster.notify(UpdateFormDelegate.self) {
//                    $0.refreshForm(formModel: formModel)
//                }
//                self.formModel = formModel
//                self.loadViewData()
//                self.showAlert(self.textTitle, stringContent: "Success".localizedString())
//            }
//            self.hideMBProgressHUD(true)
//        }).continueOnErrorWith(continuation: { error in
//            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
//            self.hideMBProgressHUD(true)
//        })
    }

    //MARK - FormWorkflowCellDelegate
    func imageViewAvatarTapped(_ imgUrl: String) {
        let photo = SKPhoto.photoWithImageURL(imgUrl)
        photo.caption = ""
        let browser = SKPhotoBrowser(photos: [photo], initialPageIndex: 0)
        present(browser, animated: true, completion: {})
    }
}

@available(iOS 14, *)
extension CreateFormViewController: PHPickerViewControllerDelegate {
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
       picker.dismiss(animated: false, completion: nil)
       
        var datas:[Data] = []
        var images:[UIImage] = []
        var localError:Error? = nil
        let group = DispatchGroup()
        for result in results {
            group.enter()
            if result.itemProvider.canLoadObject(ofClass: UIImage.self) {
                result.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (object, error) in
                    localError = error
                    if let image = object as? UIImage {
                        if var imageData = image.jpegData(compressionQuality: 1) {
                            if imageData.count > 20*1024 {
                                imageData = image.jpegData(compressionQuality: 0.7)!
                            }
                            datas.append(imageData)
                        }
                        images.append(image)
                    }
                    group.leave()
                })
            } else {
                group.leave()
            }
  
        }
        
        group.notify(queue: .main) {[weak self] in
            if let error = localError {
                self?.showAlert("Alert".localizedString(), stringContent: error.localizedDescription)
            }

            // neu la tao form moi thi keep upload
            if self?.formModel.id == 0 {
                self?.imagesWaitUpload.append(contentsOf: images)
                
                // show selected images
                if let cell = self?.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                    cell.loadContent(listImagePost: self?.imagesWaitUpload.compactMap({
                        let image = ImagePost(image: UIImage.compressImage($0, compressRatio: 0.7))
                        image.isImagePost = true
                        return image
                    }) ?? [])
                }
                return
            }
            
            self?.uploadImages(images: images)
        }
    }
}
