//
//  FormListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/13/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class FormListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, NewFormTableViewCellDelegate, UpdateFormDelegate, FormWorkflowCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var listFormModel = [FormModel]()
    
    var listSearchFormModel = [FormModel]()
    
    var listFilter = [CommonFilterModel]()
    
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero) //CustomSearchBarView(frame: CGRect.zero)
    
    private var pageIndex: Int = 1
    
    private var pageIndexSearch: Int = 1
    
    var refreshControl: UIRefreshControl!
    
    var addBarButtonItem: UIBarButtonItem!
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var leftBarButtonItem: UIBarButtonItem!
    
    var textSearch: String = ""
    
    var selectedFilter: String = "ALL"
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView?.addSubview(refreshControl)
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        searchBar.sizeToFit()
        if #available(iOS 11.0, *) {
            //searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.tableView.fd_debugLogEnabled = true
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        
        addBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.addBarButtonItemPressed(_:)))
        addBarButtonItem.setFAIcon(icon: .FAPlus, iconSize: 20)
        self.navigationItem.rightBarButtonItems = [addBarButtonItem]
        
        leftBarButtonItem = UIBarButtonItem(title: " ", style: .done, target: self, action: #selector(self.buttonSelectEmptyPress))
        //self.navigationItem.leftBarButtonItem = leftBarButtonItem
        setupSearchBar()

        tableView.keyboardDismissMode = .onDrag
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        //tableView.emptyDataSetSource = self
        //tableView.emptyDataSetDelegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView.register(NewFormTableViewCell.nib, forCellReuseIdentifier: "NewFormTableViewCell")
        tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
                
        //self.textTitle = "FORM LIST".localizedString()
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
        
        self.tableView.alwaysBounceVertical = true
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        //self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
//            if(self?.isSearch == true) {
//                self?.loadSearchMoreData() {
//                    self?.tableView!.finishInfiniteScroll()
//                }
//                return
//            }
            self?.loadMoreData() {
                self?.tableView!.finishInfiniteScroll()
            }
            
        }
        
        Broadcaster.register(UpdateFormDelegate.self, observer: self)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func buttonSelectEmptyPress() {
    }
    
    @objc func addBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.pushCreateFormViewController(formModel: nil)
    }
    
    func setupSearchBar() {
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        //self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.returnKeyType = .done
        self.searchBar.placeholder = "Search".localizedString()
        self.searchBar.delegate = self
        self.searchBar.showsBookmarkButton = true
        self.searchBar.setImage(UIImage(named: "ic_filter")?.resizeImageWith(newSize: CGSize(width: 20, height: 20)).withRenderingMode(.alwaysOriginal), for: .bookmark, state: .normal)
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
//        if(self.presentedViewController != nil) {
//            if let navigation = self.presentedViewController as? UINavigationController {
//                navigation.dismiss(animated: true, completion: nil)
//                return false
//            }
//        }
        searchBar.returnKeyType = .search
        self.navigationItem.rightBarButtonItem = nil
//        //self.navigationItem.leftBarButtonItem = nil
//        self.isSearch = true
//        self.tableView.reloadData()
        return true
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.setShowsCancelButton(true, animated: true)
        self.searchBar.showsCancelButton = true
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
        }
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        self.searchBar.sizeToFit()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.textSearch = searchBar.text ?? ""
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.text = ""
        self.searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.resignFirstResponder()
        self.navigationItem.rightBarButtonItem = self.addBarButtonItem
        //self.navigationItem.leftBarButtonItem = leftBarButtonItem
//        self.isSearch = false
        self.tableView.reloadData()
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
//    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
//        searchBar.setShowsCancelButton(true, animated: true)
//        searchBar.showsCancelButton = true
//        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
//            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
//        }
//        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
//            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
//            self.navigationItem.titleView = searchBarContainer
//            self.searchBar.sizeToFit()
//        }
//    }
    
//    func searchFilter(text: String) {
//        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
//        pageIndexSearch = 1
//        textSearch = text
//        ServiceData.sharedInstance.taskFormList(keyword: textSearch, categoriesId: 0, pageIndex: pageIndexSearch).continueOnSuccessWith(continuation: { task in
//
//            let result = task as! JSON
//            if let array = result.array {
//                var arrayModel = Array<FormModel>()
//                for item in array {
//                    let model = FormModel(json: item)
//                    arrayModel.append(model)
//                }
//                self.listSearchFormModel = arrayModel
//
//                self.tableView.reloadData()
//                self.hideMBProgressHUD(true)
//                self.refreshControl.endRefreshing()
//            }
//
//        }).continueOnErrorWith(continuation: { error in
//            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
//            self.hideMBProgressHUD(true)
//            self.refreshControl.endRefreshing()
//        })
//    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if self.listFilter.count == 0 {
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            self.getFilterData() {
                self.didTapFilter(searchBar)
            }
        } else {
            didTapFilter(searchBar)
        }
    }
    
    @objc func didTapFilter(_ sender: UISearchBar) {
        var array = Array<CommonPopoverModel>()
        //array.append(CommonPopoverModel.init("", "All".localizedString(), "All".localizedString(), self.selectedFilter == ""))
        for item in listFilter {
            array.append(CommonPopoverModel.init(item.Code, item.Code, item.Name, self.selectedFilter == item.Code))
        }
        
        CommonCategoriesPopover.appearFrom(originView: sender, baseViewController: self, title: "Please select".localizedString(), arrayData: array, doneAction: {[weak self] array in
            for item in array {
                if item.IsSelected {
                    self?.selectedFilter = item.Id
                    self?.showMBProgressHUD("Loading...".localizedString(), animated: true)
                    self?.getNewData()
                    break
                }
            }
        }, cancelAction: { print("cancel") },
        modalStyle: .popover,
        arrowDirection: .any)
    }
    
    func getFilterData(completion: @escaping () -> Void) {
        ServiceData.sharedInstance.taskFormGetFilter().continueOnSuccessWith(continuation: { task in
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<CommonFilterModel>()
                for item in array {
                    let model = CommonFilterModel(json: item)
                    arrayModel.append(model)
                }
                self.listFilter = arrayModel
            }
            self.hideMBProgressHUD(true)
            completion()
        }).continueOnErrorWith(continuation: { error in
            self.hideMBProgressHUD(true)
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            completion()
        })
    }
    
    @objc func refreshData() {
        getNewData()
    }
    
    func getNewData() {
        pageIndex = 1
        ServiceData.sharedInstance.taskFormList(keyword: self.textSearch, categoriesId: 0, pageIndex: pageIndex, filter: self.selectedFilter).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<FormModel>()
                for item in array {
                    let model = FormModel(json: item)
                    arrayModel.append(model)
                }
                self.listFormModel = arrayModel
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    func loadMoreData(_ handler: (() -> Void)?) {
        
        if(self.listFormModel.count > 1 && !self.refreshControl.isRefreshing){
            pageIndex += 1
            
            ServiceData.sharedInstance.taskFormList(keyword: self.textSearch, categoriesId: 0, pageIndex: pageIndex, filter: self.selectedFilter).continueOnSuccessWith(continuation: { task in
                let result = task as! JSON
                if let array = result.array, array.count > 0 {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in array {
                        listIndexPath.append(IndexPath(row: self.listFormModel.count, section: 0))
                        let model = FormModel(json: item)
                        self.listFormModel.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                
                handler?()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                handler?()
            })
            
        } else {
            handler?()
        }
    }
    
    
//    func loadSearchMoreData(_ handler: (() -> Void)?) {
//
//        if(self.listSearchFormModel.count > 1 && !self.refreshControl.isRefreshing){
//            pageIndexSearch += 1
//            ServiceData.sharedInstance.taskFormList(keyword: textSearch, categoriesId: 0, pageIndex: pageIndexSearch, filter: self.selectedFilter).continueOnSuccessWith(continuation: { task in
//
//                let result = task as! JSON
//                if let array = result.array, array.count > 0 {
//                    self.tableView?.beginUpdates()
//                    var listIndexPath = Array<IndexPath>()
//                    for item in array {
//                        listIndexPath.append(IndexPath(row: self.listSearchFormModel.count, section: 0))
//                        let model = FormModel(json: item)
//                        self.listSearchFormModel.append(model)
//                    }
//                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
//                    self.tableView?.endUpdates()
//                }
//                self.hideMBProgressHUD(true)
//
//                handler?()
//
//            }).continueOnErrorWith(continuation: { error in
//                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
//                self.hideMBProgressHUD(true)
//                self.refreshControl.endRefreshing()
//            })
//
//        } else {
//            handler?()
//        }
//    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: FormTableViewCellDelegate
    
    func actionFormTableViewCell(action: ActionUpdateFormType, formModel: FormModel) {
        if(action == .remove) {
            self.confirmDelete(formModel: formModel)
        } else {
            self.pushCreateFormViewController(formModel: formModel)
        }
    }
    
    
    func pressFormTableViewCell(cell: NewFormTableViewCell) {
        
    }
    
    func configure(cell: NewFormTableViewCell, at indexPath: IndexPath) {
        if(isSearch) {
            cell.loadContent(formModel: self.listSearchFormModel[indexPath.row])
        } else {
            cell.loadContent(formModel: self.listFormModel[indexPath.row])
        }
        cell.delegateAction = self
        cell.delegateAvatar = self
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearch) {
            return self.listSearchFormModel.count
        }
        return self.listFormModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewFormTableViewCell")! as! NewFormTableViewCell
        self.configure(cell: cell, at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "NewFormTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! NewFormTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model: FormModel
        if(isSearch) {
            model = self.listSearchFormModel[indexPath.row]
        } else {
            model = self.listFormModel[indexPath.row]
        }
        self.pushCreateFormViewController(formModel: model)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    
    //
    
    func confirmDelete(formModel: FormModel) {
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
            guard let `self` = self else {
                return
            }
            self.showMBProgressHUD("Update...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskFormRemove(id: formModel.id).continueOnSuccessWith(continuation: { task in
                self.refreshListModel(model: formModel)
                self.hideMBProgressHUD(true)
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you sure delete ?".localizedString(), message: "", actions: [yesAction, cancelAction])
    }
    
    
    func refreshListModel(model: FormModel) {
        for index in 0..<self.listFormModel.count {
            if (self.listFormModel[index].id == model.id) {
                self.listFormModel.remove(at: index)
                self.tableView.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                return
            }
        }
    }
    
    
    //MARK - UpdateFormDelegate
    func addForm(formModel: FormModel) {
        self.listFormModel.insert(formModel, at: 0)
        self.tableView.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
    }
    
    func refreshForm(formModel: FormModel) {
        for index in 0..<self.listFormModel.count {
            if(formModel.id == self.listFormModel[index].id) {
                self.listFormModel[index] = formModel
                self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                return
            }
            
        }
        self.addForm(formModel: formModel)
    }
    
    func imageViewAvatarTapped(_ imgUrl: String) {
        let photo = SKPhoto.photoWithImageURL(imgUrl)
        photo.caption = ""
        let browser = SKPhotoBrowser(photos: [photo], initialPageIndex: 0)
        present(browser, animated: true, completion: {})
    }
    
}

