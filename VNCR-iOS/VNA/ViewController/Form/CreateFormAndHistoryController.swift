//
//  CreateFormAndHistoryController.swift
//  VNA
//
//  Created by Dai Pham on 11/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class CreateFormAndHistoryController: BaseViewController {

    var formModel: FormModel?
    
    var createFormController:CreateFormViewController?
    var historyController:FormHistoryViewController?
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackContainer: UIStackView!
    @IBOutlet weak var btnSegment: UISegmentedControl!
    
    init(formModel: FormModel?) {
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle.main)
        self.formModel = formModel
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        Broadcaster.register(UpdateFormDelegate.self, observer: self)
        config()
    }
    
    private func config() {
        historyController =  UIStoryboard.init(name: "Main_iphone", bundle: nil).instantiateViewController(withIdentifier: "FormHistoryViewController") as? FormHistoryViewController
        historyController?.cid = formModel?.cid ?? ""
        createFormController =  UIStoryboard.init(name: "Main_iphone", bundle: nil).instantiateViewController(withIdentifier: "CreateFormViewController") as? CreateFormViewController
        createFormController?.formModel = formModel
        
        var formId = 0
        if let f = formModel {
            formId = f.id
        }
        
        btnSegment.isHidden = formId == 0
        
        if let vc = createFormController {
            addChild(vc)
            stackContainer.addArrangedSubview(vc.view)
        }
        if let vc = historyController {
            addChild(vc)
            stackContainer.addArrangedSubview(vc.view)
        }
        // set contraint cho first view
        if let v = stackContainer.arrangedSubviews.first {
            v.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
        }
        
        if let form = formModel, form.id != 0 {
            btnSegment.setTitle("\(form.cName.isEmpty ? form.cid : form.cName)", forSegmentAt: 0)
        } else {
            btnSegment.setTitle("CREATE FORM".localizedString(), forSegmentAt: 0)
        }
        btnSegment.setTitle("History".localizedString(), forSegmentAt: 1)
        btnSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white,
                                           NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .medium)], for: UIControl.State())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        createFormController?.refreshBarItems()
    }
    
    @IBAction func actionSegment(_ sender: UISegmentedControl) {
        scrollView.setContentOffset(CGPoint(x: sender.selectedSegmentIndex * Int(self.scrollView.frame.width), y: 0), animated: false)
        if sender.selectedSegmentIndex == 0 {
            createFormController?.refreshBarItems()
        } else {
            historyController?.refreshBarItems()
        }
    }
    
}
extension CreateFormAndHistoryController: UpdateFormDelegate {
    func addForm(formModel: FormModel) {
        self.formModel = formModel
        btnSegment.isHidden = formModel.id == 0
        btnSegment.setTitle("\(formModel.cName.isEmpty ? formModel.cid : formModel.cName)", forSegmentAt: 0)
    }
    
    func refreshForm(formModel: FormModel) {
        self.formModel = formModel
        btnSegment.isHidden = formModel.id == 0
        btnSegment.setTitle("\(formModel.cName.isEmpty ? formModel.cid : formModel.cName)", forSegmentAt: 0)
    }
}
