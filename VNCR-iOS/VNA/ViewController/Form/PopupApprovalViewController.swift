//
//  PopupApprovalViewController.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 22/03/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import STPopup
import IQKeyboardManagerSwift

protocol PopupApprovalViewControllerDelagate: NSObjectProtocol {
    func popupApprovalViewControllerSendPress(text: String, object: NSObject?)
}

class PopupApprovalViewController: BaseViewController {
    
    weak var object: NSObject?
    
    var model: FormModel!
    
    var textContent: String = ""
    
    var textButtonDone: String = "Send".localizedString()
    
    var textContentHint: String = "Enter text".localizedString()
    
    var textTitlePopup: String = "Note".localizedString()
    
    weak var popupApprovalViewControllerDelagate: PopupApprovalViewControllerDelagate?
    
    @IBOutlet weak var buttonSendNote: UIButton!
    
    @IBOutlet weak var textViewNote: IQTextView!
    
    @IBOutlet weak var viewSelectFromDate: UIView!
    @IBOutlet weak var lblTitleFromDate: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
    
    @IBOutlet weak var lblTitleToDate: UILabel!
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var viewSelectToDate: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.contentSizeInPopup = CGSize(width: 320, height: 350)
        self.landscapeContentSizeInPopup = CGSize(width: 320, height: 350)
        
        self.buttonSendNote.backgroundColor = UIColor("#166880")
        self.buttonSendNote.layer.cornerRadius = 2.5
        self.buttonSendNote.setTitleColor(UIColor.white, for: .normal)
        self.buttonSendNote.addTarget(self, action: #selector(buttonSendNotePress), for: .touchUpInside)
        
        self.textViewNote.layer.borderColor = UIColor.lightGray.cgColor
        self.textViewNote.layer.borderWidth = 0.4
        self.textViewNote.layer.cornerRadius = 2.5
        self.textViewNote.text = textContent
        self.textViewNote.placeholder = textContentHint
        
        self.lblTitleFromDate.text = "Approval from".localizedString()
        self.lblTitleToDate.text = "Approval to".localizedString()

        self.navigationItem.title = textTitlePopup
        self.buttonSendNote.setTitle(textButtonDone, for: .normal)
        
        guard let formModel = object as? FormModel else {
            return
        }
        self.model = formModel
        if self.model.approvalFromDate == nil {
            self.model.approvalFromDate = self.model.fromDate
        }
        if self.model.approvalToDate == nil {
            self.model.approvalToDate = self.model.toDate
        }
        
        if self.model.actionStatus == Common.FormStatus.Accepted {
            self.buttonSendNote.backgroundColor = UIColor("#388E3C")
        } else if self.model.actionStatus == Common.FormStatus.Rejected {
            self.buttonSendNote.backgroundColor = UIColor("#DA0019")
        }
        
        self.lblFromDate.text = self.model.approvalFromDate?.dateTimeToYYYYMMdd
        self.lblToDate.text = self.model.approvalToDate?.dateTimeToYYYYMMdd
        
        self.viewSelectFromDate.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.selectFromDatePress(_:))))
        self.viewSelectToDate.addGestureRecognizer(UITapGestureRecognizer(target: self, action:  #selector(self.selectToDatePress(_:))))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func buttonSendNotePress() {
        self.popupApprovalViewControllerDelagate?.popupApprovalViewControllerSendPress(text: self.textViewNote.text, object: self.model)
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func selectFromDatePress(_ sender: UITapGestureRecognizer) {
        let initialDate = self.model.approvalFromDate ?? Date()
        CalendarPickerPopover.appearFrom(
            originView: lblTitleFromDate,
            baseViewController: self,
            title: "Approval from".localizedString(),
            dateMode: .date,
            initialDate: initialDate,
            doneAction: {[weak self] selectedDate in
                self?.model?.approvalFromDate = selectedDate
                self?.lblFromDate.text = selectedDate.dateTimeToYYYYMMdd
            },
            cancelAction: {print("cancel")}
        )
    }
    
    @objc func selectToDatePress(_ sender: UITapGestureRecognizer) {
        let initialDate = self.model.approvalToDate ?? Date()
        CalendarPickerPopover.appearFrom(
            originView: lblTitleToDate,
            baseViewController: self,
            title: "Approval to".localizedString(),
            dateMode: .date,
            initialDate: initialDate,
            doneAction: {[weak self] selectedDate in
                self?.model?.approvalToDate = selectedDate
                self?.lblToDate.text = selectedDate.dateTimeToYYYYMMdd
            },
            cancelAction: {print("cancel")}
        )
    }
    
}
