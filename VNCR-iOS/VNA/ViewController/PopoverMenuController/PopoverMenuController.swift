//
//  PopoverMenuController.swift
//  GlobeDr
//
//  Created by Pham Dai on 13/08/2021.
//  Copyright © 2021 GlobeDr. All rights reserved.
//

import UIKit

class PopoverMenuController: BaseViewController {

    class MenuItem:NSObject {
        var title:String
        var icon:UIImage?
        var identifier:String
        
        init(title:String,
             icon:UIImage?,
             identifier:String) {
            self.title = title
            self.icon = icon
            self.identifier = identifier
            super.init()
        }
    }
    
    // MARK: -  outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackContainer: UIStackView!
    
    // MARK: -  properties
    typealias ACTION = ((MenuItem?)->Void)
    var action:ACTION?
    var items:[MenuItem]
    var currentSelectedIdentifier:String?
    
    init(items:[MenuItem], currentSelectedIdentifier:String?, action:ACTION?) {
        self.items = items
        super.init(nibName: String(describing: PopoverMenuController.self), bundle: .main)
        self.action = action
        self.currentSelectedIdentifier = currentSelectedIdentifier
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalPresentationStyle=UIModalPresentationStyle.custom
        modalPresentationCapturesStatusBarAppearance = true
        if #available(iOS 13.0, *){
//            self.isModalInPresentation = true
            self.isModalInPopover = !isIpad
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        items.forEach { menu in
            let btn = Button10Corner(type: .custom)
            btn.titleLabel?.adjustsFontSizeToFitWidth = false
            btn.object = menu
            btn.addTarget(self, action: #selector(self.selectButton(_:)), for: .touchUpInside)
            btn.contentHorizontalAlignment = .left
            btn.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 10)
            if let icon = menu.icon {
                btn.setImage(icon/*.resizeImageWith(newSize: CGSize(width: 15, height: 15))*/, for: UIControl.State())
                btn.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: -10)
            }
            btn.setTitle(menu.title, for: UIControl.State())
            btn.setTitleColor(currentSelectedIdentifier == menu.identifier ? .systemBlue : .black, for: UIControl.State())
            btn.titleLabel?.font = .systemFont(ofSize: 16)
            stackContainer.addArrangedSubview(btn)
        }
        
        updatePreferredContentSize()
        
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    @objc func selectButton(_ sender:Button10Corner) {
        self.dismiss(animated: true) {[weak self] in guard let `self` = self else { return }
            self.action?( sender.object as? MenuItem)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updatePreferredContentSize()
    }
    
    func updatePreferredContentSize() {
        var height:CGFloat = scrollView.contentSize.height + scrollView.contentInset.top + scrollView.contentInset.bottom
        if #available(iOS 11.0, *) {
            height += self.view.directionalLayoutMargins.top + self.view.directionalLayoutMargins.bottom
        } else {
            height += self.view.layoutMargins.top + self.view.layoutMargins.bottom
        }
        let width = self.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).width
        let max = UIScreen.bounceWindow.height * 0.8
        if height > max {
            height = max
        }
        
        preferredContentSize = CGSize(width: width, height: height)
        self.navigationController?.preferredContentSize = preferredContentSize
    }
}
