//
//  FlightSeatMapListViewController.swift
//  VNA
//
//  Created by Dai Pham on 22/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class FlightSeatMapListViewController: BaseViewController {

    // MARK: -  thuoc tinh
    var titleText: String
    var flightId: Int
    var seatmap: AircraftCategoryType = .none
    
    // MARK: -  outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: SearchBar!
    @IBOutlet weak var stackContainer: UIStackView!
    @IBOutlet weak var btnSegment: UISegmentedControl!

    // MARK: -  properties
    var listPassenger:PassengerListModel = []
    var filterListPassenger:PassengerListModel = []
    
    var seatMapController:SeatmapViewController = SeatmapViewController()
    
    
    init(titleText: String,flightId: Int, seatmap: AircraftCategoryType) {
        self.titleText = titleText
        self.flightId = flightId
        super.init(nibName: String(describing:type(of: self)), bundle: Bundle.main)
        self.seatmap = seatmap
    }
    
    required init?(coder: NSCoder) {
        self.titleText = ""
        self.flightId = 0
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // thiet lap left Column Width = 0.5 screen width
        splitViewController?.maximumPrimaryColumnWidth = UIScreen.bounceWindow.width;
        splitViewController?.preferredPrimaryColumnWidthFraction = 0.5;
        
        // theit lap seat map view
        seatMapController.flightId = self.flightId
        seatMapController.titleText = self.titleText
        seatMapController.seatmap = self.seatmap
        seatMapController.delegate = self
        
        addChild(seatMapController)
        stackContainer.insertArrangedSubview(seatMapController.view, at: 0)
        seatMapController.view.translatesAutoresizingMaskIntoConstraints = false
        seatMapController.view.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1, constant: 0).isActive = true
        tableView.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1, constant: 0).isActive = true
        
        if navigationController?.viewControllers.count ?? 0 < 2 { // truong hop this controller is root of navigation controller
            self.setBarCloseButton()
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 50
        tableView.register(SeatListCell.nib, forCellReuseIdentifier: SeatListCell.identifier)
        
        // add search bar
        searchBar.delegate = self
        searchBar.placeholder = "Search".localizedString()
        
        btnSegment.setTitle("Passenger".localizedString(), forSegmentAt: 1)
        btnSegment.setTitle("Seat Map".localizedString(), forSegmentAt: 0)
        btnSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white,
                                           NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .medium)], for: UIControl.State())
        
        self.navigationItem.title = titleText
        
        // Do any additional setup after loading the view.
        getPasssengerList()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        if btnSegment.selectedSegmentIndex > 0 {
            scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(50)) {
                self.scrollView.setContentOffset(CGPoint(x: self.btnSegment.selectedSegmentIndex * Int(self.scrollView.frame.width), y: 0), animated: true)
            }

        }
    }
    
    // MARK: -  event
    @IBAction func changeTab(_ sender: UISegmentedControl) {
        scrollView.setContentOffset(CGPoint(x: sender.selectedSegmentIndex * Int(self.scrollView.frame.width), y: 0), animated: false)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.view.safeAreaInsets.bottom + btnSegment.frame.height + 20, right: 0)
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20 + btnSegment.frame.height, right: 0)
        }
    }
    
    func filterPassenger(keyword:String) {
        if keyword.count > 0 {
            filterListPassenger = self.listPassenger.filter({$0.seat?.lowercased().contains(keyword.lowercased()) == true ||
                                                                $0.fullName?.lowercased().contains(keyword.lowercased()) == true ||
                $0.title?.lowercased().contains(keyword.lowercased()) == true
            })
        } else {
            filterListPassenger = listPassenger
        }
        tableView.reloadData()
        
        // xoa trang detail
//        self.splitViewController?.viewControllers = [self]
    }
    
    func getPasssengerList(completion: (() -> Void)? = nil) {
        self.showMBProgressHUD("", animated: true)
        ServiceDataCD.getPassengers(flightId: flightId) {[weak self] list, error in guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.hideMBProgressHUD(true)
                if let error = error {
                    self.showAlert("Alert".localizedString(), message: error, buttons: ["Ok".localizedString()]) { action, pos in
                        if action.title == "Ok".localizedString() {
                            self.close()
                        }
                    }
                } else {
                    self.listPassenger = list ?? []
                    self.filterPassenger(keyword: "")
                    NotificationCenter.default.post(name: .passengerMapLoadDone, object: nil)
                }
                completion?()
            }
        }
    }
}

// MARK: -  TableView delegate, datasource
extension FlightSeatMapListViewController: UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if splitViewController == nil ||
           filterListPassenger.count == 0 {
            tableView.deselectRow(at: indexPath, animated: true)
        }
        
        if filterListPassenger.count <= 0 {return} // truong hop ko co record
       
        let passenger = filterListPassenger[indexPath.row]
        #if DEBUG
        print("\(String(describing: passenger.fullName)) \(#function)")
        #endif

        seatMapController.resetSelected()
        seatMapController.setSelectedSeat(seatId: passenger.seat)
        let vc = FlightSeatInformationController(passenger: passenger)
        if splitViewController == nil {
            let nv = BasePresentNavigationController(root: vc)
            self.present(vc: nv, transitioning: ScaleUpAnimateTransitionDelegate(), completion: nil)
        } else {
            self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: vc), sender: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if filterListPassenger.count > 0 {
            let passenger = filterListPassenger[indexPath.row]
            
            if let cell = tableView.dequeueReusableCell(withIdentifier: SeatListCell.identifier) as? SeatListCell {
                cell.show(passenger: passenger)
                return cell
            }
        }
        
        let cell = UITableViewCell()
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = "No items".localizedString()
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterListPassenger.count > 0 ? filterListPassenger.count : 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        UITableView.automaticDimension
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        (self.navigationItem.titleView as? SearchBarContainerView)?.searchBar.resignFirstResponder()
        searchBar.resignFirstResponder()
    }
}

// MARK: -  UISearchBarDelegate
extension FlightSeatMapListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterPassenger(keyword: searchText)
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        (self.navigationItem.titleView as? SearchBarContainerView)?.searchBar.resignFirstResponder()
        searchBar.resignFirstResponder()
    }
}

// MARK: -  SeatMapControllerDelegate
extension FlightSeatMapListViewController: SeatViewControllerDelegate {
    func SeatViewController_selectSeat(seatId: String?) {
        if let index = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: index, animated: false)
        }
        if let index = filterListPassenger.firstIndex(where: {$0.seat == seatId}), splitViewController != nil {
            tableView.selectRow(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .middle)
        }
    }
    
    func SeatViewController_getPassergerList() -> PassengerListModel {
        return listPassenger
    }
}

// MARK: -  UISplitViewControllerDelegate
extension FlightSeatMapListViewController: UISplitViewControllerDelegate {
    
}
