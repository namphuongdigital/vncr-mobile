//
//  FlightSeatInformationController.swift
//  VNA
//
//  Created by Dai Pham on 23/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class FlightSeatInformationController: BaseViewController {

    // MARK: -  properties
    var passenger:PassengerSeat?
    
    var onDismiss:(()->Void)? //handle after this controller dismiss

    // MARK: -  outlets
    @IBOutlet weak var avatarView: RoundImageView!
    @IBOutlet weak var lblFullName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var txvNote: UITextView10Corner!
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var btnOrderList: UIButton!
    @IBOutlet weak var btnLotusShop: UIButton!
    @IBOutlet weak var lotusComponent: LotusShopComponentView!
    
    init(passenger:PassengerSeat) {
        self.passenger = passenger
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle.main)
        self.setCustomPresentationStyle()
    }
    
    required init?(coder: NSCoder) {
        passenger = nil
        super.init(coder: coder)
        self.setCustomPresentationStyle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lotusComponent.delegate = self
        lotusComponent.controller = self
        
        txvNote.delegate = self
        scrollView.delegate = self
        txvNote.placeholder1 = "Enter text".localizedString()
        txvNote.backgroundColor = .white
        txvNote.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        txvNote.addToolbar(title: "Done".localizedString(), action: #selector(hideKeyboard),target: self)
        
        lblDescription.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        lblFullName.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        
        btnOrderList.setImage(#imageLiteral(resourceName: "ic_order_64"), for: UIControl.State())
        btnLotusShop.setImage(#imageLiteral(resourceName: "ic_shopping_64"), for: UIControl.State())
        
        btnOrderList.setTitle("Order list".localizedString(), for: UIControl.State())
        
        loadData()
        
        self.view.layoutIfNeeded()
        updatePrsentSize()
    }
    
    func loadData() {
        lotusComponent.show(passenger: passenger)
        lotusComponent.isHidden = passenger?.lotusShopStatus == nil
        
        btnLotusShop.isHidden = passenger?.lotusShopStatus != nil || !(passenger?.haveLotusitems ?? false)
        btnLotusShop.setTitle("LotuShop (\(passenger?.lotusShopItems.count ?? 0) \(passenger?.lotusShopItems.count ?? 0 > 1 ? "items" : "item"))" , for: UIControl.State())

        showPassenger()
    }
    
    @objc func hideKeyboard() {
        txvNote.resignFirstResponder()
    }
    
    // MARK: -  event
    @IBAction func actionButton(_ sender: UIButton) {
        switch sender {
        case btnOrderList:
            break
        case btnLotusShop:
            guard let passenger = passenger, let flightId = passenger.flightID else {return}
            let vc = ReportLotusShopController(passenger: passenger, flightID: flightId) {[weak self] newStatus in guard let `self` = self else { return }
                self.LotusShopComponentView_shouldRefreshLotus(new: newStatus)
            }
            self.present(vc: BasePresentNavigationController(root: vc), transitioning: ScaleUpAnimateTransitionDelegate(), completion: nil)
        default:
            break
        }
    }
    
    func localEvent() {
        scrollView.addEvent {[weak self] scrolView in
            self?.txvNote.resignFirstResponder()
        }
    }
    
    private func showPassenger() {
        guard let pass = passenger else {
            // trong truong hop ko phai tren ipad thi 100% la present nen dismiss view neu khong co du lieu
            if splitViewController == nil {
                close()
            }
            return
        }
        
        avatarView.loadImageNoProgressBar(url: URL(string: pass.avatarURL ?? ""))
        lblFullName.text = pass.fullName
        lblDescription.text = pass.desc
        
        lblFullName.textColor = pass.titleColor
        lblDescription.textColor = pass.descColor
        txvNote.textColor = pass.titleColor
        
        lblDescription.isHidden = pass.desc?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0
        lblFullName.isHidden = pass.fullName?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0
        
        if self.navigationController != nil && self.splitViewController == nil {
            setBarCloseButton(title: pass.seat)
        } else {
            navigationItem.title = pass.seat
        }
    }
    
    func updatePrsentSize() {
        var height = scrollView.contentSize.height + 20 // constant bottom
        let max = UIScreen.bounceWindow.height*0.9
        if height > max {
            height = max
        }
        self.preferredContentSize = CGSize(width: UIScreen.bounceWindow.width - 40, height: height)
        UIView.animate(withDuration: 0.2) {
            self.navigationController?.preferredContentSize = self.preferredContentSize
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.roundCornersFull(corners: [.allCorners], radius: 10)
        self.navigationController?.view.roundCornersFull(corners: [.allCorners], radius: 10)
        updatePrsentSize()
    }
    
    deinit {
        onDismiss?()
    }
}

// MARK: -  UItextView Delegate
extension FlightSeatInformationController: UITextViewDelegate, UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        txvNote.resignFirstResponder()
    }
}
extension FlightSeatInformationController: LotusShopComponentViewDelegate {
    func LotusShopComponentView_shouldRefreshLotus(new: LotusShopStatus?) {
        self.passenger?.lotusShopStatus = new
        loadData()
        self.view.setNeedsLayout()
    }
}
