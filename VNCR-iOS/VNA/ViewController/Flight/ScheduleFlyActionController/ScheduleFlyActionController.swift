//
//  SheduleFlyActionController.swift
//  VNA
//
//  Created by Dai Pham on 07/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class ScheduleFlyActionController: BaseViewController {

    enum ScheduleFlyActionType {
        case normal
        case personal
    }
    
    enum ScheduleFlyAction:Int {
        case task = 1
        case report
        case assignment
        case survey
        case seatMap
        case oJT
        case quickReport
        case synchronize
        case saveLocal
        case refreshLocal
        case deleteLocal
    }
    
    @IBOutlet weak var vwLine2: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnTask: UIButton!
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var btnAssessment: UIButton!
    @IBOutlet weak var btnSurvey: UIButton!
    @IBOutlet weak var btnSeatMap: UIButton!
    @IBOutlet weak var btnOJT: UIButton!
    @IBOutlet weak var btnQuickReport: UIButton!
    @IBOutlet weak var btnSynchronize: UIButton!
    @IBOutlet weak var btnSaveLocal: UIButton!
    @IBOutlet weak var btnDeleteLocal: UIButton!
    
    var listType:ScheduleFlyActionType
    
    var onGoto:((ScheduleFlyAction)->Void)?
    var scheduleFly: ScheduleFlightModel!
    
    override var multiplierWidth: CGFloat {
        get {
            let widthScreen = UIScreen.bounceWindow.width
            let result = (isIpad ? (widthScreen > 1284 ? 1284/widthScreen : 1.0) : 1.0)
            return result
        }
    }
    
    init(listType:ScheduleFlyActionType,scheduleFly:ScheduleFlightModel, action:((ScheduleFlyAction)->Void)?) {
        self.listType = listType
        super.init(nibName: String(describing: type(of: self)), bundle: .main)
        self.scheduleFly = scheduleFly
        self.onGoto = action
        
        self.setCustomPresentationStyle(modalPresentationStyle: isIpad ? .popover : .custom)
    }
    
    required init?(coder: NSCoder) {
        listType = .normal
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        config()
        
        self.preferredContentSize = self.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
    private func config() {
        
        view.backgroundColor = .white
        
        lblTitle.font = UIFont.systemFont(ofSize: 20)
        if #available(iOS 10.0, *) {
            lblTitle.adjustsFontForContentSizeCategory = true
        }
        lblTitle.textColor = .black

        if let schedule = scheduleFly {
            lblTitle.text = "\(schedule.flightNo)/\(schedule.departedDate)"
        }
        
        btnTask.tag = ScheduleFlyAction.task.rawValue
        btnSurvey.tag = ScheduleFlyAction.survey.rawValue
        btnReport.tag = ScheduleFlyAction.report.rawValue
        btnAssessment.tag = ScheduleFlyAction.assignment.rawValue
        btnSeatMap.tag = ScheduleFlyAction.seatMap.rawValue
        btnOJT.tag = ScheduleFlyAction.oJT.rawValue
        btnQuickReport.tag = ScheduleFlyAction.quickReport.rawValue
        btnSynchronize.tag = ScheduleFlyAction.synchronize.rawValue
        btnDeleteLocal.tag = ScheduleFlyAction.deleteLocal.rawValue
        
        btnSeatMap.backgroundColor = UIColor("#166880")
        btnOJT.backgroundColor = UIColor("#7788aa")
        btnTask.backgroundColor = UIColor("#dba510")
        btnReport.backgroundColor = UIColor.lightGray
        btnSurvey.backgroundColor = UIColor("#6699cc")
        btnAssessment.backgroundColor = UIColor("#33cc90")
        btnQuickReport.backgroundColor = .darkText
        btnSynchronize.backgroundColor = #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
        
        btnTask.setImage(UIImage.init(named: "task.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        btnReport.setImage(UIImage.init(named: "Flight Report.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        btnAssessment.setImage(UIImage.init(named: "Assessment.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        btnSurvey.setImage(UIImage.init(named: "Survey.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        btnSeatMap.setImage(UIImage.init(named: "Seatmap-icon.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        btnOJT.setImage(UIImage(named: "OJT (Lecture).png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        btnQuickReport.setImage(UIImage(named: "quick.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnSynchronize.setImage(UIImage(named: "ic_synchronize.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        btnSaveLocal.setImage(UIImage(named: "ic_save_local"), for: .normal)
        btnSaveLocal.setImage(UIImage(named: "ic_refresh_local"), for: .selected)
        btnDeleteLocal.setImage(UIImage(named: "ic_delete_local"), for: .normal)
        
        btnQuickReport.imageEdgeInsets = UIEdgeInsets(top: 11, left: 11, bottom: 11, right: 11)
        btnSynchronize.imageEdgeInsets = UIEdgeInsets(top: 11, left: 11, bottom: 11, right: 11)
        
        btnTask.tintColor = .white
        btnReport.tintColor = .white
        btnAssessment.tintColor = .white
        btnSurvey.tintColor = .white
        btnSeatMap.tintColor = .white
        btnOJT.tintColor = .white
        btnQuickReport.tintColor = .white
        btnSynchronize.tintColor = .white
        
        if #available(iOS 10.0, *) {
            checkShouldShowActionHandleLocalData()
        }
    }
    
    @available(iOS 10.0, *)
    func checkShouldShowActionHandleLocalData() {
        ServiceDataCD.flightIsSaved(flightId: scheduleFly.flightID) {isExist in
            DispatchQueue.main.async {[weak self] in
                UIView.animate(withDuration: 0.2) {[weak self] in
                    guard let `self` = self else { return }
                    self.btnDeleteLocal.isHidden = !isExist
                    self.btnSaveLocal.isHidden = !NetworkObserver.shared.isConnectedInternet
                    self.btnSaveLocal.isSelected = isExist
                }
                self?.btnSaveLocal.tag = isExist ? ScheduleFlyAction.refreshLocal.rawValue : ScheduleFlyAction.saveLocal.rawValue
            }
        }
    }
    
    override func observerNetwork(object: Notification) {
        super.observerNetwork(object: object)
        
        if #available(iOS 10.0, *) {
            checkShouldShowActionHandleLocalData()
        }
    }
    
    func hideTitle() {
        lblTitle.isHidden = true
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        self.preferredContentSize = self.view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        view.roundCornersFull(corners: [.topLeft,.topRight,.bottomLeft,.bottomRight], radius: 10)
    }
    
    @IBAction func action(_ sender: UIButton) {
        if let action = ScheduleFlyActionController.ScheduleFlyAction(rawValue: sender.tag) {
            self.onGoto?(action)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
