//
//  BriefingCell.swift
//  VNA
//
//  Created by Pham Dai on 24/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

protocol BriefingCellDelegate:AnyObject {
    func BriefingCell_replyBriefing(briefing:Briefing?, cell:BriefingCell)
    func BriefingCell_deleteBriefing(briefing:Briefing?, cell:BriefingCell)
    func BriefingCell_viewImage(url:String, cell:BriefingCell)
}

class BriefingCell: BaseTableViewCell {

    // MARK: -  outlets
    @IBOutlet weak var imvAvatar: RoundImageView!
    @IBOutlet weak var lblNameAndDate: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var vwAttachments: UIView!
    @IBOutlet weak var stackAttachments: UIStackView!
    
    @IBOutlet weak var vwQuotesMessage: Corner10View!
    @IBOutlet weak var lblQuoteMessage: UILabel!
    @IBOutlet weak var lblNameDateQuote: UILabel!
    
    @IBOutlet weak var vwMessage: Corner10View!
    @IBOutlet weak var btnReply: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    
    // MARK: -  properties
    var briefing:Briefing?
    var parentBriefing:Briefing? // # nil show quotes parent message
    weak var delegate:BriefingCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectedBackgroundView = UIView()
        config()
    }
    
    private func config() {
        
        lblNameAndDate.font = .systemFont(ofSize: 14)
        lblNameAndDate.textColor = .gray
        
        lblNameDateQuote.font = .systemFont(ofSize: 14)
        lblNameDateQuote.textColor = .gray
        
        lblMessage.textColor = .darkText
        lblQuoteMessage.textColor = .darkText
        lblQuoteMessage.font = .italicSystemFont(ofSize: 16)
        
        btnReply.setTitle("REPLY".localizedString(), for: UIControl.State())
        btnReply.titleLabel?.font = .systemFont(ofSize: 14, weight: .medium)
        
        btnDelete.setTitle("Delete".localizedString(), for: UIControl.State())
        btnDelete.titleLabel?.font = .systemFont(ofSize: 14, weight: .regular)
        btnDelete.setTitleColor(.red, for: UIControl.State())
        
        
        vwMessage.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
    }
    
    @IBAction func actionCell(_ sender: UIButton) {
        switch sender {
        case btnReply:
            delegate?.BriefingCell_replyBriefing(briefing: briefing, cell: self)
        case btnDelete:
            delegate?.BriefingCell_deleteBriefing(briefing: briefing, cell: self)
        default:
            break
        }
    }
    
    /// hiển thị nội dung của briefing
    /// - Parameters:
    ///   - briefing: briefing
    ///   - parentBriefing: quoted briefing
    ///   - delegate: BriefingCellDelegate
    ///   - isCalculatorSize: isCalculatorSize => nếu true sẽ không load image
    func show(briefing:Briefing?, parentBriefing:Briefing?, delegate:BriefingCellDelegate?, isCalculatorSize:Bool = false) {
        self.briefing = briefing
        self.parentBriefing = parentBriefing
        self.delegate = delegate
        
        btnDelete.isHidden = self.briefing?.senderCode != ServiceData.sharedInstance.userModel?.codeTV
        
        configQuoteMessage()
        configAttachments(isCalculatorSize: isCalculatorSize)
        
        guard let briefing = briefing else {
            return
        }
        
        if !isCalculatorSize {
            imvAvatar.loadImageNoProgressBar(url: URL(string: briefing.avatarURL))
        }
        vwMessage.isHidden = briefing.content.count == 0 && (parentBriefing?.content.count ?? 0) == 0
        
        lblMessage.text = briefing.content
        lblMessage.isHidden = briefing.content.count == 0
        btnReply.isHidden = briefing.content.count == 0 //do không có content nên ẩn, hiện tại chưa hỗ trợ quote attachments
        
        lblNameAndDate.text = "\(briefing.senderName), \(briefing.sendTimeValue)"
    }
    
    func configAttachments(isCalculatorSize:Bool) {
        
        stackAttachments.arrangedSubviews.forEach{$0.removeFromSuperview()}
        
        guard let briefing = briefing, briefing.attachmentURL.count > 0 else {
            vwAttachments.isHidden = true
            return
        }
        
        vwAttachments.isHidden = false
        
        let image = ImageView10Corner()
        image.contentMode = .scaleAspectFill
        if !isCalculatorSize {
            image.loadImageNoProgressBar(url: URL(string: briefing.attachmentURL))
        }
        stackAttachments.addArrangedSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        let height = image.heightAnchor.constraint(equalToConstant: 80)
        height.priority = UILayoutPriority(999)
        let ratio = image.widthAnchor.constraint(equalTo: image.heightAnchor, multiplier: 1)
        ratio.priority = UILayoutPriority(999)
        image.addConstraints([height,ratio])
        
        if !isCalculatorSize {
            image.addTargetCustom(target: self, action: #selector(onPress(_:)), keepObject: image)
        }
    }
    
    @objc func onPress(_ sender:Button10Corner) {
        guard let imv = sender.object as? UIImageView, let briefing = self.briefing, imv.image != UIImage(named: "ic_errorImage.png") else { return }
        self.delegate?.BriefingCell_viewImage(url: briefing.attachmentURL, cell: self)
    }
    
    func configQuoteMessage() {
        
        vwQuotesMessage.isHidden = parentBriefing == nil
        
        guard let briefing = parentBriefing, briefing.content.count > 0 else {
            vwQuotesMessage.isHidden = true
            return
        }
        
        lblQuoteMessage.text = briefing.content

        lblNameDateQuote.text = "\(briefing.senderName), \(briefing.sendTimeValue)"
    }

    override func prepareForReuse() {
        imvAvatar.cancelLoadImageProgress()
        parentBriefing = nil
        briefing = nil
        imvAvatar.image = nil
        vwQuotesMessage.isHidden = true
        vwAttachments.isHidden = true
        stackAttachments.arrangedSubviews.forEach{
            $0.removeTargetCustom(target: self, action: #selector(onPress(_:)))
            $0.removeFromSuperview()
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
