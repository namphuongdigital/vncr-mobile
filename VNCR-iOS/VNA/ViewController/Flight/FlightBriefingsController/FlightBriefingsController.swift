//
//  FlightBriefingsController.swift
//  VNA
//
//  Created by Pham Dai on 24/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

protocol FlightBriefingsControllerDelegate:AnyObject {
    func FlightBriefingsController_replyBriefing(briefing: Briefing?, cell: BriefingCell, tableView:UITableView)
    func FlightBriefingsController_deleteBriefing(briefing: Briefing?)
}

class FlightBriefingsController: BaseViewController {

    // MARK: -  outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var comInput: UserInputComponentView!
    
    // MARK: -  properties
    weak var delegate:FlightBriefingsControllerDelegate?
    
    var items:[Briefing]
    var repliedCell:BriefingCell?
    var repliedBriefing:Briefing?
    
    init(items:[Briefing], delegate:FlightBriefingsControllerDelegate?) {
        self.items = items
        super.init(nibName: String(describing: FlightBriefingsController.self), bundle: .main)
        self.delegate = delegate
    }
    
    required init?(coder: NSCoder) {
        items = []
        super.init(coder: coder)
    }
    
    func setReplied(repliedBriefing:Briefing?,repliedCell:BriefingCell?) {
        self.repliedCell = repliedCell
        self.repliedBriefing = repliedBriefing
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        config()
        
        tableView.reloadData()
        
        tableView.isScrollEnabled = parent == nil
        comInput.isHidden = parent != nil
    }
    
    private func config() {
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(BriefingCell.nib, forCellReuseIdentifier: BriefingCell.identifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell") // truong hop khogn tim thay briefing cell
        tableView.estimatedRowHeight = 250
        if #available(iOS 11.0, *) {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.view.safeAreaInsets.bottom + 20, right: 0)
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        }
    }
    
    /// tính toán tổng chiều dài của view
    /// - Returns: height của viewcontroller này (tableview contentsize, inset top|bottom)
    func getRealHeightBriefings() -> CGFloat {
        
        var height:CGFloat = 0
        
        items.forEach {
            if let flagCell = Bundle.main.loadNibNamed(String(describing: BriefingCell.self), owner: nil, options: nil)?.first as? BriefingCell {
                
                let briefing = $0
                let parent = items.first(where: {$0.id == briefing.replyID})
                
                flagCell.show(briefing: briefing, parentBriefing: parent, delegate: nil, isCalculatorSize: true)
                flagCell.layoutIfNeeded()
                let size = flagCell.systemLayoutSizeFitting(CGSize(width: self.view.frame.width, height: CGFloat.infinity))
                height += size.height
            }
        }
        
        return height + tableView.contentInset.top + tableView.contentInset.bottom
    }
    
    func show(items:[Briefing]) {
        self.items = items
        tableView.reloadData()
    }
    
    func insert(item:Briefing) {
        tableView.beginUpdates()
        items.append(item)
        tableView.insertRows(at: [IndexPath(row: items.count, section: 0)], with: .bottom)
        tableView.reloadData()
    }
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension FlightBriefingsController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: BriefingCell.identifier) as? BriefingCell {
            
            let briefing = items[indexPath.row]
            let parent = items.first(where: {$0.id == briefing.replyID})
            
            cell.show(briefing: briefing, parentBriefing: parent, delegate: self)
            
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: -  BriefingCellDelegate
extension FlightBriefingsController: BriefingCellDelegate {
    func BriefingCell_deleteBriefing(briefing: Briefing?, cell: BriefingCell) {
        self.delegate?.FlightBriefingsController_deleteBriefing(briefing: briefing)
    }
    
    func BriefingCell_replyBriefing(briefing: Briefing?, cell: BriefingCell) {
        setReplied(repliedBriefing: briefing, repliedCell: cell)
        self.delegate?.FlightBriefingsController_replyBriefing(briefing: briefing, cell: cell,tableView: tableView)
    }
    
    func BriefingCell_viewImage(url: String, cell: BriefingCell) {
        let browser = SKPhotoBrowser(photos: [SKPhoto(url: url)], initialPageIndex: 0)
        self.present(browser, animated: true, completion: nil)
    }
}
