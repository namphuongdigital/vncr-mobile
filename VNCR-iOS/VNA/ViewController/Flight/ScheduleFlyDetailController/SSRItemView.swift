//
//  SSRItemView.swift
//  VNA
//
//  Created by Pham Dai on 23/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class SSRItemView: BaseView {

    @IBOutlet weak var imvAvatar: RoundImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    var identifier:String?
    
    override init() {
        super.init(frame: .zero)
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init()
        config()
    }
    
    override func config() {
        lblTitle.font = .systemFont(ofSize: 14, weight: .medium)
        lblDescription.font = .systemFont(ofSize: 14)
        
        lblTitle.textColor = .black
        lblDescription.textColor = .gray
        
        imvAvatar.backgroundColor = #colorLiteral(red: 0.9272654653, green: 0.927420795, blue: 0.9272450805, alpha: 0.752046131)
    }
    
    func show(title:String?, description:String?, avatar:String?, titleColor:String?, descriptionColor:String?, identifier:String?) {
        
        self.identifier = identifier
        self.lblTitle.text = title
        lblDescription.text = description
        
        if let v = titleColor, v.count > 0 {
            lblTitle.textColor = UIColor(v)
        }
        
        if let v = descriptionColor, v.count > 0 {
            lblDescription.textColor = UIColor(v)
        }
        
        imvAvatar.loadImageNoProgressBar(url: URL(string: avatar ?? ""))
        
        lblDescription.isHidden = description == nil
    }
}
