//
//  ScheduleFlyDetailController.swift
//  VNA
//
//  Created by Dai Pham on 08/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import Photos
import SwiftyJSON

class ScheduleFlyDetailController: BaseViewController, UIScrollViewDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewFirst: UIScrollView!
    @IBOutlet weak var btnSegment: UISegmentedControl!
    
    @IBOutlet weak var stackACInfoSpecial: UIStackView!
    @IBOutlet weak var stackDepartArrivalInfor: UIStackView!
    @IBOutlet weak var stackSMWHCRVIPUMINF: UIStackView!
    
    @IBOutlet weak var lblTypeApl: UILabel!
    @IBOutlet weak var lblDepartureCode: UILabel!
    @IBOutlet weak var lblArrivalCode: UILabel!
    @IBOutlet weak var lblDeaprtureNameAirport: UILabel!
    @IBOutlet weak var lblArrivalNameAirport: UILabel!
    @IBOutlet weak var lblTimeDepartUTC: UILabel!
    @IBOutlet weak var lblTimeArrivalUTC: UILabel!
    @IBOutlet weak var iconFlight: UIImageView!
    @IBOutlet weak var stackContainer: UIStackView!
    @IBOutlet weak var lblDistanceTime: UILabel!
    
    // Flight common infor(s)
    @IBOutlet weak var lblFlightTotal1: UILabel!
    @IBOutlet weak var lblFlightTotal2: UILabel!
    @IBOutlet weak var lblFlightTotal3: UILabel!
    @IBOutlet weak var lblFlightTotal4: UILabel!
    @IBOutlet weak var lblFlightTotal5: UILabel!
    @IBOutlet weak var lblLabelSSR: UILabel! // for personal schedule
    
    // depart & arrival information
    @IBOutlet weak var lblCarry: UILabel!
    @IBOutlet weak var lblDepartTerminal: UILabel!
    @IBOutlet weak var lblDepartGate: UILabel!
    @IBOutlet weak var lblDepartBay: UILabel!
    @IBOutlet weak var lblArrivalTerminal: UILabel!
    @IBOutlet weak var lblArrivalGate: UILabel!
    @IBOutlet weak var lblArrivalBay: UILabel!
    @IBOutlet weak var lblParking: UILabel!
    @IBOutlet weak var lblTimeDepartLocal: UILabel!
    @IBOutlet weak var lblTimeArrivalLocal: UILabel!
    
    // a/c infor & special
    @IBOutlet var lblTitleACInfoSpecials: [UILabel]!
    
    @IBOutlet weak var lblCapacityI: UILabel!
    @IBOutlet weak var lblCapacityW: UILabel!
    @IBOutlet weak var lblCapacityY: UILabel!
    
    @IBOutlet weak var lblBookedI: UILabel!
    @IBOutlet weak var lblBookedW: UILabel!
    @IBOutlet weak var lblBookedY: UILabel!
    
    @IBOutlet weak var lblCheckedInI: UILabel!
    @IBOutlet weak var lblCheckedInW: UILabel!
    @IBOutlet weak var lblCheckedInY: UILabel!
    
    // ssr
    @IBOutlet weak var vwTrips: UIView!
    @IBOutlet weak var stackSSR: UIStackView!
    @IBOutlet weak var btnViewMoreSsr: UIButton!
    
    // briefing
    @IBOutlet weak var vwBriefings: UIView!
    
    // user input
    @IBOutlet weak var comUserInput: UserInputComponentView!

    var refreshControl:UIRefreshControl?
    
    var briefingsController:FlightBriefingsController?
    let maxShowSSRItems:Int = 8 // config max items displayed ssr
    
    var scheduleFly: ScheduleFlightModel?
    var titleText:String?
    var onGoto:((ScheduleFlyActionController.ScheduleFlyAction)->Void)?
    var detailType:ScheduleFlyActionController.ScheduleFlyActionType = .normal //dung de nhan biet detail cua personal hay cua flight
    var leadReportPositionViewController:LeadReportPositionViewController!
    
    init(detailType:ScheduleFlyActionController.ScheduleFlyActionType, scheduleFly: ScheduleFlightModel?, titleText:String?, action:((ScheduleFlyActionController.ScheduleFlyAction)->Void)?) {
        super.init(nibName: String(describing: type(of: self)), bundle: .main)
        self.scheduleFly = scheduleFly
        self.titleText = titleText
        self.onGoto = action
        self.detailType = detailType
        leadReportPositionViewController = UIStoryboard.init(name: "Main_iphone", bundle: nil).instantiateViewController(withIdentifier: "LeadReportPositionViewController") as? LeadReportPositionViewController
        leadReportPositionViewController.scheduleFlyModel = scheduleFly
        addChild(leadReportPositionViewController)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        leadReportPositionViewController = UIStoryboard.init(name: "Main_iphone", bundle: nil).instantiateViewController(withIdentifier: "LeadReportPositionViewController") as? LeadReportPositionViewController
        leadReportPositionViewController.scheduleFlyModel = scheduleFly
        addChild(leadReportPositionViewController)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // add leadreport view
        stackContainer.addArrangedSubview(leadReportPositionViewController.view)
        
        config()
        loadData()
        refresh()
        
        refreshControl = UIRefreshControl()
        refreshControl?.attributedTitle = nil//NSAttributedString(string: "pull_to_refresh".localized())
        refreshControl?.addTarget(self, action: #selector(self.pullToRefresh), for: UIControl.Event.valueChanged)
        scrollViewFirst.addSubview(refreshControl!)
    }
    
    @objc func pullToRefresh() {
        self.refreshControl?.beginRefreshing()
        refresh()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        scrollViewFirst.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 20 + (UIScreen.bounceWindow.height - btnSegment.frame.minY), right: 0)
    }
    
    @objc func goto(_ sender:UIBarButtonItem) {
        guard let schedule = scheduleFly else {return}
        
        let vc = ScheduleFlyActionController(listType: self.detailType, scheduleFly: schedule,action: onGoto)
        // show popover cho ipad
        if isIpad {
            vc.popoverPresentationController?.barButtonItem = sender
            vc.isModalInPopover = false
        }
        self.present(vc: vc, transitioning: SlideUpAnimateTransitionDelegate(), completion: nil)
    }
    
    private func config() {
        
        comUserInput.controller = self
        comUserInput.delegate = self
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu_verticle").resizeImage(newSize: CGSize(width: 24, height: 24)), style: .done, target: self, action: #selector(goto(_:)))
        
        scrollView.delegate = self
        
        // cau hinh controls
        lblArrivalNameAirport.font = .systemFont(ofSize: 16, weight: .regular)
        lblArrivalNameAirport.textColor = .gray
        
        lblDeaprtureNameAirport.font = .systemFont(ofSize: 16, weight: .regular)
        lblDeaprtureNameAirport.textColor = .gray
        
        lblDepartureCode.font = .systemFont(ofSize: 30, weight: .medium)
        lblArrivalCode.font = .systemFont(ofSize: 30, weight: .medium)
        lblDistanceTime.font = .systemFont(ofSize: 20, weight: .medium)
        
        iconFlight.image = #imageLiteral(resourceName: "ic_flightTo")
        
        lblTypeApl.font = .systemFont(ofSize: 16, weight: .medium)
        lblTypeApl.textColor = .black
        
        lblLabelSSR.font = .systemFont(ofSize: 17, weight: .medium)
        lblLabelSSR.textColor = .black
        
        [lblFlightTotal1,
         lblFlightTotal2,
         lblFlightTotal3,
         lblFlightTotal4,
         lblFlightTotal5
        ].forEach {
            $0?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            $0?.textColor = .gray
            $0?.adjustsFontSizeToFitWidth = true
            $0?.textAlignment = .left
        }
        
        [lblCapacityI,
         lblCapacityW,
         lblCapacityY,
         lblBookedI,
         lblBookedW,
         lblBookedY,
         lblCheckedInI,
         lblCheckedInW,
         lblCheckedInY
        ].forEach {
            $0?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            $0?.textColor = .gray
            $0?.numberOfLines = 0
            $0?.textAlignment = .left
        }
        
        lblTitleACInfoSpecials.forEach {
            $0.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            $0.textColor = .gray
            $0.textAlignment = .left
            $0.adjustsFontSizeToFitWidth = true
        }
        
        [lblDepartTerminal,lblDepartGate,lblDepartBay,lblArrivalTerminal,lblArrivalGate,lblArrivalBay,
         lblTimeDepartUTC, lblTimeArrivalUTC,
        lblParking,
        lblTimeDepartLocal,lblTimeArrivalLocal].forEach {
            $0?.font = UIFont.systemFont(ofSize: 16, weight: .medium)
            $0?.textColor = .gray
            $0?.numberOfLines = 0
            $0?.textAlignment = .left
        }
        
        setupSpacingBetweenItems(size: self.view.frame.size)
        
        btnViewMoreSsr.setTitle("VIEW MORE".localizedString(), for: .normal)
        btnViewMoreSsr.setTitle("VIEW LESS".localizedString(), for: .selected)
        
        btnSegment.setTitle("Crew".localizedString(), forSegmentAt: 1)
        btnSegment.setTitle("Info".localizedString(), forSegmentAt: 0)
        btnSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white,
                                           NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .medium)], for: UIControl.State())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let labelTitle = UILabel()
        labelTitle.textColor = .white
        labelTitle.text = "\(scheduleFly?.flightNo ?? "")/\(scheduleFly?.flightDate ?? "")"
        labelTitle.font = UIFont.systemFont(ofSize: 24, weight: .bold)
        navigationItem.titleView = labelTitle
    }
    
    @IBAction func `switch`(_ sender: UISegmentedControl) {
        
        self.view.endEditing(true)
        
        scrollView.setContentOffset(CGPoint(x: sender.selectedSegmentIndex * Int(self.scrollView.frame.width), y: 0), animated: false)
        
        configUIWithSegment()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        btnSegment.selectedSegmentIndex = Int(scrollView.contentOffset.x / scrollView.frame.width)
        configUIWithSegment()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            btnSegment.selectedSegmentIndex = Int(scrollView.contentOffset.x / scrollView.frame.width)
            configUIWithSegment()
        }
    }
    
    func configUIWithSegment() {
        let contraintViaSafeArea = btnSegment.superview?.constraints.first(where: {$0.identifier == "safeArea"})
        let contraintViaUserInput = btnSegment.superview?.constraints.first(where: {$0.identifier == "userInput"})
        
        contraintViaSafeArea?.priority = UILayoutPriority(btnSegment.selectedSegmentIndex == 0 ? 1 : 999)
        contraintViaUserInput?.priority = UILayoutPriority(btnSegment.selectedSegmentIndex == 0 ? 999 : 1)
        
        UIView.animate(withDuration: 0.25) {[weak self] in
            guard let `self` = self else { return }
            if self.btnSegment.selectedSegmentIndex == 1 {
                self.comUserInput.isHidden = self.btnSegment.selectedSegmentIndex == 1
                self.btnSegment.superview?.layoutIfNeeded()
            } else {
                self.btnSegment.superview?.layoutIfNeeded()
                self.comUserInput.isHidden = self.btnSegment.selectedSegmentIndex == 1
            }
        }
        
        // show button upload for crews list
        if btnSegment.selectedSegmentIndex == 1 {
            leadReportPositionViewController.addUploadButton()
        } else {
            leadReportPositionViewController.removeUploadButton()
        }
    }
    
    func setupSpacingBetweenItems(size:CGSize) {
        self.stackDepartArrivalInfor.spacing = size.width > size.height ? 50 : 20
        self.stackACInfoSpecial.spacing = size.width > size.height ? 50 : 20
        self.stackSMWHCRVIPUMINF.spacing = size.width > size.height ? 30 : 15
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        setupSpacingBetweenItems(size: size)
    }
    
    
    @IBAction func actionMoreSSR(_ sender: UIButton) {
        guard let scheduleFly = scheduleFly, scheduleFly.ssrItems.count > maxShowSSRItems else {
            sender.isHidden = true
            return
        }
        let isMore = !sender.isSelected
        
        var items:[CommonItem] = []
        if isMore {
            if stackSSR.arrangedSubviews.count == scheduleFly.ssrItems.count {return} // prevent loop neu nhu chuc nang nay co van de
            
            let number = scheduleFly.ssrItems.count - maxShowSSRItems
            items = Array(scheduleFly.ssrItems.prefix(number))
            items.forEach { item in
                let ssrView = SSRItemView()
                ssrView.isHidden = true
                ssrView.show(title: item.title, description: item.Description,avatar: item.imageURL, titleColor: item.titleColor, descriptionColor: item.descriptionColor, identifier: item.id)
                self.stackSSR.addArrangedSubview(ssrView)
            }
            
            UIView.animate(withDuration: 0.2, delay: 0, options: [.transitionFlipFromTop]) {
                self.stackSSR.arrangedSubviews.filter({$0.isHidden}).forEach({$0.isHidden = false})
            } completion: { bool in
                
            }
        } else {
            let views = stackSSR.arrangedSubviews.enumerated().filter { e in
                return e.offset >= maxShowSSRItems
            }
            UIView.animate(withDuration: 0.2, delay: 0, options: [.transitionFlipFromBottom]) {
                views.forEach{$0.element.isHidden = true}
            } completion: { bool in
                views.forEach{$0.element.removeFromSuperview()}
                if self.stackSSR.arrangedSubviews.count == 0 {return}
                let rect = self.stackSSR.convert(self.stackSSR.arrangedSubviews.last!.frame, to: self.scrollView)
                let rect1 = self.scrollView.convert(self.stackSSR.arrangedSubviews.last!.frame, to: self.scrollView)
                self.scrollView.setContentOffset(CGPoint(x: 0, y: rect.origin.y - rect1.origin.y + rect1.size.height), animated: false)
            }
        }
        
        sender.isSelected = !sender.isSelected
    }
    
    func refresh(completion: COMPLETEDNORESULT = nil) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceDataCD.getFlightDetail(flightId: scheduleFly?.flightID ?? 0) {[weak self] object, error in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.hideMBProgressHUD(true)
                self.refreshControl?.endRefreshing()
                
                if let error = error {
                    self.showAlert("Alert".localizedString(), stringContent: error)
                } else {
                    self.scheduleFly = object
                    self.loadData()
                }
                completion?()
            }
        }
    }
    
    func loadData() {
        guard let scheduleFly = scheduleFly else {return}
        
        self.scheduleFly = scheduleFly
        
        var typeAplAttribute:[UILabel.ELEMENT_ATTRIBUTE] = []
        if scheduleFly.typeAPL.count > 0 {
            typeAplAttribute.append((text:"Type: ",color:.gray,font:nil))
            typeAplAttribute.append((text:"\(scheduleFly.typeAPL)",color:.black,font:nil))
        }
        
        var carryAttribute:[UILabel.ELEMENT_ATTRIBUTE] = []
        if scheduleFly.carry.count > 0 {
            carryAttribute.append((text:"Carry: ",color:.gray,font:nil))
            carryAttribute.append((text:"\(scheduleFly.carry)",color:.black,font:nil))
        }
        
        lblTypeApl.isHidden = typeAplAttribute.count == 0
        self.lblTypeApl.setAttribute(elements: typeAplAttribute)
        lblCarry.isHidden = carryAttribute.count == 0
        self.lblCarry.setAttribute(elements: carryAttribute)
        
        lblDistanceTime.text = scheduleFly.flightDistanceTime
        
        lblDepartureCode.text = scheduleFly.departureCodeValue
        lblArrivalCode.text = scheduleFly.arrivalCodeValue
        
        lblDepartureCode.textColor = UIColor(scheduleFly.departureCodeTextColor)
        lblArrivalCode.textColor = UIColor(scheduleFly.arrivalCodeTextColor)
        
        lblArrivalNameAirport.text = scheduleFly.arrivalNameValue
        lblDeaprtureNameAirport.text = scheduleFly.departureNameValue
        
        lblArrivalNameAirport.textColor = UIColor(scheduleFly.arrivalNameTextColor)
        lblDeaprtureNameAirport.textColor = UIColor(scheduleFly.departureNameTextColor)
        
        // special items
        lblFlightTotal1.setAttribute(elements: [(text:"VIP : ",color:nil,font:nil),(text:String(format: "%d", scheduleFly.totalVIP),color:.black,font:nil)])
        lblFlightTotal2.setAttribute(elements: [(text:"UM : ",color:nil,font:nil),(text:String(format: "%d", scheduleFly.totalUM),color:.black,font:nil)])
        lblFlightTotal3.setAttribute(elements: [(text:"Inf : ",color:nil,font:nil),(text:String(format: "%d", scheduleFly.totalINF),color:.black,font:nil)])
        lblFlightTotal4.setAttribute(elements: [(text:"WCH : ",color:nil,font:nil),(text:String(format: "%d", scheduleFly.totalWchr),color:.black,font:nil)])
        lblFlightTotal5.setAttribute(elements: [(text:"SM : ",color:nil,font:nil),(text:String(format: "%d", scheduleFly.totalSM),color:.black,font:nil)])
        
        // depart & arrival information
        self.lblTimeDepartUTC.setAttribute(elements: [(text:("UTC".localizedString() + "\n"),color:nil,font:nil),(text:scheduleFly.departureUTCValue,color:.black,font:nil)])
        self.lblTimeArrivalUTC.setAttribute(elements: [(text:("UTC".localizedString() + "\n"),color:nil,font:nil),(text:scheduleFly.arrivalUTCValue,color:.black,font:nil)])
        
        self.lblTimeDepartLocal.setAttribute(elements: [(text:("Local".localizedString() + "\n"),color:nil,font:nil),(text:scheduleFly.departureTimeValue,color:.black,font:nil)])
        self.lblTimeArrivalLocal.setAttribute(elements: [(text:("Local".localizedString() + "\n"),color:nil,font:nil),(text:scheduleFly.arrivalTimeValue,color:.black,font:nil)])
        
        lblDepartTerminal.setAttribute(elements: [(text:("Terminal".localizedString() + " "),color:nil,font:nil),(text:scheduleFly.departureTerminal,color:.black,font:nil)])
        lblDepartGate.setAttribute(elements: [(text:("Gate".localizedString() + " "),color:nil,font:nil),(text:scheduleFly.departureGate,color:.black,font:nil)])
        lblDepartBay.setAttribute(elements: [(text:("Bay".localizedString() + " "),color:nil,font:nil),(text:scheduleFly.departureBay,color:.black,font:nil)])
        
        lblArrivalTerminal.setAttribute(elements: [(text:("Terminal".localizedString() + " "),color:nil,font:nil),(text:scheduleFly.arrivalTerminal,color:.black,font:nil)])
        lblArrivalGate.setAttribute(elements: [(text:("Belt".localizedString() + " "),color:nil,font:nil),(text:scheduleFly.arrivalBelt,color:.black,font:nil)])
        lblArrivalBay.setAttribute(elements: [(text:("Bay".localizedString() + " "),color:nil,font:nil),(text:scheduleFly.arrivalBay,color:.black,font:nil)])
        
        lblParking.setAttribute(elements: [(text:("Parking".localizedString() + " "),color:nil,font:nil),(text:scheduleFly.parking,color:.black,font:nil)])
        lblParking.isHidden = scheduleFly.parking.count == 0
        
        // a/c infor & special
        lblCapacityI.setAttribute(elements: [(text:("I".localizedString() + ": "),color:nil,font:nil),(text:"\(scheduleFly.capacityC)",color:.black,font:nil)])
        lblCapacityW.setAttribute(elements: [(text:("W".localizedString() + ": "),color:nil,font:nil),(text:"\(scheduleFly.capacityW)",color:.black,font:nil)])
        lblCapacityY.setAttribute(elements: [(text:("Y".localizedString() + ": "),color:nil,font:nil),(text:"\(scheduleFly.capacityY)",color:.black,font:nil)])
        
        lblBookedI.setAttribute(elements: [(text:("I".localizedString() + ": "),color:nil,font:nil),(text:"\(scheduleFly.bookedC)",color:.black,font:nil)])
        lblBookedW.setAttribute(elements: [(text:("W".localizedString() + ": "),color:nil,font:nil),(text:"\(scheduleFly.bookedW)",color:.black,font:nil)])
        lblBookedY.setAttribute(elements: [(text:("Y".localizedString() + ": "),color:nil,font:nil),(text:"\(scheduleFly.bookedY)",color:.black,font:nil)])
        
        lblCheckedInI.setAttribute(elements: [(text:("I".localizedString() + ": "),color:nil,font:nil),(text:"\(scheduleFly.checkedInC)",color:.black,font:nil)])
        lblCheckedInW.setAttribute(elements: [(text:("W".localizedString() + ": "),color:nil,font:nil),(text:"\(scheduleFly.checkedInW)",color:.black,font:nil)])
        lblCheckedInY.setAttribute(elements: [(text:("Y".localizedString() + ": "),color:nil,font:nil),(text:"\(scheduleFly.checkedInY)",color:.black,font:nil)])
        
        // ssr
        stackSSR.arrangedSubviews.forEach{$0.removeFromSuperview()}
        btnViewMoreSsr.isHidden = scheduleFly.ssrItems.count <= maxShowSSRItems
        btnViewMoreSsr.isSelected = false
        if(scheduleFly.ssrItems.count > 0) {
            for item in scheduleFly.ssrItems.prefix(maxShowSSRItems) {
                let ssrView = SSRItemView()
                ssrView.isHidden = true
                ssrView.show(title: item.title, description: item.Description,avatar: item.imageURL, titleColor: item.titleColor, descriptionColor: item.descriptionColor, identifier: item.id)
                self.stackSSR.addArrangedSubview(ssrView)
                UIView.animate(withDuration: 0.2, delay: 0, options: [.transitionFlipFromBottom]) {
                    self.stackSSR.arrangedSubviews.filter({$0.isHidden}).forEach({$0.isHidden = false})
                } completion: { bool in
                    
                }
            }
        }
        self.lblLabelSSR.text = "SSR".localizedString()
        vwTrips.isHidden = scheduleFly.ssrItems.count == 0
        
        // briefings
        vwBriefings.isHidden = scheduleFly.briefings.count == 0
        vwBriefings.subviews.first(where: {$0.tag == 19001601})?.removeFromSuperview()
        briefingsController?.removeFromParent()
        briefingsController = FlightBriefingsController(items: scheduleFly.briefings, delegate: self)
        addChild(briefingsController!)
        vwBriefings.addSubview(briefingsController!.view)
        vwBriefings.isHidden = scheduleFly.briefings.count == 0
        briefingsController?.view.tag = 19001601
        briefingsController?.view.translatesAutoresizingMaskIntoConstraints = false
        briefingsController?.view.boundInside(vwBriefings, insets: UIEdgeInsets(top: 60, left: 0, bottom: 0, right: 0))
        DispatchQueue.main.async {
            self.updateHeightBriefingsView()
        }
        
        // show action buttons voi iphone
        /*
        if !isIpad {
            stackContainer.arrangedSubviews.first(where: {$0.tag == 19001600})?.removeFromSuperview()
            self.children.forEach({$0.removeFromParent()})
            let vc = ScheduleFlyActionController(listType: self.detailType, scheduleFly: scheduleFly, action: onGoto)
            addChild(vc)
            vc.view.tag = 19001600
            stackContainer.addArrangedSubview(vc.view)
            vc.hideTitle()
            vc.turnOffEffect()
        }*/
    }
    
    func updateHeightBriefingsView(scrollToEnd:Bool = false, newBriefingId:Int = 0) {
        
        guard let briefingController = briefingsController else {return}
        
        let height = briefingController.getRealHeightBriefings()
        if let c = briefingController.view.constraints.first(where: {$0.identifier == "heightBriefingsView"}) {
            c.constant = height
        } else {
            let c = briefingController.view.heightAnchor.constraint(equalToConstant: height)
            c.identifier = "heightBriefingsView"
            briefingController.view.addConstraint(c)
        }
        UIView.animate(withDuration: 0.25, delay: 0, options: [.transitionFlipFromBottom]) {
            self.view.layoutIfNeeded()
        } completion: { bool in
            if scrollToEnd {
                if let cell = self.briefingsController?.tableView.visibleCells.first(where: { cell in
                    if let c = cell as? BriefingCell {
                        return c.briefing?.id == newBriefingId
                    }
                    return false
                }), let tableView = self.briefingsController?.tableView {
                    let rect = tableView.convert(cell.frame, to: self.scrollViewFirst)
                    let miliseconds = self.comUserInput.duration * 1000
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(miliseconds))) {
                        self.scrollViewFirst.setContentOffset(CGPoint(x: 0, y: rect.origin.y - self.comUserInput.frame.minY + rect.size.height), animated: true)
                    }
                }
            }
        }
    }
    
    override func actionBarRightButton() {
        // TODO: request detail and update last update
        let currentPosition = scrollViewFirst.contentOffset.y
        refresh {[weak self] in
            guard let `self` = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
                self.scrollViewFirst.setContentOffset(CGPoint(x: 0, y: currentPosition), animated: false)
            }
        }
    }
}

// MARK: -  UserInputComponentViewDelegate
extension ScheduleFlyDetailController: UserInputComponentViewDelegate {
    func scrollToRepliedBriefing () {
        if let cell = briefingsController?.repliedCell, let tableView = briefingsController?.tableView {
            let rect = tableView.convert(cell.frame, to: scrollViewFirst)
            let miliseconds = comUserInput.duration * 1000
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(miliseconds))) {
                self.scrollViewFirst.setContentOffset(CGPoint(x: 0, y: rect.origin.y - self.comUserInput.frame.minY + rect.size.height), animated: true)
            }
        }
    }
    
    func UserInputComponentView_send(assets: [PHAsset], content: String, component: UserInputComponentView) {
        #if DEBUG
        print("\(#function) ====Send briefing====\nPhotos: \(assets.count)\nContent: \(content)\n=======")
        #endif
        
        guard let scheduleFly = scheduleFly else {
            return
        }
        
        var replyId = 0
        var base64Image:String = ""
        
        if let replyBriefing = briefingsController?.repliedBriefing {
            replyId = replyBriefing.id
        }
        
        let group = DispatchGroup()
        
        if assets.count > 0, let asset = assets.first {
            group.enter()
            asset.getDataToUpload { url, data, error in
                if let data = data {
                    base64Image = data.base64EncodedString()
                }
                group.leave()
            }
        }
        
        group.notify(queue: .main) {[weak self] in
            guard let `self` = self else { return }
            // empty content for new comment
            self.showMBProgressHUD("", animated: true)
            ServiceData.sharedInstance.taskSaveBriefing(flightId: scheduleFly.flightID, content: content, base64Image: base64Image, replyId: replyId).continueOnSuccessWith(continuation: {[weak self] task in
                #if DEBUG
                print("\(task) \(#function)")
                #endif
                self?.hideMBProgressHUD(true)
                if let listData = try? (task as? JSON)?.rawData(),
                   let object:Briefing = try? listData.load() {
                    self?.vwBriefings.isHidden = false
                    self?.scheduleFly?.briefings.append(object)
                    self?.briefingsController?.show(items: self?.scheduleFly?.briefings ?? [])
                    self?.updateHeightBriefingsView(scrollToEnd: true,newBriefingId: object.id)
                }
                component.clear()
            }).continueOnErrorWith(continuation: { error in
                self.hideMBProgressHUD(true)
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            })
        }
    }
    
    func UserInputComponentView_selectedAssets(assets: [PHAsset], component: UserInputComponentView) {
        scrollToRepliedBriefing()
    }
    
    func UserInputComponentView_clearQuote(component: UserInputComponentView) {
        briefingsController?.setReplied(repliedBriefing: nil, repliedCell: nil)
    }
    
    func UserInputComponentView_willDismissKeyboard(component: UserInputComponentView) {
        let contraintViaSafeArea = btnSegment.superview?.constraints.first(where: {$0.identifier == "safeArea"})
        let contraintViaUserInput = btnSegment.superview?.constraints.first(where: {$0.identifier == "userInput"})
        
        contraintViaSafeArea?.priority = UILayoutPriority(1)
        contraintViaUserInput?.priority = UILayoutPriority(999)
        
        UIView.animate(withDuration: 0.25) {[weak self] in
            guard let `self` = self else { return }
            self.btnSegment.superview?.layoutIfNeeded()
        }
    }
    
    func UserInputComponentView_willShowKeyboard(component: UserInputComponentView) {
        let contraintViaSafeArea = btnSegment.superview?.constraints.first(where: {$0.identifier == "safeArea"})
        let contraintViaUserInput = btnSegment.superview?.constraints.first(where: {$0.identifier == "userInput"})
        
        contraintViaSafeArea?.priority = UILayoutPriority(999)
        contraintViaUserInput?.priority = UILayoutPriority(1)
        
        UIView.animate(withDuration: 0.25) {[weak self] in
            guard let `self` = self else { return }
            self.btnSegment.superview?.layoutIfNeeded()
        }
    }
}

// MARK: - FlightBriefingsControllerDelegate
extension ScheduleFlyDetailController:FlightBriefingsControllerDelegate {
    func FlightBriefingsController_deleteBriefing(briefing: Briefing?) {
        guard let briefing = briefing,let scheduleFly = scheduleFly else {return}
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
            guard let `self` = self else {
                return
            }
            self.showMBProgressHUD("Update...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskDeleteBriefing(flightId: scheduleFly.flightID, id: briefing.id).continueOnSuccessWith(continuation: { task in
                self.hideMBProgressHUD(true)
                if let index = self.scheduleFly?.briefings.firstIndex(where: {$0.id == briefing.id}) {
                    self.scheduleFly?.briefings.remove(at: index)
                    self.briefingsController?.show(items: self.scheduleFly?.briefings ?? [])
                }
                self.updateHeightBriefingsView(scrollToEnd: false)
                self.vwBriefings.isHidden = self.scheduleFly?.briefings.count ?? 0 == 0
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you sure delete ?".localizedString(), message: "", actions: [yesAction, cancelAction])
    }
    
    func FlightBriefingsController_replyBriefing(briefing: Briefing?, cell: BriefingCell, tableView: UITableView) {

        comUserInput.isFirstResponder = true
        
        // calculator position replied cell
        let rect = tableView.convert(cell.frame, to: scrollViewFirst)
        // doi den khi keyboard show, de lay origin chinh xac cua comUserInput
        let miliseconds = comUserInput.duration * 1000
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Int(miliseconds))) {
            self.comUserInput.showQuote(message: briefing?.content, nameDate: "\(briefing?.senderName ?? ""), \(briefing?.sendTimeValue ?? "")")
            self.scrollViewFirst.setContentOffset(CGPoint(x: 0, y: rect.origin.y - self.comUserInput.frame.minY + rect.size.height), animated: true)
        }
    }
}
