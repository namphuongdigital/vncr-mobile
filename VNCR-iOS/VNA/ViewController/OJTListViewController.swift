//
//  OJTListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class OJTListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, OJTTableViewCellDelegate {
    
    var cid: String = ""

    var isListSaved: Bool = false
    
    weak var scheduleFlyModel: ScheduleFlightModel?
    
    var refreshControl: UIRefreshControl!
    
    var segmentedControl: UISegmentedControl = UISegmentedControl(frame: CGRect.zero)
    
    @IBOutlet weak var tableView: UITableView!
    
    var listOJTModel = [OJTModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        let nib = UINib(nibName: "OJTTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "OJTTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.textTitle = "OJT".localizedString()
        
        self.tableView.alwaysBounceVertical = true
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        getNewData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    @objc func refreshData() {
        getNewData()
    }
    
    func getNewData() {
        if(isListSaved) {
            self.textTitle = "OJT ARCHIVES".localizedString()
            if let listOJTModel = ApplicationData.sharedInstance.getOJTModels() {
                self.listOJTModel = listOJTModel
            }
            self.tableView.reloadData()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        } else {
            ServiceData.sharedInstance.taskOJTList(flightID: self.scheduleFlyModel!.flightID, cid: cid).continueOnSuccessWith(continuation: { task in
                
                let result = task as! JSON
                if let array = result.array {
                    var arrayModel = Array<OJTModel>()
                    for item in array {
                        let model = OJTModel(json: item)
                        arrayModel.append(model)
                    }
                    self.listOJTModel = arrayModel
                    
                }
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            })
        }
        
    }
    
    func configure(cell: OJTTableViewCell, at indexPath: IndexPath) {
        if(isListSaved) {
            cell.ojtTableViewCellDelegate = self
        } else {
            cell.ojtTableViewCellDelegate = nil
        }
        cell.ojtModel = self.listOJTModel[indexPath.row]
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listOJTModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OJTTableViewCell")! as! OJTTableViewCell
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "OJTTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! OJTTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(self.listOJTModel[indexPath.row].isReadOnly == false) {
            if(isListSaved) {
                self.pushOJTDetailViewController(ojtModel: self.listOJTModel[indexPath.row], scheduleFlyModel: nil, isModelSaved: true, cid: self.cid)
            } else {
                self.pushOJTDetailViewController(ojtModel: self.listOJTModel[indexPath.row], scheduleFlyModel: self.scheduleFlyModel!, cid: self.cid)
            }
        }
        
        
    }
    /*
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
     return 0.01
     }
     
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     return 0.01
     }
     */
    
    //MARK - OJTTableViewCellDelegate
    func deleteOJTTableViewCellPress(ojtModel: OJTModel) {
        for index in 0..<self.listOJTModel.count {
            if(self.listOJTModel[index].id == ojtModel.id) {
                self.listOJTModel.remove(at: index)
                break
            }
        }
        ApplicationData.sharedInstance.deleteOJTModel(id: ojtModel.id)
        self.tableView.reloadData()
    }

}
