//
//  MainViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/15/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import QRCodeReader
import AVFoundation

class MainViewController: BaseViewController, UICollectionViewDelegate, UICollectionViewDataSource, FSPagerViewDataSource, FSPagerViewDelegate, QRCodeReaderViewControllerDelegate {
    
    var refreshControl: UIRefreshControl!
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                collectionView?.addSubview(refreshControl)
            }
            
        }
    }
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObject.ObjectType.qr])
            $0.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    var settingsBarButtonItem: UIBarButtonItem {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        button.setImage(UIImage.init(named: "settings-icon-bar.png")!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.addTarget(self, action: #selector(settingBarButtonItemPressed(_:)), for: UIControl.Event.touchUpInside)
        return UIBarButtonItem(customView: button)
    }
    
    var qrCodeBarButtonItem: UIBarButtonItem {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        button.setImage(UIImage.init(named: "settings-icon-qrcode.png")!.withRenderingMode(UIImage.RenderingMode.alwaysTemplate), for: .normal)
        button.addTarget(self, action: #selector(qrCodeBarButtonItemPressed(_:)), for: UIControl.Event.touchUpInside)
        return UIBarButtonItem(customView: button)
    }

    var isLanguageVN: Bool = false
    
    var leftBarButtonItem: UIBarButtonItem!
    
    var listMenuItemModel = [MenuItemModel]()
    
    var listBannerMenuItemModel = [MenuItemModel]()
    
    let columnLayout = ColumnFlowLayout(
        cellsPerRow: UIDevice.current.userInterfaceIdiom == .pad ? 6 : 3,
        minimumInteritemSpacing: 0,
        minimumLineSpacing: 0,
        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    )
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var pagerView: FSPagerView!
    
    var pageControl: FSPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.showsVerticalScrollIndicator = false
        // Do any additional setup after loading the view.
        if let substring = Locale.preferredLanguages.first?.prefix(2) {
            let language = String(substring).lowercased()
            if(language == "vi") {
                self.isLanguageVN = true
            }
            
        }
        
        
        self.loadPagerView()
        
        collectionView?.collectionViewLayout = columnLayout
        if #available(iOS 11.0, *) {
            collectionView?.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }
        
        columnLayout.headerReferenceSize = CGSize(width: UIScreen.bounceWindow.width, height: (UIScreen.bounceWindow.width * 9) / 16)
        self.collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerID")
        
        collectionView?.register(UINib(nibName: "ItemMenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ItemMenuCollectionViewCell")
        
        self.navigationItem.rightBarButtonItems = [settingsBarButtonItem, qrCodeBarButtonItem]
        
        let userView = UserView.loadView()!
        userView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width - 50, height: 40)
        //userView.backgroundColor = UIColor.gray
        userView.labelTitle.text = (ServiceData.sharedInstance.userModel?.nameTV ?? "")
        userView.imageView.loadImageProgress(url: URL.init(string: ServiceData.sharedInstance.userModel!.imageBase64))
        userView.buttonOverlay.addTarget(self, action: #selector(buttonOverlayUserPressed(_:)), for: UIControl.Event.touchUpInside)
        //self.navigationItem.titleView = userView
        /*
         if let navigationBar = navigationController?.navigationBar {
         userView.widthAnchor.constraint(equalTo: navigationBar.widthAnchor, constant: -40).isActive = true
         }
         */
        leftBarButtonItem = UIBarButtonItem(customView: userView)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
        self.collectionView.alwaysBounceVertical = true
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        collectionView.addSubview(refreshControl)
        
        /*
        if(ServiceData.sharedInstance.userModel?.phone != nil && ServiceData.sharedInstance.userModel!.phone != "") {
            ServiceData.sharedInstance.taskRegister4vnc(phoneNo: ServiceData.sharedInstance.userModel!.phone, pushToken: ServiceData.sharedInstance.oneSignalUserId).continueOnSuccessWith(continuation: { task in
                print("")
                
            }).continueOnErrorWith(continuation: { error in
                print("")
            })
        }
        */
        
        tabBarItem.title = ServiceData.sharedInstance.userModel?.firstNameVn ?? "Personal".localizedString()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getNewData()
    }
    
    @objc func refreshData() {
        getNewData()
    }
    
    
    func loadPagerView() {
        let width = isIpad ? 410 : UIScreen.bounceWindow.width
        pagerView = FSPagerView(frame: CGRect(x: 0, y: 0, width: UIScreen.bounceWindow.width, height: (width * 9) / 16.0))
        self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
        self.pagerView.itemSize = FSPagerView.automaticSize
        self.pagerView.itemSize = (isIpad ? CGSize(width: 410, height: self.pagerView.frame.size.height) : self.pagerView.frame.size).applying(CGAffineTransform(scaleX: 0.9, y: 0.9))
        self.pagerView.interitemSpacing = 10
        self.pagerView.automaticSlidingInterval = 4
        self.pagerView.isInfinite = true
        pagerView.delegate = self
        pagerView.dataSource = self
        
        self.pageControl = FSPageControl(frame: CGRect(x: 10, y: self.pagerView.frame.size.height - 32, width: self.pagerView.frame.width - 20, height: 25))
        self.pageControl.numberOfPages = self.listBannerMenuItemModel.count
        self.pageControl.contentHorizontalAlignment = .center
        self.pageControl.contentInsets = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: nil) { context in
            
            let width = self.isIpad ? 410 : UIScreen.bounceWindow.width
            self.pagerView.frame = CGRect(x: 0, y: 0, width: UIScreen.bounceWindow.width, height: (width * 9) / 16.0)
            self.pagerView.itemSize = FSPagerView.automaticSize
            self.pagerView.itemSize = (self.isIpad ? CGSize(width: 410, height: self.pagerView.frame.size.height) : self.pagerView.frame.size).applying(CGAffineTransform(scaleX: 0.9, y: 0.9))
            self.pageControl.frame = CGRect(x: 10, y: self.pagerView.frame.size.height - 32, width: self.pagerView.frame.width - 20, height: 25)
            self.columnLayout.invalidateLayout()
            self.collectionView.reloadData()
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    
    
    func getNewData() {
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        // loaf menu when offline
        if !NetworkObserver.shared.isConnectedInternet,
           #available(iOS 10.0, *),
           let tempData = UserDefaults.standard.data(forKey: "MainMenus"),
           let result = try? JSON(data: tempData)  {
            self.setupData(result: result)
        } else {
            //self.textTitle = "DEVICES".localizedString()
            ServiceData.sharedInstance.taskGetcava().continueOnSuccessWith(continuation: { task in
                
                #if DEBUG
                print("\(task) \(#function)")
                #endif
                let result = task as! JSON
                // cache menu
                if let jsondata = try? result.rawData() {
                    let data = NSData(data: jsondata)
                    UserDefaults.standard.setValue(data, forKey: "MainMenus")
                }
                self.setupData(result: result)
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            })
        }
    }
    
    func setupData(result:JSON) {
        self.listBannerMenuItemModel.removeAll()
        if let array = result["Banners"].array {
            var arrayModel = Array<MenuItemModel>()
            for item in array {
                let model = MenuItemModel(json: item)
                arrayModel.append(model)
            }
            self.listBannerMenuItemModel = arrayModel
            if(self.listBannerMenuItemModel.count > 0) {
                let width = self.isIpad ? 410 : UIScreen.bounceWindow.width
                self.columnLayout.headerReferenceSize = CGSize(width: UIScreen.main.bounds.width, height: (width * 9) / 16)
            } else {
                self.columnLayout.headerReferenceSize = CGSize(width: 0, height: 0)
            }
            self.pageControl.numberOfPages = self.listBannerMenuItemModel.count
            self.pageControl.currentPage = 0
            self.pagerView.reloadData()
            
        }
        self.listMenuItemModel.removeAll()
        if let array = result["Menus"].array {
            var arrayModel = Array<MenuItemModel>()
            for item in array {
                let model = MenuItemModel(json: item)
                arrayModel.append(model)
            }
            self.listMenuItemModel = arrayModel
            
        }
        self.collectionView.reloadData()
        self.hideMBProgressHUD(true)
        self.refreshControl.endRefreshing()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        var v : UICollectionReusableView! = nil
        if kind == UICollectionView.elementKindSectionHeader {
            v = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "headerID", for: indexPath)
            // clear before add, prevent loop
            v.subviews.filter({$0.isKind(of: FSPagerView.self) || $0.isKind(of: FSPageControl.self)}).forEach{$0.removeFromSuperview()}
            v.addSubview(self.pagerView)
            v.addSubview(self.pageControl)
            
            
        }
        return v
        
    }
    
    @objc func buttonHeaderPress(sender: Button10Corner) {
        guard let menuItemModel = sender.object as? MenuItemModel else {return}
        var title = menuItemModel.titleEN
        if(self.isLanguageVN) {
            title = menuItemModel.titleVN
        }
        if menuItemModel.ActionObjectId.count > 0 {
            self.pushWebViewDetailBannerViewController(id: menuItemModel.id, title: title)
        } else {
            NotificationCenter.default.post(name: NSNotification.Name("filterNews"), object: nil, userInfo: ["id":menuItemModel.ActionCategory])
        }
    }
    
    
    //1
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listMenuItemModel.count
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemMenuCollectionViewCell", for: indexPath) as! ItemMenuCollectionViewCell
        cell.menuItemModel = self.listMenuItemModel[indexPath.row]
        if(self.isLanguageVN) {
            cell.textTitleLabel.text = cell.menuItemModel?.titleVN
        } else {
            cell.textTitleLabel.text = cell.menuItemModel?.titleEN
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let menuItemModel = self.listMenuItemModel[indexPath.row]
        if(menuItemModel.category == 2) {
            if(menuItemModel.id == 1) {
                self.pushMessageListViewController()
            } else if(menuItemModel.id == 2) {
                self.pushNewListViewController()
                //self.pushSupportListViewController()
            } else if(menuItemModel.id == 6) {
                self.pushIncomeListViewController()
            }
        } else if(menuItemModel.category == 4) {
            let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "PersonalScheduleFlyViewController") as! PersonalScheduleFlyViewController
            if isIpad {
                let nv = UINavigationController(rootViewController: viewcontroller)
                self.present(BaseSplitController(root: nv, detail: nil), animated: true, completion: nil)
            } else {
                self.navigationController?.pushViewController(viewcontroller, animated: true)
            }
        } else if(menuItemModel.category == 8) {
            self.pushFormListViewController()
        }
        //        else if(menuItemModel.category == 16) {
        //            self.pushStatisticViewController()
        //        }
        else if(menuItemModel.category == 64) {
            self.pushAllFlightReportListViewController()
        } else if(menuItemModel.category == 128) {
            let scheduleFlyModel = ScheduleFlightModel.empty
            self.pushSurveyListViewController(scheduleFlyModel: scheduleFlyModel, isListSaved: false, isSameSchedule: true)
        } else if(menuItemModel.category == 256) {
            self.pushSupportListViewController()
        } else if(menuItemModel.category == 512) {
            self.pushWeeklyReportListViewController()
        } else if(menuItemModel.category == -1) { // scan qr code
            self.scanInModalAction(nil)
        } else if(menuItemModel.category == 0) { // documents
            self.pushItemMenuViewController(menuItem: menuItemModel)
        } else {
            self.pushItemMenuViewController(menuItem: menuItemModel)
        }
        self.collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    @objc func buttonOverlayUserPressed(_ sender: UIButton) {
        self.pushProfileViewController(crewId: ServiceData.sharedInstance.userModel?.crewID ?? "")
    }
    
    @objc func settingBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.pushSettingViewController()
    }
    
    @objc func qrCodeBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.scanInModalAction(nil)
    }
    
    @IBAction func scanInModalAction(_ sender: AnyObject?) {
        guard checkScanPermissions() else { return }
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate = self
        
        readerVC.completionBlock = {[weak self] (result: QRCodeReaderResult?) in
            guard let `self` = self else {
                return
            }
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
                self.verifyCode(codeScan: result.value)
            }
            
            
        }
        present(readerVC, animated: true, completion: nil)
    }
    
    //MARK - QRCodeReaderViewControllerDelegate
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
    }
    
    //
    func verifyCode(codeScan: String) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskDeviceVerifyQR(qrCode: codeScan).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            if let responseServiceModel = task as? ResponseServiceModel {
                let message = responseServiceModel.message ?? "QR code active accepted full".localizedString()
                self.showAlert("Message".localizedString(), stringContent: message)
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    
    // MARK:- FSPagerViewDataSource
    
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return self.listBannerMenuItemModel.count
    }
    
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        let menuItemModel = self.listBannerMenuItemModel[index]
        //cell.imageView?.image = UIImage(named: self.imageNames[index])
        cell.imageView?.loadImageNoProgressBar(url: URL.init(string: menuItemModel.imageUrl))
        cell.imageView?.contentMode = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        if(self.isLanguageVN) {
            cell.textLabel?.text = menuItemModel.titleVN
        } else {
            cell.textLabel?.text = menuItemModel.titleEN
        }
        
        
        let button = Button10Corner(frame: CGRect(x: 0, y: 0, width: self.pagerView.bounds.size.width, height: self.pagerView.bounds.size.height))
        button.object = menuItemModel
        button.removeTarget(self, action: #selector(self.buttonHeaderPress(sender:)), for: .touchUpInside)
        button.addTarget(self, action: #selector(self.buttonHeaderPress(sender:)), for: UIControl.Event.touchUpInside)
        cell.addSubview(button)
        return cell
    }
    
    // MARK:- FSPagerViewDelegate
    
    func pagerView(_ pagerView: FSPagerView, shouldSelectItemAt index: Int) -> Bool {
        return true
    }
    
    
    func pagerView(_ pagerView: FSPagerView, shouldHighlightItemAt index: Int) -> Bool {
        return true
    }
    
    func pagerView(_ pagerView: FSPagerView, didHighlightItemAt index: Int) {
        
    }
    
    
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        //pagerView.deselectItem(at: index, animated: true)
        //pagerView.scrollToItem(at: index, animated: true)
        //let menuItemModel = self.listBannerMenuItemModel[index]
        //self.pushWebViewDetailBannerViewController(id: menuItemModel.id, title: menuItemModel.titleVN)
        
    }
    
    func pagerViewWillEndDragging(_ pagerView: FSPagerView, targetIndex: Int) {
        self.pageControl.currentPage = targetIndex
    }
    
    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {
        self.pageControl.currentPage = pagerView.currentIndex
    }

}

class ColumnFlowLayout: UICollectionViewFlowLayout {
    
    let cellsPerRow: Int
    
    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()
        
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else { return }
        var collectionViewSafeAreaInsetsLeft: CGFloat = 0
        var collectionViewSafeAreaInsetsRight: CGFloat = 0
        if #available(iOS 11.0, *) {
            collectionViewSafeAreaInsetsLeft = collectionView.safeAreaInsets.left
            collectionViewSafeAreaInsetsRight = collectionView.safeAreaInsets.right
        }
        let marginsAndInsets = sectionInset.left + sectionInset.right + collectionViewSafeAreaInsetsLeft + collectionViewSafeAreaInsetsRight + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        itemSize = CGSize(width: itemWidth, height: itemWidth)
    }
    
    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
    
    
    
    
    
}
