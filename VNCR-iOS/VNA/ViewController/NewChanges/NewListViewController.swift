//
//  NewListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/25/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class NewListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var pageIndexNew: Int = 0
    
    var listNewMessageModel = [MessageModel]()
    
    var listFilter = [CommonFilterModel]()
    
    var isAppeared = false
    var isRequesting = false {
        didSet {
            if let wait = waitNewFilter {
                selectedFilter = wait
                waitNewFilter = nil
            }
        }
    }
    var waitNewFilter:String?
    
    var textSearch: String = ""
    
    var selectedFilter: String = "ALL" {
        didSet {
            DispatchQueue.main.async {
                self.showMBProgressHUD("Loading...".localizedString(), animated: true)
                self.getNewData()
            }
        }
    }
    
    var refreshControl: UIRefreshControl!
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView?.addSubview(refreshControl)
            }
            
        }
    }
    
    var listFilterSelected: [FilterModel] = []
    var searchText: String = ""
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero)

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "News".localizedString()
        
        tableView.keyboardDismissMode = .onDrag
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        tableView.register(ArticleCell.nib, forCellReuseIdentifier: ArticleCell.identifier)
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.layoutMargins = .zero
        tableView.separatorStyle = .none
        
        //self.textTitle = "FORM LIST".localizedString()
        
        self.tableView.alwaysBounceVertical = true
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40//isIpad ? 40 : 10
        // Set custom trigger offset
        //self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            self?.loadMoreDataNew() {
                self?.tableView!.finishInfiniteScroll()
            }
            
        }
        
        self.configSearchBar()
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
        
        if #available(iOS 11.0, *) {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.view.safeAreaInsets.bottom + 20, right: 0)
        } else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.view.backgroundColor = UIColor("#166880")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        isAppeared = true
    }
    
    @objc func refreshData() {
        getNewData()
    }

    func getNewData() {
        pageIndexNew = 1
        isRequesting = true
        ServiceData.sharedInstance.taskNewsList(keyword: self.searchText, listFilter: self.selectedFilter, pageIndex: pageIndexNew).continueOnSuccessWith(continuation: { task in
            self.isRequesting = false
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<MessageModel>()
                for item in array {
                    let model = MessageModel(json: item)
                    ApplicationData.sharedInstance.updateMessageModel(message: model)
                    arrayModel.append(model)
                }
                self.listNewMessageModel = arrayModel
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.isRequesting = false
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    func loadMoreDataNew(_ handler: (() -> Void)?) {
        self.isRequesting = true
        if(self.listNewMessageModel.count > 1 && !self.refreshControl.isRefreshing){
            pageIndexNew += 1
            ServiceData.sharedInstance.taskNewsList(keyword: self.searchText, listFilter: self.selectedFilter, pageIndex: pageIndexNew).continueOnSuccessWith(continuation: { task in
                self.isRequesting = false
                let result = task as! JSON
                if let array = result.array, array.count > 0 {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in array {
                        listIndexPath.append(IndexPath(row: self.listNewMessageModel.count, section: 0))
                        let model = MessageModel(json: item)
                        ApplicationData.sharedInstance.updateMessageModel(message: model)
                        self.listNewMessageModel.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                handler?()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.isRequesting = false
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                handler?()
            })
            
            
        } else {
            handler?()
        }
    }

    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listNewMessageModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ArticleCell") as? ArticleCell else {return UITableViewCell()}
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
        return tableView.fd_heightForCell(withIdentifier: "ArticleCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! ArticleCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//    }
    
//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//        return indexPath
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model: MessageModel = self.listNewMessageModel[indexPath.row]
        
        if(model.isHtml) {
            self.pushWebViewNewsViewController(id: model.id, title: model.sender)
        } else {
            self.pushNewDetailViewController(messageModel: model)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 0.01
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 0.01
//    }
    
    func configure(cell: ArticleCell, at indexPath: IndexPath) {
        
        cell.labelContent.numberOfLines = 4
        cell.labelTitle.numberOfLines = 2
        cell.loadContent(messageModel: self.listNewMessageModel[indexPath.row])
        
    }
    
}

extension NewListViewController: UISearchBarDelegate {
    
    // MARK: - Config Search Bar
    private func configSearchBar() {
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.placeholder = "Search".localizedString()
        self.searchBar.delegate = self
        self.searchBar.sizeToFit()
        self.searchBar.showsBookmarkButton = true
        self.searchBar.setImage(UIImage(named: "ic_filter")?.resizeImageWith(newSize: CGSize(width: 20, height: 20)).withRenderingMode(.alwaysOriginal), for: .bookmark, state: .normal)
        //self.tableView.tableHeaderView = self.searchBar
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
    }
    
    // MARK: - UISearchBarDelegate
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.searchBar.sizeToFit()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        self.searchBar.sizeToFit()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.searchText = searchText
        self.searchBar.text =  self.searchText
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if (self.searchBar.text == "" || self.searchBar.text == nil) {
            self.searchText = ""
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchText = searchBar.text ?? ""
        self.view.endEditing(true)
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        if self.listFilter.count == 0 {
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            self.getFilterData() {
                self.didTapFilter(searchBar)
            }
        } else {
            didTapFilter(searchBar)
        }
    }
    
    @objc func didTapFilter(_ sender: UISearchBar) {
        var array = Array<CommonPopoverModel>()
        //array.append(CommonPopoverModel.init("", "All".localizedString(), "All".localizedString(), self.selectedFilter == ""))
        for item in listFilter {
            array.append(CommonPopoverModel.init(item.Code, item.Code, item.Name, self.selectedFilter == item.Code))
        }
        
        var view:UIView = sender
        if let v = sender.textField.rightView {
            view = v
        }
        
        CommonCategoriesPopover.appearFrom(originView: sender, baseView: view, baseViewController: self, title: "Please select".localizedString(), arrayData: array, doneAction: {[weak self] array in
            for item in array {
                if item.IsSelected {
                    self?.selectedFilter = item.Id
                    break
                }
            }
        }, cancelAction: { print("cancel") },
        modalStyle: .popover,
        arrowDirection: .any)
    }
    
    func getFilterData(completion: @escaping () -> Void) {
        ServiceData.sharedInstance.taskNewsGetCategory().continueOnSuccessWith(continuation: { task in
            #if DEBUG
            print("\(task) \(#function)")
            #endif
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<CommonFilterModel>()
                for item in array {
                    let model = CommonFilterModel(json: item)
                    arrayModel.append(model)
                }
                self.listFilter = arrayModel
            }
            self.hideMBProgressHUD(true)
            completion()
        }).continueOnErrorWith(continuation: { error in
            self.hideMBProgressHUD(true)
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            completion()
        })
    }
}

