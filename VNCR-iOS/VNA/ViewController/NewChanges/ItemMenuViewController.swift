//
//  ItemMenuViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/16/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
//import SKPhotoBrowser

class ItemMenuViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, ContentTableViewCellDelegate {
    
    var menuItem: MenuItemModel!
    
    var pageNumber = 1
    
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero) //CustomSearchBarView = CustomSearchBarView(frame: CGRect.zero)
    
    //var listContents = [ContentModel]()
    var sectionData = SectionModel()
    
    var refreshControl: UIRefreshControl!
        
    @IBOutlet weak var tableView: UITableView!
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var fromDateSeleted: Date!
    
    var rightBarButtonItem: UIBarButtonItem!
    
    var searchText = ""
    
    var sectionChartView: SectionChartView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        //self.navigationItem.setHidesBackButton(true, animated:true)
        //self.customBackButton(imageName: "back-icon.png", renderingMode: UIImageRenderingMode.alwaysTemplate)
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        fromDateSeleted = Date()
        let button = UIButton(type: .custom)
        button.setTitle("", for: .normal)
        button.setTitleColor(UIColor("#cc9e73"), for: .normal)
        button.titleLabel?.textColor = UIColor("#cc9e73")
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.addTarget(self, action: #selector(self.buttonSelectDateStartPress), for: .touchUpInside)
        //buttonSelectDateStartPress
        button.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        //button.backgroundColor = UIColor.black
        rightBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        self.setupSearchBar()
        //self.setUpChartView()
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        let nib = UINib(nibName: "ContentTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ContentTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.keyboardDismissMode = .onDrag
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        self.tableView.alwaysBounceVertical = true
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 30
        
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            self?.loadMoreData() {
                self?.tableView!.finishInfiniteScroll()
                self?.refreshControl?.endRefreshing()
            }
            
        }
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        
        showStartDateInfo(date: fromDateSeleted)
        //getNewData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func buttonSelectDateStartPress(sender: UIButton) {
        //let buttonItemView = sender.value(forKey: "view") as! UIView
        self.searchBar.resignFirstResponder()
        CalendarPickerPopover.appearFrom(
            originView: sender,
            baseViewController: self,
            title: "Date Picker".localizedString(),
            dateMode: .date,
            initialDate: fromDateSeleted,
            doneAction: {[weak self] selectedDate in
                print("selectedDate \(selectedDate)")
                self?.showStartDateInfo(date: selectedDate)
            },
            cancelAction: {print("cancel")}
        )
    }
    
    func showStartDateInfo(date: Date) {
        fromDateSeleted = date
        if let button = rightBarButtonItem.customView as? UIButton {
            button.setTitle(fromDateSeleted.dateTimeToddMMM, for: .normal)
            
        }
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        pageNumber = 1
        getNewData()
    }
    
    @objc func refreshData() {
        getNewData()
    }
    
    func setupSearchBar() {
        //self.searchBar.searchBarOriginX = 0
        //        self.searchBar.tintColor = .white
        //        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.placeholder = "Search".localizedString()
        searchBar.delegate = self
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
    }
    
    func setUpChartView() {
        self.sectionChartView = SectionChartView.loadView() as? SectionChartView
        self.sectionChartView.backgroundColor = UIColor.white
        let view = UIView(frame: self.sectionChartView.frame)
        view.addSubview(self.sectionChartView)
        tableView.tableHeaderView = view
    }
    
    func getNewData() {
        pageNumber = 1
        ServiceData.sharedInstance.taskCavaGetSectionList(id: menuItem.id, date: fromDateSeleted, keyword: searchText).continueOnSuccessWith(continuation: { task in
            
            if let result = task as? JSON {
                let data = SectionModel(json: result)
                print("HARRY 1: \(data)")
                self.sectionData = data
                if data.BarItems.count == 0 && data.PieItems.count == 0 {
                    self.tableView.tableHeaderView = nil
                } else {
                    self.setUpChartView()
                    self.sectionChartView.showChart(barChartTitle: data.BarTitle, barChartItem: data.BarItems, pieChartTitle: data.PieTitle, pieChartItem: data.PieItems)
                    self.tableView.tableHeaderView?.frame.size.height = self.sectionChartView.height
                    self.tableView.setNeedsDisplay()
                    self.tableView.layoutIfNeeded()
                }
            }
            
            self.tableView.reloadData()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    func loadMoreData(_ handler: (() -> Void)?) {
        
        let countSection = self.sectionData.ListItems.count
        if(countSection > 0 && self.sectionData.ListItems[countSection-1].Items.count > 1 && !self.refreshControl.isRefreshing) {
            
            pageNumber += 1
            ServiceData.sharedInstance.taskCavaGetSectionList(id: menuItem.id, date: fromDateSeleted, keyword: searchText, pageIndex: pageNumber).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    let data = SectionModel(json: result)
                    print("HARRY 2: \(data)")
                    if data.ListItems.count > 0 && data.ListItems[0].Items.count > 0 {
                        self.tableView?.beginUpdates()
                        var listIndexPath = Array<IndexPath>()
                        for model in data.ListItems[0].Items {
                            listIndexPath.append(IndexPath(row: self.sectionData.ListItems[countSection-1].Items.count, section: countSection-1))
                            self.sectionData.ListItems[countSection-1].Items.append(model)
                        }
                        self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                        self.tableView?.endUpdates()
                    }
                }
                self.hideMBProgressHUD(true)
                handler?()
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                handler?()
            })
            
        } else {
            handler?()
        }
        
    }
    
    //MARK - UISearchBarDelegate
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
        }
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if(self.searchBar.text == "" || self.searchBar.text == nil) {
            searchText = ""
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("")
        searchText = searchBar.text ?? ""
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
    func configure(cell: ContentTableViewCell, at indexPath: IndexPath) {
        cell.contentModel = self.sectionData.ListItems[indexPath.section].Items[indexPath.row]//self.listContents[indexPath.row]
        cell.contentTableViewCellDelegate = self;
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sectionData.ListItems.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView: CustomHeader = .fromNib()
        headerView.setData(self.sectionData.ListItems[section].Title)
        return headerView
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 30
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sectionData.ListItems[section].Items.count//self.listContents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContentTableViewCell")! as! ContentTableViewCell
        //cell.accessoryType = .detailButton
        self.configure(cell: cell, at: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "ContentTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! ContentTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let model = self.sectionData.ListItems[indexPath.section].Items[indexPath.row]//self.listContents[indexPath.row]
        //self.pushStatisticDetailViewController(id: model.id)
        self.pushMenuItemDetailViewController(menuItem: self.menuItem, id: model.id)
    }
        
    //MARK - ContentTableViewCellDelegate
    
    func tapAvatarContentTableViewCell(cell: ContentTableViewCell) {
        let photo = SKPhoto.photoWithImageURL(cell.contentModel?.imageUrl ?? "")
        photo.caption = ""
        //let browser = SKPhotoBrowser(originImage: UIImage.init(named: "photo-camera.png")!, photos: [photo], animatedFromView: cell.imageViewAvatar)
        //browser.initializePageIndex(0)
        let browser = SKPhotoBrowser(photos: [photo], initialPageIndex: 0)
        present(browser, animated: true, completion: {})
    }
    
    func tapInfoButtonContentTableViewCell(cell: ContentTableViewCell) {
        if let model = cell.contentModel {
            //self.pushStatisticDetailViewController(id: model.id)
            self.pushMenuItemDetailViewController(menuItem: self.menuItem, id: model.id)
        }
        
    }
    
}
