//
//  ScheduleFlyViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/29/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//
import UIKit
import Foundation
import DateTools
import SwiftyJSON

class ScheduleFlyViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, ScheduleFlyTableViewCellDelegate, ReportPositionDelegate, UISearchBarDelegate {
    
    private var pageIndex: Int = 1
    
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero) //CustomSearchBarView(frame: CGRect.zero)
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewHeader: UIView!
    
    var fromDateSeleted: Date!
    
    var toDateSeleted: Date!
    
    var listScheduleFly = Array<ScheduleFlightModel>()
    
    var searchText = ""
    
    var leftBarButtonItem: UIBarButtonItem!
    
    var rightBarButtonItem: UIBarButtonItem!
    
    var searchBarButtonItem: UIBarButtonItem!
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var refreshControl: UIRefreshControl!
    
    var selectedSchedule:ScheduleFlightModel?
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView?.addSubview(refreshControl)
            }
            
        }
    }
    
    override var isOfflineMode: Bool {
        didSet {
            var offline = ""
            if isOfflineMode {
                offline = " (\("offline".localizedString()))"
            }
            DispatchQueue.main.async {
                self.tabBarController?.tabBar.items?[0].title = "Flight".localizedString() + offline
            }
        }
    }
    
    override func networkDidChanged(connectedInternet: Bool, reachability: Reachability) {
        super.networkDidChanged(connectedInternet: connectedInternet, reachability: reachability)
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //searchBar.sizeToFit()
        self.view.layoutIfNeeded()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.9803921569, blue: 0.9921568627, alpha: 1)
        tableView.backgroundColor = #colorLiteral(red: 0.968627451, green: 0.9803921569, blue: 0.9921568627, alpha: 1)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(ScheduleFlyCell.nib, forCellReuseIdentifier: ScheduleFlyCell.identifier)
        tableView.allowsSelection = !(ServiceData.sharedInstance.userModel?.isGuest ?? false)
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        textTitle = "SCHEDULE FLY".localizedString()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        Broadcaster.register(ReportPositionDelegate.self, observer: self)
        
        self.setupScheduleHeaderView()
        
        self.setupSearchBar()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        var button = UIButton(type: .custom)
        button.setTitle("", for: .normal)
        button.setTitleColor(UIColor("#cc9e73"), for: .normal)
        button.titleLabel?.textColor = UIColor("#cc9e73")
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.addTarget(self, action: #selector(ScheduleFlyViewController.buttonSelectDateStartPress), for: .touchUpInside)
        //buttonSelectDateStartPress
        button.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        //button.backgroundColor = UIColor.black
        leftBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        
        button = UIButton(frame: CGRect(x: 0, y: 0, width: 36, height: 36))
        button.setTitle("", for: .normal)
        //button.setTitleColor(UIColor("#cc9e73"), for: .normal)
        //button.titleLabel?.textColor = UIColor("#cc9e73")
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.addTarget(self, action: #selector(ScheduleFlyViewController.buttonSelectPersonSchedule), for: .touchUpInside)
        button.setImage(UIImage.init(named: "personal-icon.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = UIColor.white
        //button.backgroundColor = UIColor.black
        rightBarButtonItem = UIBarButtonItem(customView: button)
        
        searchBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.search, target: self, action: #selector(self.searchBarButtonItemPressed(_:)))
        
        if ServiceData.sharedInstance.userModel?.isGuest == true {
            self.navigationItem.rightBarButtonItems = [searchBarButtonItem]
        } else {
            self.navigationItem.rightBarButtonItems = [rightBarButtonItem, searchBarButtonItem]
        }
        
        /*
        self.textFieldSearch.layer.cornerRadius = 2.5
        self.textFieldSearch.layer.borderColor = UIColor.lightGray.cgColor
        self.textFieldSearch.layer.borderWidth = 0.5
        self.textFieldSearch.clipsToBounds = true
        */
        /*
        if let array = UserDefaults.standard.object(forKey: "AppleLanguages") {
            UserDefaults.standard.set("vi", forKey: "AppleLanguages")
            UserDefaults.standard.synchronize()
            print(array)
            
        }
        */
        fromDateSeleted = Date()//"01/11/2016".stringToDate
        toDateSeleted = Date()//"30/11/2016".stringToDate
        toDateSeleted = (toDateSeleted as NSDate).addingDays(10) as Date
        self.showStartDateInfo(date: fromDateSeleted)
        self.showEndDateInfo(date: toDateSeleted)
        //self.radioButtonAllFlight.isSelected = true
        
        self.tableView!.rowHeight = UITableView.automaticDimension
        self.tableView!.estimatedRowHeight = 320
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            if(self?.isSearch == true) {
                self?.tableView!.finishInfiniteScroll()
                return
            }
            self?.loadMoreData() {
                self?.tableView!.finishInfiniteScroll()
                self?.refreshControl?.endRefreshing()
            }
            
        }
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getDataResult()
        
    }
    
    @objc func searchBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getDataResult()
    }
    
    func setupSearchBar() {
//        self.searchBar.tintColor = .white
//        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.placeholder = "Search".localizedString()
        searchBar.delegate = self
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
    }
    
    //MARK - UISearchBarDelegate
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
        }
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        self.navigationItem.leftBarButtonItem = leftBarButtonItem
        self.navigationItem.rightBarButtonItems = [rightBarButtonItem, searchBarButtonItem]
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if(self.searchBar.text == "" || self.searchBar.text == nil) {
            searchText = ""
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("")
        searchText = searchBar.text ?? ""
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getDataResult()
    }
    
    func leftBarButtonItemPress() {
        
    }
    
    func setupScheduleHeaderView() {
        /*
        self.scheduleHeaderView = ScheduleHeaderView.loadView() as! ScheduleHeaderView
        self.scheduleHeaderView.radioButtonAllFlight.isSelected = true
        tableView.tableHeaderView = scheduleHeaderView
        
        scheduleHeaderView.iconArrow1.setFAIcon(icon: .FAArrowRight, iconSize: 18)
        scheduleHeaderView.labelIconArrowDown1?.setFAIcon(icon: .FAAngleDown, iconSize: 18)
        scheduleHeaderView.labelIconArrowDown2?.setFAIcon(icon: .FAAngleDown, iconSize: 18)
        
        scheduleHeaderView.buttonSearch.layer.cornerRadius = 2.5
        
        scheduleHeaderView.radioButtonAllFlight.setTitle("All flight".localizedString(), for: UIControlState())
        scheduleHeaderView.radioButtonAllFlight.circleColor = UIColor("#166880")
        scheduleHeaderView.radioButtonAllFlight.tintColor = UIColor("#166880")
        scheduleHeaderView.radioButtonAllFlight.addTarget(self, action: #selector(radioButtonAllFlighPressed(_:)), for: .touchUpInside)
        
        scheduleHeaderView.buttonSelectDateStart.addTarget(self, action: #selector(buttonSelectDateStartPress(sender:)), for: .touchUpInside)
        
        scheduleHeaderView.buttonSelectDateEnd.addTarget(self, action: #selector(buttonSelectDateEndPress(sender:)), for: .touchUpInside)
        
        scheduleHeaderView.buttonSearch.addTarget(self, action: #selector(buttonSearchPress(sender:)), for: .touchUpInside)
        scheduleHeaderView.buttonSearch.setTitle("Search".localizedString(), for: UIControlState())
        
        
        scheduleHeaderView.textFieldSearch.placeholder = "FlightNo/Routing".localizedString()
        scheduleHeaderView.viewTextFieldSearch.layer.cornerRadius = 2.5
        scheduleHeaderView.viewTextFieldSearch.layer.borderColor = UIColor.lightGray.cgColor
        scheduleHeaderView.viewTextFieldSearch.layer.borderWidth = 0.5
        scheduleHeaderView.viewTextFieldSearch.clipsToBounds = true
        scheduleHeaderView.viewTextFieldSearch.backgroundColor = UIColor.groupTableViewBackground
         */
    }
    
    @objc func refreshData() {
        self.getDataResult()
    }
    
    func loadMoreData(_ handler: (() -> Void)?) {
        
        if(self.listScheduleFly.count > 1 && !self.refreshControl.isRefreshing){
            let userId = ServiceData.sharedInstance.userId ?? 0
            pageIndex += 1
            ServiceDataCD.getFlights(fromDate: fromDateSeleted, toDate: toDateSeleted, keyword: searchText, pageIndex: pageIndex, userId: userId, isOffline: isOfflineMode) {[weak self] list, error in
                guard let `self` = self else { return }
                DispatchQueue.main.async {
                    self.hideMBProgressHUD(true)
                    if let error = error {
                        self.showAlert("Alert".localizedString(), stringContent: error)
                    } else {
                        self.tableView?.beginUpdates()
                        var listIndexPath = Array<IndexPath>()
                        list.forEach({
                            listIndexPath.append(IndexPath(row: self.listScheduleFly.count, section: 0))
                            self.listScheduleFly.append($0)
                        })
                        self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                        self.tableView?.endUpdates()
                    }
                    handler?()
                }
            }
        } else {
            handler?()
            
        }
        
    }
    
    func displayTitle(text: String) {
        
    }
    
    func buttonBackPress(sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showStartDateInfo(date: Date) {
        fromDateSeleted = date
        //let date = date as NSDate
        /*
        scheduleHeaderView.labelDayStart.text = String(format: "%d", date.day())
        scheduleHeaderView.labelMonthStart.text = String(format: "Month %d", date.month()).localizedString()
        scheduleHeaderView.labelDayOfWeekStart.text = String(format: "T %d", date.weekday()).localizedString()
        */
        if let button = leftBarButtonItem.customView as? UIButton {
            button.setTitle(fromDateSeleted.dateTimeToddMMM, for: .normal)
            toDateSeleted = (fromDateSeleted as NSDate).addingDays(7) as Date
        }
        
    }
    
    func showEndDateInfo(date: Date) {
        toDateSeleted = date
        //let date = date as NSDate
        /*
        scheduleHeaderView.labelDayEnd.text = String(format: "%d", date.day())
        scheduleHeaderView.labelMonthEnd.text = String(format: "Month %d", date.month()).localizedString()
        scheduleHeaderView.labelDayOfWeekEnd.text = String(format: "T %d", date.weekday()).localizedString()
        */
    }
    
    func getDataResult() {
        self.showMBProgressHUD("Loading...", animated: true)
        let userId = ServiceData.sharedInstance.userId ?? 0
        pageIndex = 1
        ServiceDataCD.getFlights(fromDate: fromDateSeleted, toDate: toDateSeleted, keyword: searchText, pageIndex: pageIndex, userId: userId, isOffline: isOfflineMode) {[weak self] list, error in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.tableView!.finishInfiniteScroll()
                self.refreshControl.endRefreshing()
                if let error = error {
                    self.showAlert("Alert".localizedString(), stringContent: error)
                } else {
                    self.listScheduleFly = list
                    self.tableView.reloadData()
                }
            }
        }
    }
    
//
//    func configure(cell: ExpanedScheduleFlyTableViewCell, at indexPath: IndexPath) {
//        let ScheduleFlightModel = self.listScheduleFly[indexPath.row]
//        cell.loadData(ScheduleFlightModel: ScheduleFlightModel, indexPath: indexPath)
//        cell.delegateAction = self
//    }
    
    //MARK: UITableViewDataSource methods
    @objc override func changeDataMode(_ sender:UIButton) {
        super.changeDataMode(sender)
        getDataResult()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 50))
        btn.setTitleColor(.systemBlue, for: UIControl.State())
        btn.removeTarget(self, action: #selector(self.changeDataMode(_:)), for: .touchUpInside)
        btn.addTarget(self, action: #selector(self.changeDataMode(_:)), for: .touchUpInside)
        if isOfflineMode && self.isConnectedInternet {
            btn.isSelected = false
            btn.setTitle("Turn off Offline Mode".localizedString(), for: UIControl.State())
            return btn
        } else if !isOfflineMode && !self.isConnectedInternet { // online & no internet
            btn.isSelected = true
            btn.setTitle("Turn on Offline Mode".localizedString(), for: UIControl.State())
            return btn
        }
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listScheduleFly.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let scheduleFly = self.listScheduleFly[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleFlyCell.identifier) as! ScheduleFlyCell
        cell.show(scheduleFly: scheduleFly)
        cell.onGoto = {[weak self] cell in
            guard let schedule = cell.scheduleFly else {return}
            
            let vc = ScheduleFlyActionController(listType: .normal, scheduleFly: schedule) {[weak self] sheduleFlyAction in
                self?.handleScheduleFlyAction(scheduleFly: schedule, sheduleFlyAction: sheduleFlyAction, indexPath: indexPath)
            }
            // show popover cho ipad
            if self?.isIpad == true {
                vc.popoverPresentationController?.sourceView = cell.btnGoto
                vc.isModalInPopover = false
            }
            self?.present(vc: vc, transitioning: SlideUpAnimateTransitionDelegate(), completion: nil)
        }
        return cell
        
//        if(scheduleFly.isExpanded == true) {
            /*
            let cell = tableView.dequeueReusableCell(withIdentifier: "ExpanedScheduleFlyTableViewCell")! as! ExpanedScheduleFlyTableViewCell
            
            //let ScheduleFlightModel = self.listScheduleFly[indexPath.row]
            //cell.loadData(ScheduleFlightModel: ScheduleFlightModel, indexPath: indexPath)
            //cell.delegateAction = self
            
            self.configure(cell: cell, at: indexPath)
            return cell
             */
        }
        
//        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleFlyTableViewCell")! as! ScheduleFlyTableViewCell
//
//        let scheduleFlyModel = self.listScheduleFly[indexPath.row]
//        cell.loadData(scheduleFlyModel: scheduleFlyModel, indexPath: indexPath)
//        cell.delegateAction = self
//        return cell
        
//    }
    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//    }
    
    /*
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        let scheduleFly = self.listScheduleFly[indexPath.row]
        if(scheduleFly.isExpanded == false) {
            var array = Array<IndexPath>()
            if let selectedIndexPath = self.tableView.indexPathForSelectedRow {
                let item = self.listScheduleFly[selectedIndexPath.row]
                item.isExpanded = false
                array.append(selectedIndexPath)
            }
            scheduleFly.isExpanded = true
            array.append(indexPath)
            self.tableView.reloadRows(at: array, with: .automatic)
        } else {
            scheduleFly.isExpanded = false
            self.tableView.reloadRows(at: [indexPath], with: .automatic)
            return nil
        }
        return indexPath
    }
 */
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let scheduleFly = self.listScheduleFly[indexPath.row]
        
        let titleText = "\(scheduleFly.flightNo)/\(scheduleFly.departedDate) \(scheduleFly.routing) \(scheduleFly.classify)"
        let vc = ScheduleFlyDetailController(detailType: .normal, scheduleFly: scheduleFly, titleText: titleText) {[weak self] sheduleFlyAction in
            // should be let indexPath = nil, because action be involked from detail so schedule model has already full information
            self?.handleScheduleFlyAction(scheduleFly: scheduleFly, sheduleFlyAction: sheduleFlyAction, indexPath: nil)
        }

        if splitViewController != nil {
            if let nv = splitViewController?.viewControllers.last as? UINavigationController,
               let vc = nv.viewControllers.last as? ScheduleFlyDetailController, vc.detailType == .normal {
                if vc.scheduleFly?.flightID == scheduleFly.flightID {return}
            }
            self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: vc), sender: nil)
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        selectedSchedule = scheduleFly
    }
    
    func handleScheduleFlyAction(scheduleFly:ScheduleFlightModel,
                                 sheduleFlyAction:ScheduleFlyActionController.ScheduleFlyAction,
                                 indexPath:IndexPath?) {
        var isSameSchedule = true
        // check same schedule
        isSameSchedule = self.selectedSchedule?.flightID == scheduleFly.flightID
        
        var shouldMarkSelected = true
        switch sheduleFlyAction {
        case .assignment:
            self.pushAssessmentCrewDutyListViewController(scheduleFlyModel: scheduleFly, isSameSchedule: isSameSchedule)
        case .report:
            self.pushFlightReportListViewController(scheduleFlyModel: scheduleFly, isSameSchedule: isSameSchedule)
        case .survey:
            self.pushSurveyListViewController(scheduleFlyModel: scheduleFly, isListSaved: false, isSameSchedule: isSameSchedule)
        case .seatMap:
            let textTitle = "\(scheduleFly.flightNo)/\(scheduleFly.departedDate) \(scheduleFly.routing) \(scheduleFly.classify)"
            self.presentSeatmapViewController(seatmap: AircraftCategoryType(rawValue: scheduleFly.seatmap) ?? .A32, title: textTitle, flightId: scheduleFly.flightID)
        case .task:
            self.pushLeadreportPosition(scheduleFly: scheduleFly, isSameSchedule: isSameSchedule)
        case .oJT:
            self.pushOJTListExaminees(scheduleFly: scheduleFly, isSameSchedule: isSameSchedule)
        case .quickReport:
            self.pushReportFlightViewcontroller(scheduleFlyModel: scheduleFly, reportModel: ReportModel(), isSameSchedule: isSameSchedule)
        case .synchronize:
            self.synchronizeThirdServices(scheduleFlyModel: scheduleFly, nil)
        case .refreshLocal, .saveLocal:
            shouldMarkSelected = false
            if #available(iOS 10.0, *) {
                self.showMBProgressHUD("Saving...", animated: true)
                self.saveScheduleFlight(scheduleFlyModel: scheduleFly,
                                        isPersonal: false,
                                        isGetFull: indexPath != nil/* nếu indexPath == nil => action được gọi từ detail page */) { error in
                    DispatchQueue.main.async {[weak self] in
                        guard let `self` = self else { return }
                        if let error = error {
                            self.showAlert("Alert".localizedString(), stringContent: error as? String ?? "")
                        } else {
                            // chỉ reload lại khi đang ở trạng thái offline
                            if !NetworkObserver.shared.isConnectedInternet {
                                self.getDataResult()
                            }
                            self.showAlert("Alert".localizedString(), stringContent: "saveFlightSuccess".localizedString())
                        }
                        self.hideMBProgressHUD(true)
                    }
                }
            }
        case .deleteLocal:
            shouldMarkSelected = false
            if #available(iOS 10.0, *) {
                self.showMBProgressHUD("Deleting...", animated: true)
                self.deleteScheduleFlightLocal(flightId: scheduleFly.flightID) { error in
                    DispatchQueue.main.async {[weak self] in
                        guard let `self` = self else { return }
                        if let error = error {
                            self.showAlert("Alert".localizedString(), stringContent: error as? String ?? "")
                        } else {
                            // chỉ reload lại khi đang ở trạng thái offline
                            if !NetworkObserver.shared.isConnectedInternet {
                                self.getDataResult()
                            }
                            self.showAlert("Alert".localizedString(), stringContent: "deleteFlightSuccess".localizedString())
                        }
                        self.hideMBProgressHUD(true)
                    }
                }
            }
        @unknown default:
            shouldMarkSelected = false
        }
        if shouldMarkSelected, let indexPath = indexPath {
            if indexPath.row < listScheduleFly.count {
                selectedSchedule = listScheduleFly[indexPath.row]
            }
            self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
        /*
        let scheduleFly = self.listScheduleFly[indexPath.row]
        if(scheduleFly.isExpanded == true) {
            if(UIDevice.current.userInterfaceIdiom == .phone) {
                if(scheduleFly.note.count > 0) {
                    //return 150
                    return tableView.fd_heightForCell(withIdentifier: "ExpanedScheduleFlyTableViewCell", configuration: {[weak self] (cell) in
                        let cell = cell as! ExpanedScheduleFlyTableViewCell
                        self?.configure(cell: cell, at: indexPath)
                    })
                } else {
                    return 115
                }
            } else {
                let scheduleFly = self.listScheduleFly[indexPath.row]
                var heightRow: CGFloat = 105
                if(scheduleFly.note.count > 0) {
                    //heightRow = 130
                    heightRow = tableView.fd_heightForCell(withIdentifier: "ExpanedScheduleFlyTableViewCell", configuration: {[weak self] (cell) in
                        let cell = cell as! ExpanedScheduleFlyTableViewCell
                        self?.configure(cell: cell, at: indexPath)
                    })
                } else {
                    heightRow = 105
                }
                if(scheduleFly.trips!.details.count > 4) {
                    let row = scheduleFly.trips!.details.count - 4
                    let height = (CGFloat(row) * 24.0) + heightRow
                    return height
                }
                return heightRow
                
            }
            
            
        }
        return 80
        */
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // offline & have internet
        if isOfflineMode && self.isConnectedInternet {
            return 50
        } else if !isOfflineMode && !self.isConnectedInternet { // online & no internet
            return 50
        }
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 1
    }
    
    //ScheduleFlyTableViewCellDelegate
    func reportPositonScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        let leadReportPositionViewController = self.storyboard?.instantiateViewController(withIdentifier: "LeadReportPositionViewController") as! LeadReportPositionViewController
        leadReportPositionViewController.scheduleFlyModel = scheduleFly
        self.navigationController?.pushViewController(leadReportPositionViewController, animated: true)
    }
    
    func reportFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        self.pushFlightReportListViewController(scheduleFlyModel: scheduleFly, isSameSchedule: true)
    }
    
    func assessmentFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        self.pushAssessmentCrewDutyListViewController(scheduleFlyModel: scheduleFly, isSameSchedule: true)
    }
    
    func surveyListFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        self.pushSurveyListViewController(scheduleFlyModel: scheduleFly, isListSaved: false, isSameSchedule: true)
    }
    
    func ojtListFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        //self.pushOJTListExamineesViewController(scheduleFlyModel: scheduleFly)
        
        let textTitle = "\(scheduleFly.flightNo)/\(scheduleFly.departedDate) \(scheduleFly.routing) \(scheduleFly.classify)"
        self.presentSeatmapViewController(seatmap: AircraftCategoryType(rawValue: scheduleFly.seatmap) ?? .A32, title: textTitle, flightId: scheduleFly.flightID)
    }
    
    //MARK - 
    
    func buttonSearchPress(sender: UIButton) {

    }
    
    @objc func buttonSelectPersonSchedule(sender: UIButton) {
        let viewcontroller = self.storyboard?.instantiateViewController(withIdentifier: "PersonalScheduleFlyViewController") as! PersonalScheduleFlyViewController
        self.navigationController?.pushViewController(viewcontroller, animated: true)
    }
    
    @objc func buttonSelectDateStartPress(sender: UIButton) {
        //let buttonItemView = sender.value(forKey: "view") as! UIView
        self.searchBar.resignFirstResponder()
        CalendarPickerPopover.appearFrom(
            originView: sender,
            baseViewController: self,
            title: "Date Picker".localizedString(),
            dateMode: .date,
            initialDate: fromDateSeleted,
            doneAction: {[weak self] selectedDate in
                print("selectedDate \(selectedDate)")
                self?.showStartDateInfo(date: selectedDate)
                self?.showMBProgressHUD("Loading...".localizedString(), animated: true)
                self?.refreshData()
            },
            cancelAction: {print("cancel")}
        )
    }
    
    func buttonSelectDateEndPress(sender: UIButton) {
        //let buttonItemView = sender.value(forKey: "view") as! UIView
        CalendarPickerPopover.appearFrom(
            originView: sender,
            baseViewController: self,
            title: "Date Picker".localizedString(),
            dateMode: .date,
            initialDate: toDateSeleted,
            doneAction: {[weak self] selectedDate in
                print("selectedDate \(selectedDate)")
                self?.showEndDateInfo(date: selectedDate)
            },
            cancelAction: {print("cancel")}
        )
    }
    
    //
    
    @IBAction func radioButtonAllFlighPressed(_ sender: UIButton) {
        /*
        if(scheduleHeaderView.radioButtonAllFlight.isSelected) {
            scheduleHeaderView.radioButtonAllFlight.isSelected = false
        } else {
            scheduleHeaderView.radioButtonAllFlight.isSelected = true
        }
         */
    }
    
    //Mark - ReportPositionDelegate
    func updateReportPosition(item: ScheduleFlightModel) {
//        for index in 0..<self.listScheduleFly.count {
//            if (item.flightID == self.listScheduleFly[index].flightID) {
//                self.self.listScheduleFly[index] = item
//                self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
//                return
//            }
//        }
    }
    
    
}


