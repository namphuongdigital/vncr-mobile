//
//  MessageListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import STPopup


class MessageListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchResultsUpdating, MessageItemTableViewCellDelegate, PopupNoteViewControllerDelagate, PushNotifyDelegate {
    
    static var notifyModelFromNotify: NotifyModel?

    @IBOutlet weak var tableView: UITableView!
    
    var listNewMessageModel = [MessageModel]()
    
    var listMessageModel = [MessageModel]()
    
    var listArchivesMessageModel = [MessageModel]()
    
    var listSearchMessageModel = [MessageModel]()
    
    private var pageIndexNew: Int = 0
    
    private var pageIndex: Int = 0
    
    private var pageIndexArchives: Int = 0
    
    private var pageIndexSearch: Int = 1
    
    var segmentedControl: UISegmentedControl = UISegmentedControl(frame: CGRect.zero)
    
    var addBarButtonItem: UIBarButtonItem!
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var refreshBarButtonItem: UIBarButtonItem!
    
    var refreshControl: UIRefreshControl!
    
    var textSearch: String = ""
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView?.addSubview(refreshControl)
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ServiceData.sharedInstance.getAlertNotificationsPermission()?.show(
            { finished, results in
                print("got results \(results)")
                ServiceData.sharedInstance.isShowAlertNotificationPermission = false
                self.becomeFirstResponder()
        },
            cancelled: { results in
                print("thing was cancelled")
                ServiceData.sharedInstance.isShowAlertNotificationPermission = false
                self.becomeFirstResponder()
        }
        )
        
//        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
//        UINavigationBar.appearance().tintColor = UIColor.white
//        UINavigationBar.appearance().barTintColor = UIColor("#166880")
//        UINavigationBar.appearance().isTranslucent = false
//
//        UITabBar.appearance().isTranslucent = false
//        UITabBar.appearance().tintColor = UIColor("#166880")
//        UITabBar.appearance().barTintColor = UIColor("#166880")
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor("#166880")], for: .normal)
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], for: .selected)
//
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
//        self.view.backgroundColor = UIColor("#166880")
        self.navigationController?.view.backgroundColor = UIColor("#166880")
//        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        //let searchBar = searchController.searchBar
        //let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        //filterContentForSearchText(searchController.searchBar.text!, scope: scope)
    }
    
    func searchBarIsEmpty() -> Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func isFiltering() -> Bool {
        return searchController.isActive && (!searchBarIsEmpty())
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.tableView.fd_debugLogEnabled = true
        Broadcaster.register(PushNotifyDelegate.self, observer: self)
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        
        // Setup the Search Controller
        searchController.searchResultsUpdater = self
        if #available(iOS 9.1, *) {
            searchController.obscuresBackgroundDuringPresentation = false
        } else {
            // Fallback on earlier versions
        }
        searchController.searchBar.placeholder = "Search".localizedString()
        if #available(iOS 11.0, *) {
            navigationItem.searchController = searchController
            navigationItem.hidesSearchBarWhenScrolling = true
            navigationItem.largeTitleDisplayMode = .always
//
//            if let textfield = self.searchController.searchBar.findTextfield() {
//                //textfield.textColor = UIColor.blue
//                if let backgroundview = textfield.subviews.first {
//                    // Background color
//                    backgroundview.backgroundColor = UIColor.white
//                    // Rounded corner
//                    backgroundview.layer.cornerRadius = 18
//                    backgroundview.clipsToBounds = true
//                }
//            }
            
        } else {
            // Fallback on earlier versions
//            if let textfield = self.searchController.searchBar.findTextfield() {
//                textfield.placeholder = "Search".localizedString()
//                textfield.backgroundColor = UIColor.groupTableViewBackground
//                textfield.font = UIFont.systemFont(ofSize: 14)
//                //textfield.frame = CGRect(x: textfield.frame.origin.x, y: 8, width: textfield.frame.width, height: 44)
//                textfield.layer.cornerRadius = 15
//                //UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).setTitleTextAttributes(attributes, for: .normal)
//                //self.searchController.searchBar.tintColor = UIColor("#166880")
//                textfield.clipsToBounds = true
//            }
            
            self.searchController.searchBar.isTranslucent = false
            self.searchController.searchBar.searchBarStyle = UISearchBar.Style.minimal
            self.tableView.tableHeaderView = self.searchController.searchBar
        }
        definesPresentationContext = true
        
        // Setup the Scope Bar
        searchController.searchBar.delegate = self
        searchController.searchBar.tintColor = .white
        searchController.searchBar.barTintColor = .white
        UITextField.appearance(whenContainedInInstancesOf: [type(of: searchController.searchBar)]).tintColor = .black
        //searchController.searchBar.searchTextField.backgroundColor = UIColor.groupTableViewBackground
        searchController.searchBar.backgroundColor = UIColor("#166880")
        
        //        self.searchBar.tintColor = .white
        //        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchController.searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchController.searchBar.barTintColor = UIColor.white
            searchText = searchController.searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchController.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchController.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        
        self.segmentedControl.insertSegment(withTitle: "New".localizedString(), at: 0, animated: false)
        self.segmentedControl.insertSegment(withTitle: "Normal".localizedString(), at: 1, animated: false)
        self.segmentedControl.insertSegment(withTitle: "Archive".localizedString(), at: 2, animated: false)
        self.segmentedControl.selectedSegmentIndex = 0
        self.navigationItem.titleView = self.segmentedControl
        self.segmentedControl.sizeToFit()
        self.segmentedControl.addTarget(self, action: #selector(segmentedControlChange), for: .valueChanged)
        
        refreshBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.refreshBarButtonItemPressed(_:)))
        refreshBarButtonItem.setFAIcon(icon: .FARefresh, iconSize: 20)
        self.navigationItem.rightBarButtonItems = [self.refreshBarButtonItem]
        
        tableView.keyboardDismissMode = .onDrag
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self

        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        //self.textTitle = "FORM LIST".localizedString()
        
        self.tableView.alwaysBounceVertical = true
        if #available(iOS 11.0, *) {
            self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: self.view.safeAreaInsets.bottom + 30, height: 30))
            self.tableView!.infiniteScrollTriggerOffset = self.view.safeAreaInsets.bottom + 40
        } else {
            self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        }
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        //self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            if(self?.isSearch == true) {
                self?.loadSearchMoreData() {
                    self?.tableView!.finishInfiniteScroll()
                }
                return
            }
            if(self?.segmentedControl.selectedSegmentIndex == 0) {
                self?.loadMoreDataNew() {
                    self?.tableView!.finishInfiniteScroll()
                }
            } else if(self?.segmentedControl.selectedSegmentIndex == 1) {
                self?.loadMoreData() {
                    self?.tableView!.finishInfiniteScroll()
                }
            } else {
                self?.loadMoreDataArchives() {
                    self?.tableView!.finishInfiniteScroll()
                }
            }
            
            
        }
        
        self.navigationController?.navigationBar.backgroundColor  = UIColor("#166880")
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.view.backgroundColor = UIColor("#166880")
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
        
        if let messageModels = ApplicationData.sharedInstance.getParentMessageModels() {
            self.listMessageModel = messageModels
            self.tableView.reloadData()
        }
 
        //ApplicationData.sharedInstance.deleteAllMessageModel()
        
        if let notifyModelFromNotify = MessageListViewController.notifyModelFromNotify {
            self.notifyOpened(item: notifyModelFromNotify)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @objc func refreshBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        refreshData()
    }
    
    @objc func segmentedControlChange() {
        self.tableView.reloadData()
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            if(pageIndexNew == 0) {
                getNewData()
                return
            }
        } else if(self.segmentedControl.selectedSegmentIndex == 1) {
            if(pageIndex == 0) {
                getNormalData()
                return
            }
        } else {
            if(pageIndexArchives == 0) {
                getNewDataArchires()
                return
            }
        }
        
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if(self.presentedViewController != nil) {
            if let navigation = self.presentedViewController as? UINavigationController {
                navigation.dismiss(animated: true, completion: nil)
                return false
            }
        }
        searchBar.returnKeyType = .search
        self.navigationItem.rightBarButtonItem = nil
        self.navigationItem.leftBarButtonItem = nil
        self.isSearch = true
        self.tableView.reloadData()
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        UIApplication.shared.statusBarStyle = .lightContent
        self.searchFilter(text: searchBar.text ?? "")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        UIApplication.shared.statusBarStyle = .lightContent
        self.searchController.searchBar.text = ""
        self.searchController.searchBar.setShowsCancelButton(false, animated: true)
        self.searchController.searchBar.resignFirstResponder()
        self.navigationItem.rightBarButtonItems = [self.refreshBarButtonItem]
        self.isSearch = false
        self.tableView.reloadData()
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        UIApplication.shared.statusBarStyle = .default
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.showsCancelButton = true
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
            self.searchController.searchBar.sizeToFit()
        }
        if #available(iOS 11.0, *) {
        } else {
            if let cancelButton = self.searchController.searchBar.findCancelButton() {
                cancelButton.setTitleColor(UIColor("#166880"), for: .normal)
            }
        }
    }
    
    func searchFilter(text: String) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.segmentedControl.isEnabled = false
        pageIndexSearch = 1
        textSearch = text
        ServiceData.sharedInstance.taskMessageList(keyword: textSearch, pageIndex: pageIndexSearch).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<MessageModel>()
                for item in array {
                    let model = MessageModel(json: item)
                    arrayModel.append(model)
                }
                self.listSearchMessageModel = arrayModel
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                self.segmentedControl.isEnabled = true
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            self.segmentedControl.isEnabled = true
        })
    }
    
    func getMessageUnreadCount() {
        ServiceData.sharedInstance.taskMesageCount(phoneNo: ServiceData.sharedInstance.userModel?.phone ?? "").continueOnSuccessWith(continuation: { task in
            let result = task as! JSON
            ServiceData.sharedInstance.unreadMessageCount = result.intValue
            Broadcaster.notify(UnreadMessageCountDelegate.self) {
                $0.unreadMessageCountUpdate(unreadMessageCount: result.intValue)
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            print(error)
        })
    }
    
    @objc func refreshData() {
        getMessageUnreadCount()
        if (self.segmentedControl.selectedSegmentIndex == 0) {
            getNewData()
        } else if(self.segmentedControl.selectedSegmentIndex == 1) {
            getNormalData()
        } else {
            getNewDataArchires()
        }
    }
    
    func getNewData() {
        pageIndexNew = 1
        self.segmentedControl.isEnabled = false
        ServiceData.sharedInstance.taskMessageListNew(keyword: "", pageIndex: pageIndexNew).continueOnSuccessWith(continuation: { task in
            #if DEBUG
            print(task)
            #endif
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<MessageModel>()
                for item in array {
                    let model = MessageModel(json: item)
                    ApplicationData.sharedInstance.updateMessageModel(message: model)
                    arrayModel.append(model)
                }
                self.listNewMessageModel = arrayModel
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                self.segmentedControl.isEnabled = true
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            self.segmentedControl.isEnabled = true
        })
    }
    
    func getNormalData() {
        pageIndex = 1
        self.segmentedControl.isEnabled = false
        ServiceData.sharedInstance.taskMessageList(keyword: "", pageIndex: pageIndex).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<MessageModel>()
                for item in array {
                    let model = MessageModel(json: item)
                    ApplicationData.sharedInstance.updateMessageModel(message: model)
                    arrayModel.append(model)
                }
                self.listMessageModel = arrayModel
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                self.segmentedControl.isEnabled = true
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            self.segmentedControl.isEnabled = true
        })
    }
    
    
    func getNewDataArchires() {
        pageIndexArchives = 1
        self.segmentedControl.isEnabled = false
        ServiceData.sharedInstance.taskMessageListArchives(keyword: "", pageIndex: pageIndexArchives).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<MessageModel>()
                for item in array {
                    let model = MessageModel(json: item)
                    arrayModel.append(model)
                }
                self.listArchivesMessageModel = arrayModel
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                self.segmentedControl.isEnabled = true
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            self.segmentedControl.isEnabled = true
        })
    }
    
    func loadMoreDataNew(_ handler: (() -> Void)?) {
        
        if(self.listNewMessageModel.count > 1 && !self.refreshControl.isRefreshing){
            pageIndexNew += 1
            self.segmentedControl.isEnabled = false
            ServiceData.sharedInstance.taskMessageListNew(keyword: "", pageIndex: pageIndexNew).continueOnSuccessWith(continuation: { task in
                let result = task as! JSON
                if let array = result.array, array.count > 0 {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in array {
                        listIndexPath.append(IndexPath(row: self.listNewMessageModel.count, section: 0))
                        let model = MessageModel(json: item)
                        ApplicationData.sharedInstance.updateMessageModel(message: model)
                        self.listNewMessageModel.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                self.segmentedControl.isEnabled = true
                
                handler?()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.segmentedControl.isEnabled = true
                handler?()
            })
            
            
        } else {
            handler?()
        }
    }
    
    func loadMoreDataArchives(_ handler: (() -> Void)?) {
        
        if(self.listArchivesMessageModel.count > 1 && !self.refreshControl.isRefreshing){
            pageIndexArchives += 1
            self.segmentedControl.isEnabled = false
            ServiceData.sharedInstance.taskMessageListArchives(keyword: "", pageIndex: pageIndexArchives).continueOnSuccessWith(continuation: { task in
                let result = task as! JSON
                if let array = result.array, array.count > 0 {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in array {
                        listIndexPath.append(IndexPath(row: self.listArchivesMessageModel.count, section: 0))
                        let model = MessageModel(json: item)
                        self.listArchivesMessageModel.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                   
                }
                self.hideMBProgressHUD(true)
                self.segmentedControl.isEnabled = true
                handler?()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.segmentedControl.isEnabled = true
                handler?()
            })
            
            
            
            
        } else {
            handler?()
        }
    }
    
    
    
    func loadMoreData(_ handler: (() -> Void)?) {
        
        if(self.listMessageModel.count > 1 && !self.refreshControl.isRefreshing){
            pageIndex += 1
            self.segmentedControl.isEnabled = false
            ServiceData.sharedInstance.taskMessageList(keyword: "", pageIndex: pageIndex).continueOnSuccessWith(continuation: { task in
                let result = task as! JSON
                if let array = result.array, array.count > 0 {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in array {
                        listIndexPath.append(IndexPath(row: self.listMessageModel.count, section: 0))
                        let model = MessageModel(json: item)
                        ApplicationData.sharedInstance.updateMessageModel(message: model)
                        self.listMessageModel.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                self.segmentedControl.isEnabled = true
                
                handler?()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.segmentedControl.isEnabled = true
                handler?()
            })
            
            
            
            
        } else {
            handler?()
        }
    }
    
    
    func loadSearchMoreData(_ handler: (() -> Void)?) {
        
        if(self.listSearchMessageModel.count > 1 && !self.refreshControl.isRefreshing){
            pageIndexSearch += 1
            self.segmentedControl.isEnabled = false
            ServiceData.sharedInstance.taskMessageList(keyword: textSearch, pageIndex: pageIndexSearch).continueOnSuccessWith(continuation: { task in
                
                let result = task as! JSON
                if let array = result.array, array.count > 0 {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in array {
                        listIndexPath.append(IndexPath(row: self.listSearchMessageModel.count, section: 0))
                        let model = MessageModel(json: item)
                        self.listSearchMessageModel.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                self.segmentedControl.isEnabled = true
                
                handler?()
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                self.segmentedControl.isEnabled = true
                handler?()
            })
            
        } else {
            handler?()
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: FormTableViewCellDelegate
    
    func actionFormTableViewCell(action: ActionUpdateFormType, formModel: FormModel) {
        if(action == .remove) {
            //self.confirmDelete(formModel: formModel)
        } else {
            //self.showMBProgressHUD("Update...".localizedString(), animated: true)
            
        }
    }
    
    
    func pressFormTableViewCell(cell: FormTableViewCell) {
        
    }
    
    func configure(cell: MessageItemTableViewCell, at indexPath: IndexPath) {
        
        cell.labelContent.numberOfLines = 4
        if(isSearch) {
            cell.loadContent(messageModel: self.listSearchMessageModel[indexPath.row])
        } else {
            if(self.segmentedControl.selectedSegmentIndex == 0) {
                cell.loadContent(messageModel: self.listNewMessageModel[indexPath.row])
            } else if(self.segmentedControl.selectedSegmentIndex == 1) {
                cell.loadContent(messageModel: self.listMessageModel[indexPath.row])
            } else {
                cell.loadContent(messageModel: self.listArchivesMessageModel[indexPath.row])
            }
            
        }
        cell.delegateAction = self
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearch) {
            return self.listSearchMessageModel.count
        }
        if(self.segmentedControl.selectedSegmentIndex == 0) {
            return self.listNewMessageModel.count
        } else if(self.segmentedControl.selectedSegmentIndex == 1) {
            return self.listMessageModel.count
        } else {
            return self.listArchivesMessageModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageItemTableViewCell")! as! MessageItemTableViewCell
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "MessageItemTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! MessageItemTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model: MessageModel
        if(isSearch) {
            model = self.listSearchMessageModel[indexPath.row]
        } else {
            if(self.segmentedControl.selectedSegmentIndex == 0) {
                model = self.listNewMessageModel[indexPath.row]
            } else if(self.segmentedControl.selectedSegmentIndex == 1) {
                model = self.listMessageModel[indexPath.row]
            } else {
                model = self.listArchivesMessageModel[indexPath.row]
            }
        }
        
        self.markActionPress(messageModel: model)
        
        if model.navi == 1 {// survey or vnVote
            
            let vc = SurveyDynamicController(tid: model.objectId)
            self.present(vc, animated: true, completion: nil)

        } else { // keep old flow
            
            if(model.isHtml) {
                self.pushWebViewMessageViewController(id: model.id, title: model.sender)
            } else {
                self.pushMessageDetailViewController(messageModel: model)
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    // MARK: - MessageItemTableViewCellDelegate
    func actionMessageItemTableViewCell(messageModel: MessageModel, actionUpdateMessageItemType: ActionUpdateMessageItemType) {
        if(actionUpdateMessageItemType == .done) {
            self.doneActionPress(messageModel: messageModel)
        } else if(actionUpdateMessageItemType == .mark) {
            self.markActionPress(messageModel: messageModel)
        } else if(actionUpdateMessageItemType == .replyNo) {
            UIAlertController.show(in: self, withTitle: "Are you sure reply \"NO\" ?".localizedString(), message: "", preferredStyle: UIAlertController.Style.alert, cancelButtonTitle: "Cancel", destructiveButtonTitle: "OK", otherButtonTitles: nil, popoverPresentationControllerBlock: nil, tap: {[weak self] (controller, action, buttonIndex) in
                if (buttonIndex == controller.cancelButtonIndex) {
                    print("Cancel Tapped")
                } else if (buttonIndex == controller.destructiveButtonIndex) {
                    self?.replyActionPress(messageModel: messageModel, actionUpdateMessageItemType: actionUpdateMessageItemType)
                }
            })
            
        } else if(actionUpdateMessageItemType == .replyYes) {
            UIAlertController.show(in: self, withTitle: "Are you sure reply \"YES\" ?".localizedString(), message: "", preferredStyle: UIAlertController.Style.alert, cancelButtonTitle: "Cancel", destructiveButtonTitle: "OK", otherButtonTitles: nil, popoverPresentationControllerBlock: nil, tap: {[weak self] (controller, action, buttonIndex) in
                if (buttonIndex == controller.cancelButtonIndex) {
                    print("Cancel Tapped")
                } else if (buttonIndex == controller.destructiveButtonIndex) {
                    self?.replyActionPress(messageModel: messageModel, actionUpdateMessageItemType: actionUpdateMessageItemType)
                }
            })
        } else if(actionUpdateMessageItemType == .replyText) {
            self.replyActionPress(messageModel: messageModel, actionUpdateMessageItemType: actionUpdateMessageItemType)
        }
        
        
    }
    
    func doneActionPress(messageModel: MessageModel) {
        var listModel: [MessageModel]
        if(isSearch) {
            listModel = self.listSearchMessageModel
        } else {
            if(self.segmentedControl.selectedSegmentIndex == 0) {
                listModel = self.listNewMessageModel
                for index in 0..<self.listArchivesMessageModel.count {
                    if(self.listArchivesMessageModel[index].id == messageModel.id) {
                        self.listArchivesMessageModel.remove(at: index)
                        break
                    }
                }
                for index in 0..<self.listMessageModel.count {
                    if(self.listMessageModel[index].id == messageModel.id) {
                        self.listMessageModel.remove(at: index)
                        break
                    }
                }
            } else if(self.segmentedControl.selectedSegmentIndex == 1) {
                listModel = self.listMessageModel
                for index in 0..<self.listNewMessageModel.count {
                    if(self.listNewMessageModel[index].id == messageModel.id) {
                        self.listNewMessageModel.remove(at: index)
                        break
                    }
                }
                for index in 0..<self.listArchivesMessageModel.count {
                    if(self.listArchivesMessageModel[index].id == messageModel.id) {
                        self.listArchivesMessageModel.remove(at: index)
                        break
                    }
                }
            } else {
                listModel = self.listArchivesMessageModel
                for index in 0..<self.listNewMessageModel.count {
                    if(self.listNewMessageModel[index].id == messageModel.id) {
                        self.listNewMessageModel.remove(at: index)
                        break
                    }
                }
                for index in 0..<self.listMessageModel.count {
                    if(self.listMessageModel[index].id == messageModel.id) {
                        self.listMessageModel.remove(at: index)
                        break
                    }
                }
            }
            
        }
        
        for index in 0..<listModel.count {
            if(listModel[index].id == messageModel.id) {
                if(isSearch) {
                    self.listSearchMessageModel.remove(at: index)
                } else {
                    
                    if(self.segmentedControl.selectedSegmentIndex == 0) {
                        self.listNewMessageModel.remove(at: index)
                    } else if(self.segmentedControl.selectedSegmentIndex == 1) {
                        self.listMessageModel.remove(at: index)
                    } else {
                        self.listArchivesMessageModel.remove(at: index)
                    }
                    
                }
                self.tableView.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                //ServiceData.sharedInstance.deincrementUnreadMessageCount()
                self.getMessageUnreadCount()
                ServiceData.sharedInstance.taskMessageDone(id: messageModel.id).continueOnSuccessWith(continuation: { task in
                    print("")
                    ApplicationData.sharedInstance.deleteMessageModel(id: messageModel.id)
                }).continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    self.hideMBProgressHUD(true)
                    
                })
                return
            }
        }
    }
    
    func markActionPress(messageModel: MessageModel) {
        
        var listModel: [MessageModel]
        if(isSearch) {
            listModel = self.listSearchMessageModel
        } else {
            if(self.segmentedControl.selectedSegmentIndex == 0) {
                listModel = self.listNewMessageModel
            } else if(self.segmentedControl.selectedSegmentIndex == 1) {
                listModel = self.listMessageModel
            } else {
                listModel = self.listArchivesMessageModel
            }
        }
        for index in 0..<listModel.count {
            if(listModel[index].id == messageModel.id) {
                self.showMBProgressHUD("Loading...".localizedString(), animated: true)
                self.segmentedControl.isEnabled = false
                ServiceData.sharedInstance.taskMessageMarkAsRead(id: messageModel.id).continueOnSuccessWith(continuation: { task in
                    ApplicationData.sharedInstance.getBlockRealmWrite {
                        /*
                        if let result = task as? JSON {
                            let messageModel = MessageModel(json: result)
                            listModel[index].childrens.append(messageModel)
                        }
                        */
                        listModel[index].status = .read
                    }
                    self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                    self.hideMBProgressHUD(true)
                    self.segmentedControl.isEnabled = true
                    //ServiceData.sharedInstance.deincrementUnreadMessageCount()
                    self.getMessageUnreadCount()
                    
                }).continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    self.hideMBProgressHUD(true)
                    self.segmentedControl.isEnabled = true
                })
                return
            }
        }
    }
    
    func replyActionPress(messageModel: MessageModel, actionUpdateMessageItemType: ActionUpdateMessageItemType) {
        if(actionUpdateMessageItemType == .replyText) {
            self.showNoteBarButtonItemPressed(messageModel: messageModel)
            return
        }
        var listModel: [MessageModel]
        if(isSearch) {
            listModel = self.listSearchMessageModel
        } else {
            if(self.segmentedControl.selectedSegmentIndex == 0) {
                listModel = self.listNewMessageModel
            } else if(self.segmentedControl.selectedSegmentIndex == 1) {
                listModel = self.listMessageModel
            } else {
                listModel = self.listArchivesMessageModel
            }
            
        }
        for index in 0..<listModel.count {
            if(listModel[index].id == messageModel.id) {
                self.showMBProgressHUD("Loading...".localizedString(), animated: true)
                self.segmentedControl.isEnabled = false
                var messageStatusType = MessageStatusType.repliedbyYes
                if(actionUpdateMessageItemType == ActionUpdateMessageItemType.replyYes) {
                    messageStatusType = MessageStatusType.repliedbyYes
                } else if(actionUpdateMessageItemType == ActionUpdateMessageItemType.replyNo) {
                    messageStatusType = MessageStatusType.repliedbyNo
                }
                ServiceData.sharedInstance.taskMessageAdd(parentID: messageModel.id, message: "", messageStatusType: messageStatusType).continueOnSuccessWith(continuation: { task in
                    ApplicationData.sharedInstance.getBlockRealmWrite {
                        if let result = task as? JSON {
                            let messageModel = MessageModel(json: result)
                            listModel[index].childrens.append(messageModel)
                        }
                        listModel[index].status = .replied
                    }
                    
                    self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                    self.hideMBProgressHUD(true)
                    self.segmentedControl.isEnabled = true
                    //ServiceData.sharedInstance.deincrementUnreadMessageCount()
                    self.getMessageUnreadCount()
                }).continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    self.hideMBProgressHUD(true)
                    self.segmentedControl.isEnabled = true
                })
                return
            }
        }
    }

    
    func showNoteBarButtonItemPressed(messageModel: MessageModel) {
        let popupNoteViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopupNoteViewController") as! PopupNoteViewController
        popupNoteViewController.popupNoteViewControllerDelagate = self
        popupNoteViewController.object = messageModel
        let popupController = STPopupController.init(rootViewController: popupNoteViewController)
        popupController.containerView.layer.cornerRadius = 3
        popupController.transitionStyle = .fade
        popupController.navigationBar.tintColor = UIColor.black
        popupController.present(in: self)
    }
    
    //MARK - PopupNoteViewControllerDelagate
    func popupNoteViewControllerSendPress(text: String, object: NSObject?) {
        let text = text
        guard let messageModel = object as? MessageModel else {
            return
        }
        var listModel: [MessageModel]
        if(isSearch) {
            listModel = self.listSearchMessageModel
        } else {
            if(self.segmentedControl.selectedSegmentIndex == 0) {
                listModel = self.listNewMessageModel
            } else if(self.segmentedControl.selectedSegmentIndex == 1) {
                listModel = self.listMessageModel
            } else {
                listModel = self.listArchivesMessageModel
            }
            
        }
        for index in 0..<listModel.count {
            if(listModel[index].id == messageModel.id) {
                self.showMBProgressHUD("Loading...".localizedString(), animated: true)
                self.segmentedControl.isEnabled = false
                ServiceData.sharedInstance.taskMessageAdd(parentID: messageModel.id, message: text, messageStatusType: MessageStatusType.repliedbyText).continueOnSuccessWith(continuation: { task in
                    ApplicationData.sharedInstance.getBlockRealmWrite {
                        if let result = task as? JSON {
                            let messageModel = MessageModel(json: result)
                            listModel[index].childrens.append(messageModel)
                        }
                        listModel[index].status = .replied
                    }
                    
                    self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                    self.hideMBProgressHUD(true)
                    self.segmentedControl.isEnabled = true
                    //ServiceData.sharedInstance.deincrementUnreadMessageCount()
                    self.getMessageUnreadCount()
                }).continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    self.hideMBProgressHUD(true)
                    self.segmentedControl.isEnabled = true
                })
                return
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    //MARK - PushNotifyDelegate
    func notifyReceiver(item: NotifyModel) {
        
    }
    
    func notifyOpened(item: NotifyModel) {
        MessageListViewController.notifyModelFromNotify = nil
        let messageId = Int(item.objectId) ?? 0
        if(messageId == 0) {
            return
        }
        self.refreshData()
        ServiceData.sharedInstance.taskMessageDetail(id: Int(item.objectId)!).continueOnSuccessWith(continuation: { task in
            let result = task as! JSON
            let messageModel = MessageModel(json: result)
            var listModel: [MessageModel]
            if(self.isSearch) {
                listModel = self.listSearchMessageModel
            } else {
                if(self.segmentedControl.selectedSegmentIndex == 0) {
                    listModel = self.listNewMessageModel
                } else if(self.segmentedControl.selectedSegmentIndex == 1) {
                    listModel = self.listMessageModel
                } else {
                    listModel = self.listArchivesMessageModel
                }
                
            }
            
            self.tableView.finishInfiniteScroll()
            
            if(!self.refreshControl.isRefreshing) {
                for index in 0..<listModel.count {
                    if(messageModel.id == listModel[index].id) {
                        listModel[index] = messageModel
                        self.tableView.reloadData()
                        break
                    }
                }
                
            }
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                if let tabBarController = appDelegate.window?.rootViewController as? UITabBarController {
                    tabBarController.selectedIndex = 1
                }
            }
            self.navigationController?.popToRootViewController(animated: false)
            
            self.hideMBProgressHUD(true)
            if(item.isHtml) {
                self.pushWebViewMessageViewController(id: messageModel.id, title: messageModel.sender)
            } else {
                self.pushMessageDetailViewController(messageModel: messageModel)
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        
        
        
    }
    

}
