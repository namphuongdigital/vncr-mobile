//
//  PersonalScheduleFlyViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/14/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import Foundation
import DateTools
import SwiftyJSON

class PersonalScheduleFlyViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, ScheduleFlyTableViewCellDelegate, ReportPositionDelegate, SelectMonthHeaderViewDelegate {
    
    var selectMonthHeaderView: SelectMonthHeaderView!
    
    private var month: Int = 1
    
    private var pageIndex: Int = 1
    
    @IBOutlet weak var tableView: UITableView!
    
    var fromDateSeleted: Date!
    
    var toDateSeleted: Date!
    
    var listScheduleFly = Array<ScheduleFlightModel>()
    
    var refreshControl: UIRefreshControl!
    
    var dateUpdate: Date = Date()
    
    var refreshBarButtonItem: UIBarButtonItem!
    
    var selectedSchedule:ScheduleFlightModel?
    
    //Mark - SelectMonthHeaderViewDelegate
    func selectedMonth(month: Int) {
        self.month = month
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getDataResult()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.textTitle = "SCHEDULE FLY".localizedString()
        
        if self.navigationController?.viewControllers.count ?? 0 < 2 {
            self.setBarCloseButton()
        }
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        self.selectMonthHeaderView = SelectMonthHeaderView.loadView()
        self.selectMonthHeaderView.backgroundColor = UIColor.black
        self.selectMonthHeaderView.delegate = self
        let view = UIView(frame: self.selectMonthHeaderView.frame)
        view.addSubview(self.selectMonthHeaderView)
        tableView.tableHeaderView = view
        
        Broadcaster.register(ReportPositionDelegate.self, observer: self)
        
        
        self.tableView!.rowHeight = UITableView.automaticDimension
        self.tableView!.estimatedRowHeight = 320
        self.tableView.register(ScheduleFlyCell.nib, forCellReuseIdentifier:ScheduleFlyCell.identifier)
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            self?.loadMoreData() {
                self?.tableView!.finishInfiniteScroll()
            }
            
        }
        
        self.month = NSDate().month()
        self.selectMonthHeaderView.collectionView.scrollToItem(at: IndexPath.init(row: month - 1, section: 0), at: UICollectionView.ScrollPosition.left, animated: false)
        self.selectMonthHeaderView.setSelected(index: month - 1)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        self.tableView.alwaysBounceVertical = true
        
        refreshBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.refreshBarButtonItemPressed(_:)))
        refreshBarButtonItem.setFAIcon(icon: .FARefresh, iconSize: 20)
        self.navigationItem.rightBarButtonItems = [self.refreshBarButtonItem]
        
    }
    
    override func networkDidChanged(connectedInternet: Bool, reachability: Reachability) {
        super.networkDidChanged(connectedInternet: connectedInternet, reachability: reachability)
        tableView.reloadData()
    }
    
    @objc func refreshBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if(refreshControl.isRefreshing) {
            return
        }
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getDataResult()
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
 
    }
    
    func loadMoreData(_ handler: (() -> Void)?) {
        if(self.listScheduleFly.count > 1 && !self.refreshControl.isRefreshing) {
            
            let userId = ServiceData.sharedInstance.userId ?? 0
            
            pageIndex += 1
            ServiceDataCD.getFlightsPersonal(month: month, pageIndex: pageIndex, userId: userId, isOffline: isOfflineMode) {[weak self] list, error in
                guard let `self` = self else { return }
                self.refreshControl.endRefreshing()
                self.hideMBProgressHUD(true)
                
                if let error = error {
                    self.showAlert("Alert".localizedString(), stringContent: error)
                } else {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    list.forEach({
                        listIndexPath.append(IndexPath(row: self.listScheduleFly.count, section: 0))
                        self.listScheduleFly.append($0)
                    })
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                
                handler?()
            }
        } else {
            handler?()
        }
        
    }
    
    
    func buttonBackPress(sender: UIButton) {
        let _ = self.navigationController?.popViewController(animated: true)
    }
    
    func showStartDateInfo(date: Date) {
        
    }
    
    func showEndDateInfo(date: Date) {
    }
    
    @objc func refreshData() {
        getDataResult()
    }
    
    func getDataResult() {
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        
        let userId = ServiceData.sharedInstance.userId ?? 0
        
        pageIndex = 1
        
        ServiceDataCD.getFlightsPersonal(month: month, pageIndex: pageIndex, userId: userId, isOffline: isOfflineMode) {[weak self] list, error in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                
                if let error = error {
                    self.showAlert("Alert".localizedString(), stringContent: error)
                } else {
                    self.textTitle = "\("Last update".localizedString()) : \(Date().dateTimeToDateTimeHHmm ?? "")"
                    self.listScheduleFly = []
                    self.listScheduleFly = list
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func configure(cell: PersonalExpanedScheduleFlyTableViewCell, at indexPath: IndexPath) {
        let scheduleFlyModel = self.listScheduleFly[indexPath.row]
        cell.loadData(scheduleFlyModel: scheduleFlyModel, indexPath: indexPath)
        cell.delegateAction = self
    }
    
    //MARK: UITableViewDataSource methods
    @objc override func changeDataMode(_ sender:UIButton) {
        super.changeDataMode(sender)
        getDataResult()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let btn = UIButton(type: .custom)
        btn.frame = CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 50))
        btn.setTitleColor(.systemBlue, for: UIControl.State())
        btn.removeTarget(self, action: #selector(self.changeDataMode(_:)), for: .touchUpInside)
        btn.addTarget(self, action: #selector(self.changeDataMode(_:)), for: .touchUpInside)
        if isOfflineMode && self.isConnectedInternet {
            btn.isSelected = false
            btn.setTitle("Turn off Offline Mode".localizedString(), for: UIControl.State())
            return btn
        } else if !isOfflineMode && !self.isConnectedInternet { // online & no internet
            btn.isSelected = true
            btn.setTitle("Turn on Offline Mode".localizedString(), for: UIControl.State())
            return btn
        }
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listScheduleFly.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let scheduleFly = self.listScheduleFly[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ScheduleFlyCell.identifier) as! ScheduleFlyCell
        cell.show(scheduleFly: scheduleFly)
        cell.onGoto = {[weak self] cell in
            guard let schedule = cell.scheduleFly else {return}
            
            let vc = ScheduleFlyActionController(listType: .personal, scheduleFly: schedule) {[weak self] sheduleFlyAction in
                self?.handleScheduleFlyAction(scheduleFly: schedule, sheduleFlyAction: sheduleFlyAction, indexPath: self?.tableView.indexPath(for: cell))
            }
            // show popover cho ipad
            if self?.isIpad == true {
                vc.popoverPresentationController?.sourceView = cell.btnGoto
                vc.isModalInPopover = false
            }
            self?.present(vc: vc, transitioning: SlideUpAnimateTransitionDelegate(), completion: nil)
        }
        return cell
        
//        if(scheduleFly.isExpanded == true) {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalExpanedScheduleFlyTableViewCell")! as! PersonalExpanedScheduleFlyTableViewCell
//            /*
//            let scheduleFlyModel = self.listScheduleFly[indexPath.row]
//            cell.loadData(scheduleFlyModel: scheduleFlyModel, indexPath: indexPath)
//            cell.delegateAction = self
//            */
//            self.configure(cell: cell, at: indexPath)
//            return cell
//        }
//
//        let cell = tableView.dequeueReusableCell(withIdentifier: "PersonalScheduleFlyTableViewCell")! as! PersonalScheduleFlyTableViewCell
//        let scheduleFlyModel = self.listScheduleFly[indexPath.row]
//        cell.loadData(scheduleFlyModel: scheduleFlyModel, indexPath: indexPath)
//        cell.delegateAction = self
//        return cell
        
    }
    
//    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//    }
    
//    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
//
//        let scheduleFly = self.listScheduleFly[indexPath.row]
//        if(scheduleFly.isExpanded == false) {
//            var array = Array<IndexPath>()
//            if let selectedIndexPath = self.tableView.indexPathForSelectedRow {
//                let item = self.listScheduleFly[selectedIndexPath.row]
//                item.isExpanded = false
//                array.append(selectedIndexPath)
//            }
//            scheduleFly.isExpanded = true
//            array.append(indexPath)
//            self.tableView.reloadRows(at: array, with: .automatic)
//        } else {
//            scheduleFly.isExpanded = false
//            self.tableView.reloadRows(at: [indexPath], with: .automatic)
//            return nil
//        }
//        return indexPath
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let scheduleFly = self.listScheduleFly[indexPath.row]
        
        let titleText = "\(scheduleFly.flightNo)/\(scheduleFly.departedDate) \(scheduleFly.routing) \(scheduleFly.classify)"
        let vc = ScheduleFlyDetailController(detailType: .personal, scheduleFly: scheduleFly, titleText: titleText) {[weak self] sheduleFlyAction in
            // should be let indexPath = nil, because action be involked from detail so schedule model has already full information
            self?.handleScheduleFlyAction(scheduleFly: scheduleFly, sheduleFlyAction: sheduleFlyAction, indexPath: nil)
        }

        if splitViewController != nil {
            if let nv = splitViewController?.viewControllers.last as? UINavigationController,
               let vc = nv.viewControllers.last as? ScheduleFlyDetailController, vc.detailType == .personal {
                if vc.scheduleFly?.flightID == scheduleFly.flightID {return}
            }
            self.splitViewController?.showDetailViewController(UINavigationController(rootViewController: vc), sender: nil)
        } else {
            self.navigationController?.pushViewController(vc, animated: true)
        }
        selectedSchedule = scheduleFly
    }
    
    func handleScheduleFlyAction(scheduleFly:ScheduleFlightModel,
                                 sheduleFlyAction:ScheduleFlyActionController.ScheduleFlyAction,
                                 indexPath:IndexPath?) {
        var isSameSchedule = true
        // check same schedule
        isSameSchedule = self.selectedSchedule?.flightID == scheduleFly.flightID
        
        var shouldMarkSelected = true
        switch sheduleFlyAction {
        case .assignment:
            self.pushAssessmentCrewDutyListViewController(scheduleFlyModel: scheduleFly, isSameSchedule: isSameSchedule)
        case .report:
            self.pushFlightReportListViewController(scheduleFlyModel: scheduleFly, isSameSchedule: isSameSchedule)
        case .survey:
            self.pushSurveyListViewController(scheduleFlyModel: scheduleFly, isListSaved: false, isSameSchedule: isSameSchedule)
        case .seatMap:
            let textTitle = "\(scheduleFly.flightNo)/\(scheduleFly.departedDate) \(scheduleFly.routing) \(scheduleFly.classify)"
            self.presentSeatmapViewController(seatmap: AircraftCategoryType(rawValue: scheduleFly.seatmap) ?? .A32, title: textTitle, flightId: scheduleFly.flightID)
        case .task:
            self.pushLeadreportPosition(scheduleFly: scheduleFly, isSameSchedule: isSameSchedule)
        case .oJT:
            self.pushOJTListExaminees(scheduleFly: scheduleFly, isSameSchedule: isSameSchedule)
        case .quickReport:
            self.pushReportFlightViewcontroller(scheduleFlyModel: scheduleFly, reportModel: ReportModel(), isSameSchedule: isSameSchedule)
        case .synchronize:
            self.synchronizeThirdServices(scheduleFlyModel: scheduleFly, nil)
        case .refreshLocal, .saveLocal:
            shouldMarkSelected = false
            if #available(iOS 10.0, *) {
                self.showMBProgressHUD("Saving...", animated: true)
                self.saveScheduleFlight(scheduleFlyModel: scheduleFly,
                                        isPersonal: true,
                                        isGetFull: indexPath != nil/* nếu indexPath == nil => action được gọi từ detail page */) { error in
                    DispatchQueue.main.async {[weak self] in
                        guard let `self` = self else { return }
                        if let error = error {
                            self.showAlert("Alert".localizedString(), stringContent: error as? String ?? "")
                        } else {
                            // chỉ reload lại khi đang ở trạng thái offline
                            if !NetworkObserver.shared.isConnectedInternet {
                                self.getDataResult()
                            }
                            self.showAlert("Alert".localizedString(), stringContent: "saveFlightSuccess".localizedString())
                        }
                        self.hideMBProgressHUD(true)
                    }
                }
            }
        case .deleteLocal:
            shouldMarkSelected = false
            if #available(iOS 10.0, *) {
                self.showMBProgressHUD("Deleting...", animated: true)
                self.deleteScheduleFlightLocal(flightId: scheduleFly.flightID) { error in
                    DispatchQueue.main.async {[weak self] in
                        guard let `self` = self else { return }
                        if let error = error {
                            self.showAlert("Alert".localizedString(), stringContent: error as? String ?? "")
                        } else {
                            // chỉ reload lại khi đang ở trạng thái offline
                            if !NetworkObserver.shared.isConnectedInternet {
                                self.getDataResult()
                            }
                            self.showAlert("Alert".localizedString(), stringContent: "deleteFlightSuccess".localizedString())
                        }
                        self.hideMBProgressHUD(true)
                    }
                }
            }
        @unknown default:
            shouldMarkSelected = false
        }
        if shouldMarkSelected, let indexPath = indexPath {
            if indexPath.row < listScheduleFly.count {
                selectedSchedule = listScheduleFly[indexPath.row]
            }
            self.tableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let scheduleFly = self.listScheduleFly[indexPath.row]
//        if(scheduleFly.isExpanded == true) {
//            if(UIDevice.current.userInterfaceIdiom == .phone) {
//                if(scheduleFly.note.count > 0) {
//                    //return 205
//                    return tableView.fd_heightForCell(withIdentifier: "PersonalExpanedScheduleFlyTableViewCell", configuration: {[weak self] (cell) in
//                        let cell = cell as! PersonalExpanedScheduleFlyTableViewCell
//                        self?.configure(cell: cell, at: indexPath)
//                    })
//                }
//                return 160
//            } else {
//                let scheduleFly = self.listScheduleFly[indexPath.row]
//                var heightRow: CGFloat = 140
//                if(scheduleFly.note.count > 0) {
//                    //heightRow = 170
//                    heightRow = tableView.fd_heightForCell(withIdentifier: "PersonalExpanedScheduleFlyTableViewCell", configuration: {[weak self] (cell) in
//                        let cell = cell as! PersonalExpanedScheduleFlyTableViewCell
//                        self?.configure(cell: cell, at: indexPath)
//                    })
//                } else {
//                    heightRow = 140
//                }
//                if(scheduleFly.trips.details.count > 4) {
//                    let row = scheduleFly.trips.details.count - 4
//                    let height = (CGFloat(row) * 24.0) + heightRow
//                    return height
//                }
//                return heightRow
//            }
//
//
//        }
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        // offline & have internet
        if isOfflineMode && self.isConnectedInternet {
            return 50
        } else if !isOfflineMode && !self.isConnectedInternet { // online & no internet
            return 50
        }
        return 0.01
    }
    
    //ScheduleFlyTableViewCellDelegate
    func reportPositonScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        let leadReportPositionViewController = self.storyboard?.instantiateViewController(withIdentifier: "LeadReportPositionViewController") as! LeadReportPositionViewController
        leadReportPositionViewController.scheduleFlyModel = scheduleFly
        self.navigationController?.pushViewController(leadReportPositionViewController, animated: true)
    }
    
    func reportFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        self.pushFlightReportListViewController(scheduleFlyModel: scheduleFly, isSameSchedule: true)
    }
    
    func assessmentFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        self.pushAssessmentCrewDutyListViewController(scheduleFlyModel: scheduleFly, isSameSchedule: true)
    }
    
    func surveyListFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        self.pushSurveyListViewController(scheduleFlyModel: scheduleFly, isListSaved: false, isSameSchedule: true)
    }
    
    func ojtListFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel) {
        //Harry Nhan changed
        //self.pushOJTListViewController(scheduleFlyModel: scheduleFly, isListSaved: false, cid: "")
        self.pushOJTListExamineesViewController(scheduleFlyModel: scheduleFly)
    }
    
    //MARK -
    
    func buttonSearchPress(sender: UIButton) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getDataResult()
    }
    
    func buttonSelectDateStartPress(sender: UIButton) {
        //let buttonItemView = sender.value(forKey: "view") as! UIView
        CalendarPickerPopover.appearFrom(
            originView: sender,
            baseViewController: self,
            title: "Date Picker".localizedString(),
            dateMode: .date,
            initialDate: fromDateSeleted,
            doneAction: {[weak self] selectedDate in
                print("selectedDate \(selectedDate)")
                self?.showStartDateInfo(date: selectedDate)
            },
            cancelAction: {print("cancel")}
        )
    }
    
    func buttonSelectDateEndPress(sender: UIButton) {
        //let buttonItemView = sender.value(forKey: "view") as! UIView
        CalendarPickerPopover.appearFrom(
            originView: sender,
            baseViewController: self,
            title: "Date Picker".localizedString(),
            dateMode: .date,
            initialDate: toDateSeleted,
            doneAction: {[weak self] selectedDate in
                print("selectedDate \(selectedDate)")
                self?.showEndDateInfo(date: selectedDate)
            },
            cancelAction: {print("cancel")}
        )
    }
    
    //
    
    @IBAction func radioButtonAllFlighPressed(_ sender: UIButton) {
    }
    
    //Mark - ReportPositionDelegate
    func updateReportPosition(item: ScheduleFlightModel) {
        for index in 0..<self.listScheduleFly.count {
            if (item.flightID == self.listScheduleFly[index].flightID) {
                self.self.listScheduleFly[index] = item
                self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                return
            }
        }
    }

}
