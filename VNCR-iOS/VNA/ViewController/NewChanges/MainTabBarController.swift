//
//  MainTabBarController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/5/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class MainTabBarController: UITabBarController, UITabBarControllerDelegate, UnreadMessageCountDelegate, PushNotifyDelegate {
    
    var scrollEnabled: Bool = true
    
    var previousIndex = 0

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Always adopt a dark interface style.
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // setup SchduleFlyViewController trên ipad và iphone
        let scheduleFlyViewController = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleFlyViewController") as! ScheduleFlyViewController
        // embed splitviewcontroller tren ipad
        if UIDevice.current.userInterfaceIdiom == .pad {
            viewControllers?.insert(BaseSplitController(root: UINavigationController(rootViewController: scheduleFlyViewController), detail: nil), at: 0)
        } else {
            viewControllers?.insert(UINavigationController(rootViewController: scheduleFlyViewController), at: 0)
        }
        
        delegate = self
        // Do any additional setup after loading the view.
        //UIApplication.shared.statusBarStyle = .lightContent
        if #available(iOS 13.0, *) {
            let appearanceTabbar = UITabBarAppearance()
            appearanceTabbar.selectionIndicatorTintColor = UIColor("#166880")
            appearanceTabbar.backgroundColor = .white
            self.tabBar.standardAppearance = appearanceTabbar
            if #available(iOS 15.0, *) {
                self.tabBar.scrollEdgeAppearance = appearanceTabbar
            }
        } else {
            UITabBar.appearance().tintColor = UIColor("#166880")
        }
        //UITabBar.appearance().barTintColor = UIColor.black
        if let array = self.tabBar.items {
            array[0].image = #imageLiteral(resourceName: "flight-icon")
            array[0].title = "Flight".localizedString()
            array[1].title = "News".localizedString()
            array[2].title = ServiceData.sharedInstance.userModel?.firstNameVn ?? "Personal".localizedString()//"Home".localizedString()
            array[3].title = "Inbox".localizedString()//"Support".localizedString()
            array[4].title = "More".localizedString()
        }
        
        self.selectedIndex = 2
        
        Broadcaster.register(UnreadMessageCountDelegate.self, observer: self)
        
        ServiceData.sharedInstance.taskMesageCount(phoneNo: ServiceData.sharedInstance.userModel?.phone ?? "").continueOnSuccessWith(continuation: { task in
            let result = task as! JSON
            ServiceData.sharedInstance.unreadMessageCount = result.intValue
            Broadcaster.notify(UnreadMessageCountDelegate.self) {
                $0.unreadMessageCountUpdate(unreadMessageCount: result.intValue)
            }
            
        }).continueOnErrorWith(continuation: { error in
            print(error)
        })
        Broadcaster.register(PushNotifyDelegate.self, observer: self)
        
        if let notifyModelFromNotify = MessageListViewController.notifyModelFromNotify {
            self.selectedIndex = notifyModelFromNotify.ui
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name("filterNews"), object: nil, queue: .main) {[weak self] noti in
            guard let `self` = self else { return }
            guard let id = noti.userInfo?["id"] as? String else {return}
            self.selectedIndex = 1
            DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(200)) {
                if let nv = self.selectedViewController as? UINavigationController,
                   let vc = nv.find(viewController: NewListViewController.self) as? NewListViewController {
                    nv.popViewController(animated: true)
                    if vc.isAppeared {
                        if vc.isRequesting == false {
                            vc.selectedFilter = id
                        } else {
                            vc.waitNewFilter = id
                        }
                    } else {
                        vc.waitNewFilter = id
                    }
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    //MARK - PushNotifyDelegate
    func notifyReceiver(item: NotifyModel) {
        
    }
    
    func notifyOpened(item: NotifyModel) {
        self.selectedIndex = item.ui
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK - UnreadMessageCountDelegate
    func unreadMessageCountUpdate(unreadMessageCount: Int) {
        if(unreadMessageCount != 0) {
            self.viewControllers?[3].tabBarItem.badgeValue = "\(unreadMessageCount)"
        } else {
            self.viewControllers?[3].tabBarItem.badgeValue = nil
        }
    }
    
    //
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        guard scrollEnabled else {
            return
        }
        
        guard let index = viewControllers?.firstIndex(of: viewController) else {
            return
        }
        
        if index == previousIndex {
            var scrollViews = [UIScrollView]()
            self.iterateThroughSubviews(parentView: self.view) { (scrollView) in
                scrollViews.append(scrollView)
            }
            
            guard let scrollView = scrollViews.first else {
                return
            }
            scrollView.setContentOffset(CGPoint(x: 0, y: -scrollView.contentInset.top), animated: true)
        }
        
        previousIndex = index
    }
    
    
    private func iterateThroughSubviews(parentView: UIView?, onRecognition: (UIScrollView) -> Void) {
        guard let view = parentView else {
            return
        }
        
        for subview in view.subviews {
            if let scrollView = subview as? UIScrollView, scrollView.scrollsToTop == true {
                onRecognition(scrollView)
            }
            
            iterateThroughSubviews(parentView: subview, onRecognition: onRecognition)
        }
    }

}
