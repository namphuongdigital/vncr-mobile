//
//  IncomeDetailViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/25/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import STPopup


class IncomeDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, MessageItemTableViewCellDelegate, PopupNoteViewControllerDelagate {

    @IBOutlet weak var tableView: UITableView!
    
    var messageModel: MessageModel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.textTitle = messageModel.sender
        self.navigationController?.navigationBar.tintColor = UIColor.white
        
        tableView.keyboardDismissMode = .onDrag
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        //self.textTitle = "FORM LIST".localizedString()
        
        self.tableView.alwaysBounceVertical = true
        //self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        //self.tableView!.infiniteScrollIndicatorMargin = 40
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messageModel.childrens.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageItemTableViewCell")! as! MessageItemTableViewCell
        if(indexPath.row == 0) {
            cell.loadContent(messageModel: self.messageModel)
            cell.delegateAction = self
        } else {
            cell.loadContent(messageModel: self.messageModel.childrens[indexPath.row - 1], isAddActionButton: false)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    // MARK: - MessageItemTableViewCellDelegate
    func actionMessageItemTableViewCell(messageModel: MessageModel, actionUpdateMessageItemType: ActionUpdateMessageItemType) {
        if(actionUpdateMessageItemType == .done) {
            self.doneActionPress(messageModel: messageModel)
        } else if(actionUpdateMessageItemType == .mark) {
            self.markActionPress(messageModel: messageModel)
        } else if(actionUpdateMessageItemType == .replyNo) {
            self.replyActionPress(messageModel: messageModel, actionUpdateMessageItemType: actionUpdateMessageItemType)
        } else if(actionUpdateMessageItemType == .replyYes) {
            self.replyActionPress(messageModel: messageModel, actionUpdateMessageItemType: actionUpdateMessageItemType)
        } else if(actionUpdateMessageItemType == .replyText) {
            self.replyActionPress(messageModel: messageModel, actionUpdateMessageItemType: actionUpdateMessageItemType)
        }
        
        
    }
    
    func doneActionPress(messageModel: MessageModel) {
        ServiceData.sharedInstance.taskIncomeDone(id: messageModel.id).continueOnSuccessWith(continuation: { task in
            print("")
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            
        })
    }
    
    func markActionPress(messageModel: MessageModel) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskIncomeMarkAsRead(id: messageModel.id).continueOnSuccessWith(continuation: { task in
            print("")
            if let result = task as? JSON {
                self.messageModel = MessageModel(json: result)
                self.tableView.reloadData()
            }
            self.hideMBProgressHUD(true)
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        return
    }
    
    func replyActionPress(messageModel: MessageModel, actionUpdateMessageItemType: ActionUpdateMessageItemType) {
        if(actionUpdateMessageItemType == .replyText) {
            self.showNoteBarButtonItemPressed(messageModel: messageModel)
            return
        }
        var messageStatusType = MessageStatusType.repliedbyYes
        if(actionUpdateMessageItemType == ActionUpdateMessageItemType.replyYes) {
            messageStatusType = MessageStatusType.repliedbyYes
        } else if(actionUpdateMessageItemType == ActionUpdateMessageItemType.replyNo) {
            messageStatusType = MessageStatusType.repliedbyNo
        }
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskIncomeAdd(parentID: messageModel.id, message: "", messageStatusType: messageStatusType).continueOnSuccessWith(continuation: { task in
            print("")
            if let result = task as? JSON {
                let messageModel = MessageModel(json: result)
                self.messageModel.childrens.append(messageModel)
                self.tableView.reloadData()
            }
            self.hideMBProgressHUD(true)
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        return
    }
    
    
    func showNoteBarButtonItemPressed(messageModel: MessageModel) {
        let popupNoteViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopupNoteViewController") as! PopupNoteViewController
        popupNoteViewController.popupNoteViewControllerDelagate = self
        popupNoteViewController.object = messageModel
        let popupController = STPopupController.init(rootViewController: popupNoteViewController)
        popupController.containerView.layer.cornerRadius = 3
        popupController.transitionStyle = .fade
        popupController.navigationBar.tintColor = UIColor.black
        popupController.present(in: self)
    }
    
    //MARK - PopupNoteViewControllerDelagate
    func popupNoteViewControllerSendPress(text: String, object: NSObject?) {
        let text = text
        guard let messageModel = object as? MessageModel else {
            return
        }
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskIncomeAdd(parentID: messageModel.id, message: text, messageStatusType: MessageStatusType.repliedbyText).continueOnSuccessWith(continuation: { task in
            print("")
            
            if let result = task as? JSON {
                let messageModel = MessageModel(json: result)
                self.messageModel.childrens.append(messageModel)
                self.tableView.reloadData()
            }
            self.hideMBProgressHUD(true)
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }

}
