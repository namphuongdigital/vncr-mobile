//
//  PopupInfoSeatViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 3/24/19.
//  Copyright © 2019 OneTeam. All rights reserved.
//

import UIKit
import STPopup
import IQKeyboardManagerSwift

protocol PopupInfoSeatViewControllerDelagate: NSObjectProtocol {
    func popupInfoSeatViewControllerOKPress(viewController: PopupInfoSeatViewController)
    
}

class PopupInfoSeatViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    weak var seatModel: SeatModel?
    
    var textContent: String = ""
    
    var textButtonDone: String = "OK".localizedString()
    
    weak var delagate: PopupInfoSeatViewControllerDelagate?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    @IBOutlet weak var labelPaxName: UILabel!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelDesc: UILabel!
    
    @IBOutlet weak var buttonOK: UIButton!
    
    @IBOutlet weak var textViewNote: IQTextView!
    
    var listImageMenuItem = [ImageMenuItem]()
    
    deinit {
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        IQKeyboardManager.shared.enableAutoToolbar = true
        self.contentSizeInPopup = CGSize(width: 320, height: 480)
        self.landscapeContentSizeInPopup = CGSize(width: 320, height: 480)
        
        self.imageViewAvatar.clipsToBounds = true
        
        self.buttonOK.backgroundColor = UIColor("#166880")
        self.buttonOK.layer.cornerRadius = 2.5
        self.buttonOK.setTitleColor(UIColor.white, for: .normal)
        self.buttonOK.addTarget(self, action: #selector(buttonOKPress), for: .touchUpInside)
        
        self.textViewNote.layer.borderColor = UIColor.lightGray.cgColor
        self.textViewNote.layer.borderWidth = 0.4
        self.textViewNote.layer.cornerRadius = 2.5
        self.textViewNote.text = textContent
        
        self.navigationItem.title = "Info".localizedString()
        
        self.buttonOK.setTitle(textButtonDone, for: .normal)
        
        if let seatModel = self.seatModel {
            self.imageViewAvatar.loadImageNoProgressBar(url: URL(string: seatModel.avatar))
            self.labelPaxName.text = seatModel.paxName
            self.labelTitle.text = seatModel.title
            self.labelDesc.text = seatModel.desc
        }
        
        
        collectionView.backgroundColor = UIColor.clear
        collectionView.register(UINib(nibName: "ImageMenuItemCell", bundle: nil), forCellWithReuseIdentifier: "ImageMenuItemCell")
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).scrollDirection = .horizontal
        
        var imageMenuItem = ImageMenuItem()
        imageMenuItem.title = ""
        imageMenuItem.imageName = "Meal-C2.jpg"
        listImageMenuItem.append(imageMenuItem)
        
        imageMenuItem = ImageMenuItem()
        imageMenuItem.title = ""
        imageMenuItem.imageName = "Meal-C3.jpg"
        listImageMenuItem.append(imageMenuItem)
        
        imageMenuItem = ImageMenuItem()
        imageMenuItem.title = ""
        imageMenuItem.imageName = "Meal-C5.jpg"
        listImageMenuItem.append(imageMenuItem)
        
        imageMenuItem = ImageMenuItem()
        imageMenuItem.title = ""
        imageMenuItem.imageName = "Meal-C6.jpg"
        listImageMenuItem.append(imageMenuItem)
        
        
        imageMenuItem = ImageMenuItem()
        imageMenuItem.title = ""
        imageMenuItem.imageName = "Meal-C7.jpg"
        listImageMenuItem.append(imageMenuItem)
        
        collectionView.reloadData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK - UICollectionViewDataSource
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    //2
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listImageMenuItem.count
    }
    
    //3
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageMenuItemCell", for: indexPath) as! ImageMenuItemCell
        cell.imageMenuItem = self.listImageMenuItem[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for item in self.listImageMenuItem {
            item.isCheck = false
        }
        self.listImageMenuItem[indexPath.row].isCheck = true
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 160, height: 120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0.0, left: 10.0, bottom: 0.0, right: 10.0)
    }
    
    @objc func buttonOKPress() {
        self.delagate?.popupInfoSeatViewControllerOKPress(viewController: self)
        self.dismiss(animated: true, completion: nil)
    }
    
    

}
