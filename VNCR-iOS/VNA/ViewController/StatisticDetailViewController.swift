//
//  StatisticDetailViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 7/15/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
//import Charts

class StatisticDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, ContentTableViewCellDelegate {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var pieChartViewContain: UIView!
    
    var pieChartView: PieChartView!
    
    @IBOutlet weak var barChartViewContain: UIView!
    
    var barChartView: BarChartView!
    
    var id: Int = 0
    
    var pageNumber = 1
    
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero) //CustomSearchBarView = CustomSearchBarView(searchBarOriginX: 2)
    
    var listContents = [ContentModel]()
    
    var listChartItems = [ChartItemModel]()
    
    var listBarChartItems = [ChartItemModel]()
    
    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var tableView: UITableView!
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var searchText = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.layoutIfNeeded()
        //self.searchBar.sizeToFit()
        //self.navigationItem.setHidesBackButton(true, animated:true)
        //self.customBackButton(imageName: "back-icon.png", renderingMode: UIImageRenderingMode.alwaysTemplate)
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let refreshBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.refreshBarButtonItemPressed(_:)))
        refreshBarButtonItem.setFAIcon(icon: .FARefresh, iconSize: 20)
        self.navigationItem.rightBarButtonItems = [refreshBarButtonItem]
        self.setupSearchBar()
        
        segmentedControl.tintColor = UIColor.white
        segmentedControl.addTarget(self, action: #selector(segmentedControlValueChange(_:)), for: .valueChanged)
        segmentedControl.setTitle("Bar chart".localizedString(), forSegmentAt: 0)
        segmentedControl.setTitle("Pie chart".localizedString(), forSegmentAt: 1)
        segmentedControl.setTitle("List".localizedString(), forSegmentAt: 2)
        self.navigationItem.titleView = segmentedControl
        
        self.pieChartViewContain.isHidden = true
        self.barChartViewContain.isHidden = true
        self.tableView.isHidden = true
        
    
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        
        let nib = UINib(nibName: "ContentTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ContentTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.keyboardDismissMode = .onDrag
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tableView.alwaysBounceVertical = true
        
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            self?.loadMoreData() {
                self?.tableView!.finishInfiniteScroll()
                self?.refreshControl?.endRefreshing()
            }
            
        }
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        
        loadDataBarChart()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func refreshBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if(refreshControl.isRefreshing) {
            return
        }
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        if(segmentedControl.selectedSegmentIndex == 0) {
            loadDataBarChart()
        } else if (segmentedControl.selectedSegmentIndex == 1) {
            loadDataChart()
        }  else {
            getNewData()
        }
    }
    
    @objc func segmentedControlValueChange(_ : UISegmentedControl) {
        if(segmentedControl.selectedSegmentIndex == 0) {
            barChartViewContain.isHidden = false
            pieChartViewContain.isHidden = true
            self.tableView.isHidden = true
            if(barChartView == nil) {
                loadDataBarChart()
            } else {
                showBarChart()
            }
        } else if (segmentedControl.selectedSegmentIndex == 1) {
            pieChartViewContain.isHidden = false
            barChartViewContain.isHidden = true
            self.tableView.isHidden = true
            if(pieChartView == nil) {
                loadDataChart()
            } else {
                showPieChart()
            }
            
        }  else {
            pieChartViewContain.isHidden = true
            barChartViewContain.isHidden = true
            self.tableView.isHidden = false
            if(self.listContents.count == 0) {
                getNewData()
            }
        }
    }
    
    @objc func refreshData() {
        getNewData()
        
    }
    
    func getNewData() {
        //self.textTitle = "DEVICES".localizedString()
        pageNumber = 1
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskStatisticDetails(id: self.id, keyword: searchText, pageIndex: pageNumber).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<ContentModel>()
                for item in array {
                    let model = ContentModel(json: item)
                    arrayModel.append(model)
                }
                self.listContents = arrayModel
                
            }
            self.tableView.reloadData()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    func setupSearchBar() {
        //self.searchBar.searchBarOriginX = 0
        //self.searchBar.tintColor = UIColor("#166880")
        //UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        //        self.searchBar.tintColor = .white
        //        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        //self.tableView.titleView = self.searchBar
        self.searchBar.searchBarStyle = UISearchBar.Style.minimal
        self.searchBar.placeholder = "Search".localizedString()
        searchBar.delegate = self
        //self.tableView.tableHeaderView = self.searchBar
        //self.searchBar.sizeToFit()
    }
    
    //MARK - UISearchBarDelegate
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.navigationItem.leftBarButtonItem = nil
        //self.navigationItem.rightBarButtonItems = nil
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            //self.navigationItem.rightBarButtonItem = cancelBarButtonItem
        }
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        self.searchBar.text = ""
        searchText = ""
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("")
        searchText = searchBar.text ?? ""
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func configure(cell: ContentTableViewCell, at indexPath: IndexPath) {
        cell.contentModel = self.listContents[indexPath.row]
        cell.infoButton.isHidden = true
        cell.contentTableViewCellDelegate = self;
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listContents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContentTableViewCell")! as! ContentTableViewCell
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "ContentTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! ContentTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        //let model = self.listContents[indexPath.row]
    }
    
    /*
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
     return 0.01
     }
     
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     return 0.01
     }
     */
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func loadMoreData(_ handler: (() -> Void)?) {
        
        if(self.listContents.count > 1 && !self.refreshControl.isRefreshing){
            
            pageNumber += 1
            ServiceData.sharedInstance.taskStatisticDetails(id: self.id, keyword: searchText, pageIndex: pageNumber).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in result.array! {
                        listIndexPath.append(IndexPath(row: self.listContents.count, section: 0))
                        let model = ContentModel(json: item)
                        self.listContents.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                handler?()
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                handler?()
            })
            
        } else {
            handler?()
            
        }
        
    }
    
    func loadDataChart() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskStatisticDetailPie(id: self.id).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<ChartItemModel>()
                for item in array {
                    let model = ChartItemModel(json: item)
                    arrayModel.append(model)
                }
                self.listChartItems = arrayModel
                
            }
            self.showPieChart()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    func showPieChart() {
        if(pieChartView == nil) {
            pieChartView = PieChartView(frame: self.pieChartViewContain.bounds)
            pieChartView.autoresizingMask = [.flexibleHeight, .flexibleWidth, .flexibleTopMargin, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin];
            self.pieChartViewContain.addSubview(pieChartView)
        }
        pieChartView.clear() 
        tableView.isHidden = true
        pieChartViewContain.isHidden = false
        pieChartView.chartDescription?.font = UIFont.systemFont(ofSize: 14)
        pieChartView.chartDescription?.text = ""//"Biểu đồ thể hiện tiến độ công việc"
        var listChartDataEntry = [ChartDataEntry]()
        var listChartColor = [UIColor]()
        var listValueChartColor = [UIColor]()
        for i in 0..<self.listChartItems.count {
            let dataEntry = PieChartDataEntry(value: Double(CGFloat(self.listChartItems[i].val) / 100.0), label: self.listChartItems[i].title)
            listChartDataEntry.append(dataEntry)
            listChartColor.append(UIColor(self.listChartItems[i].color))
            listValueChartColor.append(UIColor(self.listChartItems[i].textColor))
        }
        let dataSet = PieChartDataSet(values: listChartDataEntry, label: "")
        dataSet.colors = listChartColor
        dataSet.valueColors = listValueChartColor
        dataSet.sliceSpace = 2.0
        
        let data = PieChartData(dataSet: dataSet)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 100.0
        pFormatter.percentSymbol = " %"
        
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        data.setValueFont(UIFont.systemFont(ofSize: 11))
        data.setValueTextColor(listValueChartColor.first ?? UIColor.white)
        pieChartView.data = data
        pieChartView.highlightValue(nil)
        
        
    }
    
    func loadDataBarChart() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        barChartViewContain.isHidden = false
        ServiceData.sharedInstance.taskStatisticDetailBar(id: self.id).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<ChartItemModel>()
                for item in array {
                    let model = ChartItemModel(json: item)
                    arrayModel.append(model)
                }
                self.listBarChartItems = arrayModel
                
            }
            self.showBarChart()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    func showBarChart() {
        
        if(barChartView == nil) {
            barChartView = BarChartView(frame: self.barChartViewContain.bounds)
            barChartView.autoresizingMask = [.flexibleHeight, .flexibleWidth, .flexibleTopMargin, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin];
            self.barChartViewContain.addSubview(barChartView)
        }
        barChartView.clear()
        tableView.isHidden = true
        pieChartViewContain.isHidden = true
        barChartView.isHidden = false
        barChartView.chartDescription?.font = UIFont.systemFont(ofSize: 14)
        barChartView.chartDescription?.text = ""//"Biểu đồ thể hiện tiến độ công việc"
        var arrayBarChartDataEntry = Array<BarChartDataEntry>()
        var listChartColor = [UIColor]()
        var listValueChartColor = [UIColor]()
        var listValueLabel = [String]()
        
        for i in 0..<self.listBarChartItems.count {
            let item = listBarChartItems[i]
            
            let entry = BarChartDataEntry.init(x: Double(i), y: item.val, data: item.title as AnyObject)
            arrayBarChartDataEntry.append(entry)
            listChartColor.append(UIColor(item.color))
            listValueChartColor.append(UIColor(item.textColor))
            listValueLabel.append(item.title)
        }
        
        let dataSet = BarChartDataSet(values: arrayBarChartDataEntry, label: "")
        dataSet.colors = listChartColor
        dataSet.valueColors = listValueChartColor
        //dataSet.stackLabels = listValueLabel
        dataSet.valueFont = UIFont.systemFont(ofSize: 12)
        
        let chartData = BarChartData(dataSet: dataSet)
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            chartData.setValueFont(UIFont.systemFont(ofSize: 10))
        } else {
            chartData.setValueFont(UIFont.systemFont(ofSize: 12))
        }
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .none
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1.0
        //pFormatter.percentSymbol = " %"
        
        let xAxis = barChartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.labelCount = listValueLabel.count
        xAxis.valueFormatter = IndexAxisValueFormatter(values: listValueLabel)
        
        chartData.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        /*
        //barChartView.xAxis.valueFormatter = formatter
        //barChartView.xAxis.granularity = 1
        barChartView.xAxis.labelCount = labels.count
        barChartView.xAxis.labelPosition = .bottom
        barChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: labels)
        barChartView.xAxis.forceLabelsEnabled = true
        barChartView.xAxis.granularity = 0.1
        barChartView.xAxis.granularityEnabled = true
        */
        barChartView.data = chartData
    }
    
    //MARK - ContentTableViewCellDelegate
    
    func tapAvatarContentTableViewCell(cell: ContentTableViewCell) {
        let photo = SKPhoto.photoWithImageURL(cell.contentModel?.imageUrl ?? "")
        photo.caption = ""
        let browser = SKPhotoBrowser(photos: [photo], initialPageIndex: 0)
        present(browser, animated: true, completion: {})
    }
    
    func tapInfoButtonContentTableViewCell(cell: ContentTableViewCell) {
        
    }
    
}
