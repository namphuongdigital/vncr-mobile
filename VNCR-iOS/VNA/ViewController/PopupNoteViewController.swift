//
//  PopupNoteViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/21/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import STPopup
import IQKeyboardManagerSwift

protocol PopupNoteViewControllerDelagate: NSObjectProtocol {
    func popupNoteViewControllerSendPress(text: String, object: NSObject?)
    
}

class PopupNoteViewController: BaseViewController {
    
    weak var object: NSObject?
    
    var textContent: String = ""
    
    var textButtonDone: String = "Send".localizedString()
    
    var textContentHint: String = "Enter text".localizedString()
    
    var textTitlePopup: String = "Note".localizedString()

    weak var popupNoteViewControllerDelagate: PopupNoteViewControllerDelagate?
    
    @IBOutlet weak var buttonSendNote: UIButton!
    
    @IBOutlet weak var textViewNote: IQTextView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.contentSizeInPopup = CGSize(width: 320, height: 240)
        self.landscapeContentSizeInPopup = CGSize(width: 320, height: 240)
        
        self.buttonSendNote.backgroundColor = UIColor("#166880")
        self.buttonSendNote.layer.cornerRadius = 2.5
        self.buttonSendNote.setTitleColor(UIColor.white, for: .normal)
        self.buttonSendNote.addTarget(self, action: #selector(buttonSendNotePress), for: .touchUpInside)
        
        self.textViewNote.layer.borderColor = UIColor.lightGray.cgColor
        self.textViewNote.layer.borderWidth = 0.4
        self.textViewNote.layer.cornerRadius = 2.5
        self.textViewNote.text = textContent
        self.textViewNote.placeholder = textContentHint

        self.navigationItem.title = textTitlePopup
        
        self.buttonSendNote.setTitle(textButtonDone, for: .normal)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func buttonSendNotePress() {
        self.popupNoteViewControllerDelagate?.popupNoteViewControllerSendPress(text: self.textViewNote.text, object: self.object)
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
