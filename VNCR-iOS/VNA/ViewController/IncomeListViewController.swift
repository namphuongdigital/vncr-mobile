//
//  IncomeListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/25/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class IncomeListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var pageIndexNew: Int = 0
    
    var listNewMessageModel = [MessageModel]()
    
    var textSearch: String = ""
    
    var refreshControl: UIRefreshControl!
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView?.addSubview(refreshControl)
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Income".localizedString()
        
        tableView.keyboardDismissMode = .onDrag
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        //self.textTitle = "FORM LIST".localizedString()
        
        self.tableView.alwaysBounceVertical = true
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        //self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            self?.loadMoreDataNew() {
                self?.tableView!.finishInfiniteScroll()
            }
            
        }
        
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
    @objc func refreshData() {
        getNewData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
    func getNewData() {
        pageIndexNew = 1
        ServiceData.sharedInstance.taskIncomeList(keyword: "", pageIndex: pageIndexNew).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<MessageModel>()
                for item in array {
                    let model = MessageModel(json: item)
                    ApplicationData.sharedInstance.updateMessageModel(message: model)
                    arrayModel.append(model)
                }
                self.listNewMessageModel = arrayModel
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    func loadMoreDataNew(_ handler: (() -> Void)?) {
        
        if(self.listNewMessageModel.count > 1 && !self.refreshControl.isRefreshing){
            pageIndexNew += 1
            ServiceData.sharedInstance.taskIncomeList(keyword: "", pageIndex: pageIndexNew).continueOnSuccessWith(continuation: { task in
                let result = task as! JSON
                if let array = result.array, array.count > 0 {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in array {
                        listIndexPath.append(IndexPath(row: self.listNewMessageModel.count, section: 0))
                        let model = MessageModel(json: item)
                        ApplicationData.sharedInstance.updateMessageModel(message: model)
                        self.listNewMessageModel.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                handler?()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                handler?()
            })
            
            
        } else {
            handler?()
        }
    }
    
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listNewMessageModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageItemTableViewCell")! as! MessageItemTableViewCell
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "MessageItemTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! MessageItemTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model: MessageModel = self.listNewMessageModel[indexPath.row]
        
        if(model.isHtml) {
            self.pushWebViewIncomeViewController(id: model.id, title: model.sender)
        } else {
            self.pushIncomeDetailViewController(messageModel: model)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func configure(cell: MessageItemTableViewCell, at indexPath: IndexPath) {
        
        cell.labelContent.numberOfLines = 4
        cell.loadContent(messageModel: self.listNewMessageModel[indexPath.row])
        
    }

}
