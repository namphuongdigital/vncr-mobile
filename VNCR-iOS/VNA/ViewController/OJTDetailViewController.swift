//
//  OJTDetailViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import STPopup
//import FTPopOverMenu
import DXPopover

class OJTDetailViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, OJTItemTableViewCellDelegate, PopupNoteViewControllerDelagate, EditContentViewControllerDelegate {
    
    var cid: String = ""
    
    weak var scheduleFlyModel: ScheduleFlightModel?
    
    var refreshControl: UIRefreshControl!
    
    @IBOutlet weak var tableView: UITableView!
    
    var ojtModel: OJTModel!
    
    var sendBarButtonItem: UIBarButtonItem!
    
    var saveOfflineBarButtonItem: UIBarButtonItem!
    
    var isEditComment: Bool = false
    
    var isModelSaved: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let button = UIButton(type: .custom)
        button.setTitle("", for: .normal)
        //button.setTitleColor(UIColor("#cc9e73"), for: .normal)
        //button.titleLabel?.textColor = UIColor("#cc9e73")
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.addTarget(self, action: #selector(self.buttonSaveOffline), for: .touchUpInside)
        button.setImage(UIImage.init(named: "save-offline.png")?.withRenderingMode(.alwaysTemplate), for: .normal)
        button.tintColor = UIColor.white
        button.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        //button.backgroundColor = UIColor.black
        saveOfflineBarButtonItem = UIBarButtonItem(customView: button)
        
        sendBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.saveBarButtonItemPressed(_:)))
        sendBarButtonItem.setFAIcon(icon: .FACheckCircle, iconSize: 20)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        var nib = UINib(nibName: "OJTItemTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "OJTItemTableViewCell")
        nib = UINib(nibName: "DynamicHeightCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "DynamicHeightCell")
        nib = UINib(nibName: "ConfirmTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ConfirmTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        //tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        
        self.textTitle = "OJT DETAIL".localizedString()
        
        self.tableView.alwaysBounceVertical = true
        
        if(isModelSaved) {
            if(self.ojtModel.isReadOnly == false) {
                self.navigationItem.rightBarButtonItems = [self.sendBarButtonItem]
            }
            self.tableView.reloadData()
        } else {
            self.tableView.reloadData()
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            getNewData()
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    
    @objc func buttonSaveOffline(sender: UIButton) {
        if ApplicationData.sharedInstance.checkOJTModel(id: self.ojtModel!.id) {
            confirmSaveOffline()
            return
        }
        ApplicationData.sharedInstance.updateOJTModel(ojt: self.ojtModel)
        self.showAlert("Save successed".localizedString(), stringContent: "")
        
    }
    
    func confirmSaveOffline() {
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
            guard let `self` = self else {
                return
            }
            ApplicationData.sharedInstance.deleteOJTModel(id: self.ojtModel.id)
            ApplicationData.sharedInstance.updateOJTModel(ojt: self.ojtModel)
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("This OJT is exist in list ofline, Are you replace this OJT saved before ?".localizedString(), message: "", actions: [yesAction,cancelAction])
    }
    
    
    @objc func saveBarButtonItemPressed(_ sender: UIBarButtonItem) {
        updateOJT()
    }
    
    func updateOJT() {
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskOJTUpdate(ojtModel: self.ojtModel!).continueOnSuccessWith(continuation: { task in
            
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            self.tableView.reloadData()
            if let responseServiceModel = task as? ResponseServiceModel {
                if let message = responseServiceModel.message {
                    if(message.count > 0) {
                        self.showAlert("Update".localizedString(), stringContent: message)
                    }
                }
                
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    @objc func refreshData() {
        
        getNewData()
    }
    
    func getNewData() {
        if let ojtModel = self.ojtModel {
            ServiceData.sharedInstance.taskOJTDetail(id: ojtModel.id, templateId: ojtModel.crOJTLessonID, flightId: scheduleFlyModel!.flightID, cid: self.cid).continueOnSuccessWith(continuation: { task in
                #if DEBUG
                print("\(task as? JSON) \(#function)")
                #endif
                let result = task as! JSON
                let ojtModel = OJTModel(json: result)
                self.ojtModel = ojtModel
                if(self.ojtModel.isReadOnly == false) {
                    self.navigationItem.rightBarButtonItems = [self.sendBarButtonItem, self.saveOfflineBarButtonItem]
                }
                self.ojtModel!.flightID = self.scheduleFlyModel!.flightID
                self.textTitle = self.ojtModel!.crOJTLesson
                
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                self.tableView.reloadData()
                
            }).continueOnErrorWith(continuation: { error in
                
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                
            })
        }
        
    }
    
    
    func configure(cell: OJTItemTableViewCell, at indexPath: IndexPath) {
        if let ojtItems = self.ojtModel?.ojtItems {
            cell.ojtItemModel = ojtItems[indexPath.row]
            if(self.ojtModel.isReadOnly == true) {
                cell.ojtItemTableViewCellDelegate = nil
            } else {
                cell.ojtItemTableViewCellDelegate = self
            }
        }
        
        
    }
    
    func configure(dynamicHeightCell: DynamicHeightCell, at indexPath: IndexPath) {
        if(indexPath.row == 0) {
            dynamicHeightCell.title = "Comment".localizedString()
            dynamicHeightCell.body = self.ojtModel.comment
            dynamicHeightCell.clearButton.addTarget(self, action: #selector(self.clearButtonCommentPress), for: .touchUpInside)
        } else {
            dynamicHeightCell.title = "Suggestion".localizedString()
            dynamicHeightCell.body = self.ojtModel.suggestion
            dynamicHeightCell.clearButton.addTarget(self, action: #selector(self.clearButtonSuggestionPress), for: .touchUpInside)
        }
        if(self.ojtModel.isReadOnly == true) {
            dynamicHeightCell.clearButton.isHidden = true
        }
        
    }
    
    func configure(confirmTableViewCell: ConfirmTableViewCell, at indexPath: IndexPath) {
        
        confirmTableViewCell.textNameLabel.text = self.ojtModel.signature
        confirmTableViewCell.buttonConfirm.addTarget(self, action: #selector(self.buttonConfirmPress), for: .touchUpInside)
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = self.ojtModel {
            return 3
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) {
            return self.ojtModel.ojtItems.count
        } else if(section == 1) {
            return 2
        } else {
            if ojtModel.IsShowConfirm {
                return 1
            }
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OJTItemTableViewCell")! as! OJTItemTableViewCell
            self.configure(cell: cell, at: indexPath)
            return cell
            
        } else if (indexPath.section == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DynamicHeightCell")! as! DynamicHeightCell
            self.configure(dynamicHeightCell: cell, at: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ConfirmTableViewCell")! as! ConfirmTableViewCell
            self.configure(confirmTableViewCell: cell, at: indexPath)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0) {
            return tableView.fd_heightForCell(withIdentifier: "OJTItemTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
                let cell = cell as! OJTItemTableViewCell
                self?.configure(cell: cell, at: indexPath)
            })
        } else if (indexPath.section == 1) {
            return tableView.fd_heightForCell(withIdentifier: "DynamicHeightCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
                let cell = cell as! DynamicHeightCell
                self?.configure(dynamicHeightCell: cell, at: indexPath)
            })
        } else {
            return tableView.fd_heightForCell(withIdentifier: "ConfirmTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
                let cell = cell as! ConfirmTableViewCell
                self?.configure(confirmTableViewCell: cell, at: indexPath)
            })
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.section == 1) {
            if(indexPath.row == 0) {
                selectCommentPress()
            } else {
                selectSuggestionPress()
            }
        }
        
    }
    
    
    //MARK - OJTItemTableViewCellDelegate
    
    func scoreButtonPressOJTItemTableViewCell(cell: OJTItemTableViewCell, ojtItemModel: OJTItemModel) {
        showPopupSelected(cell: cell, ojtItemModel: ojtItemModel)
    }
    func commentButtonPressOJTItemTableViewCell(cell: OJTItemTableViewCell, ojtItemModel: OJTItemModel) {
        showNoteBarButtonItemPressed(ojtItemModel: ojtItemModel)
    }
    
    
    
    //
    
    func showNoteBarButtonItemPressed(ojtItemModel: OJTItemModel) {
        let popupNoteViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopupNoteViewController") as! PopupNoteViewController
        popupNoteViewController.popupNoteViewControllerDelagate = self
        popupNoteViewController.object = ojtItemModel
        popupNoteViewController.textContent = ojtItemModel.comment
        popupNoteViewController.textButtonDone = "Done".localizedString()
        let popupController = STPopupController.init(rootViewController: popupNoteViewController)
        popupController.containerView.layer.cornerRadius = 3
        popupController.transitionStyle = .fade
        popupController.navigationBar.tintColor = UIColor.black
        popupController.present(in: self)
    }
    
    //MARK - PopupNoteViewControllerDelagate
    func popupNoteViewControllerSendPress(text: String, object: NSObject?) {
        let text = text
        guard let ojtItemModel = object as? OJTItemModel else {
            return
        }
        ApplicationData.sharedInstance.getBlockRealmWrite {
            ojtItemModel.comment = text
        }
        self.updateThisOJTModelToDB()
        for index in 0..<self.ojtModel!.ojtItems.count {
            if(self.ojtModel!.ojtItems[index].id == ojtItemModel.id) {
                self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                break
            }
        }
    }
    
    func showPopupSelected(cell: OJTItemTableViewCell, ojtItemModel: OJTItemModel) {
        var arrayString = ojtItemModel.scoreType
        arrayString.insert("✖︎", at: 0)
        //FTPopOverMenu.show(forSender: cell.buttonScore, withMenuArray: arrayString, doneBlock: {[weak self] (index) in
        FTPopOverMenu.show(forSender: cell.buttonScore, withMenu: arrayString, menuType: FTPopOverMenuType.square, doneBlock: {[weak self] (index) in
            
            guard let `self` = self else {
                return
            }
            ApplicationData.sharedInstance.getBlockRealmWrite {
                if(index == 0) {
                    ojtItemModel.score = ""
                } else {
                    ojtItemModel.score = arrayString[index]
                }
                
            }
            self.updateThisOJTModelToDB()
            for index in 0..<self.ojtModel!.ojtItems.count {
                if(self.ojtModel!.ojtItems[index].id == ojtItemModel.id) {
                    self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                    break
                }
            }
            
            }, dismiss: {
                print("")
        })
        
        
    }
    
    func selectSuggestionPress() {
        if(self.ojtModel.isReadOnly == true) {
            return
        }
        self.isEditComment = false
        self.pushEditContentViewController(title: "Suggestion".localizedString(), content: self.ojtModel?.suggestion ?? "", delegate: self)
    }
    
    @objc func clearButtonCommentPress(sender: UIButton) {
        ApplicationData.sharedInstance.getBlockRealmWrite {
            self.ojtModel.comment = ""
        }
        self.updateThisOJTModelToDB()
        self.tableView.reloadRows(at: [IndexPath.init(item: 0, section: 1)], with: .automatic)
        
    }
    
    @objc func clearButtonSuggestionPress(sender: UIButton) {
        ApplicationData.sharedInstance.getBlockRealmWrite {
            self.ojtModel.suggestion = ""
        }
        self.updateThisOJTModelToDB()
        self.tableView.reloadRows(at: [IndexPath.init(item: 1, section: 1)], with: .automatic)
    }
    
    @objc func buttonConfirmPress(sender: UIButton) {
        self.showMBProgressHUD("Loading".localizedString(), animated: true)
        ServiceData.sharedInstance.taskOJTSignature(id: self.ojtModel.id, comment: "").continueOnSuccessWith(continuation: { task in
            
            self.hideMBProgressHUD(true)
            if let responseServiceModel = task as? ResponseServiceModel {
                ApplicationData.sharedInstance.getBlockRealmWrite {
                    self.ojtModel.signature = responseServiceModel.message ?? ""
                }
                self.updateThisOJTModelToDB()
                self.tableView.reloadRows(at: [IndexPath.init(item: 0, section: 2)], with: UITableView.RowAnimation.automatic)
                
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    func selectCommentPress() {
        if(self.ojtModel.isReadOnly == true) {
            return
        }
        self.isEditComment = true
        self.pushEditContentViewController(title: "Comment".localizedString(), content: self.ojtModel.comment, delegate: self)
    }
    
    //MARK - EditContentViewControllerDelegate
    func editContentViewControllerSave(viewController: UIViewController, content: String) {
        if(self.isEditComment) {
            ApplicationData.sharedInstance.getBlockRealmWrite {
                self.ojtModel.comment = content
            }
            self.updateThisOJTModelToDB()
            self.tableView.reloadRows(at: [IndexPath.init(item: 0, section: 1)], with: .automatic)
            
        } else {
            ApplicationData.sharedInstance.getBlockRealmWrite {
                self.ojtModel.suggestion = content
            }
            self.updateThisOJTModelToDB()
            self.tableView.reloadRows(at: [IndexPath.init(item: 1, section: 1)], with: .automatic)
        }
        viewController.navigationController?.popViewController(animated: true)
    }
    
    func updateThisOJTModelToDB() {
        if(self.isModelSaved) {
            ApplicationData.sharedInstance.updateOJTModel(ojt: self.ojtModel)
        }
    }

}
