//
//  GuestTabbarController.swift
//  VNA
//
//  Created by Pham Dai on 10/11/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class GuestTabbarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //UIApplication.shared.statusBarStyle = .lightContent
        if #available(iOS 13.0, *) {
            let appearanceTabbar = UITabBarAppearance()
            appearanceTabbar.selectionIndicatorTintColor = UIColor("#166880")
            appearanceTabbar.backgroundColor = .white
            self.tabBar.standardAppearance = appearanceTabbar
            if #available(iOS 15.0, *) {
                self.tabBar.scrollEdgeAppearance = appearanceTabbar
            }
        } else {
            UITabBar.appearance().tintColor = UIColor("#166880")
        }
        
        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let flightTabbarItem = UITabBarItem(title: "Flight".localizedString(), image: UIImage(named: "flight-icon"), tag: 0)
        let newsTabbarItem = UITabBarItem(title: "News".localizedString(), image: UIImage(named: "ic_news"), tag: 1)
        
        // setup SchduleFlyViewController trên ipad và iphone
        let scheduleFlyViewController = storyboard.instantiateViewController(withIdentifier: "ScheduleFlyViewController") as! ScheduleFlyViewController
        let newsViewController = storyboard.instantiateViewController(withIdentifier: "NewListViewController") as! NewListViewController
        
        scheduleFlyViewController.tabBarItem = flightTabbarItem
        newsViewController.tabBarItem = newsTabbarItem
        
        var vcs = [UIViewController]()
        
        // embed splitviewcontroller tren ipad
        if UIDevice.current.userInterfaceIdiom == .pad {
            vcs.insert(BaseSplitController(root: UINavigationController(rootViewController: scheduleFlyViewController), detail: nil), at: 0)
        } else {
            vcs.insert(UINavigationController(rootViewController: scheduleFlyViewController), at: 0)
        }
        
        // setup SchduleFlyViewController trên ipad và iphone
        vcs.append((UINavigationController(rootViewController: newsViewController)))
        
        
        viewControllers = vcs
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Always adopt a dark interface style.
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.setNeedsStatusBarAppearanceUpdate()
    }

}
