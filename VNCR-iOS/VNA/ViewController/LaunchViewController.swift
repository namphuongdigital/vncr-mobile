//
//  LaunchViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/21/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class LaunchViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            //imageView.image = UIImage.init(named: "LaunchImage-iPhone7.png")
        } else {
            //imageView.image = UIImage.init(named: "LaunchImage-iPadAir.png")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
