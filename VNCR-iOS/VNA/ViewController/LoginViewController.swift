//
//  LoginViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/28/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import BoltsSwift
import SwiftyJSON

class LoginViewController: FormViewController {
    
    var tryAgainView: TryAgainView!
    
    var imageBackground : UIImageView!
    
    var imageViewFooter: UIImageView!
    
    var usernameRow: CustomRowFormer<TextTableViewCell>!
    
    var passwordRow: CustomRowFormer<TextTableViewCell>!
    
    var buttonLoginRow: CustomRowFormer<ButtonActionTableViewCell>!
    
    @IBOutlet weak var buttonCloseForm: UIButton!
    
    var taskCompletionSource: TaskCompletionSource<AnyObject>?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.tryAgainView = TryAgainView.loadView() as! TryAgainView
        tryAgainView.isHidden = true
        tryAgainView.buttonTryAgain.addTarget(self, action: #selector(buttonTryAgain(sender:)), for: .touchUpInside)
        self.view.addSubview(tryAgainView)
        
        buttonCloseForm.addTarget(self, action: #selector(buttonCloseFormPress(sender:)), for: .touchUpInside)
        buttonCloseForm.setFAIcon(icon: .FAClose, forState: .normal)
        buttonCloseForm.setFATitleColor(color: .white)
        buttonCloseForm.layer.borderWidth = 0.7
        buttonCloseForm.layer.borderColor = UIColor.white.cgColor
        buttonCloseForm.layer.cornerRadius = buttonCloseForm.frame.size.width / 2.0
        
        imageBackground = UIImageView(frame: self.view.bounds)
        //imageBackground.image = UIImage(named: "logo-VNA.png")
        imageBackground.contentMode = .scaleAspectFill
        
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        self.view.insertSubview(imageBackground, belowSubview: tableView)
        self.view.backgroundColor = UIColor("#006885")
        
        let logoRow = CustomRowFormer<LogoLoginTableViewCell>(instantiateType: .Nib(nibName: "LogoLoginTableViewCell")) {
            print($0.textLabel?.text ?? "")
            
            }.configure {
                $0.rowHeight = 150
        }
        
        usernameRow = CustomRowFormer<TextTableViewCell>(instantiateType: .Nib(nibName: "TextTableViewCell")) {
            print($0.textLabel?.text ?? "")
            $0.textFieldInput.keyboardType = .emailAddress
            //$0.textFieldInput.addRegx("[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", withMsg: "Tên đăng nhập không hợp lệ")
            //$0.textFieldInput.addRegx("^.{9,11}$", withMsg: "Số điện thoại phải từ 9 - 11 số")
            $0.textFieldInput.placeholder = "Username".localizedString()
            $0.labelIconInput.setFAIcon(icon: FAType.FAUser, iconSize: 18)
            if(UserDefaults.standard.value(forKey: "username") != nil) {
                $0.textFieldInput.text = UserDefaults.standard.value(forKey: "username") as? String
            }
            }.configure {
                $0.rowHeight = 60
        }
        
        passwordRow = CustomRowFormer<TextTableViewCell>(instantiateType: .Nib(nibName: "TextTableViewCell")) {
            print($0.textLabel?.text ?? "")
            $0.textFieldInput.placeholder = "Password".localizedString()
            $0.textFieldInput.isSecureTextEntry = true
            $0.labelIconInput.setFAIcon(icon: FAType.FALock, iconSize: 18)
            }.configure {
                $0.rowHeight = 60
        }
        
        buttonLoginRow = CustomRowFormer<ButtonActionTableViewCell>(instantiateType: .Nib(nibName: "ButtonActionTableViewCell")) {[weak self] in
            print($0.textLabel?.text ?? "")
            $0.buttonAction.backgroundColor = UIColor("#dba510")
            $0.buttonAction.setTitle("Login".localizedString(), for: UIControl.State())
            $0.buttonAction.addTarget(self, action: #selector(self?.buttonLoginPress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        let createHeader: ((String, _ height: CGFloat) -> ViewFormer) = { text, height in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor.clear
                $0.titleLabel.font = UIFont.systemFont(ofSize: 14)
                }.configure {
                    $0.viewHeight = height
                    $0.text = text
                    
            }
        }
        
        let customRowFormerSection = SectionFormer(rowFormer: logoRow, usernameRow, passwordRow, buttonLoginRow).set(headerViewFormer: createHeader("", 40))
        
        _ = CustomRowFormer<TextTableViewCell>(instantiateType: .Nib(nibName: "TextTableViewCell")) {
            print($0.textLabel?.text ?? "")
            
            }.configure {
                $0.rowHeight = 0
        }
        
        
        former.append(sectionFormer: customRowFormerSection)
        if(taskCompletionSource == nil) {
            getUserCheckaccesstoken()
            self.buttonCloseForm.isHidden = true
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func buttonLoginPress(sender: UIButton) {
        getLogin()
    }
    
    func getLogin() {
        let usernameTableViewCell = usernameRow.cellInstance as! TextTableViewCell
        if(usernameTableViewCell.textFieldInput.validate() == false){
            usernameTableViewCell.updateValidationState(result: .invalid)
            self.showAlert("Alert".localizedString(), stringContent: "Username invalid".localizedString())
            return
        }
        usernameTableViewCell.updateValidationState(result: .valid)
        
        let passwordTableViewCell = passwordRow.cellInstance as! TextTableViewCell
        if(passwordTableViewCell.textFieldInput.hasText == false){
            passwordTableViewCell.updateValidationState(result: .invalid)
            self.showAlert("Alert".localizedString(), stringContent: "Password not empty".localizedString())
            return
        }
        passwordTableViewCell.updateValidationState(result: .valid)
        
        self.showMBProgressHUD("Login...".localizedString(), animated: true)
  
        ServiceData.sharedInstance.taskGetUserLogin(username: usernameTableViewCell.textFieldInput.text!, password: passwordTableViewCell.textFieldInput.text!).continueOnSuccessWith(continuation: { task in
            UserDefaults.standard.setValue(usernameTableViewCell.textFieldInput.text!, forKey: "username")
            UserDefaults.standard.setValue(ServiceData.sharedInstance.userModel?.token ?? "", forKey: "token")
            ServiceData.sharedInstance.taskFlightPilotGetList(position: .main).continueWith(continuation: { task in
                print("")
                self.hideMBProgressHUD(true)
                if let taskCompletionSource = self.taskCompletionSource {
                    taskCompletionSource.trySet(result: true as AnyObject)
                } else {
                    self.gotoMainApp()
                }
                
            })
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        
        
    }
    
    
    @objc func buttonTryAgain(sender: UIButton) {
        self.tryAgainView.isHidden = true
        self.getUserCheckaccesstoken()
    }
    
    @objc func buttonCloseFormPress(sender: UIButton) {
        let error = NSError(domain: "serviceError", code: 10, userInfo: [NSLocalizedDescriptionKey: "User not authentication"])
        self.taskCompletionSource?.trySet(error: error)
        self.dismiss(animated: true, completion: nil)
    }
    
    func getUserCheckaccesstoken() {
        self.tableView.isHidden = true
        self.showMBProgressHUD("Connecting...".localizedString(), animated: true)
        if let userName = UserDefaults.standard.value(forKey: "username") as? String, let token =  UserDefaults.standard.value(forKey: "token") as? String {
         
            if(userName.count > 0 && token.count > 0) {
                ServiceData.sharedInstance.taskUserCheckaccesstoken(userName: userName, token: token).continueOnSuccessWith(continuation: { task in
                    UserDefaults.standard.setValue(userName, forKey: "username")
                    let userModel = UserModel(json: task as! JSON)
                    ApplicationData.sharedInstance.updateUserLogin(userModel: userModel)
                    ServiceData.sharedInstance.userModel = userModel
                    UserDefaults.standard.setValue(ServiceData.sharedInstance.userModel?.token ?? "", forKey: "token")
                    ServiceData.sharedInstance.taskFlightPilotGetList(position: .main).continueWith(continuation: { task in
                        print("")
                        self.hideMBProgressHUD(true)
                        self.gotoMainApp()
                    })
                    
                    
                    
                }).continueOnErrorWith(continuation: { error in
                    if((error as NSError).code == -1009) {
                        if let userModel = ApplicationData.sharedInstance.getLoginUserModel() {
                            ServiceData.sharedInstance.userModel = userModel
                            /*
                            ApplicationData.sharedInstance.getBlockRealmWrite(block: {
                                ServiceData.sharedInstance.userModel?.token = "0"
                            })
                            */
                        } else {
                            let userModel = UserModel()
                            userModel.token = "0"
                            ServiceData.sharedInstance.userModel = userModel
                            
                        }
                        //self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                        self.tryAgainView.labelTitleTryAgain.text = (error as NSError).localizedDescription
                        //self.tryAgainView.isHidden = false
                        self.gotoMainApp()
                    } else {
                        self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                        UserDefaults.standard.setValue("", forKey: "token")
                        self.tableView.isHidden = false
                    }
                    self.hideMBProgressHUD(true)
                    
                })
                return
            }
        }
        self.hideMBProgressHUD(true)
        self.tableView.isHidden = false

    }
}
