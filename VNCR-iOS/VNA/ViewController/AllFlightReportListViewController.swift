//
//  AllFlightReportListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 3/13/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON


class AllFlightReportListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, FlightReportFeedTableViewCellDelegate, ListImageViewDelegate, ExpandableLabelDelegate {
    
    var fromDateSeleted: Date!
    
    var rightBarButtonItem: UIBarButtonItem!
    
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero) //CustomSearchBarView = CustomSearchBarView(frame: CGRect.zero)
    
    var listReportModel = Array<ReportModel>()
    
    var refreshControl: UIRefreshControl!
    
    var segmentedControl: UISegmentedControl = UISegmentedControl(frame: CGRect.zero)
    
    @IBOutlet weak var tableView: UITableView!
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var searchText = ""
    
    var pageNumber = 1
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView?.addSubview(refreshControl)
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.searchBar.sizeToFit()
        //self.navigationItem.setHidesBackButton(true, animated:true)
        //self.customBackButton(imageName: "back-icon.png", renderingMode: UIImageRenderingMode.alwaysTemplate)
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        fromDateSeleted = Date()
        
        var button = UIButton(type: .custom)
        button.setTitle("", for: .normal)
        button.setTitleColor(UIColor("#cc9e73"), for: .normal)
        button.titleLabel?.textColor = UIColor("#cc9e73")
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.titleLabel?.numberOfLines = 2
        button.titleLabel?.textAlignment = .center
        button.titleLabel?.lineBreakMode = .byWordWrapping
        button.addTarget(self, action: #selector(AllFlightReportListViewController.buttonSelectDateStartPress), for: .touchUpInside)
        //buttonSelectDateStartPress
        button.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        //button.backgroundColor = UIColor.black
        rightBarButtonItem = UIBarButtonItem(customView: button)
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        showStartDateInfo(date: fromDateSeleted)
        
        self.setupSearchBar()
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        let nib = UINib(nibName: "CommadDeviceTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "CommadDeviceTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 135
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.keyboardDismissMode = .onDrag
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tableView.alwaysBounceVertical = true
        
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            if(self?.isSearch == true) {
                self?.tableView!.finishInfiniteScroll()
                return
            }
            self?.loadMoreData() {
                self?.tableView!.finishInfiniteScroll()
                self?.refreshControl?.endRefreshing()
            }
            
        }
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        getNewData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadMoreData(_ handler: (() -> Void)?) {
        
        if(self.listReportModel.count > 1 && !self.refreshControl.isRefreshing){
            
            pageNumber += 1
            ServiceData.sharedInstance.taskFlightFinalReportGetAll(date: fromDateSeleted, keyword: searchText, pageIndex: pageNumber).continueOnSuccessWith(continuation: { task in
                
                if let result = task as? JSON {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in result.array! {
                        listIndexPath.append(IndexPath(row: self.listReportModel.count, section: 0))
                        let model = ReportModel(json: item)
                        self.listReportModel.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                handler?()
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                handler?()
            })
        } else {
            handler?()
            
        }
        
    }
    
    @objc func refreshData() {
        pageNumber = 1
        getNewData()
    }
    
    func getNewData() {
        //self.textTitle = "DEVICES".localizedString()
        
        ServiceData.sharedInstance.taskFlightFinalReportGetAll(date: fromDateSeleted, keyword: searchText, pageIndex: pageNumber).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<ReportModel>()
                for item in array {
                    let model = ReportModel(json: item)
                    arrayModel.append(model)
                }
                self.listReportModel = arrayModel
                
            }
            self.tableView.reloadData()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    @objc func buttonSelectDateStartPress(sender: UIButton) {
        //let buttonItemView = sender.value(forKey: "view") as! UIView
        self.searchBar.resignFirstResponder()
        CalendarPickerPopover.appearFrom(
            originView: sender,
            baseViewController: self,
            title: "Date Picker".localizedString(),
            dateMode: .date,
            initialDate: fromDateSeleted,
            doneAction: {[weak self] selectedDate in
                print("selectedDate \(selectedDate)")
                self?.showStartDateInfo(date: selectedDate)
            },
            cancelAction: {print("cancel")}
        )
    }
    
    func showStartDateInfo(date: Date) {
        fromDateSeleted = date
        if let button = rightBarButtonItem.customView as? UIButton {
            button.setTitle(fromDateSeleted.dateTimeToddMMM, for: .normal)
           
        }
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        pageNumber = 1
        getNewData()
    }
    
    func setupSearchBar() {
        //self.searchBar.searchBarOriginX = 0
        //        self.searchBar.tintColor = .white
        //        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.placeholder = "Search".localizedString()
        searchBar.delegate = self
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
    }
    
    //MARK - UISearchBarDelegate
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
        }
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if(self.searchBar.text == "" || self.searchBar.text == nil) {
            searchText = ""
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("")
        searchText = searchBar.text ?? ""
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        pageNumber = 1
        self.getNewData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func configure(cell: CommadDeviceTableViewCell, at indexPath: IndexPath) {
        //cell.commandModel = self.listCommands[indexPath.row]
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listReportModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FlightReportFeedTableViewCell")! as! FlightReportFeedTableViewCell
        cell.loadContent(reportModel: self.listReportModel[indexPath.row])
        cell.delegate = self
        cell.listImageView.delegate = self
        cell.labelContent.delegate = self
        cell.buttonAction.isHidden = true
        
        cell.layoutIfNeeded()
        
        cell.labelContent.shouldCollapse = true
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            cell.labelContent.numberOfLines = 8
        } else {
            cell.labelContent.numberOfLines = 4
        }
        cell.labelContent.collapsed = self.listReportModel[indexPath.row].isCollapsed
        
        //cell.labelContent.text = self.listReportModel[indexPath.row].content
        cell.setTextForContent(content: self.listReportModel[indexPath.row].content, comment: self.listReportModel[indexPath.row].comment)

        cell.labelContent.setLessLinkWith(lessLink: "Less".localizedString(), attributes: [NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: cell.labelContent.font.pointSize), NSAttributedString.Key.foregroundColor: UIColor.blue], position: NSTextAlignment.right)
        cell.labelContent.collapsedAttributedLink = NSAttributedString(string: "Read more".localizedString(), attributes: [NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: cell.labelContent.font.pointSize), NSAttributedString.Key.foregroundColor: UIColor.blue])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        //let model = self.listReportModel[indexPath.row]
        //print(model.attachments)
       //self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        
    }

    // MARK: - ListImageViewDelegate
    func didSelectCellPress(viewController: UIViewController) {
        present(viewController, animated: true, completion: {})
    }
    
    //MARK - FlightReportFeedTableViewCellDelegate
    func buttonMoreCellPress(reportModel: ReportModel) {
        showActionReport(reportModel)
    }
    
    func showActionReport(_ reportModel: ReportModel) {
        var arrayAction = Array<UIAlertAction>()
        if(reportModel.isCanDelete) {
            let action1 = UIAlertAction(title: "Delete".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
                self?.confirmDeleteReport(reportModel)
                
            })
            arrayAction.append(action1)
        }
        
        if(reportModel.isCanUpdate) {
            let action2 = UIAlertAction(title: "Edit".localizedString(), style: UIAlertAction.Style.default, handler: {[weak self] (UIAlertAction) -> Void in
                self?.editReport(reportModel)
                
            })
            arrayAction.append(action2)
        }
        
        if(arrayAction.count > 0) {
            let cancelAction = UIAlertAction(title: "Cancel".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
                print("Cancel pressed")
            })
            
            arrayAction.append(cancelAction)
            
            self.showActionSheetWithAction(title, message: nil, actions: arrayAction)
        }
        
        
    }
    
    func editReport(_ report: ReportModel) {

    }
    
    func confirmDeleteReport(_ report: ReportModel) {
        
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
            self?.showMBProgressHUD("Deleting...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskFlightFinalReportDelete(reportID: report.id).continueOnSuccessWith(continuation: { task in
                guard let `self` = self else {
                    return
                }
                for index in 0..<self.listReportModel.count {
                    if(report.id == self.listReportModel[index].id) {
                        self.listReportModel.remove(at: index)
                        self.tableView.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                        break
                    }
                    
                }
                
                self.hideMBProgressHUD(true)
                
            }).continueOnErrorWith(continuation: { error in
                self?.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self?.hideMBProgressHUD(true)
            })
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you want to delete this report ?".localizedString(), message: "", actions: [yesAction,cancelAction])
    }
    
    //
    // MARK: ExpandableLabelDelegate
    //
    
    func willExpandLabel(_ label: ExpandableLabel) {
       
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            let model = self.listReportModel[indexPath.row]
            self.listReportModel[indexPath.row].isCollapsed = false
            self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
   
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            let model = self.listReportModel[indexPath.row]
            self.listReportModel[indexPath.row].isCollapsed = true
            self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
    
}
