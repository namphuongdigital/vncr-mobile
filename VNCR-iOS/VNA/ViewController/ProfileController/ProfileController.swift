//
//  ProfileController.swift
//  ACV
//
//  Created by Pham Dai on 04/10/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import Photos
import SwiftyJSON

class ProfileController: BaseViewController {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtFullname: TextField!
    @IBOutlet weak var txtEmail: TextField!
    @IBOutlet weak var txtPhone: TextField!
    @IBOutlet weak var txtDOB: TextField!
    @IBOutlet weak var txtIdCardNo: TextField!
    @IBOutlet weak var txtGender: TextField!
    @IBOutlet weak var btnLogin: Button5Corner!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var stackContainer: UIStackView!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var btnSignOut: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        config()
    }
    
    private func config() {
        
        view.backgroundColor = .white
        
        lblVersion.font = .systemFont(ofSize: 14)
        lblVersion.textColor = .lightGray
        lblVersion.text = "Version".localizedString() + " " + (Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "Unknown")
        
        btnSignOut.locKey = "Sign out".localizedString()
        btnSignOut.setTitleColor(.red, for: UIControl.State())
        btnSignOut.titleLabel?.font = .systemFont(ofSize: 14)
        
        lblTitle.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        lblTitle.textColor = .black
        lblTitle.text = "Profile".localizedString().uppercased()
        
        txtFullname.setup(type: .editable, placeHolder: "Full name".localizedString(), icon: UIImage.init(icon: .FAUser), delegate: self)
        txtEmail.setup(type: .editable, placeHolder: "Email".localizedString(), icon: UIImage.init(icon: .FAEnvelopeSquare), delegate: self)
        txtPhone.setup(type: .editable, placeHolder: "Phone".localizedString(), icon: UIImage.init(icon: .FAPhoneSquare), delegate: self)
        txtDOB.setup(type: .date, placeHolder: "Birthday".localizedString(), icon: UIImage.init(icon: .FACalendar), delegate: self)
        txtIdCardNo.setup(type: .editable, placeHolder: "Id card no".localizedString(), icon: UIImage.init(icon: .FACode), delegate: self)
        txtGender.setup(type: .editable, placeHolder: "Gender".localizedString(), icon: UIImage.init(icon: .FATransgender), delegate: self)
        txtEmail.setKeyboardType(keyboardType: .emailAddress)
        txtPhone.setKeyboardType(keyboardType: .phonePad)
        
        txtFullname.setTypeReturnKey(type: .next)
        txtEmail.setTypeReturnKey(type: .next)
        txtPhone.setTypeReturnKey(type: .next)
        txtIdCardNo.setTypeReturnKey(type: .next)
        txtDOB.setTypeReturnKey(type: .done)
        
        txtFullname.borderColor = .gray
        txtEmail.borderColor = .gray
        txtPhone.borderColor = .gray
        txtDOB.borderColor = .gray
        txtIdCardNo.borderColor = .gray
        txtGender.borderColor = .gray
        
        txtFullname.onTapReturn = {[weak self] _ in guard let `self` = self else { return }
            if !self.txtEmail.isEnabled {self.txtPhone.isFirstResponse = true; return}
            self.txtEmail.isFirstResponse = true
        }
        
        txtEmail.onTapReturn = {[weak self] _ in guard let `self` = self else { return }
            if !self.txtPhone.isEnabled {self.txtIdCardNo.isFirstResponse = true; return}
            self.txtPhone.isFirstResponse = true
        }
        
        txtPhone.onTapReturn = {[weak self] _ in guard let `self` = self else { return }
            self.txtIdCardNo.isFirstResponse = true
        }
        
        txtIdCardNo.onTapReturn = {[weak self] _ in guard let `self` = self else { return }
            self.txtDOB.isFirstResponse = true
        }
        
        txtDOB.onTapReturn = {[weak self] _ in guard let `self` = self else { return }
            self.txtDOB.isFirstResponse = false
        }
        
        btnLogin.setTitle("Update".localizedString(), for: UIControl.State())
        btnLogin.setTitleColor(.white, for: UIControl.State())
        btnLogin.backgroundColor = .greenBGButtonForDark
        
        imageAvatar.addTargetCustom(target: self, action: #selector(self.selectAvatar(_:)), keepObject: nil)
    }
    
    private func getProfile() {
        /*
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        Client.shared.loadUser {[weak self] in guard let `self` = self else { return }
            self.hideMBProgressHUD(true)
            self.person = Client.shared.user
        }
         */
    }
    
    func showData() {
        
        /*
        guard let person = person else {
            return
        }
        
        if let gender = person.person?.gender {
        } else {
            user?.gender = "Male"
        }
        txtGender.setValue(value: user?.gender ?? "Male")
        
        txtFullname.setValue(value: person.person?.fullName ?? "")
        txtEmail.setValue(value: person.person?.email ?? "")
        txtPhone.setValue(value: person.person?.phone ?? "")
        txtDOB.setValue(value: person.person?.birthday?.stringToDate?.dateTimeToddMMyyyy ?? "")
        txtDOB.setValueDate(date: person.person?.birthday?.stringToDateYYYYMMdd ?? Date())
        
        txtEmail.isEnabled = person.person?.email?.count ?? 0 == 0
        txtPhone.isEnabled = person.person?.phone?.count ?? 0 == 0
        
        if let base64 = Client.shared.user?.person?.avatar, base64.count > 0, let dataImage = Data(base64Encoded: base64) {
            imageAvatar.image = UIImage(data: dataImage)
        } else {
            imageAvatar.image = UIImage(named: "photo-camera")
        }
        */
        checkValidData()
    }
    
    func checkValidData() {
        btnLogin.isEnabled = txtFullname.getValue()?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0 &&
        txtEmail.getValue()?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0 &&
        txtPhone.getValue()?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0 &&
        txtDOB.getValue()?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 > 0
    }
    
    @objc func selectAvatar(_ sender:Any) {
        let vc = PhotosController(type: .photo, maximumSelect: 1, delegate: self)
        let nv = UINavigationController(rootViewController: vc)
        self.present(nv, animated: true, completion: nil)
    }

    @IBAction func actionUpdate(_ sender: Any) {
        /*
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskUpdatePersonalInfo(data:try? user?.jsonString())
        .continueOnSuccessWith(continuation: { task in
            let result = task as? JSON
            let data = try result?.rawData()
            if let object:VerifyCodeResponse = try data?.load() {
                UserDefaults.dataSuite.saveObject(key: .userVerify, value:try? object.user.jsonData())
                UserDefaults.dataSuite.saveObject(key: .device, value:try? object.device.jsonData())
            }
            ServiceData.sharedInstance.taskSetPerson(params: self.user?.toParamsACV() ?? [:])
                .continueOnSuccessWith(continuation: { _ in
                    self.getProfile()
                })
                .continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    self.hideMBProgressHUD(true)
                })
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
         */
    }
}

// MARK: - TextFieldDelegate
extension ProfileController: TextFieldDelegate {
    
    func shouldBeginEdit(view: TextField?) -> Bool {
        if view?.isEqual(txtGender) == true {
            let vc = PopoverMenuController(items: ["Male","Female"].compactMap({PopoverMenuController.MenuItem(title: $0, icon: nil, identifier: $0)}), currentSelectedIdentifier:nil) {[weak self] menu in guard let `self` = self else { return }
//                self.user?.gender = menu?.identifier
                self.txtGender.setValue(value: menu?.title ?? "")
            }
            self.present(vc: vc, transitioning: PopoverAnimateTransitionDelegate(sourceView: view), completion: nil)
            return false
        }
        
        return true
    }
    
    func select(view: TextField?) {
        checkValidData()
    }
    
    func date(text: String, view: TextField?) {
//        user?.birthday = view?.selectedDate?.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ss.sss'Z'")
        checkValidData()
    }
    
    func minimumDate(view: TextField?) -> Date? {
        return nil
    }
    
    func maximumDate(view: TextField?) -> Date? {
        return Date()
    }
    
    func typing(text: String?, view: TextField?) {
        
        switch view {
//        case txtFullname: user?.fullName = text
//        case txtPhone: user?.phoneNumber = text
//        case txtEmail: user?.emailAddress = text
//        case txtIdCardNo: user?.idCardNo = text
        default:break
        }
        
        checkValidData()
    }
}

// MARK: - PhotosControllerDelegate
extension ProfileController:PhotosControllerDelegate {
    func selectedPhotos(photos: [PHAsset]) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        photos.first?.getDataToUpload(completionHandler: {[weak self] responseURL, data, error in
            guard let `self` = self else { return }
//            self.user?.avatar = data?.base64EncodedString()
            if let data = data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.imageAvatar.image = image
                }
            }
            DispatchQueue.main.async {
                self.hideMBProgressHUD(true)
            }
        })
    }
}
