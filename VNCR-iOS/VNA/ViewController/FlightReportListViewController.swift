//
//  FlightReportListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/16/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class FlightReportListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, FlightReportFeedTableViewCellDelegate, UpdateFlightReportDelegate, ListImageViewDelegate, FloatyDelegate, ExpandableLabelDelegate {
    
    var floaty = Floaty()
    
    weak var scheduleFlyModel: ScheduleFlightModel! {
        didSet {
            if scheduleFlyModel == nil {return}
            let labelTitle = UILabel()
            labelTitle.textColor = .white
            labelTitle.text = "\(scheduleFlyModel.flightNo)/\(scheduleFlyModel.flightDate)"
            labelTitle.font = UIFont.systemFont(ofSize: 24, weight: .bold)
            navigationItem.titleView = labelTitle
        }
    }
    
    var listReportModel = Array<ReportModel>()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 135
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        self.navigationItem.title = "REPORT LIST".localizedString()
        
        let addBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.addReportBarButtonItemPressed(_:)))
        addBarButtonItem.setFAIcon(icon: .FAPlusCircle, iconSize: 20)
        
        //self.navigationItem.rightBarButtonItems = [addBarButtonItem]
        
        self.layoutFAB()
        
        Broadcaster.register(UpdateFlightReportDelegate.self, observer: self)
        
        self.getNewData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func addReportBarButtonItemPressed(_ sender: UIBarButtonItem?) {
        self.pushReportFlightViewcontroller(scheduleFlyModel: self.scheduleFlyModel, reportModel: ReportModel(), isSameSchedule: true)
    }

    func getNewData() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightFinalreportgetlist(flightId: scheduleFlyModel.flightID).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayReportModel = Array<ReportModel>()
                for item in array {
                    let reportModel = ReportModel(json: item)
                    arrayReportModel.append(reportModel)
                }
                
                self.listReportModel = arrayReportModel
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
            }
                
            
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    

    func layoutFAB() {
        let item = FloatyItem()
        item.hasShadow = false
        item.buttonColor = UIColor("#dba510")
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor("#dba510")
        item.titleLabelPosition = .left
        item.icon = UIImage(named: "create-new.png")?.withRenderingMode(.alwaysTemplate)
        item.iconTintColor = UIColor.white
        item.title = "New report".localizedString()
        item.handler = {[weak self] item in
            guard let `self` = self else {
                return
            }
            self.addReportBarButtonItemPressed(nil)
        }
        floaty.addItem(item: item)
        
        let item2 = FloatyItem()
        item2.hasShadow = false
        item2.buttonColor = UIColor("#dba510")
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor("#dba510")
        item2.titleLabelPosition = .left
        item2.icon = UIImage(named: "quick.png")?.withRenderingMode(.alwaysTemplate)
        item2.iconTintColor = UIColor.white
        item2.title = "Quick report".localizedString()
        item2.handler = {[weak self] item in
            guard let `self` = self else {
                return
            }
            self.createQuickReport()
        }
        floaty.addItem(item: item2)
        
        //floaty.paddingX = self.view.frame.width/2 - floaty.frame.width/2
        //floaty.paddingY = floaty.frame.height
        floaty.fabDelegate = self
        floaty.buttonColor = self.navigationController?.navigationBar.barTintColor ?? UIColor("#166880")
        floaty.plusColor = UIColor.white
        if let tabbar = tabBarController?.tabBar {
            if #available(iOS 11.0, *) {
                floaty.paddingY = tabbar.frame.height + 10 + self.view.safeAreaInsets.bottom
            } else {
                floaty.paddingY = tabbar.frame.height + 10
            }
        } else {
            if #available(iOS 11.0, *) {
                floaty.paddingY = 10 + self.view.safeAreaInsets.bottom
            } else {
                floaty.paddingY = 10
            }
        }
        
        self.view.addSubview(floaty)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listReportModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FlightReportFeedTableViewCell")! as! FlightReportFeedTableViewCell
        cell.loadContent(reportModel: self.listReportModel[indexPath.row])
        cell.delegate = self
        cell.listImageView.delegate = self
        cell.labelContent.delegate = self
        
        cell.layoutIfNeeded()
        cell.labelContent.shouldCollapse = true
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            cell.labelContent.numberOfLines = 8
        } else {
            cell.labelContent.numberOfLines = 4
        }
        cell.labelContent.collapsed = self.listReportModel[indexPath.row].isCollapsed
        
        //cell.labelContent.text = self.listReportModel[indexPath.row].content
        cell.setTextForContent(content: self.listReportModel[indexPath.row].content, comment: self.listReportModel[indexPath.row].comment)
        
        cell.labelContent.setLessLinkWith(lessLink: "Less".localizedString(), attributes: [NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: cell.labelContent.font.pointSize), NSAttributedString.Key.foregroundColor: UIColor.blue], position: NSTextAlignment.right)
        cell.labelContent.collapsedAttributedLink = NSAttributedString(string: "Read more".localizedString(), attributes: [NSAttributedString.Key.font: UIFont.italicSystemFont(ofSize: cell.labelContent.font.pointSize), NSAttributedString.Key.foregroundColor: UIColor.blue])
      
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    
    // MARK: - ListImageViewDelegate
    func didSelectCellPress(viewController: UIViewController) {
        present(viewController, animated: true, completion: {})
    }
    
    
    //MARK - FlightReportFeedTableViewCellDelegate
    func buttonMoreCellPress(reportModel: ReportModel) {
        showActionReport(reportModel)
    }
    
    //
    
    func showActionReport(_ reportModel: ReportModel) {
        var arrayAction = Array<UIAlertAction>()
        if(reportModel.isCanDelete) {
            let action1 = UIAlertAction(title: "Delete".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
                self?.confirmDeleteReport(reportModel)
                
            })
            arrayAction.append(action1)
        }
        
        if(reportModel.isCanUpdate) {
            let action2 = UIAlertAction(title: "Edit".localizedString(), style: UIAlertAction.Style.default, handler: {[weak self] (UIAlertAction) -> Void in
                self?.editReport(reportModel)
                
            })
            arrayAction.append(action2)
        }
        
        if(arrayAction.count > 0) {
            let cancelAction = UIAlertAction(title: "Cancel".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
                print("Cancel pressed")
            })
            
            arrayAction.append(cancelAction)
            
            self.showActionSheetWithAction(title, message: nil, actions: arrayAction)
        }
        
        
    }
    
    func editReport(_ report: ReportModel) {
        self.pushReportFlightViewcontroller(scheduleFlyModel: self.scheduleFlyModel, reportModel: report, isSameSchedule: true)
    }
    
    func confirmDeleteReport(_ report: ReportModel) {
        
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
            self?.showMBProgressHUD("Deleting...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskFlightFinalReportDelete(reportID: report.id).continueOnSuccessWith(continuation: { task in
                guard let `self` = self else {
                    return
                }
                for index in 0..<self.listReportModel.count {
                    if(report.id == self.listReportModel[index].id) {
                        self.listReportModel.remove(at: index)
                        self.tableView.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                        break
                    }
                    
                }
                
                self.hideMBProgressHUD(true)
                
            }).continueOnErrorWith(continuation: { error in
                self?.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self?.hideMBProgressHUD(true)
            })
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you want to delete this report ?".localizedString(), message: "", actions: [yesAction,cancelAction])
    }

    //MARK - UpdateFlightReportDelegate
    func addFlightReporte(report: ReportModel) {
        self.listReportModel.insert(report, at: 0)
        self.tableView.insertRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
    }
    
    
    func refreshFlightReporte(report: ReportModel) {
        for index in 0..<self.listReportModel.count {
            if(report.id == self.listReportModel[index].id) {
                report.avatarURL = self.listReportModel[index].avatarURL
                report.isCanDelete = self.listReportModel[index].isCanDelete
                report.isCanUpdate = self.listReportModel[index].isCanUpdate
                report.created = self.listReportModel[index].created
                self.listReportModel[index] = report
                self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                return
            }
            
        }
        self.addFlightReporte(report: report)
    }
    
    
    // MARK: - FloatyDelegate
    func floatyWillOpen(_ floaty: Floaty) {
        print("Floaty Will Open")
    }
    
    func floatyDidOpen(_ floaty: Floaty) {
        print("Floaty Did Open")
    }
    
    func floatyWillClose(_ floaty: Floaty) {
        print("Floaty Will Close")
    }
    
    func floatyDidClose(_ floaty: Floaty) {
        print("Floaty Did Close")
    }
    
    
    //
    func createQuickReport() {
        let reportModel = ReportModel()
        reportModel.flightID = scheduleFlyModel.flightID
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightFinalReportUpdateInfo(finalReport: reportModel).continueOnSuccessWith(continuation: { task in
            
            if let result = task as? JSON {
                let reportModel = ReportModel(json: result)
                Broadcaster.notify(UpdateFlightReportDelegate.self) {
                    $0.refreshFlightReporte(report: reportModel)
                }
            }
            self.hideMBProgressHUD(true)
            //let _ = self.navigationController?.popViewController(animated: true)
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    //
    // MARK: ExpandableLabelDelegate
    //
    
    func willExpandLabel(_ label: ExpandableLabel) {
        
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            self.listReportModel[indexPath.row].isCollapsed = false
            self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
        
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView.indexPathForRow(at: point) as IndexPath? {
            self.listReportModel[indexPath.row].isCollapsed = true
            self.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
        }
    }
    
}
