//
//  SeatmapViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 3/24/19.
//  Copyright © 2019 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import STPopup

protocol SeatViewControllerDelegate:AnyObject {
    func SeatViewController_getPassergerList() -> PassengerListModel
    func SeatViewController_selectSeat(seatId:String?)
}

class SeatmapViewController: SeatViewController, PopupInfoSeatViewControllerDelagate {
    
    var customTransitionDelegate:UIViewControllerTransitioningDelegate?
    var havePassengerMapList: Bool = false
    var passengers:[PassengerSeat]? = []
    var hasPresented = false
    var hasFillSeats  = false
    
    var seatIdWaitSelected:String? // trong truong hop, this controller nay chua duoc load lan dau thi can store lai de handle sau
    
    var titleText: String = ""
    
    var flightId: Int = 0
    
    var seatmap: AircraftCategoryType = .none
    
    var delegate:SeatViewControllerDelegate?
    
    /// store list position seats by board
    var seats:[BoardSeats.Seat] = []
    
    var arrayJSON: [JSON]!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.navigationItem.title = titleText
        
        // get passenger map
//        self.loadDatService()
        loadData()
        
        let barbutton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_close_128").resizeImageWith(newSize: CGSize(width: 30, height: 30)), style: .done, target: self, action: #selector(close))
        self.navigationItem.setRightBarButton(barbutton, animated: false)
        navigationItem.rightBarButtonItem?.tintColor = .black
        
        NotificationCenter.default.addObserver(self, selector: #selector(fillSeats), name: NSNotification.Name(rawValue: "passengerMapLoadDone"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // remove searchbar duoc add trong list seat
//        self.navigationItem.titleView = nil
//        if self.navigationController != nil {
//            self.navigationController?.makeNavigationBarTransparent()
//        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        hasPresented = true
        // neu chua render ghe
        if !hasFillSeats && delegate?.SeatViewController_getPassergerList().count ?? 0 > 0 {
            fillSeats()
            
            // neu seat duoc chon ma chua duoc xu ly thi xu ly
            if seatIdWaitSelected != nil {
                setSelectedSeat(seatId: seatIdWaitSelected)
            }
        }
    }

    @objc func close() {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "passengerMapLoadDone"), object: nil)
    }
    
    override func loadData() {
        // load map seats by board
        switch seatmap {
        case .A321_16: seats = A32116Board().getSeats()
        case .A321_8: seats = A3218Board().getSeats()
        case .A350: seats = A350Board().getSeats()
        case .B787: seats = A787Board().getSeats()
        case .AT7: seats = AT7Board().getSeats()
        case .ATR: seats = ATRBoard().getSeats()
        case .A32: seats = A32Board().getSeats()
        case .B32: seats = B32Board().getSeats()
        case .C32: seats = C32Board().getSeats()
        case .D32: seats = D32Board().getSeats()
        case .E32: seats = E32Board().getSeats()
        case .N32: seats = N32Board().getSeats()
        case .A35: seats = A35Board().getSeats()
        case .B35: seats = B35Board().getSeats()
        case .A78: seats = A78Board().getSeats()
        case .B78: seats = B78Board().getSeats()
        case .C78: seats = C78Board().getSeats()
        case .none:
            print("seatmap not found")
        }
        
        self.imageBackgroundName = "\(seatmap.rawValue)_empty.png"
        self.initSelectionView([])
    }
    
    @objc func fillSeats() {
        passengers = delegate?.SeatViewController_getPassergerList()
        if hasPresented {
            seats.forEach { seat in
                let path = UIBezierPath(rect: seat.position)
                let markedLayer = CAShapeLayer()
                markedLayer.path = path.cgPath
                if let passenger = passengers?.first(where: {$0.seat?.lowercased() == seat.identifier.lowercased()}) {
                    markedLayer.fillColor = passenger.iconBackgroundColor.cgColor
                    markedLayer.lineWidth = 0
                } else {
                    markedLayer.fillColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
                    markedLayer.strokeColor = UIColor.gray.cgColor
//                    markedLayer.lineWidth = seat.position.width/10
                    markedLayer.lineCap = .round
                }
                selectionView.seatView.imageViewBackground.layer.addSublayer(markedLayer)
            }
            selectionView.initindicator([]) // setup mini map view
            hasFillSeats = true
        }
    }
    
    /// kiem tra touch point tren page seat view
    /// - Parameters:
    ///   - touchPoint: diem user cham tren hinh
    ///   - seatview: hinh uer cham
    override func seatTouch(_ touchPoint: CGPoint, seatView seatview: UIView) {
        seatview.layer.sublayers?.filter({$0.isKind(of: CAShapeLayer.self)}).forEach({$0.removeFromSuperlayer()})
        let seat = seats.first {
            $0.position.contains(touchPoint)
        }
        
        if let s = seat {
            handleSeat(seat: s)
            // goi delegate de update ui cho nhung view lien quan
            delegate?.SeatViewController_selectSeat(seatId: s.identifier)
        }
    }
    
    /// remove highlighted seat
    func resetSelected() {
        selectionView.seatView.layer.sublayers?.filter({$0.name == "selected"}).forEach({$0.removeFromSuperlayer()})
    }
    
    /// hightlight seat
    /// - Parameter seatId: seat identifier
    func setSelectedSeat(seatId:String?) {
        let seat = seats.first {
            $0.identifier == seatId
        }
        
        handleSeat(seat: seat)
        
        if !hasFillSeats {
            seatIdWaitSelected = seatId
        } else {
            seatIdWaitSelected = nil
        }
    }
    
    /// xu ly hanh dong ke tiep cua 1 seat
    /// - Parameter seat: BoardSeats.Seat
    func handleSeat(seat:BoardSeats.Seat?) {
        if seat != nil {
            if let passenger = delegate?.SeatViewController_getPassergerList().first(where: {$0.seat?.lowercased() == seat?.identifier.lowercased()})
            {
                let path = UIBezierPath(rect: seat!.position)
                let markedLayer = CAShapeLayer()
                markedLayer.name = "selected"
                markedLayer.path = path.cgPath
                markedLayer.fillColor = UIColor.systemBlue.cgColor
                selectionView.seatView.layer.addSublayer(markedLayer)
                
                if self.splitViewController == nil {
                    let vc = FlightSeatInformationController(passenger: passenger)
                    let nv = BasePresentNavigationController(root: vc)
                    customTransitionDelegate = ScaleUpAnimateTransitionDelegate()
                    nv.transitioningDelegate = customTransitionDelegate
                    self.present(nv, animated: true, completion: nil)
                    // remove selected
                    vc.onDismiss = {[weak self] in
                        self?.selectionView.seatView.layer.sublayers?.filter({$0.name == "selected"}).forEach({$0.removeFromSuperlayer()})
                    }
                } else {
                    let vc = FlightSeatInformationController(passenger: passenger)
                    let nv = BasePresentNavigationController(root: vc)
                    self.splitViewController?.showDetailViewController(nv, sender: nil)
                }
            } else {
//                resetSelected()
//                let alert = UIAlertController(title: "Alert".localizedString(), message: "Empty seat", preferredStyle: .alert)
//                alert.addAction(UIAlertAction(title: "Ok".localizedString(), style: .default, handler: { action in
//
//                }))
//                self.present(alert, animated: true, completion: nil)
                
                // remove detail view controller
//                if let split = self.splitViewController,
//                   let root = split.viewControllers.first {
//                    self.splitViewController?.viewControllers = [root]
//                }
            }
        }
    }
    
    override func seatSelectionChanged() {
        if let seatButton = self.selecetedSeats.firstObject as? ZFSeatButton, let seatModel = seatButton.seatmodel as? SeatModel {
            print("seatModel \(seatModel.paxName)")
            self.show(seatButton: seatButton, seatModel: seatModel)
        }
    }
    
    func show(seatButton: ZFSeatButton, seatModel: SeatModel) {
        let storyboard = UIStoryboard(name: "Main_iphone", bundle: nil)
        let popupInfoSeatViewController = storyboard.instantiateViewController(withIdentifier: "PopupInfoSeatViewController") as! PopupInfoSeatViewController
        popupInfoSeatViewController.delagate = self
        popupInfoSeatViewController.seatModel = seatModel
        let popupController = STPopupController.init(rootViewController: popupInfoSeatViewController)
        popupController.containerView.layer.cornerRadius = 3
        popupController.transitionStyle = .fade
        popupController.navigationBar.tintColor = UIColor.black
        popupController.present(in: self) {
            if self.selecetedSeats.count > 0 {
                seatButton.isSelected = false
                self.selecetedSeats.remove(seatButton)
            }
        }
    }
    
    func loadDataA321_16() {
        self.imageBackgroundName = "\(AircraftCategoryType.A321_16.rawValue).png";
        let arrayXColumn = [97.0, 145.0, 250.0, 307.0]
        let arrayYRow = [371.0, 420.0, 470.0, 517.0]
        let seatWidth = 40.0
        let seatHeight = 39.0
        let seatsModelArray = NSMutableArray()
        for i in 0..<arrayYRow.count {
            let seatsModel = ZFSeatsModel()
            seatsModel.rowNum = NSNumber(value: (i + 1))
            seatsModel.rowId = String(format: "%@", seatsModel.rowNum)
            var arrayColumn = [ZFSeatModel]();
            for j in 0..<arrayXColumn.count  {
                let json = self.jsonFor(row: i, column: j)
                let seatModel = SeatModel(json: json)
                seatModel.columnId = ""
                seatModel.seatNo = ""
                if(seatModel.paxName == "") {
                    seatModel.st = "E";
                } else {
                    seatModel.st = "N";
                }
                seatModel.frame = CGRect.zero
                seatModel.frame = CGRect(x: arrayXColumn[j], y: arrayYRow[i], width: seatWidth, height: seatHeight)
                arrayColumn.append(seatModel)
            }
            seatsModel.columns = arrayColumn
            seatsModelArray.add(seatsModel)
        }
        self.seatsModelArray = seatsModelArray
        self.initSelectionView(seatsModelArray)
    }
    
    func loadDataA321_8() {
        self.imageBackgroundName = "\(AircraftCategoryType.A321_8.rawValue).png";
        let arrayXColumn = [98.0, 146.0, 250.0, 307.0]
        let arrayYRow = [299.0, 348.0]
        let seatWidth = 40.0
        let seatHeight = 37.0
        let seatsModelArray = NSMutableArray()
        for i in 0..<arrayYRow.count {
            let seatsModel = ZFSeatsModel()
            seatsModel.rowNum = NSNumber(value: (i + 1))
            seatsModel.rowId = String(format: "%@", seatsModel.rowNum)
            var arrayColumn = [ZFSeatModel]();
            for j in 0..<arrayXColumn.count  {
                let json = self.jsonFor(row: i, column: j)
                let seatModel = SeatModel(json: json)
                seatModel.columnId = ""
                seatModel.seatNo = ""
                if(seatModel.paxName == "") {
                    seatModel.st = "E";
                } else {
                    seatModel.st = "N";
                }
                seatModel.frame = CGRect.zero
                seatModel.frame = CGRect(x: arrayXColumn[j], y: arrayYRow[i], width: seatWidth, height: seatHeight)
                arrayColumn.append(seatModel)
            }
            seatsModel.columns = arrayColumn
            seatsModelArray.add(seatsModel)
        }
        self.seatsModelArray = seatsModelArray
        self.initSelectionView(seatsModelArray)
    }

    func loadDataA350() {
        self.imageBackgroundName = "\(AircraftCategoryType.A350.rawValue).png";
        let arrayXColumn = [88.0, 178.0, 223.0, 322.0]
        let arrayYRow = [302.0, 339.0, 375.0, 412.0, 449.0, 486.0, 522.0, 560.0]
        let seatWidth = 38.0
        let seatHeight = 31.0
        let seatsModelArray = NSMutableArray()
        for i in 0..<arrayYRow.count {
            let seatsModel = ZFSeatsModel()
            seatsModel.rowNum = NSNumber(value: (i + 1))
            seatsModel.rowId = String(format: "%@", seatsModel.rowNum)
            var arrayColumn = [ZFSeatModel]();
            for j in 0..<arrayXColumn.count  {
                let json = self.jsonFor(row: i, column: j)
                let seatModel = SeatModel(json: json)
                seatModel.columnId = ""
                seatModel.seatNo = ""
                if(seatModel.paxName == "") {
                    seatModel.st = "E";
                } else {
                    seatModel.st = "N";
                }
                seatModel.frame = CGRect.zero
                seatModel.frame = CGRect(x: arrayXColumn[j], y: arrayYRow[i], width: seatWidth, height: seatHeight)
                arrayColumn.append(seatModel)
            }
            seatsModel.columns = arrayColumn
            seatsModelArray.add(seatsModel)
        }
        self.seatsModelArray = seatsModelArray
        self.initSelectionView(seatsModelArray)
    }
    
    func loadDataB787() {
        self.imageBackgroundName = "\(AircraftCategoryType.B787.rawValue).png";
        let arrayXColumn = [86.5, 178.0, 226.5, 318.0]
        let arrayYRow = [362.0, 401.5, 440.0, 481.0, 519.0, 558.0, 597.0]
        let seatWidth = 37.0
        let seatHeight = 31.0
        let seatsModelArray = NSMutableArray()
        for i in 0..<arrayYRow.count {
            let seatsModel = ZFSeatsModel()
            seatsModel.rowNum = NSNumber(value: (i + 1))
            seatsModel.rowId = String(format: "%@", seatsModel.rowNum)
            var arrayColumn = [ZFSeatModel]();
            for j in 0..<arrayXColumn.count  {
                let json = self.jsonFor(row: i, column: j)
                let seatModel = SeatModel(json: json)
                seatModel.columnId = ""
                seatModel.seatNo = ""
                if(seatModel.paxName == "") {
                    seatModel.st = "E";
                } else {
                    seatModel.st = "N";
                }
                seatModel.frame = CGRect.zero
                seatModel.frame = CGRect(x: arrayXColumn[j], y: arrayYRow[i], width: seatWidth, height: seatHeight)
                arrayColumn.append(seatModel)
            }
            seatsModel.columns = arrayColumn
            seatsModelArray.add(seatsModel)
        }
        self.seatsModelArray = seatsModelArray
        self.initSelectionView(seatsModelArray)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func loadDatService() {
        ServiceData.sharedInstance.taskFlightGetSeatmap(flightId: flightId).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            //var arrayModel = [SeatModel]()
            if let array = result.array {
                self.arrayJSON = array
                self.loadData()
            }
        }).continueOnErrorWith(continuation: { error in
            print((error as NSError).localizedDescription)
            //self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            //self.hideMBProgressHUD(true)
        })
    }
    
    func jsonFor(row: Int, column: Int) -> JSON {
        if let arrayJSON = self.arrayJSON {
            for json in arrayJSON {
                if let r = json["R"].int, let c = json["C"].int {
                    if(row == r && column == c) {
                        return json
                    }
                }
            }
        }
        return JSON()
    }
    
    //MARK - PopupInfoSeatViewControllerDelagate
    func popupInfoSeatViewControllerOKPress(viewController: PopupInfoSeatViewController) {
        
    }

}
