//
//  ListDeviceViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/17/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON


extension UIViewController {
    
    func createBarButton(imageName: String, action: Selector? = nil, renderingMode: UIImage.RenderingMode) -> UIBarButtonItem {
        return self.createDismissBarButton(imageName:imageName, action:action, renderingMode:renderingMode)
    }
    
    
    func createDismissBarButton(imageName: String = "icon-bar.png", action: Selector? = nil, renderingMode: UIImage.RenderingMode) -> UIBarButtonItem {
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 20))
        button.setImage(UIImage.init(named: imageName)?.withRenderingMode(renderingMode), for: .normal)
        if let action = action {
            button.addTarget(self, action: action, for: UIControl.Event.touchUpInside)
        } else {
            button.addTarget(self, action: #selector(self.dismissBarButtonItemPressed(_:)), for: UIControl.Event.touchUpInside)
        }
        
        let dismissBarButtonItem = UIBarButtonItem(customView: button)
        return dismissBarButtonItem
    }
    
    @objc func dismissBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func customBackButton(imageName: String = "icon-bar.png", isDismiss: Bool = false, renderingMode: UIImage.RenderingMode = .alwaysOriginal) {
        if(isDismiss) {
            self.navigationItem.leftBarButtonItem = self.createDismissBarButton(imageName: imageName, renderingMode: renderingMode)
        } else {
            self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: imageName)?.withRenderingMode(renderingMode)
            self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: imageName)?.withRenderingMode(.alwaysOriginal)
            self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItem.Style.plain, target: nil, action: nil)
        }
        
    }
}


class ListDeviceViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, DisplayDeviceTableViewCellDelegate, UISearchBarDelegate {
    
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero) //CustomSearchBarView = CustomSearchBarView(frame: CGRect.zero)
    
    var listDeviceModel = [DeviceModel]()
    
    var refreshControl: UIRefreshControl!
    
    var segmentedControl: UISegmentedControl = UISegmentedControl(frame: CGRect.zero)
    
    @IBOutlet weak var tableView: UITableView!
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var searchText = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.searchBar.sizeToFit()
        //self.navigationItem.setHidesBackButton(true, animated:true)
        //self.customBackButton(imageName: "back-icon.png", renderingMode: UIImageRenderingMode.alwaysTemplate)
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.setupSearchBar()
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        let nib = UINib(nibName: "DisplayDeviceTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "DisplayDeviceTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.keyboardDismissMode = .onDrag
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tableView.alwaysBounceVertical = true
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        getNewData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func refreshData() {
        getNewData()
    }
    
    func getNewData() {
        //self.textTitle = "DEVICES".localizedString()
        ServiceData.sharedInstance.taskDeviceMydevices(keyword: searchText).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<DeviceModel>()
                for item in array {
                    let model = DeviceModel(json: item)
                    arrayModel.append(model)
                }
                self.listDeviceModel = arrayModel
                
            }
            self.tableView.reloadData()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }

    
    func setupSearchBar() {
        //self.searchBar.searchBarOriginX = 0
        //        self.searchBar.tintColor = .white
        //        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.placeholder = "Search".localizedString()
        searchBar.delegate = self
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
    }
    
    //MARK - UISearchBarDelegate
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
        }
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if(self.searchBar.text == "" || self.searchBar.text == nil) {
            searchText = ""
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("")
        searchText = searchBar.text ?? ""
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func configure(cell: DisplayDeviceTableViewCell, at indexPath: IndexPath) {
        cell.deviceModel = self.listDeviceModel[indexPath.row]
        cell.displayDeviceTableViewCellDelegate = self
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listDeviceModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DisplayDeviceTableViewCell")! as! DisplayDeviceTableViewCell
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "DisplayDeviceTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! DisplayDeviceTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
     
    }
    /*
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
     return 0.01
     }
     
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     return 0.01
     }
     */
    
    //MARK - DisplayDeviceTableViewCellDelegate
    func deleteDisplayDeviceTableViewCellPress(deviceModel: DeviceModel) {
   
        UIAlertController.show(in: self, withTitle: "Are you sure deactive this device ?".localizedString(), message: "", preferredStyle: UIAlertController.Style.alert, cancelButtonTitle: "NO", destructiveButtonTitle: "YES", otherButtonTitles: nil, popoverPresentationControllerBlock: nil, tap: {[weak self] (controller, action, buttonIndex) in
            if (buttonIndex == controller.cancelButtonIndex) {
                print("Cancel Tapped")
            } else if (buttonIndex == controller.destructiveButtonIndex) {
                guard let `self` = self else {
                    return
                }
                self.showMBProgressHUD("Loading...".localizedString(), animated: true)
                ServiceData.sharedInstance.taskDeviceDeActivate(id: deviceModel.id).continueOnSuccessWith(continuation: { task in
                    
                    if let responseServiceModel = task as? ResponseServiceModel {
                        let message = responseServiceModel.message ?? ""
                        self.showAlert("Message".localizedString(), stringContent: message)
                    }
                    for index in 0..<self.listDeviceModel.count {
                        if(self.listDeviceModel[index].id == deviceModel.id) {
                            self.listDeviceModel.remove(at: index)
                            break
                        }
                    }
                    self.tableView.reloadData()
                    self.hideMBProgressHUD(true)
                    self.refreshControl.endRefreshing()
                    
                }).continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    self.hideMBProgressHUD(true)
                    self.refreshControl.endRefreshing()
                })
            }
        })
        
       
        
    }
    
    func setMasterDisplayDeviceTableViewCellPress(deviceModel: DeviceModel) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskDeviceSetMaster(id: deviceModel.id).continueOnSuccessWith(continuation: { task in
            
            if let responseServiceModel = task as? ResponseServiceModel {
                let message = responseServiceModel.message ?? ""
                self.showAlert("Message".localizedString(), stringContent: message)
            }
            self.getNewData()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
}
