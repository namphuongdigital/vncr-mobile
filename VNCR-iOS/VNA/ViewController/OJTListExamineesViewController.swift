//
//  OJTListExamineesViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON


class OJTListExamineesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    weak var scheduleFlyModel: ScheduleFlightModel? {
        didSet {
            if scheduleFlyModel == nil {return}
            let labelTitle = UILabel()
            labelTitle.textColor = .white
            labelTitle.text = "\(scheduleFlyModel?.flightNo ?? "")/\(scheduleFlyModel?.flightDate ?? "")"
            labelTitle.font = UIFont.systemFont(ofSize: 24, weight: .bold)
            navigationItem.titleView = labelTitle
        }
    }
    
    var refreshControl: UIRefreshControl!
    
    var segmentedControl: UISegmentedControl = UISegmentedControl(frame: CGRect.zero)
    
    @IBOutlet weak var tableView: UITableView!
    
    var listUserExamineeModel = [UserExamineeModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        let nib = UINib(nibName: "UserExamineeTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "UserExamineeTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
//        self.textTitle = "Select OJT Examinee".localizedString()
        
        self.tableView.alwaysBounceVertical = true
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        getNewData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func refreshData() {
        getNewData()
    }
    
    func getNewData() {
        ServiceData.sharedInstance.taskOJTExaminees(flightID: self.scheduleFlyModel!.flightID).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<UserExamineeModel>()
                for item in array {
                    let model = UserExamineeModel(json: item)
                    arrayModel.append(model)
                }
                self.listUserExamineeModel = arrayModel
                
            }
            self.tableView.reloadData()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
        
    }
    
    func configure(cell: UserExamineeTableViewCell, at indexPath: IndexPath) {

        cell.userExamineeModel = self.listUserExamineeModel[indexPath.row]
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listUserExamineeModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserExamineeTableViewCell")! as! UserExamineeTableViewCell
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "UserExamineeTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! UserExamineeTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.pushOJTListViewController(scheduleFlyModel: scheduleFlyModel, isListSaved: false, cid: self.listUserExamineeModel[indexPath.row].crewID)
    }
    
}
