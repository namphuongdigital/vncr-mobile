//
//  LeadReportPositionViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/30/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//
import UIKit
import Foundation
//import FTPopOverMenu
import DXPopover
import STPopup
import SwiftyJSON

class LeadReportPositionViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, LeaderPersonReportTableViewCellDelegate, LeadReportPositionDelegate, FloatyDelegate {
    
    var floaty = Floaty()
    
    weak var scheduleFlyModel: ScheduleFlightModel?
    
    @IBOutlet weak var tableView: UITableView!
    
    var sendBarButtonItem: UIBarButtonItem!
    
    var noteBarButtonItem: UIBarButtonItem!
    
    var pilots:[CrewDutyModelNew] = []
    var staffs:[CrewDutyModelNew] = []
    
    var crewTaskModel: CrewTaskModel? {
        didSet {
            if let list = crewTaskModel?.crewDutyList {
                pilots = list.filter({$0.isPilot})
                staffs = list.filter({!$0.isPilot})
                
                // get assessment
//                let group = DispatchGroup()
//                staffs.forEach({
//                    $0.loadAssessment(group: group)
//                })
//                group.notify(queue: .main) {[weak self] in guard let `self` = self else { return }
                    self.tableView.reloadData()
//                }
            }
        }
    }
    
    var listSelectorTypeModel: SelectorTypeModels = []
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
 
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "LEADER REPORT POSITION FLIGHT".localizedString()
        
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        
        tableView.contentInset = UIEdgeInsets(top: tableView.contentInset.top, left: tableView.contentInset.left, bottom: 70, right: tableView.contentInset.right)
        
        if #available(iOS 11.1, *) {
            tableView.verticalScrollIndicatorInsets = .zero
        } else {
            // Fallback on earlier versions
        }
        
//        if(UIDevice.current.userInterfaceIdiom == .phone) {
//            self.tableView?.register(UINib(nibName: "LeadReportTableViewHeaderFooterView_iphone", bundle: nil), forHeaderFooterViewReuseIdentifier: NSStringFromClass(LeadReportTableViewHeaderFooterView.self))
//        } else {
//            self.tableView?.register(UINib(nibName: "LeadReportTableViewHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: NSStringFromClass(LeadReportTableViewHeaderFooterView.self))
//        }
        self.tableView?.register(UINib(nibName: "LeadReportTableViewHeaderFooterView_iphone", bundle: nil), forHeaderFooterViewReuseIdentifier: NSStringFromClass(LeadReportTableViewHeaderFooterView.self))
        
        
        sendBarButtonItem = UIBarButtonItem(image: UIImage(named: "ic_upload")?.resizeImage(newSize: CGSize(width: 32, height: 32)), style: .done, target: self, action: #selector(self.sendBarButtonItemPressed(_:)))
//        sendBarButtonItem.setFAIcon(icon: .FACheckCircle, iconSize: 20)
        
        noteBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.noteBarButtonItemPressed(_:)))
        noteBarButtonItem.setFAIcon(icon: .FAStickyNote, iconSize: 20)
    
        
        //self.tableView?.register(LeadReportTableViewHeaderFooterView.self, forHeaderFooterViewReuseIdentifier: NSStringFromClass(LeadReportTableViewHeaderFooterView.self))
        
        
        //self.buttonBack.addTarget(self, action: #selector(self.buttonBackPress(sender:)), for: .touchUpInside)
        /*
        self.buttonNote.addTarget(self, action: #selector(self.buttonNotePress(sender:)), for: .touchUpInside)
        self.buttonNote.backgroundColor = UIColor("#cc9e73")
        self.buttonSend.backgroundColor = UIColor("#166880")
        self.buttonSend.layer.cornerRadius = 3.5
        self.buttonNote.layer.cornerRadius = 3.5
        */
        getFlightCrewtaskcategorylist()
        
        Broadcaster.register(LeadReportPositionDelegate.self, observer: self)
        
        self.layoutFAB()
    }
    
    func getFlightCrewtaskcategorylist() {
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceDataCD.getCrewTaskCategory(flightId: scheduleFlyModel?.flightID ?? 0) {[weak self] model, error in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                if let error = error {
                    self.hideMBProgressHUD(true)
                    self.showAlert("Alert".localizedString(), stringContent: error)
                } else {
                    self.listSelectorTypeModel = model ?? []
                    self.loadData()
                }
            }
        }
    }
    
    func layoutFAB() {
        let item = FloatyItem()
        item.hasShadow = false
        item.buttonColor = UIColor("#dba510")
        //item.circleShadowColor = UIColor.red
        //item.titleShadowColor = UIColor("#dba510")
        item.titleLabelPosition = .left
        item.icon = UIImage(named: "quick.png")?.withRenderingMode(.alwaysTemplate)
        item.iconTintColor = UIColor.white
        item.title = "Auto"
        item.handler = {[weak self] item in
            guard let `self` = self else {
                return
            }
            if let crewTaskModel = self.crewTaskModel {
                for item in crewTaskModel.crewDutyList {
                    if(item.ca.count == 0) {
                        item.ca = item.autoCA
                    }
                    if(item.job.count == 0) {
                        item.job = item.autoJob
                    }
                }
            }
        }
        
        floaty.addItem(item: item)
        //floaty.paddingX = self.view.frame.width/2 - floaty.frame.width/2
        //floaty.paddingY = floaty.frame.height
        floaty.fabDelegate = self
        floaty.buttonColor = self.navigationController?.navigationBar.barTintColor ?? UIColor("#166880")
        floaty.plusColor = UIColor.white
        if let tabbar = tabBarController?.tabBar {
            if #available(iOS 11.0, *) {
                floaty.paddingY = tabbar.frame.height + 10 + self.view.safeAreaInsets.bottom
            } else {
                floaty.paddingY = tabbar.frame.height + 10
            }
        } else {
            if #available(iOS 11.0, *) {
                floaty.paddingY = 10 + self.view.safeAreaInsets.bottom
            } else {
                floaty.paddingY = 10
            }
        }
        self.view.addSubview(floaty)
        
    }
    
    func loadData() {
        ServiceDataCD.getCrewTask(flightId: scheduleFlyModel!.flightID) {[weak self] task, error in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                self.hideMBProgressHUD(true)
                if let error = error {
                    self.showAlert("Alert".localizedString(), stringContent: error)
                } else {
                    self.crewTaskModel = task
                    if let p = self.parent as? ScheduleFlyDetailController {
                        p.configUIWithSegment()
                    } else {
                        self.addUploadButton()
                    }
                    if let flightInfo = self.crewTaskModel?.listFlightInfo.first {
                        self.textTitle = "\(flightInfo.flightNo)/\(flightInfo.departedDate) \(flightInfo.routing)"
                    }
                }
            }
        }
    }
    
    func addUploadButton() {
        if(self.crewTaskModel?.permission != .read) {
            if parent != nil && parent?.isKind(of: BaseViewController.self) == true {
                parent?.navigationItem.rightBarButtonItems?.insert(self.sendBarButtonItem, at: 0)
            } else {
                self.navigationItem.rightBarButtonItems = [self.sendBarButtonItem]
            }
        } else {
            removeUploadButton()
        }
    }
    
    func removeUploadButton() {
        if let keepMenu = parent?.navigationItem.rightBarButtonItems?.filter({!$0.isEqual(self.sendBarButtonItem)}) {
            self.parent?.navigationItem.rightBarButtonItems = keepMenu
        }
    }
    
    @objc func sendBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.updateAllToServer()
    }
    
    @objc func noteBarButtonItemPressed(_ sender: UIBarButtonItem) {
        let popupNoteViewController = self.storyboard?.instantiateViewController(withIdentifier: "PopupNoteViewController") as! PopupNoteViewController
        //popupNoteViewController.popupNoteViewControllerDelagate = self
        let popupController = STPopupController.init(rootViewController: popupNoteViewController)
        popupController.containerView.layer.cornerRadius = 4
        popupController.transitionStyle = .fade
        popupController.navigationBar.tintColor = UIColor.black
        popupController.present(in: self)
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) {
            return staffs.count
        } else {
            return pilots.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if(indexPath.section == 0) {
//            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoFlightReportTableViewCell") as! InfoFlightReportTableViewCell
//            let flightInfo = crewTaskModel!.listFlightInfo[indexPath.row]
//            cell.loadData(flightInfo: flightInfo)
//            return cell
//        }
        
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderPersonReportTableViewCell")! as! LeaderPersonReportTableViewCell
            let crewDutyModel = staffs[indexPath.row]
            if let model = crewDutyModel.old {
                cell.loadData(crewDutyModel: model)
            }
            cell.delegate = self
            if(indexPath.row % 2 == 0) {
                cell.contentView.backgroundColor = UIColor.white
            } else {
                cell.contentView.backgroundColor = UIColor("#fafcfa")
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "LeaderPilotReportTableViewCell")! as! LeaderPilotReportTableViewCell
            let crewDutyModel = pilots[indexPath.row]
            if let model = crewDutyModel.old {
                cell.loadData(crewDutyModel: model)
            }
            cell.delegate = self
            if(indexPath.row % 2 == 0) {
                cell.contentView.backgroundColor = UIColor.white
            } else {
                cell.contentView.backgroundColor = UIColor("#fafcfa")
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    //
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
        var shouldShowheader = true
        
        if(section == 0) {
            shouldShowheader = staffs.count > 0
        } else {
            shouldShowheader = pilots.count > 0
        }
        
        if shouldShowheader {
            let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: NSStringFromClass(LeadReportTableViewHeaderFooterView.self))! as! LeadReportTableViewHeaderFooterView
            if(section == 1) {
                cell.configForPilot()
            }
            return cell
        } else {
            return UIView(frame: CGRect.zero)
        }
    }
    
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0, staffs.count == 0 {
            return 0.0
        } else if section == 1, pilots.count == 0 {
            return 0.0
        }
        if section == 1 {
            return 10
        } else {
            if(UIDevice.current.userInterfaceIdiom == .phone) {
                return 36.0
            }
            return 50.0
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            if(indexPath.section == 0) {
                return 50.0
            } else {
                return 50.0
            }
        } else {
            if(indexPath.section == 0) {
                return 59.5
            } else {
                return 59.5
            }
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    //MARK - LeaderPersonReportTableViewCellDelegate
    
    func imageViewAvatarTapped(crewDutyModel: CrewDutyModel) {
        self.pushProfileViewController(crewId: crewDutyModel.crewID, permission: self.crewTaskModel?.permission ?? .read, flightId: self.scheduleFlyModel?.flightID ?? 0, sourceCrewID: ServiceData.sharedInstance.userModel?.crewID ?? "", destinationCrewID: crewDutyModel.crewID)
    }
    
    func showPopupSelected(sender: UIButton, tableViewCell: UITableViewCell, array: Array<AnyObject>?, buttonColunmType: CategoryType) {
        if(self.crewTaskModel?.permission != .read) {
            var menuType = FTPopOverMenuType.square
            var array = Array<AnyObject>()
            var arrayString = Array<String>()
            if(buttonColunmType == .training) {
                menuType = FTPopOverMenuType.wide
                if let indexPath = tableView.indexPath(for: tableViewCell) {
                    let crewDutyModel = self.crewTaskModel!.crewDutyList[indexPath.row]
                    if(crewDutyModel.isTrainee == false) {
                        return;
                    }
                    array = Array<CrewDutyModel>()
                    for item in crewTaskModel!.listTrainer {
                        if(item.crewFirstName != crewDutyModel.crewFirstName) {
                            array.append(item)
                        }
                    }
                    
                    //array = crewTaskModel!.crewDutyList//listKem(crewDutyModel: crewDutyModel)
                    
                    for item in array {
                        let crewDutyModel = item as! CrewDutyModel
                        arrayString.append(crewDutyModel.crewFirstName)
                    }
                    array.insert(CrewDutyModel(), at: 0)
                    arrayString.insert("✖︎", at: 0)
                    if(array.count == 1 && crewDutyModel.training == "") {
                        array.removeAll()
                        arrayString.removeAll()
                    }
                }
                
            } else {
                if(buttonColunmType == .ca) {
                    for item in self.listSelectorTypeModel {
                        if(item.categoryId == .all || item.categoryId == buttonColunmType) {
                            var isExist = false
                            for itemSelected in self.crewTaskModel!.crewDutyList {
                                if(item.name == itemSelected.ca) {
                                    isExist = true
                                    break
                                }
                            }
                            if(isExist == false) {
                                array.append(item)
                                arrayString.append(item.name)
                            }
                            
                        }
                    }
                } else {
                    for item in self.listSelectorTypeModel {
                        if(item.categoryId == .all || item.categoryId == buttonColunmType) {
                            array.append(item)
                            arrayString.append(item.name)
                        }
                    }
                }
                
                if array.count > 0 {
                    array.insert(SelectorTypeModel(name: "✖︎", id: -1, categoryID: "", selectorTypeModelDescription: ""), at: 0)
                    arrayString.insert("✖︎", at: 0)
                }
                
            }
            
            if(array.count == 0) {
                return
            }
            
            //FTPopOverMenu.show(forSender: sender, withMenuArray: arrayString, doneBlock: {[weak self] (index) in
            FTPopOverMenu.show(forSender: sender, withMenu: arrayString, menuType: menuType, doneBlock: {[weak self] (index) in
                
                guard let `self` = self else {
                    return
                }
                
                if let indexPath = self.tableView.indexPath(for: tableViewCell) {
                    
                    let item = self.crewTaskModel!.crewDutyList[indexPath.row]//CrewDutyModel(crewDutyModel: self.crewTaskModel!.crewDutyList[indexPath.row])
                    
                    if(buttonColunmType == .training) {
                        if(index == 0) {
                            item.trainingCrewDutyModel = nil
                        } else {
                            item.trainingCrewDutyModel = array[index] as? CrewDutyModel
                        }
                        
                    } else {
                        if index < array.count, let selectorTypeModel = array[index] as? SelectorTypeModel {
                            let name = selectorTypeModel.name.lowercased() == "✖︎" ? "" : selectorTypeModel.name
                            if(buttonColunmType == .job) {
                                item.job = name
                            } else if(buttonColunmType == .ca) {
                                item.ca = name
                            } else if(buttonColunmType == .ann) {
                                item.ann = name
                            } else if(buttonColunmType == .vip) {
                                item.vip = name
                            } else if(buttonColunmType == .dutyFree) {
                                item.dutyFree = name
                            }
                        }
                        
                    }
                    
                    //self.updateRowToServer(indexPath: indexPath)
                    //self.updateRowToServer(indexPath: indexPath, crewDutyModel: item)
                    self.tableView.reloadRows(at: [indexPath], with: .none)
                }
                
                }, dismiss: {
                    print("")
            })
        }
        
        
    }
    
    func listKem(crewDutyModel: CrewDutyModelNew) -> [CrewDutyModelNew] {
        var listKem = Array<CrewDutyModelNew>()
        if(isKemInList(crewID: crewDutyModel.crewID) == false) {
            for person in self.crewTaskModel!.crewDutyList {
                if(crewDutyModel.crewID != person.crewID) {
                    if(person.training == "" && isKemInList(crewID: person.crewID) == false) && person.isReadOnly == false {
                        listKem.append(person)
                    }
                }
            }
        }
        
        return listKem
    }
    
    func isKemInList(crewID: String) -> Bool {
        for personKem in self.crewTaskModel!.crewDutyList {
            if(personKem.training == crewID) {
                return true
            }
        }
        return false
    }
    
    func updateRowToServer(indexPath: IndexPath) {
        
        var crewDutyList = Array<[String: String]>()
        let crewDutyModel = self.crewTaskModel!.crewDutyList[indexPath.row]
        let parameters = ["CrewID": crewDutyModel.crewID, "CrewFirstName": crewDutyModel.crewFirstName, "CrewLastName": crewDutyModel.crewLastName, "FO_Job": crewDutyModel.foJob,"Ability": crewDutyModel.ability, "Job": crewDutyModel.job, "CA": crewDutyModel.ca, "ANN": crewDutyModel.ann, "Training": crewDutyModel.training, "VIP": crewDutyModel.vip, "Other": crewDutyModel.other, "DutyFree": crewDutyModel.dutyFree]
        crewDutyList.append(parameters)
        ServiceData.sharedInstance.taskFlightCrewtaskUpdate(flightId: self.scheduleFlyModel!.flightID, tripId: self.scheduleFlyModel!.isExpanded == true ? scheduleFlyModel!.trips.tripID : 0, crewDutyList: crewDutyList).continueOnSuccessWith(continuation: { task in
            
            let responseServiceModel = task as! ResponseServiceModel
            let result = responseServiceModel.result as! JSON
            let scheduleFlyModel = ScheduleFlyModel(json: result)
            scheduleFlyModel.message = responseServiceModel.message ?? "Your changes have been saved.".localizedString()
            //self.crewTaskModel?.crewDutyList[indexPath.row] = crewDutyModel
            //self.tableView.reloadRows(at: [indexPath], with: .none)
            
            self.hideMBProgressHUD(true)
            self.showMessageSuccess(content: scheduleFlyModel.message)
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    func updateAllToServer() {
        
        var crewDutyList = Array<[String: String]>()
        for crewDutyModel in self.crewTaskModel!.crewDutyList.filter({!$0.isPilot}) {
            let parameters = ["CrewID": crewDutyModel.crewID, "CrewFirstName": crewDutyModel.crewFirstName, "CrewLastName": crewDutyModel.crewLastName, "FO_Job": crewDutyModel.foJob,"Ability": crewDutyModel.ability, "Job": crewDutyModel.job, "CA": crewDutyModel.ca, "ANN": crewDutyModel.ann, "Training": crewDutyModel.training, "VIP": crewDutyModel.vip, "Other": crewDutyModel.other, "DutyFree": crewDutyModel.dutyFree]
            crewDutyList.append(parameters)
        }
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightCrewtaskupdatesupporttrip(flightId: self.scheduleFlyModel!.flightID, isUpdateTrip: self.scheduleFlyModel!.isExpanded == true ? 1 : 0, crewDutyList: crewDutyList).continueOnSuccessWith(continuation: { task in
            let responseServiceModel = task as! ResponseServiceModel
            let result = responseServiceModel.result as? JSON
            
            do {
                let data = try result?.rawData()
                if let item:ScheduleFlightModel = try data?.load() {
                    item.message = responseServiceModel.message ?? "Your changes have been saved.".localizedString()
                    self.showMessageSuccess(content: item.message)
                    Broadcaster.notify(ReportPositionDelegate.self) {
                        $0.updateReportPosition(item: item)
                    }
                }
            } catch let err {
                #if DEBUG
                print("\(err) \(#function)")
                #endif
                self.showAlert("Alert".localizedString(), stringContent: (err as NSError).localizedDescription)
            }
            self.hideMBProgressHUD(true)
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    //MARK - LeadReportPositionDelegate
    func refreshAuthorizeLeadReportPosition() {
        getFlightCrewtaskcategorylist()
    }
    
    
    // MARK: - FloatyDelegate
    func floatyWillOpen(_ floaty: Floaty) {
        print("Floaty Will Open")
    }
    
    func floatyDidOpen(_ floaty: Floaty) {
        print("Floaty Did Open")
    }
    
    func floatyWillClose(_ floaty: Floaty) {
        print("Floaty Will Close")
    }
    
    func floatyDidClose(_ floaty: Floaty) {
        print("Floaty Did Close")
    }
    
   
}
