//
//  AssessmentCrewDutyListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class AssessmentCrewDutyListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, CrewDutyModelDelete, AssessmentCrewDutyTableViewCellDelegate {

    weak var scheduleFlyModel: ScheduleFlightModel! {
        didSet {
            if scheduleFlyModel == nil {return}
            let labelTitle = UILabel()
            labelTitle.textColor = .white
            labelTitle.text = "\(scheduleFlyModel.flightNo)/\(scheduleFlyModel.flightDate)"
            labelTitle.font = UIFont.systemFont(ofSize: 24, weight: .bold)
            navigationItem.titleView = labelTitle
        }
    }
    
    var listCrewDutyModel = Array<CrewDutyModel>()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "ASSESSMENT LIST".localizedString()
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        Broadcaster.register(CrewDutyModelDelete.self, observer: self)
        
//        if(UIDevice.current.userInterfaceIdiom == .phone) {
            self.tableView?.register(UINib(nibName: "AssessmentTableViewHeaderFooterView_iphone", bundle: nil), forHeaderFooterViewReuseIdentifier: NSStringFromClass(AssessmentTableViewHeaderFooterView.self))
//        } else {
//            self.tableView?.register(UINib(nibName: "AssessmentTableViewHeaderFooterView", bundle: nil), forHeaderFooterViewReuseIdentifier: NSStringFromClass(AssessmentTableViewHeaderFooterView.self))
//        }
        
        self.getNewData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func getNewData() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightAssessmentcrewdutylist(flightId: scheduleFlyModel.flightID).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayCrewDutyModel = Array<CrewDutyModel>()
                for item in array {
                    let crewDutyModel = CrewDutyModel(json: item)
                    arrayCrewDutyModel.append(crewDutyModel)
                }
                self.listCrewDutyModel = arrayCrewDutyModel
                let group = DispatchGroup()
//                self.listCrewDutyModel.forEach {
//                    $0.loadAssessment(group: group)
//                }
                group.notify(queue: .main) {
                    self.tableView.reloadData()
                    self.hideMBProgressHUD(true)
                }
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listCrewDutyModel.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "AssessmentCrewDutyTableViewCell")! as! AssessmentCrewDutyTableViewCell
        let crewDutyModel = self.listCrewDutyModel[indexPath.row]
        cell.loadData(crewDutyModel: crewDutyModel)
        cell.delegate = self
        if(indexPath.row % 2 == 0) {
            cell.contentView.backgroundColor = UIColor.white
        } else {
            cell.contentView.backgroundColor = UIColor("#fafcfa")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let crewDutyModel = self.listCrewDutyModel[indexPath.row]
        if !crewDutyModel.isReadOnly {
            ApplicationData.sharedInstance.getBlockRealmWrite {
                ServiceData.sharedInstance.userModel?.job = ""
                for item in self.listCrewDutyModel {
                    if(item.crewID == ServiceData.sharedInstance.userModel?.crewID) {
                        ServiceData.sharedInstance.userModel?.job = item.foJob
                    }
                }
                self.pushAssessmentCrewDutyViewController(crewDutyModel: self.listCrewDutyModel[indexPath.row])
                self.tableView.deselectRow(at: indexPath, animated: true)
            }
            
        }
        
    }
    
    
    //
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: NSStringFromClass(AssessmentTableViewHeaderFooterView.self))! as! AssessmentTableViewHeaderFooterView
        
        return cell
        
    }
    
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(UIDevice.current.userInterfaceIdiom == .phone)
        {
            return 36.0
        }
        return 50.0
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            return 50.0
        } else {
            return 60.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    //MARK - CrewDutyModelDelegate
    func refreshCrewDuty(_ crewDutyModel: CrewDutyModel) {
        for index in 0..<self.listCrewDutyModel.count {
            if(crewDutyModel.crewID == self.listCrewDutyModel[index].crewID) {
                self.listCrewDutyModel[index] = crewDutyModel
                self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                return
            }
        }
    }
    
    //MARK - AssessmentCrewDutyTableViewCellDelegate
    func imageViewAvatarTapped(crewDutyModel: CrewDutyModel) {
        self.pushProfileViewController(crewId: crewDutyModel.crewID)
    }
    
}
