//
//  PhotoLibraryCollectCell.swift
//  GlobeDr
//
//  Created by Dai Pham on 3/16/18.
//  Copyright © 2018 Eating VietNam. All rights reserved.
//

import UIKit
import Photos

class PhotoLibraryCollectCell: BaseCollectionCell {

    // MARK: - api
    func load(asset:PHAsset? = nil,_ isSelected:Bool = false) {
        self.asset = asset
        if let asset = asset {
            imageView.contentMode = .scaleAspectFill
            requestAssetID = asset.getImage(size: CGSize(width: 250, height: 250), {[weak self] (image) in
                guard let _self = self, let image = image else {return}
                _self.imageView.image = image
            })
            
            vwTime.isHidden = !(asset.mediaType == .video)
            if asset.mediaType == .video {
                lblTime.text = asset.duration.stringFromTimeInterval()
            }
        } else {
            imageView.contentMode = .center
            imageView.image = #imageLiteral(resourceName: "photo-camera").tint(with: #colorLiteral(red: 0.5026370883, green: 0.5068690181, blue: 0.5180484056, alpha: 1))
        }
        if isSelected {
            imageView.layer.borderColor = #colorLiteral(red: 0.1294117647, green: 0.5882352941, blue: 0.9529411765, alpha: 1)
            imageView.layer.borderWidth = 4
            imageView.addMask(color: #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1), 0.3)
        } else {
            imageView.layer.borderColor = UIColor.clear.cgColor
            imageView.layer.borderWidth = 0
            imageView.removeMask()
        }
    }
    
    // MARK: - private
    private func config() {
        lblTime.font = .systemFont(ofSize: 12)
        lblTime.textColor = .white
        imageView.circularProgressView.isHidden = true
    }
    
    // MARK: - init
    override func awakeFromNib() {
        super.awakeFromNib()
        
        config()
    }
    
    override func prepareForReuse() {
        imageView.image = nil
        imageView.contentMode = .scaleAspectFill
        imageView.layer.borderColor = UIColor.clear.cgColor
        imageView.layer.borderWidth = 0
        asset = nil
        imageView.removeMask()
        if let id = requestAssetID {
            asset?.cancelRequest(requestId: id)
        }
        requestAssetID = nil
        vwTime.isHidden = true
    }
    
    // MARK: - closures
    
    // MARK: - properties
    var asset: PHAsset?
    var requestAssetID:PHImageRequestID?
    
    // MARK: - outlet
    @IBOutlet weak var imageView: ImageView10Corner!
    @IBOutlet weak var vwTime: UIView!
    @IBOutlet weak var lblTime: UILabel!
}
