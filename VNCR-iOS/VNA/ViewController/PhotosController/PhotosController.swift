//
//  PhotosController.swift
//  GlobeDr
//
//  Created by dai on 1/9/19.
//  Copyright © 2019 GlobeDr. All rights reserved.
//

import UIKit
import Photos
import MobileCoreServices

fileprivate let column = UIDevice.current.userInterfaceIdiom == .pad ? CGFloat(6) : CGFloat(3)
fileprivate let space = CGFloat(10)

@objc protocol PhotosControllerDelegate:AnyObject {
    @objc optional func selectedPhotos(photos:[PHAsset])
    @objc optional func takeAPhoto(photo:UIImage)
    @objc optional func takeAVideo(data:Data,url:URL, thumbnail:UIImage?)
    @objc optional func numerMediasSelected() -> Int
}

extension PhotosController:PhotosModelDelegate {
    
    func PhotosModel_PHAuthorizationStatus(status:PHAuthorizationStatus) {
        if #available(iOS 14, *) {
            btnUpdateLibrary.isHidden = status != .limited
        } else {
            btnUpdateLibrary.isHidden = true
        }
    }
    
    func apply(updates: [IndexPath], deletes: [IndexPath]) {
        collectionView.reloadData()
    }
    
    func startRequest(showLoading: Bool) {
        
    }
    
    func finishRequest() {
        
    }
    
    func failedRequest(error: Any?) {
        
    }
    
    func forceClose() {
        DispatchQueue.main.async {
            self.actionBarRightButton()
        }
    }
}

extension PhotosController:UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.items.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoLibraryCollectCell.identifier, for: indexPath) as! PhotoLibraryCollectCell
        if indexPath.row == 0 {
            cell.load(asset: nil, false)
        } else {
            let item = viewModel.items[indexPath.row - 1]
            cell.load(asset: item, viewModel.itemsSelected.contains(item))
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let numberFilesSelected = (delegate?.numerMediasSelected?() ?? 0) + self.viewModel.itemsSelected.count
        
        if indexPath.row == 0 {
            if numberFilesSelected >= viewModel.maximumSelect && maximumSelect > 1 {
                self.showAlert("Alert".localizedString(), stringContent: "Only selected \(maximumSelect) PHOTO(s)".localizedString())
                return
            }
            openCamera(view: collectionView.cellForItem(at: indexPath))
        } else {
            let item = self.viewModel.items[indexPath.row - 1]
            if maximumSelect == 1 {
                self.viewModel.itemsSelected = []
                self.viewModel.itemsSelected.append(item)
                collectionView.reloadData()
            } else {
                if !self.viewModel.itemsSelected.contains(item) {
                    if numberFilesSelected >= viewModel.maximumSelect {return}
                    if maximumSelect == 1 {
                        self.viewModel.itemsSelected = [item]
                        actionBarRightButton() // forced select
                    } else {
                        self.viewModel.itemsSelected.append(item)
                    }
                } else {
                    for (i,v) in self.viewModel.itemsSelected.reversed().enumerated() {
                        if v.isEqual(item) {
                            self.viewModel.itemsSelected.remove(at: (self.viewModel.itemsSelected.count - 1) - i)
                        }
                    }
                }
                collectionView.reloadItems(at: [indexPath])
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let realspace = space * (column-1)
        let width = (collectionView.frame.size.width - (space*2) - realspace)/column - 5
        return CGSize(width:width, height:width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 0, left: space, bottom: 0, right: space)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return CGFloat(space)
    }
}

// MARK: -  private
extension PhotosController {
    func setPhotos(items:[PHAsset]) {
        viewModel.itemsSelected = items
    }
    
    func openCamera(view:UIView? = nil) {
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            EZAlertController.alert("Alert", message: "Camera is not available on your phone.")
            return
        }
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.sourceType = .camera
        imagePickerController.modalPresentationStyle = .overFullScreen
        switch type {
        case .photo:
            imagePickerController.mediaTypes = [kUTTypeImage as String]
        case .video:
            imagePickerController.mediaTypes = [kUTTypeMovie as String]
        case .both:
            imagePickerController.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        }
        imagePickerController.delegate = self
        if UIDevice.current.userInterfaceIdiom == .pad {
            imagePickerController.popoverPresentationController?.sourceView = view
        }
        EasyPermission.requestCameraPermission { (status) in
            if(status == EasyAuthorityStatus.authorizationStatusDenied) {
                DispatchQueue.main.async {
                    EasyPermission.alertTitle("Alert".localizedString(), message: "Press Setting for config Camera".localizedString())
                }
                //
            } else {
                DispatchQueue.main.async {
                    self.present(imagePickerController, animated: true, completion: nil)
                }
            }
        }
    }
    
    private func config() {
        
        view.backgroundColor = .white
        collectionView.backgroundColor = .white
        
        setBarCloseButton(title: "library".localizedString())
        
        let doneButtonItem = UIBarButtonItem(title: "Done".localizedString(), style: .done, target: self, action: #selector(actionBarRightButton))
        navigationItem.rightBarButtonItem = doneButtonItem
        
        btnUpdateLibrary.titleLabel?.font = .systemFont(ofSize: 13, weight: .medium)
        btnUpdateLibrary.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(PhotoLibraryCollectCell.nib, forCellWithReuseIdentifier: PhotoLibraryCollectCell.identifier)
        
        viewModel.authoriztionAndFetch()
        
        if #available(iOS 11.0, *) {
            collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: self.view.safeAreaInsets.bottom + 20, right: 0)
        } else {
            collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 0, bottom: 20, right: 0)
        }
    }
    
    override func actionBarRightButton() {
        delegate?.selectedPhotos?(photos: self.viewModel.itemsSelected)
        if self.navigationController?.viewControllers.count ?? 0 > 1 {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
}

// MARK: -  override, outlet, properties
class PhotosController: BaseViewController {
    
    enum PhotoType:Int {
        case photo
        case video
        case both
    }
    
    // MARK: -  properties
    weak var delegate:PhotosControllerDelegate?
    var viewModel:PhotosModel!
    var shouldDismiss = false
    var viewVisible = false
    let maximumSelect:Int
    
    var type:PhotoType
    var animationProgressWhenInterrupted:CGFloat = 0
    let cardHeight:CGFloat = UIScreen.bounceWindow.height - 44
    let cardHandleAreaHeight:CGFloat = 20
    
    var isLoadingWebView:Bool = false
    
    // MARK: -  outlet
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnUpdateLibrary: UIButton!
    
    init(type:PhotoType, maximumSelect:Int = 3, delegate:PhotosControllerDelegate?) {
        self.type = type
        self.maximumSelect = maximumSelect
        super.init(nibName: String(describing: PhotosController.self), bundle: .main)
        self.delegate = delegate
        viewModel = PhotosModel(type: type, maximumSelect: maximumSelect, delegate: self)
    }
    
    required init?(coder: NSCoder) {
        type = .both
        maximumSelect = 3
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        config()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        preferredContentSize = CGSize(width: self.view.frame.width, height: UIScreen.bounceWindow.height * 0.75)
    }
    
    @IBAction func updateLibrary(_ sender: Any) {
        self.viewModel.presentLimitedLibraryPicker()
    }
}

// MARK: -
var PICKER = "PIKCER"
extension PhotosController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        // Local variable inserted by Swift 4.2 migrator.
        let info = convertFromUIImagePickerControllerInfoKeyDictionary(info)
        let mediaType = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaType)] as! NSString
        objc_setAssociatedObject(self, &PICKER, picker, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        if mediaType.isEqual(to: kUTTypeImage as NSString as String) {
            if let image = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.originalImage)] as? UIImage {
                UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
            else {
                picker.dismiss(animated: true) {[weak self] in
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                        self?.viewModel.authoriztionAndFetch()
                    }
                }
            }
        } else if mediaType.isEqual(to: kUTTypeMovie as NSString as String) {
            // Is Video
            if let url: URL = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL {
//               let chosenVideo = info[convertFromUIImagePickerControllerInfoKey(UIImagePickerController.InfoKey.mediaURL)] as? URL
//               let videoData = try? Data(contentsOf: chosenVideo, options: [])
//                let thumbnail = url.getThumbnail()
//                picker.dismiss(animated: true) {
//                    self.delegate?.takeAVideo?(data: videoData, url: url, thumbnail:thumbnail)
//                }
                UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, #selector(self.video(_:didFinishSavingWithError:contextInfo:)), nil)
            } else {
                picker.dismiss(animated: true) {[weak self] in
                    DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(500)) {
                        self?.viewModel.authoriztionAndFetch()
                    }
                }
            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let err = error {
            if let picker = objc_getAssociatedObject(self, &PICKER) as? UIImagePickerController {
                picker.dismiss(animated: true, completion: {
                    self.showAlert("Alert".localizedString(), stringContent: err.localizedDescription)
                })
            }
        } else {
            if let picker = objc_getAssociatedObject(self, &PICKER) as? UIImagePickerController {
                self.viewModel.fetch(true)
                picker.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func video(_ videoPath: String, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let err = error {
            if let picker = objc_getAssociatedObject(self, &PICKER) as? UIImagePickerController {
                picker.dismiss(animated: true, completion: {
                    self.showAlert("Alert".localizedString(), stringContent: err.localizedDescription)
                })
            }
        } else {
            if let picker = objc_getAssociatedObject(self, &PICKER) as? UIImagePickerController {
                self.viewModel.fetch(true)
                picker.dismiss(animated: true, completion: nil)
            }
        }
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKeyDictionary(_ input: [UIImagePickerController.InfoKey: Any]) -> [String: Any] {
	return Dictionary(uniqueKeysWithValues: input.map {key, value in (key.rawValue, value)})
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromUIImagePickerControllerInfoKey(_ input: UIImagePickerController.InfoKey) -> String {
	return input.rawValue
}
