//
//  PhotosModel.swift
//  GlobeDr
//
//  Created by dai on 2/15/19.
//  Copyright © 2019 GlobeDr. All rights reserved.
//

import UIKit
import Photos

protocol PhotosModelDelegate: AnyObject {
    func startRequest(showLoading:Bool)
    func finishRequest()
    func failedRequest(error:Any?)
    func apply(updates: [IndexPath], deletes:[IndexPath])
    func forceClose()
    
    func PhotosModel_PHAuthorizationStatus(status:PHAuthorizationStatus)
}

private extension PhotosModel {
    func setup(newItems: [PHAsset], isDelete:Bool) {
        items = newItems
        delegate?.apply(updates: [], deletes: [])
    }
}

class PhotosModel: NSObject {

    // MARK: -  properties
    weak var delegate:PhotosModelDelegate?
    var items:[PHAsset] = []
    var itemsSelected:[PHAsset] = []
    var type:PhotosController.PhotoType
    var maximumSelect:Int = 3
    init(type:PhotosController.PhotoType,maximumSelect:Int, delegate:PhotosModelDelegate?) {
        self.type = type
        self.maximumSelect = maximumSelect
        super.init()
        self.delegate = delegate
        PHPhotoLibrary.shared().register(self)
    }
    
    func presentLimitedLibraryPicker() {
        if let vc = self.delegate as? UIViewController {
            if #available(iOS 14, *) {
                PHPhotoLibrary.shared().presentLimitedLibraryPicker(from: vc)
            } else {
                // Fallback on earlier versions
            }
        }
    }
}

extension PhotosModel: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func fetch(_ selectFirst:Bool = false) {
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        fetchOptions.includeHiddenAssets = false
        fetchOptions.includeAllBurstAssets = false
        fetchOptions.includeAssetSourceTypes = [.typeUserLibrary,.typeCloudShared,.typeiTunesSynced]
        
        let handle:(PHFetchResult<PHAsset>)->Void = {[weak self] allPhotos in guard let `self` = self else { return }
            var temp:[PHAsset] = []
            if allPhotos.count == 0 {
                self.itemsSelected = []
                DispatchQueue.main.async {
                    self.delegate?.apply(updates: [], deletes: [])
                }
                return
            }
            allPhotos.enumerateObjects({(asset, idx, _) in
                if selectFirst && idx == 0 && self.itemsSelected.count < self.maximumSelect {
                    self.itemsSelected.append(asset)
                }
                
                temp.append(asset)
            })
            
            self.items = temp
            // check asset is deleted or remove
            self.itemsSelected
                .filter({!self.items.contains($0)})
                .enumerated()
                .compactMap({$0.offset})
                .forEach{self.itemsSelected.remove(at: $0)}
                
            DispatchQueue.main.async {
                self.delegate?.apply(updates: [], deletes: [])
            }
        }
        
        switch type {
        case .both:
            handle(PHAsset.fetchAssets(with: fetchOptions))
        case .photo:
            handle(PHAsset.fetchAssets(with: .image, options: fetchOptions))
        case .video:
            handle(PHAsset.fetchAssets(with: .video, options: fetchOptions))
        }
    }
    
    func authoriztionAndFetch() {
        func handle(status:PHAuthorizationStatus) {
            switch status
            {
            case .authorized:
                fetch()
            case .denied, .restricted, .notDetermined:
                DispatchQueue.main.async {
                    self.showPermissionPhotoInvalid(self.delegate as? UIViewController)
                }
            case .limited:
                fetch()
            default:
                break
            }
            
            DispatchQueue.main.async {
                self.delegate?.PhotosModel_PHAuthorizationStatus(status:status)
            }
        }
        
        if #available(iOS 14, *) {
            PHPhotoLibrary.requestAuthorization(for: .readWrite) { (status) in
                handle(status: status)
            }
        } else {
            PHPhotoLibrary.requestAuthorization { (status) in
                handle(status: status)
            }
        }
    }
    
    func showPermissionPhotoInvalid(_ fromController:UIViewController? = nil) {
        EasyPermission.requestPhotoLibrayPermission { (status) in
            if(status == EasyAuthorityStatus.authorizationStatusDenied) {
                DispatchQueue.main.async {
                    EasyPermission.alertTitle("Alert".localizedString(), message: "Press Setting for config Photos".localizedString())
                }
            }
        }
    }
}

extension PhotosModel: PHPhotoLibraryChangeObserver {
    func photoLibraryDidChange(_ changeInstance: PHChange) {
        fetch(false)
    }
}
