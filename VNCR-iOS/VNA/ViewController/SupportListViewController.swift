//
//  SupportListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 4/6/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class SupportListViewController: TextInputViewController, UITableViewDelegate, UITableViewDataSource, SupportItemTableViewCellDelegate, UISearchBarDelegate {

    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero) //CustomSearchBarView(frame: CGRect.zero)
    
    private var pageIndex: Int = 1
    
    var listSupportModel = Array<SupportModel>()
    
    var listSearchSupportModel = Array<SupportModel>()
    
    var refreshControl: UIRefreshControl!
    
    var addBarButtonItem: UIBarButtonItem!
    
    var filterBarButtonItem: UIBarButtonItem!
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView?.addSubview(refreshControl)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //searchBar.sizeToFit()
        if #available(iOS 11.0, *) {
            //searchBar.heightAnchor.constraint(equalToConstant: 44).isActive = true
        }
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //self.tableView.fd_debugLogEnabled = true
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        
        //        self.searchBar.tintColor = .white
        //        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.placeholder = "Search".localizedString()
        searchBar.delegate = self
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
        self.searchBar.showsBookmarkButton = true
        self.searchBar.setImage(UIImage(named: "ic_filter")?.resizeImageWith(newSize: CGSize(width: 24, height: 24)).tint(with: UIColor.white).withRenderingMode(.alwaysOriginal), for: .bookmark, state: .normal)
        
        addBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.addBarButtonItemPressed(_:)))
        addBarButtonItem.setFAIcon(icon: .FAPlus, iconSize: 20)
        self.navigationItem.rightBarButtonItems = [addBarButtonItem]
        
        tableView.keyboardDismissMode = .onDrag
//        filterBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.filterBarButtonItemPress(sender:)))
//        filterBarButtonItem.setFAIcon(icon: .FAFilter, iconSize: 20)
//        self.navigationItem.leftBarButtonItem = filterBarButtonItem
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        //tableView.rowHeight = UITableViewAutomaticDimension
        //tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.navigationItem.title = "SUPPORT LIST".localizedString()
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
        
        self.tableView.alwaysBounceVertical = true
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            if(self?.isSearch == true) {
                self?.tableView!.finishInfiniteScroll()
                return
            }
            self?.loadMoreData() {
                self?.tableView!.finishInfiniteScroll()
            }
            
        }
        
        self.loadTextInput()
        
        //searchBar.sizeToFit()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func addBarButtonItemPressed(_ sender: UIBarButtonItem) {
        if(self.presentedViewController != nil) {
            if let navigation = self.presentedViewController as? UINavigationController {
                navigation.dismiss(animated: true, completion: nil)
                return
            }
        }
        textInputBar.textView.returnKeyType = .done
        textInputBar.isHidden = false
        self.textInputBar.textView.becomeFirstResponder()
    }
    
    @objc func filterBarButtonItemPress(sender: UISearchBar) {
        self.textInputBar.textView.resignFirstResponder()
        ChooseFilterPopover.appearFrom(originView: sender, baseViewController: self, title: "Filter".localizedString(), arrayData: nil, doneAction: {[weak self] array in
                ServiceData.sharedInstance.listFilterSelected = array
            }, cancelAction: {
                print("cancel")
        })
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if(self.presentedViewController != nil) {
            if let navigation = self.presentedViewController as? UINavigationController {
                navigation.dismiss(animated: true, completion: nil)
                return false
            }
        }
        searchBar.returnKeyType = .search
        textInputBar.isHidden = true
        //self.navigationItem.rightBarButtonItem = nil
        //self.navigationItem.leftBarButtonItem = nil
        self.isSearch = true
        //self.tableView.reloadData()
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        self.searchFilter(text: searchBar.text ?? "")
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.text = ""
        self.searchBar.setShowsCancelButton(false, animated: true)
        self.searchBar.resignFirstResponder()
        self.navigationItem.rightBarButtonItem = self.addBarButtonItem
        //self.navigationItem.leftBarButtonItem = self.filterBarButtonItem
        self.isSearch = false
        self.tableView.reloadData()
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
        searchBar.showsCancelButton = true
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
        }
        if let searchBarContainer = self.navigationItem.titleView as? SearchBarContainerView {
            searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
            self.navigationItem.titleView = searchBarContainer
            self.searchBar.sizeToFit()
        }
    }
    
    func searchBarBookmarkButtonClicked(_ searchBar: UISearchBar) {
        self.filterBarButtonItemPress(sender: searchBar)
    }
        
    func searchFilter(text: String) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskGetSupportList(keyword: text, pageIndex: 1).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arraySupport = Array<SupportModel>()
                for item in array {
                    let supportModel = SupportModel(json: item)
                    arraySupport.append(supportModel)
                }
                self.listSearchSupportModel = arraySupport
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    
    @objc func refreshData() {
        getNewData()
    }

    func getNewData() {
        pageIndex = 1
        ServiceData.sharedInstance.taskGetSupportList(pageIndex: pageIndex).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arraySupport = Array<SupportModel>()
                for item in array {
                    let supportModel = SupportModel(json: item)
                    arraySupport.append(supportModel)
                }
                self.listSupportModel = arraySupport
                
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    func loadMoreData(_ handler: (() -> Void)?) {
        
        if(self.listSupportModel.count > 1 && !self.refreshControl.isRefreshing){
            
            pageIndex += 1
            
            ServiceData.sharedInstance.taskGetSupportList(pageIndex: pageIndex).continueOnSuccessWith(continuation: { task in
                
                let result = task as! JSON
                if let array = result.array {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in array {
                        listIndexPath.append(IndexPath(row: self.listSupportModel.count, section: 0))
                        let supportModel = SupportModel(json: item)
                        self.listSupportModel.append(supportModel)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                    self.hideMBProgressHUD(true)
                }
                
                handler?()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                handler?()
            })
            
            
            
            
        } else {
            handler?()
        }
        
        
        
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    func configure(cell: SupportItemTableViewCell, at indexPath: IndexPath) {
        if(isSearch) {
            cell.loadData(supportModel: self.listSearchSupportModel[indexPath.row])
            cell.delegateAction = nil
        } else {
            cell.loadData(supportModel: self.listSupportModel[indexPath.row])
            cell.delegateAction = self
        }
    }
    
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(isSearch) {
            return self.listSearchSupportModel.count
        }
        return self.listSupportModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SupportItemTableViewCell")! as! SupportItemTableViewCell
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "SupportItemTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! SupportItemTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let supportMpdel: SupportModel
        if(isSearch) {
            supportMpdel = self.listSearchSupportModel[indexPath.row]
        } else {
            supportMpdel = self.listSupportModel[indexPath.row]
        }
        self.pushSupportDetailViewController(supportModel: supportMpdel)
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
    func refreshListSupport(support: SupportModel) {
        for index in 0..<self.listSupportModel.count {
            if (self.listSupportModel[index].supportID == support.supportID) {
                if(support.status == .hide || support.status == .removed) {
                    self.listSupportModel.remove(at: index)
                     self.tableView.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                } else {
                    self.listSupportModel[index] = support
                    self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                }
                
                return
            }
        }
    }
    
    
    func confirmDelete(supportModel: SupportModel) {
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
            guard let `self` = self else {
                return
            }
            self.showMBProgressHUD("Update...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskSupportUpdate(action: .remove, supportID: supportModel.supportID).continueOnSuccessWith(continuation: { task in
                
                if let result = task as? JSON {
                    let supportModel = SupportModel(json: result)
                    self.refreshListSupport(support: supportModel)
                }
                self.hideMBProgressHUD(true)
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you sure delete ?".localizedString(), message: "", actions: [yesAction,cancelAction])
    }
    
    //MARK - SupportItemTableViewCellDelegate
    
    func actionSupportItemTableViewCell(action: ActionUpdateSupportType, supportModel: SupportModel) {
        if(action == .remove) {
            self.confirmDelete(supportModel: supportModel)
        } else {
            self.showMBProgressHUD("Update...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskSupportUpdate(action: action, supportID: supportModel.supportID).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    let supportModel = SupportModel(json: result)
                    self.refreshListSupport(support: supportModel)
                }
                self.hideMBProgressHUD(true)
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
        }
        
    }
    
    override func sendSuport(text: String) {
        self.textInputBar.textView.resignFirstResponder()
        self.textInputBar.textView.text = ""
        self.showMBProgressHUD("Add new...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskSupportUpdate(action: .add, supportID: 0, message: text).continueOnSuccessWith(continuation: { task in

            if let result = task as? JSON {
                let supportModel = SupportModel(json: result)
                self.listSupportModel.insert(supportModel, at: 0)
                self.tableView.reloadData()
            }
            
            self.hideMBProgressHUD(true)
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    override func sendSuport(image: UIImage) {
        self.showMBProgressHUD("Add new...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFilemanagerSupportuploadfiles(supportID: 0, fileName: "image-Support.jpg", image: image).continueOnSuccessWith(continuation: { task in
            if let result = task as? JSON {
                let supportModel = SupportModel(json: result)
                self.listSupportModel.insert(supportModel, at: 0)
                self.tableView.reloadData()
            }
            self.hideMBProgressHUD(true)
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    
    }



}
