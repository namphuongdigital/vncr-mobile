//
//  SplashController.swift
//  VNA
//
//  Created by Pham Dai on 01/11/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import LocalAuthentication

class SplashController: BaseViewController {

    @IBOutlet weak var logo: UIImageView!
    @IBOutlet weak var btnFaceId: UIButton!
    @IBOutlet weak var btnRegiter: UIButton!
    
    /// An authentication context stored at class scope so it's available for use during UI updates.
    var context = LAContext()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnRegiter.setTitle("Unable to authenticate? Login with phone number".localizedString(), for: UIControl.State())
        btnRegiter.setTitleColor(.white, for: UIControl.State())
        btnRegiter.titleLabel?.font = .italicSystemFont(ofSize: isIpad ? 16 : 14)
        btnRegiter.titleLabel?.adjustsFontSizeToFitWidth = true
        
        btnFaceId.setTitle(nil, for: UIControl.State())
        if #available(iOS 11.0, *) {
            switch self.context.biometryType {
            case .faceID:
                btnFaceId.setImage(UIImage(named: "ic_faceID_128")?.tint(with: .white), for: UIControl.State())
            case .touchID:
                btnFaceId.setImage(UIImage(named: "ic_touchID_128")?.tint(with: .white), for: UIControl.State())
            default:break
            }
        }
        
        var isLogin = false
        var errorPolicyLA:NSError?
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &errorPolicyLA)
        let shouldShowFaceIDWithDevice = UserDefaults.standard.bool(forKey: .keyEnableFaceID)
        
        // trong truong hop user khong cho phep quyen dung face id
        var shouldUseFaceID = true
        if #available(iOS 11.0, *) {
            if errorPolicyLA?.code == LAError.biometryNotAvailable.rawValue {
                shouldUseFaceID = false
            }
        } else {
            if errorPolicyLA?.code == LAError.touchIDNotAvailable.rawValue {
                shouldUseFaceID = false
            }
        }
        
        if let userModel = ApplicationData.sharedInstance.getLoginUserModel(), userModel.userId > 0 {
            isLogin = true
            ServiceData.sharedInstance.userModel = userModel
            if shouldShowFaceIDWithDevice, shouldUseFaceID {
                embedDeviceOwnerAuthentication(requireSuccess: true)
            } else {
                self.gotoMainApp()
            }
        }
        
        if !isLogin {
            checkingApproval {[weak self] isWaitingApproval in guard let `self` = self else {
                self?.gotoRegister()
                return
            }
                if isWaitingApproval {
                    // go to new screen
                    self.gotoGuest()
                    
                } else {
                    self.gotoRegister()
                }
            }
        }
    }
    
    @IBAction func loginWithFaceId(_ sender: Any) {
        embedDeviceOwnerAuthentication(requireSuccess: true)
    }
    
    @IBAction func gotoRegister(_ sender: Any) {
        gotoRegister()
    }
    
    private func embedDeviceOwnerAuthentication(requireSuccess:Bool) {
        context = LAContext()
        
        let taskGotoMap = {
            self.gotoMainApp()
        }
        
        deviceOwnerAuthentication(context: context) {
            #if DEBUG
            print("deviceOwnerAuthentication with Success")
            #endif
            taskGotoMap()
        } onError: { LACode in
            #if DEBUG
            print("deviceOwnerAuthentication with Error: \(LACode)")
            #endif
            if !requireSuccess {
                taskGotoMap()
            }
            self.btnRegiter.isHidden = false
            self.btnFaceId.isHidden = false
        } onNotAvailable: { isDeviceNotSupported in
            #if DEBUG
            print("deviceOwnerAuthentication with message error: \(isDeviceNotSupported ? "DEVICE NOT SUPPORTED" : "USER DENIED")")
            #endif
            if !requireSuccess {
                taskGotoMap()
            }
            self.btnFaceId.isHidden = false
            self.btnRegiter.isHidden = false
        }
    }
    
    private func gotoRegister() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let navigationRegisterDeviceViewController = UIStoryboard(name: "Main_iphone", bundle: nil).instantiateViewController(withIdentifier: "NavigationRegisterDeviceViewController")
        appDelegate?.window?.rootViewController = navigationRegisterDeviceViewController
        appDelegate?.window?.makeKeyAndVisible()
        navigationRegisterDeviceViewController.view.alpha = 0.0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            navigationRegisterDeviceViewController.view.alpha = 1.0
        })
    }
    
    private func gotoGuest() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let navigationRegisterDeviceViewController = GuestTabbarController()
        appDelegate?.window?.rootViewController = navigationRegisterDeviceViewController
        appDelegate?.window?.makeKeyAndVisible()
        navigationRegisterDeviceViewController.view.alpha = 0.0
        UIView.animate(withDuration: 1.0, animations: { () -> Void in
            navigationRegisterDeviceViewController.view.alpha = 1.0
        })
    }
    
    private func checkingApproval(_ completed:@escaping (Bool)->Void) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskCheckingWaitingApproval()
            .continueOnSuccessWith(continuation: { task in
                self.hideMBProgressHUD(true)
#if DEBUG
print("\(task)")
#endif
                if let result = task as? JSON {
                    let isWaitingApproval = result["IsWaitingApproval"].boolValue
                    let token = result["Token"].stringValue
                    let userModel = UserModel()
                    userModel.userId = 0
                    userModel.token = token
                    userModel.isGuest = true
                    ServiceData.sharedInstance.userModel = userModel
                    ApplicationData.sharedInstance.updateUserLogin(userModel: userModel)
                    completed(isWaitingApproval)
                }
            }).continueOnErrorWith(continuation: { error in
                self.hideMBProgressHUD(true)
                completed(false)
            })
    }
}
