//
//  AssessmentCrewDutyViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
//import FTPopOverMenu
import SwiftyJSON

class AssessmentCrewDutyViewController: BaseViewController, NewAssessmentItemTableViewCellDelegate, UITableViewDelegate, UITableViewDataSource, EditContentViewControllerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var sendBarButtonItem: UIBarButtonItem!
    
    var crewDutyModel: CrewDutyModel!
    
    var assessmentModel: AssessmentModel!
    
    var totalScore: Int = 0
    
    var isEditStrength: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "ASSESSMENT".localizedString()
        
        var nib = UINib(nibName: "NewAssessmentItemTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "NewAssessmentItemTableViewCell")
        nib = UINib(nibName: "DynamicHeightCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "DynamicHeightCell")
        
//        if(UIDevice.current.userInterfaceIdiom == .phone) {
            nib = UINib(nibName: "InfoCrewDutyTableViewCell_iphone", bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: "InfoCrewDutyTableViewCell")
//        } else {
//            nib = UINib(nibName: "InfoCrewDutyTableViewCell", bundle: nil)
//            tableView.register(nib, forCellReuseIdentifier: "InfoCrewDutyTableViewCell")
//        }
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tableView.tableHeaderView = UIView(frame: frame)
        
        sendBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.sendBarButtonItemPressed(_:)))
        sendBarButtonItem.setFAIcon(icon: .FACheckCircle, iconSize: 20)
        self.navigationItem.rightBarButtonItems = [self.sendBarButtonItem]
        totalScore = self.crewDutyModel.totalScore
        self.getNewData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func sendBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.updateAllToServer()
    }
    

    func getNewData() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightAssessmentgetinfo(flightId: crewDutyModel.flightID, destinationCrewID: crewDutyModel.crewID, destinationJob: crewDutyModel.foJob).continueOnSuccessWith(continuation: { task in
            #if DEBUG
            print("\(task) \(#function)")
            #endif
            if let result = task as? JSON {
                let assessmentModel = AssessmentModel(json: result)
                self.assessmentModel = assessmentModel
                self.hideMBProgressHUD(true)
                self.loadViewData()
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    func updateAllToServer() {
        
        let strength = self.assessmentModel.strength
        let weaknes = self.assessmentModel.weakness
        
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightAssessmentUpdateInfo(flightId: self.assessmentModel.flightID, destinationCrewID: crewDutyModel.crewID, destinationJob: crewDutyModel.foJob, lessonID: self.assessmentModel.lessonID, assessmentItems: self.assessmentModel.assessmentItems, strength: strength, weaknes: weaknes).continueOnSuccessWith(continuation: { task in
//            #if DEBUG
//            print("\(task) \(#function)")
//            #endif
            let result = task as! JSON
            let crewDutyModel = CrewDutyModel(json: result)
            Broadcaster.notify(CrewDutyModelDelete.self) {
                $0.refreshCrewDuty(crewDutyModel)
            }
            
            self.hideMBProgressHUD(true)
            let _ = self.navigationController?.popViewController(animated: true)
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func loadViewData() {
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "CREATE ASSESSMENT".localizedString()
        /*
        var nibInfoCrewDutyTableViewCell = "InfoCrewDutyTableViewCell"
        var nibStrengthReportTableViewCell = "ContentReportTableViewCell"
        var nibWeaknessReportTableViewCell = "ContentReportTableViewCell"
        var nibUpdateReportTableViewCelll = "UpdateReportTableViewCell"
        var nibAssessmentItemTableViewCell = "AssessmentItemTableViewCell"
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            nibInfoCrewDutyTableViewCell = String(format: "%@_iphone", nibInfoCrewDutyTableViewCell)
            nibStrengthReportTableViewCell = String(format: "%@_iPhone", nibStrengthReportTableViewCell)
            nibWeaknessReportTableViewCell = String(format: "%@_iPhone", nibWeaknessReportTableViewCell)
            nibUpdateReportTableViewCelll = String(format: "%@_iPhone", nibUpdateReportTableViewCelll)
            nibAssessmentItemTableViewCell = String(format: "%@_iphone", nibAssessmentItemTableViewCell)
            
        }
        var arrayRowFormer = [RowFormer]()
        infoCrewDutyTableViewCell = CustomRowFormer<InfoCrewDutyTableViewCell>(instantiateType: .Nib(nibName: nibInfoCrewDutyTableViewCell)) {[weak self] in
            guard let `self` = self else {
                return
            }
            $0.loadData(crewDutyModel: self.crewDutyModel, totalScore: self.totalScore)
            print($0.textLabel?.text)
            }.configure {
                if(UIDevice.current.userInterfaceIdiom == .phone) {
                    $0.rowHeight = 110
                } else {
                    $0.rowHeight = 180
                }
                
        }
        arrayRowFormer.append(infoCrewDutyTableViewCell)
        
       
        for item in self.assessmentModel.assessmentItems {
            let cell = CustomRowFormer<AssessmentItemTableViewCell>(instantiateType: .Nib(nibName: nibAssessmentItemTableViewCell)) {[weak self] in
                print($0.textLabel?.text)
                //$0.loadData(assessmentItemModel: item)
                }.configure {
                    if(UIDevice.current.userInterfaceIdiom == .phone) {
                        $0.rowHeight = 90
                    } else {
                        $0.rowHeight = 80
                    }
                    
            }
            (cell.cellInstance as? AssessmentItemTableViewCell)?.loadData(assessmentItemModel: item)
            (cell.cellInstance as? AssessmentItemTableViewCell)?.delegate = self
            arrayRowFormer.append(cell)
        }
        
        strengthCrewDutyTableViewCell = CustomRowFormer<ContentReportTableViewCell>(instantiateType: .Nib(nibName: nibStrengthReportTableViewCell)) {[weak self] in
            print($0.textLabel?.text)
            $0.textViewContent.text = self?.assessmentModel.strength
            $0.textViewContent.placeholder = "Enter text".localizedString()
            $0.labelContent.text = "Strength".localizedString()
            //$0.textViewContent.textColor = UIColor.gray
            //$0.textViewContent.text = self?.taskModel.descriptionContent
            //$0.textViewContent.isEditable = false
            }.configure {
                if(UIDevice.current.userInterfaceIdiom == .phone) {
                    $0.rowHeight = 125
                } else {
                    $0.rowHeight = 150
                }
        }
        arrayRowFormer.append(strengthCrewDutyTableViewCell)
        
        weaknessCrewDutyTableViewCell = CustomRowFormer<ContentReportTableViewCell>(instantiateType: .Nib(nibName: nibStrengthReportTableViewCell)) {[weak self] in
            print($0.textLabel?.text)
            $0.textViewContent.text = self?.assessmentModel.weakness
            $0.textViewContent.placeholder = "Enter text".localizedString()
            $0.labelContent.text = "Weakness".localizedString()
            //$0.textViewContent.textColor = UIColor.gray
            //$0.textViewContent.text = self?.taskModel.descriptionContent
            //$0.textViewContent.isEditable = false
            }.configure {
                if(UIDevice.current.userInterfaceIdiom == .phone) {
                    $0.rowHeight = 125
                } else {
                    $0.rowHeight = 150
                }
        }
        arrayRowFormer.append(weaknessCrewDutyTableViewCell)
        
        updateCrewDutyTableViewCell = CustomRowFormer<UpdateReportTableViewCell>(instantiateType: .Nib(nibName: nibUpdateReportTableViewCelll)) {[weak self] in
            print($0.textLabel?.text)
            //$0.buttonUpdate.addTarget(self, action: #selector(self?.buttonUpdateReportPress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        let createHeader: ((String, _ height: CGFloat) -> ViewFormer) = { text, height in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor.clear
                $0.titleLabel.font = UIFont.systemFont(ofSize: 14)
                }.configure {
                    $0.viewHeight = height
                    $0.text = text
                    
            }
        }
        
        
        let customRowFormerSection = SectionFormer(rowFormers: arrayRowFormer).set(headerViewFormer: createHeader("", 0))
        /*
         let customRowFormerSection2 = SectionFormer(rowFormer: updateReportTableViewCell).set(headerViewFormer: createHeader("", 40))
         */
        former.append(sectionFormer: customRowFormerSection)
        self.tableView.separatorStyle = .none
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.01))
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        */
        tableView.reloadData()
    }
    
    func configure(dynamicHeightCell: DynamicHeightCell, at indexPath: IndexPath) {
        if(indexPath.row == 0) {
            dynamicHeightCell.title = "Strength".localizedString()
            dynamicHeightCell.body = self.assessmentModel.strength
            dynamicHeightCell.clearButton.addTarget(self, action: #selector(self.clearButtonStrengthPress), for: .touchUpInside)
        } else {
            dynamicHeightCell.title = "Weakness".localizedString()
            dynamicHeightCell.body = self.assessmentModel.weakness
            dynamicHeightCell.clearButton.addTarget(self, action: #selector(self.clearButtonWeaknessPress), for: .touchUpInside)
        }
        
    }
    
    func configure(newAssessmentItemTableViewCell: NewAssessmentItemTableViewCell, at indexPath: IndexPath) {
        newAssessmentItemTableViewCell.assessmentItemModel = self.assessmentModel.assessmentItems[indexPath.row]
        newAssessmentItemTableViewCell.newAssessmentItemTableViewCellDelegate = self
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.assessmentModel == nil) {
            return 0
        }
        if(section == 0) {
            return 1
        } else if(section == 1) {
            return self.assessmentModel.assessmentItems.count
        } else {
            return 2
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCrewDutyTableViewCell")! as! InfoCrewDutyTableViewCell
            cell.loadData(crewDutyModel: self.crewDutyModel, totalScore: self.totalScore)
            return cell
        } else if (indexPath.section == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NewAssessmentItemTableViewCell")! as! NewAssessmentItemTableViewCell
            self.configure(newAssessmentItemTableViewCell: cell, at: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DynamicHeightCell")! as! DynamicHeightCell
            self.configure(dynamicHeightCell: cell, at: indexPath)
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0) {
            if(UIDevice.current.userInterfaceIdiom == .phone) {
                return 110
            } else {
                return 180
            }
        } else if (indexPath.section == 1) {
            return tableView.fd_heightForCell(withIdentifier: "NewAssessmentItemTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
                let cell = cell as! NewAssessmentItemTableViewCell
                self?.configure(newAssessmentItemTableViewCell: cell, at: indexPath)
            })
        } else {
            return tableView.fd_heightForCell(withIdentifier: "DynamicHeightCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
                let cell = cell as! DynamicHeightCell
                self?.configure(dynamicHeightCell: cell, at: indexPath)
            })
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(indexPath.section == 2) {
            if(indexPath.row == 0) {
                selectStrengthPress()
            } else {
                selectWeaknessPress()
            }
        }
        
    }
    
    func selectStrengthPress() {
        self.isEditStrength = true
        self.pushEditContentViewController(title: "Strength".localizedString(), content: self.assessmentModel.strength, delegate: self)
    }
    
    func selectWeaknessPress() {
        self.isEditStrength = false
        self.pushEditContentViewController(title: "Weakness".localizedString(), content: self.assessmentModel.weakness, delegate: self)
    }
 
    
    func updateTotalScore() {
        var total: Float = 0
        for item in self.assessmentModel.assessmentItems {
            total = total + Float(item.score)
        }
        total = total / Float(self.assessmentModel.assessmentItems.count)
        total.round()
        totalScore = Int(total)
        self.tableView.reloadRows(at: [IndexPath.init(row: 0, section: 0)], with: .automatic)
    }
    
    
    //MARK - NewAssessmentItemTableViewCellDelegate
    func scoreButtonPressNewAssessmentItemTableViewCell(cell: NewAssessmentItemTableViewCell, assessmentItemModel: AssessmentItemModel) {
        let arrayString = assessmentItemModel.Categories.compactMap({$0.Name})
        //, menuType: FTPopOverMenuType.square
        //FTPopOverMenu.show(forSender: cell.buttonScore, withMenuArray: arrayString, doneBlock: {[weak self] (index) in
        FTPopOverMenu.show(forSender: cell.buttonScore, withMenu: arrayString, menuType: FTPopOverMenuType.square, doneBlock: {[weak self] (index) in
            
            guard let `self` = self else {
                return
            }
            let rate = arrayString[index]
            assessmentItemModel.rate = rate
//            assessmentItemModel.score = index
            cell.buttonScore.setTitle(String(format: "%@", rate), for: UIControl.State())
            self.updateTotalScore()
            }, dismiss: {
                print("")
        })
    }
    
    //MARK - EditContentViewControllerDelegate
    func editContentViewControllerSave(viewController: UIViewController, content: String) {
        
        if(self.isEditStrength) {
            self.assessmentModel.strength = content
            self.tableView.reloadRows(at: [IndexPath.init(item: 0, section: 2)], with: .automatic)
            
        } else {
            self.assessmentModel.weakness = content
            self.tableView.reloadRows(at: [IndexPath.init(item: 1, section: 2)], with: .automatic)
        }
        
        viewController.navigationController?.popViewController(animated: true)
    }
    
    //
    @objc func clearButtonStrengthPress(sender: UIButton) {
        self.assessmentModel.strength = ""
        self.tableView.reloadRows(at: [IndexPath.init(item: 0, section: 2)], with: .automatic)
        
    }
    
    @objc func clearButtonWeaknessPress(sender: UIButton) {
        self.assessmentModel.weakness = ""
        self.tableView.reloadRows(at: [IndexPath.init(item: 1, section: 2)], with: .automatic)
    }
}
