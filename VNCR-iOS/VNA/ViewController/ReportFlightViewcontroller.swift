//
//  ReportFlightViewcontroller.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/12/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import StepSlider
import UIImage_ImageCompress
import SwiftyJSON
import Photos
import PhotosUI

enum UITextFieldTag: Int {
    case main = 1
    case second = 2
}

class ReportFlightViewcontroller: FormViewController, UITextFieldDelegate, ListImagePostTableViewCellDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, EditContentViewControllerDelegate {
    
    let imagePicker = UIImagePickerController()
    
    weak var scheduleFlyModel: ScheduleFlightModel!
    
    var finalReportModel: ReportModel?
    
    //var infoReportTableViewCell: CustomRowFormer<InfoReportTableViewCell>!
    
    var mainPilotReportTableViewCell: CustomRowFormer<MainPilotReportTableViewCell>!
    
    var secondPilotReportTableViewCell: CustomRowFormer<SecondPilotReportTableViewCell>!
    
    var emergencyReportTableViewCell: CustomRowFormer<EmergencyReportTableViewCell>!
    
    var categoriesReportTableViewCell: CustomRowFormer<DynamicHeightCell>!
    
    var contentReportTableViewCell: CustomRowFormer<DynamicHeightCell>!
    
    var sectionFormer: SectionFormer!
    
    var updateReportTableViewCell: CustomRowFormer<UpdateReportTableViewCell>!
    
    var emergency: EmergencyType = .slow
    
    var textContent: String = ""
    
    var textMainPilot: String = ""
    
    var textSecondPilot: String = ""
    
    var sendBarButtonItem: UIBarButtonItem!
    
    weak var selectedImagePost: ImagePost?

    override func viewDidLoad() {
        super.viewDidLoad()

        sendBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.sendBarButtonItemPressed(_:)))
        sendBarButtonItem.setFAIcon(icon: .FACheckCircle, iconSize: 20)
        
        self.loadData()
    }
    
    @objc func sendBarButtonItemPressed(_ sender: UIBarButtonItem) {
        buttonUpdateReportPress(sender: nil)
    }
    
    func loadViewData() {
        
        // Do any additional setup after loading the view.
        
        self.textTitle = "\(scheduleFlyModel.flightNo)/\(scheduleFlyModel.departedDate) \(scheduleFlyModel.routing)"
        
        var stringNameCategory = ""
        for item in self.finalReportModel!.categories {
            for sub in item.subCatetories {
                if(sub.isSelected) {
                    if(stringNameCategory.count > 0) {
                        stringNameCategory = String(format: "%@ - %@", stringNameCategory, sub.subCategoryName)
                    } else {
                        stringNameCategory = String(format: "%@", sub.subCategoryName)
                    }
                    
                }
            }
            
        }
        if(stringNameCategory.isEmpty) {
            stringNameCategory = "\n\n"
        }
        
        if(self.finalReportModel?.id == 0) {
            self.navigationItem.title = "CREATE REPORT".localizedString()
        } else {
            self.navigationItem.title = "UPDATE REPORT".localizedString()
        }
        
        var nibNameInfoReportTableViewCell = "InfoReportTableViewCell"
        var nibNameMainPilotReportTableViewCell = "MainPilotReportTableViewCell"
        var nibNameSecondPilotReportTableViewCell = "SecondPilotReportTableViewCell"
        var nibNameEmergencyReportTableViewCell = "EmergencyReportTableViewCell"
        var nibNameCategoriesReportTableViewCell = "CategoriesReportTableViewCell"
        var nibNameContentReportTableViewCell = "ContentReportTableViewCell"
        var nibNameUpdateReportTableViewCell = "UpdateReportTableViewCell"
        
        //if(UIDevice.current.userInterfaceIdiom == .phone) {
            nibNameInfoReportTableViewCell = String(format: "%@_iPhone", nibNameInfoReportTableViewCell)
            nibNameMainPilotReportTableViewCell = String(format: "%@_iPhone", nibNameMainPilotReportTableViewCell)
            nibNameSecondPilotReportTableViewCell = String(format: "%@_iPhone", nibNameSecondPilotReportTableViewCell)
            nibNameEmergencyReportTableViewCell = String(format: "%@_iPhone", nibNameEmergencyReportTableViewCell)
            nibNameCategoriesReportTableViewCell = String(format: "%@_iPhone", nibNameCategoriesReportTableViewCell)
            nibNameContentReportTableViewCell = String(format: "%@_iPhone", nibNameContentReportTableViewCell)
            nibNameUpdateReportTableViewCell = String(format: "%@_iPhone", nibNameUpdateReportTableViewCell)
            
        //}
        
        
        
        mainPilotReportTableViewCell = CustomRowFormer<MainPilotReportTableViewCell>(instantiateType: .Nib(nibName: nibNameMainPilotReportTableViewCell)) {[weak self] in
            $0.pilotNameTextFiled.delegate = self
            $0.pilotNameTextFiled.text = self?.textMainPilot
            $0.pilotNameTextFiled.tag = UITextFieldTag.main.rawValue
            //$0.pilotNameTextFiled.willMove(toSuperview: self?.tableView)
            //$0.textViewContent.textColor = UIColor.gray
            //$0.textViewContent.text = self?.taskModel.descriptionContent
            //$0.textViewContent.isEditable = false
            }.configure {
                if(UIDevice.current.userInterfaceIdiom == .pad) {
                    $0.rowHeight = 80
                } else {
                    $0.rowHeight = 50
                }
        }
        
        emergencyReportTableViewCell = CustomRowFormer<EmergencyReportTableViewCell>(instantiateType: .Nib(nibName: nibNameEmergencyReportTableViewCell)) {[weak self] in
            
            let p = UInt(self?.emergency.rawValue ?? 0)
            $0.sliderView.index = p
            $0.sliderView.addTarget(self, action: #selector(self?.sliderViewChange(sender:)), for: .valueChanged)
            
            }.configure {
                if(UIDevice.current.userInterfaceIdiom == .pad) {
                    $0.rowHeight = 120
                } else {
                    $0.rowHeight = 80
                }
                
        }
        
        categoriesReportTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {
            $0.title = "Categories".localizedString()
            $0.body = stringNameCategory
            //$0.accessoryType = .disclosureIndicator
            }.configure {
                $0.rowHeight = UITableView.automaticDimension
            }.onSelected({[weak self] (row) in
                print("")
                self?.buttonSelectCategoriesPress(sender: row.cell)
            })
        contentReportTableViewCell = CustomRowFormer<DynamicHeightCell>(instantiateType: .Nib(nibName: "DynamicHeightCell")) {[weak self] in
            $0.title = "Content".localizedString()
            $0.body = self?.textContent
            //$0.accessoryType = .disclosureIndicator
            }.configure {
                $0.rowHeight = UITableView.automaticDimension
            }.onSelected({[weak self] (row) in
                print("")
                self?.selectContentPress()
            })
       
        var array = Array<ImagePost>()
        var image = ImagePost(named: "")
        image.index = 0
        array.append(image)
        image = ImagePost(named: "")
        image.index = 1
        array.append(image)
        image = ImagePost(named: "")
        image.index = 2
        array.append(image)
        image = ImagePost(named: "")
        image.index = 3
        array.append(image)
        image = ImagePost(named: "")
        image.index = 4
        array.append(image)
        
        if let attachments = self.finalReportModel?.attachments {
            for index in 0..<attachments.count {
                if(index == 4) {
                    break
                } else {
                    array[index].imageUrl = attachments[index].thumbnailPath
                    array[index].imageFullUrl = attachments[index].downloadPath
                }
            }
        }
        
        let listImagePostTableViewCell = CustomViewFormer<ListImagePostTableViewCell>(instantiateType: .Nib(nibName: "ListImagePostTableViewCell")) {
            $0.loadContent(listImagePost: array)
            $0.delegate = self
            }.configure { view in
                view.viewHeight = 100
        }
        
        let empety = CustomRowFormer<InfoReportTableViewCell>(instantiateType: .Nib(nibName: nibNameInfoReportTableViewCell)) {
            print($0.labelC)
            
            }.configure {
                $0.rowHeight = 0
        }
        
        sectionFormer = SectionFormer(rowFormer: empety).set(headerViewFormer: listImagePostTableViewCell)
        sectionFormer.remove(atIndex: 0)
        
        updateReportTableViewCell = CustomRowFormer<UpdateReportTableViewCell>(instantiateType: .Nib(nibName: nibNameUpdateReportTableViewCell)) {[weak self] in
            //print($0.textLabel?.text)
            $0.buttonUpdate.addTarget(self, action: #selector(self?.buttonUpdateReportPress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        let createHeader: ((String, _ height: CGFloat) -> ViewFormer) = { text, height in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor.clear
                $0.titleLabel.font = UIFont.systemFont(ofSize: 14)
                }.configure {
                    $0.viewHeight = height
                    $0.text = text
                    
            }
        }
        
        let customRowFormerSection = SectionFormer(rowFormer: categoriesReportTableViewCell, emergencyReportTableViewCell, contentReportTableViewCell).set(headerViewFormer: createHeader("", 0))
        /*
        let customRowFormerSection2 = SectionFormer(rowFormer: updateReportTableViewCell).set(headerViewFormer: createHeader("", 40))
        */
        former.append(sectionFormer: customRowFormerSection, sectionFormer)
        self.tableView.separatorStyle = .none
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.01))
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
        
        if(self.finalReportModel?.isReadOnly == true) {
            self.tableView.isUserInteractionEnabled = false
        } else {
            self.navigationItem.rightBarButtonItems = [self.sendBarButtonItem]
        }
    }
    
    func loadData() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        
        let group = DispatchGroup()
        
        if(ServiceData.sharedInstance.listMainPilot == nil) {
            group.enter()
            ServiceData.sharedInstance.taskFlightPilotGetList(position: .main).continueOnSuccessWith(continuation: { task in
                print("")
                group.leave()
            }).continueOnErrorWith(continuation: { error in
                print(error)
                group.leave()
            })
        }
        
        if(finalReportModel == nil) {
            group.enter()
            ServiceData.sharedInstance.taskFlightFinalReportGetInfo(flightId: scheduleFlyModel.flightID).continueOnSuccessWith(continuation: { task in
                
                if let result = task as? JSON {
                    let reportModel = ReportModel(json: result)
                    self.finalReportModel = reportModel
                }
                
                group.leave()
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                group.leave()
            })
            
        } else {
            if(finalReportModel?.id == 0) {
                group.enter()
                ServiceData.sharedInstance.taskFlightFinalReportGetInfo(flightId: scheduleFlyModel.flightID).continueOnSuccessWith(continuation: { task in
                    
                    if let result = task as? JSON {
                        let reportModel = ReportModel(json: result)
                        self.finalReportModel = reportModel
                        self.textMainPilot = self.finalReportModel?.mainPilot ?? ""
                        self.textSecondPilot = self.finalReportModel?.secondPilot ?? ""
                        self.textContent = self.finalReportModel?.content ?? ""
                        self.emergency = self.finalReportModel?.emergency ?? .slow
                        self.loadViewData()
                    }
                    group.leave()
                }).continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    group.leave()
                })
            } else {
                finalReportModel?.flightID = self.scheduleFlyModel.flightID
                self.textMainPilot = self.finalReportModel?.mainPilot ?? ""
                self.textSecondPilot = self.finalReportModel?.secondPilot ?? ""
                self.textContent = self.finalReportModel?.content ?? ""
                self.emergency = self.finalReportModel?.emergency ?? .slow
                self.loadViewData()
            }
        }
        
        group.notify(queue: .main) {[weak self] in
            guard let `self` = self else { return }
            self.hideMBProgressHUD(true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func uploadImageBackground(image: UIImage, attachmentGroupID: Int, finalReportId: Int) {
        self.showMBProgressHUD("Uploading...".localizedString(), animated: true)
         ServiceData.sharedInstance.taskBackgroundFilemanagerFlightreportuploadfiles(finalReportID: finalReportId, flightId: self.finalReportModel!.flightID, attachmentGroupID: attachmentGroupID, image: image, imageName: "image-test.jpg").continueOnSuccessWith(continuation: { task in
            if let attachmentModel = task as? AttachmentModel {
                if(self.finalReportModel?.attachmentGroupID == 0) {
                    self.finalReportModel?.attachmentGroupID = attachmentModel.attachmentID
                    self.finalReportModel?.attachments.append(attachmentModel)
                    self.updateReport(isPopToRoot: false)
                } else {
                    self.finalReportModel?.attachmentGroupID = attachmentModel.attachmentID
                    self.finalReportModel?.attachments.append(attachmentModel)
                    self.hideMBProgressHUD(true)
                }
                
                if let cell = self.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                    self.selectedImagePost?.image = image
                    self.selectedImagePost?.isImagePost = true
                    cell.reloadData()
                    
                }
                
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        
    }
    
    func uploadImage(image: UIImage, attachmentGroupID: Int, finalReportId: Int) {
        self.showMBProgressHUD("Uploading...".localizedString(), animated: true)
        
        ServiceData.sharedInstance.taskFilemanagerFlightreportuploadfiles(finalReportID: finalReportId, flightId: self.finalReportModel!.flightID, attachmentGroupID: attachmentGroupID, image: image, imageName: "image-test.jpg").continueOnSuccessWith(continuation: { task in
            if let attachmentModel = task as? AttachmentModel {
                if(self.finalReportModel?.attachmentGroupID == 0) {
                    self.finalReportModel?.attachmentGroupID = attachmentModel.attachmentID
                    self.finalReportModel?.attachments.append(attachmentModel)
                    //self.updateReport(isPopToRoot: false)
                    self.hideMBProgressHUD(true)
                } else {
                    self.finalReportModel?.attachmentGroupID = attachmentModel.attachmentID
                    self.finalReportModel?.attachments.append(attachmentModel)
                    self.hideMBProgressHUD(true)
                }
                
                if let cell = self.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                    self.selectedImagePost?.image = image
                    self.selectedImagePost?.isImagePost = true
                    cell.reloadData()
                    
                }
                
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        
    }
    
    @objc func sliderViewChange(sender: StepSlider) {
        
        self.emergency = EmergencyType(rawValue: Int(sender.index))!
        print("%f", self.emergency)
        
    }
    
    @objc func buttonUpdateReportPress(sender: UIButton?) {
        self.updateReport()
    }
    
    func updateReport(isPopToRoot: Bool = true) {
        guard let finalReportModel = self.finalReportModel else {
            return
        }
        
        self.textContent = (contentReportTableViewCell.cell as DynamicHeightCell).body ?? ""
        finalReportModel.content = self.textContent
        finalReportModel.mainPilot = self.textMainPilot
        finalReportModel.secondPilot = self.textSecondPilot
        finalReportModel.emergency = self.emergency
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightFinalReportUpdateInfo(finalReport: finalReportModel).continueOnSuccessWith(continuation: { task in
            
            if let result = task as? JSON {
                let reportModel = ReportModel(json: result)
                self.finalReportModel = reportModel
                Broadcaster.notify(UpdateFlightReportDelegate.self) {
                    $0.refreshFlightReporte(report: reportModel)
                }
            }
            self.hideMBProgressHUD(true)
            if(isPopToRoot == true) {
                let _ = self.navigationController?.popViewController(animated: true)
            }
            
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - ListImagePostTableViewCellDelegate
    
    func addImagePostCellPress(imagePost: ImagePost) {
        self.selectedImagePost = imagePost
        showActionSheetSelectImage()
        /*
        let cell = sectionFormer.headerViewFormer!.viewInstance as! ListImagePostTableViewCell
        
        self.chooseImageWrapper?.showImagePicker(selected: cell.getImagePostNotSeletedCount())
        */
    }
    
    
    func didSelectCellPress(viewController: UIViewController) {
        present(viewController, animated: true, completion: {})
    }
    
    func selectedImagePostCellPress(imagePost: ImagePost) {
        /*
        let cell = sectionFormer.headerViewFormer!.viewInstance as! ListImagePostTableViewCell
        self.chooseImageWrapper?.showEditImage(image: imagePost.image!, finishEditImage: { (image) in
            imagePost.image = image.scaleImageToSize(newSize: CGSize(width: 700, height: 525))
            cell.reloadData()
        })
        */
    }
    
    func removeImagePostCellPress(imagePost: ImagePost, index: Int) {
        
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
            guard let `self` = self else {
                return
            }
            let attachment = self.finalReportModel!.attachments[index]
            self.showMBProgressHUD("Deleting...", animated: true)
            ServiceData.sharedInstance.taskFilemanagerDeletefiles(fileId: attachment.fileID).continueOnSuccessWith(continuation: { task in
                self.finalReportModel!.attachments.remove(at: index)
                self.hideMBProgressHUD(true)
                if let cell = self.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                    imagePost.isImagePost = false
                    cell.reloadData()
                }
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you want delete this file ?".localizedString(), message: "", actions: [yesAction,cancelAction])
        
        
    }
    
    
    func selectContentPress() {
        self.pushEditContentViewController(title: "Report content".localizedString(), content: self.textContent, delegate: self)
    }
    
    
    func buttonSelectCategoriesPress(sender: UIView) {
        
        if let view = sender.superview?.superview {
            let rectInSuperview = self.tableView.convert(sender.frame, to: self.tableView.superview)
            let viewRect = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 64))
            //let viewRect = UIView(frame: rectInSuperview)
            var array = Array<CategoryModel>()
            for item in self.finalReportModel!.categories {
                array.append(CategoryModel(categoryModel: item))
            }
            ReasonCategoriesPopover.appearFrom(originView: viewRect, baseViewController: self, title: "Categories".localizedString(), arrayData: array, doneAction: {[weak self] array in
                self?.finalReportModel?.categories = array 
                    self?.categoriesReportTableViewCell.update({[weak self] (item) in
                        guard let `self` = self else {
                            return
                        }
                        var stringName = ""
                        for item in self.finalReportModel!.categories {
                            for sub in item.subCatetories {
                                if(sub.isSelected) {
                                    if(stringName.count > 0) {
                                        stringName = String(format: "%@ - %@", stringName, sub.subCategoryName)
                                    } else {
                                        stringName = String(format: "%@", sub.subCategoryName)
                                    }
                                    
                                }
                            }
                            
                        }
                        if(stringName.isEmpty) {
                            stringName = "\n\n"
                        }
                        item.cellUpdate({[weak self] (cell) in
                            cell.body = stringName
                            self?.tableView.reloadData()
                        })
                        
                    })
                }, cancelAction: {
                    print("cancel")
            })
        }
        
        
    }
    
    
    //
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        if let cell = textField.superview?.superview as? UITableViewCell {
            if let indexPath = self.tableView.indexPath(for: cell) {
                let rectCell = self.tableView.rectForRow(at: indexPath)
                let rectOfCellInSuperview = self.tableView.convert(rectCell, to: tableView.superview)
                if let autoCompleteTextField = textField as? AutoCompleteTextField {
                    autoCompleteTextField.autoCompleteTableView?.removeFromSuperview()
                    
                    autoCompleteTextField.autoCompleteTableView?.frame = CGRect(x: autoCompleteTextField.autoCompleteTableView!.frame.origin.x, y: rectOfCellInSuperview.origin.y - autoCompleteTextField.autoCompleteTableView!.frame.size.height, width: autoCompleteTextField.autoCompleteTableView!.frame.size.width + 20, height: autoCompleteTextField.autoCompleteTableView!.frame.size.height)
                    self.view.addSubview(autoCompleteTextField.autoCompleteTableView!)
                }
                
                print(rectOfCellInSuperview)
            }
            
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if(textField.tag == UITextFieldTag.main.rawValue) {
            self.textMainPilot = textField.text ?? ""
        } else if(textField.tag == UITextFieldTag.second.rawValue) {
            self.textSecondPilot = textField.text ?? ""
        }
        if let autoCompleteTextField = textField as? AutoCompleteTextField {
            autoCompleteTextField.autoCompleteTableView?.removeFromSuperview()
        }
    }
    
    
    func takeImage() {
        
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    //
    
    func showActionSheetSelectImage() {
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.popover
            if let cell = self.sectionFormer.headerViewFormer?.viewInstance as? ListImagePostTableViewCell {
                imagePicker.popoverPresentationController?.sourceView = cell
            } else {
                imagePicker.popoverPresentationController?.sourceView = self.view
            }
            
        }
        
        
        var arrayAction = Array<UIAlertAction>()
        
        let takePhotoAction = UIAlertAction(title: "Camera".localizedString(), style: UIAlertAction.Style.default, handler: {[weak self] (UIAlertAction) -> Void in
            self?.takeImage()
        })
        arrayAction.append(takePhotoAction)
        
        let chooseFromLibAction = UIAlertAction(title: "Library".localizedString(), style: UIAlertAction.Style.default, handler: {[weak self] (UIAlertAction) -> Void in
            if #available(iOS 14, *) {
                var config = PHPickerConfiguration()
                config.selectionLimit = 1
                config.filter = PHPickerFilter.images
                let pickerViewController = PHPickerViewController(configuration: config)
                pickerViewController.delegate = self
                self?.present(pickerViewController, animated: true, completion: nil)
            } else {
                self?.imagePicker.delegate = self
                self?.imagePicker.allowsEditing = false
                self?.imagePicker.sourceType = .photoLibrary
                self?.present(self!.imagePicker, animated: true, completion: nil)
            }
        })
        arrayAction.append(chooseFromLibAction)
        
        let cancelAction = UIAlertAction(title: "Cancel".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        arrayAction.append(cancelAction)
        
        self.showActionSheetWithAction("Upload image report".localizedString(), message: nil, actions: arrayAction)
        
        
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let newImg = UIImage.compressImage(pickedImage, compressRatio: 0.7)
            self.uploadImage(image: newImg!, attachmentGroupID: self.finalReportModel!.attachmentGroupID, finalReportId: self.finalReportModel?.flightID ?? 0)
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    //MARK - EditContentViewControllerDelegate
    func editContentViewControllerSave(viewController: UIViewController, content: String) {
        
        self.textContent = content
        self.contentReportTableViewCell.update({[weak self] (row) in
            guard let `self` = self else {
                return
            }
            
            row.cellUpdate({[weak self] (cell) in
                cell.body = content
                self?.tableView.reloadData()
            })
            
        })
        
        self.tableView.reloadData()
        viewController.navigationController?.popViewController(animated: true)
    }
    

}

extension UILabel {
    var optimalHeight : CGFloat {
        get  {
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: CGFloat.greatestFiniteMagnitude))
            label.numberOfLines = 0
            label.lineBreakMode = self.lineBreakMode
            label.font = self.font
            label.text = self.text
            
            label.sizeToFit()
            
            return label.frame.height
        }
    }
}

@available(iOS 14, *)
extension ReportFlightViewcontroller: PHPickerViewControllerDelegate {
    func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
       picker.dismiss(animated: false, completion: nil)
       
        var datas:[Data] = []
        var images:[UIImage] = []
        var localError:Error? = nil
        let group = DispatchGroup()
        for result in results {
            group.enter()
            if result.itemProvider.canLoadObject(ofClass: UIImage.self) {
                result.itemProvider.loadObject(ofClass: UIImage.self, completionHandler: { (object, error) in
                    localError = error
                    if let image = object as? UIImage {
                        if var imageData = image.jpegData(compressionQuality: 1) {
                            if imageData.count > 20*1024 {
                                imageData = image.jpegData(compressionQuality: 0.7)!
                            }
                            datas.append(imageData)
                        }
                        images.append(image)
                    }
                    group.leave()
                })
            } else {
                group.leave()
            }
  
        }
        
        group.notify(queue: .main) {[weak self] in
            guard let `self` = self else {return}
            if let error = localError {
                self.showAlert("Alert".localizedString(), stringContent: error.localizedDescription)
            }
            
            if let pickedImage = images.last {
                let newImg = UIImage.compressImage(pickedImage, compressRatio: 0.7)
                self.uploadImage(image: newImg!, attachmentGroupID: self.finalReportModel!.attachmentGroupID, finalReportId: self.finalReportModel?.flightID ?? 0)
            }
        }
    }
}
