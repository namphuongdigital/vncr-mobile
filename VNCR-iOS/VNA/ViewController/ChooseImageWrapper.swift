//
//  ChooseImageWrapper.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import Photos


protocol ChooseImageWrapperDelagate: NSObjectProtocol {
    func finishChooseImageWrapper(image: UIImage)
    func cancelChooseImageWrapper()
    
}

typealias SelectedImageBlock = (_ image: UIImage) -> Void

class ChooseImageWrapper: NSObject {
    
    var selectedImageBlock: SelectedImageBlock?
    
    private weak var delagate: ChooseImageWrapperDelagate?
    
    private weak var viewController: UIViewController?

    deinit {
        print("Deinit ChooseImageWrapper")
    }
    
    init(viewController: UIViewController) {
        super.init()
        self.viewController = viewController
        self.delagate = self.viewController as! ChooseImageWrapperDelagate?
        
    }
    
    
    func showImagePicker(selectedImageBlock: @escaping SelectedImageBlock) {
        self.selectedImageBlock = selectedImageBlock
    }
}
