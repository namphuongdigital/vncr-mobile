//
//  ReportLotusShopController.swift
//  VNA
//
//  Created by Pham Dai on 27/08/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class ReportLotusShopController: BaseViewController {

    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var txvRemark: UITextView10Corner!
    @IBOutlet weak var comPhoto: PhotoViewComponent!
    @IBOutlet weak var stackOptions: UIStackView!
    @IBOutlet weak var tableView: UITableView!
    
    var attachmenIdsremoved:[Int] = []
    var options:[Button10Corner] = []
    var selectedOption:CommonItem?
    var currentOption:CommonItem?
    var passenger:PassengerSeat!
    let flightID:Int
    var dictList:[[CommonItem]] = []
    
    var onSaveSuccess:((LotusShopStatus?)->Void)?
    
    init(passenger:PassengerSeat,flightID:Int, onSaveSuccess:((LotusShopStatus?)->Void)? = nil) {
        self.flightID = flightID
        self.passenger = passenger
        super.init(nibName: String(describing: type(of: self)), bundle: Bundle.main)
        self.setCustomPresentationStyle()
        self.onSaveSuccess = onSaveSuccess
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.navigationController != nil && self.splitViewController == nil {
            setBarCloseButton()
        }
        navigationItem.title = "\("LotuShop's") " + (passenger?.seat ?? "")
        
        if passenger.lotusShopItems.count > 0 {
            dictList.append(passenger.lotusShopItems)
        }
        if passenger.lotusShopNotes.count > 0 {
            dictList.append(passenger.lotusShopNotes)
        }
        
        setBarRightButton(image:#imageLiteral(resourceName: "ic_upload"), color: .white)
        
        txvRemark.addToolbarEndEditing()
        txvRemark.font = .systemFont(ofSize: 14)
        txvRemark.borderColor = .gray
        
        comPhoto.controller = self
        comPhoto.maxItems = 3
        comPhoto.delegate = self
        
        tableView.tableHeaderView = UIView(frame: CGRect(origin: .zero, size: CGSize(width: 1, height: 0.01)))
        tableView.estimatedRowHeight = 50
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        tableView.register(LotusItemCell.nib, forCellReuseIdentifier: LotusItemCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        loadLotusShopData()
        
        self.view.layoutIfNeeded()
        updatePrsentSize()
        
        if #available(iOS 10.0, *) {// check if have request stored offline
            checkLoadStoredRequest()
        }
    }
    
    private func loadLotusShopData() {
        
        comPhoto.reset()
        stackOptions.arrangedSubviews.forEach{$0.removeFromSuperview()}
        
        // option selected
        selectedOption = passenger.lotusReportItems.first(where: {$0.id == passenger.lotusShopStatus?.selectedItemID})
        currentOption = selectedOption
        passenger.lotusReportItems.forEach { (item) in
            addOption(data: item,isSelect: selectedOption?.id == item.id)
        }
        
        // remark
        if passenger.lotusShopStatus == nil || passenger.lotusShopStatus?.remark?.count ?? 0 == 0 {
            txvRemark.placeholder1 = "Note".localizedString()
        } else {
            txvRemark.text = passenger.lotusShopStatus?.remark
        }
        
        // attachments
        if let attach = passenger.lotusShopStatus?.attachments, attach.count > 0 {
            attach.forEach { att in
                let item = MediaItem(type: .image)
                item.object = att
                if let base64 = att.base64File,
                   let data = Data(base64Encoded: base64) {
                    item.image = UIImage(data: data)
                } else {
                    item.urlString = att.FoFileUrl
                }
                
                comPhoto.addItem(item: item)
            }
        }

    }
    
    @available(iOS 10.0, *)
    private func checkLoadStoredRequest() {
        guard let key = passenger.lotusShopKey else {return}
        let identifier = "\(self.flightID)\(key)"
        ManagerRequests.getRequest(type: StoredRequest.RequestType.lotusShop.rawValue,
                                   identifier: identifier) {[weak self] request in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                if let shop = request?.lotusShopStatus {
                    self.passenger.lotusShopStatus = shop
                    self.loadLotusShopData()
                }
            }
        }
    }
    
    override func actionBarRightButton() {
        guard let select = selectedOption, let key = passenger.lotusShopKey else {return}
        
        self.showMBProgressHUD("", animated:true)
        
        let group = DispatchGroup()
        
        var dict:[DynamicModelString] = []
        
        // upload again old attachments
        comPhoto.items.filter({$0.urlString != nil && ($0.urlString?.count ?? 0 > 0)}).forEach({
            if let urlS = $0.urlString,
               let url = URL(string: urlS),
               let data = try? Data(contentsOf: url) {
                let dataString = data.base64EncodedString()
                dict.append(DynamicModelString(dictionary: ["OriginalFileName" : "img_\(Date().timeIntervalSince1970).png",
                                                            "ContentBase64":dataString]))
            }
        })

        // upload new attachments
        comPhoto.items.forEach { item in
            if let asset = item.asset {
                group.enter()
                asset.getDataToUpload { _, data, error in
                    if let dataString = data?.base64EncodedString() {
                        dict.append(DynamicModelString(dictionary: ["OriginalFileName" : "img_\(Date().timeIntervalSince1970).png",
                                                                    "ContentBase64":dataString]))
                    } else {
                        print("cant parse to base64encodedstring")
                    }
                    group.leave()
                }
            }
        }
        group.notify(queue: .main) {[weak self] in guard let `self` = self else { return }
            
            ManagerRequests.requestLotus(flightId: self.flightID, selectedItemId: select.id, lotusShopKey: key, remark: self.txvRemark.text, deletedAttachment: [], attachments: dict) {[weak self] data, error in
                guard let `self` = self else { return }
                DispatchQueue.main.async {
                    self.hideMBProgressHUD(true)
                    if let error = error {
                        self.showAlert("Alert".localizedString(), stringContent: error)
                    } else {
                        self.passenger.lotusShopStatus = data
                        self.onSaveSuccess?(data)
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                }
            }
        }
        
    }
    
    override func close() {
        if (selectedOption != nil && currentOption?.id != selectedOption?.id) ||
            self.attachmenIdsremoved.count > 0 ||
            (self.passenger.lotusShopStatus?.remark != txvRemark.text && self.passenger.lotusShopStatus?.remark != nil) ||
            comPhoto.numerItemsSelected() > 0 {
            self.showAlert("Alert".localizedString(), message: "areyouclose".localizedString(), buttons: ["Cancel".localizedString(),"OK".localizedString()]) { action, i in
                if i == 1 {
                    super.close()
                }
            }
        } else {
            super.close()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updatePrsentSize()
    }
    
    func updatePrsentSize() {
        
        var heightTableView:CGFloat = tableView.contentInset.top + tableView.contentInset.bottom
        dictList.enumerated().forEach({ (i,k) in
            if i > 0 {
                heightTableView += tableView.sectionHeaderHeight
            }
            k.forEach {
                if let flagCell = Bundle.main.loadNibNamed(String(describing: LotusItemCell.self), owner: nil, options: nil)?.first as? LotusItemCell {
                    flagCell.show(item: $0, backgroundColor: .lightGray)
                    flagCell.layoutIfNeeded()
                    let size = flagCell.systemLayoutSizeFitting(CGSize(width: self.view.frame.width, height: CGFloat.infinity))
                    heightTableView += size.height
                }
            }
        })
        
        if let height = tableView.getHeightConstraint() {
            height.constant = heightTableView
        } else {
            tableView.heightAnchor.constraint(equalToConstant: heightTableView).isActive = true
        }
        self.view.layoutIfNeeded()
        
        var height = scrollView.contentSize.height
        let max = UIScreen.bounceWindow.height*0.75
        if height > max {
            height = max
        }
        self.preferredContentSize = CGSize(width: isIpad ? UIScreen.bounceWindow.width * 0.5 : UIScreen.bounceWindow.width - 40, height: height)
        UIView.animate(withDuration: 0.2) {
            self.navigationController?.preferredContentSize = self.preferredContentSize
        }
    }
    
    @objc func selectOption(_ sender:Button10Corner) {
        if sender.isSelected {return}
        options.forEach{$0.isSelected = false}
        sender.isSelected = !sender.isSelected
        selectedOption = sender.object as? CommonItem
    }
    
    func addOption(data:CommonItem, isSelect:Bool = false) {
        let maxColumn:Int = isIpad ? 2 : 2
        var st = self.stackOptions.arrangedSubviews.last as? UIStackView
        if st == nil {
            st = UIStackView(frame: .zero)
            st!.axis = .horizontal
            st!.spacing = 30
            st!.alignment = .fill
            st!.distribution = .fillEqually
            self.stackOptions.addArrangedSubview(st!)
        }
        let vTag = Button10Corner(type: .custom)
        vTag.setContentHuggingPriority(.fittingSizeLevel, for: .horizontal)
        options.append(vTag)
        vTag.setTitleColor(normal: .black, highlighted: .gray)
        vTag.setImage(UIColor.groupTableViewBackground.withAlphaComponent(0.1).imageRepresentation.resizeImage(newSize: CGSize(width: 16, height: 16)).maskRoundedImage(radius: 8), for: .normal)
        vTag.setImage(UIColor.systemBlue.imageRepresentation.resizeImage(newSize: CGSize(width: 16, height: 16)).maskRoundedImage(radius: 8), for: .selected)
        vTag.contentHorizontalAlignment = .left
        vTag.titleEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: -10)
        vTag.contentEdgeInsets = UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 20)
        vTag.titleLabel?.font = .systemFont(ofSize: 14)
        vTag.setTitle(data.title, for: UIControl.State())
        vTag.isSelected = isSelect
        vTag.object = data
        vTag.removeTarget(self, action: #selector(self.selectOption(_:)), for: .touchUpInside)
        vTag.addTarget(self, action: #selector(self.selectOption(_:)), for: .touchUpInside)
        st!.addArrangedSubview(vTag)
        self.view.layoutIfNeeded()
        st!.layoutIfNeeded()
        if st!.arrangedSubviews.count > maxColumn {
            st!.removeArrangedSubview(vTag)
            let stack = UIStackView()
            stack.addArrangedSubview(vTag)
            stack.axis = .horizontal
            stack.spacing = 30
            stack.alignment = .fill
            stack.distribution = .fillEqually
            self.stackOptions.addArrangedSubview(stack)
        }
    }
}

// MARK: - UITableViewDelegate,UITableViewDataSource
extension ReportLotusShopController: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return dictList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let values = dictList[indexPath.section]
        let item = values[indexPath.row]

        var color = indexPath.row%2 != 0 ? UIColor("#E7F1FE").withAlphaComponent(0.5) : UIColor("#E7F1FE")
        if indexPath.section > 0 {
            if tableView.numberOfRows(inSection: indexPath.section - 1) % 2 != 0 {
                color = indexPath.row%2 == 0 ? UIColor("#E7F1FE").withAlphaComponent(0.5) : UIColor("#E7F1FE")
            }
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: LotusItemCell.identifier) as! LotusItemCell
        cell.show(item: item, backgroundColor: color)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictList[section].count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(origin: .zero, size: CGSize(width: tableView.frame.width, height: 1)))
        view.backgroundColor = .darkGray
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }
        return 1
    }
}

// MARK: - PhotoViewComponentDelegate
extension ReportLotusShopController:PhotoViewComponentDelegate {
    func PhotoViewComponentDelegate_updateHeight(view: PhotoViewComponent) {
        self.view.layoutIfNeeded()
        updatePrsentSize()
    }
    
    func PhotoViewComponentDelegate_overMaxItemsConfig(view: PhotoViewComponent) {
        self.showAlert("Alert".localizedString(), stringContent: "Maximum number selected photos".localizedString())
    }
    
    func PhotoViewComponentDelegate_removeItem(view: PhotoViewComponent, item: MediaItem?, index: Int, result: @escaping ((Int, Bool) -> Void)) -> Bool {
        if item?.asset != nil {
            return true
        }
        self.showAlert("Alert".localizedString(), message: "Are you sure delete ?".localizedString(), buttons: ["NO".localizedString(),"YES".localizedString()]) {[weak self] action, i in
            if action.title == "YES".localizedString() {
                result(index,true)
                guard let `self` = self, let att = item?.object as? AttachmentCommonModel else { return }
                self.attachmenIdsremoved.append(att.FileID)
            }
        }
        return false
    }
    
}
