//
//  SupportDetailViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 4/6/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class SupportDetailViewController: TextInputViewController, UITableViewDelegate, UITableViewDataSource, SupportItemTableViewCellDelegate {

    var supportModel: SupportModel!
    
    var listSupportModel = Array<SupportModel>()
    
    var refreshControl: UIRefreshControl!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if #available(iOS 11.0, *) {
            self.tableView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let addBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.addBarButtonItemPressed(_:)))
        addBarButtonItem.setFAIcon(icon: .FAPlus, iconSize: 20)
        //self.navigationItem.rightBarButtonItems = [addBarButtonItem]
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.navigationItem.title = "SUPPORT LIST DETAIL".localizedString()
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
        
        self.loadTextInput()
        self.isHiddenTexInputWhenHiddenKeyboard = false
        self.tableView.frame = CGRect(x: self.tableView.frame.origin.x, y: self.tableView.frame.origin.y, width: self.tableView.frame.size.width, height: self.tableView.frame.size.height - 45)
        
        
        self.navigationController?.view.backgroundColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    @objc func refreshData() {
        getNewData()
    }
    
    func getNewData() {
        
        ServiceData.sharedInstance.taskGetSupportDetail(supportID: self.supportModel.supportID).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arraySupport = Array<SupportModel>()
                for item in array {
                    let supportModel = SupportModel(json: item)
                    arraySupport.append(supportModel)
                }
                self.listSupportModel = arraySupport
            }
            self.tableView.reloadData()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func addBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.textInputBar.textView.becomeFirstResponder()
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listSupportModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let support = self.listSupportModel[indexPath.row]
        if(support.parentID == 0) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportItemTableViewCell")! as! SupportItemTableViewCell
            cell.loadData(supportModel: support, actions: [])
            cell.delegateAction = self
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SupportItemTableViewCell2")! as! SupportItemTableViewCell
            cell.loadData(supportModel: support, actions: [.hide, .remove])
            cell.delegateAction = self
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    
    func refreshListSupport(support: SupportModel) {
        
        for index in 0..<self.listSupportModel.count {
            if (self.listSupportModel[index].supportID == support.supportID) {
                if(support.status == .hide || support.status == .removed) {
                    self.listSupportModel.remove(at: index)
                    self.tableView.deleteRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                } else {
                    self.listSupportModel[index] = support
                    self.tableView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .automatic)
                }
                
                return
            }
        }
    }
    
    
    func confirmDelete(supportModel: SupportModel) {
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self] (UIAlertAction) -> Void in
            guard let `self` = self else {
                return
            }
            self.showMBProgressHUD("Update...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskSupportUpdate(action: .remove, supportID: supportModel.supportID).continueOnSuccessWith(continuation: { task in
                
                if let result = task as? JSON {
                    let supportModel = SupportModel(json: result)
                    self.refreshListSupport(support: supportModel)
                }
                self.hideMBProgressHUD(true)
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style:UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you sure delete ?".localizedString(), message: "", actions: [yesAction,cancelAction])
    }
    
    //MARK - SupportItemTableViewCellDelegate
    
    func actionSupportItemTableViewCell(action: ActionUpdateSupportType, supportModel: SupportModel) {
        if(action == .remove) {
            self.confirmDelete(supportModel: supportModel)
        } else {
            self.showMBProgressHUD("Update...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskSupportUpdate(action: action, supportID: supportModel.supportID).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    let supportModel = SupportModel(json: result)
                    self.refreshListSupport(support: supportModel)
                }
                self.hideMBProgressHUD(true)
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
        }
    }
    
    
    override func sendSuport(text: String) {
        self.textInputBar.textView.resignFirstResponder()
        self.textInputBar.textView.text = ""
        self.showMBProgressHUD("Add new...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskSupportUpdate(action: .add, supportID: self.supportModel.supportID, message: text).continueOnSuccessWith(continuation: { task in
            
            if let result = task as? JSON {
                let supportModel = SupportModel(json: result)
                self.listSupportModel.append(supportModel)
                self.tableView.insertRows(at: [IndexPath.init(row: self.listSupportModel.count - 1, section: 0)], with: .automatic)
                self.tableView.scrollToRow(at: IndexPath.init(row: self.listSupportModel.count - 1, section: 0), at: .bottom, animated: true)
            }
            self.hideMBProgressHUD(true)
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    override func sendSuport(image: UIImage) {
        self.showMBProgressHUD("Add new...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFilemanagerSupportuploadfiles(supportID: self.supportModel.supportID, fileName: "image-Support.jpg", image: image).continueOnSuccessWith(continuation: { task in
            if let support = task as? SupportModel {
                
                self.listSupportModel.append(support)
                self.tableView.insertRows(at: [IndexPath.init(row: self.listSupportModel.count - 1, section: 0)], with: .automatic)
                self.tableView.scrollToRow(at: IndexPath.init(row: self.listSupportModel.count - 1, section: 0), at: .bottom, animated: true)
                
            }
            self.hideMBProgressHUD(true)
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        
    }
}
