//
//  SurveyListViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/29/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class SurveyListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, SurveyTableViewCellDelegate, UISearchBarDelegate {
    
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero) //CustomSearchBarView = CustomSearchBarView(frame: CGRect.zero)
    
    var isListSaved: Bool = false
    
    var scheduleFlyModel: ScheduleFlightModel?
    
    var refreshControl: UIRefreshControl!
    
    var segmentedControl: UISegmentedControl = UISegmentedControl(frame: CGRect.zero)
    
    @IBOutlet weak var tableView: UITableView!
    
    var listSurveyModel = [SurveyModel]()
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var searchText = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func emptyDataSetShouldAllowScroll(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        self.textTitle = "SURVEY".localizedString()
        if(isListSaved == false) {
            self.setupSearchBar()
        }
        
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        let nib = UINib(nibName: "SurveyTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "SurveyTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tableView.alwaysBounceVertical = true
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        
        getNewData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func refreshData() {
        getNewData()
    }
    
    func getNewData() {
        if(isListSaved) {
            self.textTitle = "SURVEY ARCHIVES".localizedString()
            if let listSurveyModel = ApplicationData.sharedInstance.getSurveyModels() {
                self.listSurveyModel = listSurveyModel
            }
            self.tableView.reloadData()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        } else {
            ServiceData.sharedInstance.taskSurveyList(flightID: self.scheduleFlyModel!.flightID, keyword: searchText).continueOnSuccessWith(continuation: { task in
                
                let result = task as! JSON
                if let array = result.array {
                    var arrayModel = Array<SurveyModel>()
                    for item in array {
                        let model = SurveyModel(json: item)
                        arrayModel.append(model)
                    }
                    self.listSurveyModel = arrayModel
                    
                }
                self.tableView.reloadData()
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            })
        }
        
    }

    func configure(cell: SurveyTableViewCell, at indexPath: IndexPath) {
        if(isListSaved) {
            cell.surveyTableViewCellDelegate = self
        } else {
            cell.surveyTableViewCellDelegate = nil
        }
        cell.surveyModel = self.listSurveyModel[indexPath.row]
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listSurveyModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SurveyTableViewCell")! as! SurveyTableViewCell
        self.configure(cell: cell, at: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "SurveyTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! SurveyTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if(self.listSurveyModel[indexPath.row].isReadOnly == false) {
            if(isListSaved) {
                self.pushSurveyDetailViewController(surveyModel: self.listSurveyModel[indexPath.row], scheduleFlyModel: nil, isModelSaved: true)
            } else {
                self.pushSurveyDetailViewController(surveyModel: self.listSurveyModel[indexPath.row], scheduleFlyModel: self.scheduleFlyModel!)
            }
        }
        
        
    }
    /*
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.01
    }
    */
    
    //MARK - SurveyTableViewCellDelegate
    func deleteSurveyTableViewCellPress(surveyModel: SurveyModel) {
        for index in 0..<self.listSurveyModel.count {
            if(self.listSurveyModel[index].id == surveyModel.id) {
                self.listSurveyModel.remove(at: index)
                break
            }
        }
        ApplicationData.sharedInstance.deleteSurveyModel(id: surveyModel.id)
        self.tableView.reloadData()
    }
    
    //
    
    func setupSearchBar() {
        //self.searchBar.searchBarOriginX = 0
        //        self.searchBar.tintColor = .white
        //        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.placeholder = "Search".localizedString()
        searchBar.delegate = self
        tableView.keyboardDismissMode = .onDrag
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
    }
    
    //MARK - UISearchBarDelegate
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
        }
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if(self.searchBar.text == "" || self.searchBar.text == nil) {
            searchText = ""
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("")
        searchText = searchBar.text ?? ""
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
}
