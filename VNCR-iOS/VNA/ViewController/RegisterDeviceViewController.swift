//
//  RegisterDeviceViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/8/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import BoltsSwift
import SwiftyJSON

class RegisterDeviceViewController: FormViewController {
    
    public static var PhoneRegister : String = ""
    
    var imageBackground : UIImageView!
    
    var imageViewFooter: UIImageView!
    
    var phoneRow: CustomRowFormer<TextTableViewCell>!
    
    var buttonLoginRow: CustomRowFormer<ButtonActionTableViewCell>!
    
    @IBOutlet weak var buttonCloseForm: UIButton!
    
    var taskCompletionSource: TaskCompletionSource<AnyObject>?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
        
        buttonCloseForm.addTarget(self, action: #selector(buttonCloseFormPress(sender:)), for: .touchUpInside)
        buttonCloseForm.setFAIcon(icon: .FAClose, forState: .normal)
        buttonCloseForm.setFATitleColor(color: .white)
        buttonCloseForm.layer.borderWidth = 0.7
        buttonCloseForm.layer.borderColor = UIColor.white.cgColor
        buttonCloseForm.layer.cornerRadius = buttonCloseForm.frame.size.width / 2.0
        
        imageBackground = UIImageView(frame: self.view.bounds)
        //imageBackground.image = UIImage(named: "logo-VNA.png")
        imageBackground.contentMode = .scaleAspectFill
        
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        self.view.insertSubview(imageBackground, belowSubview: tableView)
        self.view.backgroundColor = UIColor("#006885")
        
        let logoRow = CustomRowFormer<LogoLoginTableViewCell>(instantiateType: .Nib(nibName: "LogoLoginTableViewCell")) {
            print($0.textLabel?.text ?? "")
            $0.backgroundColor = UIColor("#006885")
            }.configure {
                $0.rowHeight = 100
        }
        
        
        /*
        phoneRow = CustomRowFormer<TextTableViewCell>(instantiateType: .Nib(nibName: "TextTableViewCell")) {
            print($0.textLabel?.text ?? "")
            $0.textFieldInput.placeholder = "Password".localizedString()
            $0.textFieldInput.isSecureTextEntry = true
            $0.labelIconInput.setFAIcon(icon: FAType.FALock, iconSize: 18)
            }.configure {
                $0.rowHeight = 60
        }
        */
        phoneRow = CustomRowFormer<TextTableViewCell>(instantiateType: .Nib(nibName: "TextTableViewCell")) {
            print($0.textLabel?.text ?? "")
            $0.backgroundColor = UIColor("#006885")
            $0.textFieldInput.keyboardType = UIKeyboardType.phonePad
            $0.textFieldInput.placeholder = "Enter phone number".localizedString()
            $0.labelIconInput.setFAIcon(icon: FAType.FAPhoneSquare, iconSize: 18)
            }.configure {
                $0.rowHeight = 60
        }
        
        buttonLoginRow = CustomRowFormer<ButtonActionTableViewCell>(instantiateType: .Nib(nibName: "ButtonActionTableViewCell")) {[weak self] in
            print($0.textLabel?.text ?? "")
            $0.backgroundColor = UIColor("#006885")
            $0.buttonAction.backgroundColor = UIColor("#dba510")
            $0.buttonAction.setTitle("Register".localizedString(), for: UIControl.State())
            $0.buttonAction.addTarget(self, action: #selector(self?.buttonRegisterPress(sender:)), for: .touchUpInside)
            }.configure {
                $0.rowHeight = 60
        }
        
        let createHeader: ((String, _ height: CGFloat) -> ViewFormer) = { text, height in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor.clear
                $0.titleLabel.font = UIFont.systemFont(ofSize: 14)
                }.configure {
                    $0.viewHeight = height
                    $0.text = text
                    
            }
        }

        let customRowFormerSection = SectionFormer(rowFormer: logoRow, phoneRow, buttonLoginRow).set(headerViewFormer: createHeader("", 40))
        former.append(sectionFormer: customRowFormerSection)
        
        if(taskCompletionSource == nil) {
            self.buttonCloseForm.isHidden = true
        }
        
        let powerBy = UILabel()
        powerBy.text = "Powered by NP Technology"
        powerBy.numberOfLines = 2
        powerBy.textAlignment = .center
        powerBy.font = .systemFont(ofSize: 14)
        powerBy.textColor = .white
        view.addSubview(powerBy)
        powerBy.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraint(equalTo: powerBy.centerXAnchor).isActive = true
        if #available(iOS 11.0, *) {
            view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: powerBy.bottomAnchor, constant: 20).isActive = true
        } else {
            view.bottomAnchor.constraint(equalTo: powerBy.bottomAnchor, constant: 20).isActive = true
        }
    }
    
    @objc func buttonRegisterPress(sender: UIButton) {
        getRegister()
    }
    
    func getRegister() {
        let phoneTableViewCell = phoneRow.cellInstance as! TextTableViewCell
        if(phoneTableViewCell.textFieldInput.validate() == false) {
            phoneTableViewCell.updateValidationState(result: .invalid)
            self.showAlert("Alert".localizedString(), stringContent: "Phone invalid".localizedString())
            return
        }
        phoneTableViewCell.updateValidationState(result: .valid)
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        RegisterDeviceViewController.PhoneRegister = phoneTableViewCell.textFieldInput.text!
        ServiceData.sharedInstance.taskRegister4vnc(phoneNo: phoneTableViewCell.textFieldInput.text!, pushToken: ServiceData.sharedInstance.oneSignalUserId).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            self.pushVerifyCodeViewController(taskCompletionSource: self.taskCompletionSource)
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        
    }
    
    
    func buttonTryAgain(sender: UIButton) {
    }
    
    @objc func buttonCloseFormPress(sender: UIButton) {
        let error = NSError(domain: "serviceError", code: 10, userInfo: [NSLocalizedDescriptionKey: "User not authentication"])
        self.taskCompletionSource?.trySet(error: error)
        self.dismiss(animated: true, completion: nil)
    }
    
}
