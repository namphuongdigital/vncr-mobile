//
//  TextInputViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 4/6/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class TextInputViewController: BaseTableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var bottomInputView: NSLayoutConstraint!
    
    @IBOutlet weak var textInputBar: ALTextInputBar!
    
    var isHiddenTexInputWhenHiddenKeyboard: Bool! {
        didSet {
            textInputBar.isHidden = isHiddenTexInputWhenHiddenKeyboard
//            if let old = oldValue {
//
//            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadTextInput() {
        configureInputBar()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardFrameChanged(notification:)), name: NSNotification.Name(rawValue: ALKeyboardFrameDidChangeNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.isHiddenTexInputWhenHiddenKeyboard = true
        //self.bottomInputView.constant =  50
        self.bottomInputView.constant = self.tabBarController?.tabBar.bounds.height ?? 50
    }

    func configureInputBar() {
        self.tableView.keyboardDismissMode = .onDrag
        let leftButton  = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 44))
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: 44, height: 44))
        leftButton.setFAIcon(icon: .FACamera, forState: UIControl.State())
        leftButton.setFATitleColor(color: UIColor("#166880"))
        leftButton.addTarget(self, action: #selector(self.attachImage(_:)), for: .touchUpInside)
        rightButton.setTitle("Send".localizedString(), for: UIControl.State())
        rightButton.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.medium)
        rightButton.setTitleColor(UIColor("#166880"), for: UIControl.State())
        rightButton.addTarget(self, action: #selector(self.sendSupportPressed(_:)), for: .touchUpInside)
        
        textInputBar.showTextViewBorder = true
        textInputBar.leftView = leftButton
        textInputBar.rightView = rightButton
        //self.tableView.frame = CGRect(x: 0, y: view.frame.origin.y, width: view.frame.size.width, height: view.frame.size.height - textInputBar.defaultHeight - 44)
        self.textInputBar.frame = CGRect(x: 0, y: view.frame.size.height - textInputBar.defaultHeight - 64, width: view.frame.size.width, height: textInputBar.defaultHeight)
        textInputBar.backgroundColor = UIColor(white: 0.95, alpha: 1)
        //view.addSubview(textInputBar)
    }
    
    @objc func sendSupportPressed(_ sender: UIButton) {
        self.sendSuport(text: textInputBar.text)
    }
    
    @objc func attachImage(_ sender: UIButton)  {
        showActionSheetSelectImage()
    }
    
    func sendSuport(text: String) {
        
    }
    
    @objc func keyboardFrameChanged(notification: NSNotification) {
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let userInfo = (notification as NSNotification).userInfo {
            if let keyboardHeight = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                self.bottomInputView.constant = keyboardHeight
                if(self.isHiddenTexInputWhenHiddenKeyboard == true) {
                    //textInputBar.isHidden = false
                }
                UIView.animate(withDuration: 0.25, animations: { () -> Void in
                    self.view.layoutIfNeeded()
                }, completion: nil)
                
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let userInfo = (notification as NSNotification).userInfo {
            if let _ = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size.height {
                //key point 0,
                self.bottomInputView.constant =  50
                
                //textViewBottomConstraint.constant = keyboardHeight
                //UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() })
                UIView.animate(withDuration: 0.25, animations: { () -> Void in self.view.layoutIfNeeded() }, completion: {[weak self] (finished) in
                    if(self?.isHiddenTexInputWhenHiddenKeyboard == true) {
                        self?.textInputBar.isHidden = true
                    }
                    
                })
                
            }
        }
    }
    
    func takeImage() {
        
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .camera
        present(imagePicker, animated: true, completion: nil)
        
    }
    
    func showActionSheetSelectImage() {
        if(UIDevice.current.userInterfaceIdiom == .pad) {
            imagePicker.modalPresentationStyle = UIModalPresentationStyle.popover
            imagePicker.popoverPresentationController?.sourceView = self.view
            
        }
        
        
        var arrayAction = Array<UIAlertAction>()
        
        let takePhotoAction = UIAlertAction(title: "Camera".localizedString(), style: UIAlertAction.Style.default, handler: {[weak self] (UIAlertAction) -> Void in
            self?.takeImage()
        })
        arrayAction.append(takePhotoAction)
        
        let chooseFromLibAction = UIAlertAction(title: "Library".localizedString(), style: UIAlertAction.Style.default, handler: {[weak self] (UIAlertAction) -> Void in
            self?.imagePicker.delegate = self
            self?.imagePicker.allowsEditing = false
            self?.imagePicker.sourceType = .photoLibrary
            self?.present(self!.imagePicker, animated: true, completion: nil)
        })
        arrayAction.append(chooseFromLibAction)
        
        let cancelAction = UIAlertAction(title: "Cancel".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        arrayAction.append(cancelAction)
        
        self.showActionSheetWithAction("Upload image".localizedString(), message: nil, actions: arrayAction)
    }
    
    // MARK: - UIImagePickerControllerDelegate Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.sendSuport(image: UIImage.compressImage(pickedImage, compressRatio: 0.7))
        }
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func sendSuport(image: UIImage) {
    }
    
    func pressImageSupportItemTableViewCell(cell: SupportItemTableViewCell) {
        if let supportModel = cell.supportModel {
            let attachment = supportModel.attachments[0]
            let photo = SKPhoto.photoWithImageURL(attachment.downloadPath)
            photo.caption = ""
            let browser = SKPhotoBrowser(photos: [photo], initialPageIndex: 0)
            present(browser, animated: true, completion: {})
        }
    }
}
