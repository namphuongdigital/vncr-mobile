//
//  VerifyCodeViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/8/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//
import UIKit
import BoltsSwift
import SwiftyJSON
import LocalAuthentication

class VerifyCodeViewController: FormViewController {
    
    let context = LAContext()
    
    var imageBackground : UIImageView!
    
    var imageViewFooter: UIImageView!
    
    var imageViewRow: CustomRowFormer<ImageViewTableViewCell>!
    
    var phoneRow: CustomRowFormer<TextTableViewCell>!
    
    var buttonOkRow: CustomRowFormer<ButtonActionTableViewCell>!
    
    var textInfoRow1: CustomRowFormer<TextInfoTableViewCell>!
    
    var textInfoRow2: CustomRowFormer<TextInfoTableViewCell>!
    
    var taskCompletionSource: TaskCompletionSource<AnyObject>?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        // Do any additional setup after loading the view.
        
        imageBackground = UIImageView(frame: self.view.bounds)
        //imageBackground.image = UIImage(named: "logo-VNA.png")
        imageBackground.contentMode = .scaleAspectFill
        
        tableView.backgroundColor = UIColor.clear
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        self.view.insertSubview(imageBackground, belowSubview: tableView)
        self.view.backgroundColor = UIColor("#006885")
        
        let logoRow = CustomRowFormer<LogoLoginTableViewCell>(instantiateType: .Nib(nibName: "LogoLoginTableViewCell")) {
            print($0.textLabel?.text ?? "")
            $0.backgroundColor = UIColor("#006885")
            }.configure {
                $0.rowHeight = 150
        }
        
        imageViewRow = CustomRowFormer<ImageViewTableViewCell>(instantiateType: .Nib(nibName: "ImageViewTableViewCell")) {
            print($0.textLabel?.text ?? "")
            $0.backgroundColor = UIColor("#006885")
            
            }.configure {
                $0.rowHeight = 200
        }
        
        
        /*
         phoneRow = CustomRowFormer<TextTableViewCell>(instantiateType: .Nib(nibName: "TextTableViewCell")) {
         print($0.textLabel?.text ?? "")
         $0.textFieldInput.placeholder = "Password".localizedString()
         $0.textFieldInput.isSecureTextEntry = true
         $0.labelIconInput.setFAIcon(icon: FAType.FALock, iconSize: 18)
         }.configure {
         $0.rowHeight = 60
         }
         */
        phoneRow = CustomRowFormer<TextTableViewCell>(instantiateType: .Nib(nibName: "TextTableViewCell")) {
            print($0.textLabel?.text ?? "")
            $0.backgroundColor = UIColor("#006885")
            //$0.textFieldInput.keyboardType = UIKeyboardType.phonePad
            $0.textFieldInput.keyboardType = .numberPad
            $0.textFieldInput.placeholder = "Enter code".localizedString()
            $0.textFieldInput.font = UIFont.systemFont(ofSize: 15)
            $0.labelIconInput.setFAIcon(icon: FAType.FACode, iconSize: 18)
            }.configure {
                $0.rowHeight = 60
        }
        
        buttonOkRow = CustomRowFormer<ButtonActionTableViewCell>(instantiateType: .Nib(nibName: "ButtonActionTableViewCell")) {[weak self] in
            print($0.textLabel?.text ?? "")
            $0.backgroundColor = UIColor("#006885")
            $0.buttonAction.backgroundColor = UIColor("#dba510")
            $0.buttonAction.setTitle("Activate".localizedString(), for: UIControl.State())
            $0.buttonAction.addTarget(self, action: #selector(self?.buttonOkPress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        textInfoRow1 = CustomRowFormer<TextInfoTableViewCell>(instantiateType: .Nib(nibName: "TextInfoTableViewCell")) {[weak self] in
            guard let `self` = self else {
                return
            }
            $0.backgroundColor = UIColor("#006885")
            $0.textInfoLabel.textColor = UIColor.white
            $0.textInfoLabel.text = String(format: "Vui lòng quét mã QR hoặc nhập mã xác thực được gửi tới số %@", RegisterDeviceViewController.PhoneRegister)
            //$0.accessoryType = .disclosureIndicator
            }.configure {
                $0.rowHeight = UITableView.automaticDimension
            }.onSelected({[weak self] (row) in
                print("")
            })
        
        textInfoRow2 = CustomRowFormer<TextInfoTableViewCell>(instantiateType: .Nib(nibName: "TextInfoTableViewCell")) {[weak self] in
            guard let `self` = self else {
                return
            }
            $0.backgroundColor = UIColor("#006885")
            $0.textInfoLabel.textColor = UIColor.white
            $0.textInfoLabel.text = String(format: "Please enter your verification code or scan QR code above")
            //$0.accessoryType = .disclosureIndicator
            }.configure {
                $0.rowHeight = UITableView.automaticDimension
            }.onSelected({[weak self] (row) in
                print("")
            })
        
        
        
        let createHeader: ((String, _ height: CGFloat) -> ViewFormer) = { text, height in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor.clear
                $0.titleLabel.font = UIFont.systemFont(ofSize: 14)
                }.configure {
                    $0.viewHeight = height
                    $0.text = text
                    
            }
        }
        
        let customRowFormerSection = SectionFormer(rowFormer: imageViewRow, phoneRow, buttonOkRow, textInfoRow1, textInfoRow2).set(headerViewFormer: createHeader("", 40))
        
        _ = CustomRowFormer<TextTableViewCell>(instantiateType: .Nib(nibName: "TextTableViewCell")) {
            print($0.textLabel?.text ?? "")
            
            }.configure {
                $0.rowHeight = 0
        }
        
        
        former.append(sectionFormer: customRowFormerSection)
        
        
        if let userModel = ApplicationData.sharedInstance.getLoginUserModel() {
            ServiceData.sharedInstance.userModel = userModel
            if UserDefaults.standard.bool(forKey: .keyEnableFaceID) {
                self.gotoMainApp()
            } else {
                self.gotoMainApp()
            }
        } else {
            self.genateQRCode()
        }
        
        let powerBy = UILabel()
        powerBy.text = "Powered by NP Technology"
        powerBy.numberOfLines = 2
        powerBy.textAlignment = .center
        powerBy.font = .systemFont(ofSize: 14)
        powerBy.textColor = .white
        view.addSubview(powerBy)
        powerBy.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraint(equalTo: powerBy.centerXAnchor).isActive = true
        if #available(iOS 11.0, *) {
            view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: powerBy.bottomAnchor, constant: 20).isActive = true
        } else {
            view.bottomAnchor.constraint(equalTo: powerBy.bottomAnchor, constant: 20).isActive = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    @objc func buttonOkPress(sender: UIButton) {
        verifyCode()
    }
    
    func verifyCode() {
        let phoneTableViewCell = phoneRow.cellInstance as! TextTableViewCell
        /*
        if(phoneTableViewCell.textFieldInput.validate() == false) {
            phoneTableViewCell.updateValidationState(result: .invalid)
            self.showAlert("Alert".localizedString(), stringContent: "Code invalid".localizedString())
            return
        }
        */
        let code = phoneTableViewCell.textFieldInput.text ?? ""
        phoneTableViewCell.updateValidationState(result: .valid)
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskOTP4VNC(otpCode: code).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            
            if let result = task as? JSON {
                let userModel = UserModel(json: result)
                ApplicationData.sharedInstance.updateUserLogin(userModel: userModel)
                let token = ServiceData.sharedInstance.token
                ServiceData.sharedInstance.serviceHeaders["X_TOKEN"] = token
                ServiceData.sharedInstance.userModel = userModel
                
                if let taskCompletionSource = self.taskCompletionSource {
                    taskCompletionSource.trySet(result: true as AnyObject)
                } else {
                    let taskGotoMap = {
                        self.gotoMainApp()
                    }
                    
                    self.deviceOwnerAuthentication(cancelTitle:"Cancel".localizedString(), context: self.context) {
                        taskGotoMap()
                    } onError: { LACode in
                        if LACode == LAError.userCancel.rawValue {
                            taskGotoMap()
                        }
                    } onNotAvailable: { isDeviceNotSupported in
                        taskGotoMap()
                    }
                }
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
        
    }
    
    
    
    
    func genateQRCode() {
        let inputCorrectionLevel = EFInputCorrectionLevel.h
        let content = String(format: "#1%@", UIDevice.current.uuidForKeychain())
        let generator = EFQRCodeGenerator(content: content, size: EFIntSize(width: 1024, height: 1024))
        generator.setInputCorrectionLevel(inputCorrectionLevel: inputCorrectionLevel)
        generator.setMode(mode: EFQRCodeMode.none)
        generator.setMagnification(magnification: EFIntSize(width: 9, height: 9))
        generator.setColors(backgroundColor: CIColor(color: UIColor.white), foregroundColor: CIColor(color: UIColor.black))
        generator.setIcon(icon: UIImage2CGimage(nil), size: nil)
        generator.setForegroundPointOffset(foregroundPointOffset: 0.0)
        generator.setAllowTransparent(allowTransparent: true)
        generator.setBinarizationThreshold(binarizationThreshold: 0.5)
        generator.setPointShape(pointShape: EFPointShape.square)
        
        if let tryCGImage = generator.generate() {
            let tryImage = UIImage(cgImage: tryCGImage)
            imageViewRow.cell.imageViewCell.image = tryImage
            print("")
        } else {
            let alert = UIAlertController(title: "Warning", message: "Create QRCode failed!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}

func UIImage2CGimage(_ image: UIImage?) -> CGImage? {
    if let tryImage = image, let tryCIImage = CIImage(image: tryImage) {
        return CIContext().createCGImage(tryCIImage, from: tryCIImage.extent)
    }
    return nil
}
