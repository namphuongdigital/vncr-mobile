//
//  WebViewDiscoverViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 7/15/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebKit

class WebViewDiscoverViewController: BaseViewController, WKUIDelegate, WKNavigationDelegate {
    
    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Discover".localizedString()
        loadWebView()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func loadView() {
        let webViewConfiguration = WKWebViewConfiguration.init()
        webViewConfiguration.allowsInlineMediaPlayback = true
        webView = WKWebView(frame: .zero, configuration: webViewConfiguration)
        webView.backgroundColor = UIColor.white
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.allowsLinkPreview = true
        if #available(iOS 13.0, *) {
            webView.scalesLargeContentImage = true
        } else {
            // Fallback on earlier versions
        }
        self.view = webView
    }
    
    func loadWebView() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskStatisticDiscover().continueOnSuccessWith(continuation: { task in
            let result = task as! JSON
            let myURL = URL(string: result.stringValue)
            let myRequest = URLRequest(url: myURL!)
            self.webView.load(myRequest)
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideMBProgressHUD(true)
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        if navigationAction.targetFrame == nil || navigationAction.targetFrame?.isMainFrame == false {
            if let urlToLoad = navigationAction.request.url {
                let url = URL(string: urlToLoad.absoluteString)!
                if UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                            print("Open url : \(success)")
                        })
                    } else {
                        // Fallback on earlier versions
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }
        return nil
    }
}
