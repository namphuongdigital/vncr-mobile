//
//  EditContentViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/17/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

protocol EditContentViewControllerDelegate : NSObjectProtocol {
    func editContentViewControllerSave(viewController: UIViewController, content: String)
}

class EditContentViewController: BaseViewController {
    
    weak var delegate: EditContentViewControllerDelegate?
    
    var content: String = ""
    
    var titleString: String = ""
    
    @IBOutlet weak var textView: IQTextView!
    
    override func viewDidAppear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let doneBarButtonItem = UIBarButtonItem(title: "Done".localizedString(), style: .done, target: self, action: #selector(self.doneBarButtonItemPressed(_:)))
        self.navigationItem.rightBarButtonItems = [doneBarButtonItem]
        
        self.textTitle = titleString
        textView.text = content
        textView.placeholder = "Content".localizedString()
        textView.becomeFirstResponder()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func doneBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.delegate?.editContentViewControllerSave(viewController: self, content: self.textView.text)
    }

}
