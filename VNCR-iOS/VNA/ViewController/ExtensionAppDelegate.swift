//
//  ExtensionAppDelegate.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit


extension UIApplication {
    
    func getAppDelegate() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController, top.view.window != nil {
                return topViewController(base: top)
            } else if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        
        return base
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension UILabel {
    func addLineSpacing(space:CGFloat, align:NSTextAlignment = .left) {
        if let text = text {
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = align
            paragraph.lineSpacing = space
            self.attributedText = NSAttributedString(string: text, attributes: [NSAttributedString.Key.paragraphStyle : paragraph])
        }
    }
}
