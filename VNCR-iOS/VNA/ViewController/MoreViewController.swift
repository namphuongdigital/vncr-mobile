//
//  SettingViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/6/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import MessageUI
import SwiftyJSON
import QRCodeReader
import AVFoundation
import LocalAuthentication

class MoreViewController: FormViewController, MFMailComposeViewControllerDelegate, QRCodeReaderViewControllerDelegate {
    
    var context = LAContext()
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObject.ObjectType.qr], captureDevicePosition: .back)
            $0.showTorchButton = true
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    var flightId: Int = 0
    
    var sourceCrewID: String = ""
    
    var destinationCrewID: String = ""
    
    var permission: PermissionCrewTaskType = .read
    
    var crewId: String = ""
    
    var userModel: UserModel!
    
    var textFieldVersionRowFormer:RowFormer!
    
    var textFieldUrlAPIRowFormer:RowFormer!
    
    fileprivate var avatarHeader:CustomViewFormer<FormHeaderFooterView>!
    
    var heightHeaderAvatar: CGFloat = 170
    
    var textVersion: String = "Unknown".localizedString()
    
    var isChangeUrlAPI: Bool = false
    
    var logoutBarButtonItem: UIBarButtonItem!
    
    var refreshControl: UIRefreshControl!
    
    var isSearch: Bool = false {
        didSet {
            if(isSearch) {
                refreshControl?.removeFromSuperview()
            } else {
                tableView.addSubview(refreshControl)
            }
            
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.emptyDataSetSource = self
        self.tableView.emptyDataSetDelegate = self
        self.navigationItem.title = "More".localizedString()
        if(crewId == "") {
            self.userModel = ServiceData.sharedInstance.userModel!
            self.loadViewData()
        } else {
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskUserGetinfobycrewid(crewId: self.crewId).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    let userModel = UserModel(json: result)
                    self.navigationItem.title = userModel.account
                    self.userModel = userModel
                    DispatchQueue.main.async {
                        self.loadViewData()
                        self.tableView.reloadData()
                    }
                }
                
                self.hideMBProgressHUD(true)
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
            })
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    @objc func refreshData() {
        if(crewId == "") {
            if(self.userModel.userId == 0) {
                ServiceData.sharedInstance.taskShowFormRegisterDevice().continueOnSuccessWith(continuation: { task in
                    self.userModel = ServiceData.sharedInstance.userModel!
                    self.former.removeAll()
                    self.loadViewData()
                    self.former.reload()
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()
                    
                    
                }).continueOnErrorWith(continuation: { error in
                    self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                    self.hideMBProgressHUD(true)
                    self.refreshControl.endRefreshing()
                })
            } else {
                self.hideMBProgressHUD(true)
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
            }
            
        } else {
            self.showMBProgressHUD("Loading...".localizedString(), animated: true)
            ServiceData.sharedInstance.taskUserGetinfobycrewid(crewId: self.crewId).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    let userModel = UserModel(json: result)
                    self.navigationItem.title = userModel.account
                    self.userModel = userModel
                    DispatchQueue.main.async {
                        self.loadViewData()
                        self.tableView.reloadData()
                    }
                }
                
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
                
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                self.refreshControl.endRefreshing()
            })
        }
    }
    
    func loadViewData() {
        //
        let createMenu: ((String, _ textColor:UIColor?, _ isDisclosureIndicator:Bool, (() -> Void)?) -> RowFormer) = { text, color, isDisclosure,  onSelected in
            return LabelRowFormer<FormLabelCell>() {
                if(color == nil){
                    $0.titleLabel.textColor = UIColor.black
                } else {
                    $0.titleLabel.textColor = color
                }
                
                $0.titleLabel.font = UIFont.systemFont(ofSize: 15)
                $0.textLabel?.font = UIFont.systemFont(ofSize: 15)
                if(isDisclosure){
                    $0.accessoryType = .disclosureIndicator
                } else {
                    $0.accessoryType = .none
                }
                
                }.configure {
                    $0.text = text
                }.onSelected { _ in
                    onSelected?()
            }
        }
        
        var nibNameUpdateAuthorizeTableViewCell = "UpdateReportTableViewCell"
        
        //if(UIDevice.current.userInterfaceIdiom == .phone) {
            heightHeaderAvatar = 140
            nibNameUpdateAuthorizeTableViewCell = String(format: "%@_iPhone", nibNameUpdateAuthorizeTableViewCell)
        //}
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            textVersion = String(format: "%@: %@", "Version".localizedString(), version)
        }
        
        if let appBuild = Bundle.main.infoDictionary?["CFBundleVersion"] as? String {
            textVersion += String(format: " (build %@)", appBuild)
        }
        
        if(self.userModel.account == "kerberos") {
            isChangeUrlAPI = true
        }
        /*
         logoutBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.logoutBarButtonItemPressed(_:)))
         logoutBarButtonItem.setFAIcon(icon: .FAPowerOff, iconSize: 20)
         if(self.userModel.userId == ServiceData.sharedInstance.userId) {
         self.navigationItem.rightBarButtonItem = self.logoutBarButtonItem
         }
         */
        self.tableView.separatorInset = UIEdgeInsets(top: self.tableView.separatorInset.top, left: 0, bottom: self.tableView.separatorInset.bottom, right: 0)
        self.tableView.backgroundColor = UIColor("#fafafa")
        //self.tableView.frame = CGRect(x: (self.tableView.frame.size.width - 320)/2.0, y: 0, width: 320, height: self.tableView.frame.size.height)
        
        
        textFieldVersionRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
        }
        (textFieldVersionRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.gray
        (textFieldVersionRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAInfoCircle, postfixText: String(format: "  %@", self.textVersion), size: 14, iconSize: 16)
        
        
        
        textFieldUrlAPIRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            
            self?.former.deselect(animated: true)
            self?.saveAPIURL()
        }
        (textFieldUrlAPIRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (textFieldUrlAPIRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FACompress, postfixText: String(format: "  %@", ServiceData.sharedInstance.hostAPI), size: 14, iconSize: 16)
        
        // Create Headers
        
        let createHeader: ((CGFloat, String) -> ViewFormer) = { height, text in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor("#fafafa")
                $0.titleLabel.font = UIFont.boldSystemFont(ofSize: 15)
                
                }.configure {
                    $0.viewHeight = height
                    $0.text = text
                    
            }
        }
        
        
        avatarHeader = CustomViewFormer<FormHeaderFooterView>() { [weak self] (formHeaderFooterView) in
            guard let `self` = self else {
                return
            }
            let imageView = UIImageViewProgress(isProgressBar: false)
            imageView.contentMode = .scaleAspectFit
            imageView.tag = 10
            imageView.frame =  CGRect(x: (UIScreen.main.bounds.width - 120) / 2.0, y: 10, width: 120, height: 120)
            //imageView.layer.backgroundColor = UIColor.init(white: 0.886, alpha: 1.000).cgColor
            imageView.layer.cornerRadius = 2.5//imageView.frame.size.height / CGFloat(2.0)
            imageView.layer.borderWidth = 0.5
            imageView.layer.borderColor = UIColor.init(white: 0.837, alpha: 1.000).cgColor
            imageView.clipsToBounds = true
            imageView.loadImageNoProgressBar(url: URL(string: self.userModel.imageBase64 ))
            imageView.isUserInteractionEnabled = true
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageAvatarTapped(tapGestureRecognizer:)))
            imageView.isUserInteractionEnabled = true
            imageView.addGestureRecognizer(tapGestureRecognizer)
            
            formHeaderFooterView.contentView.addSubview(imageView)
            
            formHeaderFooterView.contentView.backgroundColor = UIColor("#fafafa")
            }.configure {
                $0.viewHeight = heightHeaderAvatar
        }
        
        // kiểm tra xem device support xac thuc bang touch or face id
        var errorPolicyLA:NSError?
        context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &errorPolicyLA)
        if #available(iOS 11.0, *) {
            if errorPolicyLA?.code == LAError.biometryNotAvailable.rawValue, UserDefaults.standard.bool(forKey: .keyEnableFaceID) {
                UserDefaults.standard.setValue(false, forKey: .keyEnableFaceID)
            }
        } else {
            if errorPolicyLA?.code == LAError.touchIDNotAvailable.rawValue, UserDefaults.standard.bool(forKey: .keyEnableFaceID) {
                UserDefaults.standard.setValue(false, forKey: .keyEnableFaceID)
            }
        }
        UserDefaults.standard.synchronize()
        
        var tempFaceIDRow:SwitchRowFormer<FormSwitchCell>?
        let faceIDRow = SwitchRowFormer<FormSwitchCell>() {[weak self] in
            $0.titleLabel.textColor = UIColor("#00aced")
            if #available(iOS 11.0, *) {
                let isTypeFaceID = self?.context.biometryType == .faceID
                $0.titleLabel.text = (isTypeFaceID ? "FaceID Authentication" : "TouchID Authentication").localizedString()
            } else {
                $0.titleLabel.text = "TouchID Authentication".localizedString()
            }
            $0.titleLabel.font = UIFont.systemFont(ofSize: 14)
        }
        .configure(handler: { cell in
            tempFaceIDRow = cell
            cell.switched = UserDefaults.standard.bool(forKey: .keyEnableFaceID)
        })
        .onSwitchChanged {[weak self] isOn in
            guard let _self = self else {return}
            if isOn {
                var shouldGotoSettings = false
                if #available(iOS 11.0, *) {
                    if errorPolicyLA?.code == LAError.biometryNotAvailable.rawValue {
                        shouldGotoSettings = true
                    }
                } else {
                    if errorPolicyLA?.code == LAError.touchIDNotAvailable.rawValue {
                        shouldGotoSettings = true
                    }
                }
                if shouldGotoSettings {
                    guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                        return
                    }
                    
                    var titleAlert = ""
                    if #available(iOS 11.0, *) {
                        let isTypeFaceID = self?.context.biometryType == .faceID
                        titleAlert = (isTypeFaceID ? "FaceID Authentication" : "TouchID Authentication").localizedString()
                    } else {
                        titleAlert = "TouchID Authentication".localizedString()
                    }
                    
                    let alert = UIAlertController(title: titleAlert, message: "GO TO SETTINGS".localizedString(), preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Settings", style: .default, handler: { action in
                        UIApplication.shared.openURL(settingsUrl)
                    }))
                    alert.addAction(UIAlertAction(title: "CANCEL".localizedString(), style: .cancel, handler: nil))
                    self?.present(alert, animated: true, completion: nil)
                    tempFaceIDRow?.cellUpdate({ cell in
                        cell.switchButton.isOn = UserDefaults.standard.bool(forKey: .keyEnableFaceID)
                    })
                } else {
                    func taskUpdateRow() {
                        tempFaceIDRow?.cellUpdate({ cell in
                            cell.switchButton.isOn = UserDefaults.standard.bool(forKey: .keyEnableFaceID)
                        })
                    }
                    
                    _self.context = LAContext()
                    self?.deviceOwnerAuthentication(cancelTitle:"Cancel".localizedString(), context: _self.context) {
                        taskUpdateRow()
                    } onError: { _ in
                        // all fail is not available
                        taskUpdateRow()
                    } onNotAvailable: { isDeviceNotSupported in
                        // all fail is not available
                        taskUpdateRow()
                    }
                }
            } else {
                UserDefaults.standard.setValue(isOn, forKey: .keyEnableFaceID)
                UserDefaults.standard.synchronize()
                _self.context.invalidate()
                _self.refreshData()
            }
        }
        
        let feedbackEmailRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.sendMailTo(email: "support.crew@vietnamairlines.com")
        }
        (feedbackEmailRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (feedbackEmailRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAEnvelopeO, postfixText: String(format: "  %@", "Feedback".localizedString()), size: 14, iconSize: 16)
        
        let helpLinkRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.gotoLink(link: "https://crew.vn/vi/tin-tuc/Guide/2/337")
        }
        (helpLinkRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (helpLinkRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAQuestion, postfixText: String(format: "  %@", "User guide".localizedString()), size: 14, iconSize: 16)
        
        var logoutRow: RowFormer?
        
        if(self.userModel.userId == ServiceData.sharedInstance.userId) {
            logoutRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
                self?.former.deselect(animated: true)
                self?.logoutBarButtonItemPressed()
            }
            (logoutRow!.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
            (logoutRow!.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAPowerOff, postfixText: String(format: "  %@", "Deactive".localizedString()), size: 14, iconSize: 16)
        }
        
        
        let textFieldFullnameRowFormer = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.showProfileInfoPress(self!)
        }
        textFieldFullnameRowFormer.cellInstance.accessoryType = .disclosureIndicator
        (textFieldFullnameRowFormer.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (textFieldFullnameRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAUser, postfixText: String(format: "  %@", self.userModel.nameTV), size: 14, iconSize: 16)
        
        let scanQRCodeRow = createMenu("", UIColor("#166880"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.scanInModalAction(self!)
        }
        scanQRCodeRow.rowHeight = 90
        (scanQRCodeRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#166880")
        (scanQRCodeRow.cellInstance as! FormLabelCell).backgroundColor = UIColor("#FFFFE0")
        (scanQRCodeRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAQrcode, postfixText: String(format: "  %@", "Scan QR code".localizedString()), size: 14, iconSize: 16)
        
        let newSurveyListArchiveRow = createMenu("", UIColor.black, false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.newSurveyListArchivePress(self!)
        }
        
        newSurveyListArchiveRow.cellInstance.accessoryType = .disclosureIndicator
        (newSurveyListArchiveRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor.black
        (newSurveyListArchiveRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAPlus, postfixText: String(format: "  %@", "My Survey".localizedString()), size: 14, iconSize: 16)
        
        
        let showSurveyListArchiveRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.showSurveyListArchivePress(self!)
        }
        
        showSurveyListArchiveRow.cellInstance.accessoryType = .disclosureIndicator
        (showSurveyListArchiveRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showSurveyListArchiveRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "Survey (Offline)".localizedString()), size: 14, iconSize: 16)
        
        
        let showOJTListArchiveRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.showOJTListArchivePress(self!)
        }
        
        showOJTListArchiveRow.cellInstance.accessoryType = .disclosureIndicator
        (showOJTListArchiveRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showOJTListArchiveRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "OJT (Offline)".localizedString()), size: 14, iconSize: 16)
        
        let showAllFlightReportListRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.showAllFlightReportListPress(self!)
        }
        showAllFlightReportListRow.cellInstance.accessoryType = .disclosureIndicator
        (showAllFlightReportListRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showAllFlightReportListRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "All Reports".localizedString()), size: 14, iconSize: 16)
        
        let showDeviceListRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.showDeviceList(self!)
        }
        showDeviceListRow.cellInstance.accessoryType = .disclosureIndicator
        (showDeviceListRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showDeviceListRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "Devices".localizedString()), size: 14, iconSize: 16)
        
        let showCommandListRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.showCommandList(self!)
        }
        
        showCommandListRow.cellInstance.accessoryType = .disclosureIndicator
        (showCommandListRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showCommandListRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "Tools of AOT".localizedString()), size: 14, iconSize: 16)
        
        let showEmployeesListRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.showEmployeesList(self!)
        }
        
        showEmployeesListRow.cellInstance.accessoryType = .disclosureIndicator
        (showEmployeesListRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showEmployeesListRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "Employees".localizedString()), size: 14, iconSize: 16)
        
        let showStatisticListRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.pushStatisticViewController()
        }
        
        showStatisticListRow.cellInstance.accessoryType = .disclosureIndicator
        (showStatisticListRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showStatisticListRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "Statistics".localizedString()), size: 14, iconSize: 16)
        
        let showDashboardRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.pushWebViewDashboardViewController()
        }
        showDashboardRow.cellInstance.accessoryType = .disclosureIndicator
        (showDashboardRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showDashboardRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "Dashboard".localizedString()), size: 14, iconSize: 16)
        
        let showDiscoverRow = createMenu("", UIColor("#00aced"), false) { [weak self] in
            self?.former.deselect(animated: true)
            self?.pushWebViewDiscoverViewController()
        }
        showDiscoverRow.cellInstance.accessoryType = .disclosureIndicator
        (showDiscoverRow.cellInstance as! FormLabelCell).textLabel?.textColor = UIColor("#00aced")
        (showDiscoverRow.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FAList, postfixText: String(format: "  %@", "Discover".localizedString()), size: 14, iconSize: 16)

        let imageSection: SectionFormer = SectionFormer(rowFormers: [textFieldFullnameRowFormer]).set(headerViewFormer: avatarHeader)
        let section1 = SectionFormer(rowFormers: [scanQRCodeRow, showDeviceListRow, showCommandListRow]).set(headerViewFormer: createHeader(30, "".localizedString()))
        
        let section2 = SectionFormer(rowFormers: [showEmployeesListRow, showStatisticListRow, showDashboardRow, showDiscoverRow]).set(headerViewFormer: createHeader(30, "".localizedString()))
        
        let section3 = SectionFormer(rowFormers: [newSurveyListArchiveRow, showSurveyListArchiveRow, showOJTListArchiveRow, showAllFlightReportListRow]).set(headerViewFormer: createHeader(30, "".localizedString()))
        
        var rowFormersSection4 = [feedbackEmailRow, helpLinkRow, logoutRow!]
        // kiem tra biometry valid for this device, khi do moi them row face authentication
        // boi vi LAError.biometryNotAvailable same for not available & user denied reasons
        var shouldInsertFaceID = true
        if #available(iOS 11.0, *) {
            if errorPolicyLA?.localizedDescription.lowercased().contains("not available") == true {
                shouldInsertFaceID = false
            }
        } else {
            if errorPolicyLA?.localizedDescription.lowercased().contains("not available") == true {
                shouldInsertFaceID = false
            }
        }
        if shouldInsertFaceID {
            rowFormersSection4.insert(faceIDRow, at: 0)
        }
        let section4 = SectionFormer(rowFormers: rowFormersSection4).set(headerViewFormer: createHeader(30, "".localizedString()))
        var listSection5 = Array<RowFormer>()
        listSection5.append(textFieldVersionRowFormer)
        if(isChangeUrlAPI) {
            listSection5.append(textFieldUrlAPIRowFormer)
        }
        let section5 = SectionFormer(rowFormers: listSection5).set(headerViewFormer: createHeader(30, "".localizedString()))
        former.append(sectionFormer: imageSection, section1, section2, section3, section4, section5).onCellSelected { [weak self] _ in
                //self?.formerInputAccessoryView.update()
        }
        
        
        
        UITextField.appearance().tintColor = UIColor.black
        UITextView.appearance().tintColor = UIColor.black
        
        
        tableView.cellLayoutMarginsFollowReadableWidth = false
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func gotoLink(link: String) {
        UIApplication.shared.openURL(URL(string: link)!)
    }
    
    func sendMailTo(email: String) {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody("<p>Send feedback !</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    
    func logoutBarButtonItemPressed() {
        
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: { (UIAlertAction) -> Void in
            
            ServiceData.sharedInstance.userModel = nil
            UserDefaults.standard.setValue("", forKey: "token")
            ApplicationData.sharedInstance.deleteAllUserLogin()
            ApplicationData.sharedInstance.deleteAllMessageModel()
            ApplicationData.sharedInstance.deleteAllSurveyModel()
            ApplicationData.sharedInstance.deleteAllOJTModel()
            
            // Dai: invalidate face.touch id for this device
            UserDefaults.standard.setValue(false, forKey: .keyEnableFaceID)
            
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            
            //let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController")
            let splashController = self.storyboard?.instantiateViewController(withIdentifier: "SplashController")
            appDelegate?.window?.rootViewController = splashController
            appDelegate?.window?.makeKeyAndVisible()
            splashController?.view.alpha = 0.0
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                splashController?.view.alpha = 1.0
            })
            
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        
        self.showAlertWithAction("Are you want to logout ?".localizedString(), message: "", actions: [yesAction,cancelAction])
    }
    
    func saveAPIURL() {
        
        //1. Create the alert controller.
        let alert = UIAlertController(title: "Edit", message: "Change API URL", preferredStyle: .alert)
        //2. Add the text field. You can configure it however you need.
        alert.addTextField(configurationHandler: {(textField : UITextField!) -> Void in
            textField.placeholder = ""
            textField.text = ServiceData.sharedInstance.hostAPI
        })
        // 3. Grab the value from the text field, and print it when the user clicks OK.
        alert.addAction(UIAlertAction(title: "Save", style: .destructive, handler: { [weak alert] (_) in
            if let textField = alert?.textFields![0] {
                UserDefaults.standard.setValue(textField.text, forKey: "hostAPI")
                (self.textFieldUrlAPIRowFormer.cellInstance as! FormLabelCell).textLabel?.setFAText(prefixText: " ", icon: FAType.FACompress, postfixText: String(format: "  %@", ServiceData.sharedInstance.hostAPI), size: 14, iconSize: 16)
                self.showAlert("Save ok !!".localizedString(), stringContent: "")
            }// Force unwrapping because we know it exists.
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel".localizedString(), style: UIAlertAction.Style.default, handler: {[weak self] (UIAlertAction) -> Void in
            print("")
            
        }))
        // 4. Present the alert.
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    
    func buttonUpdateAuthorizePress(sender: UIButton) {
        
        let yesAction = UIAlertAction(title: "YES".localizedString(), style: UIAlertAction.Style.destructive, handler: {[weak self](UIAlertAction) -> Void in
            self?.updateAuthorizePress()
            
        })
        
        let cancelAction = UIAlertAction(title: "NO".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
            
        })
        let stringTitle = String(format: "%@ %@ ?", "Would you like to set the permission for".localizedString(), userModel.account)
        self.showAlertWithAction(stringTitle, message: "", actions: [yesAction,cancelAction])
        
    }
    
    func updateAuthorizePress() {
        
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightCrewtaskauthorize(flightId: self.flightId, sourceCrewID: self.sourceCrewID, destinationCrewID: self.destinationCrewID).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            if let message = task as? String {
                
                let cancelAction = UIAlertAction(title: "Close".localizedString(), style: UIAlertAction.Style.cancel, handler: { (UIAlertAction) -> Void in
                    Broadcaster.notify(LeadReportPositionDelegate.self) {
                        $0.refreshAuthorizeLeadReportPosition()
                    }
                    let _ = self.navigationController?.popViewController(animated: true)
                })
                
                self.showAlertWithAction("Alert".localizedString(), message: message, actions: [cancelAction])
            }
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    
    //MARK
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    
    func showAvatarProfile() {
        let photo = SKPhoto.photoWithImageURL(userModel.imageBase64)
        photo.caption = ""
        let browser = SKPhotoBrowser(photos: [photo], initialPageIndex: 0)
        present(browser, animated: true, completion: {})
    }
    
    @objc func imageAvatarTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        //let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        self.showAvatarProfile()
    }
    
    func verifyCode(codeScan: String) {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskDeviceVerifyQR(qrCode: codeScan).continueOnSuccessWith(continuation: { task in
            self.hideMBProgressHUD(true)
            if let responseServiceModel = task as? ResponseServiceModel {
                let message = responseServiceModel.message ?? "QR code active accepted full".localizedString()
                self.showAlert("Message".localizedString(), stringContent: message)
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    //
    
    
    func newSurveyListArchivePress(_ sender: AnyObject) {
        let scheduleFlyModel = ScheduleFlightModel.empty
        self.pushSurveyListViewController(scheduleFlyModel: scheduleFlyModel, isListSaved: false, isSameSchedule: true)
    }
    
    func showSurveyListArchivePress(_ sender: AnyObject) {
        self.pushSurveyListViewController(scheduleFlyModel: nil, isListSaved: true, isSameSchedule: true)
    }
    
    func showOJTListArchivePress(_ sender: AnyObject) {
        self.pushOJTListViewController(scheduleFlyModel: nil, isListSaved: true, cid: "")
    }
    
    func showAllFlightReportListPress(_ sender: AnyObject) {
        self.pushAllFlightReportListViewController()
    }
    
    func showDeviceList(_ sender: AnyObject) {
        self.pushListDeviceViewController()
    }
    
    func showCommandList(_ sender: AnyObject) {
        self.pushListCommandViewController()
    }
    
    func showProfileInfoPress(_ sender: AnyObject) {
        self.pushProfileViewController(crewId: self.crewId)
    }
    
    func showEmployeesList(_ sender: AnyObject) {
        self.pushEmployeesViewController()
    }
    
    //
    @IBAction func scanInModalAction(_ sender: AnyObject) {
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate = self
        
        readerVC.completionBlock = {[weak self] (result: QRCodeReaderResult?) in
            guard let `self` = self else {
                return
            }
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
                self.verifyCode(codeScan: result.value)
            }
            
            
        }
        present(readerVC, animated: true, completion: nil)
    }
    
    //MARK - QRCodeReaderViewControllerDelegate
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        
        dismiss(animated: true) { [weak self] in
            print("")
            /*
             let alert = UIAlertController(
             title: "QRCodeReader",
             message: String (format:"%@ (of type %@)", result.value, result.metadataType),
             preferredStyle: .alert
             )
             alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
             
             self?.present(alert, animated: true, completion: nil)
             */
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        print("Switching capturing to: \(newCaptureDevice.device.localizedName)")
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }

    
}
