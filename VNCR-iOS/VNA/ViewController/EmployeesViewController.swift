//
//  EmployeesViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 7/15/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON


class EmployeesViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, ContentTableViewCellDelegate {

    var pageNumber = 1
    
    var searchBar: UISearchBar = UISearchBar(frame: CGRect.zero) //CustomSearchBarView = CustomSearchBarView(frame: CGRect.zero)
    
    var listContents = [ContentModel]()
    
    var refreshControl: UIRefreshControl!
    
    var segmentedControl: UISegmentedControl = UISegmentedControl(frame: CGRect.zero)
    
    @IBOutlet weak var tableView: UITableView!
    
    var cancelBarButtonItem: UIBarButtonItem!
    
    var searchText = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.searchBar.sizeToFit()
        //self.navigationItem.setHidesBackButton(true, animated:true)
        //self.customBackButton(imageName: "back-icon.png", renderingMode: UIImageRenderingMode.alwaysTemplate)
        //self.navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        
        self.setupSearchBar()
        cancelBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(self.searchBarCancelButtonClicked(_:)))
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        tableView.addSubview(refreshControl)
        
        
        let nib = UINib(nibName: "ContentTableViewCell", bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: "ContentTableViewCell")
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 320
        tableView.emptyDataSetSource = self
        tableView.emptyDataSetDelegate = self
        tableView.keyboardDismissMode = .onDrag
        //tableView.tableHeaderView = UIView(frame: CGRect.zero)
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.tableView.alwaysBounceVertical = true
        
        self.tableView!.infiniteScrollIndicatorView = CustomInfiniteIndicator(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        self.tableView!.infiniteScrollIndicatorMargin = 40
        // Set custom trigger offset
        self.tableView!.infiniteScrollTriggerOffset = 2000
        self.tableView!.addInfiniteScroll { [weak self] (tableView) -> Void in
            self?.loadMoreData() {
                self?.tableView!.finishInfiniteScroll()
                self?.refreshControl?.endRefreshing()
            }
            
        }
        
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        getNewData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func refreshData() {
        getNewData()
    }
    
    func getNewData() {
        //self.textTitle = "DEVICES".localizedString()
        pageNumber = 1
        ServiceData.sharedInstance.taskUserGetEmployees(keyword: searchText).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<ContentModel>()
                for item in array {
                    let model = ContentModel(json: item)
                    arrayModel.append(model)
                }
                self.listContents = arrayModel
                
            }
            self.tableView.reloadData()
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
            self.refreshControl.endRefreshing()
        })
    }
    
    
    func setupSearchBar() {
        //self.searchBar.searchBarOriginX = 0
        //        self.searchBar.tintColor = .white
        //        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .black
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchBar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            self.searchBar.barTintColor = UIColor.white
            searchText = searchBar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        self.searchBar.tintColor = .darkText
        UITextField.appearance(whenContainedInInstancesOf: [type(of: self.searchBar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        self.searchBar.enablesReturnKeyAutomatically = false
        self.searchBar.placeholder = "Search".localizedString()
        searchBar.delegate = self
        let searchBarContainer = SearchBarContainerView(customSearchBar: self.searchBar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        self.navigationItem.titleView = searchBarContainer
    }
    
    //MARK - UISearchBarDelegate
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = true
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.rightBarButtonItems = nil
        if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
            self.navigationItem.rightBarButtonItem = cancelBarButtonItem
        }        
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchBar.showsCancelButton = false
    }
    
    public func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        if(self.searchBar.text == "" || self.searchBar.text == nil) {
            searchText = ""
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("")
        searchText = searchBar.text ?? ""
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        self.getNewData()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func configure(cell: ContentTableViewCell, at indexPath: IndexPath) {
        cell.contentModel = self.listContents[indexPath.row]
        cell.contentTableViewCellDelegate = self;
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listContents.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContentTableViewCell")! as! ContentTableViewCell
        //cell.accessoryType = .detailButton
        self.configure(cell: cell, at: indexPath)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.fd_heightForCell(withIdentifier: "ContentTableViewCell", cacheBy: indexPath, configuration: {[weak self] (cell) in
            let cell = cell as! ContentTableViewCell
            self?.configure(cell: cell, at: indexPath)
        })
        
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        let model = self.listContents[indexPath.row]
        self.pushDetailEmployeesViewController(id: model.id)
    }
    
    /*
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
     return 0.01
     }
     
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
     return 0.01
     }
     */
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func loadMoreData(_ handler: (() -> Void)?) {
        
        if(self.listContents.count > 1 && !self.refreshControl.isRefreshing){
            
            pageNumber += 1
            
            ServiceData.sharedInstance.taskUserGetEmployees(keyword: searchText, pageIndex: pageNumber).continueOnSuccessWith(continuation: { task in
                if let result = task as? JSON {
                    self.tableView?.beginUpdates()
                    var listIndexPath = Array<IndexPath>()
                    for item in result.array! {
                        listIndexPath.append(IndexPath(row: self.listContents.count, section: 0))
                        let model = ContentModel(json: item)
                        self.listContents.append(model)
                    }
                    self.tableView?.insertRows(at: listIndexPath, with: .bottom)
                    self.tableView?.endUpdates()
                }
                self.hideMBProgressHUD(true)
                handler?()
                
            }).continueOnErrorWith(continuation: { error in
                self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
                self.hideMBProgressHUD(true)
                handler?()
            })
            
        } else {
            handler?()
            
        }
        
    }
    
    //MARK - ContentTableViewCellDelegate
    
    func tapAvatarContentTableViewCell(cell: ContentTableViewCell) {
        let photo = SKPhoto.photoWithImageURL(cell.contentModel?.imageUrl ?? "")
        photo.caption = ""
        let browser = SKPhotoBrowser(photos: [photo], initialPageIndex: 0)
        present(browser, animated: true, completion: {})
    }
    
    func tapInfoButtonContentTableViewCell(cell: ContentTableViewCell) {
        if let model = cell.contentModel {
            self.pushDetailEmployeesViewController(id: model.id)
        }
    }
    
    
}
