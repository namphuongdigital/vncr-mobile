//
//  SurveyDynamicController.swift
//  VNA
//
//  Created by Pham Dai on 02/12/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class SurveyDynamicController: BaseViewController {

    @IBOutlet weak var vwProgress: UIView!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var lblTimeCountdown: UILabel!
    @IBOutlet weak var btnBack: Button5Corner!
    @IBOutlet weak var btnCenter: Button5Corner!
    @IBOutlet weak var btnNext: Button5Corner!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackPages: UIStackView!
    
    let tid:String
    
    var pageIndex:Int = 0
    var currentPage:SurveyPageView?
    
    var isOnlyResultPage:Bool = false
    
    var totalQuestions:Int = 0
    var totalAnswer:Int = 0 {
        didSet {
            progressBar.setProgress(Float(CGFloat(totalAnswer)/CGFloat(totalQuestions)), animated: true)
        }
    }
    
    var surveyPageModel:SurveyPageModel?
    
    init(tid:String) {
        self.tid = tid
        super.init(nibName: String(describing: SurveyDynamicController.self), bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        self.tid = ""
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnBack.setImage(UIImage(named: "back-icon")?.resizeImage(newSize: CGSize(width: 23, height: 23)).tint(with: .systemBlue), for: UIControl.State())
        self.btnBack.setTitle("Back".localizedString(), for: UIControl.State())
        self.btnBack.setTitleColor(.systemBlue, for: UIControl.State())
        
        scrollView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loadData()
    }

    private func presentData() {
        totalQuestions = 0
        surveyPageModel?.questions?.forEach({ question in
            totalQuestions += question.totalQuestions
        })
        configUI()
    }
    
    func loadData() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.getSurveyPage(id: tid)
            .continueOnSuccessWith(continuation: {task in
                self.hideMBProgressHUD(true)
            #if DEBUG
            print("\(task) \(#function)")
            #endif
            if let listData = try? (task as? JSON)?.rawData() {
                do {
                    let task:SurveyPageModel? = try listData.load()
                    self.surveyPageModel = task
                    self.presentData()
                } catch let err {
#if DEBUG
print("\(err) \(#function)")
#endif
                    self.showAlert("Error".localizedString(), stringContent: (err as NSError).localizedDescription)
                }
            }
        }).continueOnErrorWith(continuation: { error in
            self.hideMBProgressHUD(true)
            self.showAlert("Error".localizedString(), stringContent: (error as NSError).localizedDescription)
        })
    }
    
    func configUI() {
        
        stackPages.arrangedSubviews.forEach({$0.removeFromSuperview()})
        
        guard let surveyPageModel = surveyPageModel else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        isOnlyResultPage = surveyPageModel.questions?.count ?? 0 == 0
        
        let startPage = SurveyPageView(question: SurveyQuestion.empty, index: 0, isLast: false, otherQuestion: SurveyPageView.OtherQuestion(title: surveyPageModel.title, content: surveyPageModel.surveyPageModelDescription, titleButton: ( isOnlyResultPage ? "Done" : "Start").localizedString(), backgroundButton: UIColor.systemBlue, colorTitleButton: UIColor.white), delegate: self)
        
        stackPages.addArrangedSubview(startPage)
        startPage.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
        
        if let questions = surveyPageModel.questions {
            questions.enumerated().forEach({ (offset,question) in
                let page = SurveyPageView(question: question, index: offset + 1, isLast: offset == questions.count - 1, otherQuestion: nil, delegate: self)
                stackPages.addArrangedSubview(page)
            })
        }
        
        self.currentPage = startPage // set thu cong currentpage cho nhung page tu them
        configNavi(page: startPage)
    }
    
    func scrollPage() {
        let page:CGFloat = CGFloat(self.pageIndex)
        scrollView.setContentOffset(CGPoint(x: page * scrollView.frame.width, y: 0), animated: true)
    }
    
    func configNavi(page:SurveyPageView) {
        
        self.btnCenter.isHidden = true
        self.btnBack.isHidden = true
        self.btnNext.isHidden = true
        self.btnNext.layer.borderWidth = 0
        
//        UIView.animate(withDuration: 0.2, delay: 0, options: [.curveLinear]) {[weak self] in guard let `self` = self else { return }
            if let otherQuestion = page.otherQuestion {// setup for other information
                self.btnCenter.isHidden = false
                self.btnCenter.setTitle(otherQuestion.titleButton, for: UIControl.State())
                self.btnCenter.setTitleColor(otherQuestion.colorTitleButton, for: UIControl.State())
                self.btnCenter.setBackground(normal: (otherQuestion.backgroundButton ?? UIColor.systemBlue).imageRepresentation, highlighted: UIColor.gray.imageRepresentation)
                self.vwProgress.isHidden = true
            } else {
                self.vwProgress.isHidden = false
                if page.isLast {
                    self.btnBack.isHidden = false
                    
                    self.btnNext.setTitle("Submit".localizedString(), for: UIControl.State())
                    self.btnNext.setTitleColor(.white, for: UIControl.State())
                    self.btnNext.setBackground(normal: UIColor.systemRed.imageRepresentation, highlighted: UIColor.gray.imageRepresentation)
                }
                else {
                    self.btnBack.isHidden = false
                    
                    self.btnNext.setTitle("Next".localizedString(), for: UIControl.State())
                    self.btnNext.setTitleColor(.black, for: UIControl.State())
                    self.btnNext.setBackground(normal: UIColor("#EBEBEB").imageRepresentation, highlighted: UIColor.clear.imageRepresentation)
                    self.btnNext.layer.borderColor = UIColor.black.cgColor
                    self.btnNext.layer.borderWidth = 1
                    
                }
                self.validateChanged(page:page)
            }
//            self.view.layoutIfNeeded()
//        } completion: { bool in
//
//        }
    }
    
    private func validateChanged(page:SurveyPageView) {
        self.currentPage = page
        let hasAnswer = page.question.hasAnswer
        btnNext.isHidden = !hasAnswer
        var totalAnswer = 0
        surveyPageModel?.questions?.forEach({ question in
            totalAnswer += question.totalAnswer
        })
        self.totalAnswer = totalAnswer
    }
    
    @IBAction func action(_ sender: UIButton) {
        guard let page = currentPage else {return}
        switch sender {
        case btnBack:
            self.pageIndex -= 1
            scrollPage()
        case btnCenter:
            self.pageIndex += 1
            scrollPage()
        case btnNext:
            if isOnlyResultPage {
                self.dismiss(animated: true, completion: nil)
            } else {
                if page.isLast {
                    submit()
                }
                else {
                    self.pageIndex += 1
                    scrollPage()
                }
            }
        default:break
        }
    }
    
    private func submit() {
        guard let surveyPageModel = surveyPageModel else {
            return
        }

        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.updateSurveyPage(surveyPage: surveyPageModel)
            .continueOnSuccessWith(continuation: {task in
                self.hideMBProgressHUD(true)
            #if DEBUG
            print("\(task) \(#function)")
            #endif
            if let listData = try? (task as? JSON)?.rawData() {
                do {
                    let task:SurveyPageModel? = try listData.load()
                    self.surveyPageModel = task
                    self.presentData()
                } catch let err {
#if DEBUG
print("\(err) \(#function)")
#endif
                    self.showAlert("Error".localizedString(), stringContent: (err as NSError).localizedDescription)
                }
            }
        }).continueOnErrorWith(continuation: { error in
            self.hideMBProgressHUD(true)
            self.showAlert("Error".localizedString(), stringContent: (error as NSError).localizedDescription)
        })
    }
}

extension SurveyDynamicController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if pageIndex < stackPages.arrangedSubviews.count,
           let page = stackPages.arrangedSubviews[pageIndex] as? SurveyPageView {
            configNavi(page: page)
        }
    }
}

extension SurveyDynamicController: SurveyPageViewDelegate {
    
    func SurveyPageView_validateChanges(page: SurveyPageView) {
        validateChanged(page: page)
    }
}
