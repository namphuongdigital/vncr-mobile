//
//  SurveyPageView.swift
//  VNA
//
//  Created by Pham Dai on 02/12/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import Photos
import WebKit
import AVKit
import AVFoundation

protocol SurveyPageViewDelegate:AnyObject {
    func SurveyPageView_validateChanges(page:SurveyPageView)
}

class SurveyPageView: BaseView {

    struct OtherQuestion {
        let title:String?
        let content:String?
        let titleButton:String?
        let backgroundButton:UIColor?
        let colorTitleButton:UIColor?
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackContent: UIStackView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwAns: UIView!
    @IBOutlet weak var stackANs: UIStackView!
    
    weak var delegate:SurveyPageViewDelegate?
    var wkContent:WKWebView = WKWebView(frame: .zero)
    
    var question:SurveyQuestion
    var originalObject:SurveyQuestion?
    var index:Int
    var isLast:Bool
    
    var otherQuestion:OtherQuestion?
    
    var wkLoadDone = false
    
    init(question:SurveyQuestion, index:Int, isLast:Bool, otherQuestion:OtherQuestion?,delegate:SurveyPageViewDelegate?) {
        self.isLast = isLast
        self.index = index
        self.question = question
        self.originalObject = question.copy()// stored to check value changed
        self.otherQuestion = otherQuestion
        self.delegate = delegate
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.question = SurveyQuestion.empty
        self.index = 0
        self.isLast = false
        super.init(coder: aDecoder)
    }
    
    override func config() {
        
        scrollView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 150, right: 0)
        
        lblTitle.font = .systemFont(ofSize: 20, weight: .bold)
        lblTitle.textColor = .black
        
        self.wkContent.navigationDelegate = self
        self.wkContent.isOpaque = true
        self.wkContent.backgroundColor = .white
        wkContent.scrollView.showsVerticalScrollIndicator = false
        wkContent.scrollView.showsHorizontalScrollIndicator = false
        wkContent.scrollView.isScrollEnabled = false
        wkContent.scrollView.backgroundColor = .white
        wkContent.scrollView.bounces = false
        stackContent.addArrangedSubview(wkContent)
        wkContent.heightAnchor.constraint(equalToConstant: 10).isActive = true
        
        configUI()
    }
    
    func configUI() {
        
        var title = question.title
        var content = question.content
        
        if let otherQuestion = otherQuestion {
            title = otherQuestion.title
            content = otherQuestion.content
        }
        
        lblTitle.text = title
        self.wkContent.loadHTMLString(WebViewUtil.getHtmlData(content ?? "", color: "#000000",bgColor: "#ffffff"), baseURL: nil)
        
        vwAns.isHidden = !question.haveAns
        
        // add ans control
        if question.haveAns {
            question.ansRadios?.forEach({ ans in
                let ansView = SelectedControl(type: .radio, value: SelectedControl.Value(content: ans.caption, isSelected: ans.val ?? false, identifier: ans.idString), frame: .zero) {[weak self] ansView in guard let `self` = self else { return }
                    self.question.ansRadios?.forEach({$0.val = false})
                    self.question.ansRadios?.first(where: {ansView.value.identifier == $0.idString })?.val = ansView.value.isSelected
                    let listRadios = self.stackANs.arrangedSubviews.filter({view in
                        if let view = view as? SelectedControl,
                           view.type == .radio {
                            return true
                        }
                        return false
                    })
                    listRadios.forEach({ view in
                        if let value = self.question.ansRadios?.first(where: { (view as? SelectedControl)?.value.identifier == $0.idString }) {
                            (view as? SelectedControl)?.value = SelectedControl.Value(content: value.caption, isSelected: value.val ?? false, identifier: value.idString)
                            (view as? SelectedControl)?.refreshUI()
                        }
                    })
                    self.delegate?.SurveyPageView_validateChanges(page: self)
                }
                stackANs.addArrangedSubview(ansView)
            })
            
            question.ansCheckboxes?.forEach({ ans in
                let ansView = SelectedControl(type: .checkbox, value: SelectedControl.Value(content: ans.caption, isSelected: ans.val ?? false, identifier: ans.idString), frame: .zero) {[weak self] ansView in guard let `self` = self else { return }
                    self.question.ansCheckboxes?.first(where: {ansView.value.identifier == $0.idString })?.val = ansView.value.isSelected
                    self.delegate?.SurveyPageView_validateChanges(page: self)
                }
                stackANs.addArrangedSubview(ansView)
            })
            
            question.ansNumbers?.forEach({ ans in
                let ansView = SectionEditable(type: .number, value: SectionEditable.Value(content: ans.val != nil ? "\(ans.val!)" : nil, title: ans.caption, identifier: ans.idString), textDidChange: {[weak self] ansView in guard let `self` = self else { return }
                    self.question.ansNumbers?.first(where: {ansView.value.identifier == $0.idString })?.val = ansView.getVal != nil ? Double(ansView.getVal!) : nil
                    self.delegate?.SurveyPageView_validateChanges(page: self)
                })
                stackANs.addArrangedSubview(ansView)
            })
            
            question.ansTexts?.forEach({ ans in
                let ansView = SectionEditable(type: .normal, value: SectionEditable.Value(content: ans.val, title: ans.caption, identifier: ans.idString), textDidChange: {[weak self] ansView in guard let `self` = self else { return }
                    self.question.ansTexts?.first(where: {ansView.value.identifier == $0.idString })?.val = ansView.getVal
                    self.delegate?.SurveyPageView_validateChanges(page: self)
                })
                stackANs.addArrangedSubview(ansView)
            })
            
            question.ansDateTimes?.forEach({ ans in
                let ansView = SectionPicker(value: SectionPicker.Value(title: ans.caption, dateVal: ans.valDate, timeVal: ans.valTime, showDate: ans.dateVisible, showTime: ans.timeVisible, identifier: ans.idString)) {[weak self] ansView in guard let `self` = self else { return }
                    if let number = self.question.ansDateTimes?.first(where: {ansView.value.identifier == $0.idString }) {
                        number.valTime = ansView.value.timeVal
                        number.valDate = ansView.value.dateVal
                    }
                    self.delegate?.SurveyPageView_validateChanges(page: self)
                }
                stackANs.addArrangedSubview(ansView)
            })
            
            if let ans = question.ansExtra {
                let ansView = SectionEditable(type: .normal, value: SectionEditable.Value(content: ans.val, title: ans.caption, identifier: ans.idString), textDidChange: {[weak self] ansView in guard let `self` = self else { return }
                    self.question.ansExtra?.val = ansView.getVal
                    self.delegate?.SurveyPageView_validateChanges(page: self)
                })
                stackANs.addArrangedSubview(ansView)
            }
        }
    }
}

extension SurveyPageView: WKNavigationDelegate {
    
    func reConfigAdsInWebPage() {
        self.wkContent.contentHeight {[weak self] (h) in
            guard let _self = self else {return}
            var contentHeight = NSNumber.init(value: h)
            if let heightConstraint = _self.wkContent.getHeightConstraint() {
                let min:Float = 50
                if contentHeight.floatValue < min {
                    contentHeight = NSNumber(value: min)
                }
                heightConstraint.constant = CGFloat(contentHeight.floatValue)
                UIView.animate(withDuration: 0.1, animations: {
                    self?.view.layoutIfNeeded()
                })
            }
        }
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        reConfigAdsInWebPage()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        reConfigAdsInWebPage()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let request = navigationAction.request
        
        if request.url?.absoluteString.contains("gdr_forced_update") ?? false {
            DispatchQueue.main.async {
                self.reConfigAdsInWebPage()
            }
            decisionHandler(.cancel)
            return
        }
        
        if (navigationAction.navigationType == .linkActivated || navigationAction.navigationType == .other) && wkLoadDone{
            if let url = request.url?.absoluteString {
                if url.contains(".jpg") || url.contains(".png") || url.contains(".gif") || url.contains(".jpeg"){
                    
                    let vc = SKPhotoBrowser(photos: [SKPhoto(url: url)])
                    UIApplication.shared.delegate?.window??.visibleViewController?.present(vc, animated: true, completion:nil)
                    
                } else if  url.contains(".mp4") {
                    var headers = request.allHTTPHeaderFields
                    headers?["Accept-Ranges"] = "bytes"
                    let customRuquest = NSMutableURLRequest(url: request.url!, cachePolicy: NSURLRequest.CachePolicy.useProtocolCachePolicy, timeoutInterval: 60)
                    customRuquest.allHTTPHeaderFields = headers
                    playVideo(url: customRuquest.url!)
                } else if  url.contains("about:blank") {
                    decisionHandler(.allow)
                    return
                }
            }
                
            decisionHandler(.cancel)
            return
        }
        
        decisionHandler(.allow)
    }
}

extension SurveyPageView {
    private func playVideo(url:URL) {
        let avasset = AVURLAsset(url: url)
        let item = AVPlayerItem(asset: avasset)
        let player = AVPlayer(playerItem: item)
        let playerController = AVPlayerViewController()
        playerController.player = player
        UIApplication.shared.delegate?.window??.visibleViewController?.present(playerController, animated: true) {
            player.play()
        }
    }
}
