//
//  AssessmentCrewDutyViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import FTPopOverMenu
import SwiftyJSON

class AssessmentCrewDutyViewController: FormViewController, AssessmentItemTableViewCellDelegate {
    
    var sendBarButtonItem: UIBarButtonItem!
    
    var crewDutyModel: CrewDutyModel!
    
    var assessmentModel: AssessmentModel!
    
    var infoCrewDutyTableViewCell: CustomRowFormer<InfoCrewDutyTableViewCell>!
    
    var strengthCrewDutyTableViewCell: CustomRowFormer<ContentReportTableViewCell>!
    
    var weaknessCrewDutyTableViewCell: CustomRowFormer<ContentReportTableViewCell>!
    
    var sectionFormer: SectionFormer!
    
    var updateCrewDutyTableViewCell: CustomRowFormer<UpdateReportTableViewCell>!
    
    var totalScore: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "ASSESSMENT".localizedString()
        
        sendBarButtonItem = UIBarButtonItem(title: "", style: .done, target: self, action: #selector(self.sendBarButtonItemPressed(_:)))
        sendBarButtonItem.setFAIcon(icon: .FACheckCircle, iconSize: 20)
        self.navigationItem.rightBarButtonItems = [self.sendBarButtonItem]
        totalScore = self.crewDutyModel.totalScore
        self.getNewData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func sendBarButtonItemPressed(_ sender: UIBarButtonItem) {
        self.updateAllToServer()
    }
    

    func getNewData() {
        self.showMBProgressHUD("Loading...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightAssessmentgetinfo(flightId: crewDutyModel.flightID, destinationCrewID: crewDutyModel.crewID, destinationJob: crewDutyModel.foJob).continueOnSuccessWith(continuation: { task in
            
            if let result = task as? JSON {
                let assessmentModel = AssessmentModel(json: result)
                self.assessmentModel = assessmentModel
                self.hideMBProgressHUD(true)
                self.loadViewData()
            }
            
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    func updateAllToServer() {
        
        let strength = (strengthCrewDutyTableViewCell.cellInstance as? ContentReportTableViewCell)?.textViewContent.text ?? ""
        let weaknes = (weaknessCrewDutyTableViewCell.cellInstance as? ContentReportTableViewCell)?.textViewContent.text ?? ""
        
        
        self.showMBProgressHUD("Updating...".localizedString(), animated: true)
        ServiceData.sharedInstance.taskFlightAssessmentUpdateInfo(flightId: self.assessmentModel.flightID, destinationCrewID: crewDutyModel.crewID, destinationJob: crewDutyModel.foJob, lessonID: self.assessmentModel.lessonID, assessmentItems: self.assessmentModel.assessmentItems, strength: strength, weaknes: weaknes).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            let crewDutyModel = CrewDutyModel(json: result)
            Broadcaster.notify(CrewDutyModelDelete.self) {
                $0.refreshCrewDuty(crewDutyModel)
            }
            
            self.hideMBProgressHUD(true)
            let _ = self.navigationController?.popViewController(animated: true)
            
        }).continueOnErrorWith(continuation: { error in
            self.showAlert("Alert".localizedString(), stringContent: (error as NSError).localizedDescription)
            self.hideMBProgressHUD(true)
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func loadViewData() {
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "CREATE ASSESSMENT".localizedString()
        
        var nibInfoCrewDutyTableViewCell = "InfoCrewDutyTableViewCell"
        var nibStrengthReportTableViewCell = "ContentReportTableViewCell"
        var nibWeaknessReportTableViewCell = "ContentReportTableViewCell"
        var nibUpdateReportTableViewCelll = "UpdateReportTableViewCell"
        var nibAssessmentItemTableViewCell = "AssessmentItemTableViewCell"
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            nibInfoCrewDutyTableViewCell = String(format: "%@_iphone", nibInfoCrewDutyTableViewCell)
            nibStrengthReportTableViewCell = String(format: "%@_iPhone", nibStrengthReportTableViewCell)
            nibWeaknessReportTableViewCell = String(format: "%@_iPhone", nibWeaknessReportTableViewCell)
            nibUpdateReportTableViewCelll = String(format: "%@_iPhone", nibUpdateReportTableViewCelll)
            nibAssessmentItemTableViewCell = String(format: "%@_iphone", nibAssessmentItemTableViewCell)
            
        }
        var arrayRowFormer = [RowFormer]()
        infoCrewDutyTableViewCell = CustomRowFormer<InfoCrewDutyTableViewCell>(instantiateType: .Nib(nibName: nibInfoCrewDutyTableViewCell)) {[weak self] in
            guard let `self` = self else {
                return
            }
            $0.loadData(crewDutyModel: self.crewDutyModel, totalScore: self.totalScore)
            print($0.textLabel?.text)
            }.configure {
                if(UIDevice.current.userInterfaceIdiom == .phone) {
                    $0.rowHeight = 110
                } else {
                    $0.rowHeight = 180
                }
                
        }
        arrayRowFormer.append(infoCrewDutyTableViewCell)
        
       
        for item in self.assessmentModel.assessmentItems {
            let cell = CustomRowFormer<AssessmentItemTableViewCell>(instantiateType: .Nib(nibName: nibAssessmentItemTableViewCell)) {[weak self] in
                print($0.textLabel?.text)
                //$0.loadData(assessmentItemModel: item)
                }.configure {
                    if(UIDevice.current.userInterfaceIdiom == .phone) {
                        $0.rowHeight = 90
                    } else {
                        $0.rowHeight = 80
                    }
                    
            }
            (cell.cellInstance as? AssessmentItemTableViewCell)?.loadData(assessmentItemModel: item)
            (cell.cellInstance as? AssessmentItemTableViewCell)?.delegate = self
            arrayRowFormer.append(cell)
        }
        
        strengthCrewDutyTableViewCell = CustomRowFormer<ContentReportTableViewCell>(instantiateType: .Nib(nibName: nibStrengthReportTableViewCell)) {[weak self] in
            print($0.textLabel?.text)
            $0.textViewContent.text = self?.assessmentModel.strength
            $0.textViewContent.placeholder = "Enter text".localizedString()
            $0.labelContent.text = "Strength".localizedString()
            //$0.textViewContent.textColor = UIColor.gray
            //$0.textViewContent.text = self?.taskModel.descriptionContent
            //$0.textViewContent.isEditable = false
            }.configure {
                if(UIDevice.current.userInterfaceIdiom == .phone) {
                    $0.rowHeight = 125
                } else {
                    $0.rowHeight = 150
                }
        }
        arrayRowFormer.append(strengthCrewDutyTableViewCell)
        
        weaknessCrewDutyTableViewCell = CustomRowFormer<ContentReportTableViewCell>(instantiateType: .Nib(nibName: nibStrengthReportTableViewCell)) {[weak self] in
            print($0.textLabel?.text)
            $0.textViewContent.text = self?.assessmentModel.weakness
            $0.textViewContent.placeholder = "Enter text".localizedString()
            $0.labelContent.text = "Weakness".localizedString()
            //$0.textViewContent.textColor = UIColor.gray
            //$0.textViewContent.text = self?.taskModel.descriptionContent
            //$0.textViewContent.isEditable = false
            }.configure {
                if(UIDevice.current.userInterfaceIdiom == .phone) {
                    $0.rowHeight = 125
                } else {
                    $0.rowHeight = 150
                }
        }
        arrayRowFormer.append(weaknessCrewDutyTableViewCell)
        
        updateCrewDutyTableViewCell = CustomRowFormer<UpdateReportTableViewCell>(instantiateType: .Nib(nibName: nibUpdateReportTableViewCelll)) {[weak self] in
            print($0.textLabel?.text)
            //$0.buttonUpdate.addTarget(self, action: #selector(self?.buttonUpdateReportPress(sender:)), for: .touchUpInside)
            
            }.configure {
                $0.rowHeight = 60
        }
        
        let createHeader: ((String, _ height: CGFloat) -> ViewFormer) = { text, height in
            return LabelViewFormer<FormLabelHeaderView>() {
                $0.contentView.backgroundColor = UIColor.clear
                $0.titleLabel.font = UIFont.systemFont(ofSize: 14)
                }.configure {
                    $0.viewHeight = height
                    $0.text = text
                    
            }
        }
        
        
        let customRowFormerSection = SectionFormer(rowFormers: arrayRowFormer).set(headerViewFormer: createHeader("", 0))
        /*
         let customRowFormerSection2 = SectionFormer(rowFormer: updateReportTableViewCell).set(headerViewFormer: createHeader("", 40))
         */
        former.append(sectionFormer: customRowFormerSection)
        self.tableView.separatorStyle = .none
        self.tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0.01))
        self.tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: CGFloat.leastNormalMagnitude))
    }
    
    
    //MARK - AssessmentItemTableViewCellDelegate
    func showPopupSelected(sender: UIButton, tableViewCell: UITableViewCell, array: Array<AnyObject>?, assessmentItemModel: AssessmentItemModel) {
        
        var arrayString = Array<String>()
        for index in 0..<11 {
            arrayString.append(String(format: "%d", index))
        }
        FTPopOverMenu.show(forSender: sender, withMenu: arrayString, menuType: FTPopOverMenuType.square, doneBlock: {[weak self] (index) in
            
            guard let `self` = self else {
                return
            }
            assessmentItemModel.score = index
            sender.setTitle(String(format: "%d", index), for: UIControlState())
            self.updateTotalScore()
            }, dismiss: {
                print("")
        })
    }
    
    func updateTotalScore() {
        var total: Float = 0
        for item in self.assessmentModel.assessmentItems {
            total = total + Float(item.score)
        }
        total = total / Float(self.assessmentModel.assessmentItems.count)
        total.round()
        totalScore = Int(total)
        (infoCrewDutyTableViewCell.cellInstance as! InfoCrewDutyTableViewCell).loadData(crewDutyModel: crewDutyModel, totalScore: totalScore)
    }
    
    
}
