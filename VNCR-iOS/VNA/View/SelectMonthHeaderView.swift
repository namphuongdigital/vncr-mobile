//
//  SelectMonthHeaderView.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/14/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

protocol SelectMonthHeaderViewDelegate : NSObjectProtocol {
    func selectedMonth(month: Int)
}

class SelectMonthHeaderView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var viewLine: UIView!
    
    weak var delegate: SelectMonthHeaderViewDelegate?

    var listItemSelect = Array<ItemSelect>()
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func loadView() -> SelectMonthHeaderView? {
//        var nibNamed = "SelectMonthHeaderView"
//        if(UIDevice.current.userInterfaceIdiom == .phone) {
//            nibNamed = "SelectMonthHeaderView_iphone"
//        }
        let nibNamed = "SelectMonthHeaderView_iphone"
        if let view = Bundle.main.loadNibNamed(nibNamed, owner: nil, options: nil)?.first as? SelectMonthHeaderView {
            let nib = UINib(nibName: "RadioCollectionViewCell", bundle: nil)
            view.collectionView.register(nib, forCellWithReuseIdentifier: "RadioCollectionViewCell")
            view.collectionView.allowsMultipleSelection = false
            view.viewLine.frame = CGRect(x: view.viewLine.frame.origin.x, y: view.viewLine.frame.origin.y, width: view.viewLine.frame.size.width, height: 0.5)
            for _ in 0..<14 {
                let itemSelect = ItemSelect()
                view.listItemSelect.append(itemSelect)
            }
            view.collectionView.reloadData()
            return view
        }
        return nil
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listItemSelect.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RadioCollectionViewCell", for: indexPath as IndexPath) as! RadioCollectionViewCell
        let itemSelect = self.listItemSelect[indexPath.row]
        let month = indexPath.row
        var string = ""
        if month == 0 {
            let date = Date()
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            string = String(format: "12.%d", year - 1).localizedString()
        } else if month == listItemSelect.count - 1 {
            let date = Date()
            let calendar = Calendar.current
            let year = calendar.component(.year, from: date)
            string = String(format: "01.%d", year + 1).localizedString()
        } else {
            string = String(format: "Month %d", month).localizedString()
        }
        
        cell.radioButton.setTitle(string, for: UIControl.State())
        cell.loadData(itemSelect: itemSelect)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0..<14 {
            self.listItemSelect[i].isSelected = false
        }
        self.listItemSelect[indexPath.row].isSelected = true
        self.collectionView.reloadSections(IndexSet.init(integer: 0))

        self.delegate?.selectedMonth(month: indexPath.row)
    }
    
    func setSelected(index: Int) {
        self.collectionView(self.collectionView, didSelectItemAt: IndexPath.init(row: index, section: 0))
    }
}
