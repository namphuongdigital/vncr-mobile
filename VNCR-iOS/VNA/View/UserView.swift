//
//  UserView.swift
//  CAVA
//
//  Created by Van Trieu Phu Huy on 9/15/18.
//  Copyright © 2018 Van Trieu Phu Huy. All rights reserved.
//

import UIKit

class UserView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var imageView: UIImageViewProgress!
    
    @IBOutlet weak var buttonOverlay: UIButton!
    
    class func loadView() -> UserView? {
        var nibNamed = "UserView"
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            nibNamed = "UserView"
        }
        if let view = Bundle.main.loadNibNamed(nibNamed, owner: nil, options: nil)?.first as? UserView {
            view.imageView.clipsToBounds = true
            view.imageView.layer.cornerRadius = 20.0
            view.labelTitle.textColor = UIColor.white
            if(UIDevice.current.userInterfaceIdiom == .phone) {
                
            } else {
               
            }
            
            return view
        }
        return nil
    }

}
