//
//  SectionChartView.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 3/14/20.
//  Copyright © 2020 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class SectionChartView: UIView {
    
    //MARK: - Method
    public func showChart(barChartTitle: String, barChartItem: [ChartItemModel], pieChartTitle: String, pieChartItem: [ChartItemModel]) {
        self.listBarChartItems = barChartItem
        self.listPieChartItems = pieChartItem
        self.dynamicHeight = 0
        if self.listBarChartItems.count == 0 {
            self.barChartViewLine.isHidden = true
            self.barChartViewContain.isHidden = true
            self.barChartHeader.isHidden = true
        } else {
            self.barChartHeader.text = barChartTitle.isEmpty ? "Bar chart".localizedString() : barChartTitle
            self.showBarChart()
        }
        if self.listPieChartItems.count == 0 {
            self.pieChartViewLine.isHidden = true
            self.pieChartViewContain.isHidden = true
            self.pieChartHeader.isHidden = true
        } else {
            self.pieChartHeader.text = pieChartTitle.isEmpty ? "Pie chart".localizedString() : pieChartTitle
            self.showPieChart()
        }
        self.barChartViewContain.layoutIfNeeded()
        self.pieChartViewContain.layoutIfNeeded()
        self.layoutIfNeeded()
    }
    
    private func showBarChart() {
        
        if(barChartView == nil) {
            //let heightChange = self.barChartViewContain.bounds.height * 0.6
            //let widthChange = self.barChartViewContain.bounds.width * 0.8
            //let x = (self.barChartViewContain.bounds.width - widthChange)/2
            //barChartView = BarChartView(frame: CGRect(x: x, y: 0, width: widthChange, height: heightChange))
            barChartView = BarChartView(frame: self.barChartViewContain.bounds)
            barChartView.autoresizingMask = [.flexibleHeight, .flexibleWidth, .flexibleTopMargin, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin];
            self.barChartViewContain.addSubview(barChartView)
        }
        barChartView.clear()
        barChartViewLine.isHidden = false
        barChartViewContain.isHidden = false
        barChartHeader.isHidden = false
        barChartView.chartDescription?.font = UIFont.systemFont(ofSize: 14)
        barChartView.chartDescription?.text = ""//"Biểu đồ thể hiện tiến độ công việc"
        var arrayBarChartDataEntry = Array<BarChartDataEntry>()
        var listChartColor = [UIColor]()
        var listValueChartColor = [UIColor]()
        var listValueLabel = [String]()
        
        for i in 0..<self.listBarChartItems.count {
            let item = listBarChartItems[i]
            let entry = BarChartDataEntry.init(x: Double(i), y: item.val, data: item.title as AnyObject)
            arrayBarChartDataEntry.append(entry)
            listChartColor.append(UIColor(item.color))
            listValueChartColor.append(UIColor(item.textColor))
            listValueLabel.append(item.title)
        }
        
        let dataSet = BarChartDataSet(values: arrayBarChartDataEntry, label: "")
        dataSet.colors = listChartColor
        dataSet.valueColors = listValueChartColor
        //dataSet.stackLabels = listValueLabel
        dataSet.valueFont = UIFont.systemFont(ofSize: 12)
        
        let chartData = BarChartData(dataSet: dataSet)
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            chartData.setValueFont(UIFont.systemFont(ofSize: 10))
        } else {
            chartData.setValueFont(UIFont.systemFont(ofSize: 12))
        }
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .none
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 1.0
        //pFormatter.percentSymbol = " %"
        
        let xAxis = barChartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.labelCount = listValueLabel.count
        xAxis.valueFormatter = IndexAxisValueFormatter(values: listValueLabel)
        
        chartData.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        /*
         //barChartView.xAxis.valueFormatter = formatter
         //barChartView.xAxis.granularity = 1
         barChartView.xAxis.labelCount = labels.count
         barChartView.xAxis.labelPosition = .bottom
         barChartView.xAxis.valueFormatter = IndexAxisValueFormatter(values: labels)
         barChartView.xAxis.forceLabelsEnabled = true
         barChartView.xAxis.granularity = 0.1
         barChartView.xAxis.granularityEnabled = true
         */
        barChartView.data = chartData
        self.dynamicHeight += self.barChartHeader.frame.height
        self.dynamicHeight += self.barChartViewLine.frame.height
        self.dynamicHeight += self.barChartViewContain.frame.height
    }
    
    private func showPieChart() {
        if(pieChartView == nil) {
            //let heightChange = self.pieChartViewContain.bounds.height * 0.75
            //let widthChange = self.pieChartViewContain.bounds.width * 0.75
            //let x = (self.pieChartViewContain.bounds.width - widthChange)/2
            //pieChartView = PieChartView(frame: CGRect(x: x, y: 0, width: widthChange, height: heightChange))
            pieChartView = PieChartView(frame: self.pieChartViewContain.bounds)
            pieChartView.autoresizingMask = [.flexibleHeight, .flexibleWidth, .flexibleTopMargin, .flexibleBottomMargin, .flexibleRightMargin, .flexibleLeftMargin];
            self.pieChartViewContain.addSubview(pieChartView)
        }
        pieChartView.clear()
        pieChartViewLine.isHidden = false
        pieChartViewContain.isHidden = false
        pieChartHeader.isHidden = false
        pieChartView.chartDescription?.font = UIFont.systemFont(ofSize: 14)
        pieChartView.chartDescription?.text = ""//"Biểu đồ thể hiện tiến độ công việc"
        var listChartDataEntry = [ChartDataEntry]()
        var listChartColor = [UIColor]()
        var listValueChartColor = [UIColor]()
        for i in 0..<self.listPieChartItems.count {
            let dataEntry = PieChartDataEntry(value: Double(CGFloat(self.listPieChartItems[i].val) / 100.0), label: self.listPieChartItems[i].title)
            listChartDataEntry.append(dataEntry)
            listChartColor.append(UIColor(self.listPieChartItems[i].color))
            listValueChartColor.append(UIColor(self.listPieChartItems[i].textColor))
        }
        let dataSet = PieChartDataSet(values: listChartDataEntry, label: "")
        dataSet.colors = listChartColor
        dataSet.valueColors = listValueChartColor
        dataSet.sliceSpace = 2.0
        
        let data = PieChartData(dataSet: dataSet)
        
        let pFormatter = NumberFormatter()
        pFormatter.numberStyle = .percent
        pFormatter.maximumFractionDigits = 1
        pFormatter.multiplier = 100.0
        pFormatter.percentSymbol = " %"
        
        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
        data.setValueFont(UIFont.systemFont(ofSize: 11))
        data.setValueTextColor(listValueChartColor.first ?? UIColor.white)
        pieChartView.data = data
        pieChartView.highlightValue(nil)
        self.dynamicHeight += self.pieChartHeader.frame.height
        self.dynamicHeight += self.pieChartViewLine.frame.height
        self.dynamicHeight += self.pieChartViewContain.frame.height
    }
    
    //MARK: - For Test
    func loadDataBarChart() {
        ServiceData.sharedInstance.taskStatisticDetailBar(id: -32805588).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<ChartItemModel>()
                for item in array {
                    let model = ChartItemModel(json: item)
                    arrayModel.append(model)
                }
                self.listBarChartItems = arrayModel
            }
            self.pieChartViewContain.isHidden = false
            self.showBarChart()
            
        }).continueOnErrorWith(continuation: { error in
            
        })
    }
    
    func loadDataPieChart() {
        ServiceData.sharedInstance.taskStatisticDetailPie(id: -32805588).continueOnSuccessWith(continuation: { task in
            
            let result = task as! JSON
            if let array = result.array {
                var arrayModel = Array<ChartItemModel>()
                for item in array {
                    let model = ChartItemModel(json: item)
                    arrayModel.append(model)
                }
                self.listPieChartItems = arrayModel
            }
            self.barChartViewContain.isHidden = false
            self.showPieChart()
            
        }).continueOnErrorWith(continuation: { error in
            
        })
    }
    
    //MARK: - Init
    class func loadView() -> UIView? {
        let nibNamed = "SectionChartView"
        if let view = Bundle.main.loadNibNamed(nibNamed, owner: nil, options: nil)?.first as? SectionChartView {
            return view
        }
        return nil
    }
    
    public var height: CGFloat {
        get {
            //            var h: CGFloat = 0
            //            if listBarChartItems.count > 0 {
            //                h += 350
            //            }
            //            if listPieChartItems.count > 0 {
            //                h += 350
            //            }
            //            return h
            return dynamicHeight + 10
        }
    }
    
    public var dynamicHeight: CGFloat = 0
    
    //MARK: - Properties
    var barChartView: BarChartView!
    var pieChartView: PieChartView!
    var listPieChartItems = [ChartItemModel]()
    var listBarChartItems = [ChartItemModel]()
    
    //MARK: - Outlet
    @IBOutlet weak var barChartViewContain: UIView!
    @IBOutlet weak var pieChartViewContain: UIView!
    @IBOutlet weak var barChartHeader: UILabel!
    @IBOutlet weak var pieChartHeader: UILabel!
    @IBOutlet weak var barChartViewLine: UIView!
    @IBOutlet weak var pieChartViewLine: UIView!
    
}
