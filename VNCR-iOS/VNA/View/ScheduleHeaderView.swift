//
//  ScheduleHeaderView.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/25/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit

class ScheduleHeaderView: UIView {
    
    @IBOutlet weak var viewLineHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewLine: UIView!
    
    @IBOutlet weak var buttonSelectDateStart: UIButton!
    
    @IBOutlet weak var buttonSelectDateEnd: UIButton!
    
    @IBOutlet weak var viewTextFieldSearch: UIView!
    
    @IBOutlet weak var textFieldSearch: UITextField!
    
    @IBOutlet weak var buttonSearch: UIButton!
    
    @IBOutlet weak var textFieldNameStart: UITextField!
    
    @IBOutlet weak var labelDayStart: UILabel!
    
    @IBOutlet weak var labelDayOfWeekStart: UILabel!
    
    @IBOutlet weak var labelMonthStart: UILabel!
    
    @IBOutlet weak var labelDayEnd: UILabel!
    
    @IBOutlet weak var labelDayOfWeekEnd: UILabel!
    
    @IBOutlet weak var labelMonthEnd: UILabel!
    
    @IBOutlet weak var iconArrow1: UILabel!
    
    @IBOutlet weak var labelIconArrowDown1: UILabel!
    
    @IBOutlet weak var labelIconArrowDown2: UILabel!
    
    @IBOutlet weak var radioButtonAllFlight: SSRadioButton!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func loadView() -> UIView? {
//        var nibNamed = "ScheduleHeaderView"
//        if(UIDevice.current.userInterfaceIdiom == .phone) {
//            nibNamed = "ScheduleHeaderView_iphone"
//        }
        let nibNamed = "ScheduleHeaderView_iphone"
        if let view = Bundle.main.loadNibNamed(nibNamed, owner: nil, options: nil)?.first as? ScheduleHeaderView {
//            if(UIDevice.current.userInterfaceIdiom == .phone) {
//                view.viewLineHeight.constant = 0.5
//            } else {
//                view.viewLine.frame = CGRect(x: view.viewLine.frame.origin.x, y: view.viewLine.frame.origin.y, width: view.viewLine.frame.size.width, height: 0.5)
//            }
            view.viewLineHeight.constant = 0.5
            return view
        }
        return nil
    }

}
