//
//  TryAgainView.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/26/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class TryAgainView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var labelTitleTryAgain: UILabel!
    
    @IBOutlet weak var buttonTryAgain: UIButton!
    
    class func loadView() -> UIView? {
//        var nibNamed = "TryAgainView"
//        if(UIDevice.current.userInterfaceIdiom == .phone) {
//            nibNamed = "TryAgainView_iphone"
//        }
        let nibNamed = "TryAgainView_iphone"
        if let view = Bundle.main.loadNibNamed(nibNamed, owner: nil, options: nil)?.first as? TryAgainView {
            view.labelTitleTryAgain.textColor = UIColor.white
            view.buttonTryAgain.layer.borderColor = UIColor("#0b7e07").cgColor
            view.buttonTryAgain.layer.cornerRadius = 3.0
            view.buttonTryAgain.layer.borderWidth = 1.4
            view.buttonTryAgain.setTitle("TRY AGAIN".localizedString(), for: UIControl.State())
            return view
        }
        return nil
    }
    

}
