//
//  BaseView.swift
//  VNA
//
//  Created by Dai Pham on 07/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit


class BaseView: UIView {

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !haveEventTouch {
            superview?.touchesBegan(touches, with: event)
            return
        }
        DispatchQueue.main.async {
            self.alpha = 1.0
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                self.alpha = 0.5
            }, completion: nil)
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !haveEventTouch {
            superview?.touchesEnded(touches, with: event)
            return
        }
        DispatchQueue.main.async {
            self.alpha = 0.5
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                self.alpha = 1.0
            }, completion: nil)
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !haveEventTouch {
            superview?.touchesCancelled(touches, with: event)
            return
        }
        DispatchQueue.main.async {
            self.alpha = 0.5
            UIView.animate(withDuration: 0.1, delay: 0.0, options: .curveLinear, animations: {
                self.alpha = 1.0
            }, completion: nil)
        }
    }
    
    // MARK: -  override
    func setupTexts() {
        // override
    }
    
    func config() {
        setupTexts()
    }
    
    // MARK: -  private
    private func loadNIb() {
        if let name = NSStringFromClass(type(of: self)).components(separatedBy: ".").last {
            Bundle.main.loadNibNamed(name, owner: self, options: nil)
        }
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    func observerTasks(tasks:[NSObjectProtocol]) {
        notifObservers.forEach({NotificationCenter.default.removeObserver($0)})
        notifObservers.append(contentsOf: tasks)
    }
    
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNIb()
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNIb()
        config()
    }
    
    init() {
        super.init(frame: CGRect.zero)
        loadNIb()
        config()
    }
    
    deinit {
        notifObservers.forEach({NotificationCenter.default.removeObserver($0)})
    }
    
    // MARK: -  properties
    var notifObservers = [NSObjectProtocol]()
    
    // MARK: - outlet
    @IBOutlet weak var view: UIView!
}

