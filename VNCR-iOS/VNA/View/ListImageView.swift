//
//  ListImageView.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/18/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

protocol ListImageViewDelegate: NSObjectProtocol {
    func didSelectCellPress(viewController: UIViewController)
    
}

class ListImageView: UIView, UICollectionViewDataSource, UICollectionViewDelegate, PostCollectionViewCellDelegate, SKPhotoBrowserDelegate {

    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    weak var delegate: ListImageViewDelegate?
    
    var listImagePost = Array<ImagePost>()
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let nib = UINib.init(nibName: "PostCollectionViewCell", bundle: nil)
        self.collectionView?.register(nib, forCellWithReuseIdentifier: "PostCollectionViewCell")
        self.collectionView?.backgroundColor = UIColor.clear
        self.backgroundColor = UIColor.white
        self.collectionView?.contentInset = UIEdgeInsets(top: self.collectionView!.contentInset.top, left: 5, bottom: self.collectionView!.contentInset.bottom, right: self.collectionView!.contentInset.right)
    }
    
    func getListImagePost() -> [UIImage] {
        var listImagePost = Array<UIImage>()
        for imagePost in self.listImagePost {
            if(imagePost.isImagePost == true) {
                listImagePost.append(imagePost.image!)
            }
        }
        return listImagePost
    }
    
    
    func getImagePostSelectedCount() -> Int {
        var count = 0
        for imagePost in self.listImagePost {
            if(imagePost.isImagePost == true) {
                count += 1
            }
        }
        return count
    }
    
    func getImagePostNotSeletedCount() -> Int {
        var count = 0
        for imagePost in self.listImagePost {
            if(imagePost.isImagePost == false) {
                count += 1
            }
        }
        return count
    }
    
    func clearContent() {
        
        for i in 0..<listImagePost.count {
            if(self.listImagePost[i].isImagePost == true) {
                self.listImagePost[i].isImagePost = false
            }
        }
        
        self.reloadData()
    }
    
    func loadContent(listImagePost: Array<ImagePost>) {
        if(self.listImagePost.count == 0) {
            self.listImagePost = listImagePost
        } else {
            for i in 0..<listImagePost.count {
                for j in 0..<self.listImagePost.count {
                    if(self.listImagePost[j].isImagePost == false) {
                        self.listImagePost[j] = listImagePost[i]
                        break
                    }
                }
                
            }
        }
        
        self.reloadData()
    }
    
    func reloadData() {
        
        self.listImagePost = self.listImagePost.sorted(by: { $0.isImagePost && !$1.isImagePost })
        self.collectionView.reloadData()
    }
    
    //MARK - PostCollectionViewCellDelegate
    
    func buttonRemoveImageCellPress(cell: UICollectionViewCell) {
   
        
    }
    
    func buttonImageViewImageCellPress(imagePost: ImagePost) {
        if(imagePost.isImagePost) {
            if let index = self.listImagePost.firstIndex(of: imagePost) {
                self.collectionView(self.collectionView, didSelectItemAt: IndexPath.init(row: index, section: 0))
            } 
        }
        
    }
    
    //MARK - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.listImagePost.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: PostCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PostCollectionViewCell", for: indexPath) as! PostCollectionViewCell
        let imagePost = self.listImagePost[indexPath.item]
        cell.loadContent(imagePost: imagePost)
        cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var listPhoto = Array<SKPhotoProtocol>()
        for item in self.listImagePost {
            if(item.isImagePost) {
                if(item.imageUrl != "") {
                    let photo = SKPhoto.photoWithImageURL(item.imageFullUrl)
                    photo.caption = ""
                    listPhoto.append(photo)
                } else {
                    let photo = SKPhoto.photoWithImage(item.image!)
                    photo.caption = ""
                    listPhoto.append(photo)
                }
                
            }
            
        }
        
        if collectionView.cellForItem(at: indexPath) != nil {
            //let browser = SKPhotoBrowser(originImage: UIImage.init(named: "photo-camera.png")!, photos: listPhoto, animatedFromView: cell)
            //browser.initializePageIndex(indexPath.item)
            let browser = SKPhotoBrowser(photos: listPhoto, initialPageIndex: indexPath.item)
            browser.delegate = self
            delegate?.didSelectCellPress(viewController: browser)
        }
        
    }
}


// MARK: - SKPhotoBrowserDelegate

extension ListImageView {
    func didShowPhotoAtIndex(_ index: Int) {
        collectionView.visibleCells.forEach({$0.isHidden = false})
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
    }
    
    func willDismissAtPageIndex(_ index: Int) {
        collectionView.visibleCells.forEach({$0.isHidden = false})
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = true
    }
    
    func willShowActionSheet(_ photoIndex: Int) {
        // do some handle if you need
    }
    
    func didDismissAtPageIndex(_ index: Int) {
        collectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isHidden = false
    }
    
    func didDismissActionSheetWithButtonIndex(_ buttonIndex: Int, photoIndex: Int) {
        // handle dismissing custom actions
    }
    
    func removePhoto(_ browser: SKPhotoBrowser, index: Int, reload: (() -> Void)) {
        reload()
    }
    
    func viewForPhoto(_ browser: SKPhotoBrowser, index: Int) -> UIView? {
        return collectionView.cellForItem(at: IndexPath(item: index, section: 0))
    }
}
