//
//  SectionEditable.swift
//  VNA
//
//  Created by Pham Dai on 03/12/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class SectionEditable: BaseView {

    enum EditableType {
        case number
        case normal
    }
    
    struct Value {
        var content:String?
        var title:String?
        var identifier:String?
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txvInput: UITextView!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var vwCoverInput: Corner5BorderView!
    
    var type:EditableType
    var value:Value
    var textDidChange:((SectionEditable)->Void)?
    let formatter = NumberFormatter()
    
    var getVal:String? {
        return self.value.content
    }
    
    init(type:EditableType,
         value:Value,
         textDidChange:((SectionEditable)->Void)?) {
        self.value = value
        self.textDidChange = textDidChange
        self.type = type
        super.init(frame: .zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.value = Value()
        self.type = .normal
        super.init(coder: aDecoder)
    }
    
    override func config() {
        
        vwCoverInput.borderColor = .gray
        
        txvInput.delegate = self
        txvInput.addToolbarEndEditing()
        txvInput.text = value.content
        
        txtInput.addToolbarEndEditing()
        txtInput.keyboardType = .decimalPad
        txtInput.text = value.content
        txtInput.delegate = self
        
        lblTitle.font = .systemFont(ofSize: 16, weight: .medium)
        lblTitle.textColor = .black
        lblTitle.text = value.title
        
        switch type {
        case .number:
            txvInput.isHidden = true
            txtInput.addTarget(self, action: #selector(self.textFieldChange(_:)), for: .editingChanged)
        case .normal:
            txtInput.isHidden = true
        }
    }
    
    func reshowValue() {
        txtInput.text = value.content
    }
}

extension SectionEditable:UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let label = textField.text else { return }
        if let v = Double(label) {
            value.content = "\(v)"
        } else {
            let maybeNumber = formatter.number(from: label)
            value.content = maybeNumber?.floatValue != nil ? "\(maybeNumber!.floatValue)" : nil
        }
        reshowValue()
        self.textDidChange?(self)
    }
}

extension SectionEditable:UITextViewDelegate {
    @objc func textFieldChange(_ sender:UITextField) {
        
    }
    
    func textViewDidChange(_ textView: UITextView) {
        value.content = textView.text
        self.textDidChange?(self)
    }
}
