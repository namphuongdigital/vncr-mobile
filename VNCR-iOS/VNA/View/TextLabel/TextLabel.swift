//
//  TextLabel.swift
//  ACV
//
//  Created by Pham Dai on 05/10/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class TextLabel: BaseView {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    var content:String? {
        set {
            lblContent.text = newValue
        }
        get {
            return lblContent.text
        }
    }
    
    var title:String? {
        get{lblTitle.text}
        set{lblTitle.text = newValue}
    }
    
    override func config() {
        
        (view as? Corner5BorderView)?.borderColor = .gray
        
        lblTitle.font = .systemFont(ofSize: 13)
        lblContent.font = .systemFont(ofSize: 16)
        
        lblTitle.textColor = .gray
        lblContent.textColor = .black
    }
    
}
