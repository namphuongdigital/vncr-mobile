//
//  CheckBoxControl.swift
//  VNA
//
//  Created by Pham Dai on 02/12/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class SelectedControl: BaseView {

    struct Value {
        var content:String?
        var isSelected:Bool
        var identifier:String?
    }
    
    enum ControlType {
        case checkbox
        case radio
    }
    
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var lblContent: UILabel!
    
    var value:Value
    var type:ControlType
    
    var action:((SelectedControl)->Void)?
    
    init(type:ControlType,
         value:Value,
         frame:CGRect = .zero,
         action:((SelectedControl)->Void)? = nil) {
        self.value = value
        self.type = type
        super.init(frame: frame)
        self.action = action
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.value = Value(isSelected: false)
        self.type = .checkbox
        super.init(coder: aDecoder)
    }
    
    override func config() {
        view.addTargetCustom(target: self, action: #selector(self.selected(_:)), keepObject: nil)
        
        configUI()
        refreshUI()
    }
    
    private func configUI() {
        
        switch type {
        case .checkbox:
            icon.image = UIImage(named:"ic_checkbox")
            icon.highlightedImage = UIImage(named: "ic_checkedbox")
        case .radio:
            icon.image = UIImage(named:"ic_radio")
            icon.highlightedImage = UIImage(named: "ic_selectedradio")
        }
        
        lblContent.text = value.content
    }
    
    func refreshUI() {
        icon.isHighlighted = value.isSelected
    }
    
    @objc func selected(_ sender:Button10Corner) {
        
//        if type == .checkbox {
            self.value.isSelected = !value.isSelected
            refreshUI()
//        }
        action?(self)
    }
}
