//
//  LotusShopComponentView.swift
//  VNA
//
//  Created by Pham Dai on 27/08/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

protocol LotusShopComponentViewDelegate:AnyObject {
    func LotusShopComponentView_shouldRefreshLotus(new:LotusShopStatus?)
}

class LotusShopComponentView: BaseView {

    // MARK: -  outlet
    @IBOutlet weak var btnReport: UIButton!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblRemark: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwAttachments: UIView!
    @IBOutlet weak var stackAttackments: UIStackView!
    
    // MARK: -  properties
    weak var delegate:LotusShopComponentViewDelegate?
    weak var controller:UIViewController?
    var isAlreadyCheckStoredRequest:Bool = false
    var passenger:PassengerSeat? {
        didSet {
            items = passenger?.lotusShopStatus?.attachments ?? []
        }
    }
    
    private var items:[AttachmentCommonModel] = [] {
        didSet {
            vwAttachments.isHidden = items.count == 0
            stackAttackments.arrangedSubviews.forEach({
                $0.removeTargetCustom(target: self, action: #selector(self.showImage(_:)))
                $0.removeFromSuperview()
            })
            items.enumerated().forEach({(i,item) in
                let imv = ImageView5Corner()
                imv.addTargetCustom(target: self, action: #selector(self.showImage(_:)), keepObject: item.FoFileUrl)
                if let base64 = item.base64File,
                   let data = Data(base64Encoded: base64) {
                    imv.image = UIImage(data: data)
                }
                if item.FoFileUrl.count > 0 {
                    imv.loadImageNoProgressBar(url: URL(string: item.FoFileUrl))
                }
                stackAttackments.addArrangedSubview(imv)
                if i == 0 {
                    imv.heightAnchor.constraint(equalToConstant: 50).isActive = true
                    imv.widthAnchor.constraint(equalTo: imv.heightAnchor, multiplier: 1, constant: 0).isActive = true
                }
            })
        }
    }
    
    @available(iOS 10.0, *)
    private func checkLoadStoredRequest() {
        guard let passenger = passenger, let key = passenger.lotusShopKey else {return}
        let identifier = "\(passenger.flightID ?? 0)\(key)"
        ManagerRequests.getRequest(type: StoredRequest.RequestType.lotusShop.rawValue,
                                   identifier: identifier) {[weak self] request in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                if let shop = request?.lotusShopStatus {
                    self.passenger?.lotusShopStatus = shop
                    self.show(passenger: self.passenger)
                }
            }
        }
    }
    
    @objc func showImage(_ sender:Button10Corner) {
        if let url = sender.object as? String {
            let item = SKPhoto(url: url)
            let vc = SKPhotoBrowser(photos: [item])
            self.controller?.present(vc, animated: true, completion: nil)
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func config() {
        
        lblRemark.font = .systemFont(ofSize: 16, weight: .medium)
        lblRemark.textColor = .gray
        
        btnReport.titleLabel?.font = .systemFont(ofSize: 16)
        btnReport.setTitleColor(normal:UIColor.systemBlue,highlighted:UIColor.gray)
        btnReport.setTitle("Edit".localizedString(), for: UIControl.State())
    }
    
    func show(passenger:PassengerSeat?) {
        self.passenger = passenger
        lblTitle.text = "LotuShop (\(passenger?.lotusShopItems.count ?? 0) \(passenger?.lotusShopItems.count ?? 0 > 1 ? "items" : "item"))"
        
        lblRemark.setAttribute(elements: [(text:"Note: ",color:nil,font:nil),
                                          (text:passenger?.lotusShopStatus?.remark ?? "",color:nil,font:nil)])
        lblRemark.isHidden = passenger?.lotusShopStatus?.remark?.trimmingCharacters(in: .whitespacesAndNewlines).count ?? 0 == 0
        
        var status = ""
        if let option = passenger?.lotusReportItems.first(where: { item in
            return passenger?.lotusShopStatus?.selectedItemID == item.id
        }) {
            status = option.title
        }
        lblStatus.setAttribute(elements: [(text:"Status: ",color:nil,font:nil),
                                          (text:status,color:.black,font:.systemFont(ofSize: 16, weight: .medium))])

        if #available(iOS 10.0, *) {
            if !isAlreadyCheckStoredRequest {
                isAlreadyCheckStoredRequest = true
                checkLoadStoredRequest()
            }
        }
    }
    
    @IBAction func report(_ sender: UIButton) {
        guard let passenger = passenger, let flightId = passenger.flightID else {return}
        let vc = ReportLotusShopController(passenger: passenger, flightID: flightId) {[weak self] newStatus in guard let `self` = self else { return }
            self.delegate?.LotusShopComponentView_shouldRefreshLotus(new: newStatus)
        }
        (self.controller as? BaseViewController)?.present(vc: BasePresentNavigationController(root: vc), transitioning: ScaleUpAnimateTransitionDelegate(), completion: nil)
    }
    
}
