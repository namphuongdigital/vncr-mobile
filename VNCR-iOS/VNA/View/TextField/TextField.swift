//
//  TextField.swift
//  GlobeDr
//
//  Created by GlobeDr on 3/25/19.
//  Copyright © 2019 GlobeDr. All rights reserved.
//

import UIKit


@objc protocol TextFieldDelegate:AnyObject {
    func select(view:TextField?)
    func date(text:String, view:TextField?)
    func minimumDate(view:TextField?) -> Date?
    func maximumDate(view:TextField?) -> Date?
    @objc optional func shouldBeginEdit(view:TextField?) -> Bool
    @objc optional func typing(text:String?, view:TextField?)
    @objc optional func shouldSelectAllContent(text:String?, view:TextField?) -> Bool
    @objc optional func inputAccessoryView(view:TextField?) -> UIView?
}


enum TextFieldType :Int {
    case editable = 0
    case password = 1
    case select = 2
    case date = 3
}

class TextField: BaseView {
    
    // MARK: -  properties
    var type:TextFieldType = .editable
    weak var delegate:TextFieldDelegate?
    
    var isActiveResponse:Bool = false
    fileprivate let datePicker = UIDatePicker()
    var selectedDate:Date?
    // MARK: -  closure
    var onTyping:((UITextField)->Void)?
    var onTapReturn:((UITextField)->Void)?
    var onHideDatePicker:(()->Void)?
    var isDefaultDate:Bool = false
    var forcedFocusTextField = false
    var listData:[Any] = []
    var placeholder:String?
    var currentBackgroundColor:UIColor = .white
    
    var borderColor:UIColor = .clear {
        didSet {
            (view as? Corner5BorderView)?.borderColor = borderColor
        }
    }
    
    var isEnabled:Bool {
        set {
            txtInput.isEnabled = newValue
            view.backgroundColor = isEnabled ? currentBackgroundColor : UIColor.lightGray.withAlphaComponent(0.4)
        }
        get {txtInput.isEnabled}
    }
    
    // MARK: -  oulet
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var txtInput: UITextField!

    func setValueDate(date : Date){
        let formatter = DateFormatter()
        switch type {
        case .date:
            formatter.dateFormat = "dd/MM/yyyy"
            datePicker.setDate(date, animated: true)
            selectedDate = date
            break
        default:
            break
        }
        let dateString = formatter.string(from: datePicker.date)
        setValue(value: dateString)
        self.delegate?.date(text: dateString, view: self)
    }
    
    func setup(type:TextFieldType,
               placeHolder:String?,
               icon:UIImage? = nil,
               delegate:TextFieldDelegate?
    ) {
        self.type = type
        setup(type: type)
        self.delegate = delegate
        let insetRight:CGFloat = 0
        self.placeholder = placeHolder
        
        self.icon.isHidden = icon == nil
        self.icon.image = icon
        
        view.addTargetCustom(target: self, action: #selector(self.touch(_:)), keepObject: nil, boundInside: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: insetRight))
    }
    
    func getValueCoverToDouble() -> Double? {
        var v = txtInput.text
        
//        if txtInput.textInputMode?.primaryLanguage?.contains("vi") == true {
        v = v?.replacingOccurrences(of: ",", with: ".")
//        }
        return Double(v ?? "")
        
    }

    func setBackgroundColor(color:UIColor) {
        currentBackgroundColor = color
        view.backgroundColor = color
    }
    
    func getUTCDateAndTimeString() ->String? {
        if var _ = txtInput.text {
        
            return selectedDate?.addingTimeInterval(60*60*7).toString(dateFormat: "yyyy-MM-dd'T'12:00:00.000",isUTC:true)
        }
        return nil
    }
    
    func getUTCDateString() ->String? {
        if var _ = txtInput.text {
            return selectedDate?.toString(dateFormat: "yyyy-MM-dd",isUTC:false)
        }
        return ""
    }
    
    func setKeyboardType(keyboardType:UIKeyboardType){
           txtInput.keyboardType = keyboardType
    }
    
    // MARK: -  interface
    func setFirstResponse() {
        forcedFocusTextField = true
        self.txtInput.isHidden = false
        self.txtInput.becomeFirstResponder()
    }
    
    var isFirstResponse:Bool {
        set {
            if newValue {
                forcedFocusTextField = true
                self.txtInput.isHidden = false
                self.txtInput.becomeFirstResponder()
            } else {
                forcedFocusTextField = false
                txtInput.resignFirstResponder()
            }
        }
        get {
            return txtInput.isFirstResponder
        }
    }
    
    
    
    override var isFirstResponder: Bool {
        return txtInput.isFirstResponder
    }
    
    func setTypeReturnKey(type:UIReturnKeyType) {
        txtInput.returnKeyType = type
    }
    
    func setDisabled() {
        txtInput.isEnabled = false
    }
    
    func getValue() -> String? {
        return txtInput.text
    }
    
    func setValue(value:String) {
        
        if txtInput.keyboardType == .decimalPad {
            var v = value
            if txtInput.textInputMode?.primaryLanguage?.contains("vi") == true {
                v = v.replacingOccurrences(of: ",", with: ".")
            }
            txtInput.text = v
        } else {
            txtInput.text = value
        }
        
        setNeedsLayout()
    }
    
    func showError(_ message:String?) {
        let show = message != nil || (message?.count ?? 0) > 0
//        self.lblError.isHidden = !show
//        lblError.text = message
        view.layer.borderWidth = show ? 1 : 0
        view.layer.borderColor = UIColor.red.cgColor
    }
    
    func setIdentifierForUITesting(_ identifier:String) {
//        lblError.accessibilityIdentifier = "lblError"+identifier
        txtInput.accessibilityIdentifier = "txt"+identifier
    }
    
    // MARK: -  event
    @objc func showCalendar() {
        txtInput.becomeFirstResponder()
    }
    
    @objc func tapShowText(_ sender: UIButton) {
        if type == .password {
            txtInput.isSecureTextEntry = !txtInput.isSecureTextEntry
            sender.isSelected = !txtInput.isSecureTextEntry
            
        }
    }
    
    @objc func textfieldDidChange(_ sender:UITextField) {
        self.delegate?.typing?(text: sender.text, view: self)
        onTyping?(sender)
    }
    
    
    // MARK: -  overridde
    override func config() {
        super.config()
        
        view.backgroundColor = .white
        
//        lblError.isHidden = true
//        lblError.font = UIFont.regular(size: 12)
//        lblError.textColor = .red
//        lblError.adjustsFontForContentSizeCategory = true
        
        txtInput.font = UIFont.systemFont(ofSize: 16)
        txtInput.delegate = self
        txtInput.addTarget(self, action: #selector(textfieldDidChange(_:)), for: UIControl.Event.editingChanged)
        txtInput.isSecureTextEntry = false
        
        view.addTargetCustom(target: self, action: #selector(self.touch(_:)), keepObject: nil)
    }
    
    @objc func touch(_ sender:UIButton) {
        txtInput.isHidden = false
        txtInput.becomeFirstResponder()
    }
    
    override func setupTexts() {
        // setup text here
//        lblError.text = lblError.text?.localized()
    }
    
    // MARK: -  private
    private func setup(type:TextFieldType) {
        //        vwCoverRadius.backgroundColor = .whiteForDark
        switch type {
        case .editable:
            txtInput.addToolbarEndEditing()
        case .password:
            if #available(iOS 11.0, *) {
                txtInput.textContentType = .password
            }
            txtInput.isSecureTextEntry = true
            view.addTargetCustom(target: self, action: #selector(self.touch(_:)), keepObject: nil, boundInside: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 60))
            txtInput.addToolbarEndEditing()
        case .select:
        break
        case .date:
            showDatePicker()
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if let string = placeholder {
            txtInput.attributedPlaceholder = NSAttributedString(string: string, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14), NSAttributedString.Key.foregroundColor :  UIColor.gray])
        } else {
            txtInput.attributedPlaceholder = nil
        }
    }
    
    @objc func onSelect() {
        if type == .select {
            self.delegate?.select(view: self)
        }
    }
}

extension TextField: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isActiveResponse = true
        if let _ = textField.text,
           delegate?.shouldSelectAllContent?(text: textField.text, view: self) == true {
            textField.becomeFirstResponder()
            textField.selectAll(nil)
        }
        setNeedsLayout()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        isActiveResponse = false
        forcedFocusTextField = false
        setNeedsLayout()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        onTapReturn?(textField)
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if let inputAccessoryView = delegate?.inputAccessoryView?(view: self) {
            txtInput.inputAccessoryView = inputAccessoryView
            txtInput.reloadInputViews()
        }
        
        if type == .editable || type == .date {
            if let bool = self.delegate?.shouldBeginEdit?(view: self) {
                if bool {
                    setNeedsLayout()
                }
                return bool
            }
        }
        else if type == .select {
            if textField == txtInput {
                self.delegate?.select(view: self)
            }
            return  false
        }
        
        //        animationTitle?.isReversed = true
        //        animationTitle?.continueAnimation(withTimingParameters: nil, durationFactor: 0)
        setNeedsLayout()
        
        return  true
    }
}


extension TextField {
    
    func updateMaxMinDate() {
        let calendar = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        comps.year = 20
        let maxDate = delegate?.maximumDate(view: self) ?? calendar.date(byAdding: comps, to: Date())
        comps.year = -105
        let minDate = delegate?.minimumDate(view: self) ?? calendar.date(byAdding: comps, to: Date())
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate
    }
    
     func showDatePicker(){
        //Formate Date
        
        switch type {
        case .date:
            datePicker.datePickerMode = .date
            let calendar = Calendar(identifier: .gregorian)
            var comps = DateComponents()
            comps.year = 20
            let maxDate = delegate?.maximumDate(view: self) ?? calendar.date(byAdding: comps, to: Date())
            comps.year = -105
            let minDate = delegate?.minimumDate(view: self) ?? calendar.date(byAdding: comps, to: Date())
            datePicker.maximumDate = maxDate
            datePicker.minimumDate = minDate
            selectedDate = datePicker.date
            txtInput.clearButtonMode = .never
            break
        default:
            break
        }
        
        if isDefaultDate {
            donedatePicker()
        }
//        datePicker.locale = NSLocale.init(localeIdentifier: AppConfig.language.getCurrentLanguageDeviceLocale) as Locale
        datePicker.addTarget(self, action: #selector(donedatePickerChange), for: .valueChanged)
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "done".localizedString(), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel".localizedString(), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.blue], for: UIControl.State())
        cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.blue], for: UIControl.State())
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: true)
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        txtInput.inputAccessoryView = toolbar
        txtInput.inputView = datePicker
    }
    
    @objc func donedatePickerChange(){
        let formatter = DateFormatter()
//        formatter.locale = NSLocale.init(localeIdentifier: AppConfig.language.getCurrentLanguageDeviceLocale) as Locale
        
        
        switch type {
        case .date:
            formatter.dateFormat = "dd/MM/yyyy"
            selectedDate = datePicker.date
            break
        default:
            break
        }
        
        let dateString = formatter.string(from: datePicker.date)
        
        setValue(value: dateString)
        self.delegate?.date(text: dateString, view: self)
    }
    
    @objc func donedatePicker(){
        let formatter = DateFormatter()
//        formatter.locale = NSLocale.init(localeIdentifier: AppConfig.language.getCurrentLanguageDeviceLocale) as Locale
        
        switch type {
        case .date:
            formatter.dateFormat = "dd/MM/yyyy"
            selectedDate = datePicker.date
            let dateString = formatter.string(from: datePicker.date)
            setValue(value: dateString)
            self.delegate?.date(text: dateString, view: self)
        default: break
        }
        self.view.endEditing(true)
        onHideDatePicker?()
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
        onHideDatePicker?()
    }
    
   
    
}
