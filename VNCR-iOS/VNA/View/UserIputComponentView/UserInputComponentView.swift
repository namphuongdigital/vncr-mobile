//
//  UserInputComponentView.swift
//  VNA
//
//  Created by Pham Dai on 24/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import Photos

@objc protocol UserInputComponentViewDelegate:AnyObject {
    @objc optional func UserInputComponentView_send(assets:[PHAsset],content:String,component:UserInputComponentView)
    @objc optional func UserInputComponentView_selectedAssets(assets:[PHAsset],component:UserInputComponentView)
    @objc optional func UserInputComponentView_clearQuote(component:UserInputComponentView)
    @objc optional func UserInputComponentView_willShowKeyboard(component:UserInputComponentView)
    @objc optional func UserInputComponentView_willDismissKeyboard(component:UserInputComponentView)
}

class UserInputComponentView: BaseView {

   // MARK: -  outlets
    @IBOutlet weak var vwStackImages: UIView!
    @IBOutlet weak var stackImages: UIStackView!
    @IBOutlet weak var txvInput: UITextView10Corner!
    @IBOutlet weak var btnPhotos: UIButton!
    @IBOutlet weak var btnSend: UIButton!
    @IBOutlet weak var bottomEqualHeightKeyboardConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightTextView: NSLayoutConstraint!
    
    @IBOutlet weak var vwQuotesMessage: Corner10View!
    @IBOutlet weak var lblQuoteMessage: UILabel!
    @IBOutlet weak var lblNameDateQuote: UILabel!
    @IBOutlet weak var btnClearQuote: UIButton!
    
    weak var controller:BaseViewController?
    weak var delegate:UserInputComponentViewDelegate?
    var photosSelected:[PHAsset] = []
    var maxNumerPhotoSelected:Int = 1
    var isOwnerKeyboard:Bool = false
    var duration:TimeInterval = 0.25
    let originContantBottom:CGFloat = 5

    override var isFirstResponder: Bool {
        set {
            txvInput.becomeFirstResponder()
        }
        get {
            return txvInput.isFirstResponder
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func config() {
        txvInput.placeholder1 = "Write a comment...".localizedString()
        txvInput.font = .systemFont(ofSize: 16)
        txvInput.textColor = .darkGray
        txvInput.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        txvInput.contentInset = UIEdgeInsets(top: txvInput.contentInset.top, left: 10, bottom: txvInput.contentInset.bottom, right: 10)
        txvInput.isScrollEnabled = false
        
        lblNameDateQuote.font = .systemFont(ofSize: 12)
        lblNameDateQuote.textColor = .gray
        
        lblQuoteMessage.textColor = .darkText
        lblQuoteMessage.font = .italicSystemFont(ofSize: 12)
        vwQuotesMessage.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        
        // handle user selectall and remove
        txvInput.onTextViewDidChange = {[weak self] textview in
            guard let `self` = self else { return }
            
            var shouldClear = false
            if self.txvInput.text == "" {
                shouldClear = true
                self.txvInput.text = " "
            }
            
            let fixedWidth = self.txvInput.frame.size.width
            let newSize = self.txvInput.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
            self.layoutIfNeeded()
            let height = newSize.height + self.txvInput.contentInset.top + self.txvInput.contentInset.bottom
            self.txvInput.isScrollEnabled = height >= self.heightTextView.constant
            
            if shouldClear {self.txvInput.text = ""}
            
            self.checkValidContent()
        }
        
        txvInput.onTextViewShouldBeginEditing = {[weak self] textView in
            guard let `self` = self else { return true}
            self.isOwnerKeyboard = true
            return true
        }
        
        txvInput.onTextViewDidEndEditing = {[weak self] textView in
            guard let `self` = self else { return }
            self.isOwnerKeyboard = false
        }
        
        
        checkValidContent()
    }
    
    private func checkValidContent() {
        let haveAttachments = self.photosSelected.count > 0
        let haveContent = txvInput.text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).count > 0
        
        btnSend.isEnabled = haveContent || haveAttachments
    }
    
    func clear() {
        actions(btnClearQuote)
        photosSelected = []
        txvInput.text = ""
        txvInput.onTextViewDidChange?(txvInput)
        loadPhotos(photos: photosSelected)
    }
    
    @objc func keyboardWillChangeFrame(notification: NSNotification) {
        // prevent active code when txvInput not first response
        if isOwnerKeyboard == false {return}
        
        //Need to calculate keyboard exact size due to Apple suggestions
        let info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size ?? .zero
        let timeDuration = (info[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 3
        if keyboardSize.height > 0 {
            if #available(iOS 11.0, *) {
                bottomEqualHeightKeyboardConstraint.constant = keyboardSize.height - (controller?.view.safeAreaInsets.bottom ?? 0) + originContantBottom
            } else {
                bottomEqualHeightKeyboardConstraint.constant = keyboardSize.height + originContantBottom
            }
        }
        duration = timeDuration
        UIView.animate(withDuration: timeDuration) {
            self.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        // prevent active code when txvInput not first response
        if isOwnerKeyboard == false {return}
        
        let info = notification.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size ?? .zero
        let timeDuration = (info[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 3
        if keyboardSize.height > 0 {
            if #available(iOS 11.0, *) {
                bottomEqualHeightKeyboardConstraint.constant = keyboardSize.height - (controller?.view.safeAreaInsets.bottom ?? 0) + originContantBottom
            } else {
                bottomEqualHeightKeyboardConstraint.constant = keyboardSize.height + originContantBottom
            }
        }
        duration = timeDuration
        UIView.animate(withDuration: timeDuration) {
            self.layoutIfNeeded()
        }
        
        delegate?.UserInputComponentView_willShowKeyboard?(component: self)
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        // prevent active code when txvInput not first response
        if isOwnerKeyboard == false {return}
        
        let info = notification.userInfo!
        let timeDuration = (info[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 3
        bottomEqualHeightKeyboardConstraint.constant = originContantBottom
        duration = timeDuration
        UIView.animate(withDuration: timeDuration) {
            self.layoutIfNeeded()
        }
        
        delegate?.UserInputComponentView_willDismissKeyboard?(component: self)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @IBAction func actions(_ sender: UIButton) {
        switch sender {
        case btnSend:
            self.delegate?.UserInputComponentView_send?(assets: photosSelected, content: txvInput.text, component: self)
        case btnPhotos:
            EasyPermission.requestPhotoLibrayPermission { (status) in
                if(status == EasyAuthorityStatus.authorizationStatusDenied) {
                    DispatchQueue.main.async {
                        EasyPermission.alertTitle("Alert".localizedString(), message: "Press Setting for config Photos".localizedString())
                    }
                } else {
                    DispatchQueue.main.async {
                        let vc = PhotosController(type: .photo, maximumSelect: self.maxNumerPhotoSelected, delegate: self)
                        vc.setPhotos(items: self.photosSelected)
                        let nv = UINavigationController(rootViewController: vc)
                        self.controller?.present(nv, animated: true, completion: nil)
                    }
                }
            }
        case btnClearQuote:
            UIView.animate(withDuration: 0.25) {
                self.vwQuotesMessage.isHidden = true
                self.lblQuoteMessage.text = nil
                self.lblNameDateQuote.text = nil
            }
            self.delegate?.UserInputComponentView_clearQuote?(component: self)
        default:break
        }
    }
    
    func showQuote(message:String?,nameDate:String?) {
        
        lblQuoteMessage.text = message
        lblNameDateQuote.text = nameDate
        
        UIView.animate(withDuration: 0.25) {
            self.vwQuotesMessage.isHidden = message?.count ?? 0 == 0
        }
    }
    
    func loadPhotos(photos: [PHAsset]) {
        UIView.animate(withDuration: 0.2) {
            self.stackImages.arrangedSubviews.forEach{$0.alpha = 0}
        } completion: { bool in
            self.stackImages.arrangedSubviews.forEach{
                $0.removeRemoveButton(target: self, action: #selector(self.remove(_ :)))
                $0.removeTargetCustom(target: self, action: #selector(self.onPress(_:)))
                $0.removeFromSuperview()
            }
            var images:[UIImage] = []
            var assetsSucess:[PHAsset] = []
            let group = DispatchGroup()
            photos.forEach{ asset in
                group.enter()
                asset.getImage(size: CGSize(width: 80, height: 80)) { image in
                    if let image = image {
                        images.append(image)
                        assetsSucess.append(asset)
                    }
                    group.leave()
                }
            }
            
            group.notify(queue: .main) {[weak self] in
                guard let `self` = self else { return }
                self.vwStackImages.isHidden = images.count == 0
                images.enumerated().forEach {
                    let image = ImageView10Corner()
                    image.contentMode = .scaleAspectFill
                    image.image = $0.element
                    image.circularProgressView.isHidden = true
                    image.addTargetCustom(target: self, action: #selector(self.onPress(_:)), keepObject: assetsSucess[$0.offset])
                    image.addRemoveButton(target: self, action: #selector(self.remove(_:)),keepObject: assetsSucess[$0.offset])
                    self.stackImages.addArrangedSubview(image)
                    if $0.offset == 0 {
                        image.translatesAutoresizingMaskIntoConstraints = false
                        image.heightAnchor.constraint(equalToConstant: 80).isActive = true
                        image.widthAnchor.constraint(equalTo: image.heightAnchor).isActive = true
                    }
                    
                    
                }
                self.checkValidContent()
                self.updateHeightTextView()
            }
        }
    }
    
    @objc func onPress(_ sender:Button10Corner) {
        guard let asset = sender.object as? PHAsset else {return}
        asset.getImage(size: UIScreen.bounceWindow.size, deliveryMode: .highQualityFormat) {[weak self] image in
            guard let `self` = self, let image = image else { return }
            let browser = SKPhotoBrowser(photos: [SKPhoto(image: image)], initialPageIndex: 0)
            self.controller?.present(browser, animated: true, completion: nil)
        }
    }
    
    private func updateHeightTextView() {
        heightTextView.constant = vwStackImages.isHidden ? 150 : 75
        UIView.animate(withDuration: 0.25) {
            self.layoutIfNeeded()
        }
    }
    
    @objc func remove(_ sender:ButtonHalfHeight) {
        // TODO: cần xử lý remove chính xác image được chọn
        self.photosSelected.removeAll(where: {$0 == (sender.object as? PHAsset)})
        loadPhotos(photos: photosSelected)
        self.delegate?.UserInputComponentView_selectedAssets?(assets: photosSelected, component: self)
    }
}

extension UserInputComponentView:PhotosControllerDelegate {
    func selectedPhotos(photos: [PHAsset]) {
        self.photosSelected = photos
        loadPhotos(photos: photos)
        self.delegate?.UserInputComponentView_selectedAssets?(assets: photosSelected, component: self)
    }
}
