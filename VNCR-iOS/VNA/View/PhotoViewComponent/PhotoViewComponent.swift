//
//  PhotoViewComponent.swift
//  5GO
//
//  Created by Dai Pham on 16/07/2021.
//

import UIKit
import Photos
import PhotosUI

protocol PhotoViewComponentDelegate:AnyObject {
    func PhotoViewComponentDelegate_overMaxItemsConfig(view: PhotoViewComponent)
    
    /// notice delete a item
    /// - Parameters:
    ///   - view: PhotoViewComponent
    ///   - item: removed MediaItem
    ///   - index: index removed
    ///   - result: return true to delete immedately, else component wait response from result block
    func PhotoViewComponentDelegate_removeItem(view: PhotoViewComponent, item:MediaItem?, index:Int, result:@escaping ((Int,Bool)->Void)) -> Bool
    func PhotoViewComponentDelegate_updateHeight(view: PhotoViewComponent)
}

class PhotoViewComponent: BaseView {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var heightCollectionView: NSLayoutConstraint!
    
    weak var delegate:PhotoViewComponentDelegate?
    var maxItems:Int = 3
    let spacingItem:CGFloat = 10
    var items:[MediaItem] = []
    var sizeCell:CGSize = .zero {
        didSet {
            collectionView.collectionViewLayout.invalidateLayout()
        }
    }
    
    var backgroundColorCell:UIColor?
    
    weak var controller:BaseViewController?
    var objectUpload:Any? // upload fiel for object, purpose can: retry, error, progress process upload for object upload
    
    var numberUploaded:Int = 0
    
    var onShouldCancel:(()->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        view.backgroundColor = .clear
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isScrollEnabled = false
        collectionView.register(PhotoComponentCell.nib, forCellWithReuseIdentifier: PhotoComponentCell.identifier)
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        loadMedias(medias: [])
    }
    
    func loadMedias(medias:[MediaItem]) {
        
        // remove empty items
        items.removeAll(where: {$0.isEmpty})
        
        if items.count < maxItems {
            items.append(MediaItem(type: .image))
        }
        
        items = items.sorted(by: {!$0.isEmpty && $1.isEmpty})
        collectionView.reloadData()
        
        updateHeight()
    }
    
    func reset() {
        items = []
        loadMedias(medias: items)
    }
    
    func numerItemsSelected() -> Int {
        return items.filter({$0.asset != nil}).count
    }
    
    func setSizePhoto(size:CGSize) {
        sizeCell = size
    }
    
    func setBackgroundCellColor(color:UIColor?) {
        backgroundColorCell = color
    }
    
    func updateHeight() {
        let numberItemPerRow:Int = (self.controller?.isIpad == true && self.controller?.transitioningDelegate == nil) ? 6 : 3
        var numberRows = ceil(Double(items.count)/Double(numberItemPerRow))
        if numberRows <= 0 {
            numberRows = 1
        }
        if sizeCell == .zero {
            let width = (self.view.frame.size.width - (CGFloat(numberItemPerRow - 1) * spacingItem) - collectionView.contentInset.left - collectionView.contentInset.right) / CGFloat(numberItemPerRow)
            heightCollectionView.constant = (width * CGFloat(numberRows)) + (CGFloat(numberRows - 1) * spacingItem)  + collectionView.contentInset.top + collectionView.contentInset.bottom
        } else {
            heightCollectionView.constant = (sizeCell.width * CGFloat(numberRows)) + (CGFloat(numberRows - 1) * spacingItem)  + collectionView.contentInset.top + collectionView.contentInset.bottom
        }
        self.delegate?.PhotoViewComponentDelegate_updateHeight(view: self)
    }
    
    func presentLibrary(animation:Bool = true) {
        EasyPermission.requestPhotoLibrayPermission { (status) in
            if(status == EasyAuthorityStatus.authorizationStatusDenied) {
                DispatchQueue.main.async {
                    EasyPermission.alertTitle("Alert".localizedString(), message: "Press Setting for config Photos".localizedString())
                }
            } else {
                DispatchQueue.main.async {
                    let vc = PhotosController(type: .photo, maximumSelect: self.maxItems, delegate: self)
                    vc.setPhotos(items: self.items.compactMap({$0.asset}))
                    let nv = UINavigationController(rootViewController: vc)
                    self.controller?.present(nv, animated: true, completion: nil)
                }
            }
        }
    }
    
    func addItem(item: MediaItem) {
        if numerMediasSelected() >= maxItems {
            self.delegate?.PhotoViewComponentDelegate_overMaxItemsConfig(view: self)
            return
        }
        self.items.append(item)
        self.loadMedias(medias: self.items)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.collectionViewLayout.invalidateLayout()
        updateHeight()
    }
}

extension PhotoViewComponent: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, PhotoComponentCellDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoComponentCell.identifier, for: indexPath) as! PhotoComponentCell
        if backgroundColorCell != nil {
            cell.setBackgroundColor(color: backgroundColorCell)
        }
        cell.show(item: items[indexPath.row], delegate: self)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return spacingItem
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacingItem
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if sizeCell == .zero {
            let numberItemPerRow:Int = (self.controller?.isIpad == true && self.controller?.transitioningDelegate == nil) ? 6 : 3
            let width = round((self.view.frame.size.width - (CGFloat(numberItemPerRow - 1) * spacingItem) - collectionView.contentInset.left - collectionView.contentInset.right) / CGFloat(numberItemPerRow)) - 5
            return CGSize(width: width, height: width)
        } else {
            return sizeCell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        if item.isEmpty {
            presentLibrary()
        } else {
            item.asset?.getURL(completionHandler: {[weak self] url in guard let `self` = self, let url = url?.absoluteString else { return }
                let photo = SKPhoto(url: url)
                let vc = SKPhotoBrowser(photos: [photo])
                self.controller?.present(vc, animated: true, completion: nil)
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == items.count - 1 {
            updateHeight()
        }
    }
    
    func PhotoComponentCell_delete(cell: PhotoComponentCell, item: MediaItem) {
        if let index = collectionView.indexPath(for: cell)?.row {
            let taskRemove:(Int)->Void = {[weak self] index in guard let `self` = self else { return }
                self.items.remove(at: index)
                self.loadMedias(medias: self.items)
            }
            
            let shouldRemove = self.delegate?.PhotoViewComponentDelegate_removeItem(view: self, item: item, index: index, result: { index, isRemove in
                if isRemove {
                    taskRemove(index)
                }
            }) ?? true
            if shouldRemove {
                taskRemove(index)
            }
        }
    }
}

extension PhotoViewComponent:PhotosControllerDelegate {
    func selectedPhotos(photos: [PHAsset]) {
        items.removeAll(where:{$0.asset != nil})
        self.items.append(contentsOf: photos.compactMap({
            let media = MediaItem(type: $0.mediaType == .video ? .video : .image)
                media.asset = $0
                return media
            }))
        self.loadMedias(medias: self.items)
    }
    
    func numerMediasSelected() -> Int {
        return items.filter({!$0.isEmpty && $0.asset == nil}).count
    }
}
