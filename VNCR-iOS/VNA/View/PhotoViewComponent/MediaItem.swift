//
//  MediaItem.swift
//  5GO
//
//  Created by Pham Dai on 26/07/2021.
//

import Foundation
import Photos

class MediaItem: NSObject {
    
    enum MediaItemType:Int {
        case video
        case image
    }
    
    var object:Any?
    var image:UIImage?
    var urlString:String?
    var type:MediaItemType
    var isReadOnly:Bool = false
    var asset:PHAsset?
    
    var onProgress:((Float)->Void)?
    var onSuccess:((Any?)->Void)?
    var onError:((Any?)->Void)?
    
    var isEmpty:Bool {image == nil && urlString == nil && asset == nil}
    
    init(type:MediaItemType) {
        self.type = type
        super.init()
    }
    
    func getDataUpload(_ result:@escaping ((Data?,Any?) -> Void)) {
        if let asset = asset {
            asset.getDataToUpload { _, data, error in
                result(data,error)
            }
        }
    }
}
