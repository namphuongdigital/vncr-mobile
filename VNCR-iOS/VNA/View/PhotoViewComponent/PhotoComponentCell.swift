//
//  PhotoComponentCell.swift
//  5GO
//
//  Created by Dai Pham on 18/07/2021.
//

import UIKit
import Photos

protocol PhotoComponentCellDelegate:AnyObject {
    func PhotoComponentCell_delete(cell:PhotoComponentCell, item:MediaItem)
}

class PhotoComponentCell: BaseCollectionCell {

    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var thumbnailView: ImageView10Corner!
    @IBOutlet weak var btnDelete: ButtonHalfHeight!
    
    var requestAssetID:PHImageRequestID?
    
    var objectUpload:Any?
    var item:MediaItem?
    var isGetSig = false
    weak var delegate:PhotoComponentCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnDelete.setImage(UIImage(named: "ic_remove"), for: UIControl.State())
        
        lblTime.textColor = .white
        lblTime.font = .systemFont(ofSize: 12)
        
        thumbnailView.backgroundColor = .white
    }

    func setBackgroundColor(color:UIColor?) {
        thumbnailView.backgroundColor = color
    }
    
    func show(item:MediaItem, delegate:PhotoComponentCellDelegate?) {
        self.item = item
        self.delegate = delegate
        
        guard let media = self.item else {
            return
        }
        
        btnDelete.isHidden = media.isEmpty || media.isReadOnly
        
        thumbnailView.contentMode = .scaleAspectFill
        if media.isEmpty {
            thumbnailView.contentMode = .center
            thumbnailView.image = UIImage(named: "ic_cross")
        } else if let asset = media.asset {
            requestAssetID = asset.getImage(size: CGSize(width: 120, height: 120)) {[weak self] image in
                guard let `self` = self else {return}
                DispatchQueue.main.async {
                    self.thumbnailView.image = image
                }
            }
        } else {
            switch media.type {
            case .image:
                if let url = media.urlString {
                    thumbnailView.loadImageNoProgressBar(url: URL(string: url))
                } else {
                    thumbnailView.image = media.image
                }
            case .video:
                if let urlString = media.urlString,
                   let url = URL(string: urlString) {
                    thumbnailView.image = url.getThumbnail {[weak self] image, url in
                        guard let `self` = self, url == self.item?.urlString else { return }
                        self.thumbnailView.image = image
                    }
                }
            }
        }
        lblTime.isHidden = !(media.type == .video)
        if media.type == .video {
            thumbnailView.addButtonPlayVideo(target: nil, action: nil)
            if let asset = media.asset {
                lblTime.text = asset.duration.stringFromTimeInterval()
            }
        }
    }
    
    @IBAction func deleteItem(_ sender: UIButton) {
        if let item = self.item {
            delegate?.PhotoComponentCell_delete(cell: self, item: item)
        }
    }
    
    override func prepareForReuse() {
        thumbnailView.image = nil
        delegate = nil
        thumbnailView.removeButtonPlayVideo()
        if let id = requestAssetID,
           let asset = item?.asset {
            asset.cancelRequest(requestId: id)
        }
        requestAssetID = nil
        lblTime.isHidden = true
    }
}
