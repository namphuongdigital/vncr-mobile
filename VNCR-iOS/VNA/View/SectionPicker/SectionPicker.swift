//
//  SectionPicker.swift
//  VNA
//
//  Created by Pham Dai on 03/12/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class SectionPicker: BaseView {

    struct Value {
        var title:String?
        var dateVal:Int64?
        var timeVal:Int64?
        var showDate:Bool
        var showTime:Bool
        var identifier:String?
        
    }
    
    @IBOutlet weak var vwOne: Corner5BorderView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtInputOne: UITextField!
    
    @IBOutlet weak var vwTwo: Corner5BorderView!
    @IBOutlet weak var txtInputTwo: UITextField!
    
    var value:Value
    var didChange:((SectionPicker)->Void)?
    
    fileprivate let datePicker = UIDatePicker()
    fileprivate let timePicker = UIDatePicker()
    
    init(value:Value,didChange:((SectionPicker)->Void)?) {
        self.value = value
        super.init(frame: .zero)
        self.didChange = didChange
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.value = Value(showDate:false, showTime: false)
        super.init(coder: aDecoder)
    }
    
    override func config() {
        
        vwOne.borderColor = .gray
        vwTwo.borderColor = .gray
        
        lblTitle.font = .systemFont(ofSize: 16, weight: .medium)
        lblTitle.textColor = .black
        
        showDatePicker()
        showTimePicker()
        
        showValues()
    }
    
    func showValues(shouldPost:Bool = false) {
        vwOne.isHidden = !self.value.showDate
        vwTwo.isHidden = !self.value.showTime
        lblTitle.text = self.value.title
        
        if let v = self.value.dateVal {
            let date = Date(timeIntervalSinceReferenceDate: TimeInterval(v))
            txtInputOne.text = date.dateTimeToddMMyyyy
            datePicker.date = date
        }
        
        if let v = self.value.timeVal {
            let time = Date(timeIntervalSinceReferenceDate: TimeInterval(v))
            txtInputTwo.text = time.dateTimeToTimeHHmm
            timePicker.date = time
        }
        
        if shouldPost {
            self.didChange?(self)
        }
    }

}

extension SectionPicker {
    func showTimePicker(){
        //Formate Date
        txtInputTwo.clearButtonMode = .never
        timePicker.datePickerMode = .time

        timePicker.addTarget(self, action: #selector(donedatePickerChange), for: .valueChanged)
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localizedString(), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localizedString(), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.systemBlue], for: UIControl.State())
        cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.systemBlue], for: UIControl.State())
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: true)
        if #available(iOS 13.4, *) {
            timePicker.preferredDatePickerStyle = .wheels
        }
        txtInputTwo.inputAccessoryView = toolbar
        txtInputTwo.inputView = timePicker
    }
    
    func showDatePicker(){
        datePicker.datePickerMode = .date
        let calendar = Calendar(identifier: .gregorian)
        var comps = DateComponents()
        comps.year = 20
        let maxDate = calendar.date(byAdding: comps, to: Date())
        comps.year = -105
        let minDate = calendar.date(byAdding: comps, to: Date())
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate

        txtInputOne.clearButtonMode = .never
        
        datePicker.addTarget(self, action: #selector(donedatePickerChange), for: .valueChanged)
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done".localizedString(), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel".localizedString(), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        doneButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.systemBlue], for: UIControl.State())
        cancelButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.systemBlue], for: UIControl.State())
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: true)
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        }
        txtInputOne.inputAccessoryView = toolbar
        txtInputOne.inputView = datePicker
    }
    
    @objc func donedatePickerChange(){
        if txtInputOne.isFirstResponder {
            value.dateVal = Int64(datePicker.date.timeIntervalSinceReferenceDate)
        } else if txtInputTwo.isFirstResponder {
        value.timeVal = Int64(timePicker.date.timeIntervalSinceReferenceDate)
        }
        showValues(shouldPost: true)
    }
    
    @objc func donedatePicker(){
        if txtInputOne.isFirstResponder {
            value.dateVal = Int64(datePicker.date.timeIntervalSinceReferenceDate)
        } else if txtInputTwo.isFirstResponder {
        value.timeVal = Int64(timePicker.date.timeIntervalSinceReferenceDate)
        }
        showValues(shouldPost: true)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
}
