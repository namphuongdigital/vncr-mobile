//
//  ManagerRequests.swift
//  VNA
//
//  Created by Pham Dai on 28/09/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import SwiftyJSON

class ManagerRequests: NSObject {

    @available(iOS 10.0, *)
    static func getNeedSynceRequests(
        completed:@escaping (([StoredRequest]) -> Void)) {
            let userId = ServiceData.sharedInstance.userId ?? 0
            StoredRequestCD.getList(userId: userId) { list, haveData in
                completed(list.filter({$0.hasSynced == false}))
            }
    }
    
    @available(iOS 10.0, *)
    static func getRequest(
        type:Int,
        identifier:String,
        completed:@escaping ((StoredRequest?) -> Void)) {
        
            let userId = ServiceData.sharedInstance.userId ?? 0
            StoredRequestCD.get(identifier: identifier, type: type, userId: userId) { request in
                if let request = request, !request.hasSynced {// neu request chua synced
                    completed(request)
                } else {
                    completed(nil)
                }
                
            }
            
    }
    
    /// reuqest lotus and store request if no internet connection
    /// - Parameters:
    ///   - flightId: flight id
    ///   - selectedItemId: select option status shop
    ///   - lotusShopKey: lotusShopKey
    ///   - remark: remark
    ///   - deletedAttachment: []]
    ///   - attachments: files attachment
    ///   - completed: return block LotusShopStatus ,String Error
    static func requestLotus(flightId:Int,
                             selectedItemId:String,
                             lotusShopKey:String,
                             remark:String,
                             deletedAttachment:[Int],
                             attachments:[DynamicModelString],
                             completed: @escaping ((LotusShopStatus?,String?)->Void)) {
        
        let userId = ServiceData.sharedInstance.userId ?? 0
        var lotusShop:LotusShopStatus?
        var localError:String?
        var shouldMarkSynced:Bool = false
        
        let type = StoredRequest.RequestType.lotusShop.rawValue
        let identifier = "\(flightId)\(lotusShopKey)"
        let attachment = ServiceData.sharedInstance.toStringArrayJSon(array: attachments.compactMap({$0.dictionary}))
        let parameters: [String : Any] = ["FlightID": flightId,
                                          "LotusShopKey": lotusShopKey,
                                          "SelectedItemId": selectedItemId,
                                          "Remark": remark,
                                          "DeletedAttachment":deletedAttachment,
                                          "Attachment":attachment]
        
        let queue = OperationQueue()
        
        // stored first
        let taskStored = AsyncBlockOperation { op in
            if #available(iOS 10.0, *) {
                StoredRequestCD.checkAndSave(model: StoredRequest(userId: userId,
                                                                  hasSynced: false,
                                                                  dateSynced: nil,
                                                                  date: Date(),
                                                                  params: try? JSONSerialization.data(withJSONObject: parameters, options: .fragmentsAllowed),
                                                                  type: type,
                                                                  identifier: identifier),
                                             identifier: identifier,
                                             type: type,
                                             userId: userId) { error in
                    if let error = error as? String {
                        localError = error
                        op.complete()
                    } else {
                        lotusShop = LotusShopStatus(id: 1, flightID: flightId, lotusShopKey: lotusShopKey, selectedItemID: selectedItemId, remark: remark, attachments: attachments.compactMap({
                            let a = AttachmentCommonModel(FileID: 0, OriginalFileName: "", FoFileSource: "", FoFileURL: "", DisplayOrder: 0, FilePath: "", IsDeleted: false)
                            a.base64File = $0.ContentBase64
                            return a
                        }))
                        op.complete()
                    }
                }
            } else {
                op.complete()
            }
        }
        
        // sync if have internet
        let taskSynced = AsyncBlockOperation { op in
            if NetworkObserver.shared.isConnectedInternet {
                if #available(iOS 10.0, *) {
                    getRequest(type: type, identifier: identifier) { request in
                        if let params = request?.paramsDynamic {
                            ServiceData.sharedInstance.taskLotusShopSubmitReportSync(parameters: params.dictionary)
                                .continueOnSuccessWith(continuation: { task in
                                    #if DEBUG
                                    print("\(task) \(#function)")
                                    #endif
                                    shouldMarkSynced = true
                                    if let listData = try? (task as? JSON)?.rawData() {
                                        do {
                                            let data:LotusShopStatus? = try listData.load()
                                            lotusShop = data
                                            op.complete()
                                        } catch {
                                            lotusShop = LotusShopStatus(id: 1, flightID: flightId, lotusShopKey: lotusShopKey, selectedItemID: selectedItemId, remark: remark, attachments: attachments.compactMap({
                                                let a = AttachmentCommonModel(FileID: 0, OriginalFileName: "", FoFileSource: "", FoFileURL: "", DisplayOrder: 0, FilePath: "", IsDeleted: false)
                                                a.base64File = $0.ContentBase64
                                                return a
                                            }))
                                            op.complete()
                                        }
                                    } else {
                                        lotusShop = LotusShopStatus(id: 1, flightID: flightId, lotusShopKey: lotusShopKey, selectedItemID: selectedItemId, remark: remark, attachments: attachments.compactMap({
                                            let a = AttachmentCommonModel(FileID: 0, OriginalFileName: "", FoFileSource: "", FoFileURL: "", DisplayOrder: 0, FilePath: "", IsDeleted: false)
                                            a.base64File = $0.ContentBase64
                                            return a
                                        }))
                                        op.complete()
                                        
                                    }
                                }).continueOnErrorWith(continuation: { error in
                                    localError = (error as NSError).localizedDescription
                                    op.complete()
                                })
                        }
                    }
                } else {
                    ServiceData.sharedInstance.taskLotusShopSubmitReport(flightId: flightId,
                                                                         selectedItemId: selectedItemId,
                                                                         lotusShopKey: lotusShopKey,
                                                                         remark: remark,
                                                                         deletedAttachment: deletedAttachment,
                                                                         attachments: attachments)
                        .continueOnSuccessWith(continuation: { task in
                            #if DEBUG
                            print("\(task) \(#function)")
                            #endif
                            if let listData = try? (task as? JSON)?.rawData() {
                                do {
                                    let data:LotusShopStatus? = try listData.load()
                                    lotusShop = data
                                    op.complete()
                                } catch {
                                    lotusShop = LotusShopStatus(id: 1, flightID: flightId, lotusShopKey: lotusShopKey, selectedItemID: selectedItemId, remark: remark, attachments: attachments.compactMap({
                                        let a = AttachmentCommonModel(FileID: 0, OriginalFileName: "", FoFileSource: "", FoFileURL: "", DisplayOrder: 0, FilePath: "", IsDeleted: false)
                                        a.base64File = $0.ContentBase64
                                        return a
                                    }))
                                    op.complete()
                                }
                            } else {
                                lotusShop = LotusShopStatus(id: 1, flightID: flightId, lotusShopKey: lotusShopKey, selectedItemID: selectedItemId, remark: remark, attachments: attachments.compactMap({
                                    let a = AttachmentCommonModel(FileID: 0, OriginalFileName: "", FoFileSource: "", FoFileURL: "", DisplayOrder: 0, FilePath: "", IsDeleted: false)
                                    a.base64File = $0.ContentBase64
                                    return a
                                }))
                                op.complete()
                                
                            }
                        }).continueOnErrorWith(continuation: { error in
                            localError = (error as NSError).localizedDescription
                            op.complete()
                        })
                }
            } else {
                if #available(iOS 10.0, *) {
                } else { // warning for ios below 10
                    localError = "Your device if offline. Please try again when connected internet."
                }
                op.complete()
            }
        }
        
        // mark synced local
        let taskMarkSynced = AsyncBlockOperation { op in
            if #available(iOS 10.0, *), shouldMarkSynced {
                StoredRequestCD.markSynced(identifier: identifier, type: type, userId: userId, {_ in
                    op.complete()
                })
            } else {
                op.complete()
            }
        }
        
        taskSynced.addDependency(taskStored)
        taskMarkSynced.addDependency(taskSynced)
        queue.addOperations([taskStored,taskSynced,taskMarkSynced], waitUntilFinished: false)
        
        taskMarkSynced.completionBlock = {
            completed(lotusShop,localError)
        }
    }
}


// MARK: - StoredRequest
public class StoredRequest: Codable {
    
    enum RequestType:Int {
        case lotusShop = 1
        case other = 2
    }
    
    public var userId: Int
    public var hasSynced: Bool
    public var dateSynced, date: Date?
    public var params:Data?
    public var type: Int
    public var identifier: String?
    
    public var paramsDynamic:DynamicModel?

    enum CodingKeys: String, CodingKey {
        case userId = "userId"
        case hasSynced, dateSynced, date, params, type, identifier
    }

    public init(userId: Int, hasSynced: Bool, dateSynced: Date?, date: Date?, params: Data?, type: Int,identifier: String?) {
        self.userId = userId
        self.hasSynced = hasSynced
        self.dateSynced = dateSynced
        self.date = date
        self.params = params
        self.type = type
        self.identifier = identifier
        
        if let data = params,
           let object = try? JSONSerialization.jsonObject(with: data, options: .fragmentsAllowed) as? [String:Any] {
            paramsDynamic = DynamicModel(dictionary: object)
        }
    }
    
    var lotusShopStatus:LotusShopStatus? {
        
        let flightID = paramsDynamic?.FlightID as? Int ?? 0
        let lotusShopKey:String = paramsDynamic?.LotusShopKey as? String ?? ""
        let selectedItemID:String? = paramsDynamic?.SelectedItemId as? String
        let remark:String? = paramsDynamic?.Remark as? String
        var attachments:[AttachmentCommonModel] = []
        
        if let attachmentString:String = paramsDynamic?.Attachment as? String,
           let array = ServiceData.sharedInstance.toArrayJSon(string: attachmentString) {
            let temp = array.compactMap({
                DynamicModel(dictionary: $0)
            })
            
            attachments = temp.compactMap({
                let a = AttachmentCommonModel(FileID: 0, OriginalFileName: "", FoFileSource: "", FoFileURL: "", DisplayOrder: 0, FilePath: "", IsDeleted: false)
                a.base64File = $0.ContentBase64 as? String
                return a
            })
        }
        
        return LotusShopStatus(id: 1,
                               flightID: flightID,
                               lotusShopKey: lotusShopKey,
                               selectedItemID: selectedItemID,
                               remark: remark,
                               attachments: attachments)
    }
}
