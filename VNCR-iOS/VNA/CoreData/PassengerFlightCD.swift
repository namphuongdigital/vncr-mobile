//
//  PassengerFlightCD.swift
//  VNA
//
//  Created by Pham Dai on 10/09/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation
import CoreData

@available(iOS 10.0, *)
extension PassengerFlightCD {
    @NSManaged public var flightId: Int
    @NSManaged public var hasSynced: Bool
    @NSManaged public var data: Data?
    @NSManaged public var lastUpdate: Date?
}

@available(iOS 10.0, *)
@objc(PassengerFlightCD)
open class PassengerFlightCD: NSManagedObject {
    
    //MARK: - Initialize
    convenience init(context: NSManagedObjectContext?) {
        
        // Create the NSEntityDescription
        let entity = NSEntityDescription.entity(forEntityName: "PassengerFlightCD", in: context!)
        
        self.init(entity: entity!, insertInto: context)
    }
    
    @nonobjc public class func fetchRequestD() -> NSFetchRequest<PassengerFlightCD> {
        let fetch = NSFetchRequest<PassengerFlightCD>(entityName: "PassengerFlightCD")
        return fetch
    }
    
    static public func getList(flightId:Int,
                               _ complete:((_ list:PassengerListModel,_ haveData:Bool)->Void)?) {
        
        let context = CoreData.sharedInstance.managedObjectContext
        context.perform {
            let fetchRequest = PassengerFlightCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "flightId == \(flightId)")]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                if let list = try context.fetch(fetchRequest).first,
                   let data1 = list.data,
                   let sub:PassengerListModel? = try? data1.load() {
                    complete?(sub ?? [],true)
                } else {
                    complete?([], false)
                }
            } catch {
                complete?([], false)
            }
        }
    }
    
    static func checkAndSave(model:PassengerListModel?,
                     flightId:Int,
                     complete:COMPLETEDERRORRESULT = nil) {
        guard let sub = model else {
            complete?("Invalid Data")
            return
        }
        
        PassengerFlightCD.isExist(flightId: flightId, { isExist in
            if isExist {
                PassengerFlightCD.update(model: sub, flightId: flightId, complete)
            } else {
                PassengerFlightCD.save(model: sub, flightId: flightId, complete: complete)
            }
        })
    }
    
    static func checkAndUpdate(model:PassengerListModel?,
                       flightId:Int,
                       _ complete:COMPLETEDERRORRESULT = nil) {
        
        guard let sub = model else {
            complete?("Invalid Data")
            return
        }
        
        PassengerFlightCD.isExist(flightId: flightId, { isExist in
            if isExist {
                PassengerFlightCD.update(model: sub, flightId: flightId, complete)
            } else {
                PassengerFlightCD.save(model: sub, flightId: flightId, complete: complete)
            }
        })
    }
    
    static private func save(model:PassengerListModel,
                     flightId:Int,
                     complete:COMPLETEDERRORRESULT = nil) {
        
        let container = CoreData.sharedInstance.saveManagedObjectContext
        container.perform {
            if let object = NSEntityDescription.insertNewObject(forEntityName: "PassengerFlightCD", into: container) as? PassengerFlightCD {
                object.flightId = flightId
                if let data = try? model.jsonData() {
                    object.data = data
                }
                object.lastUpdate = Date()
            }
            do {
                try container.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(#function) \(err.localizedDescription) ")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static private func update(model:PassengerListModel?,
                       flightId:Int,
                       _ complete:COMPLETEDERRORRESULT = nil) {
        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = PassengerFlightCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "flightId == \(flightId)")]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                try context.fetch(fetchRequest).forEach({
                    if let data = try? model?.jsonData() {
                        $0.setValue(data, forKey: "data")
                    }
                    $0.setValue(Date(), forKey: "lastUpdate")
                    $0.setValue(true, forKey: "hasSynced")
                })
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static func isExist(flightId:Int,
                        _ completed:COMPLETEDBOOLRESULT) {
        do {
            let context = CoreData.sharedInstance.managedObjectContext
            context.perform {
                let fetchRequest = PassengerFlightCD.fetchRequestD()
                let listPredicate = [NSPredicate(format: "flightId == \(flightId)")]
                let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
                fetchRequest.predicate = finalPredicate
                do {
                    let count  = try context.count(for: fetchRequest)
                    completed?(count > 0)
                } catch  {
                    completed?(false)
                }
            }
        }
    }
    
    static func delete(flightId:Int,
                       _ complete:COMPLETEDERRORRESULT) {

        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = PassengerFlightCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "flightId == \(flightId)")]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                try context.fetch(fetchRequest).forEach({context.delete($0)})
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static func reset(_ complete:COMPLETEDERRORRESULT) {
        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = PassengerFlightCD.fetchRequestD()
            do {
                try context.fetch(fetchRequest).forEach({context.delete($0)})
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
}
