//
//  ServiceDataCD.swift
//  VNA
//
//  Created by Pham Dai on 10/09/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation
import SwiftyJSON

public typealias COMPLETEDNORESULT = (()->Void)?
public typealias COMPLETEDBOOLRESULT = ((Bool)->Void)?
public typealias COMPLETEDERRORRESULT = ((Any?)->Void)?

public class ServiceDataCD: NSObject {
    
}

// MARK: -  FLight
public extension ServiceDataCD {
    
    /// save a  flight to local database
    /// - Parameters:
    ///   - flightId: flight id
    ///   - userId: suer id
    ///   - model: scheduleFlightModel
    ///   - isPeronal: true if save from personal schedules list
    ///   - isGetFull: if true,  get detail from server then save
    ///   - completed: complete block with error
    @available(iOS 10.0, *)
    static func saveFlight(flightId:Int, userId:Int, model:ScheduleFlightModel, isPersonal:Bool, isGetFull:Bool, completed:COMPLETEDERRORRESULT) {
        var tempModel = model
        let group = DispatchGroup()
        if isGetFull {
            group.enter()
            ServiceData.sharedInstance.taskGetFlightScheduleDetail(flightId: flightId)
                .continueOnSuccessWith(continuation: {task in
                    #if DEBUG
                    print("\(task) \(#function)")
                    #endif
                    if let listData = try? (task as? JSON)?.rawData(),
                       let object:ScheduleFlightModel = try? listData.load() {
                        tempModel = object
                    }
                    group.leave()
                })
                .continueOnErrorWith(continuation: {error in
                    group.leave()
                })
        }
        
        // save passengers list
        group.enter()
        savePassengers(flightId: flightId) { error in
            group.leave()
        }
        
        // save crew task
        group.enter()
        saveCrewTask(flightId: flightId) { _ in
            group.leave()
        }
        
        // save crew task category
        group.enter()
        saveCrewTaskCategory(flightId: flightId) { _ in
            group.leave()
        }
        
        group.notify(queue: .global()) {
            FlightCD.checkAndSave(model: tempModel, userId: userId, flightId: flightId, isPersonal: isPersonal, complete: completed)
        }
    }
    
    /// update a  flight to local database
    /// - Parameters:
    ///   - flightId: flight id
    ///   - userId: suer id
    ///   - model: scheduleFlightModel
    ///   - isPeronal: true if save from personal schedules list
    ///   - isGetFull: if true,  get detail from server then save
    ///   - completed: complete block with error
    @available(iOS 10.0, *)
    static func updateFlight(flightId:Int, userId:Int, model:ScheduleFlightModel, isPersonal:Bool, isGetFull:Bool, completed:COMPLETEDERRORRESULT) {
        
        let group = DispatchGroup()
        
        // get passengers list
        group.enter()
        savePassengers(flightId: flightId) { error in
            group.leave()
        }
        
        // save crew task
        group.enter()
        saveCrewTask(flightId: flightId) { _ in
            group.leave()
        }
        
        // save crew task category
        group.enter()
        saveCrewTaskCategory(flightId: flightId) { _ in
            group.leave()
        }
        
        group.notify(queue: .global()) {
            if isGetFull {
                saveFlight(flightId: flightId, userId: userId, model: model, isPersonal: isPersonal, isGetFull: isGetFull, completed: completed)
            } else {
                FlightCD.checkAndUpdate(model: model, userId: userId, flightId: flightId, isPersonal: isPersonal, completed)
            }
        }
    }
    
    /// get personal schedule flights list with month
    /// - Parameters:
    ///   - month: month
    ///   - pageIndex: page index
    ///   - userId: current user Id
    ///   - completed: completed block: flights list, error string
    static func getFlightsPersonal(month: Int,
                                   pageIndex: Int,
                                   userId:Int,
                                   isOffline:Bool,
                                   completed: @escaping ((ScheduleFlightModels, String?)->Void)) {
        
        if isOffline,
           #available(iOS 10.0, *) {
            
            FlightCD.getList(userId: userId, isPersonal: true) { list, haveData in
                completed(list,nil)
            }
            
        } else {
            ServiceData.sharedInstance.taskFlightPersonalschedule(month: month, pageIndex: pageIndex)
                .continueOnSuccessWith(continuation: { task in
                    
                    if let listData = try? (task as? JSON)?.rawData(),
                       let list:ScheduleFlightModels = try? listData.load() {
                        completed(list,nil)
                    } else {
                        completed([],nil)
                    }
                    
                })
                .continueOnErrorWith(continuation: { error in
                    completed([],(error as NSError).localizedDescription)
                })
        }
    }
    
    /// get flights list from data to date
    /// - Parameters:
    ///   - fromDate: fromDate
    ///   - toDate: toDate
    ///   - keyword: search keyword
    ///   - pageIndex: pageIndex
    ///   - userId: current user Id
    ///   - completed: completed block: flights list, error string
    static func getFlights(fromDate: Date,
                           toDate: Date,
                           keyword: String,
                           pageIndex: Int,
                           userId:Int,
                           isOffline:Bool,
                           completed: @escaping ((ScheduleFlightModels, String?)->Void)) {
        
        if isOffline,
           #available(iOS 10.0, *) {
            FlightCD.getList(userId: userId, isPersonal: false) { list, haveData in
                completed(list,nil)
            }
            
        } else {
            ServiceData.sharedInstance.taskFlightInfoList(fromDate: fromDate, toDate: toDate, keyword: keyword, isGetAll: true, pageIndex: pageIndex)
                .continueOnSuccessWith(continuation: { task in
                    
                    if let listData = try? (task as? JSON)?.rawData(),
                       let list:ScheduleFlightModels = try? listData.load() {
                        completed(list,nil)
                    } else {
                        completed([],nil)
                    }
                    
                })
                .continueOnErrorWith(continuation: { error in
                    completed([],(error as NSError).localizedDescription)
                })
        }
    }
    
    /// get flight detail with check internet, if no internet => get local else get from server
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block: ScheduleFlightModel, error string
    static func getFlightDetail(flightId:Int,
                                completed:@escaping ((ScheduleFlightModel?,String?)->Void)) {
        var temp:ScheduleFlightModel?
        var localError:String?
        let group = DispatchGroup()
        let userId = ServiceData.sharedInstance.userId ?? 0
        if !NetworkObserver.shared.isConnectedInternet,
           #available(iOS 10.0, *) {
            group.enter()
            FlightCD.get(userId: userId, flightId: flightId) { model, haveData in
                temp = model
                group.leave()
            }
        } else {
            group.enter()
            ServiceData.sharedInstance.taskGetFlightScheduleDetail(flightId: flightId)
                .continueOnSuccessWith(continuation: {task in
//                    #if DEBUG
//                    print("\(task) \(#function)")
//                    #endif
                    if let listData = try? (task as? JSON)?.rawData(),
                       let object:ScheduleFlightModel = try? listData.load() {
                        temp = object
                    }
                    group.leave()
                }).continueOnErrorWith(continuation: {error in
                    localError = (error as NSError).localizedDescription
                    group.leave()
                })
        }
        
        group.notify(queue: .global()) {
            completed(temp,localError)
        }
    }
    
    /// check flight is saved
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block bool
    @available(iOS 10.0,*)
    static func flightIsSaved(flightId:Int, completed:COMPLETEDBOOLRESULT) {
        let userId = ServiceData.sharedInstance.userId ?? 0
        FlightCD.isExist(userId: userId, flightId: flightId, completed)
    }
    
    /// delete a storage flight
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block error string
    @available(iOS 10.0,*)
    static func deleteSavedFlight(flightId:Int, completed:COMPLETEDERRORRESULT) {
        let userId = ServiceData.sharedInstance.userId ?? 0
        let group = DispatchGroup()
        
        // delete passengers
        group.enter()
        deletePassengers(flightId: flightId) { error in
            group.leave()
        }
        
        // delete crew task
        group.enter()
        deleteCrewTask(flightId: flightId) { error in
            group.leave()
        }
        
        // delete crew task
        group.enter()
        deleteCrewTaskCategory(flightId: flightId) { error in
            group.leave()
        }
        
        group.notify(queue: .global()) {
            FlightCD.delete(userId: userId, flightId: flightId, completed)
        }
    }
    
}

// MARK: -  Passenger List
extension ServiceDataCD {
    
    /// get passengers list local & server
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block PassengerListModel?, error string
    static func getPassengers(flightId:Int, completed:@escaping ((PassengerListModel?,String?)->Void)) {
        var temp:PassengerListModel?
        var localError:String?
        let group = DispatchGroup()
        if !NetworkObserver.shared.isConnectedInternet,
           #available(iOS 10.0, *) {
            group.enter()
            PassengerFlightCD.getList(flightId: flightId) { list, haveData in
                temp = list
                group.leave()
            }
        } else {
            group.enter()
            ServiceData.sharedInstance.taskGetPassengerList(flightId: flightId).continueOnSuccessWith(continuation: {task in
                #if DEBUG
                print("\(task) \(#function)")
                #endif
                if let listData = try? (task as? JSON)?.rawData() {
                    do {
                        let list:PassengerListModel? = try listData.load()
                        temp = list
                    } catch let err {
                        localError = (err as NSError).localizedDescription
                    }
                }
                group.leave()
            }).continueOnErrorWith(continuation: { error in
                localError = (error as NSError).localizedDescription
                group.leave()
            })
        }
        
        group.notify(queue: .global()) {
            completed(temp,localError)
        }
    }
    
    /// save passengers's flight
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block error string
    @available(iOS 10.0, *)
    static func savePassengers(flightId:Int, completed:COMPLETEDERRORRESULT) {
        var temp: PassengerListModel?
        let group = DispatchGroup()
        
        group.enter()
        ServiceData.sharedInstance.taskGetPassengerList(flightId: flightId).continueOnSuccessWith(continuation: {task in
            #if DEBUG
            print("\(task) \(#function)")
            #endif
            if let listData = try? (task as? JSON)?.rawData() {
                do {
                    let list:PassengerListModel? = try listData.load()
                    temp = list
                } catch let err {
                    #if DEBUG
                    print("\((err as NSError).description) \(#function)")
                    #endif
                }
            }
            group.leave()
        }).continueOnErrorWith(continuation: { error in
            group.leave()
        })
        
        group.notify(queue: .global()) {
            PassengerFlightCD.checkAndSave(model: temp, flightId: flightId, complete: completed)
        }
    }
    
    /// delete passengers's flight
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block error string
    @available(iOS 10.0, *)
    static func deletePassengers(flightId:Int, completed:COMPLETEDERRORRESULT) {
        PassengerFlightCD.delete(flightId: flightId, completed)
    }
    
}

// MARK: -  CrewTaskCD
extension ServiceDataCD {
    
    /// get passengers list local & server
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block PassengerListModel?, error string
    static func getCrewTask(flightId:Int, completed:@escaping ((CrewTaskModel?,String?)->Void)) {
        var temp:CrewTaskModel?
        var localError:String?
        let group = DispatchGroup()
        if !NetworkObserver.shared.isConnectedInternet,
           #available(iOS 10.0, *) {
            group.enter()
            CrewTaskCD.get(flightId: flightId) { task, haveData in
                temp = task
                group.leave()
            }
        } else {
            group.enter()
            ServiceData.sharedInstance.taskGetFlightCrewtaskgetlistsupporttrip(flightId: flightId, isGetTrip:false).continueOnSuccessWith(continuation: {task in
                #if DEBUG
                print("\(task) \(#function)")
                #endif
                if let listData = try? (task as? JSON)?.rawData() {
                    do {
                        let task:CrewTaskModel? = try listData.load()
                        temp = task
                    } catch let err {
                        localError = (err as NSError).localizedDescription
                    }
                }
                group.leave()
            }).continueOnErrorWith(continuation: { error in
                localError = (error as NSError).localizedDescription
                group.leave()
            })
        }
        
        group.notify(queue: .global()) {
            completed(temp,localError)
        }
    }
    
    /// save passengers's flight
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block error string
    @available(iOS 10.0, *)
    static func saveCrewTask(flightId:Int, completed:COMPLETEDERRORRESULT) {
        var temp: CrewTaskModel?
        let group = DispatchGroup()
        
        group.enter()
        ServiceData.sharedInstance.taskGetFlightCrewtaskgetlistsupporttrip(flightId: flightId, isGetTrip:false).continueOnSuccessWith(continuation: {task in
            #if DEBUG
            print("\(task) \(#function)")
            #endif
            if let listData = try? (task as? JSON)?.rawData() {
                do {
                    let task:CrewTaskModel? = try listData.load()
                    temp = task
                } catch let err {
                    #if DEBUG
                    print("\(err.localizedDescription) \(#function)")
                    #endif
                }
            }
            group.leave()
        }).continueOnErrorWith(continuation: { error in
            group.leave()
        })
        
        group.notify(queue: .global()) {
            CrewTaskCD.checkAndSave(model: temp, flightId: flightId, complete: completed)
        }
    }
    
    /// delete passengers's flight
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block error string
    @available(iOS 10.0, *)
    static func deleteCrewTask(flightId:Int, completed:COMPLETEDERRORRESULT) {
        CrewTaskCD.delete(flightId: flightId, completed)
    }
    
}

// MARK: -  CrewTaskCategoryCD
extension ServiceDataCD {
    
    /// get passengers list local & server
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block PassengerListModel?, error string
    static func getCrewTaskCategory(flightId:Int, completed:@escaping ((SelectorTypeModels?,String?)->Void)) {
        var temp:SelectorTypeModels?
        var localError:String?
        let group = DispatchGroup()
        if !NetworkObserver.shared.isConnectedInternet,
           #available(iOS 10.0, *) {
            group.enter()
            CrewTaskCategoryCD.get(flightId: flightId) { task, haveData in
                temp = task
                group.leave()
            }
        } else {
            group.enter()
            ServiceData.sharedInstance.taskFlightCrewtaskcategorylist().continueOnSuccessWith(continuation: {task in
                #if DEBUG
                print("\(task) \(#function)")
                #endif
                if let listData = try? (task as? JSON)?.rawData() {
                    do {
                        let task:SelectorTypeModels? = try listData.load()
                        temp = task
                    } catch let err {
                        localError = (err as NSError).localizedDescription
                    }
                }
                group.leave()
            }).continueOnErrorWith(continuation: { error in
                localError = (error as NSError).localizedDescription
                group.leave()
            })
        }
        
        group.notify(queue: .global()) {
            completed(temp,localError)
        }
    }
    
    /// save passengers's flight
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block error string
    @available(iOS 10.0, *)
    static func saveCrewTaskCategory(flightId:Int, completed:COMPLETEDERRORRESULT) {
        var temp: SelectorTypeModels?
        let group = DispatchGroup()
        
        group.enter()
        ServiceData.sharedInstance.taskFlightCrewtaskcategorylist().continueOnSuccessWith(continuation: {task in
            #if DEBUG
            print("\(task) \(#function)")
            #endif
            if let listData = try? (task as? JSON)?.rawData() {
                do {
                    let task:SelectorTypeModels? = try listData.load()
                    temp = task
                } catch let err {
                }
            }
            group.leave()
        }).continueOnErrorWith(continuation: { error in
            group.leave()
        })
        
        group.notify(queue: .global()) {
            CrewTaskCategoryCD.checkAndSave(model: temp, flightId: flightId, complete: completed)
        }
    }
    
    /// delete passengers's flight
    /// - Parameters:
    ///   - flightId: flight id
    ///   - completed: completed block error string
    @available(iOS 10.0, *)
    static func deleteCrewTaskCategory(flightId:Int, completed:COMPLETEDERRORRESULT) {
        CrewTaskCategoryCD.delete(flightId: flightId, completed)
    }
    
}
