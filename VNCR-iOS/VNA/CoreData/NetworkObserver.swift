//
//  ManageStoredRequests.swift
//  VNA
//
//  Created by Pham Dai on 28/09/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class NetworkObserver: NSObject {
    public static let shared: NetworkObserver = { return NetworkObserver() }()
    
    var internetReachability:Reachability!
    var isConnectedInternet:Bool = false {
        didSet {
            if isConnectedInternet {
                if #available(iOS 10.0, *) {
                    syncedStoredRequests()
                }
            }
        }
    }
    
    override init() {
        super.init()
        
        internetReachability = Reachability.forInternetConnection()
        isConnectedInternet = internetReachability.currentReachabilityStatus() != NotReachable
        NotificationCenter.default.addObserver(self, selector: #selector(self.observerNetwork(object:)), name: NSNotification.Name.reachabilityChanged, object: nil)
    }
    
    func startNotifierNetwork() {
        internetReachability.startNotifier()
    }
    
    func stopNotifierNetwork() {
        internetReachability.stopNotifier()
    }
    
    @objc func observerNetwork(object: Notification) {
        if let reachability = object.object as? Reachability {
            updateInterfaceWithReachability(reachability: reachability)
        }
    }
    
    @objc func updateInterfaceWithReachability(reachability:Reachability) {
        let netStatus = reachability.currentReachabilityStatus()
        switch netStatus {
        case NotReachable:
            // not internet connection
            #if DEBUG
            print("\("Internet Not Available") \(#function)")
            #endif
            networkDidChanged(connectedInternet: false, reachability: reachability)
        case ReachableViaWWAN, ReachableViaWiFi:
            #if DEBUG
            print("\("Reachable WWAN") \(#function)")
            #endif
            networkDidChanged(connectedInternet: true, reachability: reachability)
        default:
            break
        }
    }
    
    @objc func networkDidChanged(connectedInternet:Bool, reachability:Reachability) {
        // override to handle
        self.isConnectedInternet = connectedInternet
    }
    
    
    @available(iOS 10.0, *)
    /// synced request
    private func syncedStoredRequests() {
        ManagerRequests.getNeedSynceRequests { list in
            list.forEach({ request in
                switch request.type {
                case StoredRequest.RequestType.lotusShop.rawValue:
                    if let params = request.paramsDynamic?.dictionary {
                        
                        ServiceData.sharedInstance.taskLotusShopSubmitReportSync(parameters: params)
                            .continueOnSuccessWith(continuation: { task in
                                StoredRequestCD.markSynced(identifier: request.identifier ?? "", type: request.type, userId: request.userId, nil)
                            })
                    }
                default:break
                }
            })
        }
    }
    
}
