//
//  FlightCD.swift
//  VNA
//
//  Created by Pham Dai on 10/09/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation
import CoreData

@available(iOS 10.0, *)
extension FlightCD {
    @NSManaged public var userId: Int
    @NSManaged public var flightId: Int
    @NSManaged public var isPersonal: Bool
    @NSManaged public var hasSynced: Bool
    @NSManaged public var data: Data?
    @NSManaged public var lastUpdate: Date?
}

@available(iOS 10.0, *)
@objc(FlightCD)
open class FlightCD: NSManagedObject {
    
    //MARK: - Initialize
    convenience init(context: NSManagedObjectContext?) {
        
        // Create the NSEntityDescription
        let entity = NSEntityDescription.entity(forEntityName: "FlightCD", in: context!)
        
        self.init(entity: entity!, insertInto: context)
    }
    
    @nonobjc public class func fetchRequestD() -> NSFetchRequest<FlightCD> {
        let fetch = NSFetchRequest<FlightCD>(entityName: "FlightCD")
        return fetch
    }
    
    static public func getList(userId:Int,
                           isPersonal:Bool,
                           _ complete:((_ list:ScheduleFlightModels,_ haveData:Bool)->Void)?) {
        
        let context = CoreData.sharedInstance.managedObjectContext
        context.perform {
            let fetchRequest = FlightCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                 NSPredicate(format: "isPersonal == \(isPersonal)")]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                let list = try context.fetch(fetchRequest).compactMap({ (item) -> ScheduleFlightModel? in
                    if let data1 = item.data,
                       let sub:ScheduleFlightModel? = try? data1.load() {
                        return sub
                    }
                    return nil
                })
                complete?(list, list.count > 0)
            } catch {
                complete?([], false)
            }
        }
    }
    
    static public func get(userId:Int,
                           flightId:Int,
                           _ complete:((_ scheduleFlightModel:ScheduleFlightModel?,_ haveData:Bool)->Void)?) {
        
        let context = CoreData.sharedInstance.managedObjectContext
        context.perform {
            let fetchRequest = FlightCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                 NSPredicate(format: "flightId == \(flightId)")]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                let list = try context.fetch(fetchRequest).compactMap({ (item) -> ScheduleFlightModel? in
                    if let data1 = item.data,
                       let sub:ScheduleFlightModel? = try? data1.load() {
                        return sub
                    }
                    return nil
                })
                complete?(list.first, list.count > 0)
            } catch {
                complete?(nil, false)
            }
        }
    }
    
    static func checkAndSave(model:ScheduleFlightModel?,
                     userId:Int,
                     flightId:Int,
                     isPersonal:Bool,
                     complete:COMPLETEDERRORRESULT = nil) {
        guard let sub = model else {
            complete?("Invalid Data")
            return
        }
        
        FlightCD.isExist(userId: userId, flightId: flightId, { isExist in
            if isExist {
                FlightCD.update(model: sub, userId: userId, flightId: flightId, isPersonal: isPersonal,complete)
            } else {
                FlightCD.save(model: sub, userId: userId, flightId: flightId, isPersonal: isPersonal,complete: complete)
            }
        })
    }
    
    static func checkAndUpdate(model:ScheduleFlightModel?,
                       userId:Int,
                       flightId:Int,
                       isPersonal:Bool,
                       _ complete:COMPLETEDERRORRESULT = nil) {
        
        guard let sub = model else {
            complete?("Invalid Data")
            return
        }
        
        FlightCD.isExist(userId: userId, flightId: flightId, { isExist in
            if isExist {
                FlightCD.update(model: sub, userId: userId, flightId: flightId, isPersonal: isPersonal, complete)
            } else {
                FlightCD.save(model: sub, userId: userId, flightId: flightId, isPersonal: isPersonal, complete: complete)
            }
        })
    }
    
    static private func save(model:ScheduleFlightModel,
                     userId:Int,
                     flightId:Int,
                     isPersonal:Bool,
                     complete:COMPLETEDERRORRESULT = nil) {
        
        let container = CoreData.sharedInstance.saveManagedObjectContext
        container.perform {
            if let object = NSEntityDescription.insertNewObject(forEntityName: "FlightCD", into: container) as? FlightCD {
                object.userId = userId
                object.isPersonal = isPersonal
                object.flightId = flightId
                if let data = try? model.jsonData() {
                    object.data = data
                }
                object.lastUpdate = Date()
            }
            do {
                try container.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(#function) \(err.localizedDescription) ")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static private func update(model:ScheduleFlightModel?,
                       userId:Int,
                       flightId:Int,
                       isPersonal:Bool,
                       _ complete:COMPLETEDERRORRESULT = nil) {
        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = FlightCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                 NSPredicate(format: "isPersonal == \(isPersonal)"),
                                 NSPredicate(format: "flightId == \(flightId)")]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                try context.fetch(fetchRequest).forEach({
                    if let data = try? model?.jsonData() {
                        $0.setValue(data, forKey: "data")
                    }
                    $0.setValue(true, forKey: "hasSynced")
                    $0.setValue(Date(), forKey: "lastUpdate")
                })
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static func isExist(userId:Int,
                        flightId:Int,
                        _ completed:COMPLETEDBOOLRESULT) {
        do {
            let context = CoreData.sharedInstance.managedObjectContext
            context.perform {
                let fetchRequest = FlightCD.fetchRequestD()
                let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                     NSPredicate(format: "flightId == \(flightId)")]
                let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
                fetchRequest.predicate = finalPredicate
                do {
                    let count  = try context.count(for: fetchRequest)
                    completed?(count > 0)
                } catch  {
                    completed?(false)
                }
            }
        }
    }
    
    static func delete(userId:Int,
                       flightId:Int,
                       _ complete:COMPLETEDERRORRESULT) {

        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = FlightCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                 NSPredicate(format: "flightId == \(flightId)")]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                try context.fetch(fetchRequest).forEach({context.delete($0)})
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static func reset(_ complete:COMPLETEDERRORRESULT) {
        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = FlightCD.fetchRequestD()
            do {
                try context.fetch(fetchRequest).forEach({context.delete($0)})
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
}
