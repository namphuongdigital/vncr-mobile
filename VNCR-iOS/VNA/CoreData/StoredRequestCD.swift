//
//  StoredRequestCD.swift
//  VNA
//
//  Created by Pham Dai on 10/09/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation
import CoreData
import RealmSwift

@available(iOS 10.0, *)
extension StoredRequestCD {
    @NSManaged public var userId: Int
    @NSManaged public var hasSynced: Bool
    @NSManaged public var dateSynced: Date?
    @NSManaged public var params: Data?
    @NSManaged public var date: Date?
    @NSManaged public var type: Int
    @NSManaged public var identifier: String?
}

@available(iOS 10.0, *)
@objc(StoredRequestCD)
open class StoredRequestCD: NSManagedObject {
    
    //MARK: - Initialize
    convenience init(context: NSManagedObjectContext?) {
        
        // Create the NSEntityDescription
        let entity = NSEntityDescription.entity(forEntityName: "StoredRequestCD", in: context!)
        
        self.init(entity: entity!, insertInto: context)
    }
    
    @nonobjc public class func fetchRequestD() -> NSFetchRequest<StoredRequestCD> {
        let fetch = NSFetchRequest<StoredRequestCD>(entityName: "StoredRequestCD")
        return fetch
    }
    
    static public func getList(userId:Int,
                               _ complete:((_ list:[StoredRequest],_ haveData:Bool)->Void)?) {
        
        let context = CoreData.sharedInstance.managedObjectContext
        context.perform {
            let fetchRequest = StoredRequestCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "userId == \(userId)")]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                var list:[StoredRequest] = []
                try context.fetch(fetchRequest).forEach({ cd in
                    list.append(StoredRequest(userId: cd.userId,
                                              hasSynced: cd.hasSynced,
                                              dateSynced: cd.dateSynced,
                                              date: cd.date,
                                              params: cd.params,
                                              type: cd.type,
                                              identifier: cd.identifier))
                })
                complete?(list,list.count > 0)
            } catch {
                complete?([], false)
            }
        }
    }
    
    static public func get(identifier:String,
                           type:Int,
                           userId:Int,
                               _ complete:((_ item:StoredRequest?)->Void)?) {
        
        let context = CoreData.sharedInstance.managedObjectContext
        context.perform {
            let fetchRequest = StoredRequestCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                 NSPredicate(format: "type == \(type)"),
                                 NSPredicate(format: "identifier LIKE %@", identifier)]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                if let cd = try context.fetch(fetchRequest).first {
                    complete?(StoredRequest(userId: cd.userId,
                                            hasSynced: cd.hasSynced,
                                            dateSynced: cd.dateSynced,
                                            date: cd.date,
                                            params: cd.params,
                                            type: cd.type,
                                            identifier: cd.identifier))
                } else {
                    complete?(nil)
                }
            } catch {
                complete?(nil)
            }
        }
    }
    
    static func checkAndSave(model:StoredRequest?,
                             identifier:String,
                             type:Int,
                             userId:Int,
                             complete:COMPLETEDERRORRESULT = nil) {
        guard let sub = model else {
            complete?("Invalid Data")
            return
        }
        
        StoredRequestCD.isExist(identifier:identifier,
                                type:type,
                                userId:userId, { isExist in
            if isExist {
                StoredRequestCD.update(model: sub,
                                       identifier:identifier,
                                       type:type,
                                       userId:userId,
                                       complete)
            } else {
                StoredRequestCD.save(model: sub,
                                     identifier:identifier,
                                     type:type,
                                     userId:userId,
                                     complete: complete)
            }
        })
    }
    
    static func checkAndUpdate(model:StoredRequest?,
                               identifier:String,
                               type:Int,
                               userId:Int,
                       _ complete:COMPLETEDERRORRESULT = nil) {
        
        guard let sub = model else {
            complete?("Invalid Data")
            return
        }
        
        StoredRequestCD.isExist(identifier:identifier,
                                type:type,
                                userId:userId, { isExist in
            if isExist {
                StoredRequestCD.update(model: sub,
                                       identifier:identifier,
                                       type:type,
                                       userId:userId,
                                       complete)
            } else {
                StoredRequestCD.save(model: sub,
                                     identifier:identifier,
                                     type:type,
                                     userId:userId,
                                     complete: complete)
            }
        })
    }
    
    static private func save(model:StoredRequest?,
                             identifier:String,
                             type:Int,
                             userId:Int,
                             complete:COMPLETEDERRORRESULT = nil) {
        
        let container = CoreData.sharedInstance.saveManagedObjectContext
        container.perform {
            if let object = NSEntityDescription.insertNewObject(forEntityName: "StoredRequestCD", into: container) as? StoredRequestCD {
                object.userId = userId
                object.identifier = identifier
                object.type = type
                object.hasSynced = false
                object.date = Date()
                object.params = model?.params
            }
            do {
                try container.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(#function) \(err.localizedDescription) ")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static private func update(model:StoredRequest?,
                               identifier:String,
                               type:Int,
                               userId:Int,
                               _ complete:COMPLETEDERRORRESULT = nil) {
        
        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = StoredRequestCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                 NSPredicate(format: "type == \(type)"),
                                 NSPredicate(format: "identifier LIKE %@", identifier)]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                try context.fetch(fetchRequest).forEach({
                    $0.setValue(model?.params, forKey: "params")
                    $0.setValue(Date(), forKey: "date")
                    $0.setValue(false, forKey: "hasSynced")
                })
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static public func markSynced(identifier:String,
                                   type:Int,
                                   userId:Int,
                               _ complete:COMPLETEDERRORRESULT = nil) {
        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = StoredRequestCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                 NSPredicate(format: "type == \(type)"),
                                 NSPredicate(format: "identifier LIKE %@", identifier)]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                try context.fetch(fetchRequest).forEach({
                    $0.setValue(Date(), forKey: "dateSynced")
                    $0.setValue(true, forKey: "hasSynced")
                })
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static func isExist(identifier:String,
                        type:Int,
                        userId:Int,
                        _ completed:COMPLETEDBOOLRESULT) {
        do {
            let context = CoreData.sharedInstance.managedObjectContext
            context.perform {
                let fetchRequest = StoredRequestCD.fetchRequestD()
                let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                     NSPredicate(format: "type == \(type)"),
                                     NSPredicate(format: "identifier LIKE %@", identifier)]
                let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
                fetchRequest.predicate = finalPredicate
                do {
                    let count  = try context.count(for: fetchRequest)
                    completed?(count > 0)
                } catch  {
                    completed?(false)
                }
            }
        }
    }
    
    static func delete(identifier:String,
                       type:Int,
                       userId:Int,
                       _ complete:COMPLETEDERRORRESULT) {

        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = StoredRequestCD.fetchRequestD()
            let listPredicate = [NSPredicate(format: "userId == \(userId)"),
                                 NSPredicate(format: "type == \(type)"),
                                 NSPredicate(format: "identifier LIKE %@", identifier)]
            let finalPredicate = NSCompoundPredicate(type: .and, subpredicates: listPredicate)
            fetchRequest.predicate = finalPredicate
            do {
                try context.fetch(fetchRequest).forEach({context.delete($0)})
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
    
    static func reset(_ complete:COMPLETEDERRORRESULT) {
        let context = CoreData.sharedInstance.saveManagedObjectContext
        context.perform {
            let fetchRequest = StoredRequestCD.fetchRequestD()
            do {
                try context.fetch(fetchRequest).forEach({context.delete($0)})
                try context.save()
                complete?(nil)
            } catch let err {
                #if DEBUG
                print("\(err.localizedDescription) \(#function)")
                #endif
                complete?(err.localizedDescription)
            }
        }
    }
}
