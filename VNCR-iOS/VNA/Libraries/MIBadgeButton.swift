//
//  MIBadgeButton.swift
//  MIBadgeButton
//
//  Created by Yosemite on 8/27/14.
//  Copyright (c) 2014 Youxel Technology. All rights reserved.
//

import UIKit

open class MIBadgeButton: UIButton {
    
    
    fileprivate var badgeLabel: UILabel
    fileprivate var fontSizeBadge: CGFloat = 12.0
    fileprivate var minWidthBadge = 20.0
    open var badgeString: String? {
        didSet {
            setupBadgeViewWithString(badgeText: badgeString)
        }
    }
    
    open var badgeEdgeInsets: UIEdgeInsets? {
        didSet {
            setupBadgeViewWithString(badgeText: badgeString)
        }
    }
    
    open var badgeBackgroundColor = UIColor.red {
        didSet {
            badgeLabel.backgroundColor = badgeBackgroundColor
        }
    }
    
    open var badgeTextColor = UIColor.white {
        didSet {
            badgeLabel.textColor = badgeTextColor
        }
    }
    
    fileprivate var badgeLabelFooter: UILabel
    open var badgeStringFooter: String? {
        didSet {
            setupBadgeViewWithStringFooter(badgeText: badgeStringFooter)
        }
    }
    
    open var badgeEdgeInsetsFooter: UIEdgeInsets? {
        didSet {
            setupBadgeViewWithStringFooter(badgeText: badgeStringFooter)
        }
    }
    
    open var badgeBackgroundColorFooter = UIColor.red {
        didSet {
            badgeLabelFooter.backgroundColor = badgeBackgroundColorFooter
        }
    }
    
    open var badgeTextColorFooter = UIColor.white {
        didSet {
            badgeLabelFooter.textColor = badgeTextColorFooter
        }
    }

    override public init(frame: CGRect) {
        badgeLabel = UILabel()
        badgeLabelFooter = UILabel()
        super.init(frame: frame)
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSizeBadge = 10
            minWidthBadge = 16
        } else {
            fontSizeBadge = 12
            minWidthBadge = 20
        }
        // Initialization code
        setupBadgeViewWithString(badgeText: "")
        setupBadgeViewWithStringFooter(badgeText: "")
    }
    
    required public init?(coder aDecoder: NSCoder) {
        
        badgeLabel = UILabel()
        badgeLabelFooter = UILabel()
        super.init(coder: aDecoder)
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSizeBadge = 10
            minWidthBadge = 16
        } else {
            fontSizeBadge = 12
            minWidthBadge = 20
        }
        setupBadgeViewWithString(badgeText: "")
        setupBadgeViewWithStringFooter(badgeText: "")

    }
    
    open func initWithFrame(frame: CGRect, withBadgeString badgeString: String, withBadgeInsets badgeInsets: UIEdgeInsets) -> AnyObject {
        
        badgeLabel = UILabel()
        badgeLabelFooter = UILabel()
        badgeEdgeInsets = badgeInsets
        setupBadgeViewWithString(badgeText: badgeString)
        setupBadgeViewWithStringFooter(badgeText: "")
        
        return self
    }
    
    fileprivate func setupBadgeViewWithString(badgeText: String?) {
        badgeLabel.clipsToBounds = true
        badgeLabel.text = badgeText
        badgeLabel.font = UIFont.systemFont(ofSize: fontSizeBadge)
        badgeLabel.textAlignment = .center
        badgeLabel.sizeToFit()
        let badgeSize = badgeLabel.frame.size
        
        let height = max(minWidthBadge, Double(badgeSize.height) + 5.0)
        let width = max(height, Double(badgeSize.width) + 10.0)
        
        var vertical: Double?, horizontal: Double?
        if let badgeInset = self.badgeEdgeInsets {
            vertical = Double(badgeInset.top) - Double(badgeInset.bottom)
            horizontal = Double(badgeInset.left) - Double(badgeInset.right)
            
            let x = (Double(bounds.size.width) - 10 + horizontal!)
            let y = -(Double(badgeSize.height) / 2) - 10 + vertical!
            badgeLabel.frame = CGRect(x: x, y: y, width: width, height: height)
        } else {
            let x = self.frame.width - CGFloat((width / 2.0))
            let y = CGFloat(-(height / 2.0))
            badgeLabel.frame = CGRect(x: x, y: y, width: CGFloat(width), height: CGFloat(height))
        }
        
        setupBadgeStyle()
        addSubview(badgeLabel)
        
        if let text = badgeText {
            badgeLabel.isHidden = text != "" ? false : true
        } else {
            badgeLabel.isHidden = true
        }
        
    }
    
    fileprivate func setupBadgeStyle() {
        badgeLabel.textAlignment = .center
        badgeLabel.backgroundColor = badgeBackgroundColor
        badgeLabel.textColor = badgeTextColor
        badgeLabel.layer.cornerRadius = badgeLabel.bounds.size.height / 2
    }
    
    fileprivate func setupBadgeViewWithStringFooter(badgeText: String?) {
        badgeLabelFooter.clipsToBounds = true
        badgeLabelFooter.text = badgeText
        badgeLabelFooter.font = UIFont.systemFont(ofSize: fontSizeBadge)
        badgeLabelFooter.textAlignment = .center
        badgeLabelFooter.sizeToFit()
        let badgeSize = badgeLabelFooter.frame.size
        
        let height = max(minWidthBadge, Double(badgeSize.height) + 5.0)
        let width = max(height, Double(badgeSize.width) + 10.0)
        
        var vertical: Double?, horizontal: Double?
        if let badgeInset = self.badgeEdgeInsetsFooter {
            vertical = Double(badgeInset.top) - Double(badgeInset.bottom)
            horizontal = Double(badgeInset.left) - Double(badgeInset.right)
            
            let x = (Double(bounds.size.width) - 10 + horizontal!)
            let y = (Double(badgeSize.height) / 2) + 10 + vertical!
            badgeLabelFooter.frame = CGRect(x: x, y: y, width: width, height: height)
        } else {
            let x = self.frame.width - CGFloat((width / 2.0))
            let y = CGFloat((height / 2.0)) + 10
            badgeLabelFooter.frame = CGRect(x: x, y: y, width: CGFloat(width), height: CGFloat(height))
        }
        
        setupBadgeStyleFooter()
        addSubview(badgeLabelFooter)
        
        if let text = badgeText {
            badgeLabelFooter.isHidden = text != "" ? false : true
        } else {
            badgeLabelFooter.isHidden = true
        }
        
    }
    
    fileprivate func setupBadgeStyleFooter() {
        badgeLabelFooter.textAlignment = .center
        badgeLabelFooter.backgroundColor = badgeBackgroundColorFooter
        badgeLabelFooter.textColor = badgeTextColorFooter
        badgeLabelFooter.layer.cornerRadius = badgeLabelFooter.bounds.size.height / 2
    }
}
