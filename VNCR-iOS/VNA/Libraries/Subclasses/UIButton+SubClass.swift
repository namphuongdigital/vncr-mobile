//
//  UITextView+SubClass.swift
//  VNA
//
//  Created by Dai Pham on 24/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class ButtonHalfHeight: UIButton {

    var object:Any? // need keep object to handle in extension using this button class
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        if #available(iOS 10.0, *) {
            titleLabel?.adjustsFontForContentSizeCategory = true
        }
        layer.cornerRadius = frame.size.height/2
        layer.masksToBounds = true
    }
}

class Button10Corner: UIButton {

    var object:Any? // need keep object to handle in extension using this button class
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        if #available(iOS 10.0, *) {
            titleLabel?.adjustsFontForContentSizeCategory = true
        }
        layer.cornerRadius = 10
        layer.masksToBounds = true
    }
}

class Button5Corner: UIButton {

    var object:Any? // need keep object to handle in extension using this button class
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        if #available(iOS 10.0, *) {
            titleLabel?.adjustsFontForContentSizeCategory = true
        }
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
}
