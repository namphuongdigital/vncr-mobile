//
//  UILabel+SubClass.swift
//  VNA
//
//  Created by Dai Pham on 24/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class RoundBorderLabel:RoundLabel {
    
    var borderColor:UIColor? = .gray {
        didSet {
            setNeedsLayout()
        }
    }
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        textAlignment = .center
        layer.borderWidth = 1
        layer.borderColor = borderColor?.cgColor
    }
}

class RoundLabel:UILabel {
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textAlignment = .center
        layer.cornerRadius = frame.size.width/2
        layer.masksToBounds = true
    }
}
