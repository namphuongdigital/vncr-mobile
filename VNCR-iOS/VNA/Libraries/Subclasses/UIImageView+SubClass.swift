//
//  Extensions.swift
//  VNA
//
//  Created by Dai Pham on 23/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class RoundImageView: UIImageViewProgress {
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = frame.size.height/2
        layer.masksToBounds = true
    }
}

class ImageView10Corner: UIImageViewProgress {
    
    var onPress:((UIImageView)->Void)?
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.isMultipleTouchEnabled = true
        layer.cornerRadius = 10
        layer.masksToBounds = true
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        onPress?(self)
        super.touchesEnded(touches, with: event)
    }
}

class ImageView5Corner: UIImageViewProgress {
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
}
