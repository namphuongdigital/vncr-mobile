//
//  Extensions.swift
//  VNA
//
//  Created by Dai Pham on 23/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class Corner10ShadowViewPath: Corner10View {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if #available(iOS 12.0, *) {
            if UIApplication.shared.keyWindow?.visibleViewController?.traitCollection.userInterfaceStyle == .dark {return}
        }
        
        let shadowSize:CGFloat = 3
        
        let shadowPath = UIBezierPath(roundedRect: CGRect(x: -shadowSize,
                                                          y: -shadowSize,
                                                          width: frame.size.width + shadowSize,
                                                          height: frame.size.height + shadowSize), cornerRadius: 10)
        layer.masksToBounds = false // if true effect shadowpath is lost
        
        layer.shadowColor = #colorLiteral(red: 0.8, green: 0.8352941176, blue: 0.8823529412, alpha: 1).cgColor
        layer.shadowOffset = CGSize(width: 0, height: shadowSize)
        layer.shadowOpacity = 0.5
        layer.shadowPath = shadowPath.cgPath
//        layer.shadowRadius = 15
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
//        layer.cornerRadius = 10
    }
}

class Corner5ShadowViewPath: Corner10View {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if #available(iOS 12.0, *) {
            if UIApplication.shared.keyWindow?.visibleViewController?.traitCollection.userInterfaceStyle == .dark {return}
        }
        
        let shadowSize:CGFloat = 3
        
        let shadowPath = UIBezierPath(roundedRect: CGRect(x: -shadowSize,
                                                          y: -shadowSize,
                                                          width: frame.size.width + shadowSize,
                                                          height: frame.size.height + shadowSize), cornerRadius: 5)
        layer.masksToBounds = false // if true effect shadowpath is lost
        layer.shadowColor = #colorLiteral(red: 0.8, green: 0.8352941176, blue: 0.8823529412, alpha: 1).cgColor
        layer.shadowOffset = CGSize(width: 0, height: shadowSize)
        layer.shadowOpacity = 0.5
        layer.shadowPath = shadowPath.cgPath
//        layer.shadowRadius = 15
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
//        layer.cornerRadius = 10
    }
}

class Corner10View: UIView {
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 10
        layer.masksToBounds = true
    }
}

class Corner5View: UIView {
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
}

class Corner10TopView: UIView {
    override func layoutSubviews() {
        super.layoutSubviews()
        roundCornersFull(corners: [.topLeft,.topRight], radius: 10)
    }
}

class TopLineView: UIView {
    
    var lineColor: UIColor = UIColor.systemGray.withAlphaComponent(0.5) {
        didSet {
            setNeedsLayout()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        _ = layer.sublayers?.filter({$0.isKind(of: CAShapeLayer.self)}).map({$0.removeFromSuperlayer()})
        
        let startingPoint   = CGPoint(x: bounds.minX, y: 0)
        let endingPoint     = CGPoint(x: bounds.maxX, y: 0)
        
        let path = UIBezierPath()
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 0.5
        let subLayer = CAShapeLayer()
        subLayer.path = path.cgPath
        subLayer.strokeColor = lineColor.cgColor
        subLayer.frame = bounds
        layer.insertSublayer(subLayer, at: 0)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        _ = layer.sublayers?.filter({$0.isKind(of: CAShapeLayer.self)}).map({$0.removeFromSuperlayer()})
        
        let startingPoint   = CGPoint(x: rect.minX, y: 0)
        let endingPoint     = CGPoint(x: rect.maxX, y: 0)
        
        let path = UIBezierPath()
        path.move(to: startingPoint)
        path.addLine(to: endingPoint)
        path.lineWidth = 0.5
        let subLayer = CAShapeLayer()
        subLayer.path = path.cgPath
        subLayer.strokeColor = lineColor.cgColor
        subLayer.frame = rect
        layer.insertSublayer(subLayer, at: 0)
    }
}

class RoundCornerView: UIView {
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = self.frame.height/2
        layer.masksToBounds = true
    }
}

class Corner5BorderView: UIView {
    
    var borderColor:UIColor = .clear {
        didSet {
            setNeedsLayout()
        }
    }
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = 5
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = borderColor.cgColor
    }
}

class GradientTwoColorView: UIView {
    
    @IBInspectable var startColor:   UIColor = UIColor.white.withAlphaComponent(0) { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = UIColor.white.withAlphaComponent(1) { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}
    @IBInspectable var isRound:    Bool =  false { didSet { updateCorner() }}
    
    override class var layerClass: AnyClass { return CAGradientLayer.self }
    
    var gradientLayer: CAGradientLayer { return layer as! CAGradientLayer }
    
    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    
    func updateColors() {
        gradientLayer.colors    = [startColor.cgColor, endColor.cgColor]
    }
    
    func updateCorner() {
        gradientLayer.cornerRadius = self.frame.height/2
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updatePoints()
        updateLocations()
        updateColors()
    }
}

class PopoverCoverView: UIView {
    
    var fillColor:UIColor = .white {
        didSet {
            setNeedsLayout()
            setNeedsDisplay()
        }
    }
    
    var cornerRadius:CGFloat = 10 {
        didSet {
            setNeedsLayout()
            setNeedsDisplay()
        }
    }
    
    var isUp:Bool = false {
        didSet {
            setNeedsLayout()
            setNeedsDisplay()
        }
    }
    
    var arrowRect:CGRect = .zero {
        didSet {
            setNeedsLayout()
            setNeedsDisplay()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setDefaultMargin()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDefaultMargin()
    }
    
    func setDefaultMargin() {
        // subviews constraints should check relativeMargins
        if #available(iOS 11.0, *) {
            self.directionalLayoutMargins = NSDirectionalEdgeInsets(top: arrowRect.height, leading: 0, bottom: arrowRect.height, trailing: 0)
        } else {
            self.layoutMargins = UIEdgeInsets(top: arrowRect.height, left: 0, bottom: arrowRect.height, right: 0)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setDefaultMargin()
    }
    
    override func draw(_ rect: CGRect) {
        guard let context = UIGraphicsGetCurrentContext(), arrowRect != .zero else { return }
        let shadowHeight:CGFloat = 0
        context.saveGState()
//        context.setShadow(offset: CGSize(width: -shadowHeight/2, height: shadowHeight), blur: 5)

        var arrowRect = self.arrowRect
        if arrowRect.minX < rect.minX + cornerRadius {
            arrowRect.origin.x = rect.minX + cornerRadius
        } else if arrowRect.maxX > rect.maxX - cornerRadius {
            arrowRect.origin.x = rect.maxX - cornerRadius - arrowRect.width
        }
        
        let minX = rect.minX + shadowHeight
        let minY = rect.minY + arrowRect.height
        let maxX = rect.maxX - shadowHeight
        let maxY = rect.maxY - arrowRect.height

        if isUp {
            
            context.beginPath()
            context.move(to: CGPoint(x: minX + cornerRadius, y: minY))
            
            // draw anchor point
            context.addLine(to: CGPoint(x: arrowRect.minX, y: minY))
            context.addLine(to: CGPoint(x: arrowRect.minX + arrowRect.width/2, y: minY - arrowRect.height + shadowHeight))
            context.addLine(to: CGPoint(x: arrowRect.minX + arrowRect.width, y: minY))
            
            context.addLine(to: CGPoint(x: maxX - cornerRadius, y: minY))
            
            // add corner top right
            context.addArc(center: CGPoint(x: maxX - cornerRadius, y: minY + cornerRadius), radius: cornerRadius, startAngle: 270 * CGFloat.pi/180, endAngle: 0 * CGFloat.pi/180, clockwise: false)
            
            context.addLine(to: CGPoint(x: maxX, y: maxY - cornerRadius))
            
            // add corner bottom right
            context.addArc(center: CGPoint(x: maxX - cornerRadius, y: maxY - cornerRadius), radius: cornerRadius, startAngle: 0 * CGFloat.pi/180, endAngle: 90 * CGFloat.pi/180, clockwise: false)
            
            context.addLine(to: CGPoint(x: minX + cornerRadius, y: maxY))
            
            // add corner bottom left
            context.addArc(center: CGPoint(x: minX + cornerRadius, y: maxY - cornerRadius), radius: cornerRadius, startAngle: 90 * CGFloat.pi/180, endAngle: 180 * CGFloat.pi/180, clockwise: false)
            
            context.addLine(to: CGPoint(x: minX, y: minY + cornerRadius))
            
            // add corner top left
            context.addArc(center: CGPoint(x: minX + cornerRadius, y: minY + cornerRadius), radius: cornerRadius, startAngle: 180 * CGFloat.pi/180, endAngle: 270 * CGFloat.pi/180, clockwise: false)
            
            context.closePath()
        } else {
            
            context.beginPath()
            context.move(to: CGPoint(x: minX + cornerRadius, y: minY))
            context.addLine(to: CGPoint(x: maxX - cornerRadius, y: minY))
            
            // add corner top right
            context.addArc(center: CGPoint(x: maxX - cornerRadius, y: minY + cornerRadius), radius: cornerRadius, startAngle: 270 * CGFloat.pi/180, endAngle: 0 * CGFloat.pi/180, clockwise: false)
            
            context.addLine(to: CGPoint(x: maxX, y: maxY - cornerRadius))
            
            // add corner bottom right
            context.addArc(center: CGPoint(x: maxX - cornerRadius, y: maxY - cornerRadius), radius: cornerRadius, startAngle: 0 * CGFloat.pi/180, endAngle: 90 * CGFloat.pi/180, clockwise: false)
            
            context.addLine(to: CGPoint(x: minX + cornerRadius, y: maxY))
            
            // draw anchor point
            context.addLine(to: CGPoint(x: arrowRect.maxX, y: maxY))
            context.addLine(to: CGPoint(x: arrowRect.maxX - arrowRect.width/2, y: maxY + arrowRect.height - shadowHeight))
            context.addLine(to: CGPoint(x: arrowRect.maxX - arrowRect.width, y: maxY))
            
            // add corner bottom left
            context.addArc(center: CGPoint(x: minX + cornerRadius, y: maxY - cornerRadius), radius: cornerRadius, startAngle: 90 * CGFloat.pi/180, endAngle: 180 * CGFloat.pi/180, clockwise: false)
            
            context.addLine(to: CGPoint(x: minX, y: minY + cornerRadius))
            
            // add corner top left
            context.addArc(center: CGPoint(x: minX + cornerRadius, y: minY + cornerRadius), radius: cornerRadius, startAngle: 180 * CGFloat.pi/180, endAngle: 270 * CGFloat.pi/180, clockwise: false)
            
            context.closePath()
        }
        
        context.setFillColor(fillColor.cgColor)
        context.fillPath()
        context.restoreGState()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setDefaultMargin()
        layer.masksToBounds = true
        backgroundColor = .clear
    }
}
