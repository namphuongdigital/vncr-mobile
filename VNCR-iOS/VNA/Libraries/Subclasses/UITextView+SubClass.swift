//
//  UITextView+SubClass.swift
//  VNA
//
//  Created by Dai Pham on 24/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class UITextView10Corner: UITextView {
    var borderColor:UIColor = .gray {
        didSet {
            setNeedsLayout()
        }
    }
    
    var onTextViewDidEndEditing:((_ textView: UITextView)->Void)?
    var onTextViewShouldBeginEditing:((_ textView: UITextView)->Bool)?
    var onTextViewDidChange:((_ textView: UITextView)->Void)?
    var onTextViewDidBeginEditing:((_ textView: UITextView)->Void)?
    
    // MARK: -  override
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 10
        layer.masksToBounds = true
        layer.borderWidth = 1
        layer.borderColor = borderColor.cgColor
    }
    
    override func textViewDidChange(_ textView: UITextView) {
        super.textViewDidChange(textView)
        onTextViewDidChange?(textView)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        onTextViewDidBeginEditing?(textView)
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        return onTextViewShouldBeginEditing?(textView) ?? true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        onTextViewDidEndEditing?(textView)
    }
}
