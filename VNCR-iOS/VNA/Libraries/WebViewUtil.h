//
//  WebViewUtil.h
//  gdrMobile
//
//  Created by Dung Hoang on 4/26/17.
//  Copyright © 2017 Dung Nguyen. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <CoreGraphics/CoreGraphics.h>
#include <UIKit/UIKit.h>

@interface WebViewUtil : NSObject
+(NSString*)getHtmlData:(NSString*)bodyHTML;
+ (NSString*)getHtmlData:(NSString*)bodyHTML color:(NSString*) color;
+(NSString*)getHtmlData:(NSString*)bodyHTML color:(NSString *)color bgColor:(NSString *)bgColor;
+ (void)readPDF:(NSURL *)sourcePDFUrl startRequest:(void (^) (void)) startRequest images:(void (^) (NSArray*)) images;
+(UIImage *)changeWhiteColorTransparent: (UIImage *)image;
@end
