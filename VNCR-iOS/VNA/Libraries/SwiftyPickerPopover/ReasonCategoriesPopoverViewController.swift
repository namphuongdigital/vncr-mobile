//
//  ReasonCategoriesPopoverViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/15/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class ReasonCategoriesPopoverViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var doneAction: (([CategoryModel])->Void)?
    
    var cancleAction: (()->Void)?
    
    var listCategory: [CategoryModel]!

    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //loadCalendar()
        //calendar.select(selectedDate, scrollToDate: true)
    }
    
    func loadTableView() {
        
    }
    
    
    @IBAction func tappedDone(_ sender: UIButton? = nil) {
        doneAction?(listCategory)
        dismiss(animated: true, completion: {})
    }
    
    @IBAction func tappedCancel(_ sender: UIButton? = nil) {
        cancleAction?()
        dismiss(animated: true, completion: {})
    }
    
    
    
    /// popover dismissed
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        tappedCancel()
    }
    
    /// Popover appears on iPhone
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return listCategory.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCategory[section].subCatetories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryTableViewCell")! as! SubCategoryTableViewCell
        let subCategory = self.listCategory[indexPath.section].subCatetories[indexPath.row]
        if(subCategory.isSelected) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.labelName.text = subCategory.subCategoryName
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let subCategory = self.listCategory[indexPath.section].subCatetories[indexPath.row]
        if(subCategory.isSelected) {
            subCategory.isSelected = false
        } else {
            subCategory.isSelected = true
        }
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30))
        let label = UILabel(frame: CGRect(x: 20, y: 5, width: UIScreen.main.bounds.size.width, height: 20))
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.black
        let category = listCategory[section]
        label.text = category.categoryName
        view.addSubview(label)
        
        return view
    }
    

}
