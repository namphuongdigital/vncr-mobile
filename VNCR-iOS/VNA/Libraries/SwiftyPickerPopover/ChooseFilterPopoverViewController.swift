//
//  ChooseFilterPopoverViewController.swift
//  ReportForBoss
//
//  Created by Van Trieu Phu Huy on 3/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class ChooseFilterPopoverViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    var isMultiSelected: Bool = true
    
    @IBOutlet weak var tableView: UITableView!
    
    var doneAction: (([Array<NSObject>])->Void)?
    
    var cancleAction: (()->Void)?
    
    var listSelectedIndexPath = Array<IndexPath>()
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.allowsMultipleSelection = isMultiSelected
        
        let list = ServiceData.sharedInstance.listFilterSelected[0]
        for index in 0..<ServiceData.sharedInstance.listFilterModel[0].count {
            let filter = ServiceData.sharedInstance.listFilterModel[0][index]
            var isSelected: Bool = false
            for i in 0..<list.count {
                if let filterSelected = list[i] as? FilterModel {
                    if(filter.id == filterSelected.id) {
                        isSelected = true
                        break
                    }
                }
                
                
            }
            filter.isSelected = isSelected
            
        }
        
        let list2 = ServiceData.sharedInstance.listFilterSelected[1]
        for index in 0..<ServiceData.sharedInstance.listFilterModel[1].count {
            let filter = ServiceData.sharedInstance.listFilterModel[1][index]
            var isSelected: Bool = false
            for i in 0..<list2.count {
                if let filterSelected = list2[i] as? FilterModel {
                    if(filter.id == filterSelected.id) {
                        isSelected = true
                        break
                    }
                }
                
                
            }
            filter.isSelected = isSelected
            
        }
        
    }
    
    func loadTableView() {
        
    }
    
    
    @IBAction func tappedDone(_ sender: UIButton? = nil) {
        
        var list = Array<FilterModel>()
        for index in 0..<ServiceData.sharedInstance.listFilterModel[0].count {
            if(ServiceData.sharedInstance.listFilterModel[0][index].isSelected) {
                list.append(ServiceData.sharedInstance.listFilterModel[0][index])
            }
            
        }
        
        var list2 = Array<FilterModel>()
        for index in 0..<ServiceData.sharedInstance.listFilterModel[1].count {
            if(ServiceData.sharedInstance.listFilterModel[1][index].isSelected) {
                list2.append(ServiceData.sharedInstance.listFilterModel[1][index])
            }
            
        }
   
        doneAction?([list, list2])
 
        dismiss(animated: true, completion: {})
        
    }
    
    @IBAction func tappedCancel(_ sender: UIButton? = nil) {
        cancleAction?()
        dismiss(animated: true, completion: {})
    }
    
    
    
    /// popover dismissed
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        tappedCancel()
    }
    
    /// Popover appears on iPhone
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) {
            return ServiceData.sharedInstance.listFilterModel[0].count
        } else {
            return ServiceData.sharedInstance.listFilterModel[1].count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryTableViewCell")! as! SubCategoryTableViewCell
        if(indexPath.section == 0) {
            let filter = ServiceData.sharedInstance.listFilterModel[0][indexPath.row]
            if(filter.isSelected) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
            cell.labelName.text = filter.filterName
        } else {
            let filter = ServiceData.sharedInstance.listFilterModel[1][indexPath.row]
            if(filter.isSelected) {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
            cell.labelName.text = filter.filterName
        }
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0) {
            let filter = ServiceData.sharedInstance.listFilterModel[0][indexPath.row]
            for index in 0..<ServiceData.sharedInstance.listFilterModel[0].count {
                ServiceData.sharedInstance.listFilterModel[0][index].isSelected = false
            }
            if(filter.isSelected) {
                filter.isSelected = false
            } else {
                filter.isSelected = true
            }
            
        } else {
            let filter = ServiceData.sharedInstance.listFilterModel[1][indexPath.row]
            if(filter.id == 0) {
                for index in 0..<ServiceData.sharedInstance.listFilterModel[1].count {
                    ServiceData.sharedInstance.listFilterModel[1][index].isSelected = false
                }
            } else {
                ServiceData.sharedInstance.listFilterModel[1][0].isSelected = false
            }
            
            if(filter.isSelected) {
                filter.isSelected = false
            } else {
                filter.isSelected = true
            }
        }
        self.tableView.reloadSections(IndexSet([indexPath.section]), with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.01
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 320, height: 40))
        let labelTitle = UILabel(frame: CGRect(x: 10, y: 20, width: 320, height: 20))
        labelTitle.font = UIFont.systemFont(ofSize: 14.0)
        labelTitle.textColor = UIColor.lightGray
        if(section == 0) {
            labelTitle.text = ""
        } else {
            labelTitle.text = ""
        }
        view.addSubview(labelTitle)
        return view
    }
    

}
