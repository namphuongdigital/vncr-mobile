//
//  CommonCategoriesPopoverViewController.swift
//  
//
//  Created by Nhan Ba Doan on 11/15/17.
//  Copyright © 2017 Nhan Ba Doan. All rights reserved.
//

import UIKit

class CommonCategoriesPopoverViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnLeft: UIBarButtonItem!
    @IBOutlet weak var btnRight: UIBarButtonItem!
    
    var doneAction: (([CommonPopoverModel])->Void)?
    
    var cancleAction: (()->Void)?
    
    var listCategory: [CommonPopoverModel]!
    
    var isMultiSelected: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.allowsMultipleSelection = isMultiSelected
        btnLeft.title = "Cancel".localizedString()
        btnRight.title = "Done".localizedString()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
    }
    
    func loadTableView() {
    }
    
    @IBAction func tappedDone(_ sender: UIButton? = nil) {
        doneAction?(listCategory)
        dismiss(animated: true, completion: {})
    }
    
    @IBAction func tappedCancel(_ sender: UIButton? = nil) {
        cancleAction?()
        dismiss(animated: true, completion: {})
    }
    
    /// popover dismissed
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        tappedCancel()
    }
    
    /// Popover appears on iPhone
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
    
    //MARK: UITableViewDataSource methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listCategory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubCategoryTableViewCell")! as! SubCategoryTableViewCell
        
        let category = self.listCategory[indexPath.row]
        if(category.IsSelected) {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }
        cell.labelName.text = category.Name.isEmpty ? category.Code : category.Name

        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var indexPathsForSelectedRows = [IndexPath]()
        if isMultiSelected {
            
        } else {
            for index in 0..<self.listCategory.count {
                if(self.listCategory[index].IsSelected) {
                    self.listCategory[index].IsSelected = false
                    indexPathsForSelectedRows.append(IndexPath.init(row: index, section: 0))
                }
            }
        }
        let category = self.listCategory[indexPath.row]
        if(category.IsSelected) {
            category.IsSelected = false
        } else {
            category.IsSelected = true
        }
        indexPathsForSelectedRows.append(indexPath)
        self.tableView.reloadRows(at: indexPathsForSelectedRows, with: .automatic)
        if self.isMultiSelected {
            
        } else {
            doneAction?(listCategory)
            dismiss(animated: true, completion: {})
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: 30))
        let label = UILabel(frame: CGRect(x: 20, y: 5, width: UIScreen.main.bounds.size.width, height: 20))
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor = UIColor.black
        label.text = ""
        view.addSubview(label)
        
        return view
    }

}
