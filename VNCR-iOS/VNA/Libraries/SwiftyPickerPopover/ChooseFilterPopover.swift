//
//  ChooseFilterPopover.swift
//  ReportForBoss
//
//  Created by Van Trieu Phu Huy on 3/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class ChooseFilterPopover: AbstractPopover {

    
    // singleton
    class var sharedInstance : ChooseFilterPopover {
        struct Static {
            static let instance : ChooseFilterPopover = ChooseFilterPopover()
        }
        return Static.instance
    }
    
    
    /// Popover appears
    /// - parameter view: origin view of popover
    /// - parameter baseView: popoverPresentationController's sourceView
    /// - parameter baseViewController: viewController to become the base
    /// - parameter title: title for navigation bar
    /// - parameter dateMode: UIDatePickerMode
    /// - parameter initialDate: initial selected date
    /// - parameter doneAction: action in which user tappend done button
    /// - parameter cancelAction: action in which user tappend cancel button
    /// - parameter clearAction: action in which user tappend clear action. Omissible.
    public class func appearFrom(originView: UIView, baseView: UIView? = nil, baseViewController: UIViewController, title: String?, arrayData:[NSObject]?, isMultiSelected: Bool = true, doneAction: (([Array<NSObject>])->Void)?, cancelAction: (()->Void)?, clearAction: (()->Void)? = nil){
        
        // create navigationController
        guard let navigationController = sharedInstance.configureNavigationController(originView, baseView: baseView, baseViewController: baseViewController, title: title) else {
            return
        }
        
        // StringPickerPopoverViewController
        if let contentViewController = navigationController.topViewController as? ChooseFilterPopoverViewController {
            contentViewController.isMultiSelected = isMultiSelected
            
            contentViewController.doneAction = doneAction
            contentViewController.cancleAction = cancelAction
            
            navigationController.popoverPresentationController?.delegate = contentViewController
        }
        
        // presnet popover
        baseViewController.present(navigationController, animated: true, completion: nil)
        navigationController.popoverPresentationController?.passthroughViews = []
    }
    

    
    public class func appearFrom(barButton: UIBarButtonItem, baseView: UIView? = nil, baseViewController: UIViewController, title: String?, arrayData:[NSObject]?, isMultiSelected: Bool = true, doneAction: (([Array<NSObject>])->Void)?, cancelAction: (()->Void)?, clearAction: (()->Void)? = nil){
        
        // create navigationController
        guard let navigationController = sharedInstance.configureNavigationController(barButtonItem: barButton, baseView: baseView, baseViewController: baseViewController, title: title) else {
            return
        }
        
        // StringPickerPopoverViewController
        if let contentViewController = navigationController.topViewController as? ChooseFilterPopoverViewController {
            contentViewController.isMultiSelected = isMultiSelected
            
            contentViewController.doneAction = doneAction
            contentViewController.cancleAction = cancelAction
            
            navigationController.popoverPresentationController?.delegate = contentViewController
            
        }
        
        // presnet popover
        baseViewController.present(navigationController, animated: true, completion: nil)
        navigationController.popoverPresentationController?.passthroughViews = []
    }
    
    /// storyboardName
    override func storyboardName()->String{
        return "ChooseFilterPopover"
    }
    
}
