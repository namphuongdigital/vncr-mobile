//
//  NetworkBackgroundkManager.swift
//  iOS Example
//
//  Created by Van Trieu Phu Huy on 2/23/17.
//  Copyright © 2017 Alamofire. All rights reserved.
//

import UIKit
import Alamofire

class NetworkBackgroundkManager: NSObject {
    
    class var sharedManager: NetworkBackgroundkManager {
        struct Static {
            static let instance: NetworkBackgroundkManager = NetworkBackgroundkManager()
        }
        return Static.instance
    }
    
    public lazy var backgroundManager: Alamofire.SessionManager = {
        let bundleIdentifier = "vn.com.doantiepvien.VNACrew"
        return Alamofire.SessionManager(configuration: URLSessionConfiguration.background(withIdentifier: bundleIdentifier + ".background"))
    }()
    
    
    var backgroundCompletionHandler: (() -> Void)? {
        get {
            return backgroundManager.backgroundCompletionHandler
        }
        set {
            backgroundManager.backgroundCompletionHandler = newValue
        }
    }

}
