//
//  WebViewUtil.m
//  gdrMobile
//
//  Created by Dung Hoang on 4/26/17.
//  Copyright © 2017 Dung Nguyen. All rights reserved.
//

#import "WebViewUtil.h"

@implementation WebViewUtil
+(NSString*)getHtmlData:(NSString*)html {
    //    NSString *title =[NSString stringWithFormat:@"<div style=\"font-size:22px\"><strong>%@</strong></div>",_detailEduBookItem.title];
    //    NSString *authors =[NSString stringWithFormat:@"%@<br/><br/>",_detailEduBookItem.authors];
    @autoreleasepool {
        NSString* head = [NSString stringWithFormat:@"<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1 maximum-scale=1.0, user-scalable=no \"><style>body, p, h1{overflow: hidden; text-overflow: ellipsis;}a {"
        "white-space: pre;           /* CSS 2.0 */"
        "white-space: pre-wrap;      /* CSS 2.1 */"
        "white-space: pre-line;      /* CSS 3.0 */"
        "white-space: -pre-wrap;     /* Opera 4-6 */"
        "white-space: -o-pre-wrap;   /* Opera 7 */"
        "white-space: -moz-pre-wrap; /* Mozilla */"
        "white-space: -hp-pre-wrap;  /* HP Printers */"
        "word-wrap: break-word;      /* IE 5+ */"
        "}body, p, h1, h2, h3, span, header, div, img, figure {line-height: 27px;} body{max-width: 100%% !important;}</style></head>"];
        return [NSString stringWithFormat:@"%@",html];
    }
}

+(NSString*)getHtmlData:(NSString*)bodyHTML color:(NSString *)color {
    //    NSString *title =[NSString stringWithFormat:@"<div style=\"font-size:22px\"><strong>%@</strong></div>",_detailEduBookItem.title];
    //    NSString *authors =[NSString stringWithFormat:@"%@<br/><br/>",_detailEduBookItem.authors];
    @autoreleasepool {
        NSString* head = [NSString stringWithFormat:@"<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1 maximum-scale=1.0, user-scalable=no \"><style>img,iframe,video{max-width: 100%% !important; width:100%% !important; height: auto !important;}body, p, h1{font-family:'Helvetica Neue' !important;;margin-left: 0,margin-right: 0; padding-left: 0; padding-right: 0; overflow: scroll; text-overflow: ellipsis;font-size:16px;color:%@ !important;}a {"
        "white-space: pre;           /* CSS 2.0 */"
        "white-space: pre-wrap;      /* CSS 2.1 */"
        "white-space: pre-line;      /* CSS 3.0 */"
        "white-space: -pre-wrap;     /* Opera 4-6 */"
        "white-space: -o-pre-wrap;   /* Opera 7 */"
        "white-space: -moz-pre-wrap; /* Mozilla */"
        "white-space: -hp-pre-wrap;  /* HP Printers */"
        "word-wrap: break-word;      /* IE 5+ */"
        "}body, p, h1, h2, h3, span, header, div, img, figure {background-color: transparent !important;line-height: 27px;} td{border:solid %@ 0.5pt;}</style></head>",color,color];
        return [NSString stringWithFormat:@"<!DOCTYPE html><html>%@<body>%@</body>"
                "<script>"
                "var x = document.getElementsByTagName('img');"
                "for (i = 0; i < x.length; i++) {"
                "x[i].setAttribute(\"style\",\"max-width: 100%% !important; width:100%% !important; height: 1px !important;\");"
                "x[i].addEventListener('click', function(j){"
                "document.location.href = this.getAttribute('src');"
                "});"
                "x[i].addEventListener('load', function(j){"
                "this.setAttribute(\"style\",\"max-width: 100%% !important; width:100%% !important; height: auto !important;\");"
                "document.location.href = 'http://gdr_forced_update.com';"
                "});"
                "}"
                
                //            "var h = document.getElementsByTagName('video');"
                //
                //            "for (i = 0; i < h.length; i++) {"
                //                "var div= document.createElement('div');"
                //            "div.innerHTML= '<a href=blob:'+ h[i].getAttribute('src') +'>Video format not supported. Click to play.</a>';"
                //                "h[i].parentNode.replaceChild(div, h[i]);"
                //            "}"
                
                "</script>"
                "</html>",head,[WebViewUtil strimStringFrom:bodyHTML withPatterns:@[/*@"background-color+:+(.*?);", @"color+:+(.*?);", */@"border(.*?);"]]];
    }
}

+(NSString*)getHtmlData:(NSString*)bodyHTML color:(NSString *)color bgColor:(NSString *)bgColor {
    //    NSString *title =[NSString stringWithFormat:@"<div style=\"font-size:22px\"><strong>%@</strong></div>",_detailEduBookItem.title];
    //    NSString *authors =[NSString stringWithFormat:@"%@<br/><br/>",_detailEduBookItem.authors];
    @autoreleasepool {
        NSString* head = [NSString stringWithFormat:@"<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1 maximum-scale=1.0, user-scalable=no \"><style>img,iframe,video{max-width: 100%% !important; width:100%% !important; height: auto !important;}body, p, h1{font-family:'Helvetica Neue' !important;margin-left: 0,margin-right: 0; padding-left: 0; padding-right: 0; overflow: hidden; text-overflow: ellipsis;font-size:16px;}a {"
        "white-space: pre;           /* CSS 2.0 */"
        "white-space: pre-wrap;      /* CSS 2.1 */"
        "white-space: pre-line;      /* CSS 3.0 */"
        "white-space: -pre-wrap;     /* Opera 4-6 */"
        "white-space: -o-pre-wrap;   /* Opera 7 */"
        "white-space: -moz-pre-wrap; /* Mozilla */"
        "white-space: -hp-pre-wrap;  /* HP Printers */"
        "word-wrap: break-word;      /* IE 5+ */"
        "}body, p, h1, h2, h3,header ,span{line-height: 27px !important;} body{background-color: %@ !important;} td{border:solid %@ 0.5pt;} </style></head>",bgColor,color];
        return [NSString stringWithFormat:@"<!DOCTYPE html><html>%@<body>%@</body>"
                "<script>"
                "var x = document.getElementsByTagName('img');"
                "for (i = 0; i < x.length; i++) {"
                "x[i].setAttribute(\"style\",\"max-width: 100%% !important; width:100%% !important; height: 1px !important;\");"
                "x[i].addEventListener('click', function(j){"
                "document.location.href = this.getAttribute('src');"
                "});"
                "x[i].addEventListener('load', function(j){"
                "this.setAttribute(\"style\",\"max-width: 100%% !important; width:100%% !important; height: auto !important;\");"
                "document.location.href = 'http://gdr_forced_update.com';"
                "});"
                "}"
                
                //            "var h = document.getElementsByTagName('video');"
                //
                //            "for (i = 0; i < h.length; i++) {"
                //                "var div= document.createElement('div');"
                //            "div.innerHTML= '<a href=blob:'+ h[i].getAttribute('src') +'>Video format not supported. Click to play.</a>';"
                //                "h[i].parentNode.replaceChild(div, h[i]);"
                //            "}"
                
                "</script>"
                "</html>",head,[WebViewUtil strimStringFrom:bodyHTML withPatterns:@[/*@"background-color+:+(.*?);", @"color+:+(.*?);", */@"border(.*?);"]]];
    }
}

+ (void)readPDF:(NSURL *)sourcePDFUrl startRequest:(void (^) (void)) startRequest images:(void (^) (NSArray*)) images
{
    startRequest();
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *dataPdf = [NSData dataWithContentsOfURL:sourcePDFUrl];
        
        //Get path directory
        NSArray *paths =
        NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                            NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        
        //Create PDF_Documents directory
        documentsDirectory = [documentsDirectory
                              stringByAppendingPathComponent:@"PDF_Documents"];
        [[NSFileManager defaultManager]
         createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:nil];
        
        NSString *filePath = [NSString stringWithFormat:@"%@/%@",documentsDirectory, @"test.pdf"];
        
        [dataPdf writeToFile:filePath atomically:YES];
        
        NSMutableArray* list = [NSMutableArray new];
        CGPDFDocumentRef sourcePDFDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)[NSURL URLWithString:[NSString stringWithFormat:@"file://%@",filePath]]);
        
        size_t numberOfPages = CGPDFDocumentGetNumberOfPages(sourcePDFDocument);
        
        for(int currentPage = 1; currentPage <= numberOfPages; currentPage ++ )
        {
            CGPDFPageRef sourcePDFPage = CGPDFDocumentGetPage(sourcePDFDocument, currentPage);
            // CoreGraphics: MUST retain the Page-Refernce manually
            CGPDFPageRetain(sourcePDFPage);
            NSString *relativeOutputFilePath = [NSString stringWithFormat:@"%@/%@%d.png", @"", @"currentPage", currentPage];
            NSString *ImageFileName = [documentsDirectory stringByAppendingPathComponent:relativeOutputFilePath];
            CGRect sourceRect = CGPDFPageGetBoxRect(sourcePDFPage, kCGPDFMediaBox);
            UIGraphicsBeginPDFContextToFile(ImageFileName, sourceRect, nil);
            UIGraphicsBeginImageContextWithOptions(sourceRect.size, false, 2.0);
//            UIGraphicsBeginImageContext(sourceRect.size);
            CGContextRef currentContext = UIGraphicsGetCurrentContext();
            CGContextTranslateCTM(currentContext, 0.0, sourceRect.size.height); //596,842 //640×960,
            CGContextScaleCTM(currentContext, 1.0, -1.0);
            CGContextDrawPDFPage (currentContext, sourcePDFPage); // draws the page in the graphics context
            UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
            [list addObject:image];
            UIGraphicsEndImageContext();
            CGPDFPageRelease(sourcePDFPage);
            //            NSString *imagePath = [documentsDirectory stringByAppendingPathComponent: relativeOutputFilePath];
            //            [UIImagePNGRepresentation(image) writeToFile: imagePath atomically:YES];
        }
        CGPDFDocumentRelease(sourcePDFDocument);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                images(list);
            });
        });
    });
}

+ (NSString*) strimStringFrom:(NSString*) from withPatterns:(NSArray*) pattern {
    NSString* temp = from;
    for (NSString* p in pattern) {
        temp = [temp stringByReplacingOccurrencesOfString:p withString:@"" options:NSRegularExpressionSearch range:NSMakeRange(0,temp.length)];
    }
    return temp;
}

+(UIImage *)changeWhiteColorTransparent: (UIImage *)image{
    CGImageRef rawImageRef=image.CGImage;
    const CGFloat colorMasking[6] = {222, 255, 222, 255, 222, 255};
    UIGraphicsBeginImageContext(image.size);
    CGImageRef maskedImageRef=CGImageCreateWithMaskingColors(rawImageRef, colorMasking);
    {
        //if in iphone
        CGContextTranslateCTM(UIGraphicsGetCurrentContext(), 0.0, image.size.height);
        CGContextScaleCTM(UIGraphicsGetCurrentContext(), 1.0, -1.0);
    }
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, image.size.width, image.size.height), maskedImageRef);
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    CGImageRelease(maskedImageRef);
    UIGraphicsEndImageContext();
    return result;
}
@end
