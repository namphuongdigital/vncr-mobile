//
//  SlideUpTransitioner.swift
//  GlobeDr
//
//  Created by dai on 1/20/20.
//  Copyright © 2020 VNA. All rights reserved.
//

import UIKit

// MARK: -  SlideUpAnimateTransitiong
class ScaleUpAnimateTransiting: NSObject,  UIViewControllerAnimatedTransitioning {
    
    var isPresentation = false
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let fromView = fromVC?.view
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let toView = toVC?.view
        
        let containerView = transitionContext.containerView
        
        let isPresentation = self.isPresentation
        
        if isPresentation {
            containerView.addSubview(toView!)
        }
        
        let animatingVC = isPresentation ? toVC : fromVC
        let animatingView = animatingVC?.view
                
        animatingView?.frame = transitionContext.finalFrame(for: animatingVC!)
        
        let transformDimiss = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        let transFormInitial = CGAffineTransform.identity.scaledBy(x: 0.001, y: 0.001)
        let transformIdentify = CGAffineTransform.identity
        
        animatingView?.transform = isPresentation ? transFormInitial : transformIdentify
        let finalTransform = isPresentation ? transformIdentify : transformDimiss
        
        // Animate using the duration from -transitionDuration:
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext), delay: 0, options: [.beginFromCurrentState,.allowUserInteraction], animations: {
            animatingView?.transform = finalTransform
        }) { (finished) in
            if !self.isPresentation {
                           fromView?.removeFromSuperview()
                       }
                       
                       transitionContext.completeTransition(true)
        }
    }
}

// MARK: -  ScaleUpAnimateTransitionDelegate
class ScaleUpAnimateTransitionDelegate:NSObject, UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return ScaleUpPresentationController(presentedViewController: presented, presenting: presenting)
    }
    
    func animationController(isPresentation:Bool) -> ScaleUpAnimateTransiting {
        let animationController = ScaleUpAnimateTransiting()
        animationController.isPresentation = isPresentation
        return animationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.animationController(isPresentation: false)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.animationController(isPresentation: true)
    }
}

// MARK: -  ScaleUpPresentationController
class ScaleUpPresentationController: UIPresentationController {
    var dimmingView:UIView = UIView()
    var shouldTapDimToClose = true
    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        prepareDimmingView()
    }
    
    func prepareDimmingView() {
        dimmingView.backgroundColor = #colorLiteral(red: 0.137254902, green: 0.1215686275, blue: 0.1254901961, alpha: 0.5151147959)
        dimmingView.alpha = 0
        
//        if shouldTapDimToClose {
//            let tap = UITapGestureRecognizer(target: self, action: #selector(dimmingViewTapped(gesture:)))
//            dimmingView.addGestureRecognizer(tap)
//        }
    }
    
    @objc func dimmingViewTapped(gesture:UIGestureRecognizer) {
        if gesture.state == .recognized {
            self.presentingViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    override var adaptivePresentationStyle: UIModalPresentationStyle {
        // When we adapt to a compact width environment, we want to be over full screen
        return .overFullScreen
    }
    
    override func presentationTransitionWillBegin() {
         // Here, we'll set ourselves up for the presentation
        dimmingView.frame = self.containerView?.bounds ?? .zero
        dimmingView.alpha = 0
        
        // Insert the dimming view below everything else
        self.containerView?.insertSubview(self.dimmingView, at: 0)
        
        if presentedViewController.transitionCoordinator != nil {
            presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (context) in
                self.dimmingView.alpha = 1
            }, completion: nil)
        } else {
            self.dimmingView.alpha = 1
        }
    }
    
    override func dismissalTransitionWillBegin() {
        if presentedViewController.transitionCoordinator != nil {
            presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (context) in
                self.dimmingView.alpha = 0
            }, completion: nil)
        } else {
            self.dimmingView.alpha = 0
        }
    }
    
    override func containerViewWillLayoutSubviews() {
        dimmingView.frame = self.containerView?.bounds ?? .zero
        self.presentedView?.layoutIfNeeded()
        self.presentedView?.frame = self.frameOfPresentedViewInContainerView
        presentedView?.center = self.containerView?.center ?? .zero
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        var presentedViewFrame:CGRect = .zero
        let containerBounds = self.containerView?.bounds ?? .zero

        presentedViewFrame.size =  self.size(forChildContentContainer: self.presentedViewController, withParentContainerSize: containerBounds.size)

        return presentedViewFrame
    }
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
        // We always want a size that's a third of our parent view width, and just as tall as our parent
//        let width:CGFloat = container.preferredContentSize.width// isIpad ? floor(parentSize.width*0.5) : floor(parentSize.width*0.8)
        #if DEBUG
        print("\(container.preferredContentSize) \(#function)")
        #endif
        return container.preferredContentSize
    }

    override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        super.preferredContentSizeDidChange(forChildContentContainer: container)
        #if DEBUG
        print("\(container.preferredContentSize) \(#function)")
        #endif
        guard let containerView = self.containerView, let presentedView = self.presentedView else {return}
        self.presentedView?.frame.size = container.preferredContentSize
        presentedView.center = containerView.center
    }
}
