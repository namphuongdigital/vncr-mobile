//
//  SlideUpTitleBarView.swift
//  GlobeDr
//
//  Created by Dai Pham on 25/05/2021.
//  Copyright © 2021 GlobeDr. All rights reserved.
//

import UIKit

class SlideUpTitleBarView: BaseView {

    @IBOutlet weak var btnClose: ButtonHalfHeight!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var vwTop: Corner10TopView!
    
    var onClose:(()->Void)?
    
    var title:String? {
        didSet {
            lblTitle.text = title
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func config() {
        lblTitle.font = UIFont.systemFont(ofSize: 16)
        lblTitle.textColor = .black
        
        btnClose.backgroundColor = UIColor.white
        btnClose.setImage(#imageLiteral(resourceName: "ic_close_128").resizeImageWith(newSize: CGSize(width: 25, height: 25)).tint(with: .black), for: UIControl.State())
        
        vwTop.backgroundColor = UIColor.white
    }
    
    @IBAction func actionClose(_ sender: Any) {
        self.onClose?()
    }
    
}
