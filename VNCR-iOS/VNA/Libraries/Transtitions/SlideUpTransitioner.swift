//
//  SlideUpTransitioner.swift
//  GlobeDr
//
//  Created by dai on 1/20/20.
//  Copyright © 2020 GlobeDr. All rights reserved.
//

import UIKit

protocol UIPresentationControllerAction {
    func willLayoutSubviewsDone()
}

// MARK: -  SlideUpAnimateTransitiong
extension SlideUpAnimateTransiting:UIGestureRecognizerDelegate {
    
}

class SlideUpAnimateTransiting: NSObject,  UIViewControllerAnimatedTransitioning {
    
    var isPresentation = false
    var panGestureRecognizer: UIPanGestureRecognizer = UIPanGestureRecognizer()
    var interactionController: UIPercentDrivenInteractiveTransition?
    
    override init() {
        super.init()
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        /*
        let fromVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let fromView = fromVC?.view
        let toVC = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        let toView = toVC?.view
        
        let containerView = transitionContext.containerView
        
        let isPresentation = self.isPresentation
        
//        if isPresentation {
//            containerView.addSubview(toView!)
//        }
        
        let animatingVC = isPresentation ? toVC : fromVC
        let animatingView = animatingVC?.view
        
        let appearedFrame = transitionContext.finalFrame(for: animatingVC!)
        // Our dismissed frame is the same as our appeared frame, but off the right edge of the container
        var dismissedFrame = appearedFrame
        var subtract = Router.getMinimumContentSafeAreaBottomView()
        if let height = fromVC?.getProtocolKeyboard()?.getHeight(), height > 0 {
            subtract = height
        }
        dismissedFrame.origin.y += dismissedFrame.size.height + subtract
        
        let initialFrame = isPresentation ? dismissedFrame : appearedFrame
        let finalFrame = isPresentation ? appearedFrame : dismissedFrame
        
        animatingView?.frame = initialFrame
        animatingVC?.view.setNeedsLayout()
        animatingVC?.view.layoutIfNeeded()
        */
        // Animate using the duration from -transitionDuration:
        UIView.animate(withDuration: self.transitionDuration(using: transitionContext),
                       animations: {
//                        animatingView?.frame = finalFrame
        }) { (finished) in
            if !self.isPresentation && !transitionContext.transitionWasCancelled {
//                fromView?.removeFromSuperview()
            }
            
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    

}

// MARK: -  SlideUpAnimateTransitionDelegate
class SlideUpAnimateTransitionDelegate:NSObject, UIViewControllerTransitioningDelegate {
    
    weak var interactionController: UIPercentDrivenInteractiveTransition?
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return SlideUpPresentationController(presentedViewController: presented, presenting: presenting, source: source)
    }
    
    func animationController(isPresentation:Bool, view:UIView?) -> SlideUpAnimateTransiting {
        let animationController = SlideUpAnimateTransiting()
        animationController.isPresentation = isPresentation
        return animationController
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.animationController(isPresentation: false, view:dismissed.view)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self.animationController(isPresentation: true,view:nil)
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactionController
    }
}

// MARK: -  SlideUpPresentationController
class SlideUpPresentationController: UIPresentationController, UIGestureRecognizerDelegate, UIScrollViewDelegate {
    
    func scrollToEnd() {
        dimmingView.setContentOffset(CGPoint(x: 1, y: dimmingView.contentSize.height - dimmingView.bounds.height + dimmingView.contentInset.bottom), animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y = scrollView.contentOffset.y
        let isHide = y < 10
        
        if isHide == titleBarView.isHidden {return}
        UIView.animate(withDuration: 0.25) {
            self.titleBarView.isHidden = isHide
        }
    }
    
    let titleBarView:SlideUpTitleBarView = {
        let view = SlideUpTitleBarView(frame: UIScreen.bounceWindow)
        view.backgroundColor = .clear
        
        return view
    }()
    
    let dimmingColor = #colorLiteral(red: 0.137254902, green: 0.1215686275, blue: 0.1254901961, alpha: 0.5)
    var dimmingView:UIScrollView = TPKeyboardAvoidingScrollView(frame: UIScreen.bounceWindow)
    var preventTapToDismiss:Bool = false
    var source:UIViewController
    
    init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?,source:UIViewController) {
        self.source = source
        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
        prepareDimmingView()
    }
    
//    override init(presentedViewController: UIViewController, presenting presentingViewController: UIViewController?) {
//        super.init(presentedViewController: presentedViewController, presenting: presentingViewController)
//        prepareDimmingView()
//    }
    
    func prepareDimmingView() {
        dimmingView.backgroundColor = .clear//dimmingColor.withAlphaComponent(0)
        dimmingView.showsVerticalScrollIndicator = false
        dimmingView.showsHorizontalScrollIndicator = false
        dimmingView.isUserInteractionEnabled = true
        dimmingView.delegate = self
    }
    
    @objc func dimmingViewTapped(gesture:UITapGestureRecognizer) {
        if !preventTapToDismiss {
            self.presentingViewController.dismiss(animated: true, completion: nil)
        }
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if let _ = gestureRecognizer as? UIPanGestureRecognizer {
//            if let tableview = gestureRecognizer.view as? UITableView {
//                return tableview.contentOffset.y <= 0 && pan.velocity(in: gestureRecognizer.view).y > 0
//            } else if let scrollview = gestureRecognizer.view as? UIScrollView {
//                return scrollview.contentOffset.y <= 0 && pan.velocity(in: gestureRecognizer.view).y > 0
//            }
            return true
        } else {
            return gestureRecognizer.view === touch.view
        }
    }
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        if gestureRecognizer.isKind(of: UITapGestureRecognizer.self){
            return true
        }
        if let pan = gestureRecognizer as? UIPanGestureRecognizer {
            if let tableview = gestureRecognizer.view as? UITableView {
                return tableview.contentOffset.y <= 0 && pan.velocity(in: gestureRecognizer.view).y > 0
            } else if let scrollview = gestureRecognizer.view as? UIScrollView {
                return scrollview.contentOffset.y <= 0 && pan.velocity(in: gestureRecognizer.view).y > 0
            }
            return true
        }
        return false
    }
    
    override func presentationTransitionWillBegin() {
        
        self.containerView?.backgroundColor = dimmingColor
        
//        #if DEBUG
//        print("\(#function)")
//        #endif
         // Here, we'll set ourselves up for the presentation
        dimmingView.frame = source.view.bounds
//        dimmingView.backgroundColor = .clear
        containerView?.isUserInteractionEnabled = true
        // Insert the dimming view below everything else
        self.containerView?.addSubview(self.dimmingView)
        if let container = containerView, let presentedView = self.presentedView {
            
            let margin:CGFloat = 10
            
            dimmingView.addSubview(presentedView)
            container.addSubview(titleBarView)
            titleBarView.isHidden = true
            
            titleBarView.onClose = {[weak self] in
                self?.presentedViewController.dismiss(animated: true, completion: nil)
            }
            
            presentedView.translatesAutoresizingMaskIntoConstraints = false
            dimmingView.translatesAutoresizingMaskIntoConstraints = false
            titleBarView.translatesAutoresizingMaskIntoConstraints = false
            
//            let heightTitleBar = titleBarView.heightAnchor.constraint(equalToConstant: 44)
//            heightTitleBar.priority = UILayoutPriority(999)
//            titleBarView.addConstraint(heightTitleBar)

            let bottom = NSLayoutConstraint(item:dimmingView, attribute: .bottom, relatedBy: .equal, toItem: presentedView, attribute: .bottom, multiplier: 1, constant: 0)
            bottom.priority = UILayoutPriority(1)
            
            let multiplierWidth = (self.presentedViewController as? BaseViewController)?.multiplierWidth ?? 1.0
            
            let topTitleBar = NSLayoutConstraint(item: titleBarView, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1, constant: UIApplication.shared.statusBarFrame.height)
            topTitleBar.priority = UILayoutPriority(999)
            
            if multiplierWidth < 1 {
                self.dimmingView.addConstraints([
                    NSLayoutConstraint(item: presentedView, attribute: .top, relatedBy: .equal, toItem: dimmingView, attribute: .top, multiplier: 1, constant: 0),
                    NSLayoutConstraint(item:presentedView, attribute: .width, relatedBy: .equal, toItem: dimmingView, attribute: .width, multiplier: multiplierWidth, constant: -(margin*2)),
                    NSLayoutConstraint(item:presentedView, attribute: .centerX, relatedBy: .equal, toItem: dimmingView, attribute: .centerX, multiplier: 1.0, constant: 0),
                    bottom
                ])
                
                let widthTitleBar = NSLayoutConstraint(item:titleBarView, attribute: .width, relatedBy: .equal, toItem: container, attribute: .width, multiplier: multiplierWidth, constant: -(margin*2))
                widthTitleBar.priority = UILayoutPriority(999)
                container.addConstraints([
                    topTitleBar,
                    widthTitleBar,
                    NSLayoutConstraint(item:titleBarView, attribute: .centerX, relatedBy: .equal, toItem: container, attribute: .centerX, multiplier: 1.0, constant: 0)
                ])
            } else {
                self.dimmingView.addConstraints([
                    NSLayoutConstraint(item: presentedView, attribute: .top, relatedBy: .equal, toItem: dimmingView, attribute: .top, multiplier: 1, constant: 0),
                    NSLayoutConstraint(item: presentedView, attribute: .leading, relatedBy: .equal, toItem: dimmingView, attribute: .leading, multiplier: 1, constant: margin),
                    NSLayoutConstraint(item:dimmingView, attribute: .trailing, relatedBy: .equal, toItem: presentedView, attribute: .trailing, multiplier: 1, constant: margin),
                    bottom
                ])
                
                let leadingTitleBar = NSLayoutConstraint(item: titleBarView, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1, constant: margin)
                leadingTitleBar.priority = UILayoutPriority(999)
                let trailingTitleBar = NSLayoutConstraint(item:container, attribute: .trailing, relatedBy: .equal, toItem: titleBarView, attribute: .trailing, multiplier: 1, constant: margin)
                trailingTitleBar.priority = UILayoutPriority(999)
                container.addConstraints([
                    topTitleBar,
                    leadingTitleBar,
                    trailingTitleBar
                ])
            }
            
            let equalHeight = NSLayoutConstraint(item:presentedView, attribute: .height, relatedBy: .equal, toItem: container, attribute: .height, multiplier: 1, constant: 0)
            equalHeight.priority = UILayoutPriority(1)
            
            self.containerView?.addConstraints([
                NSLayoutConstraint(item: dimmingView, attribute: .top, relatedBy: .equal, toItem: container, attribute: .top, multiplier: 1, constant: titleBarView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height + UIApplication.shared.statusBarFrame.height + 1),
                NSLayoutConstraint(item: dimmingView, attribute: .leading, relatedBy: .equal, toItem: container, attribute: .leading, multiplier: 1, constant: 0),
                NSLayoutConstraint(item:container, attribute: .trailing, relatedBy: .equal, toItem: dimmingView, attribute: .trailing, multiplier: 1, constant: 0),
                NSLayoutConstraint(item:container, attribute: .bottom, relatedBy: .equal, toItem: dimmingView, attribute: .bottom, multiplier: 1, constant: 0),
                NSLayoutConstraint(item:presentedView, attribute: .width, relatedBy: .equal, toItem: container, attribute: .width, multiplier: multiplierWidth, constant: -(margin*2)),
                equalHeight
            ])
            
            presentedView.transform = CGAffineTransform.identity.translatedBy(x: 0, y: dimmingView.frame.maxY)
        }
        
        if presentedViewController.transitionCoordinator != nil {
            presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (context) in
                self.containerView?.backgroundColor = self.dimmingColor
                self.presentedView?.transform = CGAffineTransform.identity
            }, completion: nil)
        } else {
            self.containerView?.backgroundColor = dimmingColor
        }
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool) {
        if completed {
            let tap = UITapGestureRecognizer(target: self, action: #selector(dimmingViewTapped(gesture:)))
            tap.numberOfTouchesRequired = 1
            tap.cancelsTouchesInView = false
            tap.delegate = self
            dimmingView.addGestureRecognizer(tap)
            let panGestureRecognizer = UIPanGestureRecognizer()
            panGestureRecognizer.delegate = self
            panGestureRecognizer.maximumNumberOfTouches = 1
            panGestureRecognizer.cancelsTouchesInView = true
            panGestureRecognizer.addTarget(self, action: #selector(initiateTransitionInteractively(_:)))
            dimmingView.addGestureRecognizer(panGestureRecognizer)
        }
    }
    
    var interactionController:UIPercentDrivenInteractiveTransition?
    @objc func initiateTransitionInteractively(_ gesture: UIPanGestureRecognizer) {
        guard !preventTapToDismiss, let pView = self.presentedView else {return}
        let translate = gesture.translation(in: gesture.view)
        let percentToDismiss = translate.y / min(dimmingView.frame.height, pView.frame.height)
        let percent   = translate.y / (dimmingView.frame.height)
        if gesture.state == .began {
            interactionController = UIPercentDrivenInteractiveTransition()
            (self.presentedViewController.transitioningDelegate as? SlideUpAnimateTransitionDelegate)?.interactionController = interactionController
            self.presentedViewController.dismiss(animated: true, completion: nil)
        } else if gesture.state == .changed {
            interactionController?.update(percent)
        } else if gesture.state == .ended || gesture.state == .cancelled {
            let velocity = gesture.velocity(in: gesture.view)
            if let intvc = interactionController {
                interactionController?.completionSpeed = (1 - percent)*intvc.duration
            }
            if percentToDismiss > 0.5 || velocity.y > 1000 {
                interactionController?.finish()
            } else {
                interactionController?.cancel()
            }
            interactionController = nil
        }
    }
    
    override func dismissalTransitionWillBegin() {
        
        titleBarView.isHidden = true
        
        if presentedViewController.transitionCoordinator != nil {
            presentedViewController.transitionCoordinator?.animate(alongsideTransition: { (context) in
                self.presentedView?.transform = CGAffineTransform.identity.translatedBy(x: 0, y: self.dimmingView.frame.maxY)
                self.containerView?.backgroundColor = .clear
            }, completion: nil)
        } else {
            self.containerView?.backgroundColor = .clear
        }
    }
    
    override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        super.preferredContentSizeDidChange(forChildContentContainer: container)
        if container.isEqual(presentedViewController) {
            titleBarView.title = presentedViewController.title
            _ = self.frameOfPresentedViewInContainerView
            containerView?.setNeedsLayout()
            containerView?.layoutIfNeeded()
        }
    }
    
    override func size(forChildContentContainer container: UIContentContainer, withParentContainerSize parentSize: CGSize) -> CGSize {
//        #if DEBUG
//        print("\(#function) \(container.preferredContentSize)")
//        #endif
        if container.isEqual(presentedViewController) {
            setContentInset(parentHeight: parentSize.height, presentedViewHeight: container.preferredContentSize.height)
            return container.preferredContentSize
        }
        return super.size(forChildContentContainer: container, withParentContainerSize: parentSize)
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        let containerViewBounds = source.view.bounds
        let presentedViewContentSize = self.size(forChildContentContainer: self.presentedViewController, withParentContainerSize: containerViewBounds.size)

        // The presented view extends presentedViewContentSize.height points from
        // the bottom edge of the screen.
        var presentedViewControllerFrame = containerViewBounds
        presentedViewControllerFrame.size.width = presentedViewContentSize.width
        presentedViewControllerFrame.size.height = presentedViewContentSize.height
//        presentedViewControllerFrame.origin.y = containerViewBounds.maxY - presentedViewContentSize.height - subtract
//        presentedViewControllerFrame.origin.x = 0
        setContentInset(parentHeight: containerViewBounds.maxY, presentedViewHeight: presentedViewContentSize.height)
//        #if DEBUG
//        print("\(#function) \(presentedViewControllerFrame)")
//        #endif
        return presentedViewControllerFrame
    }
    
    func setContentInset(parentHeight:CGFloat, presentedViewHeight:CGFloat) {
        var subtract:CGFloat = UIScreen.getMinimumContentSafeAreaBottomView()
        if let height = self.presentedViewController.getProtocolKeyboard()?.getHeight(), height > 0 {
            subtract = height
        }
        var insetTop = parentHeight - presentedViewHeight - subtract - titleBarView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height - UIApplication.shared.statusBarFrame.height - 1
        if insetTop < 0 {
            insetTop = titleBarView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height - UIApplication.shared.statusBarFrame.height - 1
        }
        let heightKeyboard = self.presentedViewController.getProtocolKeyboard()?.getHeight() ?? 0
        let insetBot = heightKeyboard > 0 ? heightKeyboard : subtract
        dimmingView.contentInset = UIEdgeInsets(top: insetTop, left: 0, bottom: insetBot, right: 0)
    }

    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
//        dimmingView.frame = self.containerView?.bounds ?? .zero
//        dimmingView.contentSize = self.frameOfPresentedViewInContainerView.size
//        CATransaction.begin()
//        UIView.animate(withDuration: 0.3) {
//            self.presentedView?.layoutIfNeeded()
////            self.presentedView?.frame = self.frameOfPresentedViewInContainerView
//        }
//        CATransaction.commit()
    }

//    override var shouldPresentInFullscreen: Bool {
//        return true
//    }
}
