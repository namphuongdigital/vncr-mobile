//
//  SearchBar.swift
//  MBN-iOS-App
//
//  Created by Van Trieu Phu Huy on 12/12/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import UIKit

class SearchBar: UISearchBar {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.autocapitalizationType = UITextAutocapitalizationType.none
        self.searchBarStyle = UISearchBar.Style.minimal
        //searchBar.sizeToFit()
        self.tintColor = UIColor.white
        self.placeholder = "Search".localizedString()
        //searchBar.barTintColor = UIColor.blackColor()
        self.backgroundColor = UIColor.clear
        
        //SearchBar Text
        let textFieldInsideUISearchBar = self.value(forKey: "searchField") as? UITextField
        textFieldInsideUISearchBar?.textColor = UIColor.white
        textFieldInsideUISearchBar?.tintColor = UIColor.lightText
        //SearchBar Placeholder
        
        let textFieldInsideUISearchBarLabel = textFieldInsideUISearchBar?.value(forKey: "_placeholderLabel") as? UILabel
        textFieldInsideUISearchBarLabel?.textAlignment = .left
    }
    
    

}

extension UISearchBar {

    /// Returns the`UITextField` that is placed inside the text field.
    var textField: UITextField {
        if #available(iOS 13, *) {
            return searchTextField
        } else {
            return self.value(forKey: "_searchField") as! UITextField
        }
    }

}

class SearchBarContainerView: UIView {
    
    let searchBar: UISearchBar
    
    init(customSearchBar: UISearchBar) {
        searchBar = customSearchBar
        super.init(frame: CGRect.zero)
        
        addSubview(searchBar)
    }
    
    override convenience init(frame: CGRect) {
        self.init(customSearchBar: UISearchBar())
        self.frame = frame
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        searchBar.frame = bounds
    }
}
