//
//  Protocols.swift
//  VNA
//
//  Created by Dai Pham on 07/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation

protocol ProtocolKeyboard {
    func getHeight() -> CGFloat
}

protocol XIBLocalizable {
    var locKey: String? { get set }
}
