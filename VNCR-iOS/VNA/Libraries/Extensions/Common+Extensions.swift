//
//  Common+Extensions.swift
//  VNA
//
//  Created by Dai Pham on 25/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation

public extension Notification.Name {
    static let passengerMapLoadDone = Notification.Name("passengerMapLoadDone")
}

extension TimeInterval{
    
    func stringFromTimeInterval() -> String {
        
        let time = NSInteger(self)
        
//        let ms = Int((self.truncatingRemainder(dividingBy: 1)) * 1000)
        let seconds = time % 60
        let minutes = (time / 60) % 60
        let hours = (time / 3600)
        if hours > 0 {
            return String(format: "%0.2d:%0.2d:%0.2d",hours,minutes,seconds)
        } else {
            return String(format: "%0.2d:%0.2d",minutes,seconds)
        }
        
    }
}
