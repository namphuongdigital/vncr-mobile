//
//  UILabel+Extensions.swift
//  VNA
//
//  Created by Pham Dai on 23/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation

extension UILabel {
    
    typealias ELEMENT_ATTRIBUTE = (text:String,color:UIColor?,font:UIFont?)
    
    /// set attribute text
    /// - Parameter elements: element include: 0: text, 1: forgroundColro, 2: UIFont
    func setAttribute(elements:[ELEMENT_ATTRIBUTE]) {
        
        let mutable = NSMutableAttributedString(string: "")
        
        elements.forEach {
            mutable.append($0.text.toAttributed(font: $0.font, foregroundColor: $0.color))
        }
        attributedText = mutable
    }
}

extension UILabel: XIBLocalizable {
    @IBInspectable var locKey: String? {
        get { return nil }
        set(key) {
            text = key?.localizedString()
        }
   }
}
