//
//  String+Extensions.swift
//  VNA
//
//  Created by Dai Pham on 25/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation

extension String {
    static let keyEnableFaceID = "keyEnableFaceID"
    
    func toAttributed(font: UIFont?, foregroundColor:UIColor?) -> NSAttributedString {
        var attributes:[NSAttributedString.Key:Any] = [:]
        if let v = foregroundColor {
            attributes[NSAttributedString.Key.foregroundColor] = v
        }
        if let v = font {
            attributes[NSAttributedString.Key.font] = v
        }
        return NSAttributedString(string: self, attributes: attributes)
    }
    
    func trunc(length: Int, trailing: String = "…") -> String {
        return (self.count > length) ? self.prefix(length) + trailing : self
    }
    
    func isValidEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return  NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: self.trimmingCharacters(in:.whitespacesAndNewlines))
    }
}

extension NSMutableAttributedString {
    func setAsLink(textToFind:String, linkName:String, color:UIColor? = nil) {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            
            var attributes:[NSAttributedString.Key:Any] = [NSAttributedString.Key.link: linkName]
            if let color = color {
                attributes[NSAttributedString.Key.foregroundColor] = color
            }
            self.addAttributes(attributes, range: foundRange)
        }
    }
}
