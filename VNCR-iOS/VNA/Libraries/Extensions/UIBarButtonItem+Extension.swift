//
//  UIBarButtonItem+Extension.swift
//  VNA
//
//  Created by Dai Pham on 21/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

extension UIBarButtonItem {
    
    convenience init(image :UIImage?,
                     title :String?,
                     color: UIColor = .white,
                     font:UIFont? = UIFont.boldSystemFont(ofSize: 20),
                     isLeft:Bool = true,
                     target: Any?,
                     action: Selector?) {
        let button = UIButton(type: .custom)
        button.contentHorizontalAlignment = isLeft ? .left : .right
        if let image = image {
            if #available(iOS 13.0, *) {
                button.setImage(image.withTintColor(color), for: UIControl.State())
            } else {
                button.setImage(image.tint(with: color), for: UIControl.State())
            }
        }
        
        button.titleLabel?.numberOfLines = 1
        button.titleLabel?.lineBreakMode = isLeft ? .byTruncatingTail : NSLineBreakMode.byWordWrapping
        button.setTitle(title, for: UIControl.State())
        button.titleLabel?.font = font
        button.titleLabel?.textAlignment = isLeft ? .left : .right
        if isLeft {
            button.titleLabel?.minimumScaleFactor = 0.6
            button.imageEdgeInsets = UIEdgeInsets.init(top: 0, left: -5, bottom: 0, right: 10)
        }
        
        button.setTitleColor(color, for: UIControl.State())
        
        var width = button.titleLabel!.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).width
        if let _ = image, width > UIScreen.main.bounds.size.width - 120 {
            width = UIScreen.main.bounds.size.width - 10 - 120
        }
        
        if let _ = image {
            button.frame = CGRect(x: 0, y: 0, width: 25 + width, height: 25)
        } else if let _ = title {
            button.frame = CGRect(x: 0, y: 0, width: width, height: 25)
        }
        
        if let target = target, let action = action {
            button.addTarget(target, action: action, for: .touchUpInside)
        }
        self.init(customView: button)
    }
}
