//
//  Webview+Extensions.swift
//  VNA
//
//  Created by Pham Dai on 03/12/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import WebKit

extension WKWebView {
    func contentHeight(_ complete:((Float)->Void)?){
        self.evaluateJavaScript("Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight );") {(result, error) in
            var h:Float = 0
            if let result = result as? Float {
                h = result
            }
            DispatchQueue.main.async {
                complete?(h)
            }
        }
    }
    
    func contentWidth(_ complete:((Float)->Void)?){
        self.evaluateJavaScript("Math.max( document.documentElement.clientWidth, document.body.clientWidth);") {(result, error) in
            var h:Float = 0
            if let result = result as? Float {
                h = result
            }
            DispatchQueue.main.async {
                complete?(h)
            }
        }
    }
    
    func fitContentSize(_ complete:(()->Void)?) {
        let group = DispatchGroup()
        var width:CGFloat = 0
        var height:CGFloat = 0
        group.enter()
        contentHeight {h in
            let contentHeight = NSNumber.init(value: h)
            height = CGFloat(contentHeight.floatValue)
            group.leave()
        }
        
        group.enter()
        contentWidth {h in
            let contentHeight = NSNumber.init(value: h)
            width = CGFloat(contentHeight.floatValue)
            group.leave()
        }
        
        group.notify(queue: .main) {[weak self] in
            guard let _self = self else {return}
            self?.frame = CGRect(origin: _self.frame.origin, size: CGSize(width: width, height: height))
            complete?()
        }
    }
}
