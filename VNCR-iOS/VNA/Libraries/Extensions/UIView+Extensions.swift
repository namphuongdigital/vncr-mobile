//
//  UIView+Extensions.swift
//  VNA
//
//  Created by Dai Pham on 23/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

private var KeepTap:String = "KeepTap"
private var KeepTapEvent:String = "KeepTapEvent"

extension UIView {
    func getHeightConstraint() -> NSLayoutConstraint? {
        for constraint in constraints {
            if constraint.firstAttribute == NSLayoutConstraint.Attribute.height {
                return constraint
            }
        }
        return nil
    }
    
    func getWidthConstraint() -> NSLayoutConstraint? {
        for constraint in constraints {
            if constraint.firstAttribute == NSLayoutConstraint.Attribute.width {
                return constraint
            }
        }
        return nil
    }
    
    var singleTap:UITapGestureRecognizer {
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(_:)))
        singleTap.cancelsTouchesInView = true
        singleTap.numberOfTapsRequired = 1 // you can change this value
        return singleTap
    }
    
    func addEvent(gestureDelegate:UIGestureRecognizerDelegate? = nil, _ event:@escaping ((UIView)->Void)) {
        if objc_getAssociatedObject(self, &KeepTap) != nil {return}
        let tap = singleTap
        tap.delegate = gestureDelegate
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tap)
        objc_setAssociatedObject(self, &KeepTap, tap, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        objc_setAssociatedObject(self, &KeepTapEvent, event, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func removeEvent() {
        if let tap = objc_getAssociatedObject(self, &KeepTap) as? UITapGestureRecognizer {
            self.removeGestureRecognizer(tap)
            objc_setAssociatedObject(self, &KeepTap, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        if let _ = objc_getAssociatedObject(self, &KeepTapEvent) as? ((UIView)->Void) {
            objc_setAssociatedObject(self, &KeepTapEvent, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    //Action
    @objc func tapDetected(_ sender:UITapGestureRecognizer) {
        if let event = objc_getAssociatedObject(self, &KeepTapEvent) as? ((UIView)->Void) {
            event(self)
        }
    }
    
    var haveEventTouch: Bool {
        return (objc_getAssociatedObject(self, &KeepTapEvent) as? (()->Void)) != nil
    }
    
    func roundCornersFull(corners:UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func boundInside(_ superView: UIView, insets:UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)){
        
        self.translatesAutoresizingMaskIntoConstraints = false
        superView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-\(insets.left)@999-[subview]-\(insets.right)@999-|", options: NSLayoutConstraint.FormatOptions(), metrics:nil, views:["subview":self]))
        superView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-\(insets.top)@999-[subview]-\(insets.bottom)@999-|", options: NSLayoutConstraint.FormatOptions(), metrics:nil, views:["subview":self]))
    }
    
    func addRemoveButton(target:Any?,action:Selector, keepObject:Any?) {
        let button = ButtonHalfHeight(type: .custom)
        button.object = keepObject
        button.contentMode = .scaleToFill
        button.tag = 19001800
        button.setImage(UIImage(named: "ic_clear"), for: UIControl.State())
        button.backgroundColor = .white
        button.addTarget(target, action: action, for: .touchUpInside)
        addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        let top = button.topAnchor.constraint(equalTo: topAnchor, constant: 0)
        top.priority = UILayoutPriority(999)
        let trailing = trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: 0)
        trailing.priority = UILayoutPriority(999)
        self.addConstraints([top,trailing])
        
        let height = button.heightAnchor.constraint(equalToConstant: 25)
        height.priority = UILayoutPriority(999)
        let ratio = button.widthAnchor.constraint(equalTo: button.heightAnchor, multiplier: 1)
        ratio.priority = UILayoutPriority(999)
        button.addConstraints([height,ratio])
    }
    
    func removeRemoveButton(target:Any?,action:Selector?) {
        let btn = self.subviews.first(where: {$0.tag == 19001800}) as? UIButton
        btn?.removeTarget(target, action: action, for: .touchUpInside)
        btn?.removeFromSuperview()
    }
    
    func addTargetCustom(target:Any?,action:Selector, keepObject:Any?, boundInside:UIEdgeInsets = .zero) {
        removeTargetCustom(target: target, action: action)
        
        isUserInteractionEnabled = true
        let button = Button10Corner(type: .custom)
        button.object = keepObject
        button.tag = 19001801
        button.setTitle(nil, for: UIControl.State())
        addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.boundInside(self, insets: boundInside)
        
        button.addTarget(target, action: action, for: .touchUpInside)
    }
    
    func removeTargetCustom(target:Any?,action:Selector?) {
        let btn = self.subviews.first(where: {$0.tag == 19001801}) as? UIButton
        btn?.removeTarget(target, action: action, for: .touchUpInside)
        btn?.removeFromSuperview()
    }
    
    func addButtonPlayVideo(target: Any?, action: Selector?) {
        let button = ButtonHalfHeight(type: .custom)
        button.tag = 19001802
        button.backgroundColor = .darkGray
        button.setImage(UIImage(named: "ic_play")?.tint(with: .white), for: UIControl.State())
        button.contentMode = .center
        if let action = action {
            button.addTarget(target, action: action, for: .touchUpInside)
        }
        addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        button.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
    }
    
    var hideButtonPlayVideo:Bool {
        set {
            subviews.first(where: {$0.tag == 19001802})?.isHidden = newValue
        }
        get {
            return subviews.first(where: {$0.tag == 19001802})?.isHidden ?? true
        }
    }
    
    func removeButtonPlayVideo() {
        subviews.first(where: {$0.tag == 19001802})?.removeFromSuperview()
    }
}
