//
//  UIColor+Extensions.swift
//  VNA
//
//  Created by Pham Dai on 24/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation

extension UIColor {
    var imageRepresentation : UIImage {
        let rect = CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0)
        UIGraphicsBeginImageContext(rect.size)
        let context = UIGraphicsGetCurrentContext()
        
        context?.setFillColor(self.withAlphaComponent(1).cgColor)
        context?.fill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
    }
}
