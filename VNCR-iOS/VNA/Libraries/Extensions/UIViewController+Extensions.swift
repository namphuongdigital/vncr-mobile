//
//  UIViewController+Extensions.swift
//  VNA
//
//  Created by Dai Pham on 23/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

extension UIViewController {
    
    var isIpad:Bool { UIDevice.current.userInterfaceIdiom == .pad }
    
    /// Nếu dùng custom transitioning thi phai chuyen sang UIModalPresentationStyle.custom
    func setCustomPresentationStyle(modalPresentationStyle:UIModalPresentationStyle = UIModalPresentationStyle.custom) {
        if #available(iOS 13.0, *){
            self.isModalInPopover = true
        }
        self.providesPresentationContextTransitionStyle = true
        self.modalPresentationStyle = modalPresentationStyle
    }
    
    func searchBarContainer(text:String?,
                            placeholder:String?,
                            delegate:UISearchBarDelegate?) -> UIView {
        let searchbar = UISearchBar()
        searchbar.tintColor = .white
        var searchText : UITextField?
        if #available(iOS 13.0, *) {
            searchText = searchbar.searchTextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        else {
            searchbar.barTintColor = UIColor.white
            searchText = searchbar.value(forKey: "_searchField") as? UITextField
            searchText?.backgroundColor = .groupTableViewBackground
        }
        UITextField.appearance(whenContainedInInstancesOf: [type(of: searchbar)]).tintColor = .darkText
        searchText?.font = UIFont.systemFont(ofSize: 12)
        searchbar.text = text
        searchbar.placeholder = placeholder
        searchbar.delegate = delegate
//        searchbar.showsCancelButton = true
        let searchBarContainer = SearchBarContainerView(customSearchBar: searchbar)
        searchBarContainer.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 44)
        return searchBarContainer
    }
    
    func getProtocolKeyboard() -> ProtocolKeyboard? {
        if let tb = (self as? UITabBarController), let vc = tb.viewControllers?[tb.selectedIndex] {
            if let temp = (vc as? UINavigationController)?.viewControllers.last as? ProtocolKeyboard {
                return temp
            } else if let temp = vc as? ProtocolKeyboard {
                return temp
            }
            return nil
        } else
        if let temp = (self as? UINavigationController)?.viewControllers.last as? ProtocolKeyboard {
            return temp
        } else if let temp = (self as? ProtocolKeyboard) {
            return temp
        }
        return nil
    }
}

extension UINavigationController {
    func find(viewController:AnyClass) ->UIViewController? {
        for vc in viewControllers {
            if vc.isKind(of: viewController) {
                return vc
            }
        }
        return nil
    }
}
