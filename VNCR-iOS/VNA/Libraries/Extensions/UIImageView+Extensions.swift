//
//  UIImageView+Extensions.swift
//  VNA
//
//  Created by Pham Dai on 24/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation
extension UIImageView {
    // use for mark favourite categories
    func addMask(color:UIColor,_ alpha:CGFloat = 1) {
        removeMask()
        let mask = UIView(frame: self.bounds)
        self.addSubview(mask)
        mask.tag = 9989
        mask.backgroundColor = color.withAlphaComponent(alpha)
        mask.translatesAutoresizingMaskIntoConstraints = false
        mask.topAnchor.constraint(equalTo: mask.superview!.topAnchor, constant: 0).isActive = true
        mask.trailingAnchor.constraint(equalTo: mask.superview!.trailingAnchor, constant: 0).isActive = true
        mask.bottomAnchor.constraint(equalTo: mask.superview!.bottomAnchor, constant: 0).isActive = true
        mask.leadingAnchor.constraint(equalTo: mask.superview!.leadingAnchor, constant: 0).isActive = true
        mask.layer.masksToBounds = true
    }
    
    func removeMask() {
        _ = self.subviews.reversed().map{if $0.tag == 9989 {$0.removeFromSuperview()}}
    }
    
    func load(urlString: String?,
              _ onComplete:((UIImage?)->Void)? = nil){
        
        //        cancelAllTaskDownloadImage()
        
        guard let URLString = urlString else {
            DispatchQueue.main.async {
                onComplete?(nil)
            }
            return
        }
        
        accessibilityIdentifier = URLString
        
        func taskDownloadImage(URLString:String,_ onComplete:((UIImage?)->Void)? = nil) {
            
            if let url = URL(string: URLString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)) {
                self.accessibilityIdentifier = url.absoluteString
                
                let config = URLSessionConfiguration.default
                //                config.requestCachePolicy = .useProtocolCachePolicy
                //            let session = URLSession(configuration: config, delegate: nil, delegateQueue: nil)
                let session = URLSession(configuration: config)
                let request = URLRequest(url: url)
                
                let task = session.dataTask(with: request, completionHandler: {[weak self] (data, response, error) in
                    if let data = data {
                        if let downloadedImage = UIImage(data: data) {
                            DispatchQueue.main.async {
                                onComplete?(downloadedImage)
                            }
                        }
                    }
                })
                task.accessibilityLabel = url.absoluteString
                task.resume()
            } else {
                onComplete?(nil)
            }
        }
        
        taskDownloadImage(URLString: URLString, onComplete)
    }
}
