//
//  Date+Extensions.swift
//  VNA
//
//  Created by Pham Dai on 22/11/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation

// MARK: - DATE
extension Date {
    func convertDateFormat(from: String, to: String, dateString: String?) -> String? {
        let fromDateFormatter = DateFormatter()
        fromDateFormatter.dateFormat = from
        var formattedDateString: String? = nil
        if dateString != nil {
            let formattedDate = fromDateFormatter.date(from: dateString!)
            if formattedDate != nil {
                let toDateFormatter = DateFormatter()
                toDateFormatter.dateFormat = to
                formattedDateString = toDateFormatter.string(from: formattedDate!)
            }
        }
        return formattedDateString
    }
    
    func toString( dateFormat format  : String ,isUTC: Bool = false) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        if isUTC {
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        }
//        dateFormatter.locale = NSLocale.init(localeIdentifier: AppConfig.language.getCurrentLanguageDeviceLocale) as Locale
        return dateFormatter.string(from: self)
    }
    
    func addedBy(minutes:Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func stringUTC() -> String {
        let formmater = DateFormatter()
        formmater.timeZone = TimeZone(abbreviation: "UTC")
        formmater.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.sss"
        return formmater.string(from: self)
    }
}

