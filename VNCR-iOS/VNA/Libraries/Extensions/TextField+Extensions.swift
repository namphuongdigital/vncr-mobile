//
//  TextField+Extensions.swift
//  VNA
//
//  Created by Pham Dai on 22/11/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation

extension UITextField {
    func addToolbarEndEditing(title:String = "Done".localizedString()) {
        let bar = UIToolbar()
        let reset = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(self.endEdit))
        bar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),reset]
        bar.sizeToFit()
        inputAccessoryView = bar
    }
    
    @objc private func endEdit() {
        resignFirstResponder()
    }
}
