//
//  PHAsset+Extensions.swift
//  VNA
//
//  Created by Pham Dai on 24/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import Photos

extension PHAsset {
    @discardableResult
    func getImage(size:CGSize, deliveryMode:PHImageRequestOptionsDeliveryMode = .opportunistic,_ completion:((UIImage?)->Void)? = nil) -> PHImageRequestID? {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        option.isNetworkAccessAllowed = true
        option.resizeMode = .exact
        option.deliveryMode = deliveryMode
        option.isSynchronous = true
        return manager.requestImage(for: self, targetSize: size, contentMode: .aspectFit, options: option, resultHandler: {[weak self] (result, info)->Void in
            guard let _self = self else {
                completion?(nil)
                return
            }
            if let result = result {
                completion?(result)
            } else {
                manager.requestImageData(for: _self, options: option) { (data, str, orientation, info) in
                    if let data = data {
                        completion?(UIImage(data: data))
                    } else {
                        completion?(nil)
                    }
                }
            }
        })
    }
    
    func cancelRequest(requestId:PHImageRequestID) {
        PHImageManager.default().cancelImageRequest(requestId)
    }
    
    func getURL(completionHandler : @escaping ((_ responseURL : URL?) -> Void)){
        if self.mediaType == .image {
            let options: PHContentEditingInputRequestOptions = PHContentEditingInputRequestOptions()
            options.canHandleAdjustmentData = {(adjustmeta: PHAdjustmentData) -> Bool in
                return true
            }
            self.requestContentEditingInput(with: options, completionHandler: {(contentEditingInput: PHContentEditingInput?, info: [AnyHashable : Any]) -> Void in
                completionHandler(contentEditingInput!.fullSizeImageURL as URL?)
            })
        } else if self.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
            options.version = .original
            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url as URL
                    completionHandler(localVideoUrl)
                } else {
                    completionHandler(nil)
                }
            })
        }
    }
    
    func getDataToUpload(completionHandler : @escaping ((_ responseURL : URL?,_ data:Data?,_ error:Any?) -> Void)){
        if self.mediaType == .image {
            let manager = PHImageManager.default()
            let option = PHImageRequestOptions()
            option.isNetworkAccessAllowed = true
            option.resizeMode = .exact
            option.deliveryMode = .highQualityFormat
            option.isSynchronous = true
            manager.requestImageData(for: self, options: option) { (data, str, orientation, info) in
                if let data = data, let image = UIImage(data: data) {
                    if var imageData = image.jpegData(compressionQuality: 1) {
                        if imageData.count > 2*1024 {
                            imageData = image.jpegData(compressionQuality: 0.5)!
                        }
                        DispatchQueue.main.async {
                            completionHandler(nil,imageData,nil)
                        }
                    }
                } else {
                    completionHandler(nil,nil,info)
                }
            }
            
        } else if self.mediaType == .video {
            let options: PHVideoRequestOptions = PHVideoRequestOptions()
//            options.version = .current
            options.deliveryMode = .mediumQualityFormat
            options.isNetworkAccessAllowed = true
            PHImageManager.default().requestAVAsset(forVideo: self, options: options, resultHandler: {(asset: AVAsset?, audioMix: AVAudioMix?, info: [AnyHashable : Any]?) -> Void in
                if let urlAsset = asset as? AVURLAsset {
                    let localVideoUrl: URL = urlAsset.url
                    completionHandler(localVideoUrl,try? Data(contentsOf: localVideoUrl, options: []),nil)
                } else {
                    completionHandler(nil,nil,info)
                }
            })
        }
    }
}
