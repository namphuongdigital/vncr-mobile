//
//  UIButton+Extensions.swift
//  VNA
//
//  Created by Pham Dai on 27/08/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import Foundation

extension UIButton {
    func setBackground(normal:UIImage?, highlighted:UIImage?) {
        setBackgroundImage(normal, for: .normal)
        setBackgroundImage(highlighted, for: .highlighted)
        showsTouchWhenHighlighted = true
    }
    
    func setTitleColor(normal:UIColor?, highlighted:UIColor?) {
        setTitleColor(normal, for: .normal)
        setTitleColor(highlighted, for: .highlighted)
        showsTouchWhenHighlighted = true
    }
}

extension UIButton: XIBLocalizable {
    @IBInspectable var locKey: String? {
        get { return nil }
        set(key) {
            setTitle(key?.localizedString(), for: UIControl.State())
        }
   }
}
