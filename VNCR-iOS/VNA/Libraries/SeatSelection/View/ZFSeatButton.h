//
//  ZFSeatButton.h
//  
//

#import <UIKit/UIKit.h>
#import "ZFSeatsModel.h"

@interface ZFSeatButton : UIButton

@property (nonatomic, assign) CGRect seatFrame;

@property (nonatomic, strong) ZFSeatModel *seatmodel;


@property (nonatomic, strong) ZFSeatsModel *seatsmodel;


@property (nonatomic,assign) NSInteger seatIndex;


-(BOOL)isSeatAvailable;
@end
