//
//  ZFSeatSelectionTool.h
//  ZFSeatsSelection
//

#import <Foundation/Foundation.h>
#import "ZFSeatButton.h"
#import "ZFSeatsModel.h"
#import "ZFSeatModel.h"
@interface ZFSeatSelectionTool : NSObject


+(BOOL)verifySelectedSeatsWithSeatsDic:(NSMutableDictionary *)allAvailableSeats seatsArray:(NSArray *)seatsArray;
+(NSArray *)getNearBySeatsInSameRowForSeat:(ZFSeatButton *)seat withAllAvailableSeats:(NSMutableDictionary *)allAvailableSeats;
+(BOOL)isSeat:(ZFSeatButton *)s1 nearBySeatWithoutRoad:(ZFSeatButton *)s2;
@end
