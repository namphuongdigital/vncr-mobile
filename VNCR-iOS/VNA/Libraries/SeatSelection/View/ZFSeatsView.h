//
//  ZFSeatsView.h
//  ZFSeatSelection
//

#import <UIKit/UIKit.h>
#import "ZFSeatButton.h"
@interface ZFSeatsView : UIView

@property (nonatomic,assign) CGFloat seatBtnMarginWidth;

@property (nonatomic,assign) CGFloat seatBtnMarginHeight;

@property (nonatomic,strong) UIImageView *imageViewBackground;

@property (nonatomic,assign) CGFloat seatBtnWidth;


@property (nonatomic,assign) CGFloat seatBtnHeight;


@property (nonatomic,assign) CGFloat seatViewWidth;


@property (nonatomic,assign) CGFloat seatViewHeight;


-(instancetype)initWithSeatsArray:(NSMutableArray *)seatsArray
              imageViewBackground:(UIImage *)image
                    maxNomarWidth:(CGFloat)maxW
               seatBtnActionBlock:(void(^)(ZFSeatButton *seatBtn,NSMutableDictionary *allAvailableSeats))actionBlock
               seatTouchPointBlock:(void(^)(CGPoint touchPoint,ZFSeatsView* seatView))seatTouchPointBlock;

@end
