//
//  ZFSeatSelectionView.h
//  ZFSeatSelection
//

#import <UIKit/UIKit.h>
#import "ZFSeatButton.h"
#import "ZFSeatsView.h"

@interface ZFSeatSelectionView : UIView

@property (nonatomic, weak) ZFSeatsView *seatView;

-(void)initindicator:(NSMutableArray *)seatsArray;
-(instancetype)initWithFrame:(CGRect)frame
         imageViewBackground:(UIImage *)image
                  SeatsArray:(NSMutableArray *)seatsArray
                    HallName:(NSString *)hallName
          seatBtnActionBlock:(void(^)(NSMutableArray *selecetedSeats,NSMutableDictionary *allAvailableSeats,NSString *errorStr))actionBlock
          seatActionTouchPointBlock:(void(^)(CGPoint touch,UIView* seatView))actionTouchPointBlock;

@end
