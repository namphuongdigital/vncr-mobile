//
//  ZFSeatsView.m
//  ZFSeatSelection
//

#import "ZFSeatsView.h"
#import "UIColor+HexString.h"
#import "ZFSeatSelectionConfig.h"

@interface ZFSeatsView ()

@property (nonatomic,copy) void (^actionBlock)(ZFSeatButton *seatBtn,NSMutableDictionary *allAvailableSeats);

@property (nonatomic,copy) void (^actionTouchPointBlock)(CGPoint point,ZFSeatsView* seatView);

@property (nonatomic,strong) NSMutableDictionary *allAvailableSeats;

@property (nonatomic,assign) CGFloat imageViewBackgroundWidth;

@property (nonatomic,assign) CGFloat imageViewBackgroundHeight;

@end

@implementation ZFSeatsView

-(NSMutableDictionary *)allAvailableSeats{
    if (!_allAvailableSeats) {
        _allAvailableSeats = [NSMutableDictionary dictionary];
    }
    return _allAvailableSeats;
}

-(instancetype)initWithSeatsArray:(NSMutableArray *)seatsArray
              imageViewBackground:(UIImage *)image
                    maxNomarWidth:(CGFloat)maxW
               seatBtnActionBlock:(void (^)(ZFSeatButton *, NSMutableDictionary *))actionBlock
              seatTouchPointBlock:(void(^)(CGPoint touchPoint,ZFSeatsView* seatView))seatTouchPointBlock{
    
    if (self = [super init]) {
        
        self.imageViewBackground = [[UIImageView alloc] init];
        self.imageViewBackground.contentMode = UIViewContentModeScaleAspectFit;
        if (image.size.width > image.size.height) {
            image = [UIImage imageWithCGImage:image.CGImage scale:1.0 orientation:(UIImageOrientationRight)];
        }
        self.imageViewBackground.image = image;
        self.imageViewBackgroundWidth = self.imageViewBackground.image.size.width;
        self.imageViewBackgroundHeight = self.imageViewBackground.image.size.height;
        
        self.seatBtnMarginWidth = 50.0f;
        
        self.seatBtnMarginHeight = 50.0f;
        
        self.actionBlock = actionBlock;
        self.actionTouchPointBlock = seatTouchPointBlock;
        
        ZFSeatsModel *seatsModel = seatsArray.firstObject;
        
        NSUInteger cloCount = [seatsModel.columns count];
        
        if (cloCount % 2) cloCount += 1;
        
        CGFloat seatViewW = maxW - 2 * ZFseastsRowMargin;
        
        CGFloat seatBtnW = seatViewW / cloCount;
        
        if (seatBtnW > ZFseastMinW_H) {
            seatBtnW = ZFseastMinW_H;
            seatViewW = (cloCount * (ZFseastMinW_H + self.seatBtnMarginWidth) + self.seatBtnMarginWidth);
        }
        
        
        
        CGFloat seatBtnH = seatBtnW;
        self.seatBtnWidth = seatBtnW;
        self.seatBtnHeight = seatBtnH;
        
        
        self.seatViewWidth = seatViewW;
        self.seatViewHeight = ([seatsArray count] * (seatBtnH + self.seatBtnMarginHeight) + self.seatBtnMarginHeight);

        
        self.seatViewWidth = self.imageViewBackgroundWidth;
        self.seatViewHeight = self.imageViewBackgroundHeight;
        
        [self addSubview:self.imageViewBackground];
        
        [self initSeatBtns:seatsArray];
        
    }
    return self;
}

-(void)initSeatBtns:(NSMutableArray *)seatsArray{

     static NSInteger seatIndex = 0;
    [seatsArray enumerateObjectsUsingBlock:^(ZFSeatsModel *seatsModel, NSUInteger idx, BOOL *stop) {
        
        for (int i = 0; i < seatsModel.columns.count; i++) {
            seatIndex++;
            ZFSeatModel *seatModel = seatsModel.columns[i];
            ZFSeatButton *seatBtn = [ZFSeatButton buttonWithType:UIButtonTypeCustom];
            seatBtn.seatmodel = seatModel;
            seatBtn.seatsmodel = seatsModel;
            
            CGFloat width = self.seatViewWidth / seatsModel.columns.count;
            CGFloat height = width;
            CGRect seatFrame = CGRectMake(width * i, height * ([seatsModel.rowNum intValue] - 1), width, height);
            seatBtn.seatFrame = seatFrame;
            
            if ([seatModel.st isEqualToString:@"N"]) {
                if(seatModel.colorHex != nil && [seatModel.colorHex isEqualToString:@""] == false) {
                    [seatBtn setImage:[[UIImage imageNamed:@"kexuan.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
                    seatBtn.tintColor = [UIColor colorWithHexString:seatModel.colorHex];
                } else {
                    [seatBtn setImage:[UIImage imageNamed:@"kexuan.png"] forState:UIControlStateNormal];
                }
                
                [seatBtn setImage:[UIImage imageNamed:@"xuanzhong.png"] forState:UIControlStateSelected];
                seatBtn.seatIndex = seatIndex;
                [self.allAvailableSeats setObject:seatBtn forKey:[@(seatIndex) stringValue]];
            }else if ([seatModel.st isEqualToString:@"E"]){
                continue;
            }else{
                [seatBtn setImage:[UIImage imageNamed:@"yishou.png"] forState:UIControlStateNormal];
                seatBtn.userInteractionEnabled = NO;
            }
            [seatBtn addTarget:self action:@selector(seatBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [self addSubview:seatBtn];
            
        }
    }];
    
}

-(void)layoutSubviews{
    [super layoutSubviews];

    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[ZFSeatButton class]]) {
            ZFSeatButton *seatBtn = (ZFSeatButton *)view;
            seatBtn.frame = seatBtn.seatmodel.frame;
            
        }
    }
    
    self.imageViewBackground.frame = self.bounds;

}
-(void)seatBtnAction:(ZFSeatButton *)seatbtn{
    
    seatbtn.selected = !seatbtn.selected;
    if (seatbtn.selected) {
        seatbtn.seatmodel.st = @"LK";
    }else{
        seatbtn.seatmodel.st = @"N";
    }
    
   if (self.actionBlock) self.actionBlock(seatbtn,self.allAvailableSeats);
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    UITouch* touch = [touches anyObject];
    CGPoint touchpoint = [touch locationInView:self];
    if (self.actionTouchPointBlock) self.actionTouchPointBlock(touchpoint,self);
}

@end
