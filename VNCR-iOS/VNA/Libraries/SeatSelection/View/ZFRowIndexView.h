//
//  ZFRowIndexView.h
//  
//

#import <UIKit/UIKit.h>

@interface ZFRowIndexView : UIView

@property (nonatomic, strong) NSMutableArray *indexsArray;

@end
