//
//  ZFAppLogoView.m
//  
//

#import "ZFAppLogoView.h"
#import "UIView+Extension.h"
@implementation ZFAppLogoView


- (void)drawRect:(CGRect)rect {
    
    UIImage *appLogo = [UIImage imageNamed:@"maoyan_logo"];
    
    [appLogo drawInRect:rect];
}


@end
