//
//  ZFSeatSelectionConfig.h
//  ZFSeatSelectionDemo


#define  ZFScreenW [UIScreen mainScreen].bounds.size.width
#define  ZFScreenH [UIScreen mainScreen].bounds.size.height
#define  ZFseastsColMargin 60
#define  ZFseastsRowMargin 40
#define  ZFCenterLineY 50
#define  ZFseastMinW_H 10
#define  ZFseastNomarW_H 25
#define  ZFseastMaxW_H 40
#define  ZFSmallMargin 10
#define  ZFAppLogoW 100 //applogo
#define  ZFHallLogoW 200 //halllogo
#define  ZFSeatBtnScale 0.95



#define  ZFMaxSelectedSeatsCount 100
#define  ZFExceededMaximumError @"ZFExceededMaximumError"
