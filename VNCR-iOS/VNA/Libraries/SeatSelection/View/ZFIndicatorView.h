//
//  ZFIndicatorView.h
//  
//

#import <UIKit/UIKit.h>

@interface ZFIndicatorView : UIView


-(instancetype)initWithView:(UIView *)mapView withRatio:(CGFloat)ratio withScrollView:(UIScrollView *)myScrollview;


- (void)updateMiniIndicator;


-(void)updateMiniImageView;


-(void)indicatorHidden;
@end
