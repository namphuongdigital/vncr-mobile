//
//  ZFHallLogoView.h
//  
//

#import <UIKit/UIKit.h>

@interface ZFHallLogoView : UIView


@property (nonatomic, strong) NSString *hallName;

@end
