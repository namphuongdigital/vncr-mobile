//
//  ZFSeatsModel.h
//  ZFSeatSelection
//

#import <Foundation/Foundation.h>
#import "ZFSeatModel.h"
@interface ZFSeatsModel : NSObject

@property (nonatomic, strong) NSArray *columns;

@property (nonatomic, copy) NSString *rowId;

@property (nonatomic, copy) NSNumber *rowNum;


@end
