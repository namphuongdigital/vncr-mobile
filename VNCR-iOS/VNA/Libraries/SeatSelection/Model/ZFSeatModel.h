//
//  ZFSeatModel.h
//  ZFSeatSelection
//
#import <Foundation/Foundation.h>
#import "ZFSeatModel.h"
#import <UIKit/UIKit.h>

@interface ZFSeatModel : NSObject


@property (nonatomic, copy) NSString *columnId;

@property (nonatomic, copy) NSString *seatNo;

@property (nonatomic, copy) NSString *st;

@property (nonatomic, copy) NSString *colorHex;

@property (nonatomic, assign) CGRect frame;

@end
