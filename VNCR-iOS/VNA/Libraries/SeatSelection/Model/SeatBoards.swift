//
//  SeatBoards.swift
//  VNA
//
//  Created by Dai Pham on 26/04/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

// MARK: -  Boars Seat Support
class BoardSeats:NSObject {
    
    // MARK: -  Cabin
     class CabinBoat:NSObject {
        let rowNames:[String]
        let columnNames:[String]
        let sizeSeat:CGSize
        let originX:[CGFloat]
        let originY:[CGFloat]
        
        init(rowNames:[String],
             columnNames:[String],
             sizeSeat:CGSize,
             originX:[CGFloat],
             originY:[CGFloat]) {
            self.rowNames = rowNames
            self.columnNames = columnNames
            self.sizeSeat = sizeSeat
            self.originX = originX
            self.originY = originY
            super.init()
        }
        
        @objc func getSeats() -> [Seat] {
            var seats:[Seat] = []
            rowNames.enumerated().forEach { (rO) in
                columnNames.enumerated().forEach { (cO) in
                    let seat = Seat()
                    if cO.offset < originX.count && rO.offset < originY.count {
                        seat.position = CGRect(origin: CGPoint(x: originX[cO.offset], y: originY[rO.offset]), size: sizeSeat)
                    }
                    seat.identifier = "\(rO.element)\(cO.element)"
                    seats.append(seat)
                }
            }
            return seats
        }
    }
    
     class Seat:NSObject {
        @objc var identifier:String = ""
        @objc var position:CGRect = .zero
        @objc var isVip:Bool = false
        
        @objc func getName() -> String {
            return identifier
        }
    }
    
    var seatInvalids:[String] = []
    var compartments:[CabinBoat] = []
    
    @objc func getSeats() -> [Seat] {
        var seats:[Seat] = []
        compartments.forEach({seats.append(contentsOf: $0.getSeats())})
        return seats.filter({!seatInvalids.contains($0.identifier)})
    }
}

// MARK: -  A787
class A787Board:BoardSeats {
    
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["25A","25B","25C","25G","25H","25K"]
        
        // for bussiness seats
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4","5","6","7"],
                                            columnNames: ["A","D","G","K"],
                                            sizeSeat: CGSize(width: 39, height: 33),
                                            originX: [85,177,226,317],
                                            originY: [361,401,440,479,519,558,597])
        
        // for bussiness seats
        let businessEcoSeats = CabinBoat(rowNames: ["10","11","12","14","15"],
                                               columnNames: ["A","C","D","E","F","G","K"],
                                               sizeSeat: CGSize(width: 25, height: 21),
                                               originX: [63,91,175,207,238,320,351],
                                               originY: [775,800,825,850,875])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["16","17","18","19","20","21","22","23","24"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [63,91,121,175,207,238,293,320,351],
                                        originY: [929,956,983,1010,1037,1064,1091,1118,1145])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["25","26","27","28","29","30","31","32","33","34","35","36","37","38"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [60,88,114,179,208,236,301,329,356],
                                        originY: [1262,1290,1318,1346,1373,1401,1429,1457,1485,1513,1540,1568,1596,1624])
        
        // for eco seats
        let ecoSeats3 = CabinBoat(rowNames: ["39","40"],
                                        columnNames: ["A","C","D","E","F","G","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [86,116,179,209,236,301,328],
                                        originY: [1679,1706])
        
        
        compartments.append(contentsOf: [businessSeats,businessEcoSeats,ecoSeats1,ecoSeats2,ecoSeats3])
    }
}

// MARK: -  A350
class A350Board:BoardSeats {
    
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["8D","8G","8K",
                        "27A","27B","27C","27G","27H","27K",
                        "42A","42B","42C","42G","42H","42K"]
        
        /// this config only appy for board image with dimension: 442 x 1926
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4","5","6","7","8"],
                                            columnNames: ["A","D","G","K"],
                                            sizeSeat: CGSize(width: 38, height: 31),
                                            originX: [88,177,224,322],
                                            originY: [302,339,376,414,449,486,523,561])
        
        // for bussiness seats
        let businessEcoSeats = CabinBoat(rowNames: ["10","11","12","14","15"],
                                               columnNames: ["A","B","C","D","E","F","G","H","K"],
                                               sizeSeat: CGSize(width: 28, height: 25),
                                               originX: [59,88,117,180,209,238,296,326,355],
                                               originY: [691,727,763,799,833])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["16","17","18","19","20","21","22","23","24","25","26"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [59,88,116,180,208,237,296,325,353],
                                        originY: [899,929,959,989,1019,1049,1079,1109,1139,1169,1199])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [59,88,116,180,208,237,296,325,353],
                                        originY: [1301,1332,1362,1392,1422,1452,1482,1512,1542,1572,1602,1632,1662,1692,1721,1752])
        
        compartments.append(contentsOf: [businessSeats,businessEcoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  A321-16
class A32116Board:BoardSeats {
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        /// this config only appy for board image with dimension: 442 x 1926
        
        seatInvalids = ["14D",
                        "27D",
                        "38A","38B","38C","38D"]
        
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4"],
                                            columnNames: ["A","C","D","G"],
                                            sizeSeat: CGSize(width: 41, height: 39),
                                            originX: [98,146,250,307],
                                            originY: [370,419,469,516])
        
        // for bussiness seats
        let ecoSeats = CabinBoat(rowNames: ["10","11","12"],
                                               columnNames: ["A","B","C","D","E","G"],
                                               sizeSeat: CGSize(width: 30, height: 30),
                                               originX: [84,123,164,251,289,328],
                                               originY: [601,638,675])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["14","15","16","17","18","19","20","21","22","23","24","25","26"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [84,123,164,251,289,328],
                                        originY: [754,792,829,866,904,941,978,1016,1053,1090,1128,1165,1202])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["27","28","29","30","31","32","33","34","35","36","37","38"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [84,123,164,251,289,328],
                                        originY: [1284,1322,1359,1396,1433,1471,1508,1546,1582,1620,1657,1695])
        
        compartments.append(contentsOf: [businessSeats,ecoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  A321-8
class A3218Board:BoardSeats {
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["17A","17G",
                        "18D",
                        "31A","31G",
                        "44A","44B","44C"]
        
        /// this config only appy for board image with dimension: 442 x 1926
        let businessSeats = CabinBoat(rowNames: ["1","2"],
                                            columnNames: ["A","C","D","G"],
                                            sizeSeat: CGSize(width: 41, height: 39),
                                            originX: [97,145,250,306],
                                            originY: [298,347])
        
        // for bussiness seats
        let ecoSeats = CabinBoat(rowNames: ["10","11","12","14","15","16","17"],
                                               columnNames: ["A","B","C","D","E","G"],
                                               sizeSeat: CGSize(width: 30, height: 30),
                                               originX: [83,122,163,250,287,327],
                                               originY: [426,463,501,540,577,614,652])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["18","19","20","21","22","23","24","25","26","27","28","29","30","31"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [84,123,164,251,289,328],
                                        originY: [719,756,794,831,868,906,943,980,1018,1056,1093, 1131,1168,1204])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["32","33","34","35","36","37","38","39","40","41","42","43","44"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [84,123,164,251,289,328],
                                        originY: [1273,1309,1348,1385,1422,1460,1498,1535,1572,1610,1647,1685,1727])
        
        compartments.append(contentsOf: [businessSeats,ecoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: - 32A
class A32Board:BoardSeats {
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        /// this config only appy for board image with dimension: 442 x 1926
        
        seatInvalids = ["14D",
                        "27D",
                        "38A","38B","38C","38D"]
        
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4"],
                                            columnNames: ["A","C","D","G"],
                                            sizeSeat: CGSize(width: 41, height: 39),
                                            originX: [93,143,248,302],
                                            originY: [370,419,469,516])
        
        // for bussiness seats
        let ecoSeats = CabinBoat(rowNames: ["10","11","12"],
                                               columnNames: ["A","B","C","D","E","G"],
                                               sizeSeat: CGSize(width: 30, height: 30),
                                               originX: [81,119,159,247,284,324],
                                               originY: [601,638,675])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["14","15","16","17","18","19","20","21","22","23","24","25","26"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [81,119,159,247,284,324],
                                        originY: [754,792,829,866,904,941,978,1016,1053,1090,1128,1165,1202])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["27","28","29","30","31","32","33","34","35","36","37","38"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [81,119,159,247,284,324],
                                        originY: [1284,1322,1359,1396,1433,1471,1508,1546,1582,1620,1657,1695])
        
        compartments.append(contentsOf: [businessSeats,ecoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  32B
class B32Board:BoardSeats {
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        /// this config only appy for board image with dimension: 442 x 1926
        
        seatInvalids = ["15D",
                        "28D",
                        "39A","39B","39C","39D"]
        
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4"],
                                            columnNames: ["A","C","D","G"],
                                            sizeSeat: CGSize(width: 41, height: 39),
                                            originX: [89,138,245,300],
                                            originY: [372,419,469,516])
        
        // for bussiness seats
        let ecoSeats = CabinBoat(rowNames: ["10","11","12","14"],
                                               columnNames: ["A","B","C","D","E","G"],
                                               sizeSeat: CGSize(width: 30, height: 30),
                                               originX: [80,119,159,239,278,317],
                                               originY: [607,643,680,718])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["15","16","17","18","19","20","21","22","23","24","25","26","27"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,159,239,278,317],
                                        originY: [790,828,866,903,940,977,1014,1052,1089,1126,1163,1201,1237])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["28","29","30","31","32","33","34","35","36","37","38","39"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,159,239,278,317],
                                        originY: [1303,1341,1377,1415,1452,1489,1526,1564,1601,1639,1676,1711])
        
        compartments.append(contentsOf: [businessSeats,ecoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  32C
class C32Board:A32116Board {
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        /// this config only appy for board image with dimension: 442 x 1926
        
        seatInvalids = ["15D",
                        "28D",
                        "39A","39B","39C","39D"]
        
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4"],
                                            columnNames: ["A","C","D","G"],
                                            sizeSeat: CGSize(width: 41, height: 39),
                                            originX: [89,138,245,300],
                                            originY: [372,419,469,516])
        
        // for bussiness seats
        let ecoSeats = CabinBoat(rowNames: ["10","11","12","14"],
                                               columnNames: ["A","B","C","D","E","G"],
                                               sizeSeat: CGSize(width: 30, height: 30),
                                               originX: [80,119,159,239,278,317],
                                               originY: [607,643,680,718])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["15","16","17","18","19","20","21","22","23","24","25","26","27"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,159,239,278,317],
                                        originY: [790,828,866,903,940,977,1014,1052,1089,1126,1163,1201,1237])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["28","29","30","31","32","33","34","35","36","37","38","39"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,159,239,278,317],
                                        originY: [1303,1341,1377,1415,1452,1489,1526,1564,1601,1639,1676,1711])
        
        compartments.append(contentsOf: [businessSeats,ecoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  32D
class D32Board:BoardSeats {
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["17A","17G",
                        "18D",
                        "31A","31G",
                        "44A","44B","44C"]
        
        /// this config only appy for board image with dimension: 442 x 1926
        let businessSeats = CabinBoat(rowNames: ["1","2"],
                                            columnNames: ["A","C","D","G"],
                                            sizeSeat: CGSize(width: 41, height: 39),
                                            originX: [94,143,248,304],
                                            originY: [298,347])
        
        // for bussiness seats
        let ecoSeats = CabinBoat(rowNames: ["10","11","12","14","15","16","17"],
                                               columnNames: ["A","B","C","D","E","G"],
                                               sizeSeat: CGSize(width: 30, height: 30),
                                               originX: [80,119,160,247,284,324],
                                               originY: [426,463,501,540,577,614,652])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["18","19","20","21","22","23","24","25","26","27","28","29","30","31"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,160,247,284,324],
                                        originY: [719,756,794,831,868,906,943,980,1018,1056,1093, 1131,1168,1204])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["32","33","34","35","36","37","38","39","40","41","42","43","44"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,160,247,284,324],
                                        originY: [1273,1309,1348,1385,1422,1460,1498,1535,1572,1610,1647,1685,1726])
        
        compartments.append(contentsOf: [businessSeats,ecoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  32E
class E32Board:BoardSeats {
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["17A","17G",
                        "18D",
                        "31A","31G",
                        "44A","44B","44C"]
        
        /// this config only appy for board image with dimension: 442 x 1926
        let businessSeats = CabinBoat(rowNames: ["1","2"],
                                            columnNames: ["A","C","D","G"],
                                            sizeSeat: CGSize(width: 41, height: 39),
                                            originX: [94,143,248,304],
                                            originY: [298,347])
        
        // for bussiness seats
        let ecoSeats = CabinBoat(rowNames: ["10","11","12","14","15","16","17"],
                                               columnNames: ["A","B","C","D","E","G"],
                                               sizeSeat: CGSize(width: 30, height: 30),
                                               originX: [80,119,160,247,284,324],
                                               originY: [426,463,501,540,577,614,652])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["18","19","20","21","22","23","24","25","26","27","28","29","30","31"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,160,247,284,324],
                                        originY: [719,756,794,831,868,906,943,980,1018,1056,1093, 1131,1168,1204])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["32","33","34","35","36","37","38","39","40","41","42","43","44"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,160,247,284,324],
                                        originY: [1273,1309,1348,1385,1422,1460,1498,1535,1572,1610,1647,1685,1726])
        
        compartments.append(contentsOf: [businessSeats,ecoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  32N
class N32Board:BoardSeats {
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["17A","17G",
                        "18D",
                        "31A","31G",
                        "44A","44B","44C"]
        
        /// this config only appy for board image with dimension: 442 x 1926
        let businessSeats = CabinBoat(rowNames: ["1","2"],
                                            columnNames: ["A","C","D","G"],
                                            sizeSeat: CGSize(width: 41, height: 39),
                                            originX: [94,143,248,304],
                                            originY: [298,347])
        
        // for bussiness seats
        let ecoSeats = CabinBoat(rowNames: ["10","11","12","14","15","16","17"],
                                               columnNames: ["A","B","C","D","E","G"],
                                               sizeSeat: CGSize(width: 30, height: 30),
                                               originX: [80,119,160,247,284,324],
                                               originY: [426,463,501,540,577,614,652])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["18","19","20","21","22","23","24","25","26","27","28","29","30","31"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,160,247,284,324],
                                        originY: [719,756,794,831,868,906,943,980,1018,1056,1093, 1131,1168,1204])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["32","33","34","35","36","37","38","39","40","41","42","43","44"],
                                        columnNames: ["A","B","C","D","E","G"],
                                        sizeSeat: CGSize(width: 30, height: 30),
                                        originX: [80,119,160,247,284,324],
                                        originY: [1273,1309,1348,1385,1422,1460,1498,1535,1572,1610,1647,1685,1726])
        
        compartments.append(contentsOf: [businessSeats,ecoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  35A
class A35Board:BoardSeats {
    
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["8D","8G","8K",
                        "27A","27B","27C","27G","27H","27K",
                        "42A","42B","42C","42G","42H","42K"]
        
        /// this config only appy for board image with dimension: 442 x 1926
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4","5","6","7","8"],
                                            columnNames: ["A","D","G","K"],
                                            sizeSeat: CGSize(width: 38, height: 31),
                                            originX: [94,183,230,328],
                                            originY: [302,339,376,414,449,486,523,561])
        
        // for bussiness seats
        let businessEcoSeats = CabinBoat(rowNames: ["10","11","12","14","15"],
                                               columnNames: ["A","B","C","D","E","F","G","H","K"],
                                               sizeSeat: CGSize(width: 28, height: 25),
                                               originX: [64,93,122,185,214,243,301,331,360],
                                               originY: [691,727,763,799,834])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["16","17","18","19","20","21","22","23","24","25","26"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [65,93,121,186,215,241,302,331,359],
                                        originY: [899,929,959,989,1019,1049,1079,1109,1139,1169,1199])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [65,93,121,186,215,241,302,331,359],
                                        originY: [1301,1332,1362,1392,1422,1452,1482,1512,1542,1572,1602,1632,1662,1692,1721,1751])
        
        compartments.append(contentsOf: [businessSeats,businessEcoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  35B
class B35Board:BoardSeats {
    
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["8D","8G","8K",
                        "15A","15C","15H","15K",
                        "28A","28B","28C","28G","28H","28K",
                        "43A","43B","43C","43G","43H","43K"]
        
        /// this config only appy for board image with dimension: 442 x 1926
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4","5","6","7","8"],
                                            columnNames: ["A","D","G","K"],
                                            sizeSeat: CGSize(width: 19, height: 18),
                                            originX: [80,126,147,196],
                                            originY: [172,193,213,234,254,274,294,314])
        
        // for bussiness seats
        let businessEcoSeats = CabinBoat(rowNames: ["10","11","12","14","15"],
                                               columnNames: ["A","C","D","E","F","G","H","K"],
                                               sizeSeat: CGSize(width: 14, height: 15),
                                               originX: [65,80,112,126,140,155,198,213],
                                               originY: [386,402,418,433,449])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["16","17","18","19","20","21","22","23","24","25","26","27"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 14, height: 15),
                                        originX: [65,79,94,126,139,154,184,198,213],
                                        originY: [484,501,517,534,550,567,583,600,616,633,649,666])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 14, height: 15),
                                        originX: [65,79,94,126,139,154,184,198,213],
                                        originY: [721,737,754,770,787,803,820,836,853,869,886,902,919,935,951,968])
        
        compartments.append(contentsOf: [businessSeats,businessEcoSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  78A
class A78Board:BoardSeats {
    
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["25A","25B","25C","25G","25H","25K",
                        "40A","40C","40G","40K"]
        
        // for bussiness seats
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4","5","6","7"],
                                            columnNames: ["A","D","G","K"],
                                            sizeSeat: CGSize(width: 39, height: 33),
                                            originX: [85,177,226,317],
                                            originY: [361,401,440,479,519,558,597])
        
        // for bussiness seats
        let businessEcoSeats = CabinBoat(rowNames: ["10","11","12","14","15"],
                                               columnNames: ["A","C","D","E","F","G","K"],
                                               sizeSeat: CGSize(width: 25, height: 21),
                                               originX: [63,94,175,207,238,320,350],
                                               originY: [775,800,825,850,875])
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["16","17","18","19","20","21","22","23","24"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [63,91,121,175,207,238,293,320,351],
                                        originY: [929,956,983,1010,1037,1064,1091,1118,1145])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["25","26","27","28","29","30","31","32","33","34","35","36","37","38"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [60,88,114,179,208,236,301,329,356],
                                        originY: [1262,1290,1318,1346,1373,1401,1429,1457,1485,1513,1540,1568,1596,1624])
        
        // for eco seats
        let ecoSeats3 = CabinBoat(rowNames: ["39","40"],
                                        columnNames: ["A","C","D","E","F","G","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [86,116,179,209,236,301,328],
                                        originY: [1679,1706])
        
        
        compartments.append(contentsOf: [businessSeats,businessEcoSeats,ecoSeats1,ecoSeats2,ecoSeats3])
    }
}

// MARK: -  78B
class B78Board:BoardSeats {
    
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["32D","32E","32F","32G","32H","32K",
                        "33A","33B","33C","33G","33H","33K",
                        "47A","47K",
                        "48A","48B","48C","48G","48H","48K",
                        "49A","49B","49C","49G","49H","49K",
                        "50A","50B","50C","50G","50H","50K"]
        
        // for bussiness seats
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4","5","6","7"],
                                            columnNames: ["A","D","G","K"],
                                            sizeSeat: CGSize(width: 39, height: 33),
                                            originX: [85,177,226,317],
                                            originY: [361,401,440,479,519,558,597])
        
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [62,91,121,175,207,238,293,320,351],
                                        originY: [776,803,830,857,884,911,938,965,992,1020,1046,1074,1101,1128,1155,1183,1210])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [62,91,121,175,207,238,293,320,351],
                                        originY: [1286,1314,1341,1369,1397,1425,1453,1481,1509,1536,1564,1592,1620,1647,1673,1701,1728,1756])
        
        
        compartments.append(contentsOf: [businessSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  78C
class C78Board:BoardSeats {
    
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = ["20A","20B","20C","20G","20H","20K",
                        "21A","21B","21C","21G","21H","21K",
                        "37A","37B","37C","37G","37H","37K",
                        "38A","38B","38C","38G","38H","38K",
                        "56A","56C","56G","56K"]
        
        // for bussiness seats
        let businessSeats = CabinBoat(rowNames: ["1","2","3","4","5","6"],
                                            columnNames: ["A","D","G","K"],
                                            sizeSeat: CGSize(width: 33, height: 27),
                                            originX: [92,170,211,287],
                                            originY: [320,349,377,406,434,463])
        
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["16","17","18","19","20"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 19, height: 16),
                                        originX: [74,98,123,169,195,221,269,294,319],
                                        originY: [519,542,565,587,611])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [74,98,123,169,195,221,269,294,319],
                                        originY: [799,822,845,868,891,914,936,959,982,1006,1028,1051,1074,1097,1120,1145,1167])
        
        // for eco seats
        let ecoSeats3 = CabinBoat(rowNames: ["38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54"],
                                        columnNames: ["A","B","C","D","E","F","G","H","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [74,98,123,169,195,221,269,294,319],
                                        originY: [1274,1297,1320,1343,1366,1389,1412,1435,1458,1481,1504,1527,1550,1573,1596,1620,1642])
        
        // for eco seats
        let ecoSeats4 = CabinBoat(rowNames: ["55","56"],
                                        columnNames: ["A","C","D","E","F","G","K"],
                                        sizeSeat: CGSize(width: 25, height: 21),
                                        originX: [98,123,169,195,221,269,294],
                                        originY: [1691,1717])
        
        
        compartments.append(contentsOf: [businessSeats,ecoSeats1,ecoSeats2,ecoSeats3,ecoSeats4])
    }
}

// MARK: -  AT7
class AT7Board:BoardSeats {
    
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = []
        
        // for bussiness seats
        let businessSeats = CabinBoat(rowNames: ["1"],
                                            columnNames: ["A","B","C","D"],
                                            sizeSeat: CGSize(width: 31, height: 32),
                                            originX: [70,104,168,201],
                                            originY: [292])
        
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18"],
                                        columnNames: ["A","B","C","D"],
                                        sizeSeat: CGSize(width: 31, height: 32),
                                        originX: [70,104,168,201],
                                        originY: [340,390,440,489,539,588,638,687,736,786,835,885,935,984,1033,1083])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["19"],
                                        columnNames: ["A","B","C","D"],
                                        sizeSeat: CGSize(width: 31, height: 32),
                                        originX: [70,104,168,201],
                                        originY: [1134])
        
        
        compartments.append(contentsOf: [businessSeats,ecoSeats1,ecoSeats2])
    }
}

// MARK: -  ATR
class ATRBoard:BoardSeats {
    
    override init() {
        super.init()
        config()
    }
    
    private func config() {
        
        seatInvalids = []
        
        // for bussiness seats
        let businessSeats = CabinBoat(rowNames: ["1"],
                                            columnNames: ["A","B","C","D"],
                                            sizeSeat: CGSize(width: 31, height: 32),
                                            originX: [70,104,168,201],
                                            originY: [292])
        
        // for eco seats
        let ecoSeats1 = CabinBoat(rowNames: ["2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18"],
                                        columnNames: ["A","B","C","D"],
                                        sizeSeat: CGSize(width: 31, height: 32),
                                        originX: [70,104,168,201],
                                        originY: [340,390,440,489,539,588,638,687,736,786,835,885,935,984,1033,1083])
        
        // for eco seats
        let ecoSeats2 = CabinBoat(rowNames: ["19"],
                                        columnNames: ["A","B","C","D"],
                                        sizeSeat: CGSize(width: 31, height: 32),
                                        originX: [70,104,168,201],
                                        originY: [1134])
        
        
        compartments.append(contentsOf: [businessSeats,ecoSeats1,ecoSeats2])
    }
}

