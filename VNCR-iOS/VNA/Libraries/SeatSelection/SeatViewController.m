//
//  SeatViewController.m
//  VNA
//
//  Created by Van Trieu Phu Huy on 3/24/19.
//  Copyright © 2019 OneTeam. All rights reserved.
//

#import "SeatViewController.h"
#import "ZFSeatsModel.h"

@interface SeatViewController ()

@end

@implementation SeatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)loadData {
    /*
    NSMutableArray *seatsModelArray = [NSMutableArray array];
    
    CGFloat xColumn1 = 97.0;
    CGFloat xColumn2 = 145.0;
    CGFloat xColumn3 = 250.0;
    CGFloat xColumn4 = 307.0;
    
    CGFloat yRow1 = 371;
    CGFloat yRow2 = 420;
    CGFloat yRow3 = 470;
    CGFloat yRow4 = 517;
    
    CGFloat seatWidth = 40.0;
    CGFloat seatHeight = 39.0;
    
    for(NSInteger i = 0; i < 4; i++) {
        ZFSeatsModel *seatsModel = [[ZFSeatsModel alloc] init];
        seatsModel.rowNum = [NSNumber numberWithInteger:(i + 1)];
        seatsModel.rowId = [NSString stringWithFormat:@"%@", seatsModel.rowNum];
        CGFloat yRow = 0;
        if(i == 0) {
            yRow = yRow1;
        } else if(i == 1) {
            yRow = yRow2;
        } else if(i == 2) {
            yRow = yRow3;
        } else if(i == 3) {
            yRow = yRow4;
        }
        NSMutableArray *arrayColumn = [NSMutableArray new];
        for(NSInteger j = 0; j < 4 ; j++) {
            CGFloat xColumn = 0;
            ZFSeatModel *seatModel = [[ZFSeatModel alloc] init];
            seatModel.columnId = @"";
            seatModel.seatNo = @"";
            seatModel.st = @"N";
            seatModel.frame = CGRectZero;
            if(j == 0) {
                xColumn = xColumn1;
                seatModel.frame = CGRectMake(xColumn, yRow, seatWidth, seatHeight);
            } else if(j == 1) {
                xColumn = xColumn2;
                seatModel.frame = CGRectMake(xColumn, yRow, seatWidth, seatHeight);
            } else if(j == 2) {
                xColumn = xColumn3;
                seatModel.frame = CGRectMake(xColumn, yRow, seatWidth, seatHeight);
            } else if(j == 3) {
                xColumn = xColumn4;
                seatModel.frame = CGRectMake(xColumn, yRow, seatWidth, seatHeight);
            }
            [arrayColumn addObject:seatModel];
        }
        seatsModel.columns = [[NSArray alloc] initWithArray:arrayColumn];
        [seatsModelArray addObject:seatsModel];
    }
    
    self.seatsModelArray = seatsModelArray;
    */
    [self initSelectionView:self.seatsModelArray];
    
}

- (void)seatSelectionChanged {
}

- (void)seatTouchPoint:(CGPoint)touchPoint seatView:(UIView*) seatview {
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)initSelectionView:(NSMutableArray *)seatsModelArray{
    __weak typeof(self) weakSelf = self;
    _selectionView = [[ZFSeatSelectionView alloc]initWithFrame:[self.view bounds] imageViewBackground:[UIImage imageNamed:self.imageBackgroundName] SeatsArray:seatsModelArray HallName:@"" seatBtnActionBlock:^(NSMutableArray *selecetedSeats, NSMutableDictionary *allAvailableSeats, NSString *errorStr) {
        if (errorStr) {
            
        } else{
            weakSelf.allAvailableSeats = allAvailableSeats;
            weakSelf.selecetedSeats = selecetedSeats;
            [weakSelf seatSelectionChanged];
        }
    }
    seatActionTouchPointBlock:^(CGPoint touch,UIView* view) {
        [weakSelf seatTouchPoint:touch seatView:view];
    }];
    
    [self.view addSubview:_selectionView];
    _selectionView.translatesAutoresizingMaskIntoConstraints = false;
    
    [[_selectionView.topAnchor constraintEqualToAnchor:self.view.topAnchor constant:0] setActive:true];
    [[_selectionView.leadingAnchor constraintEqualToAnchor:self.view.leadingAnchor constant:0] setActive:true];
    [[self.view.bottomAnchor constraintEqualToAnchor:_selectionView.bottomAnchor constant:0] setActive:true];
    [[self.view.trailingAnchor constraintEqualToAnchor:_selectionView.trailingAnchor constant:0] setActive:true];
}


@end
