//
//  SeatViewController.h
//  VNA
//
//  Created by Van Trieu Phu Huy on 3/24/19.
//  Copyright © 2019 OneTeam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZFSeatSelectionView.h"

NS_ASSUME_NONNULL_BEGIN

@interface SeatViewController : UIViewController

@property (nonatomic, strong) NSString *imageBackgroundName;

@property (nonatomic, strong) NSMutableArray *seatsModelArray;

@property (nonatomic, strong) NSMutableArray *selecetedSeats;

@property (nonatomic, strong) NSMutableDictionary *allAvailableSeats;

@property (nonatomic, strong) ZFSeatSelectionView *selectionView;

- (void)loadData;

- (void)initSelectionView:(NSMutableArray *)seatsModelArray;

- (void)seatSelectionChanged;
- (void)seatTouchPoint:(CGPoint)touchPoint seatView:(UIView*) seatview;

@end

NS_ASSUME_NONNULL_END
