//
//  ViewController.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/27/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var buttonScheduleFlyViewController: UIButton!
    
    @IBOutlet weak var buttonLeadReportPositionViewController: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.navigationController?.isNavigationBarHidden = true
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showScheduleFlyViewController(sender: UIButton) {
        
        
        let scheduleFlyViewController = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleFlyViewController") as! ScheduleFlyViewController
        self.navigationController?.pushViewController(scheduleFlyViewController, animated: true)
 
    }

    @IBAction func showLeadReportPositionViewController(sender: UIButton) {
        let leadReportPositionViewController = self.storyboard?.instantiateViewController(withIdentifier: "LeadReportPositionViewController") as! LeadReportPositionViewController
        self.navigationController?.pushViewController(leadReportPositionViewController, animated: true)
    }


}

