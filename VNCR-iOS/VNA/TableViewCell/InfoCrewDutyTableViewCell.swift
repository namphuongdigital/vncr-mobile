//
//  InfoCrewDutyTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class InfoCrewDutyTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNo: UILabel!
    @IBOutlet weak var labelText: UILabel!
    
    @IBOutlet weak var viewLine: UIView!
    
    weak var crewDutyModel: CrewDutyModel?
    
    var fontSize: CGFloat = 16.0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 13
        }
        
        self.imageViewAvatar.contentMode = .scaleAspectFit
        self.imageViewAvatar.layer.borderWidth = 0.5
        self.imageViewAvatar.layer.borderColor = UIColor.lightGray.cgColor
        self.imageViewAvatar.layer.cornerRadius = 1.5
        self.imageViewAvatar.clipsToBounds = true
        
        viewLine.backgroundColor = UIColor.lightGray
        viewLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: UIScreen.main.bounds.size.width, height: 0.8)
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(crewDutyModel: CrewDutyModel, totalScore: Int) {
        self.crewDutyModel = crewDutyModel
        self.labelName.text = String(format: "%@ %@", crewDutyModel.crewLastName, crewDutyModel.crewFirstName)
        
        var colorCrewFirstName = "#6699cc"
        if(crewDutyModel.isPurser) {
            colorCrewFirstName = "#DBA510"
        } else {
            colorCrewFirstName = "#A0A0A0"
        }
        
        self.labelNo.text = String(format: "%@ / %@ / %@ (%@)", crewDutyModel.training, crewDutyModel.foJob, crewDutyModel.pos, crewDutyModel.ability)
        self.labelNo.attributedText = addBoldText(fullString: self.labelNo.text! as NSString, boldPartsOfString: [crewDutyModel.foJob as NSString, crewDutyModel.pos as NSString], font: self.labelNo.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor("#dba510"))
        
        self.labelName.attributedText = addBoldText(fullString: self.labelName.text! as NSString, boldPartsOfString: [crewDutyModel.crewFirstName as NSString], font: self.labelName.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor(colorCrewFirstName))
        
        self.labelText.text = String(format: "%@ %d", "Total score :".localizedString(), totalScore)
        
        self.imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: crewDutyModel.avatarURL))
        
    }
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!, boldColor: UIColor) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: font!]
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: boldColor]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        
        
        return boldString
    }
    
}
