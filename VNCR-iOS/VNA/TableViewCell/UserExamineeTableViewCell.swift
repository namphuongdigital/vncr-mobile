//
//  UserExamineeTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class UserExamineeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var textTaskLabel: UILabel!
    
    @IBOutlet weak var textNameLabel: UILabel!
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    var userExamineeModel: UserExamineeModel? {
        didSet {
            self.loadViewData()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imageViewAvatar.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadViewData() {
        if let userExamineeModel = self.userExamineeModel {
            textNameLabel.text = userExamineeModel.fullName
            //textNameLabel.textColor = UIColor(userExamineeModel.textColor)
            textTaskLabel.text = userExamineeModel.task
            self.imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: userExamineeModel.avatar))
            
        }
    }
    
}
