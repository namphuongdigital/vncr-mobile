//
//  CommadDeviceTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 3/13/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit



class CommadDeviceTableViewCell: UITableViewCell {
    
    @IBOutlet weak var textTitleLabel: UILabel!
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    weak var commandModel: CommandModel? {
        didSet {
            if(commandModel != nil) {
                textTitleLabel.text = commandModel!.display
                textTitleLabel.textColor = UIColor(commandModel!.color)
                imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: commandModel!.imageUrl))
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.imageViewAvatar.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
