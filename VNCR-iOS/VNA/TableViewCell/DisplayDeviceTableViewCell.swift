//
//  DisplayDeviceTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/17/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell


protocol DisplayDeviceTableViewCellDelegate : NSObjectProtocol {
    func deleteDisplayDeviceTableViewCellPress(deviceModel: DeviceModel)
    func setMasterDisplayDeviceTableViewCellPress(deviceModel: DeviceModel)
}


class DisplayDeviceTableViewCell: MGSwipeTableCell {
    
    weak var displayDeviceTableViewCellDelegate: DisplayDeviceTableViewCellDelegate?
    
    @IBOutlet weak var textNameLabel: UILabel!
    
    weak var deviceModel: DeviceModel? {
        didSet {
            self.rightButtons.removeAll()
            if(deviceModel != nil) {
                textNameLabel.text = deviceModel!.displayName
                textNameLabel.textColor = UIColor(deviceModel!.textColor)
                self.addRightButtons()
            }
        }
    }

    var fontSize: CGFloat = 15.0
    var buttonWidth: CGFloat = 120.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
            buttonWidth = 60
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func addRightButtons() {
        
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        
        let button1: MGSwipeButton = MGSwipeButton(title: "Deactive".localizedString(), backgroundColor: UIColor.red, callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            if let deviceModel = self.deviceModel {
                self.displayDeviceTableViewCellDelegate?.deleteDisplayDeviceTableViewCellPress(deviceModel: deviceModel)
            }
            
            return true
        })
        button1.setImage(UIImage.init(named: "Deactivate.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button1.tintColor = UIColor.white
        button1.imageView?.contentMode = .scaleAspectFit
        button1.buttonWidth = buttonWidth
        button1.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button2: MGSwipeButton = MGSwipeButton(title: "Set master".localizedString(), backgroundColor: UIColor("#166880"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            if let deviceModel = self.deviceModel {
                self.displayDeviceTableViewCellDelegate?.setMasterDisplayDeviceTableViewCellPress(deviceModel: deviceModel)
            }
            
            return true
        })
        button2.setImage(UIImage.init(named: "SetMaster.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button2.tintColor = UIColor.white
        button2.imageView?.contentMode = .scaleAspectFit
        button2.buttonWidth = buttonWidth
        button2.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        self.rightButtons = [button1, button2]
        
    }
    
}
