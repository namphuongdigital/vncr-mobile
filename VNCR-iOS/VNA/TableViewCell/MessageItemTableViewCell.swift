//
//  MessageItemTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell

public enum ActionUpdateMessageItemType: Int {
    case done = 1
    case mark = 2
    case replyYes = 3
    case replyNo = 4
    case replyText = 5
    
}

protocol MessageItemTableViewCellDelegate : NSObjectProtocol {
    func actionMessageItemTableViewCell(messageModel: MessageModel, actionUpdateMessageItemType: ActionUpdateMessageItemType)
}

class MessageItemTableViewCell: MGSwipeTableCell {
    
    weak var delegateAction: MessageItemTableViewCellDelegate?
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    @IBOutlet weak var labelContent: UILabel!
    
    @IBOutlet weak var imageViewIconTime: UIImageViewProgress!
    
    @IBOutlet weak var labelTime: UILabel!
    
    weak var messageModel: MessageModel?
    
    var fontSize: CGFloat = 15.0
    
    var buttonWidth: CGFloat = 120.0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 14
            buttonWidth = 60
        }
        self.labelTime.font = UIFont.systemFont(ofSize: fontSize - 1)
        self.labelContent.font = UIFont.systemFont(ofSize: fontSize)
        self.imageViewAvatar.clipsToBounds = true
        //self.imageViewAvatar.layer.cornerRadius = 3.0
        //self.imageViewAvatar.layer.borderColor = UIColor("#dddddd").cgColor
        //self.imageViewAvatar.layer.borderWidth = 0.4
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadContent(messageModel: MessageModel, isAddActionButton: Bool = true) {
        self.messageModel = messageModel
        self.labelContent.text = String(format: "%@ %@ %@", messageModel.sender, messageModel.message, messageModel.poweredby)
        self.labelContent.attributedText = addBoldText(fullString: self.labelContent.text! as NSString, highlightPartsOfString: [messageModel.sender as NSString, messageModel.poweredby as NSString], font: UIFont.systemFont(ofSize: fontSize), highlightFont: UIFont.boldSystemFont(ofSize: fontSize))
        
        self.labelTime.text = messageModel.date//.dateTimeToDateTimeHHmm
        self.imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: messageModel.avatarUrl))
        if(isAddActionButton == true) {
            addLefttButtons()
            addRightButtons()
        } else {
            self.leftButtons.removeAll()
            self.rightButtons.removeAll()
        }
        
        if(messageModel.status.rawValue < 8) {
            self.contentView.subviews.first?.backgroundColor = UIColor("#e8f6eb")
        } else {
            self.contentView.subviews.first?.backgroundColor = UIColor.clear
        }
        /*
        if(messageModel.status == .unread) {
            self.backgroundColor = UIColor("#e8f6eb")
        } else {
            self.backgroundColor = UIColor.clear
        }
        */
    }
    
    func addBoldText(fullString: NSString, highlightPartsOfString: Array<NSString>, font: UIFont!, highlightFont: UIFont!) -> NSAttributedString {
        let fontAttribute = [NSAttributedString.Key.font: font!]
        let highlightFontAttribute = [NSAttributedString.Key.font: highlightFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: UIColor.black]
        let fullStringAttribute = NSMutableAttributedString(string: fullString as String, attributes:fontAttribute)
        for i in 0 ..< highlightPartsOfString.count {
            fullStringAttribute.addAttributes(highlightFontAttribute, range: fullString.range(of: highlightPartsOfString[i] as String))
            fullStringAttribute.addAttributes(lightColorAttribute, range: fullString.range(of: highlightPartsOfString[i] as String))
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        let attrString = NSMutableAttributedString(string: fullString as String)
        fullStringAttribute.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: NSMakeRange(0, attrString.length))
        
        return fullStringAttribute
    }
    
    
    func addLefttButtons() {
        
        self.leftButtons.removeAll()
        //configure right buttons
        self.leftSwipeSettings.transition = .clipCenter
        self.leftSwipeSettings.keepButtonsSwiped = false
        self.leftExpansion.expansionLayout = .center
        self.leftExpansion.buttonIndex = 0
        self.leftExpansion.threshold = 1.0
        self.leftExpansion.fillOnTrigger = true
        let button: MGSwipeButton  = MGSwipeButton(title: "Done".localizedString(), backgroundColor: UIColor.init(red: 33/255.0, green: 175/255.0, blue: 67/255.0, alpha: 1.0), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return false
            }
            guard let model = self.messageModel else {
                return false
            }
            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.done)
            return true
        })
        button.buttonWidth = buttonWidth
        button.titleLabel?.font = UIFont.systemFont(ofSize: fontSize + 2)
        self.leftButtons.append(button)
        
        
    }
    
    func addRightButtons() {
        
        self.rightButtons.removeAll()
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        let button3: MGSwipeButton  = MGSwipeButton(title: "No".localizedString(), backgroundColor: UIColor.red, callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let model = self.messageModel else {
                return true
            }
            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.replyNo)
            return false
        })
        button3.buttonWidth = buttonWidth
        button3.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button4: MGSwipeButton  = MGSwipeButton(title: "Yes".localizedString(), backgroundColor: UIColor("#dba510"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let model = self.messageModel else {
                return true
            }
            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.replyYes)
            return false
        })
        button4.buttonWidth = buttonWidth
        button4.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button5: MGSwipeButton  = MGSwipeButton(title: "Text".localizedString(), backgroundColor: UIColor("#006885"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let model = self.messageModel else {
                return true
            }
            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.replyText)
            return false
        })
        button5.buttonWidth = buttonWidth
        button5.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button6: MGSwipeButton  = MGSwipeButton(title: "Mark as read".localizedString(), backgroundColor: UIColor.lightGray, callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let model = self.messageModel else {
                return true
            }
            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.mark)
            return false
        })
        button6.buttonWidth = buttonWidth
        button6.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        self.rightButtons.append(button3)
        self.rightButtons.append(button4)
        self.rightButtons.append(button5)
        self.rightButtons.append(button6)
        
        
        
    }
    
}
