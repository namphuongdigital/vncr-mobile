//
//  InfoFlightReportTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/25/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit

class InfoFlightReportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelDateStart: UILabel!
    @IBOutlet weak var labelDateEnd: UILabel!
    @IBOutlet weak var labelTimeStart: UILabel!
    @IBOutlet weak var labelTimeEnd: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNum: UILabel!
    @IBOutlet weak var iconArrow: UILabel!
    
    weak var flightInfo: FlightInfo?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.iconArrow.setFAIcon(icon: .FAArrowRight, iconSize: 18)
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(flightInfo: FlightInfo)  {
        self.flightInfo = flightInfo
        self.labelName.text = flightInfo.routing
        self.labelNum.text = flightInfo.flightNo
        
        self.labelTimeStart.text = flightInfo.departedTime//flightInfo.departed.dateTimeToTimeHHmm
        self.labelDateStart.text = flightInfo.departedDate//flightInfo.departed.dateTimeToddMMyyyy
        
        self.labelTimeEnd.text = flightInfo.arrivedTime//flightInfo.arrived.dateTimeToTimeHHmm
        self.labelDateEnd.text = flightInfo.arrivedDate//flightInfo.arrived.dateTimeToddMMyyyy
        
    }

}
