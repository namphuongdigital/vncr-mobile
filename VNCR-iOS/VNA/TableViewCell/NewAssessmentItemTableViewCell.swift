//
//  NewAssessmentItemTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/14/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

protocol NewAssessmentItemTableViewCellDelegate : NSObjectProtocol {
    func scoreButtonPressNewAssessmentItemTableViewCell(cell: NewAssessmentItemTableViewCell, assessmentItemModel: AssessmentItemModel)
}

class NewAssessmentItemTableViewCell: UITableViewCell {
    
    weak var newAssessmentItemTableViewCellDelegate: NewAssessmentItemTableViewCellDelegate?
    
    var assessmentItemModel: AssessmentItemModel? {
        didSet {
            self.loadViewData()
        }
    }
    
    @IBOutlet weak var textNameLabel: UILabel!
    
    @IBOutlet weak var buttonScore: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        self.buttonScore.layer.cornerRadius = self.buttonScore.frame.height / 2.0
        self.buttonScore.layer.borderWidth = 0.8
        self.buttonScore.layer.borderColor = UIColor.darkGray.cgColor
        self.buttonScore.setTitleColor(UIColor.black, for: UIControl.State.normal)
        
        self.buttonScore.addTarget(self, action: #selector(self.buttonScorePressed(_:)), for: .touchUpInside)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func loadViewData() {
        if let assessmentItemModel = self.assessmentItemModel {
            textNameLabel.text = assessmentItemModel.question
            textNameLabel.textColor = UIColor(assessmentItemModel.textColor)
            self.buttonScore.setTitle(String(format: "%@", assessmentItemModel.rate), for: UIControl.State.normal)
            self.buttonScore.isHidden = !assessmentItemModel.isInput
            if(assessmentItemModel.isInput) {
                textNameLabel.font = UIFont.systemFont(ofSize: 14)
                self.backgroundColor = UIColor.white
            } else {
                textNameLabel.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
                self.backgroundColor = UIColor.groupTableViewBackground
            }
        }
    }
    
    @objc func buttonScorePressed(_ sender: UIButton) {
        if let assessmentItemModel = self.assessmentItemModel {
            self.newAssessmentItemTableViewCellDelegate?.scoreButtonPressNewAssessmentItemTableViewCell(cell: self, assessmentItemModel: assessmentItemModel)
        }
        
    }
    
}
