//
//  AssessmentTableViewHeaderFooterView.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class AssessmentTableViewHeaderFooterView: UITableViewHeaderFooterView {

    @IBOutlet weak var viewContainerHeader: UIView!
    @IBOutlet weak var viewHeaderName: UIView!
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override func awakeFromNib() {
        self.viewContainerHeader.backgroundColor = UIColor("#166880")
        self.viewHeaderName.backgroundColor = UIColor("#166880")
    }
    
    func configForPilot() {
        viewContainerHeader.subviews.filter({$0.isKind(of: UILabel.self)}).forEach({$0.alpha = 0})
    }
    
    override func prepareForReuse() {
        viewContainerHeader.subviews.filter({$0.isKind(of: UILabel.self)}).forEach({$0.alpha = 1})
    }
}
