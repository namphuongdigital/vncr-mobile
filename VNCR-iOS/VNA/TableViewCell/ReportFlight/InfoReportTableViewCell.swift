//
//  InfoReportTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/13/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class InfoReportTableViewCell: UITableViewCell {

    weak var scheduleFlyModel: ScheduleFlightModel?
    
    @IBOutlet weak var viewLine: UIView!
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNum: UILabel!
    @IBOutlet weak var labelDateStart: UILabel!
    @IBOutlet weak var labelTimeStart: UILabel!
    
    @IBOutlet weak var labelPax: UILabel!
    @IBOutlet weak var labelC: UILabel!
    @IBOutlet weak var labelY: UILabel!
    @IBOutlet weak var labelVIP: UILabel!
    
    @IBOutlet weak var labelInf: UILabel!
    @IBOutlet weak var labelUM: UILabel!
    @IBOutlet weak var labelSpecialMeal: UILabel!
    @IBOutlet weak var labelAircraft: UILabel!
    @IBOutlet weak var labelCIP: UILabel!
    @IBOutlet weak var labelBSCT: UILabel!
    @IBOutlet weak var labelWCHC: UILabel!
    
    var fontSize: CGFloat = 15.0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewLine.backgroundColor = UIColor.lightGray
        viewLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: UIScreen.main.bounds.size.width, height: 0.8)
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
        }
        
        labelTitle?.text = ""//"Detail".localizedString()
        
        labelPax.isHidden = true
        labelC.isHidden = true
        labelY.isHidden = true
        labelVIP.isHidden = true
        labelInf.isHidden = true
        labelUM.isHidden = true
        labelSpecialMeal.isHidden = true
        labelAircraft.isHidden = true
        labelCIP.isHidden = true
        labelBSCT.isHidden = true
        labelWCHC.isHidden = true
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.labelDateStart.textColor = UIColor("#cc9e73")
        self.labelName.textColor = UIColor("#166880")
        self.labelNum.textColor = UIColor("#3bae8c")
    }
    
    func loadData(scheduleFlyModel: ScheduleFlightModel)  {
        self.scheduleFlyModel = scheduleFlyModel
        
        self.labelName.text = scheduleFlyModel.routing
        self.labelNum.text = scheduleFlyModel.flightNo
        
        self.labelTimeStart.text = scheduleFlyModel.departedTime//scheduleFlyModel.departed.dateTimeToTimeHHmm
        self.labelDateStart.text = scheduleFlyModel.departedDate//scheduleFlyModel.departed.dateTimeToddMMyyyy
        
        self.labelAircraft.text = String.init(format: "Aircraft : %@", scheduleFlyModel.aircraft)
        self.labelAircraft.attributedText = addBoldText(fullString: self.labelAircraft.text! as NSString, boldPartsOfString: ["Aircraft :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        
//        self.labelPax.text = String.init(format: "Pax : %@", String(format: "%d", scheduleFlyModel.totalPax))
//        self.labelPax.attributedText = addBoldText(fullString: self.labelPax.text! as NSString, boldPartsOfString: ["Pax :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelC.text = String.init(format: "C : %d", scheduleFlyModel.c)
//        self.labelC.attributedText = addBoldText(fullString: self.labelC.text! as NSString, boldPartsOfString: ["C :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelY.text = String.init(format: "Y : %d", scheduleFlyModel.y)
//        self.labelY.attributedText = addBoldText(fullString: self.labelY.text! as NSString, boldPartsOfString: ["Y :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelVIP.text = String.init(format: "VIP : %d", scheduleFlyModel.VIP)
//        self.labelVIP.attributedText = addBoldText(fullString: self.labelVIP.text! as NSString, boldPartsOfString: ["VIP :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelCIP.text = String.init(format: "CIP : %d", scheduleFlyModel.CIP)
//        self.labelCIP.attributedText = addBoldText(fullString: self.labelCIP.text! as NSString, boldPartsOfString: ["CIP :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelInf.text = String.init(format: "Inf : %d", scheduleFlyModel.inf)
//        self.labelInf.attributedText = addBoldText(fullString: self.labelInf.text! as NSString, boldPartsOfString: ["Inf :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelUM.text = String.init(format: "UM : %d", scheduleFlyModel.um)
//        self.labelUM.attributedText = addBoldText(fullString: self.labelUM.text! as NSString, boldPartsOfString: ["UM :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelBSCT.text = String.init(format: "BSCT : %d", scheduleFlyModel.BSCT)
//        self.labelBSCT.attributedText = addBoldText(fullString: self.labelBSCT.text! as NSString, boldPartsOfString: ["BSCT :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelSpecialMeal.text = String.init(format: "Special meal : %d", scheduleFlyModel.specialMeal)
//        self.labelSpecialMeal.attributedText = addBoldText(fullString: self.labelSpecialMeal.text! as NSString, boldPartsOfString: ["Special meal :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelWCHC.text = String.init(format: "WCHC : %d", scheduleFlyModel.WCHC)
        self.labelWCHC.attributedText = addBoldText(fullString: self.labelWCHC.text! as NSString, boldPartsOfString: ["WCHC :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        
        
    }
    
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: font!]
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: UIColor.black]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        
        
        return boldString
    }
    
    
}
