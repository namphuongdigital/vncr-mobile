//
//  UpdateReportTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/15/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class UpdateReportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var buttonUpdate: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        buttonUpdate.setTitle("Update report".localizedString(), for: UIControl.State())
        buttonUpdate.backgroundColor = UIColor("#166880")
        buttonUpdate.layer.cornerRadius = 3.0
        buttonUpdate.layer.borderWidth = 0.0
        buttonUpdate.clipsToBounds = true
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            buttonUpdate.titleLabel?.font = UIFont.systemFont(ofSize: 12)
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
