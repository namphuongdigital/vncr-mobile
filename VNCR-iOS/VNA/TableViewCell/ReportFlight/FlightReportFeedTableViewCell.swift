//
//  FlightReportFeedTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/16/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import DateTools

protocol FlightReportFeedTableViewCellDelegate: NSObjectProtocol {
    func buttonMoreCellPress(reportModel: ReportModel)
    
}

class FlightReportFeedTableViewCell: UITableViewCell {
    
    @IBOutlet weak var buttonAction: UIButton!
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelCategory: UILabel!
    
    @IBOutlet weak var labelContent: ExpandableLabel!
    
    @IBOutlet weak var labelPilot: UILabel!
    
    @IBOutlet weak var labelEmergency: UILabel!
    
    @IBOutlet weak var viewEmergency: UIView!
    
    @IBOutlet weak var listImageView: ListImageView!
    
    @IBOutlet weak var heightLabelPilot: NSLayoutConstraint!
    
    /*
    @IBOutlet weak var heightLabelCategory: NSLayoutConstraint!
    
    @IBOutlet weak var heightLabelContent: NSLayoutConstraint!
    
    @IBOutlet weak var heightLabelPilot: NSLayoutConstraint!
    */
    @IBOutlet weak var heightListImageView: NSLayoutConstraint!
    
    weak var reportModel: ReportModel?
    
    weak var delegate: FlightReportFeedTableViewCellDelegate?
    
    override func prepareForReuse() {
        super.prepareForReuse()
        labelContent.collapsed = true
        labelContent.text = nil
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.labelContent.numberOfLines = 4
        
        self.buttonAction.setFAIcon(icon: .FAChevronDown, iconSize: 10, forState: UIControl.State())
        //self.imageViewAvatar.layer.backgroundColor = UIColor.init(white: 0.886, alpha: 1.000).cgColor
        self.imageViewAvatar.clipsToBounds = true
        self.imageViewAvatar.layer.cornerRadius = 3.0
        self.imageViewAvatar.layer.borderColor = UIColor("#dddddd").cgColor
        self.imageViewAvatar.layer.borderWidth = 0.4
        
        viewEmergency.backgroundColor = UIColor.clear
        self.viewEmergency.clipsToBounds = true
        self.viewEmergency.layer.cornerRadius = 2.0
        self.viewEmergency.layer.borderColor = UIColor("#dddddd").cgColor
        self.viewEmergency.layer.borderWidth = 0.6
        
        self.labelName.textColor = UIColor("#166880")
        
        self.buttonAction.layer.cornerRadius = self.buttonAction.frame.size.width / 2.0
        self.buttonAction.layer.borderColor = UIColor("#dddddd").cgColor
        self.buttonAction.layer.borderWidth = 0.6
        
        self.buttonAction.addTarget(self, action: #selector(buttonActionPress(sender:)), for: .touchUpInside)
        
        self.labelPilot.textColor = UIColor("#166880")
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func buttonActionPress(sender: UIButton) {
        guard let reportModel = reportModel else {
            return
        }
        self.delegate?.buttonMoreCellPress(reportModel: reportModel)
    }
    
    func loadContent(reportModel: ReportModel? = nil) {
        if(reportModel == nil) {
            self.labelCategory.text = ""
            self.labelContent.text = ""
        } else {
            self.reportModel = reportModel
            guard let reportModel = reportModel else {
                return
            }
            if(reportModel.isCanUpdate == false && reportModel.isCanDelete == false) {
                self.buttonAction.isHidden = true
            } else {
                self.buttonAction.isHidden = false
            }
            
            self.imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: reportModel.avatarURL))
            self.labelName.text = reportModel.fullName
            self.labelDate.text = reportModel.created
            if(reportModel.comment.count > 0) {
                self.labelContent.text = String(format: "%@\n\n%@", reportModel.content, reportModel.comment)
                self.labelContent.attributedText = addBoldText(fullString: self.labelContent.text! as NSString, boldPartsOfString: [reportModel.comment as NSString], font: self.labelContent.font, boldFont: self.labelContent.font, boldColor: UIColor("#dba510"))
            } else {
                self.labelContent.text = String(format: "%@", reportModel.content)
            }
            self.labelPilot.text = reportModel.pilot
            if(reportModel.emergency == .slow) {
                self.labelEmergency.text = String(format: "%@", "Low".localizedString())
            } else if(reportModel.emergency == .medium) {
                self.labelEmergency.text = String(format: "%@", "Normal".localizedString())
            } else if(reportModel.emergency == .high) {
                self.labelEmergency.text = String(format: "%@", "High".localizedString())
            }
            var stringName = ""
            for item in reportModel.categories {
                for sub in item.subCatetories {
                    if(sub.isSelected) {
                        if(stringName.count > 0) {
                            stringName = String(format: "%@ - %@", stringName, sub.subCategoryName)
                        } else {
                            stringName = String(format: "%@", sub.subCategoryName)
                        }
                    }
                }
                
            }
            self.labelCategory.text = stringName
            if(reportModel.pilot.count == 0) {
                heightLabelPilot.constant = 0
            }
            /*
            if(stringName.characters.count == 0) {
                heightLabelCategory.constant = 0
            }
            if(reportModel.pilot.characters.count == 0) {
                heightLabelPilot.constant = 0
            }
            if(reportModel.content.characters.count == 0) {
                heightLabelContent.constant = 0
            }
            */
            self.listImageView.clearContent()
            self.listImageView.listImagePost.removeAll()
            loadImage()
            
            
        
        }
        
    }
    
    func loadImage() {
        var array = Array<ImagePost>()
        
        if let attachments = self.reportModel?.attachments {
            for index in 0..<attachments.count {
                let image = ImagePost(named: "")
                image.isImagePost = true
                image.index = index
                image.isHiddenButtonDelete = true
                image.imageUrl = attachments[index].thumbnailPath
                image.imageFullUrl = attachments[index].downloadPath
                array.append(image)
            }
        }
        if(self.reportModel?.id == 19887) {
            print("")
        }
        self.listImageView.loadContent(listImagePost: array)
        if(array.count == 0) {
            self.heightListImageView.constant = 0
        } else {
            self.heightListImageView.constant = 100
        }
    }
    
    func setTextForContent(content: String, comment: String) {
        if(comment.count > 0) {
            let text = String(format: "%@\n\n%@", content, comment)
            self.labelContent.attributedText = addBoldText(fullString: text as NSString, boldPartsOfString: [comment as NSString], font: self.labelContent.font, boldFont: self.labelContent.font, boldColor: UIColor("#dba510"))
        } else {
            self.labelContent.text = String(format: "%@", content)
        }
        
    }
    
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!, boldColor: UIColor) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: font!]
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: boldColor]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        
        
        return boldString
    }

}
