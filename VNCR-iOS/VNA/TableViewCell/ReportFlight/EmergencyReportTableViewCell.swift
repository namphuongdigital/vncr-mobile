//
//  EmergencyReportTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/13/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import StepSlider

class EmergencyReportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewLine: UIView!
    
    @IBOutlet weak var sliderView: StepSlider!
    
    @IBOutlet weak var labelValueLow: UILabel!
    
    @IBOutlet weak var labelValueMedium: UILabel!
    
    @IBOutlet weak var labelValueHigh: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewLine.backgroundColor = UIColor.lightGray
        viewLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: UIScreen.main.bounds.size.width, height: 0.8)
        
        //labelValue.textColor = UIColor("#166880")
        sliderView.trackColor = UIColor("#166880")
        sliderView.tintColor = UIColor("#166880")
        sliderView.sliderCircleColor = UIColor("#166880")
        sliderView.maxCount = 3
        sliderView.isDotsInteractionEnabled = false
        sliderView.sliderCircleColor = UIColor("#dba510")
        sliderView.addTarget(self, action: #selector(changeValue(sender:)), for: .valueChanged)
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
        }
        labelValueLow.text = "Low".localizedString()
        labelValueMedium.text = "Normal".localizedString()
        labelValueHigh.text = "High".localizedString()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func changeValue(sender: StepSlider) {
        //labelValue.text = String(format: "%d %@", sliderView.index * 10, "%")
    }
    
}
