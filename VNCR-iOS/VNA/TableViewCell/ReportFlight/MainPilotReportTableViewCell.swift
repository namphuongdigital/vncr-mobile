//
//  MainPilotReportTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/13/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class MainPilotReportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var viewLine: UIView!

    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var pilotNameTextFiled: AutoCompleteTextField!
    
    var countriesList = Array<String>()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewLine.backgroundColor = UIColor.lightGray
        viewLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: UIScreen.main.bounds.size.width, height: 0.8)
        
        configureTextField()
        handleTextFieldInterfaces()
        
        labelTitle.text = "Pilot".localizedString()
        pilotNameTextFiled.placeholder = "Enter name pilot".localizedString()
     
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        
    }
    
    
    
    func configureTextField(){
     
        var attributes = [NSAttributedString.Key: AnyObject]()
        attributes[NSAttributedString.Key.foregroundColor] = UIColor.black
        
        pilotNameTextFiled.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            pilotNameTextFiled.autoCompleteCellHeight = 35.0
            pilotNameTextFiled.autoCompleteTextFont = UIFont.boldSystemFont(ofSize: 14)
            attributes[NSAttributedString.Key.font] = UIFont.boldSystemFont(ofSize: 14)
        } else {
            pilotNameTextFiled.autoCompleteCellHeight = 50.0
            pilotNameTextFiled.autoCompleteTextFont = UIFont.boldSystemFont(ofSize: 20)
            attributes[NSAttributedString.Key.font] = UIFont.boldSystemFont(ofSize: 20)
        }
        pilotNameTextFiled.maximumAutoCompleteCount = 20
        pilotNameTextFiled.hidesWhenSelected = true
        pilotNameTextFiled.hidesWhenEmpty = true
        pilotNameTextFiled.enableAttributedText = true
        
        
        pilotNameTextFiled.autoCompleteAttributes = attributes
        
    }
    
    func handleTextFieldInterfaces(){
        pilotNameTextFiled.onTextChange = {[weak self] text in
            if !text.isEmpty{
                
                ServiceData.sharedInstance.taskFlightPilotGetList(position: .main).continueOnSuccessWith(continuation: { task in
                    if let array = task as? Array<PilotModel> {
                        var arrayString = Array<String>()
                        for item in array {
                            arrayString.append(item.fullName)
                        }
                        self?.pilotNameTextFiled.autoCompleteStrings = arrayString
                    }
                    
                    
                }).continueOnErrorWith(continuation: { error in
                    print(error)
                })
                
            }
            
            
        }
        
        pilotNameTextFiled.onSelect = {[weak self] text, indexpath in
            print("")
        }
    }
    
}

