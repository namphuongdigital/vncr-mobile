//
//  PostCollectionViewCell.swift
//  MBN-iOS-App
//
//  Created by Van Trieu Phu Huy on 11/9/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import UIKit

@objc protocol PostCollectionViewCellDelegate: NSObjectProtocol {
    @objc optional func buttonRemoveImageCellPress(cell: UICollectionViewCell)
    @objc optional func buttonImageViewImageCellPress(imagePost: ImagePost)
}

class PostCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: PostCollectionViewCellDelegate?
    
    weak var imagePost: ImagePost!
    
    @IBOutlet weak var imageViewPost: UIImageViewProgress!
    
    @IBOutlet weak var buttonRemove: UIButton!
    
    @IBOutlet weak var buttonImageView: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.buttonRemove.setTitleColor(UIColor.red, for: .normal)
        self.buttonRemove.setFAIcon(icon: .FARemove, forState: .normal)
        //self.buttonRemove.layer.cornerRadius = self.buttonRemove.frame.size.height / 2.0
        self.buttonRemove.addTarget(self, action: #selector(self.buttonRemoveClick(sender:)), for: .touchUpInside)
        
        self.buttonImageView.addTarget(self, action: #selector(self.buttonImageViewClick(sender:)), for: .touchUpInside)
        
        self.imageViewPost.layer.backgroundColor = UIColor.white.cgColor
        self.imageViewPost.layer.cornerRadius = 1.5
        self.imageViewPost.clipsToBounds = true
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }
    
    
    
    func loadContent(imagePost: ImagePost) {
        self.imagePost = imagePost
        if(imagePost.isHiddenButtonDelete) {
            self.buttonRemove.isHidden = true
            if(imagePost.imageUrl != "") {
                self.imagePost.isImagePost = true
                self.buttonRemove.isHidden = true
                //self.imageViewPost.cancelLoadImageProgress()
                self.imageViewPost.sd_setImage(with: URL.init(string: imagePost.imageUrl), completed: {[weak self] (image, error, cacheType, url) in
                    if(error == nil) {
                        
                        self?.imageViewPost.image = image
                        imagePost.image = image
                    } else {
                        self?.imageViewPost.image = nil
                        imagePost.image = nil
                    }
                    
                })
            } else {
                self.imageViewPost.image = imagePost.image
            }
        } else {
            if(imagePost.imageUrl != "") {
                self.imagePost.isImagePost = true
                self.buttonRemove.isHidden = true
                //self.imageViewPost.cancelLoadImageProgress()
                self.imageViewPost.sd_setImage(with: URL.init(string: imagePost.imageUrl), completed: {[weak self] (image, error, cacheType, url) in
                    if(error == nil) {
                        self?.buttonRemove.isHidden = false
                        self?.imageViewPost.image = image
                        imagePost.image = image
                    } else {
                        self?.buttonRemove.isHidden = true
                        self?.imageViewPost.image = nil
                        imagePost.image = nil
                    }
                    
                })
            } else {
                if(self.imagePost.isImagePost == true) {
                    self.buttonRemove.isHidden = false
                } else {
                    self.buttonRemove.isHidden = true
                }
                self.imageViewPost.image = imagePost.image
            }
        }
        
    }
    
    @objc func buttonRemoveClick(sender: UIButton) {
        guard self.imagePost != nil else {
            return
        }
        delegate?.buttonRemoveImageCellPress?(cell: self)
    }
    
    @objc func buttonImageViewClick(sender: UIButton) {
        guard self.imagePost != nil else {
            return
        }
        delegate?.buttonImageViewImageCellPress?(imagePost: self.imagePost)
    }
    
    override func prepareForReuse() {
        imageViewPost.image = nil
    }

}
