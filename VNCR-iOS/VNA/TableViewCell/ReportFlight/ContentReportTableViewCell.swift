//
//  ContentReportTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 1/14/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


class ContentReportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var viewLine: UIView!
    
    @IBOutlet weak var labelContent: UILabel!
    
    @IBOutlet weak var textViewContent: IQTextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewLine.backgroundColor = UIColor.lightGray
        viewLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: UIScreen.main.bounds.size.width, height: 0.8)
        
        textViewContent.placeholder = "Content report".localizedString()
        labelTitle.text = "Content".localizedString()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
