//
//  PersonalExpanedScheduleFlyTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/25/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell

protocol ScheduleFlyTableViewCellDelegate : NSObjectProtocol {
    func reportPositonScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel)
    func reportFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel)
    func assessmentFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel)
    func surveyListFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel)
    func ojtListFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlightModel)
}

class PersonalExpanedScheduleFlyTableViewCell: MGSwipeTableCell, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView?
    
    @IBOutlet weak var buttonReportFly: MIBadgeButton!
    @IBOutlet weak var buttonReportPostion: MIBadgeButton!
    @IBOutlet weak var buttonReportMultiRate: MIBadgeButton!
    @IBOutlet weak var labelFullName: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNum: UILabel!
    @IBOutlet weak var labelDateTimeStart: UILabel!
    @IBOutlet weak var labelPax: UILabel!
    @IBOutlet weak var labelC: UILabel!
    @IBOutlet weak var labelVIP: UILabel!
    @IBOutlet weak var labelInf: UILabel!
    @IBOutlet weak var labelUM: UILabel!
    @IBOutlet weak var labelSpecialMeal: UILabel!
    @IBOutlet weak var labelAircraft: UILabel!
    @IBOutlet weak var labelY: UILabel!
    @IBOutlet weak var labelCIP: UILabel!
    @IBOutlet weak var labelBSCT: UILabel!
    @IBOutlet weak var labelWCHC: UILabel!
    
    @IBOutlet weak var labelNote: UILabel!
    
    @IBOutlet weak var labelListFull: UILabel?
    
    var fontSize: CGFloat = 15.0
    var buttonWidth: CGFloat = 120.0
    
    weak var delegateAction: ScheduleFlyTableViewCellDelegate?
    
    weak var scheduleFly: ScheduleFlightModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
            buttonWidth = 60
        }
        
        self.buttonReportFly.setFAIcon(icon: .FAPlane, forState: .normal)
        self.buttonReportFly.setTitleColor(UIColor("#006482"), for: .normal)
        self.buttonReportPostion.setFAIcon(icon: .FAUser, forState: .normal)
        self.buttonReportPostion.setTitleColor(UIColor("#006482"), for: .normal)
        self.buttonReportMultiRate.setFAIcon(icon: .FAListAlt, forState: .normal)
        self.buttonReportMultiRate.setTitleColor(UIColor("#006482"), for: .normal)
        
        self.buttonReportFly.badgeBackgroundColor = UIColor("#FFCC00")
        self.buttonReportPostion.badgeBackgroundColor = UIColor("#FFCC00")
        self.buttonReportMultiRate.badgeBackgroundColor = UIColor("#FFCC00")
        self.labelNote.textColor = UIColor.black
        
        self.labelDateTimeStart.textColor = UIColor("#cc9e73")
        
        self.labelName.textColor = UIColor("#166880")
        self.labelNum.textColor = UIColor("#3bae8c")//UIColor("#6d6b6c")
        
        self.labelFullName.textColor = UIColor("#166880")
        self.labelListFull?.textColor = UIColor("#cc9e73")
        
        self.tableView?.register(UINib(nibName: "TripInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "TripInfoTableViewCell")
        self.tableView?.backgroundColor = UIColor.clear
        self.tableView?.isUserInteractionEnabled = false
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(scheduleFly == nil) {
            return 0
        } else {
            return scheduleFly?.trips.details.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripInfoTableViewCell")! as! TripInfoTableViewCell
        let title = scheduleFly!.trips.details[indexPath.row]
        cell.labelTitle.text = title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView?.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 24
        
        
    }
    
    //
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if(selected == true) {
            self.backgroundColor = UIColor("#fafffb")
        } else {
            self.backgroundColor = UIColor.clear
        }
        
    }
    
    func loadData(scheduleFlyModel: ScheduleFlightModel, indexPath: IndexPath)  {
        self.scheduleFly = scheduleFlyModel
        
        self.tableView?.reloadData()
        
        self.labelName.text = scheduleFlyModel.routing
        self.labelNum.text = scheduleFlyModel.flightNo
        self.labelNum.textColor = UIColor(scheduleFlyModel.fcolor)
        
        if(scheduleFlyModel.trips.routing == "") {
            self.labelFullName.text = scheduleFlyModel.routing
        } else {
            self.labelFullName.text = scheduleFlyModel.trips.routing
        }
        //Top
        self.buttonReportFly.badgeString = scheduleFlyModel.icon1Top
        self.buttonReportPostion.badgeString = scheduleFlyModel.icon2Top
        self.buttonReportMultiRate.badgeString = scheduleFlyModel.icon3Top
        
        self.buttonReportFly.badgeBackgroundColor = UIColor(scheduleFlyModel.icon1State)
        self.buttonReportPostion.badgeBackgroundColor = UIColor(scheduleFlyModel.icon2State)
        self.buttonReportMultiRate.badgeBackgroundColor = UIColor(scheduleFlyModel.icon3State)
        
        self.buttonReportFly.badgeTextColor = UIColor(scheduleFlyModel.icon1TextColor)
        self.buttonReportPostion.badgeTextColor = UIColor(scheduleFlyModel.icon2TextColor)
        self.buttonReportMultiRate.badgeTextColor = UIColor(scheduleFlyModel.icon3TextColor)
        
        //Footer
        self.buttonReportFly.badgeStringFooter = scheduleFlyModel.icon1Bot
        self.buttonReportPostion.badgeStringFooter = scheduleFlyModel.icon2Bot
        self.buttonReportMultiRate.badgeStringFooter = scheduleFlyModel.icon3Bot
        
        self.buttonReportFly.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon1StateBot)
        self.buttonReportPostion.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon2StateBot)
        self.buttonReportMultiRate.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon3StateBot)
        
        self.buttonReportFly.badgeTextColorFooter = UIColor(scheduleFlyModel.icon1TextColorBot)
        self.buttonReportPostion.badgeTextColorFooter = UIColor(scheduleFlyModel.icon2TextColorBot)
        self.buttonReportMultiRate.badgeTextColorFooter = UIColor(scheduleFlyModel.icon3TextColorBot)
        
        self.labelNote.text = scheduleFlyModel.note
        
        self.labelDateTimeStart.text = String(format: "%@ %@", scheduleFlyModel.departedTime, scheduleFlyModel.departedDate)//scheduleFlyModel.departed.dateTimeToHHmmddMMyyyy
        self.labelAircraft.text = String.init(format: "%@", scheduleFlyModel.aircraft)
        self.labelPax.text = String.init(format: "Pax : %@", String(format: "%d", scheduleFlyModel.totalPax))
        self.labelPax.attributedText = addBoldText(fullString: self.labelPax.text! as NSString, boldPartsOfString: ["Pax :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelC.text = String.init(format: "C : %d", scheduleFlyModel.totalPaxC)
        self.labelC.attributedText = addBoldText(fullString: self.labelC.text! as NSString, boldPartsOfString: ["C :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelY.text = String.init(format: "Y : %d", scheduleFlyModel.totalPaxY)
        self.labelY.attributedText = addBoldText(fullString: self.labelY.text! as NSString, boldPartsOfString: ["Y :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelVIP?.text = String.init(format: "VIP : %d", scheduleFlyModel.totalVIP)
        self.labelVIP?.attributedText = addBoldText(fullString: self.labelVIP.text! as NSString, boldPartsOfString: ["VIP :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelCIP.text = String.init(format: "CIP : %d", scheduleFlyModel.totalCIP)
        self.labelCIP.attributedText = addBoldText(fullString: self.labelCIP.text! as NSString, boldPartsOfString: ["CIP :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelInf.text = String.init(format: "Inf : %d", scheduleFlyModel.totalINF)
        self.labelInf.attributedText = addBoldText(fullString: self.labelInf.text! as NSString, boldPartsOfString: ["Inf :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelUM.text = String.init(format: "UM : %d", scheduleFlyModel.totalUM)
        self.labelUM.attributedText = addBoldText(fullString: self.labelUM.text! as NSString, boldPartsOfString: ["UM :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelBSCT.text = String.init(format: "BSC : %d", scheduleFlyModel.totalBSCT)
        self.labelBSCT.attributedText = addBoldText(fullString: self.labelBSCT.text! as NSString, boldPartsOfString: ["BSC :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelSpecialMeal.text = String.init(format: "SM : %d", scheduleFlyModel.totalSM)
        self.labelSpecialMeal.attributedText = addBoldText(fullString: self.labelSpecialMeal.text! as NSString, boldPartsOfString: ["SM :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelWCHC.text = String.init(format: "WCH : %d", scheduleFlyModel.totalWchr)
        self.labelWCHC.attributedText = addBoldText(fullString: self.labelWCHC.text! as NSString, boldPartsOfString: ["WCH :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        if(scheduleFlyModel.selectTripYN) {
            self.addRightButtons()
        } else {
            self.rightButtons.removeAll()
        }
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            var string = ""
            if(scheduleFly!.trips.details.count > 0) {
                for item in scheduleFly!.trips.details {
                    string += String(format: "%@\n", item)
                    
                }
                
            }
            self.labelListFull!.text = string
            
        }
        
    }
    
    func addRightButtons() {
        
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        let button3: MGSwipeButton  = MGSwipeButton(title: NSLocalizedString("Report position multi flight", comment: ""), backgroundColor: UIColor("#dba510"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.reportPositonScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return false
        })
        button3.setImage(UIImage.init(named: "task.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button3.tintColor = UIColor.white
        button3.imageView?.contentMode = .scaleAspectFit
        button3.buttonWidth = buttonWidth
        button3.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        let button2: MGSwipeButton = MGSwipeButton(title: NSLocalizedString("Report multi flight", comment: ""),backgroundColor: UIColor.lightGray, callback: {[weak self] (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.reportFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        button2.setImage(UIImage.init(named: "Flight Report.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button2.tintColor = UIColor.white
        button2.imageView?.contentMode = .scaleAspectFit
        button2.buttonWidth = buttonWidth
        button2.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button1: MGSwipeButton = MGSwipeButton(title: NSLocalizedString("Multidimensional assessment journey", comment: ""),backgroundColor: UIColor("#33cc90"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.assessmentFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        button1.setImage(UIImage.init(named: "Assessment.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button1.tintColor = UIColor.white
        button1.imageView?.contentMode = .scaleAspectFit
        button1.buttonWidth = buttonWidth
        button1.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        //
        self.rightButtons = [button1, button2, button3]
        
        
        
    }
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: font!]
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: UIColor.black]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        return boldString
    }

}
