//
//  ScheduleFlyTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/29/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class ScheduleFlyTableViewCell: MGSwipeTableCell {
    
    @IBOutlet weak var buttonReportFly: MIBadgeButton!
    @IBOutlet weak var buttonReportPostion: MIBadgeButton!
    @IBOutlet weak var buttonReportMultiRate: MIBadgeButton!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNum: UILabel!
    @IBOutlet weak var labelDateStart: UILabel!
    @IBOutlet weak var labelTimeStart: UILabel!
    @IBOutlet weak var labelPax: UILabel!
    @IBOutlet weak var labelC: UILabel!
    @IBOutlet weak var labelY: UILabel!
    @IBOutlet weak var labelVIP: UILabel!
    
    weak var delegateAction: ScheduleFlyTableViewCellDelegate?
    
    var fontSize: CGFloat = 15.0
    var buttonWidth: CGFloat = 120.0
    
    
    var scheduleFly: ScheduleFlyModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
            buttonWidth = 60
        }
        
        self.buttonReportFly.setFAIcon(icon: .FAPlane, forState: .normal)
        self.buttonReportFly.setTitleColor(UIColor("#006482"), for: .normal)
        self.buttonReportPostion.setFAIcon(icon: .FAUser, forState: .normal)
        self.buttonReportPostion.setTitleColor(UIColor("#006482"), for: .normal)
        self.buttonReportMultiRate.setFAIcon(icon: .FAListAlt, forState: .normal)
        self.buttonReportMultiRate.setTitleColor(UIColor("#006482"), for: .normal)
        
        
        self.labelDateStart.textColor = UIColor("#cc9e73")
        
        self.labelName.textColor = UIColor("#166880")
        self.labelNum.textColor = UIColor("#3bae8c")//UIColor("#6d6b6c")
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if(selected == true) {
            self.backgroundColor = UIColor("#dbd8d9")
        } else {
            self.backgroundColor = UIColor.clear
        }
        
    }
    
    func loadData(scheduleFlyModel: ScheduleFlyModel, indexPath: IndexPath)  {
        self.scheduleFly = scheduleFlyModel
        
        self.labelName.text = scheduleFlyModel.routing
        self.labelNum.text = scheduleFlyModel.flightNo
        
        //Top
        
        self.buttonReportFly.badgeString = scheduleFlyModel.icon1Top
        self.buttonReportPostion.badgeString = scheduleFlyModel.icon2Top
        self.buttonReportMultiRate.badgeString = scheduleFlyModel.icon3Top
        
        self.buttonReportFly.badgeBackgroundColor = UIColor(scheduleFlyModel.icon1State)
        self.buttonReportPostion.badgeBackgroundColor = UIColor(scheduleFlyModel.icon2State)
        self.buttonReportMultiRate.badgeBackgroundColor = UIColor(scheduleFlyModel.icon3State)
        
        self.buttonReportFly.badgeTextColor = UIColor(scheduleFlyModel.icon1TextColor)
        self.buttonReportPostion.badgeTextColor = UIColor(scheduleFlyModel.icon2TextColor)
        self.buttonReportMultiRate.badgeTextColor = UIColor(scheduleFlyModel.icon3TextColor)
        //Footer
        self.buttonReportFly.badgeStringFooter = scheduleFlyModel.icon1Bot
        self.buttonReportPostion.badgeStringFooter = scheduleFlyModel.icon2Bot
        self.buttonReportMultiRate.badgeStringFooter = scheduleFlyModel.icon3Bot
        
        self.buttonReportFly.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon1StateBot)
        self.buttonReportPostion.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon2StateBot)
        self.buttonReportMultiRate.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon3StateBot)
        
        self.buttonReportFly.badgeTextColorFooter = UIColor(scheduleFlyModel.icon1TextColorBot)
        self.buttonReportPostion.badgeTextColorFooter = UIColor(scheduleFlyModel.icon2TextColorBot)
        self.buttonReportMultiRate.badgeTextColorFooter = UIColor(scheduleFlyModel.icon3TextColorBot)
        
        self.labelTimeStart.text = scheduleFlyModel.departedTime
        self.labelDateStart.text = scheduleFlyModel.departedDate
        
        self.labelPax.text = String.init(format: "Pax : %@", String(format: "%d", scheduleFlyModel.totalPax))
        self.labelPax.attributedText = addBoldText(fullString: self.labelPax.text! as NSString, boldPartsOfString: ["Pax :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelC.text = String.init(format: "C : %d", scheduleFlyModel.c)
        self.labelC.attributedText = addBoldText(fullString: self.labelC.text! as NSString, boldPartsOfString: ["C :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelY.text = String.init(format: "Y : %d", scheduleFlyModel.y)
        self.labelY.attributedText = addBoldText(fullString: self.labelY.text! as NSString, boldPartsOfString: ["Y :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelVIP.text = String.init(format: "VIP : %d", scheduleFlyModel.VIP)
        self.labelVIP.attributedText = addBoldText(fullString: self.labelVIP.text! as NSString, boldPartsOfString: ["VIP :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        
        self.addRightButtons()
    }
    
    func addRightButtons() {
        
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        let button1: MGSwipeButton  = MGSwipeButton(title: "Report position".localizedString(), backgroundColor: UIColor("#dba510"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.reportPositonScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return false
        })
        button1.buttonWidth = buttonWidth
        button1.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        let button2: MGSwipeButton = MGSwipeButton(title: "Report single flight".localizedString(), backgroundColor: UIColor.lightGray, callback: {[weak self] (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.reportFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        button2.buttonWidth = buttonWidth
        button2.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button3: MGSwipeButton = MGSwipeButton(title: "Multidimensional assessment single flight".localizedString(), backgroundColor: UIColor("#33cc90"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.assessmentFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        button3.buttonWidth = buttonWidth
        button3.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        /*
        let button4: MGSwipeButton = MGSwipeButton(title: "Training".localizedString(), backgroundColor: UIColor("#6699cc"), callback: {
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            return true
        })
        button4.buttonWidth = buttonWidth
        button4.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        */
        //
        self.rightButtons = [button3, button2, button1]
        
        
        
    }
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSFontAttributeName: font!]
        let boldFontAttribute = [NSFontAttributeName: boldFont!]
        let lightColorAttribute = [NSForegroundColorAttributeName: UIColor.lightGray]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        
        
        return boldString
    }
    
}
