//
//  OJTItemTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

protocol OJTItemTableViewCellDelegate : NSObjectProtocol {
    func scoreButtonPressOJTItemTableViewCell(cell: OJTItemTableViewCell, ojtItemModel: OJTItemModel)
    func commentButtonPressOJTItemTableViewCell(cell: OJTItemTableViewCell, ojtItemModel: OJTItemModel)
}

class OJTItemTableViewCell: UITableViewCell {
    

    weak var ojtItemTableViewCellDelegate: OJTItemTableViewCellDelegate?
    
    var isItem: Bool = true {
        didSet {
            if(isItem) {
                textNameLabelLeadingContraint.constant = 15
            } else {
                textNameLabelLeadingContraint.constant = 15
            }
            
        }
    }
    
    
    var ojtItemModel: OJTItemModel? {
        didSet {
            self.loadViewData()
        }
    }
    
    @IBOutlet weak var textNameLabelLeadingContraint: NSLayoutConstraint!
    
    @IBOutlet weak var textNameLabel: UILabel!
    
    @IBOutlet weak var buttonScore: UIButton!
    
    @IBOutlet weak var buttonText: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.buttonScore.layer.cornerRadius = self.buttonScore.frame.height / 2.0
        self.buttonScore.layer.borderWidth = 0.8
        self.buttonScore.layer.borderColor = UIColor.darkGray.cgColor
        self.buttonScore.setTitleColor(UIColor.black, for: UIControl.State.normal)
        /*
         self.buttonText.layer.cornerRadius = self.buttonText.frame.height / 2.0
         self.buttonText.layer.borderWidth = 0.8
         self.buttonText.layer.borderColor = UIColor.darkGray.cgColor
         */
        self.buttonText.setTitle("", for: .normal)
        self.buttonText.backgroundColor = UIColor.clear
        self.isItem = true
        
        self.buttonScore.addTarget(self, action: #selector(self.buttonScorePressed(_:)), for: .touchUpInside)
        
        self.buttonText.addTarget(self, action: #selector(self.buttonTextPressed(_:)), for: .touchUpInside)
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func loadViewData() {
        if let ojtItemModel = self.ojtItemModel {
            textNameLabel.text = ojtItemModel.question
            textNameLabel.textColor = UIColor(ojtItemModel.textColor)
            self.buttonScore.setTitle(ojtItemModel.score, for: UIControl.State.normal)
            if(ojtItemModel.comment == "") {
                self.buttonText.setImage(UIImage.init(named: "comment-off-icon.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
            } else {
                self.buttonText.setImage(UIImage.init(named: "comment-on-icon.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
            }
            self.buttonText.isHidden = !ojtItemModel.isInput
            self.buttonScore.isHidden = !ojtItemModel.isInput
            if(ojtItemModel.isInput) {
                textNameLabel.font = UIFont.systemFont(ofSize: 14)
                self.backgroundColor = UIColor.white
            } else {
                textNameLabel.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
                self.backgroundColor = UIColor.groupTableViewBackground
            }
        }
    }
    
    @objc func buttonScorePressed(_ sender: UIButton) {
        if let ojtItemModel = self.ojtItemModel {
            self.ojtItemTableViewCellDelegate?.scoreButtonPressOJTItemTableViewCell(cell: self, ojtItemModel: ojtItemModel)
        }
        
    }
    
    @objc func buttonTextPressed(_ sender: UIButton) {
        if let ojtItemModel = self.ojtItemModel {
            self.ojtItemTableViewCellDelegate?.commentButtonPressOJTItemTableViewCell(cell: self, ojtItemModel: ojtItemModel)
        }
    }
    
}
