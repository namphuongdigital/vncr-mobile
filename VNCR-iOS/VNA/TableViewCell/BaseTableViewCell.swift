//
//  BaseTableViewCell.swift
//  VNA
//
//  Created by Dai Pham on 23/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class BaseTableViewCell: UITableViewCell {

    func checkAsyncData() {
        // override this function for child cell
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        if let name = NSStringFromClass(self).components(separatedBy: ".").last {
            return name
        }
        return String(describing: self)
    }
    
    var onNeedReload:((UITableViewCell)->Void)?
    var statusObserverSuccess:NSKeyValueObservation?
    var statusObserverError:NSKeyValueObservation?
}

class BaseCollectionCell: UICollectionViewCell {

    var indexPath:IndexPath?
    
    func checkAsyncData() {
        // override this function for child cell
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }

    var onSelect:((_ indexPath:IndexPath?)->Void)?
    var onNeedReload:((UITableViewCell)->Void)?
    var statusObserverSuccess:NSKeyValueObservation?
    var statusObserverError:NSKeyValueObservation?
}
