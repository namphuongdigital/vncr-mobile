//
//  TripInfoTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/6/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit

class TripInfoTableViewCell: UITableViewCell {
    
    @IBOutlet weak var labelTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.labelTitle.textColor = UIColor("#cc9e73")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
