//
//  LotusItemCell.swift
//  VNA
//
//  Created by Pham Dai on 28/08/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class LotusItemCell: BaseTableViewCell {

    // MARK: -  outlets
    @IBOutlet weak var imvView: RoundImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var vwCover: Corner5View!
    
    // MARK: -  properties
    var item:CommonItem?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        lblTitle.font = .systemFont(ofSize: 14, weight: .medium)
        lblDescription.font = .systemFont(ofSize: 14, weight: .regular)
        
        lblTitle.textColor = .black
        lblDescription.textColor = .darkGray
    }
    
    func show(item:CommonItem, backgroundColor:UIColor?) {
        self.item = item
        lblTitle.text = item.title
        if let v = item.getTitleColor {
            lblTitle.textColor = v
        }
        
        lblDescription.text = item.Description
        if let v = item.getDescriptionColor {
            lblDescription.textColor = v
        }
        
        imvView.loadImageNoProgressBar(url: URL(string: item.imageURL))
        
        vwCover.backgroundColor = backgroundColor
    }
    
    override func prepareForReuse() {
        imvView.cancelLoadImageProgress()
    }
    
}
