//
//  ImageViewTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/26/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class ImageViewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageViewCell: UIImageViewProgress!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
