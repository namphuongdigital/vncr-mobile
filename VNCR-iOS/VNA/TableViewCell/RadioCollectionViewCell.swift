//
//  RadioCollectionViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/14/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class RadioCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var radioButton: SSRadioButton!
    
    weak var itemSelect: ItemSelect?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        radioButton.circleColor = UIColor("#166880")
        radioButton.tintColor = UIColor("#166880")
        radioButton.isUserInteractionEnabled = false
        radioButton.isSelected = false
        
    }
    
    func selectedCell() {
        radioButton.isSelected = true
    }
    
    func deSelectedCell() {
        radioButton.isSelected = false
    }
    
    @IBAction func radioButtonPressed(_ sender: UIButton) {
        /*
        if(self.radioButton.isSelected) {
            radioButton.isSelected = false
        } else {
            radioButton.isSelected = true
        }
        */
    }
    
    func loadData(itemSelect: ItemSelect) {
        self.itemSelect = itemSelect
        radioButton.isSelected = itemSelect.isSelected
    }
}
