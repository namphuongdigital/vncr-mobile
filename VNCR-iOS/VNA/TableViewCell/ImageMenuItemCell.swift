//
//  ImageMenuItemCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 3/24/19.
//  Copyright © 2019 OneTeam. All rights reserved.
//

import UIKit

class ImageMenuItemCell: UICollectionViewCell {
    
    weak var imageMenuItem: ImageMenuItem? {
        didSet {
            self.loadData()
        }
    }
    
    
    
    @IBOutlet weak var imageViewCheck: UIImageViewProgress!
    
    @IBOutlet weak var imageViewMenuItem: UIImageViewProgress!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imageViewMenuItem.layer.cornerRadius = 2.5
        self.imageViewMenuItem.clipsToBounds = true
        
        self.imageViewCheck.clipsToBounds = true
        self.imageViewCheck.image = UIImage(named: "check-icon.png")
        self.imageViewCheck.layer.borderColor = UIColor.gray.cgColor
        self.imageViewCheck.layer.borderWidth = 0.8
        self.imageViewCheck.layer.cornerRadius = self.imageViewCheck.frame.size.height / 2.0
        
        self.backgroundColor = UIColor.clear
    }
    
    func loadData() {
        if let imageMenuItem = self.imageMenuItem {
            self.imageViewMenuItem.image = UIImage(named: imageMenuItem.imageName)
            if(imageMenuItem.isCheck) {
                self.imageViewCheck.isHidden = false
            } else {
                self.imageViewCheck.isHidden = true
            }
        }
    }

}
