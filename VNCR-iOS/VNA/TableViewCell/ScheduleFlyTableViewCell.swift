//
//  ScheduleFlyTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/29/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell

class ScheduleFlyTableViewCell: MGSwipeTableCell {
    
    @IBOutlet weak var buttonReportFly: MIBadgeButton!
    @IBOutlet weak var buttonReportPostion: MIBadgeButton!
    @IBOutlet weak var buttonReportMultiRate: MIBadgeButton!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNum: UILabel!
    @IBOutlet weak var labelDateStart: UILabel!
    @IBOutlet weak var labelTimeStart: UILabel!
    @IBOutlet weak var labelClassify: UILabel!
    @IBOutlet weak var labelY: UILabel!
    @IBOutlet weak var labelCI: UILabel!
    @IBOutlet weak var labelInf: UILabel?
    
    weak var delegateAction: ScheduleFlyTableViewCellDelegate?
    
    var fontSize: CGFloat = 15.0
    var buttonWidth: CGFloat = 120.0
    
    
    var scheduleFly: ScheduleFlightModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
            buttonWidth = 60
        }
        
        self.buttonReportFly.setFAIcon(icon: .FAPlane, forState: .normal)
        self.buttonReportFly.setTitleColor(UIColor("#006482"), for: .normal)
        self.buttonReportPostion.setFAIcon(icon: .FAUser, forState: .normal)
        self.buttonReportPostion.setTitleColor(UIColor("#006482"), for: .normal)
        self.buttonReportMultiRate.setFAIcon(icon: .FAListAlt, forState: .normal)
        self.buttonReportMultiRate.setTitleColor(UIColor("#006482"), for: .normal)
        
        self.labelClassify.textColor = UIColor.black
        self.labelDateStart.textColor = UIColor("#cc9e73")
        
        self.labelName.textColor = UIColor("#166880")
        self.labelNum.textColor = UIColor("#3bae8c")//UIColor("#6d6b6c")
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if(selected == true) {
            self.backgroundColor = UIColor("#dbd8d9")
        } else {
            self.backgroundColor = UIColor.clear
        }
        
    }
    
    func loadData(scheduleFlyModel: ScheduleFlightModel, indexPath: IndexPath)  {
        self.scheduleFly = scheduleFlyModel
        
        self.labelName.text = scheduleFlyModel.routing
        self.labelNum.text = scheduleFlyModel.flightNo
        self.labelNum.textColor = UIColor(scheduleFlyModel.fcolor)
        
        //Top
        
        self.buttonReportFly.badgeString = scheduleFlyModel.icon1Top
        self.buttonReportPostion.badgeString = scheduleFlyModel.icon2Top
        self.buttonReportMultiRate.badgeString = scheduleFlyModel.icon3Top
        
        self.buttonReportFly.badgeBackgroundColor = UIColor(scheduleFlyModel.icon1State)
        self.buttonReportPostion.badgeBackgroundColor = UIColor(scheduleFlyModel.icon2State)
        self.buttonReportMultiRate.badgeBackgroundColor = UIColor(scheduleFlyModel.icon3State)
        
        self.buttonReportFly.badgeTextColor = UIColor(scheduleFlyModel.icon1TextColor)
        self.buttonReportPostion.badgeTextColor = UIColor(scheduleFlyModel.icon2TextColor)
        self.buttonReportMultiRate.badgeTextColor = UIColor(scheduleFlyModel.icon3TextColor)
        //Footer
        self.buttonReportFly.badgeStringFooter = scheduleFlyModel.icon1Bot
        self.buttonReportPostion.badgeStringFooter = scheduleFlyModel.icon2Bot
        self.buttonReportMultiRate.badgeStringFooter = scheduleFlyModel.icon3Bot
        
        self.buttonReportFly.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon1StateBot)
        self.buttonReportPostion.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon2StateBot)
        self.buttonReportMultiRate.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon3StateBot)
        
        self.buttonReportFly.badgeTextColorFooter = UIColor(scheduleFlyModel.icon1TextColorBot)
        self.buttonReportPostion.badgeTextColorFooter = UIColor(scheduleFlyModel.icon2TextColorBot)
        self.buttonReportMultiRate.badgeTextColorFooter = UIColor(scheduleFlyModel.icon3TextColorBot)
        
        self.labelTimeStart.text = scheduleFlyModel.departedTime
        self.labelTimeStart.textColor = UIColor(scheduleFlyModel.timeColor)
        self.labelDateStart.text = scheduleFlyModel.departedDate
        
        self.labelClassify.text = String.init(format: "%@", scheduleFlyModel.classify)
        self.labelClassify.attributedText = addBoldText(fullString: self.labelClassify.text! as NSString, boldPartsOfString: ["" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelCI.text = String.init(format: "C %@", scheduleFlyModel.totalCI)
        self.labelCI.attributedText = addBoldText(fullString: self.labelCI.text! as NSString, boldPartsOfString: ["C " as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        self.labelY.text = String.init(format: "Y %@", scheduleFlyModel.totalY)
        self.labelY.attributedText = addBoldText(fullString: self.labelY.text! as NSString, boldPartsOfString: ["Y " as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
//        self.labelInf?.text = String.init(format: "Inf : %d", scheduleFlyModel.inf)
        self.labelInf?.attributedText = addBoldText(fullString: self.labelInf!.text! as NSString, boldPartsOfString: ["Inf :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold))
        
        self.addRightButtons()
    }
    
    func addRightButtons() {
        
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        let button1: MGSwipeButton  = MGSwipeButton(title: "Report position".localizedString(), backgroundColor: UIColor("#dba510"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.reportPositonScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return false
        })
        button1.setImage(UIImage.init(named: "task.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button1.tintColor = UIColor.white
        button1.imageView?.contentMode = .scaleAspectFit
        button1.buttonWidth = buttonWidth
        button1.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        let button2: MGSwipeButton = MGSwipeButton(title: "Report single flight".localizedString(), backgroundColor: UIColor.lightGray, callback: {[weak self] (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.reportFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        button2.setImage(UIImage.init(named: "Flight Report.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button2.tintColor = UIColor.white
        button2.imageView?.contentMode = .scaleAspectFit
        button2.buttonWidth = buttonWidth
        button2.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button3: MGSwipeButton = MGSwipeButton(title: "Multidimensional assessment single flight".localizedString(), backgroundColor: UIColor("#33cc90"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.assessmentFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        button3.setImage(UIImage.init(named: "Assessment.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button3.imageView?.contentMode = .scaleAspectFit
        button3.tintColor = UIColor.white
        button3.buttonWidth = buttonWidth
        button3.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        /*
        let button4: MGSwipeButton = MGSwipeButton(title: "Training".localizedString(), backgroundColor: UIColor("#6699cc"), callback: {
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            return true
        })
        button4.buttonWidth = buttonWidth
        button4.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        */
        //
        
        let button4: MGSwipeButton = MGSwipeButton(title: "Survey list".localizedString(), backgroundColor: UIColor("#6699cc"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.surveyListFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        button4.setImage(UIImage.init(named: "Survey.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button4.tintColor = UIColor.white
        button4.imageView?.contentMode = .scaleAspectFit
        button4.buttonWidth = buttonWidth
        button4.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button5: MGSwipeButton = MGSwipeButton(title: "OJT list".localizedString(), backgroundColor: UIColor("#166880"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.ojtListFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        //button5.setImage(UIImage.init(named: "OJT (Lecture).png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
        button5.setImage(UIImage.init(named: "Seatmap-icon.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State.normal)
       
        button5.tintColor = UIColor.white
        button5.imageView?.contentMode = .scaleAspectFit
        button5.buttonWidth = buttonWidth
        button5.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        self.rightButtons = [button5, button4, button3, button2, button1]
        
    }
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: font!]
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: UIColor.lightGray]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        
        
        return boldString
    }
    
}
