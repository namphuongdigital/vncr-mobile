//
//  ExpanedScheduleFlyTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/13/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell

protocol ScheduleFlyTableViewCellDelegate : NSObjectProtocol {
    func reportPositonScheduleFlyTableViewCell(scheduleFly: ScheduleFlyModel)
    func reportFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlyModel)
    func assessmentFlightScheduleFlyTableViewCell(scheduleFly: ScheduleFlyModel)
}

class ExpanedScheduleFlyTableViewCell: MGSwipeTableCell, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView?

    @IBOutlet weak var buttonReportFly: MIBadgeButton!
    @IBOutlet weak var buttonReportPostion: MIBadgeButton!
    @IBOutlet weak var buttonReportMultiRate: MIBadgeButton!
    @IBOutlet weak var labelFullName: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNum: UILabel!
    @IBOutlet weak var labelDateTimeStart: UILabel!
    @IBOutlet weak var labelPax: UILabel!
    @IBOutlet weak var labelC: UILabel!
    @IBOutlet weak var labelVIP: UILabel!
    @IBOutlet weak var labelInf: UILabel!
    @IBOutlet weak var labelUM: UILabel!
    @IBOutlet weak var labelSpecialMeal: UILabel!
    @IBOutlet weak var labelAircraft: UILabel!
    @IBOutlet weak var labelY: UILabel!
    @IBOutlet weak var labelCIP: UILabel!
    @IBOutlet weak var labelBSCT: UILabel!
    @IBOutlet weak var labelWCHC: UILabel!
    
    @IBOutlet weak var labelListFull: UILabel?
    
    var fontSize: CGFloat = 15.0
    var buttonWidth: CGFloat = 120.0
    
    weak var delegateAction: ScheduleFlyTableViewCellDelegate?
    
    var scheduleFly: ScheduleFlyModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
            buttonWidth = 60
        }
     
        self.buttonReportFly.setFAIcon(icon: .FAPlane, forState: .normal)
        self.buttonReportFly.setTitleColor(UIColor("#006482"), for: .normal)
        self.buttonReportPostion.setFAIcon(icon: .FAUser, forState: .normal)
        self.buttonReportPostion.setTitleColor(UIColor("#006482"), for: .normal)
        self.buttonReportMultiRate.setFAIcon(icon: .FAListAlt, forState: .normal)
        self.buttonReportMultiRate.setTitleColor(UIColor("#006482"), for: .normal)
        
        self.buttonReportFly.badgeBackgroundColor = UIColor("#FFCC00")
        self.buttonReportPostion.badgeBackgroundColor = UIColor("#FFCC00")
        self.buttonReportMultiRate.badgeBackgroundColor = UIColor("#FFCC00")

        self.labelDateTimeStart.textColor = UIColor("#cc9e73")
        
        self.labelName.textColor = UIColor("#166880")
        self.labelNum.textColor = UIColor("#3bae8c")//UIColor("#6d6b6c")
        
        self.labelFullName.textColor = UIColor("#166880")
        self.labelListFull?.textColor = UIColor("#cc9e73")
        
        self.tableView?.register(UINib(nibName: "TripInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "TripInfoTableViewCell")
        self.tableView?.backgroundColor = UIColor.clear
        self.tableView?.isUserInteractionEnabled = false
    }
    
    //MARK: UITableViewDataSource methods
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(scheduleFly == nil) {
            return 0
        } else {
            return scheduleFly!.trips?.details.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TripInfoTableViewCell")! as! TripInfoTableViewCell
        let title = scheduleFly!.trips!.details[indexPath.row]
        cell.labelTitle.text = title
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView?.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 24
        
        
    }

    //
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        if(selected == true) {
            self.backgroundColor = UIColor("#fafffb")
        } else {
            self.backgroundColor = UIColor.clear
        }
        
    }
    
    func loadData(scheduleFlyModel: ScheduleFlyModel, indexPath: IndexPath)  {
        self.scheduleFly = scheduleFlyModel
        
        self.tableView?.reloadData()
        
        self.labelName.text = scheduleFlyModel.routing
        self.labelNum.text = scheduleFlyModel.flightNo
        if(scheduleFlyModel.trips?.routing == "") {
            self.labelFullName.text = scheduleFlyModel.routing
        } else {
            self.labelFullName.text = scheduleFlyModel.trips?.routing
        }
        //Top
        self.buttonReportFly.badgeString = scheduleFlyModel.icon1Top
        self.buttonReportPostion.badgeString = scheduleFlyModel.icon2Top
        self.buttonReportMultiRate.badgeString = scheduleFlyModel.icon3Top
        
        self.buttonReportFly.badgeBackgroundColor = UIColor(scheduleFlyModel.icon1State)
        self.buttonReportPostion.badgeBackgroundColor = UIColor(scheduleFlyModel.icon2State)
        self.buttonReportMultiRate.badgeBackgroundColor = UIColor(scheduleFlyModel.icon3State)
        
        self.buttonReportFly.badgeTextColor = UIColor(scheduleFlyModel.icon1TextColor)
        self.buttonReportPostion.badgeTextColor = UIColor(scheduleFlyModel.icon2TextColor)
        self.buttonReportMultiRate.badgeTextColor = UIColor(scheduleFlyModel.icon3TextColor)
        
        //Footer
        self.buttonReportFly.badgeStringFooter = scheduleFlyModel.icon1Bot
        self.buttonReportPostion.badgeStringFooter = scheduleFlyModel.icon2Bot
        self.buttonReportMultiRate.badgeStringFooter = scheduleFlyModel.icon3Bot
        
        self.buttonReportFly.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon1StateBot)
        self.buttonReportPostion.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon2StateBot)
        self.buttonReportMultiRate.badgeBackgroundColorFooter = UIColor(scheduleFlyModel.icon3StateBot)
        
        self.buttonReportFly.badgeTextColorFooter = UIColor(scheduleFlyModel.icon1TextColorBot)
        self.buttonReportPostion.badgeTextColorFooter = UIColor(scheduleFlyModel.icon2TextColorBot)
        self.buttonReportMultiRate.badgeTextColorFooter = UIColor(scheduleFlyModel.icon3TextColorBot)
     
        self.labelDateTimeStart.text = String(format: "%@ %@", scheduleFlyModel.departedTime, scheduleFlyModel.departedDate)//scheduleFlyModel.departed.dateTimeToHHmmddMMyyyy
        self.labelAircraft.text = String.init(format: "%@", scheduleFlyModel.aircraft)
        self.labelPax.text = String.init(format: "Pax : %@", String(format: "%d", scheduleFlyModel.totalPax))
        self.labelPax.attributedText = addBoldText(fullString: self.labelPax.text! as NSString, boldPartsOfString: ["Pax :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelC.text = String.init(format: "C : %d", scheduleFlyModel.c)
        self.labelC.attributedText = addBoldText(fullString: self.labelC.text! as NSString, boldPartsOfString: ["C :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelY.text = String.init(format: "Y : %d", scheduleFlyModel.y)
        self.labelY.attributedText = addBoldText(fullString: self.labelY.text! as NSString, boldPartsOfString: ["Y :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelVIP.text = String.init(format: "VIP : %d", scheduleFlyModel.VIP)
        self.labelVIP.attributedText = addBoldText(fullString: self.labelVIP.text! as NSString, boldPartsOfString: ["VIP :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelCIP.text = String.init(format: "CIP : %d", scheduleFlyModel.CIP)
        self.labelCIP.attributedText = addBoldText(fullString: self.labelCIP.text! as NSString, boldPartsOfString: ["CIP :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelInf.text = String.init(format: "Inf : %d", scheduleFlyModel.inf)
        self.labelInf.attributedText = addBoldText(fullString: self.labelInf.text! as NSString, boldPartsOfString: ["Inf :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelUM.text = String.init(format: "UM : %d", scheduleFlyModel.um)
        self.labelUM.attributedText = addBoldText(fullString: self.labelUM.text! as NSString, boldPartsOfString: ["UM :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelBSCT.text = String.init(format: "BSCT : %d", scheduleFlyModel.BSCT)
        self.labelBSCT.attributedText = addBoldText(fullString: self.labelBSCT.text! as NSString, boldPartsOfString: ["BSCT :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelSpecialMeal.text = String.init(format: "Special meal : %d", scheduleFlyModel.specialMeal)
        self.labelSpecialMeal.attributedText = addBoldText(fullString: self.labelSpecialMeal.text! as NSString, boldPartsOfString: ["Special meal :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        self.labelWCHC.text = String.init(format: "WCHC : %d", scheduleFlyModel.WCHC)
        self.labelWCHC.attributedText = addBoldText(fullString: self.labelWCHC.text! as NSString, boldPartsOfString: ["WCHC :" as NSString], font: UIFont.systemFont(ofSize: fontSize), boldFont: UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold))
        if(scheduleFlyModel.selectTripYN) {
            self.addRightButtons()
        } else {
            self.rightButtons.removeAll()
        }
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            var string = ""
            if(scheduleFly!.trips?.details != nil) {
                for item in scheduleFly!.trips!.details {
                    string += String(format: "%@\n", item)
                    
                }
                
            }
            self.labelListFull!.text = string
            
        }
        
    }
    
    func addRightButtons() {
        
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        let button3: MGSwipeButton  = MGSwipeButton(title: NSLocalizedString("Report position multi flight", comment: ""), backgroundColor: UIColor("#dba510"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.reportPositonScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return false
        })
        button3.buttonWidth = buttonWidth
        button3.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        let button2: MGSwipeButton = MGSwipeButton(title: NSLocalizedString("Report multi flight", comment: ""),backgroundColor: UIColor.lightGray, callback: {[weak self] (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.reportFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        button2.buttonWidth = buttonWidth
        button2.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button1: MGSwipeButton = MGSwipeButton(title: NSLocalizedString("Multidimensional assessment journey", comment: ""),backgroundColor: UIColor("#33cc90"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            self.delegateAction?.assessmentFlightScheduleFlyTableViewCell(scheduleFly: self.scheduleFly!)
            return true
        })
        button1.buttonWidth = buttonWidth
        button1.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        //
        self.rightButtons = [button1, button2, button3]
        
        
        
    }
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!) -> NSAttributedString {
        let nonBoldFontAttribute = [NSFontAttributeName: font!]
        let boldFontAttribute = [NSFontAttributeName: boldFont!]
        let lightColorAttribute = [NSForegroundColorAttributeName: UIColor.black]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        
        
        return boldString
    }
    
    
}
