//
//  LeaderPersonReportTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 10/30/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//
import UIKit
import Foundation

protocol LeaderPersonReportTableViewCellDelegate : NSObjectProtocol {
    func showPopupSelected(sender: UIButton, tableViewCell: UITableViewCell, array: Array<AnyObject>?, buttonColunmType: CategoryType)
    func imageViewAvatarTapped(crewDutyModel: CrewDutyModel)
}

class LeaderPersonReportTableViewCell: UITableViewCell {
    
    var crewDutyModel: CrewDutyModel?
    
    weak var delegate: LeaderPersonReportTableViewCellDelegate?
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNo: UILabel!
    @IBOutlet weak var buttonJob: ButtonHalfHeight!
    @IBOutlet weak var buttonPosition: ButtonHalfHeight!
    @IBOutlet weak var buttonAnn: ButtonHalfHeight!
    @IBOutlet weak var buttonKem: ButtonHalfHeight!
    @IBOutlet weak var buttonVip: ButtonHalfHeight!
    @IBOutlet weak var buttonDuty: ButtonHalfHeight!
    
    var fontSize: CGFloat = 16.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 11
        }
        
        //self.imageViewAvatar.layer.backgroundColor = UIColor.lightGray.cgColor
        self.imageViewAvatar.contentMode = .scaleAspectFit
        self.imageViewAvatar.layer.borderWidth = 0.5
        self.imageViewAvatar.layer.borderColor = UIColor.lightGray.cgColor
        self.imageViewAvatar.layer.cornerRadius = 1.5
        self.imageViewAvatar.clipsToBounds = true
        
        self.buttonJob.addTarget(self, action: #selector(self.buttonJobPress(sender:)), for: UIControl.Event.touchUpInside)
        self.buttonPosition.addTarget(self, action: #selector(self.buttonPositionPress(sender:)), for: .touchUpInside)
        self.buttonAnn.addTarget(self, action: #selector(self.buttonAnnPress(sender:)), for: .touchUpInside)
        self.buttonKem.addTarget(self, action: #selector(self.buttonKemPress(sender:)), for: .touchUpInside)
        self.buttonVip.addTarget(self, action: #selector(self.buttonVipPress(sender:)), for: .touchUpInside)
        self.buttonDuty.addTarget(self, action: #selector(self.buttonDutyPress(sender:)), for: .touchUpInside)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageViewAvatarTapped(tapGestureRecognizer:)))
        imageViewAvatar.isUserInteractionEnabled = true
        imageViewAvatar.addGestureRecognizer(tapGestureRecognizer)
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        self.buttonJob.layer.cornerRadius = self.buttonJob.frame.height / 2.0
        self.buttonJob.layer.borderWidth = 0.8
        self.buttonJob.layer.borderColor = UIColor.darkGray.cgColor
//        self.buttonPosition.layer.cornerRadius = self.buttonPosition.frame.height / 2.0
        self.buttonPosition.layer.borderWidth = 0.8
        self.buttonPosition.layer.borderColor = UIColor.darkGray.cgColor
//        self.buttonAnn.layer.cornerRadius = self.buttonAnn.frame.height / 2.0
        self.buttonAnn.layer.borderWidth = 0.8
        self.buttonAnn.layer.borderColor = UIColor.darkGray.cgColor
//        self.buttonKem.layer.cornerRadius = self.buttonKem.frame.height / 2.0
        self.buttonKem.layer.borderWidth = 0.8
        self.buttonKem.layer.borderColor = UIColor.darkGray.cgColor
//        self.buttonVip.layer.cornerRadius = self.buttonVip.frame.height / 2.0
        self.buttonVip.layer.borderWidth = 0.8
        self.buttonVip.layer.borderColor = UIColor.darkGray.cgColor
//        self.buttonDuty.layer.cornerRadius = self.buttonDuty.frame.height / 2.0
        self.buttonDuty.layer.borderWidth = 0.8
        self.buttonDuty.layer.borderColor = UIColor.darkGray.cgColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
        if(crewDutyModel?.isReadOnly == false) {
            if(selected == true) {
                self.backgroundColor = UIColor("#dbd8d9")
            } else {
                self.backgroundColor = UIColor.clear
            }
        }
    }
    
    
    @objc func imageViewAvatarTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if let tappedImage = tapGestureRecognizer.view as? UIImageView {
            guard let crewDutyModel = self.crewDutyModel else {
                return
            }
            delegate?.imageViewAvatarTapped(crewDutyModel: crewDutyModel)
        }
        
        // Your action
    }

    
    @objc func buttonJobPress(sender: UIButton) {
        self.delegate?.showPopupSelected(sender: sender, tableViewCell: self, array: nil, buttonColunmType: .job)
    }
    
    @objc func buttonPositionPress(sender: UIButton) {
        self.delegate?.showPopupSelected(sender: sender, tableViewCell: self, array: nil, buttonColunmType: .ca)
    }
    
    @objc func buttonAnnPress(sender: UIButton) {
        self.delegate?.showPopupSelected(sender: sender, tableViewCell: self, array: nil, buttonColunmType: .ann)
    }
    
    @objc func buttonKemPress(sender: UIButton) {
        self.delegate?.showPopupSelected(sender: sender, tableViewCell: self, array: nil, buttonColunmType: .training)
    }
    
    @objc func buttonVipPress(sender: UIButton) {
        self.delegate?.showPopupSelected(sender: sender, tableViewCell: self, array: nil, buttonColunmType: .vip)
    }
    
    @objc func buttonDutyPress(sender: UIButton) {
        self.delegate?.showPopupSelected(sender: sender, tableViewCell: self, array: nil, buttonColunmType: .dutyFree)
    }
    
    
    func loadData(crewDutyModel: CrewDutyModel)  {
        self.crewDutyModel = crewDutyModel
        //FirstName + LastName + CrewID / FO_Job (Ability)
        self.buttonVip.isEnabled = false
        self.buttonVip.alpha = 0.3
        self.buttonAnn.isEnabled = false
        self.buttonAnn.alpha = 0.3
        
        if crewDutyModel.isPilot {
            buttonJob.alpha = 0
            buttonJob.isEnabled = false
            buttonPosition.alpha = 0
            buttonPosition.isEnabled = false
            buttonKem.alpha = 0
            buttonKem.isEnabled = false
            buttonDuty.alpha = 0
            buttonDuty.isEnabled = false
        } else {
            if(crewDutyModel.isReadOnly == true) {
                self.contentView.alpha = 0.5
                buttonJob.alpha = 0.3
                buttonJob.isEnabled = false
                buttonPosition.alpha = 0.3
                buttonPosition.isEnabled = false
                buttonKem.alpha = 0.3
                buttonKem.isEnabled = false
                buttonDuty.alpha = 0.3
                buttonDuty.isEnabled = false
            } else {
                self.contentView.alpha = 1.0
                buttonJob.alpha = 1.0
                buttonJob.isEnabled = true
                buttonPosition.alpha = 1.0
                buttonPosition.isEnabled = true
                buttonKem.alpha = 1.0
                buttonKem.isEnabled = true
                buttonDuty.alpha = 1.0
                buttonDuty.isEnabled = true
            }
        }
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            self.labelName.text = String(format: "%@", crewDutyModel.crewFirstName)
        } else {
            self.labelName.text = String(format: "%@ %@", crewDutyModel.crewLastName, crewDutyModel.crewFirstName)
        }
        
        var colorCrewFirstName = "#6699cc"
        if(crewDutyModel.isPurser) {
            colorCrewFirstName = "#DBA510"
        } else {
            colorCrewFirstName = "#A0A0A0"
        }
        
        self.labelName.attributedText = addBoldText(fullString: self.labelName.text! as NSString, boldPartsOfString: [crewDutyModel.crewFirstName as NSString], font: self.labelName.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor(colorCrewFirstName))
        
        /*
         Default = "#02BBBE";//Blue
         CanWrite = "#DBA510";//Yellow
         ReadOnly = "#A0A0A0";//Grey
        */
        //UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold)
        /*
        self.labelNo.text = String(format: "%@ / %@ / %@ (%@)", crewDutyModel.training, crewDutyModel.foJob, crewDutyModel.pos, crewDutyModel.ability)
        self.labelNo.attributedText = addBoldText(fullString: self.labelNo.text! as NSString, boldPartsOfString: [crewDutyModel.foJob as NSString, crewDutyModel.pos as NSString], font: self.labelNo.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor("#dba510"))
        */
        self.labelNo.text = String(format: "%@ / %@ %@", crewDutyModel.foJob, crewDutyModel.pos, crewDutyModel.ability)
        self.labelNo.attributedText = addBoldText(fullString: self.labelNo.text! as NSString, boldPartsOfString: [crewDutyModel.foJob as NSString, crewDutyModel.pos as NSString], font: self.labelNo.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor("#dba510"))
        //"Nguyễn Ngọc Hùng 4 0111 / P / POS (P/B/*E)"
        self.buttonJob.setTitle(crewDutyModel.job, for: .normal)
        self.buttonPosition.setTitle(crewDutyModel.ca, for: .normal)
        self.buttonAnn.setTitle(crewDutyModel.ann, for: .normal)
        
        self.buttonKem.setTitle(crewDutyModel.trainingCrewName, for: .normal)
        
        self.buttonVip.setTitle(crewDutyModel.vip, for: .normal)
        
        self.buttonDuty.setTitle(crewDutyModel.dutyFree, for: .normal)
        
        self.imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: crewDutyModel.avatarURL))
       
    }
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!, boldColor: UIColor) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: font!]
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: boldColor]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        
        
        return boldString
    }
    
}

class LeaderPilotReportTableViewCell: UITableViewCell {
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNo: UILabel!

    weak var delegate: LeaderPersonReportTableViewCellDelegate?
    var crewDutyModel: CrewDutyModel?
    
    var fontSize: CGFloat = 16.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 11
        }
        
        labelName.numberOfLines = 2
        
        //self.imageViewAvatar.layer.backgroundColor = UIColor.lightGray.cgColor
        self.imageViewAvatar.contentMode = .scaleAspectFit
        self.imageViewAvatar.layer.borderWidth = 0.5
        self.imageViewAvatar.layer.borderColor = UIColor.lightGray.cgColor
        self.imageViewAvatar.layer.cornerRadius = 1.5
        self.imageViewAvatar.clipsToBounds = true
                
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageViewAvatarTapped(tapGestureRecognizer:)))
        imageViewAvatar.isUserInteractionEnabled = true
        imageViewAvatar.addGestureRecognizer(tapGestureRecognizer)
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
    }
    
    @objc func imageViewAvatarTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if let tappedImage = tapGestureRecognizer.view as? UIImageView {
            guard let crewDutyModel = self.crewDutyModel else {
                return
            }
            delegate?.imageViewAvatarTapped(crewDutyModel: crewDutyModel)
        }
        
        // Your action
    }
    
    func loadData(crewDutyModel: CrewDutyModel)  {
        self.crewDutyModel = crewDutyModel
        //FirstName + LastName + CrewID / FO_Job (Ability)
    
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            self.labelName.text = String(format: "%@", crewDutyModel.crewFirstName)
        } else {
            self.labelName.text = String(format: "%@ %@", crewDutyModel.crewLastName, crewDutyModel.crewFirstName)
        }
        
        var colorCrewFirstName = "#6699cc"
        if(crewDutyModel.isPurser) {
            colorCrewFirstName = "#DBA510"
        } else {
            colorCrewFirstName = "#A0A0A0"
        }
        
        let mutable = NSMutableAttributedString(attributedString: addBoldText(fullString: self.labelName.text! as NSString, boldPartsOfString: [crewDutyModel.crewFirstName as NSString], font: self.labelName.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor(colorCrewFirstName)))
        mutable.append(addBoldText(fullString: String(format: "\n%@ %@", crewDutyModel.pos, crewDutyModel.ability) as NSString, boldPartsOfString: [crewDutyModel.pos as NSString], font: self.labelNo.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor("#dba510")))
        self.labelName.attributedText = mutable
        
        /*
         Default = "#02BBBE";//Blue
         CanWrite = "#DBA510";//Yellow
         ReadOnly = "#A0A0A0";//Grey
        */
        //UIFont.systemFont(ofSize: fontSize, weight: UIFontWeightSemibold)
        /*
        self.labelNo.text = String(format: "%@ / %@ / %@ (%@)", crewDutyModel.training, crewDutyModel.foJob, crewDutyModel.pos, crewDutyModel.ability)
        self.labelNo.attributedText = addBoldText(fullString: self.labelNo.text! as NSString, boldPartsOfString: [crewDutyModel.foJob as NSString, crewDutyModel.pos as NSString], font: self.labelNo.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor("#dba510"))
        */
        self.labelNo.text = String(format: "%@", crewDutyModel.foJob)
        self.labelNo.attributedText = addBoldText(fullString: self.labelNo.text! as NSString, boldPartsOfString: [crewDutyModel.foJob as NSString], font: self.labelNo.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor("#dba510"))
        //"Nguyễn Ngọc Hùng 4 0111 / P / POS (P/B/*E)"
        
        self.imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: crewDutyModel.avatarURL))
       
    }
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!, boldColor: UIColor) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: font!]
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: boldColor]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        
        
        return boldString
    }
}
