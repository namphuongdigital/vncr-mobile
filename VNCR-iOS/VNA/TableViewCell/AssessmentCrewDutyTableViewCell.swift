//
//  AssessmentCrewDutyTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

protocol AssessmentCrewDutyTableViewCellDelegate : NSObjectProtocol {
    func imageViewAvatarTapped(crewDutyModel: CrewDutyModel)
}

class AssessmentCrewDutyTableViewCell: UITableViewCell {

    var crewDutyModel: CrewDutyModel?
    
    weak var delegate: AssessmentCrewDutyTableViewCellDelegate?
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNo: UILabel!
    @IBOutlet weak var buttonJob: UIButton!
    @IBOutlet weak var buttonTotalScore: UIButton!
    
    var fontSize: CGFloat = 16.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 11
        }
        
        //self.imageViewAvatar.layer.backgroundColor = UIColor.lightGray.cgColor
        self.imageViewAvatar.contentMode = .scaleAspectFit
        self.imageViewAvatar.layer.borderWidth = 0.5
        self.imageViewAvatar.layer.borderColor = UIColor.lightGray.cgColor
        self.imageViewAvatar.layer.cornerRadius = 1.5
        self.imageViewAvatar.clipsToBounds = true
        
        self.buttonJob.layer.cornerRadius = self.buttonJob.frame.height / 2.0
        self.buttonJob.layer.borderWidth = 0.8
        self.buttonJob.layer.borderColor = UIColor.darkGray.cgColor
        self.buttonTotalScore.layer.cornerRadius = self.buttonTotalScore.frame.height / 2.0
        self.buttonTotalScore.layer.borderWidth = 0.8
        self.buttonTotalScore.layer.borderColor = UIColor.darkGray.cgColor
        
        
        self.buttonJob.addTarget(self, action: #selector(self.buttonJobPress(sender:)), for: UIControl.Event.touchUpInside)
        self.buttonTotalScore.addTarget(self, action: #selector(self.buttonTotalScorePress(sender:)), for: .touchUpInside)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageViewAvatarTapped(tapGestureRecognizer:)))
        imageViewAvatar.isUserInteractionEnabled = true
        imageViewAvatar.addGestureRecognizer(tapGestureRecognizer)
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
      
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        
        if(crewDutyModel?.isReadOnly == false) {
            if(selected == true) {
                self.backgroundColor = UIColor("#dbd8d9")
            } else {
                self.backgroundColor = UIColor.clear
            }
        }
        
        
        
    }
    
    @objc func imageViewAvatarTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if (tapGestureRecognizer.view as? UIImageView) != nil {
            guard let crewDutyModel = self.crewDutyModel else {
                return
            }
            delegate?.imageViewAvatarTapped(crewDutyModel: crewDutyModel)
        }
        
        // Your action
    }
    
    @objc func buttonJobPress(sender: UIButton) {
        //self.delegate?.showPopupSelected(sender: sender, tableViewCell: self, array: nil, buttonColunmType: .job)
    }
    
    @objc func buttonTotalScorePress(sender: UIButton) {
        //self.delegate?.showPopupSelected(sender: sender, tableViewCell: self, array: nil, buttonColunmType: .totalScore)
    }
    
    
    
    
    func loadData(crewDutyModel: CrewDutyModel)  {
        self.crewDutyModel = crewDutyModel
        //FirstName + LastName + CrewID / FO_Job (Ability)
        
        self.labelName.text = String(format: "%@ %@", crewDutyModel.crewLastName, crewDutyModel.crewFirstName)
        
        var colorCrewFirstName = "#6699cc"
        if(crewDutyModel.isPurser) {
            colorCrewFirstName = "#DBA510"
        } else {
            colorCrewFirstName = "#A0A0A0"
        }
        
        self.labelNo.text = String(format: "%@ / %@ / %@ (%@)", crewDutyModel.training, crewDutyModel.foJob, crewDutyModel.pos, crewDutyModel.ability)
        self.labelNo.attributedText = addBoldText(fullString: self.labelNo.text! as NSString, boldPartsOfString: [crewDutyModel.foJob as NSString, crewDutyModel.pos as NSString], font: self.labelNo.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor("#dba510"))
        
        self.labelName.attributedText = addBoldText(fullString: self.labelName.text! as NSString, boldPartsOfString: [crewDutyModel.crewFirstName as NSString], font: self.labelName.font, boldFont: UIFont.boldSystemFont(ofSize: fontSize), boldColor: UIColor(colorCrewFirstName))
        
        //"Nguyễn Ngọc Hùng 4 0111 / P / POS (P/B/*E)"
        self.buttonJob.setTitle(crewDutyModel.job, for: .normal)
        
        self.buttonTotalScore.setTitle(String(format: "%d", crewDutyModel.totalScore), for: .normal)
        
        self.imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: crewDutyModel.avatarURL))
        
        
    }
    
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!, boldColor: UIColor) -> NSAttributedString {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: font!]
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: boldColor]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(lightColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        
        
        
        return boldString
    }

}
