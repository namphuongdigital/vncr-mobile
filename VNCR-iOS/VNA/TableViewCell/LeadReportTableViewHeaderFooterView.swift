//
//  LeadReportTableViewHeaderFooterView.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/25/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit

class LeadReportTableViewHeaderFooterView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var viewContainerHeader: UIView!
    @IBOutlet weak var viewHeaderName: UIView!

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override func awakeFromNib() {
        self.viewContainerHeader.backgroundColor = UIColor("#EAEAEA")//UIColor("#166880")
        self.viewHeaderName.backgroundColor = .clear//UIColor("#166880")
    }
    
    func configForPilot() {
        viewContainerHeader.subviews.forEach({$0.alpha = 0})
        self.viewHeaderName.backgroundColor = .clear
        self.viewContainerHeader.backgroundColor = UIColor("#EAEAEA")
    }
    
    override func prepareForReuse() {
        viewContainerHeader.subviews.forEach({$0.alpha = 1})
        self.viewContainerHeader.backgroundColor = UIColor("#EAEAEA")//UIColor("#166880")
        self.viewHeaderName.backgroundColor = .clear//UIColor("#166880")
    }

}
