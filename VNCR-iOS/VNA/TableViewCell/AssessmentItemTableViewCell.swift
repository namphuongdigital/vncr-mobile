//
//  AssessmentItemTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 2/19/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

protocol AssessmentItemTableViewCellDelegate : NSObjectProtocol {
    func showPopupSelected(sender: UIButton, tableViewCell: UITableViewCell, array: Array<AnyObject>?, assessmentItemModel: AssessmentItemModel)
}

class AssessmentItemTableViewCell: UITableViewCell {
    
    weak var assessmentItemModel: AssessmentItemModel?
    
    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var buttonSelected: UIButton!
    
    @IBOutlet weak var viewLine: UIView!
    
    weak var delegate: AssessmentItemTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.buttonSelected.layer.cornerRadius = self.buttonSelected.frame.height / 2.0
        self.buttonSelected.layer.borderWidth = 0.8
        self.buttonSelected.layer.borderColor = UIColor.darkGray.cgColor
        self.buttonSelected.addTarget(self, action: #selector(self.buttonSelectedPress(sender:)), for: .touchUpInside)
        
        viewLine.backgroundColor = UIColor.lightGray
        viewLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: UIScreen.main.bounds.size.width, height: 0.8)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func buttonSelectedPress(sender: UIButton) {
        guard let assessmentItemModel = assessmentItemModel else {
            return
        }
        self.delegate?.showPopupSelected(sender: sender, tableViewCell: self, array: nil, assessmentItemModel: assessmentItemModel)
    }
    
    func loadData(assessmentItemModel: AssessmentItemModel) {
        self.assessmentItemModel = assessmentItemModel
        self.labelTitle.text = assessmentItemModel.question
        self.buttonSelected.setTitle(String(format: "%d", assessmentItemModel.score), for: UIControl.State())
    }
}
