//
//  FormWofkflowCell.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 11/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

protocol FormWorkflowCellDelegate : NSObjectProtocol {
    func imageViewAvatarTapped(_ imgUrl: String)
}

class FormWorkflowCell: UITableViewCell {

    // MARK: -  function
    func setData(data: FormWorkflowModel) {
        
        self.localData = data
        
        self.titleLabel.text = data.Title
        if !data.TextColor.isEmpty {
            titleLabel.textColor = UIColor.init(data.TextColor)
        }
        
        self.descriptionLabel.text = data.ApprovedBy
        self.descriptionLabel.isHidden = data.ApprovedBy.isEmpty
        if !data.ApprovedByTextColor.isEmpty {
            descriptionLabel.textColor = UIColor.init(data.ApprovedByTextColor)
        }
        
        self.remarkLabel.text = data.Remark
        self.remarkLabel.isHidden = data.Remark.isEmpty
        if !data.RemarkTextColor.isEmpty {
            remarkLabel.textColor = UIColor.init(data.RemarkTextColor)
        }
        
        if data.AvatarUrl.starts(with: "http") {
            imgViewLeft.loadImageProgress(url: URL.init(string: data.AvatarUrl))
        } else {
            imgViewLeft.image = #imageLiteral(resourceName: "ic_profile").resizeImageWith(newSize: CGSize(width: 60, height: 60))
        }
        
        imgViewRight.image = data.Status == "\(Common.FormStatus.Accepted)" ? #imageLiteral(resourceName: "ic_circle_outline_check").resizeImageWith(newSize: iconSize).tint(with: Common.shared.APP_GREEN_COLOR) : data.Status == "\(Common.FormStatus.Rejected)" ? #imageLiteral(resourceName: "ic_cancel").resizeImageWith(newSize: iconSize).tint(with: Common.shared.APP_RED_COLOR) : #imageLiteral(resourceName: "ic_wating").resizeImageWith(newSize: iconSize).tint(with: Common.shared.APP_ORANGE_COLOR)
        
    }
    
    fileprivate func loadDefault() {
        
        localData = nil
        containView.layer.borderWidth = 0
        containView.layer.borderColor = UIColor.lightGray.cgColor
        imgViewLeft.image = nil
        imgViewRight.image = nil
        
        titleLabel.text = nil
        titleLabel.textColor = .black
        titleLabel.font = UIFont.systemFont(ofSize: 12, weight: .medium)
        titleLabel.addLineSpacing(space: 6.0)
        
        descriptionLabel.text = nil
        descriptionLabel.isHidden = true
        descriptionLabel.textColor = .darkGray
        descriptionLabel.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        descriptionLabel.addLineSpacing(space: 6.0)
        
        remarkLabel.text = nil
        remarkLabel.isHidden = true
        remarkLabel.textColor = .darkGray
        remarkLabel.font = UIFont.systemFont(ofSize: 11, weight: .regular)
        remarkLabel.addLineSpacing(space: 6.0)

    }
    
    @objc func imageViewAvatarTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if (tapGestureRecognizer.view as? UIImageView) != nil {
            Broadcaster.notify(FormWorkflowCellDelegate.self) {
                $0.imageViewAvatarTapped(self.localData?.AvatarUrl ?? "")
            }
        }
    }
    
    // MARK: -  private
    private func config() {
        containView.layer.masksToBounds = false
        containView.layer.shadowOffset = CGSize(width: 1, height: 1)
        containView.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        containView.layer.shadowOpacity = 0.5
        containView.layer.cornerRadius = 2
        containView.layer.shadowRadius = 1
        
        imgViewLeft.contentMode = .scaleAspectFit
        imgViewLeft.layer.borderWidth = 0.5
        imgViewLeft.layer.borderColor = UIColor.lightGray.cgColor
        imgViewLeft.layer.cornerRadius = 1.5
        imgViewLeft.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageViewAvatarTapped(tapGestureRecognizer:)))
        imgViewLeft.isUserInteractionEnabled = true
        imgViewLeft.addGestureRecognizer(tapGestureRecognizer)
        
        loadDefault()
    }
    
    // MARK: -  init
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        config()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        loadDefault()
    }
    
    // MARK: -  properties
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    
    var localData: FormWorkflowModel?
    let iconSize = CGSize(width: 24, height: 24)
    
    // MARK: -  oulet
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgViewLeft: UIImageViewProgress!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var remarkLabel: UILabel!
    @IBOutlet weak var imgViewRight: UIImageView!
    
}
