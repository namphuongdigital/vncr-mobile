//
//  ArticleCell.swift
//  VNA
//
//  Created by Nhan Ba Doan on 11/25/2020.
//

import UIKit
import MGSwipeTableCell

class ArticleCell: BaseTableViewCell {
    
    weak var delegateAction: MessageItemTableViewCellDelegate?
    
    @IBOutlet weak var imageViewThumb: UIImageViewProgress!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    
    @IBOutlet weak var labelTime: UILabel!
    @IBOutlet weak var btnTotalView: UILabel!
    @IBOutlet weak var btnTotalLike: UILabel!
    @IBOutlet weak var btnTotalComm: UILabel!
    
    @IBOutlet weak var imgTotalView: UIImageView!
    @IBOutlet weak var imgTotalLike: UIImageView!
    @IBOutlet weak var imgTotalComm: UIImageView!
        
    weak var messageModel: MessageModel?
    
    var fontSize: CGFloat = 15.0
    var buttonWidth: CGFloat = 120.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 14
            buttonWidth = 60
        }
        self.backgroundColor = UIColor.clear

        self.labelTime.font = UIFont.systemFont(ofSize: fontSize - 1)
        self.labelContent.font = UIFont.systemFont(ofSize: fontSize)
        self.labelTitle.font = UIFont.systemFont(ofSize: fontSize)
        self.labelTitle.textColor = UIColor.darkText
        self.labelContent.textColor = UIColor.darkText
        
        let isGuest = ServiceData.sharedInstance.userModel?.isGuest ?? false
        
        self.imgTotalView.superview?.isHidden = isGuest
        self.imgTotalLike.superview?.isHidden = isGuest
        self.imgTotalComm.superview?.isHidden = isGuest
        
        self.btnTotalView.isHidden = isGuest
        self.btnTotalLike.isHidden = isGuest
        self.btnTotalComm.isHidden = isGuest
        
        self.btnTotalView.font = UIFont.systemFont(ofSize: fontSize - 1)
        self.btnTotalLike.font = UIFont.systemFont(ofSize: fontSize - 1)
        self.btnTotalComm.font = UIFont.systemFont(ofSize: fontSize - 1)

        self.btnTotalView.textColor = #colorLiteral(red: 0.0862745098, green: 0.4078431373, blue: 0.5019607843, alpha: 1)
        self.btnTotalLike.textColor = #colorLiteral(red: 0.0862745098, green: 0.4078431373, blue: 0.5019607843, alpha: 1)
        self.btnTotalComm.textColor = #colorLiteral(red: 0.0862745098, green: 0.4078431373, blue: 0.5019607843, alpha: 1)

        self.imgTotalView.image = #imageLiteral(resourceName: "ic_eye").resizeImageWith(newSize: CGSize(width: 18, height: 18)).tint(with: #colorLiteral(red: 0.0862745098, green: 0.4078431373, blue: 0.5019607843, alpha: 1))
        self.imgTotalLike.image = #imageLiteral(resourceName: "ic_like").resizeImageWith(newSize: CGSize(width: 18, height: 18)).tint(with: #colorLiteral(red: 0.0862745098, green: 0.4078431373, blue: 0.5019607843, alpha: 1))
        self.imgTotalComm.image = #imageLiteral(resourceName: "ic_message_line").resizeImageWith(newSize: CGSize(width: 18, height: 18)).tint(with: #colorLiteral(red: 0.0862745098, green: 0.4078431373, blue: 0.5019607843, alpha: 1))

        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        self.layoutMargins = .zero
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        imageViewThumb.cancelLoadImageProgress()
    }
    
    // MARK: -  properties
//    static var identifier:String {
//        return String(describing: self)
//    }
//
//    static var nib:UINib {
//        return UINib(nibName: identifier, bundle: Bundle.main)
//    }
    
    func loadContent(messageModel: MessageModel, isAddActionButton: Bool = true) {
        
        self.messageModel = messageModel
        
        self.labelTitle.text = messageModel.title
        self.labelContent.text = messageModel.message.trimmingCharacters(in: .whitespacesAndNewlines)
        
        self.labelTime.text = messageModel.dateRecord?.dateTimeToArticle
        self.imageViewThumb.loadImageNoProgressBar(url: URL.init(string: messageModel.imageUrl))
        
        self.btnTotalView.text = "\(messageModel.totalView)"
        self.btnTotalLike.text = "\(messageModel.totalLike)"
        self.btnTotalComm.text = "\(messageModel.totalComm)"
        
//        if(isAddActionButton == true) {
//            addLefttButtons()
//            addRightButtons()
//        } else {
//            self.leftButtons.removeAll()
//            self.rightButtons.removeAll()
//        }
        
        if(messageModel.status.rawValue < 8) {// < 8 Unread
            //self.backgroundColor = UIColor("#e8f6eb")
            //self.labelTitle.textColor = #colorLiteral(red: 0.09803921569, green: 0.462745098, blue: 0.8235294118, alpha: 1)
            //self.labelContent.textColor = #colorLiteral(red: 0.09803921569, green: 0.462745098, blue: 0.8235294118, alpha: 1)
            self.labelTitle.font = UIFont.systemFont(ofSize: fontSize, weight: .bold)
            self.labelContent.font = UIFont.systemFont(ofSize: fontSize, weight: .bold)
        } else {
            //self.labelTitle.textColor = UIColor.darkText
            //self.labelContent.textColor = UIColor.darkText
            self.labelTitle.font = UIFont.systemFont(ofSize: fontSize, weight: .regular)
            self.labelContent.font = UIFont.systemFont(ofSize: fontSize, weight: .regular)
        }
    }
    
    func loadContent(model: WeeklyReportModel, isAddActionButton: Bool = false) {
        //self.backgroundColor = UIColor.lightGray
        
        self.labelTitle.font = UIFont.systemFont(ofSize: fontSize, weight: .semibold)
        self.labelContent.font = UIFont.systemFont(ofSize: fontSize, weight: .regular)
        
        self.labelTitle.text = model.Title
        if !model.TextColor.isEmpty {
            labelTitle.textColor = UIColor.init(model.TextColor)
        }
        
        self.labelContent.text = model.Content.trimmingCharacters(in: .whitespacesAndNewlines)
        if !model.DescriptionTextColor.isEmpty {
            self.labelContent.textColor = UIColor.init(model.DescriptionTextColor)
        }
        
        self.labelTime.text = model.DateString
        if !model.RemarkTextColor.isEmpty {
            self.labelTime.textColor = UIColor.init(model.RemarkTextColor)
        }
        
        //self.imageViewThumb.loadImageNoProgressBar(url: URL.init(string: model.ImageUrl))
        self.imageViewThumb.isHidden = true //model.ImageUrl.isEmpty
        
//        self.leftButtons.removeAll()
//        self.rightButtons.removeAll()
        
        self.btnTotalView.isHidden = true
        self.btnTotalLike.isHidden = true
        self.btnTotalComm.isHidden = true
        
        self.imgTotalView.isHidden = true
        self.imgTotalLike.isHidden = true
        self.imgTotalComm.isHidden = true
    }
    
    func addBoldText(fullString: NSString, highlightPartsOfString: Array<NSString>, font: UIFont!, highlightFont: UIFont!) -> NSAttributedString {
        let fontAttribute = [NSAttributedString.Key.font: font!]
        let highlightFontAttribute = [NSAttributedString.Key.font: highlightFont!]
        let lightColorAttribute = [NSAttributedString.Key.foregroundColor: UIColor.black]
        let fullStringAttribute = NSMutableAttributedString(string: fullString as String, attributes:fontAttribute)
        for i in 0 ..< highlightPartsOfString.count {
            fullStringAttribute.addAttributes(highlightFontAttribute, range: fullString.range(of: highlightPartsOfString[i] as String))
            fullStringAttribute.addAttributes(lightColorAttribute, range: fullString.range(of: highlightPartsOfString[i] as String))
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        let attrString = NSMutableAttributedString(string: fullString as String)
        fullStringAttribute.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range: NSMakeRange(0, attrString.length))
        
        return fullStringAttribute
    }
    
//    func addLefttButtons() {
//
//        self.leftButtons.removeAll()
//        //configure right buttons
//        self.leftSwipeSettings.transition = .clipCenter
//        self.leftSwipeSettings.keepButtonsSwiped = false
//        self.leftExpansion.expansionLayout = .center
//        self.leftExpansion.buttonIndex = 0
//        self.leftExpansion.threshold = 1.0
//        self.leftExpansion.fillOnTrigger = true
//        let button: MGSwipeButton  = MGSwipeButton(title: "Done".localizedString(), backgroundColor: UIColor.init(red: 33/255.0, green: 175/255.0, blue: 67/255.0, alpha: 1.0), callback: {[weak self]
//            (MGSwipeTableCell) -> Bool in
//            guard let `self` = self else {
//                return false
//            }
//            guard let model = self.messageModel else {
//                return false
//            }
//            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.done)
//            return true
//        })
//        button.buttonWidth = buttonWidth
//        button.titleLabel?.font = UIFont.systemFont(ofSize: fontSize + 2)
//        self.leftButtons.append(button)
//
//
//    }
//
//    func addRightButtons() {
//
//        self.rightButtons.removeAll()
//        //configure right buttons
//        self.rightSwipeSettings.transition = MGSwipeTransition.drag
//        let button3: MGSwipeButton  = MGSwipeButton(title: "No".localizedString(), backgroundColor: UIColor.red, callback: {[weak self]
//            (MGSwipeTableCell) -> Bool in
//            guard let `self` = self else {
//                return true
//            }
//            guard let model = self.messageModel else {
//                return true
//            }
//            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.replyNo)
//            return false
//        })
//        button3.buttonWidth = buttonWidth
//        button3.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
//
//        let button4: MGSwipeButton  = MGSwipeButton(title: "Yes".localizedString(), backgroundColor: UIColor("#dba510"), callback: {[weak self]
//            (MGSwipeTableCell) -> Bool in
//            guard let `self` = self else {
//                return true
//            }
//            guard let model = self.messageModel else {
//                return true
//            }
//            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.replyYes)
//            return false
//        })
//        button4.buttonWidth = buttonWidth
//        button4.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
//
//        let button5: MGSwipeButton  = MGSwipeButton(title: "Text".localizedString(), backgroundColor: UIColor("#006885"), callback: {[weak self]
//            (MGSwipeTableCell) -> Bool in
//            guard let `self` = self else {
//                return true
//            }
//            guard let model = self.messageModel else {
//                return true
//            }
//            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.replyText)
//            return false
//        })
//        button5.buttonWidth = buttonWidth
//        button5.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
//
//        let button6: MGSwipeButton  = MGSwipeButton(title: "Mark as read".localizedString(), backgroundColor: UIColor.lightGray, callback: {[weak self]
//            (MGSwipeTableCell) -> Bool in
//            guard let `self` = self else {
//                return true
//            }
//            guard let model = self.messageModel else {
//                return true
//            }
//            self.delegateAction?.actionMessageItemTableViewCell(messageModel: model, actionUpdateMessageItemType: ActionUpdateMessageItemType.mark)
//            return false
//        })
//        button6.buttonWidth = buttonWidth
//        button6.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
//
//        self.rightButtons.append(button3)
//        self.rightButtons.append(button4)
//        self.rightButtons.append(button5)
//        self.rightButtons.append(button6)
//
//
//
//    }
}
