//
//  NewFormTableViewCell.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 09/02/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell

protocol NewFormTableViewCellDelegate : NSObjectProtocol {
    func actionFormTableViewCell(action: ActionUpdateFormType, formModel: FormModel)
    func pressFormTableViewCell(cell: NewFormTableViewCell)
}

class NewFormTableViewCell: MGSwipeTableCell {
    
    // MARK: -  properties
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    
    weak var delegateAction: NewFormTableViewCellDelegate?
    
    weak var delegateAvatar: FormWorkflowCellDelegate?
    
    @IBOutlet weak var imageViewStatus: UIImageViewProgress!
    
    @IBOutlet weak var imgViewLeft: UIImageViewProgress!

    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelDescription: UILabel!
    
    @IBOutlet weak var labelCategory: UILabel!
    
    weak var formModel: FormModel?
    
    var fontSize: CGFloat = 15.0
    
    var buttonWidth: CGFloat = 120.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
            buttonWidth = 60
        }
        self.labelCategory.textColor = UIColor("#3192b5")
        self.imageViewStatus.clipsToBounds = true
        self.imageViewStatus.layer.cornerRadius = 3.0
        self.imageViewStatus.layer.borderColor = UIColor("#dddddd").cgColor
        self.imageViewStatus.layer.borderWidth = 0.0
        self.labelDescription.numberOfLines = 4
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.imgViewLeft.contentMode = .scaleAspectFit
        self.imgViewLeft.layer.borderWidth = 0.5
        self.imgViewLeft.layer.borderColor = UIColor.lightGray.cgColor
        self.imgViewLeft.layer.cornerRadius = 1.5
        self.imgViewLeft.clipsToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageViewAvatarTapped(tapGestureRecognizer:)))
        self.imgViewLeft.isUserInteractionEnabled = true
        self.imgViewLeft.addGestureRecognizer(tapGestureRecognizer)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    func loadContent(formModel: FormModel) {
        self.formModel = formModel
        self.imageViewStatus.loadImageNoProgressBar(url: URL.init(string: formModel.statusUrl))
        self.labelTitle.text = formModel.title
        self.labelCategory.text = formModel.categoryName
        self.labelDescription.text = formModel.descriptionContent
        addRightButtons([ActionUpdateFormType.remove, ActionUpdateFormType.edit])
        //addLefttButtons()
        
        if formModel.AvatarUrl.starts(with: "http") {
            self.imgViewLeft.loadImageProgress(url: URL.init(string: formModel.AvatarUrl))
        } else {
            self.imgViewLeft.image = #imageLiteral(resourceName: "ic_profile").resizeImageWith(newSize: CGSize(width: 60, height: 60))
        }
    }
    
    func addRightButtons(_ actions: Array<ActionUpdateFormType>) {
        
        self.rightButtons.removeAll()
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        
        let button3: MGSwipeButton  = MGSwipeButton(title: "Edit".localizedString(), backgroundColor: UIColor.lightGray, callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let model = self.formModel else {
                return true
            }
            self.delegateAction?.actionFormTableViewCell(action: ActionUpdateFormType.edit, formModel: model)
            return false
        })
        button3.buttonWidth = buttonWidth
        button3.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button4: MGSwipeButton  = MGSwipeButton(title: "Delete".localizedString(), backgroundColor: UIColor.red, callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let model = self.formModel else {
                return true
            }
            self.delegateAction?.actionFormTableViewCell(action: ActionUpdateFormType.remove, formModel: model)
            return false
        })
        button4.buttonWidth = buttonWidth
        button4.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        for item in actions {
            if(item == .edit) {
                self.rightButtons.append(button3)
            }else if(item == .remove) {
                self.rightButtons.append(button4)
            }
        }
    }
    
    func addLefttButtons() {
        self.leftButtons.removeAll()
        if(self.formModel?.isReadonly == true) {
            return
        }
        //configure right buttons
        self.leftSwipeSettings.transition = .clipCenter
        self.leftSwipeSettings.keepButtonsSwiped = false
        self.leftExpansion.expansionLayout = .center
        self.leftExpansion.buttonIndex = 0
        self.leftExpansion.threshold = 1.0
        self.leftExpansion.fillOnTrigger = true
        let button: MGSwipeButton  = MGSwipeButton(title: "Done".localizedString(), backgroundColor: UIColor.init(red: 33/255.0, green: 175/255.0, blue: 67/255.0, alpha: 1.0), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return false
            }
            guard let model = self.formModel else {
                return false
            }
            self.delegateAction?.actionFormTableViewCell(action: ActionUpdateFormType.done, formModel: model)
            return true
        })
        //button.buttonWidth = buttonWidth
        button.titleLabel?.font = UIFont.systemFont(ofSize: fontSize + 3)
        self.leftButtons.append(button)
    }
    
    @objc func imageViewAvatarTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        if (tapGestureRecognizer.view as? UIImageView) != nil {
            self.delegateAvatar?.imageViewAvatarTapped(self.formModel?.AvatarUrl ?? "")
        }
    }
}
