//
//  ListFormWorkflowTableViewCell.swift
//  VNA
//
//  Created by Nhan Bá Đoàn on 19/01/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class ListFormWorkflowTableViewCell: UITableViewCell, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let numOfSections: Int = listOriginal.count
        if numOfSections == 0 {
            tableView.showNoData()
        } else {
            tableView.removeNoData()
        }
        return numOfSections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FormWorkflowCell.identifier, for: indexPath) as? FormWorkflowCell  else {
            return UITableViewCell()
        }
        cell.setData(data: listOriginal[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return []
    }
    
    func setData(_ list: [FormWorkflowModel] = []) {
        self.listOriginal = list
        tableView.reloadData()
    }

    private func config() {
        self.listOriginal = formModel?.workFlows ?? []
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView.register(FormWorkflowCell.nib, forCellReuseIdentifier: FormWorkflowCell.identifier)
        tableView.tableFooterView = UIView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        config()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    // MARK: -  properties
    var listOriginal: [FormWorkflowModel] = []
    var formModel: FormModel? = nil
    
    @IBOutlet weak var tableView: UITableView!
}
