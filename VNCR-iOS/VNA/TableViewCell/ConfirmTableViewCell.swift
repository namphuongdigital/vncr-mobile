//
//  ConfirmTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/3/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class ConfirmTableViewCell: UITableViewCell {
    
    @IBOutlet weak var textNameLabel: UILabel!
    
    @IBOutlet weak var buttonConfirm: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.buttonConfirm.setTitle("Confirm".localizedString(), for: UIControl.State.normal)
        self.buttonConfirm.setTitleColor(UIColor.white, for: UIControl.State.normal)
        self.buttonConfirm.backgroundColor = UIColor("#006885")
        self.buttonConfirm.layer.cornerRadius = 2.5
    }
}
