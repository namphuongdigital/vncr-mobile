//
//  SupportItemTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 4/6/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell


protocol SupportItemTableViewCellDelegate : NSObjectProtocol {
    func actionSupportItemTableViewCell(action: ActionUpdateSupportType, supportModel: SupportModel)
    func pressImageSupportItemTableViewCell(cell: SupportItemTableViewCell)
}

class SupportItemTableViewCell: MGSwipeTableCell {
    
    weak var delegateAction: SupportItemTableViewCellDelegate?
    
    @IBOutlet weak var imageViewAttach: UIImageViewProgress?
    
    @IBOutlet weak var buttonAction: UIButton?
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var labelDate: UILabel!
    
    @IBOutlet weak var labelContent: UILabel!
    
    @IBOutlet weak var heightListImageView: NSLayoutConstraint?
    
    @IBOutlet weak var heightTextContent: NSLayoutConstraint?
    
    @IBOutlet weak var labelStatus: UILabel!
    
    @IBOutlet weak var viewStatus: UIView!
    
    var fontSize: CGFloat = 15.0
    var buttonWidth: CGFloat = 120.0
    
    weak var supportModel: SupportModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
            buttonWidth = 60
        }
        
        self.buttonAction?.setFAIcon(icon: .FAChevronDown, iconSize: 10, forState: UIControl.State())
        //self.imageViewAvatar.layer.backgroundColor = UIColor.init(white: 0.886, alpha: 1.000).cgColor
        self.imageViewAvatar.clipsToBounds = true
        self.imageViewAvatar.layer.cornerRadius = 3.0
        self.imageViewAvatar.layer.borderColor = UIColor("#dddddd").cgColor
        self.imageViewAvatar.layer.borderWidth = 0.4
        
        
        
        viewStatus?.backgroundColor = UIColor.clear
        self.viewStatus?.clipsToBounds = true
        self.viewStatus?.layer.cornerRadius = 2.0
        self.viewStatus?.layer.borderColor = UIColor("#dddddd").cgColor
        self.viewStatus?.layer.borderWidth = 0.6
        
        self.labelStatus?.textColor = UIColor.orange
        
        self.imageViewAttach?.clipsToBounds = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageViewAttachTapped(tapGestureRecognizer:)))
        imageViewAttach?.isUserInteractionEnabled = true
        imageViewAttach?.addGestureRecognizer(tapGestureRecognizer)
        
        
        self.labelName.textColor = UIColor("#166880")
        
        //self.buttonAction.addTarget(self, action: #selector(buttonActionPress(sender:)), for: .touchUpInside)
        
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = UIScreen.main.scale
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @objc func imageViewAttachTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        guard let supportModel = self.supportModel else {
            return
        }
        self.delegateAction?.pressImageSupportItemTableViewCell(cell: self)
    }

    
    func loadData(supportModel: SupportModel, actions: Array<ActionUpdateSupportType> = [ActionUpdateSupportType.complete, ActionUpdateSupportType.take, ActionUpdateSupportType.hide, ActionUpdateSupportType.remove]) {
        self.supportModel = supportModel
        self.labelName.text = supportModel.sender
        self.labelDate.text = supportModel.date
        self.labelContent.text = supportModel.message
        self.labelStatus?.text = supportModel.statusText
       
        if(supportModel.attachments.count > 0) {
            if let imageViewAttach = self.imageViewAttach {
                imageViewAttach.isHidden = false
                imageViewAttach.loadImageNoProgressBar(url: URL.init(string: supportModel.attachments[0].thumbnailPath))
                if(supportModel.parentID == 0) {
                    if(UIDevice.current.userInterfaceIdiom == .pad) {
                        self.heightListImageView?.constant = UIScreen.main.bounds.size.width / 2.0
                    } else {
                        self.heightListImageView?.constant = UIScreen.main.bounds.size.width
                    }
                } else {
                    if(UIDevice.current.userInterfaceIdiom == .pad) {
                        self.heightListImageView?.constant = (UIScreen.main.bounds.size.width / 2.0) - 40
                    } else {
                        self.heightListImageView?.constant = UIScreen.main.bounds.size.width - 40
                    }
                }
                
                
                self.heightTextContent?.constant = 1
            }
            
        } else {
            self.heightListImageView?.constant = 1
            self.imageViewAttach?.isHidden = true
        }
        self.imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: supportModel.avatar))
        
        addRightButtons(actions)
        
        
    }
    
    func addRightButtons(_ actions: Array<ActionUpdateSupportType>) {
        
        self.rightButtons.removeAll()
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        let button1: MGSwipeButton  = MGSwipeButton(title: "Done".localizedString(), backgroundColor: UIColor("#166880"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let supportModel = self.supportModel else {
                return true
            }
            self.delegateAction?.actionSupportItemTableViewCell(action: ActionUpdateSupportType.complete, supportModel: supportModel)
            return false
        })
        button1.buttonWidth = buttonWidth
        button1.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button2: MGSwipeButton  = MGSwipeButton(title: "Take".localizedString(), backgroundColor: UIColor("#33cc90"), callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let supportModel = self.supportModel else {
                return true
            }
            self.delegateAction?.actionSupportItemTableViewCell(action: ActionUpdateSupportType.take, supportModel: supportModel)
            return false
        })
        button2.buttonWidth = buttonWidth
        button2.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button3: MGSwipeButton  = MGSwipeButton(title: "Hide".localizedString(), backgroundColor: UIColor.lightGray, callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let supportModel = self.supportModel else {
                return true
            }
            self.delegateAction?.actionSupportItemTableViewCell(action: ActionUpdateSupportType.hide, supportModel: supportModel)
            return false
        })
        button3.buttonWidth = buttonWidth
        button3.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        let button4: MGSwipeButton  = MGSwipeButton(title: "Delete".localizedString(), backgroundColor: UIColor.red, callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            guard let `self` = self else {
                return true
            }
            guard let supportModel = self.supportModel else {
                return true
            }
            self.delegateAction?.actionSupportItemTableViewCell(action: ActionUpdateSupportType.remove, supportModel: supportModel)
            return false
        })
        button4.buttonWidth = buttonWidth
        button4.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        for item in actions {
            if(item == .complete) {
                self.rightButtons.append(button1)
            } else if(item == .take) {
                self.rightButtons.append(button2)
            } else if(item == .hide) {
                self.rightButtons.append(button3)
            }else if(item == .remove) {
                self.rightButtons.append(button4)
            }
        }
        
        
        
    }
    
}
