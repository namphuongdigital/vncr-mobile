//
//  ContentTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 7/15/18.
//  Copyright © 2018 OneTeam. All rights reserved.
//

import UIKit

protocol ContentTableViewCellDelegate : NSObjectProtocol {
    func tapAvatarContentTableViewCell(cell: ContentTableViewCell)
    func tapInfoButtonContentTableViewCell(cell: ContentTableViewCell)
}

class ContentTableViewCell: UITableViewCell {
    
    weak var contentTableViewCellDelegate: ContentTableViewCellDelegate?

    @IBOutlet weak var infoButton: UIButton!

    @IBOutlet weak var textTitleLabel: UILabel!
    
    @IBOutlet weak var textContentLabel: UILabel!
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    weak var contentModel: ContentModel? {
        didSet {
            if(contentModel != nil) {
                textTitleLabel.text = contentModel!.title
                textContentLabel.text = contentModel!.descriptionContent
                textTitleLabel.textColor = UIColor(contentModel!.color)
                textContentLabel.textColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
                imageViewAvatar.loadImageNoProgressBar(url: URL.init(string: contentModel!.imageUrl))
                textContentLabel.addLineSpacing(space: 2)
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        self.imageViewAvatar.clipsToBounds = true
        
        imageViewAvatar.isUserInteractionEnabled = true
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageAvatarTapped(tapGestureRecognizer:)))
        imageViewAvatar.isUserInteractionEnabled = true
        imageViewAvatar.addGestureRecognizer(tapGestureRecognizer)
        
        self.infoButton.setImage(UIImage.init(named: "UserInfo-icon.png")?.withRenderingMode(.alwaysTemplate), for: UIControl.State())
        self.infoButton.tintColor = UIColor("#166880")
        self.infoButton.addTarget(self, action: #selector(self.infoButtonPressed(_:)), for: .touchUpInside)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @objc func infoButtonPressed(_ sender: UIButton) {
        self.contentTableViewCellDelegate?.tapInfoButtonContentTableViewCell(cell: self)
    }
    
    @objc func imageAvatarTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        self.contentTableViewCellDelegate?.tapAvatarContentTableViewCell(cell: self)
    }
    
}
