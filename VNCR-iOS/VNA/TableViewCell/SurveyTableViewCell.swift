//
//  SurveyTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 11/30/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell

protocol SurveyTableViewCellDelegate : NSObjectProtocol {
    func deleteSurveyTableViewCellPress(surveyModel: SurveyModel)
}

class SurveyTableViewCell: MGSwipeTableCell {
    
    weak var surveyTableViewCellDelegate: SurveyTableViewCellDelegate?
    
    weak var surveyModel: SurveyModel? {
        didSet {
            self.rightButtons.removeAll()
            if(surveyModel != nil) {
                textNameLabel.text = surveyModel!.fullName
                textNameLabel.textColor = UIColor(surveyModel!.textColor)
                self.imageViewStatus.loadImageNoProgressBar(url: URL.init(string: surveyModel!.statusUrl))
                if(self.surveyTableViewCellDelegate != nil) {
                    self.addRightButtons()
                }
            }
        }
    }
    
    @IBOutlet weak var textNameLabel: UILabel!
    
    @IBOutlet weak var imageViewStatus: UIImageViewProgress!
    
    
    var fontSize: CGFloat = 15.0
    var buttonWidth: CGFloat = 120.0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
            buttonWidth = 60
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func addRightButtons() {
        
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        
        let button1: MGSwipeButton = MGSwipeButton(title: "Delete".localizedString(), backgroundColor: UIColor.red, callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            if let surveyModel = self.surveyModel {
                self.surveyTableViewCellDelegate?.deleteSurveyTableViewCellPress(surveyModel: surveyModel)
            }
            
            return true
        })
        button1.buttonWidth = buttonWidth
        button1.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        self.rightButtons = [button1]
        
    }
    
}
