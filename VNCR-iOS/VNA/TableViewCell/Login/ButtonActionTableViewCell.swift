//
//  ButtonLoginTableViewCell.swift
//  MBN-iOS-App
//
//  Created by Van Trieu Phu Huy on 11/17/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import UIKit

class ButtonActionTableViewCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var buttonAction: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        buttonAction.backgroundColor = UIColor("#0b7e07")
        buttonAction.layer.borderColor = UIColor("#0b7e07").cgColor
        buttonAction.layer.cornerRadius = 2.5
        buttonAction.setTitleColor(UIColor.white, for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
