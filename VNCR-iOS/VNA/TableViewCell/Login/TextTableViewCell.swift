//
//  PhoneTextTableViewCell.swift
//  MBN-iOS-App
//
//  Created by Van Trieu Phu Huy on 11/18/16.
//  Copyright © 2016 ePepaperSmart. All rights reserved.
//

import UIKit


enum ValidationResult: Int {
    case valid = 0
    case invalid = 1
}

class TextTableViewCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    
    @IBOutlet weak var textFieldInput: TextFieldValidator!
    
    @IBOutlet weak var labelIconInput: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewContainer.layer.borderColor = UIColor.clear.cgColor
        viewContainer.layer.borderWidth = 0.7
        viewContainer.layer.cornerRadius = 2.5
        viewContainer.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateValidationState(result: ValidationResult) {
        switch result {
        case .valid:
            viewContainer.layer.borderColor = UIColor.clear.cgColor
        case .invalid:
            textFieldInput.becomeFirstResponder()
            viewContainer.layer.borderColor = UIColor.red.cgColor
        }
    }
}
