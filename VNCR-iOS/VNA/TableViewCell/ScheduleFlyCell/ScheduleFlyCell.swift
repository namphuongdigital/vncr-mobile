//
//  ScheduleFlyCell.swift
//  VNA
//
//  Created by Dai Pham on 07/07/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class ScheduleFlyCell: BaseTableViewCell {

    @IBOutlet weak var lblFlightInfo: UILabel!
    @IBOutlet weak var lblDepartureCode: UILabel!
    @IBOutlet weak var lblArrivalCode: UILabel!
    @IBOutlet weak var lblDeaprtureNameAirport: UILabel!
    @IBOutlet weak var lblArrivalNameAirport: UILabel!
    @IBOutlet weak var lblTimeStart: UILabel!
    @IBOutlet weak var lblTimeArrival: UILabel!
    @IBOutlet weak var btnGoto: UIButton!
    @IBOutlet weak var iconFlight: UIImageView!
    @IBOutlet weak var lblFlightDate: UILabel!
    
    @IBOutlet weak var lblFlyTop: ButtonHalfHeight!
    @IBOutlet weak var lblFlyBottom: ButtonHalfHeight!
    @IBOutlet weak var lblPosTop: ButtonHalfHeight!
    @IBOutlet weak var lblPosBottom: ButtonHalfHeight!
    @IBOutlet weak var lblRateTop: ButtonHalfHeight!
    @IBOutlet weak var lblRateBottom: ButtonHalfHeight!
    
    @IBOutlet weak var vwPrivateInfor: UIView!
    @IBOutlet weak var seprateLine: UIView!
    
    
    var scheduleFly: ScheduleFlightModel?
    var onGoto:((ScheduleFlyCell)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let viewSelected = UIView()
        if UIDevice.current.userInterfaceIdiom == .pad { // affect only ipad
            viewSelected.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
        }
        selectedBackgroundView = viewSelected
        layoutMargins = .zero
        
        lblArrivalNameAirport.font = .systemFont(ofSize: 12, weight: .regular)
        lblArrivalNameAirport.textColor = .gray
        
        lblDeaprtureNameAirport.font = .systemFont(ofSize: 12, weight: .regular)
        lblDeaprtureNameAirport.textColor = .gray
        
        lblDepartureCode.font = .systemFont(ofSize: 20, weight: .medium)
        lblArrivalCode.font = .systemFont(ofSize: 20, weight: .medium)
        
        lblFlightInfo.font = .systemFont(ofSize: 20, weight: .medium)
        
        lblFlightDate.font = .systemFont(ofSize: 16, weight: .medium)
        lblFlightDate.textColor = .blue
        
        lblTimeStart.font = .systemFont(ofSize: 12)
        lblTimeArrival.font = .systemFont(ofSize: 12)
        
        btnGoto.setTitle("GO TO".localizedString(), for: UIControl.State())
        btnGoto.setImage(#imageLiteral(resourceName: "ic_arrow_right").resizeImage(newSize: CGSize(width: 20, height: 20)), for: UIControl.State())
        btnGoto.semanticContentAttribute = .forceRightToLeft
        btnGoto.setTitleColor(UIColor.gray, for: .normal)
        btnGoto.titleLabel?.font = UIFont.systemFont(ofSize: 14)
        btnGoto.tintColor = .gray
        
        iconFlight.image = #imageLiteral(resourceName: "ic_flightTo")
        
        [lblFlyTop,lblFlyBottom,lblPosTop,lblPosBottom,lblRateTop,lblRateBottom].forEach {
            if $0?.isEqual(lblPosTop) == true {
                $0?.titleLabel?.font = .systemFont(ofSize: 10)
            } else {
                $0?.titleLabel?.font = .systemFont(ofSize: 12)
            }
        }
        
        vwPrivateInfor.isHidden = (ServiceData.sharedInstance.userModel?.isGuest ?? false)
        seprateLine.isHidden = vwPrivateInfor.isHidden
    }
    
    func show(scheduleFly: ScheduleFlightModel?) {
        guard let scheduleFly = scheduleFly else {return}
        self.scheduleFly = scheduleFly
        
        self.lblFlightInfo.text = scheduleFly.flightNo
        self.lblFlightInfo.textColor = UIColor(scheduleFly.fcolor)
        
        lblDepartureCode.text = scheduleFly.departureCodeValue
        lblArrivalCode.text = scheduleFly.arrivalCodeValue
        
        lblArrivalNameAirport.text = scheduleFly.arrivalNameValue
        lblDeaprtureNameAirport.text = scheduleFly.departureNameValue
        
        self.lblTimeStart.text = scheduleFly.departureTimeValue
        self.lblTimeArrival.text = scheduleFly.arrivalTimeValue
        
        lblDepartureCode.textColor = UIColor(scheduleFly.departureCodeTextColor)
        lblArrivalCode.textColor = UIColor(scheduleFly.arrivalCodeTextColor)
        
        lblArrivalNameAirport.textColor = UIColor(scheduleFly.arrivalNameTextColor)
        lblDeaprtureNameAirport.textColor = UIColor(scheduleFly.departureNameTextColor)
        
        self.lblTimeStart.textColor = UIColor(scheduleFly.departureTimeTextColor)
        self.lblTimeArrival.textColor = UIColor(scheduleFly.arrivalTimeTextColor)
        
        lblFlightDate.textColor = UIColor(scheduleFly.fcolor)
        lblFlightDate.text = scheduleFly.flightDate
        
        lblFlyTop.setTitle(scheduleFly.icon1Top, for: UIControl.State())
        lblPosTop.setTitle(scheduleFly.icon2Top, for: UIControl.State())
        lblRateTop.setTitle(scheduleFly.icon3Top, for: UIControl.State())
        
        lblFlyBottom.setTitle(scheduleFly.icon1Bot, for: UIControl.State())
        lblPosBottom.setTitle(scheduleFly.icon2Bot, for: UIControl.State())
        lblRateBottom.setTitle(scheduleFly.icon3Bot, for: UIControl.State())
        
        lblFlyTop.backgroundColor = scheduleFly.icon1Top.count == 0 ? .clear : UIColor(scheduleFly.icon1State)
        lblPosTop.backgroundColor = scheduleFly.icon2Top.count == 0 ? .clear : UIColor(scheduleFly.icon2State)
        lblRateTop.backgroundColor = scheduleFly.icon3Top.count == 0 ? .clear : UIColor(scheduleFly.icon3State)
        
        lblFlyBottom.backgroundColor = scheduleFly.icon1Bot.count == 0 ? .clear : UIColor(scheduleFly.icon1StateBot)
        lblPosBottom.backgroundColor = scheduleFly.icon2Bot.count == 0 ? .clear : UIColor(scheduleFly.icon2StateBot)
        lblRateBottom.backgroundColor = scheduleFly.icon3Bot.count == 0 ? .clear : UIColor(scheduleFly.icon3StateBot)
        
        lblFlyTop.setTitleColor(UIColor(scheduleFly.icon1TextColor), for: UIControl.State())
        lblPosTop.setTitleColor(UIColor(scheduleFly.icon2TextColor), for: UIControl.State())
        lblRateTop.setTitleColor(UIColor(scheduleFly.icon3TextColor), for: UIControl.State())
        
        lblFlyBottom.setTitleColor(UIColor(scheduleFly.icon1TextColorBot), for: UIControl.State())
        lblPosBottom.setTitleColor(UIColor(scheduleFly.icon2TextColorBot), for: UIControl.State())
        lblRateBottom.setTitleColor(UIColor(scheduleFly.icon3TextColorBot), for: UIControl.State())
    }
    
    @IBAction func goto(_ sender: Any) {
        self.onGoto?(self)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        scheduleFly = nil
    }
}
