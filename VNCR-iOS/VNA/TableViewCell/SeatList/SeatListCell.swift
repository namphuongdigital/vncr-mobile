//
//  SeatListCell.swift
//  VNA
//
//  Created by Dai Pham on 23/06/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class SeatListCell: BaseTableViewCell {

    // MARK: -  outlets
    @IBOutlet weak var lblSeat: RoundBorderLabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    // MARK: -  properties
    var passenger:PassengerSeat?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        lblSeat.font = UIFont.systemFont(ofSize: 18, weight: .bold)
    }
    
    func show(passenger:PassengerSeat?) {
        self.passenger = passenger
        
        lblSeat.text = passenger?.seat
        
        lblTitle.text = passenger?.fullName
        lblTitle.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        
        lblDescription.text = passenger?.desc
        lblDescription.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        lblDescription.isHidden = passenger?.desc?.trimmingCharacters(in: .whitespacesAndNewlines).count == 0
        
        lblTitle.textColor = passenger?.titleColor
        lblDescription.textColor = passenger?.descColor

        lblSeat.textColor = passenger?.iconColor
        lblSeat.borderColor = passenger?.iconBorderColor
        lblSeat.backgroundColor = passenger?.iconBackgroundColor
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.passenger = nil
        lblDescription.isHidden = true
    }
}
