//
//  OJTTableViewCell.swift
//  VNA
//
//  Created by Van Trieu Phu Huy on 12/10/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit
import MGSwipeTableCell


protocol OJTTableViewCellDelegate : NSObjectProtocol {
    func deleteOJTTableViewCellPress(ojtModel: OJTModel)
}


class OJTTableViewCell: MGSwipeTableCell {

    weak var ojtTableViewCellDelegate: OJTTableViewCellDelegate?
    
    @IBOutlet weak var textNameLabel: UILabel!
    
    @IBOutlet weak var imageViewStatus: UIImageViewProgress!
    
    weak var ojtModel: OJTModel? {
        didSet {
            self.rightButtons.removeAll()
            if(ojtModel != nil) {
                textNameLabel.text = ojtModel!.crOJTLesson
                textNameLabel.textColor = UIColor(ojtModel!.textColor)
                self.imageViewStatus.loadImageNoProgressBar(url: URL.init(string: ojtModel!.statusUrl))
                if(self.ojtTableViewCellDelegate != nil) {
                    self.addRightButtons()
                }
            }
        }
    }
    
    var fontSize: CGFloat = 15.0
    var buttonWidth: CGFloat = 120.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if(UIDevice.current.userInterfaceIdiom == .phone) {
            fontSize = 12
            buttonWidth = 60
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func addRightButtons() {
        
        //configure right buttons
        self.rightSwipeSettings.transition = MGSwipeTransition.drag
        
        let button1: MGSwipeButton = MGSwipeButton(title: "Delete".localizedString(), backgroundColor: UIColor.red, callback: {[weak self]
            (MGSwipeTableCell) -> Bool in
            // -- More button clicked
            guard let `self` = self else {
                return true
            }
            if let ojtModel = self.ojtModel {
                self.ojtTableViewCellDelegate?.deleteOJTTableViewCellPress(ojtModel: ojtModel)
            }
            
            return true
        })
        button1.buttonWidth = buttonWidth
        button1.titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        self.rightButtons = [button1]
        
    }
    
}
