//
//  ItemMenuCollectionViewCell.swift
//  CAVA
//
//  Created by Van Trieu Phu Huy on 9/15/18.
//  Copyright © 2018 Van Trieu Phu Huy. All rights reserved.
//

import UIKit

class ItemMenuCollectionViewCell: UICollectionViewCell {
    
    weak var menuItemModel: MenuItemModel? {
        didSet {
            self.loadData()
        }
    }
    
    @IBOutlet weak var textBagdesLabel: UILabel!
    
    @IBOutlet weak var textBagdesView: UIView!
    
    @IBOutlet weak var textTitleLabel: UILabel!
    
    @IBOutlet weak var textContentLabel: UILabel!
    
    @IBOutlet weak var imageViewAvatar: UIImageViewProgress!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imageViewAvatar.clipsToBounds = true
        
        self.textBagdesView.clipsToBounds = true
        self.textBagdesView.layer.cornerRadius = self.textBagdesView.frame.height / 2.0
        self.textBagdesView.backgroundColor = UIColor.red
        self.textBagdesLabel.textColor = UIColor.white
        
        self.selectedBackgroundView = UIView(frame: self.bounds)
        self.selectedBackgroundView?.backgroundColor = UIColor.lightGray.withAlphaComponent(0.5)
    }
    
    func loadData() {
        if let menuItemModel = self.menuItemModel {
            if(menuItemModel.BadgeValue.count == 0) {
                self.textBagdesView.isHidden = true
            } else {
                self.textBagdesView.isHidden = false
                self.textBagdesLabel.text = String.init(format: "%@", menuItemModel.BadgeValue)
            }
            
            self.imageViewAvatar.loadImageNoProgressBar(url: URL(string: menuItemModel.imageUrl))
            self.textTitleLabel.text = menuItemModel.titleVN
            self.textContentLabel?.text = menuItemModel.descriptionContent
        }
    }
    
    override func prepareForReuse() {
        self.imageViewAvatar.cancelLoadImageProgress()
        self.imageViewAvatar.image = nil
    }

}
