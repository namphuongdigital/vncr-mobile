//
//  TermsOfConditionsCell.swift
//  VNA
//
//  Created by Pham Dai on 08/08/2021.
//  Copyright © 2021 OneTeam. All rights reserved.
//

import UIKit

class TermsOfConditionsCell: UITableViewHeaderFooterView {

    @IBOutlet weak var btnTerm: UIButton!
    @IBOutlet weak var txtTerm: UITextView!
    
    var onPresentTermsAndConditions:(()->Void)?
    
    var isAccepted:Bool {btnTerm.isSelected}
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        
        [btnTerm].forEach({
            $0?.setImage(#imageLiteral(resourceName: "ic_checkbox"), for: .normal)
            $0?.setImage(#imageLiteral(resourceName: "ic_checkedbox"), for: .selected)
            $0?.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
            $0?.isSelected = true
            $0?.titleLabel?.textAlignment = .left
            $0?.contentMode = .left
        })
        
        txtTerm.delegate = self
        
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineSpacing = 4
        paragraph.alignment = .left
        let mutableAttributedString = NSMutableAttributedString(string: "I agree to the ".localizedString() + "Terms And Conditions".localizedString(), attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15), .paragraphStyle:paragraph, .foregroundColor : UIColor.black])
        mutableAttributedString.setAsLink(textToFind: "Terms And Conditions".localizedString(), linkName: "term")
        txtTerm.attributedText = mutableAttributedString
        
        txtTerm.contentInset = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        
        [txtTerm].forEach{
            ($0?.superview as? UIStackView)?.alignment = UIDevice.current.userInterfaceIdiom == .pad ? .center : .top
        }
    }
    
    @IBAction func action(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    override func prepareForReuse() {
        btnTerm.isSelected = true
    }
}

extension TermsOfConditionsCell: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        if URL.absoluteString == "term"{
            self.onPresentTermsAndConditions?()
            return true
        }
        return false
    }
}
