

Replace: /Flight/InfoListExcludeTrip => /Flight/GetGeneralScheduleFlights
Replace: /Flight/PersonalSchedule => /Flight/GetPersonalScheduleFlights (Lưu ý UI cũ có phần Trips (field: Trips))

New value 
{
"DepartureCodeValue": "HAN",
"DepartureCodeTextColor": "#000000",
"DepartureNameValue": "HAN AIRPORT",
"DepartureNameTextColor": "#D3D3D3",
"DepartureTimeValue": "23:30|19 Jul",
"DepartureTimeTextColor": "#E5EE7E",
"ArrivalCodeValue": "CAN",
"ArrivalCodeTextColor": "#000000",
"ArrivalNameValue": "CAN AIRPORT",
"ArrivalNameTextColor": "#D3D3D3",
"ArrivalTimeValue": "02:00|20 Jul",
"ArrivalTimeTextColor": "#006400"
}