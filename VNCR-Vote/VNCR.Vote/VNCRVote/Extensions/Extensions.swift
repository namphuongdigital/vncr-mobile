//
//  Extensions.swift
//  BUUP
//
//  Created by Nhan Ba Doan on 1/7/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation
import QuartzCore
import UIKit
import AVKit

// MARK: - STRING
extension String {
    
    func getListSub(patterns:[String]) -> [String] {
        var matchResults:[String] = []
        for pattern in patterns {
            if let regex = try? NSRegularExpression(pattern: pattern, options: NSRegularExpression.Options.caseInsensitive) {
                let uls = regex.matches(in: self, options: NSRegularExpression.MatchingOptions.init(rawValue: 0), range: NSMakeRange(0, self.count))
                
                for match in uls {
                    matchResults.append((self as NSString).substring(with: match.range))
                }
            }
        }
        return matchResults
    }
    
    func trim(pattern:[String]) -> String {
        var temp:String = self
        for p in pattern {
            temp = temp.replacingOccurrences(of: p, with: "", options: .regularExpression)
        }
        return temp
    }
    
    func removeScript() -> String {
        return self.trim(pattern: ["</(.*)>", "<(.*)/>"])
    }
    
    func isValidEmail() -> Bool {
        var returnValue = true
        let emailRegEx = "(?:[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[\\p{L}0-9!#$%\\&'*+/=?\\^_`{|}" +
            "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\" +
            "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[\\p{L}0-9](?:[a-" +
            "z0-9-]*[\\p{L}0-9])?\\.)+[\\p{L}0-9](?:[\\p{L}0-9-]*[\\p{L}0-9])?|\\[(?:(?:25[0-5" +
            "]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-" +
            "9][0-9]?|[\\p{L}0-9-]*[\\p{L}0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21" +
        "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = self as NSString
            let results = regex.matches(in: self, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    func isValidPassword() -> Bool {
        var returnValue = true
        if(self.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count < 6 || self.getListSub(patterns: ["</(.*)>", "<(.*)/>"]).count > 0){
            returnValue = false
        }
        if (self as NSString).substring(to: 0) == " " {
            returnValue = false
        }
        return returnValue
    }
    
    func toDate() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: self) ?? Date.init(timeIntervalSinceNow: 0)
    }
    
    func toDate1(_ format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: self) ?? Date.init(timeIntervalSinceNow: 0)
    }
    
    func toDate2() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: self) ?? Date.init(timeIntervalSinceNow: 0)
    }
    
    func toDate3() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.date(from: self) ?? Date.init(timeIntervalSinceNow: 0)
    }
    
    func localToUTC(format:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: self)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = format
        
        return dateFormatter.string(from: dt!)
    }
    
    func UTCToLocal(format:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let dt = dateFormatter.date(from: self)
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter1.dateFormat = format
        dateFormatter1.timeZone = TimeZone.current
        return dateFormatter1.string(from: dt!)
    }
    
    func isoDateToDate() -> Date? {
        if self.isEmpty { return nil }
        //        let dateFormatter = DateFormatter()
        //        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        //        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        //        let date = dateFormatter.date(from: self)!
        //        let calendar = Calendar.current
        //        let components = calendar.dateComponents([.year, .month, .day, .hour], from: date)
        //        let finalDate = calendar.date(from:components)
        //        return finalDate
        //let string = "2017-07-11T06:52:15.948Z"
        let string = (self as NSString).substring(to: 19)
        let dateFormatter = DateFormatter()
        //let tempLocale = dateFormatter.locale // save locale temporarily
        dateFormatter.locale = Locale(identifier: "en_US_POSIX") // set locale to reliable US_POSIX
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"//.SSSS
        let date = dateFormatter.date(from: string)!
        return date
        //dateFormatter.dateFormat = "MMM d, yyyy" //"dd-MM-yyyy HH:mm:ss"
        //dateFormatter.locale = tempLocale // reset the locale --> but no need here
        //let dateString = dateFormatter.string(from: date)
        
    }
    
    func unixTimestampToDate() -> Date? {
        if self.isEmpty { return nil }
        let date = Date(timeIntervalSince1970: Double(self) ?? 0)
        return date
    }
    
    func getDateNowString(format: String) -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
        let result = formatter.string(from: date)
        return result
    }
    
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.localizedBundle(), value: "", comment: "")
    }
    
    func localizeWithFormat(arguments: [CVarArg]) -> String{
        return String(format: self.localized(), arguments: arguments)
    }
    
    func removeUnicode() -> String {
        var s = self.folding(options: .diacriticInsensitive, locale: Locale(identifier: "en_US"))
        s = s.replacingOccurrences(of: "Đ", with: "D")
        s = s.replacingOccurrences(of: "đ", with: "d")
        return s
    }
    
    func removeUnicodeAndLowerCased() -> String {
        return self.removeUnicode().lowercased()
    }
}

// MARK: - BUNDLE
extension Bundle {
    private static var bundle: Bundle!
    
    public static func localizedBundle() -> Bundle! {
        if bundle == nil {
            let path = Bundle.main.path(forResource: AppHelper.shared.LANGUAGE_OF_DEVICE, ofType: "lproj")
            bundle = Bundle(path: path!)
        }
        return bundle
    }
    
    public static func setLanguage(lang: String) {
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
    }
}

// MARK: - DOUBLE
extension Double {
    
    func unixTimestampToDate() -> Date? {
        if self == 0 { return nil }
        let date = Date(timeIntervalSince1970: self)
        return date
    }
    
    func unixTimestampToDate() -> Date {
        if self == 0 { return Date() }
        let date = Date(timeIntervalSince1970: self)
        return date
    }
    
}

// MARK: - DATE
extension Date {
    
    func toUnixTimestamp() -> Int64 {
        
        return Int64(self.timeIntervalSince1970)
        
    }
    
    func toUTCDate() -> Date {
        
        let sNow = self.toString(dateFormat: "yyyy-MM-dd HH:mm:ss").localToUTC(format: "yyyy-MM-dd HH:mm:ss")
        return sNow.toDate2()
        
    }
    
    func addedByDay(day: Int) -> Date {
        
        //return self.addedBy(minutes: day * 24 * 60)
        var dateComponent = DateComponents()
        dateComponent.day = day
        return Calendar.current.date(byAdding: dateComponent, to: self)!
    }
    
    func addedByWeek(week: Int) -> Date {
        
        return self.addedByDay(day: week * 7)
        
    }
    
    func addedByMonth(month: Int) -> Date {
        
        var dateComponent = DateComponents()
        dateComponent.month = month
        return Calendar.current.date(byAdding: dateComponent, to: self)!
        
    }
    
    func addedByYear(year: Int) -> Date {
        
        var dateComponent = DateComponents()
        dateComponent.year = year
        return Calendar.current.date(byAdding: dateComponent, to: self)!
        
    }
    
    func convertDateFormat(from: String, to: String, dateString: String?) -> String? {
        let fromDateFormatter = DateFormatter()
        fromDateFormatter.dateFormat = from
        var formattedDateString: String? = nil
        if dateString != nil {
            let formattedDate = fromDateFormatter.date(from: dateString!)
            if formattedDate != nil {
                let toDateFormatter = DateFormatter()
                toDateFormatter.dateFormat = to
                formattedDateString = toDateFormatter.string(from: formattedDate!)
            }
        }
        return formattedDateString
    }
    
    func toString( dateFormat format  : String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    func addedBy(seconds:Int) -> Date {
        return Calendar.current.date(byAdding: .second, value: seconds, to: self)!
    }
    
    func addedBy(minutes:Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func toChatString() -> String {
        
        let currentDate = Date()
        let calendar = Calendar.current
        
        let cYear = calendar.component(.year, from: currentDate)
        let cMonth = calendar.component(.month, from: currentDate)
        let cDay = calendar.component(.day, from: currentDate)
        
        let sYear = calendar.component(.year, from: self)
        let sMonth = calendar.component(.month, from: self)
        let sDay = calendar.component(.day, from: self)
        
        if (cYear == sYear && cMonth == sMonth && cDay == sDay) {
            
            let components = Calendar.current.dateComponents([.minute], from: self, to: currentDate)
            let totalMinutes = components.minute ?? 0
            if (totalMinutes < 1) {
                return "just now".localized()
            } else if (totalMinutes == 1) {
                return "a min ago".localized()
            } else if (totalMinutes < 60) {
                return String(totalMinutes) + " mins ago".localized()
            } else {
                return self.toString(dateFormat: "HH:mm")
            }
            
        }
        
        return self.toString(dateFormat: "longdateformat".localized())
    }
    
    func toDisplaySecondFromNow(_ prefix: String = "", _ textToZero: String = "0") -> String {
        let currentDate = Date()
        let components = Calendar.current.dateComponents([.second], from: currentDate, to: self)
        let totalSeconds = components.second ?? 0
        
        if totalSeconds <= 0 {
            return textToZero
        } else {
            return prefix.isEmpty ? "\(totalSeconds)s" : "\(prefix): \(totalSeconds)s"
        }
    }
    
    func toDisplaySecondFromNowWithSuffix(_ textToZero: String = "0") -> String {
        let currentDate = Date()
        let components = Calendar.current.dateComponents([.second], from: currentDate, to: self)
        let totalSeconds = components.second ?? 0
        if totalSeconds <= 0 {
            return textToZero
        } else if totalSeconds == 1 {
            return "\(totalSeconds) \("second".localized())"
        }
        return "\(totalSeconds) \("seconds".localized())"
    }
    
    func getDateComponent(_ component: Calendar.Component) -> Int {
        let calendar = Calendar.current
        return calendar.component(component, from: self)
    }
    
    func toDisplayCalendarString() -> String {
        
        let currentDate = Date()
        let calendar = Calendar.current
        
        let cYear = calendar.component(.year, from: currentDate)
        let cMonth = calendar.component(.month, from: currentDate)
        let cDay = calendar.component(.day, from: currentDate)
        
        let sYear = calendar.component(.year, from: self)
        let sMonth = calendar.component(.month, from: self)
        let sDay = calendar.component(.day, from: self)
        
        if (cYear == sYear && cMonth == sMonth && cDay == sDay) {
            return "Today".localized()
        }
        return self.toString(dateFormat: "EEEE, dd-MM-yyyy")
        
    }
    
    func toDisplayMonthViewString(_ isFuture: Bool = false) -> String {
        
        let calendar = Calendar.current
        let compareDateComponents = calendar.dateComponents([.year, .month], from: self)
        
        // Extract the components of the dates
        let currentDateComponents = calendar.dateComponents([.year, .month], from: Date())
        let prevMonthDateComponents = calendar.dateComponents([.year, .month], from: Date().addedByMonth(month: -1))
        let nextMonthDateComponents = calendar.dateComponents([.year, .month], from: Date().addedByMonth(month: 1))
        
        //        print(compareDateComponents.year)
        //        print(currentDateComponents.year)
        //        print(prevMonthDateComponents.year)
        //        print(nextMonthDateComponents.year)
        //
        //        print(compareDateComponents.month)
        //        print(currentDateComponents.month)
        //        print(prevMonthDateComponents.month)
        //        print(nextMonthDateComponents.month)
        
        if compareDateComponents.year == currentDateComponents.year && compareDateComponents.month == currentDateComponents.month {
            return "THIS MONTH".localized()
        } else if compareDateComponents.year == prevMonthDateComponents.year && compareDateComponents.month == prevMonthDateComponents.month {
            return "LAST MONTH".localized()
        } else if isFuture && compareDateComponents.year == nextMonthDateComponents.year && compareDateComponents.month == nextMonthDateComponents.month {
            return "NEXT MONTH".localized()
        }
        
        return self.toString(dateFormat: "MM/yyyy")
        
    }
    
    func isNextMonthFromNow() -> Bool {
        
        let calendar = Calendar.current
        let compareDateComponents = calendar.dateComponents([.year, .month], from: self)
        let nextMonthDateComponents = calendar.dateComponents([.year, .month], from: Date().addedByMonth(month: 1))
        
        if compareDateComponents.year == nextMonthDateComponents.year && compareDateComponents.month == nextMonthDateComponents.month {
            return true
        }
        return false
        
    }
}

// MARK: - IMAGEVIEW
private var KeepTap:String = "KeepTap"
private var KeepTapEvent:String = "KeepTapEvent"
private var BLURMASK:String = "BLURMASK"
let imageCache = NSCache<NSString, UIImage>()
extension UIImageView {
    
    func loadImageUsingCacheWithURLString(_ URLString: String, size:CGSize? = nil, placeHolder: UIImage? = #imageLiteral(resourceName: "no_image_available"), _ loadCached:Bool = true, color: UIColor? = nil, _ onComplete:((UIImage?)->Void)? = nil){
        
        self.startAnimating()
        
        self.image = placeHolder
        
        if let cachedImage = imageCache.object(forKey: NSString(string: URLString)) {
            if loadCached {
                if (color == nil) {
                    self.image = cachedImage
                } else {
                    self.image = cachedImage.tint(with: color!)
                }
                self.stopAnimating()
                onComplete?(self.image)
                return
            } else {
                imageCache.removeObject(forKey: NSString(string: URLString))
            }
        }
        
        if let url = URL(string: URLString) {
            
            var request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Common.shared.TIMEOUT_INTERVAL)
            if (!loadCached) {
                request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: Common.shared.TIMEOUT_INTERVAL)
            }
            
            URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                
                if error != nil {
                    if let er = error {
                        print("ERROR LOADING IMAGES FROM URL: \(er)")
                    }
                    DispatchQueue.main.async {
                        self.stopAnimating()
                    }
                    return
                }
                if let data = data {
                    if let downloadedImage = UIImage(data: data) {
                        
                        if let s = size {
                            let imgScale = downloadedImage.resizeImageWith(newSize: s)
                            if let imageData = imgScale.jpegData(compressionQuality: 0.7) {
                                if let img = UIImage(data: imageData) {
                                    imageCache.setObject(img, forKey: NSString(string: URLString))
                                    DispatchQueue.main.async {
                                        //self.image = img
                                        if (color == nil) {
                                            self.image = img
                                        } else {
                                            self.image = img.tint(with: color!)
                                        }
                                        onComplete?(self.image)
                                    }
                                }
                            }
                        } else {
                            imageCache.setObject(downloadedImage, forKey: NSString(string: URLString))
                            DispatchQueue.main.async {
                                //self.image = downloadedImage
                                if (color == nil) {
                                    self.image = downloadedImage
                                } else {
                                    self.image = downloadedImage.tint(with: color!)
                                }
                                onComplete?(self.image)
                            }
                        }
                    }
                }
                DispatchQueue.main.async {
                    self.stopAnimating()
                }
            }).resume()
        } else {
            self.stopAnimating()
            onComplete?(self.image)
        }
    }
    
    // use for mark favourite categories
    func addMask(color:UIColor,_ alpha:CGFloat = 1,_ removeIconCheck:Bool = false) {
        let mask = UIView(frame: self.bounds)
        if !removeIconCheck {
            let imv = UIImageView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
            imv.image = UIImage(named: "ic_check_circle_128")?.tint(with: UIColor.white)
            mask.addSubview(imv)
            imv.translatesAutoresizingMaskIntoConstraints = false
            imv.topAnchor.constraint(equalTo: imv.superview!.topAnchor, constant: 5).isActive = true
            imv.rightAnchor.constraint(equalTo: imv.superview!.rightAnchor, constant: -5).isActive = true
            imv.widthAnchor.constraint(equalToConstant: 30).isActive = true
            imv.heightAnchor.constraint(equalToConstant: 30).isActive = true
        }
        self.addSubview(mask)
        mask.tag = 9989
        mask.backgroundColor = color.withAlphaComponent(alpha)
        mask.translatesAutoresizingMaskIntoConstraints = false
        mask.topAnchor.constraint(equalTo: mask.superview!.topAnchor, constant: 0).isActive = true
        mask.trailingAnchor.constraint(equalTo: mask.superview!.trailingAnchor, constant: 0).isActive = true
        mask.bottomAnchor.constraint(equalTo: mask.superview!.bottomAnchor, constant: 0).isActive = true
        mask.leadingAnchor.constraint(equalTo: mask.superview!.leadingAnchor, constant: 0).isActive = true
    }
    
    func removeMask() {
        _ = self.subviews.reversed().map{if $0.tag == 9989 {$0.removeFromSuperview()}}
    }
    
    func blur(position:[String]? = ["bottom"]) {
        removeBlur()
        let blur = CAGradientLayer()
        var rect = self.bounds
        rect.size.height = rect.size.height * 30/100
        rect.origin.y  = self.bounds.size.height - rect.size.height
        blur.frame = rect
        let transWhiteColor = UIColor.black.withAlphaComponent(0).cgColor
        let endColor = UIColor.black.withAlphaComponent(0.5).cgColor
        blur.colors = [transWhiteColor,endColor]
        layer.insertSublayer(blur, at: 0)
        objc_setAssociatedObject(self, &BLURMASK, blur, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func removeBlur() {
        if let layer = objc_getAssociatedObject(self, &BLURMASK) as? CAGradientLayer {
            layer.removeFromSuperlayer()
        }
    }
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        if let layer = objc_getAssociatedObject(self, &BLURMASK) as? CAGradientLayer {
            var rect = self.bounds
            rect.size.height = rect.size.height * 30/100
            rect.origin.y  = self.bounds.size.height - rect.size.height
            layer.frame = rect
        }
    }
}

class UIImageViewRound: UIImageView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        layer.cornerRadius = frame.size.width/2
        layer.masksToBounds = true
    }
}

// MARK: - IMAGE
public extension UIImage {
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
    func resizeImageWith(newSize: CGSize) -> UIImage {
        let horizontalRatio = newSize.width / size.width
        let verticalRatio = newSize.height / size.height
        let ratio = max(horizontalRatio, verticalRatio)
        let newSize = CGSize(width: size.width * ratio, height: size.height * ratio)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0)
        draw(in: CGRect(origin: CGPoint(x: 0, y: 0), size: newSize))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    func maskRoundedImage(radius: CGFloat? = nil) -> UIImage {
        let width = min(self.size.width,self.size.height)
        let imageView: UIImageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: width)))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        let layer = imageView.layer
        layer.masksToBounds = true
        layer.cornerRadius = radius == nil ? self.size.width/2 : radius!
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size,false, UIScreen.main.scale)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage!
    }
    
    func maskCornerRadiusImage(radius: CGFloat) -> UIImage {
        let width = min(self.size.width,self.size.height)
        let imageView: UIImageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize(width: width, height: width)))
        imageView.contentMode = .scaleAspectFill
        imageView.image = self
        let layer = imageView.layer
        layer.masksToBounds = true
        layer.cornerRadius = radius
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size,false, UIScreen.main.scale)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let roundedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return roundedImage!
    }
    
    func tint(with color: UIColor) -> UIImage {
        var image = withRenderingMode(.alwaysTemplate)
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        color.set()
        image.draw(in: CGRect(origin: .zero, size: size))
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
    static func textToImage(drawText text: String, inImage image: UIImage, atPoint point: CGPoint? = nil) -> UIImage {
        let textColor = UIColor.red
        let textFont = UIFont.boldSystemFont(ofSize: 12)
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes:[NSAttributedString.Key:Any] = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        var p = CGPoint.zero
        if let pp = point {
            p = pp
        }
        let rect = CGRect(origin: p, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func isEqualToImage(image: UIImage) -> Bool {
        guard let data1 = self.pngData(),
            let data2 = image.pngData() else {return false}
        let dt1 = data1 as NSData
        let dt2 = data2 as NSData
        return dt1.isEqual(dt2)
    }
    
}

// MARK: - BUTTON
var ButtonBag = "ButtonBag"
var ButtonColorObjc = "ButtonColorobjc"
var ButtonBorderWidthObjc = "ButtonBorderWithobjc"
var ButtonBackgroundColorObjc = "ButtonBackgroundColorobjc"
extension UIButton {
    
    func startAnimation(activityIndicatorStyle:UIActivityIndicatorView.Style,_ isHideContent:Bool = false) {
        
        self.setTitleColor(UIColor.clear, for: .disabled)
        
        stopAnimation()
        
        self.isEnabled = false
        
        let indicator = UIActivityIndicatorView(style: activityIndicatorStyle)
        self.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        indicator.startAnimating()
        
        if isHideContent {
            objc_setAssociatedObject(self, &ButtonColorObjc, self.titleColor(for: self.state), objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            objc_setAssociatedObject(self, &ButtonBorderWidthObjc, self.layer.borderWidth, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            objc_setAssociatedObject(self, &ButtonBackgroundColorObjc, self.backgroundColor, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            self.setTitleColor(UIColor.clear, for: UIControl.State())
        }
    }
    
    func stopAnimation() {
        _ = self.subviews.map({
            if $0.isKind(of:UIActivityIndicatorView.self) {
                $0.removeFromSuperview()
            }
        })
        if let x = objc_getAssociatedObject(self, &ButtonColorObjc) as? UIColor {
            self.setTitleColor(x, for: self.state)
        }
        if let x = objc_getAssociatedObject(self, &ButtonBorderWidthObjc) as? CGFloat {
            self.layer.borderWidth = x
        }
        if let x = objc_getAssociatedObject(self, &ButtonBackgroundColorObjc) as? UIColor {
            self.backgroundColor = x
        }
        objc_setAssociatedObject(self, &ButtonColorObjc, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        objc_setAssociatedObject(self, &ButtonBorderWidthObjc, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        objc_setAssociatedObject(self, &ButtonBackgroundColorObjc, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        self.isEnabled = true
    }
    
    func setBackgroundColor(color: UIColor, forState: UIControl.State) {
        self.clipsToBounds = true  // add this to maintain corner radius
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 1))
        if let context = UIGraphicsGetCurrentContext() {
            context.setFillColor(color.cgColor)
            context.fill(CGRect(x: 0, y: 0, width: 1, height: 1))
            let colorImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            self.setBackgroundImage(colorImage, for: forState)
        }
    }
}

// MARK: - LABEL
class UILabelPadding: UILabel {
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    @IBInspectable var topInset: CGFloat = 5.0
    @IBInspectable var bottomInset: CGFloat = 5.0
    @IBInspectable var leftInset: CGFloat = 5.0
    @IBInspectable var rightInset: CGFloat = 5.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}

class UILabelRound: UILabel {
    override func layoutSubviews() {
        super.layoutSubviews()
        clipsToBounds = true
        layer.cornerRadius = frame.size.height/2
    }
    
    @IBInspectable var topInset: CGFloat = 0
    @IBInspectable var bottomInset: CGFloat = 0
    @IBInspectable var leftInset: CGFloat = 3.0
    @IBInspectable var rightInset: CGFloat = 3.0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets.init(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        let size = super.intrinsicContentSize
        return CGSize(width: size.width + leftInset + rightInset,
                      height: size.height + topInset + bottomInset)
    }
}

private var RECTLINKS:String = "RECTLINKS"
private var TAPLINKS:String = "TAPLINKS"
private var EVENTLINKS:String = "EVENTLINKS"
extension UILabel {
    
    var tap:UITapGestureRecognizer {
        if let  singleTap = objc_getAssociatedObject(self,&TAPLINKS) as? UITapGestureRecognizer  { return singleTap}
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.tapLinks(_:)))
        //        singleTap.numberOfTapsRequired = 1 // you can change this value
        return singleTap
    }
    
    @objc func tapLinks(_ tap:UITapGestureRecognizer) {
        guard let action = objc_getAssociatedObject(self, &EVENTLINKS) as? (String?)->Void,
            let tap = objc_getAssociatedObject(self, &TAPLINKS) as? UITapGestureRecognizer,
            let rectlinks =  objc_getAssociatedObject(self,&RECTLINKS) as? [String:Any] else {return}
        let touchPoint = tap.location(in: self)
        for key in rectlinks.keys {
            if NSCoder.cgRect(for: key).contains(touchPoint) {
                if let url = rectlinks[key] as? URL {
                    action(url.absoluteString)
                } else if let str = rectlinks[key] as? NSString {
                    action(str as String)
                }
                break
            }
        }
    }
    
    func removeHandleLinks() {
        removeEvent()
        objc_setAssociatedObject(self, &RECTLINKS, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        objc_setAssociatedObject(self, &TAPLINKS, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        objc_setAssociatedObject(self, &EVENTLINKS, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func boundingRect(range:NSRange)->CGRect {
        guard let attribut = attributedText else {return CGRect.zero}
        let textStorage = NSTextStorage(attributedString: attribut)
        let layoutManager = NSLayoutManager()
        textStorage.addLayoutManager(layoutManager)
        let textContainer = NSTextContainer(size: self.bounds.size)
        textContainer.lineFragmentPadding = 0
        layoutManager.addTextContainer(textContainer)
        
        var glyphRange =  NSRange()
        layoutManager.characterRange(forGlyphRange: range, actualGlyphRange: &glyphRange)
        return layoutManager.boundingRect(forGlyphRange: glyphRange, in: textContainer)
    }
    
    
}


// MARK: - UIVIEW
private var CLOSEBUTTON:String = "CloseBUTTON"
private var CHECKBAG:String = "CHECKBAG"
extension UIView {
    
    var singleTap:UITapGestureRecognizer {
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(self.tapDetected(_:)))
        //        singleTap.numberOfTapsRequired = 1 // you can change this value
        return singleTap
    }
    
    func addEvent(_ event:@escaping (()->Void)) {
        if objc_getAssociatedObject(self, &KeepTap) != nil {return}
        
        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(singleTap)
        objc_setAssociatedObject(self, &KeepTap, singleTap, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        objc_setAssociatedObject(self, &KeepTapEvent, event, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func removeEvent() {
        if let tap = objc_getAssociatedObject(self, &KeepTap) as? UITapGestureRecognizer {
            self.removeGestureRecognizer(tap)
            objc_setAssociatedObject(self, &KeepTap, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
        if let _ = objc_getAssociatedObject(self, &KeepTapEvent) as? (()->Void) {
            objc_setAssociatedObject(self, &KeepTapEvent, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    //Action
    @objc func tapDetected(_ sender:UITapGestureRecognizer) {
        if let event = objc_getAssociatedObject(self, &KeepTapEvent) as? (()->Void) {
            event()
        }
    }
    
    func startLoading(activityIndicatorStyle:UIActivityIndicatorView.Style,_ hiddenSubview:Bool = false) {
        if hiddenSubview {
            _ = self.subviews.map({
                $0.isHidden = true
            })
        }
        stopLoading()
        self.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        let indicator = UIActivityIndicatorView(style: activityIndicatorStyle)
        indicator.color = Common.shared.APP_MAIN_COLOR
        self.addSubview(indicator)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        indicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        indicator.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        indicator.startAnimating()
    }
    
    func stopLoading() {
        _ = self.subviews.map({
            if $0.isKind(of:UIActivityIndicatorView.self) {
                $0.removeFromSuperview()
            }
        })
    }
    
    func showNoData(_ message:String = "No data.".localized()) {
        removeNoData()
        let mask = UIView(frame: self.bounds)
        mask.backgroundColor = UIColor.clear
        let imv = UILabel(frame: mask.frame)
        imv.text = message
        imv.textAlignment = .center
        imv.numberOfLines = 0
        mask.tag = 9989
        
        self.addSubview(mask)
        mask.translatesAutoresizingMaskIntoConstraints = false
        mask.centerXAnchor.constraint(equalTo: mask.superview!.centerXAnchor, constant: 0).isActive = true
        mask.centerYAnchor.constraint(equalTo: mask.superview!.centerYAnchor, constant: 0).isActive = true
        mask.widthAnchor.constraint(equalTo: mask.superview!.widthAnchor, constant: 0).isActive = true
        mask.heightAnchor.constraint(equalTo: mask.superview!.heightAnchor, constant: 0).isActive = true
        
        mask.addSubview(imv)
        imv.translatesAutoresizingMaskIntoConstraints = false
        imv.topAnchor.constraint(equalTo: imv.superview!.topAnchor, constant: 0).isActive = true
        imv.rightAnchor.constraint(equalTo: imv.superview!.rightAnchor, constant: 0).isActive = true
        imv.bottomAnchor.constraint(equalTo: imv.superview!.bottomAnchor, constant: 0).isActive = true
        imv.leftAnchor.constraint(equalTo: imv.superview!.leftAnchor, constant: 0).isActive = true
    }
    
    func removeNoData() {
        _ = self.subviews.reversed().map{if $0.tag == 9989 {$0.removeFromSuperview()}}
    }
    
    func addCloseButton(_ size:CGSize? = nil,_ event:@escaping (()->Void)) {
        
        if let image = objc_getAssociatedObject(self, &CLOSEBUTTON) as? UIImageView {
            image.removeEvent()
            image.removeFromSuperview()
        }
        
        let imgClose = UIImageView(frame: self.bounds)
        self.addSubview(imgClose)
        objc_setAssociatedObject(self, &CLOSEBUTTON, imgClose, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        imgClose.translatesAutoresizingMaskIntoConstraints = false
        var width = CGFloat(30)
        var height = CGFloat(30)
        if let si = size {
            width = si.width
            height = si.height
        }
        imgClose.widthAnchor.constraint(equalToConstant: width).isActive = true
        imgClose.heightAnchor.constraint(equalToConstant: height).isActive = true
        imgClose.topAnchor.constraint(equalTo: imgClose.superview!.topAnchor, constant: 2).isActive = true
        imgClose.leadingAnchor.constraint(equalTo: imgClose.superview!.leadingAnchor, constant: 2).isActive = true
        imgClose.image = #imageLiteral(resourceName: "ic_close_black_76").tint(with: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0))
        imgClose.addEvent(event)
    }
    
    func getWindowTop(to containerView: UIView? = nil) -> CGPoint {
        let targetRect = self.convert(self.bounds , to: containerView)
        return targetRect.origin
    }
    
    /// set Bag for view
    /// - Parameters:
    ///   - bag: number to display: 0 is hidden bag
    ///   - position: 0 => center | 1 => left | 2 => right
    func setBag(_ bag:Int, _ position:Int = 0, _ size:CGSize = CGSize(width: 12, height: 12)) {
        setBagString("\(bag)", position, UIColor.red, UIColor.white, size)
    }
    
    /// set Bag for view
    /// - Parameters:
    ///   - bag: number to display: 0 is hidden bag
    ///   - position: 0 => center | 1 => left | 2 => right
    func setBagString(_ bag: String, _ position: Int = 0, _ backgroundColor: UIColor = #colorLiteral(red: 0.1450980392, green: 0.3058823529, blue: 0.4274509804, alpha: 1), _ textColor: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), _ size: CGSize = CGSize(width: 12, height: 12)) {
        if let label = objc_getAssociatedObject(self, &ButtonBag) as? UILabelRound{
            if bag == "" || bag == "0" {
                label.removeFromSuperview()
                objc_setAssociatedObject(self, &ButtonBag, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return
            } else {
                label.text = "\(bag)"
            }
        } else {
            if bag == "" || bag == "0" {return}
            let label = UILabelRound(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            label.textColor = textColor
            label.textAlignment = .center
            label.backgroundColor = backgroundColor
            label.font = UIFont.systemFont(ofSize: 9)
            label.text = bag
            self.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            
            if position == 0 {
                label.centerXAnchor.constraint(equalTo: label.superview!.centerXAnchor, constant: (size.width/2)*30/100).isActive = true
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: 0).isActive = true
            } else if position == 1 {
                label.leadingAnchor.constraint(equalTo: label.superview!.leadingAnchor, constant: 0).isActive = true
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: 0).isActive = true
            } else if position == 2 {
                label.trailingAnchor.constraint(equalTo: label.superview!.trailingAnchor, constant:0).isActive = true
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: 0).isActive = true
            }
            
            let width = label.widthAnchor.constraint(greaterThanOrEqualToConstant: 14)
            width.priority = UILayoutPriority.init(751)
            label.addConstraint(width)
            label.heightAnchor.constraint(equalToConstant: 14).isActive = true
            label.layoutIfNeeded()
            label.setNeedsDisplay()
            objc_setAssociatedObject(self, &ButtonBag, label, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func setChecked(_ isCheck:Bool,_ position:Int = 0,_ size:CGSize = CGSize(width: 15, height: 15)) {
        if let label = objc_getAssociatedObject(self, &CHECKBAG) as? UIImageViewRound{
            if !isCheck {
                label.removeFromSuperview()
                objc_setAssociatedObject(self, &CHECKBAG, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return
            }
        } else {
            if !isCheck {return}
            let label = UIImageViewRound(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            label.contentMode = .scaleAspectFit
            label.backgroundColor = UIColor.clear
            label.image = #imageLiteral(resourceName: "ic_done").tint(with: Common.shared.APP_GREEN_COLOR)
            self.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            
            //            if position == 0 {
            //                label.centerXAnchor.constraint(equalTo: label.superview!.centerXAnchor, constant: (size.width/2)*(size.width*2)/100).isActive = true
            //                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: 0).isActive = true
            //            } else if position == 1 {
            //                label.leadingAnchor.constraint(equalTo: label.superview!.leadingAnchor, constant: -(size.width*2)).isActive = true
            //                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: 0).isActive = true
            //            } else if position == 2 {
            //                label.trailingAnchor.constraint(equalTo: label.superview!.trailingAnchor, constant: size.width*2).isActive = true
            //                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: 0).isActive = true
            //            }
            
            if position == 0 {
                label.centerXAnchor.constraint(equalTo: label.superview!.centerXAnchor, constant: (size.width/2)*30/100).isActive = true
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: -2).isActive = true
            } else if position == 1 {
                label.leadingAnchor.constraint(equalTo: label.superview!.leadingAnchor, constant: -2).isActive = true
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: -2).isActive = true
            } else if position == 2 {
                label.trailingAnchor.constraint(equalTo: label.superview!.trailingAnchor, constant: 2).isActive = true
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: -2).isActive = true
            }
            
            let width = label.widthAnchor.constraint(greaterThanOrEqualToConstant: size.width)
            width.priority = UILayoutPriority.init(751)
            label.addConstraint(width)
            label.heightAnchor.constraint(equalToConstant: size.height).isActive = true
            label.layoutIfNeeded()
            label.setNeedsDisplay()
            objc_setAssociatedObject(self, &CHECKBAG, label, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    ///   - position: 0 => center | 1 => left | 2 => right
    func setIndicator(_ isCheck: Bool, _ position: Int = 2, _ size: CGSize = CGSize(width: 10, height: 10)) {
        if let label = objc_getAssociatedObject(self, &CHECKBAG) as? UIImageViewRound{
            if !isCheck {
                label.removeFromSuperview()
                objc_setAssociatedObject(self, &CHECKBAG, nil, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
                return
            }
        } else {
            if !isCheck {return}
            let label = UIImageViewRound(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
            label.contentMode = .scaleAspectFit
            label.backgroundColor = UIColor.clear
            label.image = #imageLiteral(resourceName: "ic_indicator").tint(with: Common.shared.APP_BLUE_COLOR)
            self.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            
            //            if position == 0 {
            //                label.centerXAnchor.constraint(equalTo: label.superview!.centerXAnchor, constant: (size.width/2)*(size.width*2)/100).isActive = true
            //                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: 0).isActive = true
            //            } else if position == 1 {
            //                label.leadingAnchor.constraint(equalTo: label.superview!.leadingAnchor, constant: -(size.width*2)).isActive = true
            //                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: 0).isActive = true
            //            } else if position == 2 {
            //                label.trailingAnchor.constraint(equalTo: label.superview!.trailingAnchor, constant: size.width*2).isActive = true
            //                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: 0).isActive = true
            //            }
            
            if position == 0 {
                label.centerXAnchor.constraint(equalTo: label.superview!.centerXAnchor, constant: (size.width/2)*30/100).isActive = true
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: -2).isActive = true
            } else if position == 1 {
                label.trailingAnchor.constraint(equalTo: label.superview!.trailingAnchor, constant: 2).isActive = true
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: -2).isActive = true
            } else if position == 2 {
                label.leadingAnchor.constraint(equalTo: label.superview!.leadingAnchor, constant: -2).isActive = true
                label.topAnchor.constraint(equalTo: label.superview!.topAnchor, constant: -2).isActive = true
            }
            
            let width = label.widthAnchor.constraint(greaterThanOrEqualToConstant: size.width)
            width.priority = UILayoutPriority.init(751)
            label.addConstraint(width)
            label.heightAnchor.constraint(equalToConstant: size.height).isActive = true
            label.layoutIfNeeded()
            label.setNeedsDisplay()
            objc_setAssociatedObject(self, &CHECKBAG, label, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    func dropShadow(offsetX: CGFloat, offsetY: CGFloat, color: UIColor, opacity: Float, radius: CGFloat, scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowOffset = CGSize(width: offsetX, height: offsetY)
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
    
    func animateToView(fraction: Int) {
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width
        let offset = fraction > 0 ? screenWidth : -screenWidth
        
        let toView = self
        toView.frame.origin = CGPoint(
            x: self.frame.origin.x + offset,
            y: self.frame.origin.y
        )
        self.superview?.addSubview(toView)
        
        // Disable interaction during animation
        self.isUserInteractionEnabled = false
        
        UIView.animate(
            withDuration: 0.7,
            delay: 0.0,
            usingSpringWithDamping: 0.9,
            initialSpringVelocity: 0.5,
            options: .curveEaseInOut,
            animations: {
                // Slide the views by -offset
                toView.center = CGPoint(x: self.center.x - offset, y: self.center.y)
                self.center = CGPoint(x: toView.center.x - offset, y: toView.center.y)
        },
            completion: { _ in
                //toView.removeFromSuperview()
                self.isUserInteractionEnabled = true
        }
        )
    }
    
    func animateSlideView(fraction: Int, duration: TimeInterval = 0.4) {
        let width = UIScreen.main.bounds.size.width
        let x =  width * CGFloat(fraction)
        self.frame.origin.x = x
        UIView.animate(
            withDuration: duration,
            delay: 0.0,
            options: .curveEaseInOut,
            animations: {
                self.frame.origin.x = 0
        }) { (completed) in }
    }
}

// MARK: - TEXTVIEW
private var TEXTVIEWPLACEHOLDER:String = "TextViewPlaceholder"
extension UITextView: UITextViewDelegate {
    
    /// Resize the placeholder when the UITextView bounds change
    override open var bounds: CGRect {
        didSet {
            self.resizePlaceholder()
        }
    }
    
    /// The UITextView placeholder text
    public var placeholder: String? {
        get {
            var placeholderText: String?
            
            if let placeholderLabel = self.viewWithTag(100) as? UILabel {
                placeholderText = placeholderLabel.text
            }
            
            return placeholderText
        }
        set {
            if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
                placeholderLabel.text = newValue
                placeholderLabel.sizeToFit()
            } else {
                self.addPlaceholder(newValue!)
            }
        }
    }
    
    /// When the UITextView did change, show or hide the label based on if the UITextView is empty or not
    ///
    /// - Parameter textView: The UITextView that got updated
    public func textViewDidChange(_ textView: UITextView) {
        if let placeholderLabel = self.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = self.text.count > 0
        }
    }
    
    /// Resize the placeholder UILabel to make sure it's in the same position as the UITextView text
    private func resizePlaceholder() {
        if let placeholderLabel = self.viewWithTag(100) as! UILabel? {
            let labelX = self.textContainer.lineFragmentPadding
            let labelY = self.textContainerInset.top - 2
            let labelWidth = self.frame.width - (labelX * 2)
            let labelHeight = placeholderLabel.frame.height
            
            placeholderLabel.frame = CGRect(x: labelX, y: labelY, width: labelWidth, height: labelHeight)
        }
    }
    
    /// Adds a placeholder UILabel to this UITextView
    private func addPlaceholder(_ placeholderText: String) {
        let placeholderLabel = UILabel()
        
        placeholderLabel.text = placeholderText
        placeholderLabel.sizeToFit()
        
        placeholderLabel.font = self.font
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.tag = 100
        
        placeholderLabel.isHidden = self.text.count > 0
        
        self.addSubview(placeholderLabel)
        self.resizePlaceholder()
        self.delegate = self
    }
}

// MARK: - Timer
final class TimerInvocation: NSObject {
    
    var callback: () -> ()
    
    init(callback: @escaping () -> ()) {
        self.callback = callback
    }
    
    @objc func invoke(timer:Timer) {
        callback()
    }
}

extension Timer {
    
    static func scheduleTimer(timeInterval: TimeInterval, repeats: Bool, invocation: TimerInvocation) {
        
        Timer.scheduledTimer(
            timeInterval: timeInterval,
            target: invocation,
            selector: #selector(TimerInvocation.invoke(timer:)),
            userInfo: nil,
            repeats: repeats)
    }
}

// MARK: - TABLEVIEW
private var PullRefreshEvent:String = "PullRefreshEvent"
private var RefreshControl:String = "PullRefreshEvent"
extension UITableView {
    
    func pullResfresh(_ event:@escaping (()->Void)) {
        
        if objc_getAssociatedObject(self, &RefreshControl) == nil {
            let refreshControl = UIRefreshControl()
            objc_setAssociatedObject(self, &RefreshControl, refreshControl, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
            refreshControl.attributedTitle = nil//NSAttributedString(string: "pull_to_refresh".localized())
            refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
            self.addSubview(refreshControl)
        }
        
        objc_setAssociatedObject(self, &PullRefreshEvent, event, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    func endPullResfresh() {
        if let refreshControl = objc_getAssociatedObject(self, &RefreshControl) as? UIRefreshControl {
            if refreshControl.isRefreshing {
                refreshControl.endRefreshing()
            }
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        // override it
        if let event = objc_getAssociatedObject(self, &PullRefreshEvent) as? (()->Void) {
            event()
        }
    }
    
    func showTableNoData(_ message: String = "No data.".localized()) {
        let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        noDataLabel.text          = message
        noDataLabel.textColor     = UIColor.black
        noDataLabel.textAlignment = .center
        self.backgroundView  = noDataLabel
        self.separatorStyle  = .none
    }
    
    func removeTableNoData() {
        self.backgroundView  = nil
    }
}

// MARK: - COLLECTVIEW
extension UICollectionView {
    func pullResfresh(_ event:@escaping (()->Void)) {
        objc_setAssociatedObject(self, &PullRefreshEvent, event, objc_AssociationPolicy.OBJC_ASSOCIATION_COPY_NONATOMIC)
    }
    
    func endPullResfresh() {
        if let refreshControl = objc_getAssociatedObject(self, &RefreshControl) as? UIRefreshControl {
            if refreshControl.isRefreshing {
                refreshControl.endRefreshing()
            }
        }
    }
    
    func isUsingPullRefresh() -> Bool {
        if let refreshControl = objc_getAssociatedObject(self, &RefreshControl) as? UIRefreshControl {
            return refreshControl.isRefreshing
        }
        return false
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        let refreshControl = UIRefreshControl()
        objc_setAssociatedObject(self, &RefreshControl, refreshControl, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        refreshControl.attributedTitle = nil//NSAttributedString(string: "pull_to_refresh".localized())
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        self.addSubview(refreshControl)
    }
    
    @objc func refresh(sender:AnyObject) {
        // override it
        if let event = objc_getAssociatedObject(self, &PullRefreshEvent) as? (()->Void) {
            event()
        }
    }
}

// MARK: - ARRAY
extension Array {
    /// Returns an array containing this sequence shuffled
    var shuffled: Array {
        var elements = self
        return elements.shuffle()
    }
    /// Shuffles this sequence in place
    @discardableResult
    mutating func shuffle() -> Array {
        let count = self.count
        indices.lazy.dropLast().forEach {
            guard case let index = Int(arc4random_uniform(UInt32(count - $0))) + $0, index != $0 else { return }
            self.swapAt($0, index)
        }
        return self
    }
    var chooseOne: Element { return self[Int(arc4random_uniform(UInt32(count)))] }
    func choose(_ n: Int) -> Array { return Array(shuffled.prefix(n)) }
    
    func reArrange(fromIndex: Int, toIndex: Int) -> Array{
        var arr = self
        let element = arr.remove(at: fromIndex)
        arr.insert(element, at: toIndex)
        
        return arr
    }
}

// MARK: - UINAVIGATIONCONTROLLER
extension UINavigationController {
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.providesPresentationContextTransitionStyle = true
        self.definesPresentationContext = true
        self.modalPresentationStyle=UIModalPresentationStyle.overCurrentContext
    }
    
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// MARK: - NOTIFICATION.NAME
extension Notification.Name {
    static let kAVPlayerViewControllerDismissingNotification = Notification.Name.init("AVPLAYER:dismissing")
}

// MARK: - AVPLAYERVIEWCONTROLLER
extension AVPlayerViewController {
    // override 'viewWillDisappear'
    open override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // now, check that this ViewController is dismissing
        if self.isBeingDismissed == false {
            return
        }
        
        // and then , post a simple notification and observe & handle it, where & when you need to.....
        NotificationCenter.default.post(name: .kAVPlayerViewControllerDismissingNotification, object: nil)
    }
}

// MARK: - UIVIEW KEY ANIMATIOn
extension UIView.KeyframeAnimationOptions {
    
    static var curveEaseIn: UIView.KeyframeAnimationOptions {
        get {
            return UIView.KeyframeAnimationOptions(animationOptions: .curveEaseIn)
        }
    }
    
    static var curveEaseOut: UIView.KeyframeAnimationOptions {
        get {
            return UIView.KeyframeAnimationOptions(animationOptions: .curveEaseOut)
        }
    }
    
    static var curveEaseInOut: UIView.KeyframeAnimationOptions {
        get {
            return UIView.KeyframeAnimationOptions(animationOptions: .curveEaseInOut)
        }
    }
    
    static var curveLinear: UIView.KeyframeAnimationOptions {
        get {
            return UIView.KeyframeAnimationOptions(animationOptions: .curveLinear)
        }
    }
    
    init(animationOptions: UIView.AnimationOptions) {
        self = UIView.KeyframeAnimationOptions(rawValue: animationOptions.rawValue)
    }
    
}

// MARK: - AVPlayerLayer
extension CGAffineTransform {
    static let ninetyDegreeRotation = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2))
}

extension AVPlayerLayer {
    
    var fullScreenAnimationDuration: TimeInterval {
        return 0.15
    }
    
    func minimizeToFrame(_ frame: CGRect) {
        UIView.animate(withDuration: fullScreenAnimationDuration) {
            self.setAffineTransform(.identity)
            self.frame = frame
        }
    }
    
    func goFullscreen() {
        UIView.animate(withDuration: fullScreenAnimationDuration) {
            self.setAffineTransform(.ninetyDegreeRotation)
            self.frame = UIScreen.main.bounds
        }
    }
}

extension CharacterSet {
    static let rfc3986Unreserved = CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-._~")
}

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}

extension UITableViewRowAction {
    
    func setIcon(iconImage: UIImage, backColor: UIColor, cellHeight: CGFloat, iconSizePercentage: CGFloat) {
        let iconHeight = cellHeight * iconSizePercentage
        let margin = (cellHeight - iconHeight) / 2 as CGFloat
        UIGraphicsBeginImageContextWithOptions(CGSize(width: cellHeight, height: cellHeight), false, 0)
        let context = UIGraphicsGetCurrentContext()
        backColor.setFill()
        context!.fill(CGRect(x: 0, y: 0, width: cellHeight, height: cellHeight))
        print("Margins: \(margin)")
        iconImage.draw(in: CGRect(x: margin / 3, y: margin / 3, width: iconHeight, height: iconHeight))
        let actionImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        self.backgroundColor = UIColor.init(patternImage: actionImage!)
    }
}

extension UIStackView {
    
    func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        
        // Deactivate all constraints
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        
        // Remove the views from self
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}

extension UIAlertController {
    
    @objc func textDidChangeInAlertChangeIP() {
        if let value1 = textFields?[0].text, let value2 = textFields?[1].text, let action = actions.last {
            action.isEnabled = isNotEmpty(value1) && isNotEmpty(value2) && value2 == "841989"
        }
    }
    
    func isNotEmpty(_ value: String) -> Bool {
        return value.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count > 0
    }
    
    @objc func textDidChangeInRemarkAlert() {
        if let value = textFields?[0].text, let action = actions.last {
            action.isEnabled = isNotEmpty(value)
        }
    }
}

// MARK: - UICOLOR
public enum UIColorInputError : Error {
    case missingHashMarkAsPrefix,
    unableToScanHexValue,
    mismatchedHexStringLength
}

extension UIColor {
    /**
     The shorthand three-digit hexadecimal representation of color.
     #RGB defines to the color #RRGGBB.
     
     - parameter hex3: Three-digit hexadecimal value.
     - parameter alpha: 0.0 - 1.0. The default is 1.0.
     */
    public convenience init(hex3: UInt16, alpha: CGFloat = 1) {
        let divisor = CGFloat(15)
        let red     = CGFloat((hex3 & 0xF00) >> 8) / divisor
        let green   = CGFloat((hex3 & 0x0F0) >> 4) / divisor
        let blue    = CGFloat( hex3 & 0x00F      ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The shorthand four-digit hexadecimal representation of color with alpha.
     #RGBA defines to the color #RRGGBBAA.
     
     - parameter hex4: Four-digit hexadecimal value.
     */
    public convenience init(hex4: UInt16) {
        let divisor = CGFloat(15)
        let red     = CGFloat((hex4 & 0xF000) >> 12) / divisor
        let green   = CGFloat((hex4 & 0x0F00) >>  8) / divisor
        let blue    = CGFloat((hex4 & 0x00F0) >>  4) / divisor
        let alpha   = CGFloat( hex4 & 0x000F       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The six-digit hexadecimal representation of color of the form #RRGGBB.
     
     - parameter hex6: Six-digit hexadecimal value.
     */
    public convenience init(hex6: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
        let green   = CGFloat((hex6 & 0x00FF00) >>  8) / divisor
        let blue    = CGFloat( hex6 & 0x0000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The six-digit hexadecimal representation of color with alpha of the form #RRGGBBAA.
     
     - parameter hex8: Eight-digit hexadecimal value.
     */
    public convenience init(hex8: UInt32) {
        let divisor = CGFloat(255)
        let red     = CGFloat((hex8 & 0xFF000000) >> 24) / divisor
        let green   = CGFloat((hex8 & 0x00FF0000) >> 16) / divisor
        let blue    = CGFloat((hex8 & 0x0000FF00) >>  8) / divisor
        let alpha   = CGFloat( hex8 & 0x000000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    /**
     The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, throws error.
     
     - parameter rgba: String value.
     */
    public convenience init(rgba_throws rgba: String) throws {
        guard rgba.hasPrefix("#") else {
            throw UIColorInputError.missingHashMarkAsPrefix
        }
        
        let hexString: String = String(rgba[rgba.index(rgba.startIndex, offsetBy: 1)...])//rgba.substring(from: rgba.index(rgba.startIndex, offsetBy: 1))
        var hexValue:  UInt32 = 0
        
        guard Scanner(string: hexString).scanHexInt32(&hexValue) else {
            throw UIColorInputError.unableToScanHexValue
        }
        
        switch (hexString.count) {
        case 3:
            self.init(hex3: UInt16(hexValue))
        case 4:
            self.init(hex4: UInt16(hexValue))
        case 6:
            self.init(hex6: hexValue)
        case 8:
            self.init(hex8: hexValue)
        default:
            throw UIColorInputError.mismatchedHexStringLength
        }
    }
    
    /**
     The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, fails to default color.
     
     - parameter rgba: String value.
     */
    public convenience init(_ rgba: String, defaultColor: UIColor = UIColor.clear) {
        guard let color = try? UIColor(rgba_throws: rgba) else {
            self.init(cgColor: defaultColor.cgColor)
            return
        }
        self.init(cgColor: color.cgColor)
    }
    
    /**
     Hex string of a UIColor instance.
     
     - parameter includeAlpha: Whether the alpha should be included.
     */
    public func hexString(_ includeAlpha: Bool = true) -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        self.getRed(&r, green: &g, blue: &b, alpha: &a)
        
        if (includeAlpha) {
            return String(format: "#%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
        } else {
            return String(format: "#%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
        }
    }
}

extension UILabel {
    func textColorChange(listChanges: [String: UIColor]) {
        let strNumber: NSString = self.text! as NSString
        let attributes = NSMutableAttributedString.init(string: self.text!)
        listChanges.forEach({
            item in
            let range = (strNumber).range(of: item.key)
            attributes.addAttribute(NSAttributedString.Key.foregroundColor, value: item.value, range: range)
        })
        self.attributedText = attributes
    }
    
    func rightAlignment(substring: String) {
        
        let style = NSMutableParagraphStyle()
        style.alignment = .right
        let attributes = [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.paragraphStyle: style
        ]
        let attributedString = NSMutableAttributedString(string: substring, attributes: attributes)
        self.attributedText = attributedString
        
    }
    
    // -> NSAttributedString
    func addBoldText(fullString: NSString, boldPartsOfString: Array<NSString>, font: UIFont!, boldFont: UIFont!, boldColor: UIColor = UIColor.darkGray) {
        let nonBoldFontAttribute = [NSAttributedString.Key.font: font!]
        let boldFontAttribute = [NSAttributedString.Key.font: boldFont!]
        let boldColorAttribute = [NSAttributedString.Key.foregroundColor: boldColor]
        let boldString = NSMutableAttributedString(string: fullString as String, attributes:nonBoldFontAttribute)
        for i in 0 ..< boldPartsOfString.count {
            boldString.addAttributes(boldFontAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
            boldString.addAttributes(boldColorAttribute, range: fullString.range(of: boldPartsOfString[i] as String))
        }
        self.attributedText = boldString
    }
    
    
    func addStrike(text: String) {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: text)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
    }
    
    func removetrike() {
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: self.text ?? "")
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 0, range: NSMakeRange(0, attributeString.length))
        self.attributedText = attributeString
    }
}


class CustomUnderlinedLabel: UILabel {
    override var text: String? {
        didSet {
            guard let text = text else { return }
            let textRange = NSMakeRange(0, text.count)
            let attributedText = NSMutableAttributedString(string: text)
            attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
            // Add other attributes if needed
            self.attributedText = attributedText
        }
    }
}

class CustomNormalLabel: UILabel {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.frame = frame
    }
    
    init(frame: CGRect,
         content: String,
         numberOfLines: Int = 0,
         lineBreakMode: NSLineBreakMode = NSLineBreakMode.byWordWrapping,
         textAlignment: NSTextAlignment = NSTextAlignment.left,
         font: UIFont = UIFont.systemFont(ofSize: 13, weight: UIFont.Weight.regular),
         layoutPriority: UILayoutPriority = UILayoutPriority.defaultLow,
         contentCompressionPriority: UILayoutPriority = UILayoutPriority.defaultLow,
         axis: NSLayoutConstraint.Axis = NSLayoutConstraint.Axis.horizontal) {
        super.init(frame: frame)
        self.frame = frame
        self.numberOfLines = numberOfLines
        self.lineBreakMode = lineBreakMode
        self.textAlignment = textAlignment
        self.font = font
        self.text = content
        self.setContentHuggingPriority(layoutPriority, for: axis)
        self.setContentCompressionResistancePriority(contentCompressionPriority, for: axis)
    }
}

class Env {
    static var iPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
}

extension UIDevice {
    static let modelName: String = {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        func mapToDevice(identifier: String) -> String {
            #if os(iOS)
            switch identifier {
            case "iPod5,1":                                 return "iPod Touch 5"
            case "iPod7,1":                                 return "iPod Touch 6"
            case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
            case "iPhone4,1":                               return "iPhone 4s"
            case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
            case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
            case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
            case "iPhone7,2":                               return "iPhone 6"
            case "iPhone7,1":                               return "iPhone 6 Plus"
            case "iPhone8,1":                               return "iPhone 6s"
            case "iPhone8,2":                               return "iPhone 6s Plus"
            case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
            case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
            case "iPhone8,4":                               return "iPhone SE"
            case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
            case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
            case "iPhone10,3", "iPhone10,6":                return "iPhone X"
            case "iPhone11,2":                              return "iPhone XS"
            case "iPhone11,4", "iPhone11,6":                return "iPhone XS Max"
            case "iPhone11,8":                              return "iPhone XR"
            case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
            case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
            case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
            case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
            case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
            case "iPad6,11", "iPad6,12":                    return "iPad 5"
            case "iPad7,5", "iPad7,6":                      return "iPad 6"
            case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
            case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
            case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
            case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
            case "iPad6,3", "iPad6,4":                      return "iPad Pro (9.7-inch)"
            case "iPad6,7", "iPad6,8":                      return "iPad Pro (12.9-inch)"
            case "iPad7,1", "iPad7,2":                      return "iPad Pro (12.9-inch) (2nd generation)"
            case "iPad7,3", "iPad7,4":                      return "iPad Pro (10.5-inch)"
            case "iPad8,1", "iPad8,2", "iPad8,3", "iPad8,4":return "iPad Pro (11-inch)"
            case "iPad8,5", "iPad8,6", "iPad8,7", "iPad8,8":return "iPad Pro (12.9-inch) (3rd generation)"
            case "AppleTV5,3":                              return "Apple TV"
            case "AppleTV6,2":                              return "Apple TV 4K"
            case "AudioAccessory1,1":                       return "HomePod"
            case "i386", "x86_64":                          return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "iOS"))"
            default:                                        return identifier
            }
            #elseif os(tvOS)
            switch identifier {
            case "AppleTV5,3": return "Apple TV 4"
            case "AppleTV6,2": return "Apple TV 4K"
            case "i386", "x86_64": return "Simulator \(mapToDevice(identifier: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"] ?? "tvOS"))"
            default: return identifier
            }
            #endif
        }
        
        return mapToDevice(identifier: identifier)
    }()
}

struct Device {
    // iDevice detection code
    static let IS_IPAD             = UIDevice.current.userInterfaceIdiom == .pad
    static let IS_IPHONE           = UIDevice.current.userInterfaceIdiom == .phone
    static let IS_RETINA           = UIScreen.main.scale >= 2.0
    
    static let SCREEN_WIDTH        = Int(UIScreen.main.bounds.size.width)
    static let SCREEN_HEIGHT       = Int(UIScreen.main.bounds.size.height)
    static let SCREEN_MAX_LENGTH   = Int( max(SCREEN_WIDTH, SCREEN_HEIGHT) )
    static let SCREEN_MIN_LENGTH   = Int( min(SCREEN_WIDTH, SCREEN_HEIGHT) )
    
    static let IS_IPHONE_4_OR_LESS = IS_IPHONE && SCREEN_MAX_LENGTH  < 568
    static let IS_IPHONE_5         = IS_IPHONE && SCREEN_MAX_LENGTH == 568
    static let IS_IPHONE_6         = IS_IPHONE && SCREEN_MAX_LENGTH == 667
    static let IS_IPHONE_6P        = IS_IPHONE && SCREEN_MAX_LENGTH == 736
    static let IS_IPHONE_X         = IS_IPHONE && SCREEN_MAX_LENGTH == 812
}
