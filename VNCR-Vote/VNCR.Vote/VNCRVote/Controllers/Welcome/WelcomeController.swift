//
//  WelcomeController.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 10/9/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import Foundation
import UIKit

class WelcomeController: BaseViewController, QRCodeScannerDelegate {
    
    // MARK: -  function
    func receiveCode(_ code: String) {
        DispatchQueue.main.async {
            //self.showMBProgressHUD()
            //AppHelper.shared.showAlert(code)
            self.txtPassword.text = code
            self.goToVote()
        }
    }
    
    @IBAction func tappedGo(_ sender: Any) {
        self.goToVote()
    }
    
    @IBAction func tappedScanQR(_ sender: Any) {
        self.openScanner()
    }
    
    @IBAction func tappedGuide(_ sender: Any) {
        self.showMBProgressHUD()
        VoteManager.shared.getDocs() { (responseModel, list) in
            self.hideMBProgressHUD()
            if responseModel.RetStatus {
                DispatchQueue.main.async {
                    if ((list?.count ?? 0) == 0) {
                        AppHelper.shared.showAlert("URLEmpty".localized()) { }
                        return
                    }
                    if let controller = AppHelper.shared.getMainViewController() as? UINavigationController? {
                        if let _ = controller?.visibleViewController as? WebViewContentViewController {
                            return
                        }
                        let vc = AttachmentManagerController()
                        vc.title = "Guide".localized()
                        vc.attachments = list ?? []
                        controller?.pushViewController(vc, animated: true)
                    }
                }
            }
//            else {
//                DispatchQueue.main.async {
//                    AppHelper.shared.showAlert(responseModel.RetMessage)
//                }
//            }
        }
    }
    
    private func getPassword() -> String {
        let pwd = self.txtPassword.text ?? ""
        return pwd
    }
    
    private func loadData(_ code: String) {
        self.showMBProgressHUD()
        VoteManager.shared.getMultiple(code: code) { (responseModel, returnData) in
            self.hideMBProgressHUD()
            if responseModel.RetStatus {
                DispatchQueue.main.async {
                    let list = returnData ?? []
                    var countVoted = 0
                    for item in list {
                        if item.IsVoted {
                            countVoted += 1
                        }
                    }
                    
                    let mainVC = ExamMainViewController(nibName: "ExamMainViewController", bundle: Bundle.main)
                    mainVC.list = list
                    mainVC.code = code
                    mainVC.isReview = list.count == countVoted
                    
                    let appSupportNV = UINavigationController(rootViewController: mainVC)
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let window = appDelegate.window
                    if let ww = window {
                        ww.rootViewController = appSupportNV
                        ww.makeKeyAndVisible()
                        UIView.transition(with: ww, duration: 0.5, options: .transitionFlipFromRight, animations: { }, completion: nil)
                    }
                }
            }
//            else {
//                DispatchQueue.main.async {
//                    AppHelper.shared.showAlert(responseModel.RetMessage)
//                }
//            }
        }
    }
    
    private func goToVote() {
        if getPassword().isEmpty { return }
        self.loadData(getPassword())
    }
    
    private func config() {
        lblWelcome.text = "Please input or scan QR code".localized()
        lblWelcome.textColor = .white
        lblWelcome.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        
        txtPassword.returnKeyType = .go
        txtPassword.delegate = self
        
        let tapper = UITapGestureRecognizer(target: self.view, action:#selector(self.view.endEditing(_:)))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
        
        btnScanQR.setTitle("Scan QR code".localized(), for: .normal)
        btnScanQR.setBackgroundColor(color: Common.shared.APP_GREEN_COLOR, forState: .normal)
        
        btnGo.setTitle("LET'S GO".localized(), for: .normal)
        btnGo.setBackgroundColor(color: Common.shared.APP_BLUE_COLOR, forState: .normal)
        
        btnGuide.setTitle("Guide".localized(), for: .normal)
        btnGuide.setBackgroundColor(color: Common.shared.APP_GREEN_COLOR, forState: .normal)
    }
    
    // MARK: -  init
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.backgroundColor = Common.shared.APP_MAIN_COLOR
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        Broadcaster.register(QRCodeScannerDelegate.self, observer: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AppUtility.lockOrientation(.portrait)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        Broadcaster.unregister(QRCodeScannerDelegate.self, observer: self)
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: -  properties
    
    // MARK: -  outlet
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnGo: ButtonTemplate!
    @IBOutlet weak var btnScanQR: ButtonTemplate!
    @IBOutlet weak var btnGuide: ButtonTemplate!
}


extension WelcomeController: UITextFieldDelegate {
    
    internal func textFieldDidEndEditing(_ textField: UITextField) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if textField.text?.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).count == 0 {return false}
        self.tappedGo((Any).self)
        return true
    }
}

