//
//  AttachmentManagerController.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 10/16/19.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit
import SKPhotoBrowser
import Photos

class AttachmentManagerController: BaseViewController, UINavigationControllerDelegate {
    
    // MARK: -  handle action
    @objc func closeAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true) { }
    }
    
    // MARK: -  private
    private func config() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView.register(SettingCell.nib, forCellReuseIdentifier: SettingCell.identifier)
        tableView.tableFooterView = UIView()
        tableView.reloadData()
    }
    
    // MARK: -  init
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.title = "Attachment".localized()
        //self.view.backgroundColor = UIColor.white
        self.view.backgroundColor = Common.shared.APP_MAIN_COLOR
        self.navigationController?.setNavigationBarHidden(false, animated: false)
//        let leftButton = UIBarButtonItem(title: "Close".localized(), style: .plain, target: self, action: #selector(closeAction(_:)))
//        leftButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,
//                                           NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17)], for: .normal)
//        self.navigationController?.topViewController?.navigationItem.leftBarButtonItem = leftButton
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    // MARK: -  properties
    var attachments: [VoteDocModel] = []
    
    @IBOutlet weak var tableView: UITableView!
}

extension AttachmentManagerController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if attachments.count == 0  {
            tableView.showNoData()
        } else {
            tableView.removeNoData()
        }
        return attachments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: SettingCell.identifier, for: indexPath) as? SettingCell  else {
            return UITableViewCell()
        }
        cell.setAttachmentData(data: attachments[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let selectedData = attachments[indexPath.row]
        if let controller = AppHelper.shared.getMainViewController() as? UINavigationController? {
            if let _ = controller?.visibleViewController as? WebViewContentViewController {
                return
            }
            let vc = WebViewContentViewController()
            vc.title = selectedData.FullName
            vc.url = selectedData.Link
            controller?.pushViewController(vc, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return []
    }
}
