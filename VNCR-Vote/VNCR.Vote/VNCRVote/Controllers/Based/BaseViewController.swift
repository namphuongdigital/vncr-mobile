//
//  BaseViewController.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 11/19/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit
import Foundation
import SwiftMessages
import SKPhotoBrowser
import QuickLook
import AVFoundation
import Reachability
import EZAlertController
import BarcodeScanner

open class BaseViewController: UIViewController {
    
    public var progressHUD: MBProgressHUD!
    var docController: UIDocumentInteractionController!
    var isShowQRCodeScanner: Bool = false
    let reachability = try! Reachability()
    var isPopup: Bool = false
    var qrCodeBarButtonItem: UIBarButtonItem {
        let imgIconMenu = UIImage(named: "ic_barcode")!.tint(with: UIColor.white).resizeImageWith(newSize: CGSize(width: 30, height: 30))
        let buttonMenu = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        buttonMenu.setBackgroundImage(imgIconMenu, for: .normal)
        buttonMenu.addTarget(self, action: #selector(qrCodeBarButtonItemPressed(_:)), for: UIControl.Event.touchUpInside)
        return UIBarButtonItem(customView: buttonMenu)
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override open func viewWillAppear(_ animated: Bool) {
                
        startReachabilityNetwork()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.backItem?.backBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15, weight: .bold), NSAttributedString.Key.foregroundColor : UIColor.white], for: .normal)
        
        self.view.backgroundColor = Common.shared.APP_MAIN_COLOR
        AppHelper.shared.setStyleForApp()
        
        // Always adopt a dark interface style.
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.setNeedsStatusBarAppearanceUpdate()
        
        // All but upside down device
        AppUtility.lockOrientation(.allButUpsideDown)
        // Or to rotate and lock
        // AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        
    }
    
    open override func viewDidDisappear(_ animated: Bool) {
        
        self.stopReachabilityNetwork()
        self.hideMBProgressHUD()
        AppHelper.shared.hideMBProgressHUD()
        // Don't forget to reset when view is being removed
        AppUtility.lockOrientation(.allButUpsideDown)
        //AppUtility.lockOrientation(.portrait)
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActiveNotification), name: UIApplication.didBecomeActiveNotification, object: nil)
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.progressHUD = MBProgressHUD.showAdded(to: self.view, animated: true)
        self.progressHUD.removeFromSuperViewOnHide = false
        self.progressHUD.hide(animated: false)
    }
    
    @objc func qrCodeBarButtonItemPressed(_ sender: UIBarButtonItem) {
        openScanner()
    }
    
    func openScanner() {
        
        if (AppHelper.shared.checkScanPermissions()) {
            
            let viewController = BarcodeScannerViewController()
            viewController.codeDelegate = self
            viewController.errorDelegate = self
            viewController.dismissalDelegate = self
            
            // Add extra metadata object type
            viewController.metadata.append(AVMetadataObject.ObjectType.qr)
            viewController.metadata.append(AVMetadataObject.ObjectType.upce)
            viewController.metadata.append(AVMetadataObject.ObjectType.code39)
            viewController.metadata.append(AVMetadataObject.ObjectType.code39Mod43)
            viewController.metadata.append(AVMetadataObject.ObjectType.code93)
            viewController.metadata.append(AVMetadataObject.ObjectType.code128)
            viewController.metadata.append(AVMetadataObject.ObjectType.ean8)
            viewController.metadata.append(AVMetadataObject.ObjectType.ean13)
            viewController.metadata.append(AVMetadataObject.ObjectType.aztec)
            viewController.metadata.append(AVMetadataObject.ObjectType.pdf417)
            viewController.metadata.append(AVMetadataObject.ObjectType.itf14)
            viewController.metadata.append(AVMetadataObject.ObjectType.dataMatrix)
            viewController.metadata.append(AVMetadataObject.ObjectType.interleaved2of5)
            
            viewController.headerViewController.titleLabel.text = "Scanning"
            viewController.headerViewController.closeButton.tintColor = .white//Common.shared.APP_MAIN_COLOR //.red
            viewController.headerViewController.titleLabel.textColor = .white
            
            viewController.messageViewController.regularTintColor = .black
            viewController.messageViewController.errorTintColor = .red
            viewController.messageViewController.textLabel.textColor = .black
            
            // Change focus view style
            viewController.cameraViewController.barCodeFocusViewType = .animated
            // Show camera position button
            viewController.cameraViewController.showsCameraButton = true
            // Set settings button text
            let title = NSAttributedString(
                string: "Settings",
                attributes: [.font: UIFont.boldSystemFont(ofSize: 17), .foregroundColor : UIColor.white]
            )
            viewController.cameraViewController.settingsButton.setAttributedTitle(title, for: UIControl.State())
            
            self.present(viewController, animated: true, completion: nil)
            
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .wifi:
            DispatchQueue.main.async {
                print("Reachable via WiFi")
            }
        case .cellular:
            DispatchQueue.main.async {
                print("Reachable via Cellular")
            }
        case .none, .unavailable:
            DispatchQueue.main.async {
                AppHelper.shared.showAlert("\("Please check your network & try again!".localized())")
            }
        }
        
    }
    
    func startReachabilityNetwork() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            NotificationCenter.default.addObserver(self, selector: #selector(self.reachabilityChanged(note:)),
                                                   name: .reachabilityChanged,
                                                   object: self.reachability)
            do {
                try self.reachability.startNotifier()
            } catch {
                print("Unable to start notifier")
            }
        }
        
    }
    
    func stopReachabilityNetwork() {
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: reachability)
    }
    
    @objc func didBecomeActiveNotification(notification: NSNotification) {
    }
    
    func navTitleWithImageAndText(titleText: String, imageName: String) -> UIView {
        
        // Creates a new UIView
        let titleView = UIView()
        
        // Creates a new text label
        let label = UILabel()
        label.text = titleText
        label.sizeToFit()
        label.center = titleView.center
        label.textAlignment = NSTextAlignment.center
        label.textColor = .white
        
        // Creates the image view
        let image = UIImageView()
        image.image = UIImage(named: imageName)
        
        // Maintains the image's aspect ratio:
        let imageAspect = image.image!.size.width / image.image!.size.height
        
        // Sets the image frame so that it's immediately before the text:
        let imageX = label.frame.origin.x - label.frame.size.height * imageAspect
        let imageY = label.frame.origin.y
        
        let imageWidth = label.frame.size.height * imageAspect
        let imageHeight = label.frame.size.height
        
        image.frame = CGRect(x: imageX - 5, y: imageY, width: imageWidth, height: imageHeight)
        
        image.contentMode = UIView.ContentMode.scaleAspectFit
        
        // Adds both the label and image view to the titleView
        titleView.addSubview(label)
        titleView.addSubview(image)
        
        // Sets the titleView frame to fit within the UINavigation Title
        titleView.sizeToFit()
        
        return titleView
    }
    
    public func initPopupNavigation() {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.view.backgroundColor = UIColor.white
        let closeButton = UIBarButtonItem(title: "Close".localized(), style: .plain, target: self, action: #selector(closePopupAction(_:)))
        self.navigationController?.topViewController?.navigationItem.leftBarButtonItem = closeButton
        self.navigationController?.topViewController?.navigationItem.rightBarButtonItem = nil
        AppHelper.shared.setStyleForApp()
    }
    
    @objc func closePopupAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true) { }
    }
}

// MARK: - Common Action
extension BaseViewController: UIDocumentInteractionControllerDelegate {
    
    func showMBProgressHUD(_ message: String = "Please wait...".localized(), _ animated: Bool = true) {
        DispatchQueue.main.async {
            if self.progressHUD != nil {
                self.progressHUD.hide(animated: animated)
            }
            self.progressHUD.label.text = message
            self.progressHUD.show(animated: animated)
        }
    }
    
    func hideMBProgressHUD(_ animated: Bool = true) {
        DispatchQueue.main.async {
            if self.progressHUD != nil {
                self.progressHUD.hide(animated: animated)
            }
        }
    }
    
    func checkOpenOrDownloadFile(url: String, name: String, view: UIView?) -> Void {
        AppHelper.shared.showToast("Checking file...", false, isTapToDismissEnabled: false, position: .bottom, view: view)
        if let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let destinationFileUrl = documentsUrl.appendingPathComponent("\(name)")
            if(FileManager.default.fileExists(atPath: destinationFileUrl.path)) {
                AppHelper.shared.hideToast(0.1, view: view)
                self.showFileWithPath(path: destinationFileUrl, name: name, view: view)
            }
            else {
                AppHelper.shared.hideToast(0.1, view: view)
                self.downloadAndOpenFile(originalStringURL: url, name: name, view: view)
            }
        }
        else {
            AppHelper.shared.hideToast(0.1, view: view)
            AppHelper.shared.showAlert("Don't have permission read/write file to your phone.".localized())
        }
    }
    
    func downloadAndOpenFile(originalStringURL: String, name: String, view: UIView?) -> Void {
        
        let url = originalStringURL.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        let sucessAlert = UIAlertController(title: "Download Files".localized(), message: "Download the file withname to your mobile for offline access.".localizeWithFormat(arguments: [name]), preferredStyle: UIAlertController.Style.alert)
        sucessAlert.addAction(UIAlertAction(title: "Download".localized(), style: UIAlertAction.Style.default, handler:  { action in
            if let fileURL = URL(string: url) {
                AppHelper.shared.showToast("Downloading".localized(), false, isTapToDismissEnabled: false, position: .bottom, view: view)
                if let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                    let destinationFileUrl = documentsUrl.appendingPathComponent("\(name)")
                    let sessionConfig = URLSessionConfiguration.default
                    let session = URLSession(configuration: sessionConfig)
                    let request = URLRequest(url:fileURL)
                    let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
                        AppHelper.shared.hideToast(0.1, view: view)
                        if let tempLocalUrl = tempLocalUrl, error == nil {
                            if let statusCode = (response as? HTTPURLResponse)?.statusCode {
                                print("Successfully downloaded. Status code: \(statusCode)")
                            }
                            do {
                                if(FileManager.default.fileExists(atPath: destinationFileUrl.path)) {
                                    try FileManager.default.removeItem(at: destinationFileUrl)
                                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                                    self.showFileWithPath(path: destinationFileUrl, name: name, view: view)
                                }
                                else {
                                    try FileManager.default.copyItem(at: tempLocalUrl, to: destinationFileUrl)
                                    self.showFileWithPath(path: destinationFileUrl, name: name, view: view)
                                }
                            }
                            catch (let writeError) {
                                let sDestinationFileUrl : String = "\(destinationFileUrl)"
                                let sWriteError : String = "\(writeError)"
                                AppHelper.shared.showAlert("Error creating a file withdestinationFileUrl : withwriteError".localizeWithFormat(arguments: [sDestinationFileUrl, sWriteError]))
                                print("Error creating a file \(destinationFileUrl) : \(writeError)")
                            }
                        }
                        else {
                            AppHelper.shared.showAlert("Download failed try again later.".localized())
                            print("Error took place while downloading a file. Error description");
                        }
                    }
                    task.resume()
                }
                else {
                    AppHelper.shared.showAlert("Don't have permission write file to your phone.".localized())
                }
            } else {
                AppHelper.shared.showAlert("Can't download from url: withurl.".localizeWithFormat(arguments: [url]))
            }
        }))
        sucessAlert.addAction(UIAlertAction(title: "Cancel".localized(), style: UIAlertAction.Style.default, handler: nil))
        self.present(sucessAlert, animated: true, completion: nil)
        
    }
    
    func showFileWithPath(path: URL, name: String, view: UIView?) {
        DispatchQueue.main.async {
            AppHelper.shared.showToast("Opening file".localized(), isTapToDismissEnabled: false, position: .bottom, view: view)
            let isFileFound:Bool? = FileManager.default.fileExists(atPath: path.path)
            if isFileFound == true {
                // take instantiation off the main thread
                if #available(iOS 13.0, *) {
                    if self.traitCollection.userInterfaceStyle == .dark {
                        UINavigationBar.appearance().tintColor = UIColor.white
                        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .medium), NSAttributedString.Key.foregroundColor : Common.shared.APP_BLUE_COLOR]
                    } else {
                        UINavigationBar.appearance().tintColor = Common.shared.APP_BLUE_COLOR
                        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .medium), NSAttributedString.Key.foregroundColor : UIColor.darkText]
                    }
                } else {
                    UINavigationBar.appearance().tintColor = UIColor.black
                    UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13, weight: .medium), NSAttributedString.Key.foregroundColor : Common.shared.APP_BLUE_COLOR]
                }
                // Instantiate the interaction controller
                self.docController = UIDocumentInteractionController(url: URL(fileURLWithPath: path.path))
                self.docController.delegate = self
                self.docController.presentPreview(animated: true)
                AppHelper.shared.hideToast(0.1, view: view)
            }
        }
    }
    
    public func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
    
    public func documentInteractionControllerDidEndPreview(_ controller: UIDocumentInteractionController) {
    }
    
}

// MARK: - QRCode / Barcode Scanner
extension BaseViewController: BarcodeScannerCodeDelegate {
    
    public func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        if (code.isEmpty) {
            return
        }
        print(code)
        controller.dismiss(animated: true, completion: {
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            Broadcaster.notify(QRCodeScannerDelegate.self, block: {
                $0.receiveCode(code)
            })
        })
    }
}

extension BaseViewController: BarcodeScannerErrorDelegate {
    public func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
        controller.dismiss(animated: true, completion: nil)
    }
}

extension BaseViewController: BarcodeScannerDismissalDelegate {
    public func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
