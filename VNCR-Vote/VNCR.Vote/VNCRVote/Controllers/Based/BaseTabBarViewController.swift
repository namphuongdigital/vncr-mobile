//
//  BaseTabBarControllerViewController.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 10/29/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import UIKit

class BaseTabBarViewController: UITabBarController, UITabBarControllerDelegate {

    var bounceAnimation: CAKeyframeAnimation = {
        let bounceAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        bounceAnimation.values = [1.0, 1.5, 0.8, 1.05, 1.0]
        bounceAnimation.duration = TimeInterval(0.3)
        bounceAnimation.calculationMode = CAAnimationCalculationMode.cubic
        return bounceAnimation
    }()
    
    var rotateAnimation: CAKeyframeAnimation = {
        let rotateAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
        
        var valuesAnimation = [NSNumber]()
        let degrees:Float = 360
        let radians = degrees * Float.pi / Float(180.0)
        
        valuesAnimation.append(NSNumber(value: 0))
        valuesAnimation.append(NSNumber(value: radians / 2))
        
        rotateAnimation.values = valuesAnimation
        rotateAnimation.repeatCount = .zero
        rotateAnimation.duration = TimeInterval(0.5)
        
        return rotateAnimation
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppHelper.shared.setStyleForApp()
        // Always adopt a dark interface style.
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        // find index if the selected tab bar item, then find the corresponding view and get its image, the view position is offset by 1 because the first item is the background (at least in this case)
        guard let idx = tabBar.items?.firstIndex(of: item),
            tabBar.subviews.count > idx + 1,
            let imageView = tabBar.subviews[idx + 1].subviews.compactMap ({ $0 as? UIImageView }).first else {
            return
        }
        
        // animate the imageView
        imageView.layer.add(bounceAnimation, forKey: "bounceAnimation")
        //imageView.layer.add(rotateAnimation, forKey: "rotateAnimation")
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        guard
            let tabViewControllers = tabBarController.viewControllers,
            let targetIndex = tabViewControllers.firstIndex(of: viewController),
            let targetView = tabViewControllers[targetIndex].view,
            let currentViewController = selectedViewController,
            let currentIndex = tabViewControllers.firstIndex(of: currentViewController)
            else { return false }
        
        if currentIndex != targetIndex {
            animateToView(targetView, at: targetIndex, from: currentViewController.view, at: currentIndex)
        }
        
        return true
    }
    
}

private extension BaseTabBarViewController {
    
    func animateToView(_ toView: UIView, at toIndex: Int, from fromView: UIView, at fromIndex: Int) {
        // Position toView off screen (to the left/right of fromView)
        let screenWidth = UIScreen.main.bounds.size.width
        let offset = toIndex > fromIndex ? screenWidth : -screenWidth
        
        toView.frame.origin = CGPoint(
            x: toView.frame.origin.x + offset,
            y: toView.frame.origin.y
        )
        
        fromView.superview?.addSubview(toView)
        
        // Disable interaction during animation
        view.isUserInteractionEnabled = false
        
        UIView.animate(
            withDuration: 0.7,
            delay: 0.0,
            usingSpringWithDamping: 0.9,
            initialSpringVelocity: 0.5,
            options: .curveEaseInOut,
            animations: {
                // Slide the views by -offset
                fromView.center = CGPoint(x: fromView.center.x - offset, y: fromView.center.y)
                toView.center = CGPoint(x: toView.center.x - offset, y: toView.center.y)
        },
            completion: { _ in
                // Remove the old view from the tabbar view.
                fromView.removeFromSuperview()
                self.selectedIndex = toIndex
                self.view.isUserInteractionEnabled = true
        }
        )
    }
    
}

