//
//  BaseNavigationViewController.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 6/7/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import UIKit
import Foundation

open class BaseNavigationViewController: UINavigationController {
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override open func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppHelper.shared.setStyleForApp()
        // Always adopt a dark interface style.
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override open func viewDidLoad() {
        super.viewDidLoad()
    }
    
}
