//
//  DynamicViewController.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 6/24/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation

class DynamicViewController: UIViewController {
    
    var sContent: String = "This function is under construction.".localized()
    
    override func loadView() {
        // super.loadView()   // DO NOT CALL SUPER
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        view = UIView()
        view.backgroundColor = .lightGray
        
        let stackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        view.addSubview(stackView)
        
        NSLayoutConstraint.activate([
            stackView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            stackView.centerYAnchor.constraint(equalTo: view.centerYAnchor)
            ])
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = sContent
        stackView.addArrangedSubview(label)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
}
