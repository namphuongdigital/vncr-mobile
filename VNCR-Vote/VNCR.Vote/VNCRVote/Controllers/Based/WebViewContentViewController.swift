//
//  WebViewContentViewController.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 6/24/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import UIKit
import WebKit

class WebViewContentViewController: BaseViewController, WKUIDelegate, WKNavigationDelegate {
    
    var webView: WKWebView!
    var url: String = ""
    var htmlContent: String = ""
    var isPopover: Bool = false
    
    func initData(_ url: String, _ htmlContent: String, _ isPopover: Bool = false) -> Void {
        self.url = url
        self.htmlContent = htmlContent
        self.isPopover = isPopover
    }
    
    @objc func closeAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    private func initStyleForPop() {
        self.view.backgroundColor = UIColor.white
        let leftButton = UIBarButtonItem(title: "Close".localized(), style: .plain, target: self, action: #selector(closeAction(_:)))
        self.navigationController?.topViewController?.navigationItem.leftBarButtonItem = leftButton
        self.navigationController?.topViewController?.navigationItem.rightBarButtonItem = nil
        AppHelper.shared.setStyleForApp()
    }
    
    private func loadDataWebView() {
        if (!url.isEmpty) {
            let myURL = URL(string: url)
            let myRequest = URLRequest(url: myURL!)
            self.showMBProgressHUD()
            webView.load(myRequest)
        } else if (!htmlContent.isEmpty) {
            webView.loadHTMLString(htmlContent, baseURL: nil)
        } else {
            AppHelper.shared.showAlert("Empty".localized())
        }
    }
    
    override func loadView() {
        webView = WKWebView(frame: .zero)
        webView.backgroundColor = UIColor.white
        webView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        webView.uiDelegate = self
        webView.navigationDelegate = self
        self.view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        if self.isPopover {
            initStyleForPop()
        }
        loadDataWebView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isPopover {
            initStyleForPop()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.isPopover {
            initStyleForPop()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.hideMBProgressHUD()
    }
    
}
