//
//  ExamMainViewController.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 12/24/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import UIKit

class ExamMainViewController: BaseViewController {

    @IBAction func tappedPrev(_ sender: Any) {
        self.checkPaging(self.pageIndex-1)
    }
    
    @IBAction func tappedNext(_ sender: Any) {
        self.checkPaging(self.pageIndex+1)
    }
    
    @IBAction func tappedFinish(_ sender: Any) {
        doFinish()
    }
    
    @objc func confirmAction(_ sender: UIBarButtonItem) {
        doFinish()
    }
    
    @objc func backAction(_ sender: UIBarButtonItem) {
        AppHelper.shared.checkUserStatus()
    }
    
    private func config() {
        
        btnPrev.setTitle("Previous".localized(), for: .normal)
        btnPrev.initUI(Common.shared.APP_BLUE_COLOR, .white)
        
        btnNext.setTitle("Next".localized(), for: .normal)
        btnNext.initUI(Common.shared.APP_BLUE_COLOR, .white)

        btnFinish.setTitle("Finish".localized(), for: .normal)
        btnFinish.initUI(Common.shared.APP_GREEN_COLOR, .white)
        btnFinish.isHidden = true
        
        lblQuestion.textColor = .darkText
        lblQuestion.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        
        lblAgree.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        lblAgree.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        lblAgree.text = "Complete to vote".localized()
        
        imgCheck.image = #imageLiteral(resourceName: "ic_square_unchecked")
        imgCheck.isUserInteractionEnabled = true
        imgCheck.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imgCheckTapped(tapGestureRecognizer:))))
        
        viewCheck.isUserInteractionEnabled = true
        viewCheck.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(imgCheckTapped(tapGestureRecognizer:))))

        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        tableView.register(ExamAnswerCell.nib, forCellReuseIdentifier: ExamAnswerCell.identifier)
        tableView.sectionHeaderHeight = UITableView.automaticDimension
        tableView.estimatedSectionHeaderHeight = 45
        
        self.checkPaging(0)
    }
    
    @objc func imgCheckTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        self.mainModel.IsVoted = true
        self.imgCheck.image = self.mainModel.IsVoted ? #imageLiteral(resourceName: "ic_square_checked") : #imageLiteral(resourceName: "ic_square_unchecked")
    }
    
    private func checkPaging(_ page: Int) {
        DispatchQueue.main.async {
            if page < 0 {
                return
            } else if page >= self.list.count {
                return
            }
            self.btnPrev.isHidden = false
            self.btnNext.isHidden = false
            self.pageIndex = page
            if self.pageIndex == 0 {
                self.btnPrev.isHidden = true
            } else if self.pageIndex == self.list.count - 1 {
                self.btnNext.isHidden = true
            } else {
                self.btnPrev.isHidden = false
                self.btnNext.isHidden = false
            }
            self.getData(page: self.pageIndex)
        }
    }
    
    private func getData(page: Int) {
        self.mainModel = list[page]
        lblQuestion.text = self.mainModel.Title
        lblQuestion.textColor = UIColor(self.mainModel.TitleTextColor)
        imgCheck.image = self.mainModel.IsVoted ? #imageLiteral(resourceName: "ic_square_checked") : #imageLiteral(resourceName: "ic_square_unchecked")
        tableView.reloadData()
        if self.mainModel.Groups.count > 0 && self.mainModel.Groups[0].Candidates.count > 0 {
            tableView.scrollToRow(at: IndexPath.init(row: 0, section: 0), at: .none, animated: false)
        }
    }
    
    private func submit(completion: @escaping (Bool) -> Void) {
        VoteManager.shared.doSubmitMultiple(code: self.code, data: self.list) { (responseModel, returnData) in
            if responseModel.RetStatus {
                DispatchQueue.main.async {
                    self.list = returnData ?? []
                    self.checkPaging(0)
                }
            }
            completion(responseModel.RetStatus)
        }
    }
    
    private func getCountVoted() -> Int {
        var countVoted = 0
        for item in self.list {
            if item.IsVoted {
                countVoted += 1
            }
        }
        return countVoted
    }
    
    private func doFinish() {
        let countVoted = getCountVoted()
        if countVoted < self.list.count {
            AppHelper.shared.showAlert("\("Please complete all before finish.".localized()) (\(countVoted)/\(self.list.count))")
            return
        }
        
        let msg = "Would you like to finish?".localized()
        EZAlertController.alert(Common.shared.APP_NAME, message: msg, buttons: ["Cancel".localized(),"Ok".localized()]) { (action, position) in
            if position == 1 {
                self.showMBProgressHUD()
                self.submit { isSuccess in
                    self.hideMBProgressHUD()
                    if isSuccess {
                        DispatchQueue.main.async {
                            AppHelper.shared.showAlert("Anh/Chị đã hoàn tất bỏ phiếu.\nTrân trọng cảm ơn!", completion: {
                                AppHelper.shared.checkUserStatus()
                            })
                        }
                    }
                }
            }
        }
    }
    
    private func getDataBySection(_ section: Int) -> VoteGroupModel {
        return mainModel.Groups[section]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        title = "Vote".localized()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        //AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
        self.view.backgroundColor = UIColor.white //Common.shared.APP_MAIN_COLOR
        
        let leftButton = UIBarButtonItem(title: "Back".localized(), style: .done, target: self, action: #selector(backAction(_:)))
        leftButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,
                                            NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 17)], for: .normal)
        self.navigationController?.topViewController?.navigationItem.leftBarButtonItem = leftButton
        
        if !isReview {
            let rightButton = UIBarButtonItem(title: "Finish".localized(), style: .done, target: self, action: #selector(confirmAction(_:)))
            rightButton.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white,
                                                NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 17)], for: .normal)
            self.navigationController?.topViewController?.navigationItem.rightBarButtonItem = rightButton
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    var list: [VoteModel] = []
    var mainModel: VoteModel!
    var code: String = ""
    var pageIndex: Int = 0
    var isReview: Bool = false

    @IBOutlet weak var lblQuestion: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnPrev: ButtonTemplate!
    @IBOutlet weak var btnNext: ButtonTemplate!
    @IBOutlet weak var btnFinish: ButtonTemplate!
    
    @IBOutlet weak var lblAgree: UILabel!
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var viewCheck: UIView!

}


extension ExamMainViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if list.count == 0 {
            return 0
        } else if self.mainModel == nil {
            self.mainModel = list[0]
        }
        return self.mainModel.Groups.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let model = self.getDataBySection(section)
        let headerView: CustomHeaderForAccounting = .fromNib()
        headerView.setData(model.Title,
                           UIColor(model.TitleTextColor),
                           Common.shared.APP_BLUE_COLOR,
                           model.Description,
                           Common.shared.APP_RED_COLOR, nil, #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1))
        return headerView
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 45
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mainModel.Groups.count == 0 {
            tableView.showNoData()
        } else {
            tableView.removeNoData()
        }
        let data = self.getDataBySection(section)
        return data.Candidates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ExamAnswerCell.identifier, for: indexPath) as? ExamAnswerCell else {
            return UITableViewCell()
        }
        let data = self.getDataBySection(indexPath.section)
        let listQuestion = data.Candidates
        if indexPath.row < listQuestion.count {
            cell.setData(data: listQuestion[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: false)
        if isReview {return}
        
        let data = self.getDataBySection(indexPath.section)
        let listQuestion = data.Candidates
        if indexPath.row >= listQuestion.count {
            return
        }
        
        let selectedData = listQuestion[indexPath.row]
        for i in 0..<listQuestion.count {
            if listQuestion[i].Id == selectedData.Id {
                listQuestion[i].IsSelected = !listQuestion[i].IsSelected
                if listQuestion[i].IsSelected {
                    AppHelper.shared.playSuccess()
                } else {
                    AppHelper.shared.playError()
                }
                break
            }
        }
        self.tableView.beginUpdates()
        self.tableView.reloadRows(at: [indexPath], with: .automatic)
        self.tableView.endUpdates()
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        return []
    }
}
