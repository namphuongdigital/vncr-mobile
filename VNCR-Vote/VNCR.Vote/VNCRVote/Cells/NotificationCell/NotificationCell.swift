//
//  NotificationCell.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 9/29/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    
    // MARK: -  function
    func setData(data: NotificationModel) {
        titleLabel.text = data.Title
        descriptionLabel.text = data.Content
        timeLabel.text = data.CreatedDate?.toChatString()
    }
    
    // MARK: -  private
    private func config() {
        containView.layer.masksToBounds = false
        containView.layer.shadowOffset = CGSize(width: 1, height: 1)
        containView.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        containView.layer.shadowOpacity = 0.5
        containView.layer.cornerRadius = 3
        containView.layer.shadowRadius = 2
    }
    
    // MARK: -  init
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        config()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        descriptionLabel.text = nil
        titleLabel.text = nil
        timeLabel.text = nil
    }
    
    // MARK: -  properties
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    

    // MARK: -  oulet
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
}

class MailIndicatorView: UIView {
    
    var indicatorColor : UIColor {
        didSet {
            self.setNeedsDisplay()
        }
    }
    var innerColor : UIColor? {
        didSet {
            self.setNeedsDisplay()
        }
    }
    
    override init(frame:CGRect) {
        indicatorColor = UIColor.clear
        super.init(frame:frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        indicatorColor = UIColor.clear
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        let ctx = UIGraphicsGetCurrentContext()
        ctx?.addEllipse(in: rect)
        ctx?.setFillColor(indicatorColor.cgColor.components!)
        ctx?.fillPath()
        
        if innerColor != nil {
            let innerSize = rect.size.width * 0.5
            let innerRect = CGRect(x: rect.origin.x + rect.size.width * 0.5 - innerSize * 0.5,
                                   y: rect.origin.y + rect.size.height * 0.5 - innerSize * 0.5,
                                   width: innerSize, height: innerSize)
            ctx?.addEllipse(in: innerRect)
            ctx?.setFillColor(innerColor!.cgColor.components!)
            ctx?.fillPath()
        }
    }
}
