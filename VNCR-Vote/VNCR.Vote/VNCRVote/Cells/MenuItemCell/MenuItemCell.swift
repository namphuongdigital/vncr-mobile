//
//  MenuItemCell.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 11/11/19.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

class MenuItemCell: UITableViewCell {

    // MARK: -  function
    func setData(data:[String:Any], isIconRounded: Bool = false, _ iconSize: CGSize = CGSize(width: 24, height: 24)) {
        
        localData = data
        
        var keepOriginalColorIcon = false
        if let d = localData?["keepOriginalIconColor"] as? Bool {
            keepOriginalColorIcon = d
        }
        
        var iconColor = UIColor.black
        if let color = localData?["color"] as? UIColor {
            iconColor = color
        }
        
        var textColor = UIColor.black
        if let color = localData?["colorText"] as? UIColor {
            textColor = color
        }
        
        if let d = localData?["icon"] as? String {
            imgViewLeft.isHidden = false
            if d.starts(with: "http") {
                imgViewLeft.loadImageUsingCacheWithURLString(d, size: iconSize, placeHolder: #imageLiteral(resourceName: "ic_filetype_image"), true)
                imgViewLeft.image = imgViewLeft.image?.resizeImageWith(newSize: iconSize)
            } else {
                if (keepOriginalColorIcon) {
                    imgViewLeft.image = UIImage(named: d)?.resizeImageWith(newSize: iconSize)
                } else {
                    imgViewLeft.image = UIImage(named: d)?.resizeImageWith(newSize: iconSize).tint(with: iconColor)
                }
            }
        } else if let d = localData?["icon"] as? UIImage {
            imgViewLeft.isHidden = false
            if (keepOriginalColorIcon) {
                imgViewLeft.image = d.resizeImageWith(newSize: iconSize)
            } else {
                imgViewLeft.image = d.resizeImageWith(newSize: iconSize).tint(with: iconColor)
            }
        }
        
        var iconColorBadge = UIColor.white
        var iconBackgroundColorBadge = UIColor.red
        var iconValueBadge = ""
        
        if let valueBadge = localData?["valueBadge"] as? String {
            iconValueBadge = valueBadge
        }
        
        if let colorBadge = localData?["colorBadge"] as? UIColor, let backgroundColorBadge = localData?["backgroundColorBadge"] as? UIColor {
            iconColorBadge = colorBadge
            iconBackgroundColorBadge = backgroundColorBadge
        }
        
        imgViewLeft.setBagString(iconValueBadge, 2, iconBackgroundColorBadge, iconColorBadge)
        
        if (isIconRounded) {
            imgViewLeft.layer.masksToBounds = false
            imgViewLeft.layer.cornerRadius = imgViewLeft.frame.height/2
            imgViewLeft.clipsToBounds = true
        }
        
        if let d = localData?["title"] as? String {
            titleLabel.text = d
            titleLabel.textColor = textColor
        }
        
    }
    
    func setData(data: QuestionItemModel) {
        imgViewLeft.isHidden = false
        self.imgViewLeft.image = #imageLiteral(resourceName: "ic_star").resizeImageWith(newSize: CGSize(width: 24, height: 24)).tint(with: Common.shared.APP_ORANGE_COLOR)
        self.titleLabel.tintColor = UIColor.darkText
//        AppHelper.shared.initAllQuestions(data.id)
//        var testedCount = 0
//        if let existRandom = UserDefaults.standard.value(forKey: data.title) as? [Int] {
//            testedCount = existRandom.count
//        }
//        self.titleLabel.text = "\(data.title) - \(testedCount)/\(AppHelper.shared.LIST_ALL_QUESTION.count)"
//        if testedCount == AppHelper.shared.LIST_ALL_QUESTION.count {
//            self.imgViewLeft.image = #imageLiteral(resourceName: "ic_star").resizeImageWith(newSize: CGSize(width: 24, height: 24)).tint(with: Common.shared.APP_BLUE_COLOR)
//            self.titleLabel.tintColor = Common.shared.APP_BLUE_COLOR
//        }
    }
    
    // MARK: -  private
    private func config() {
        imgViewLeft.image = nil
        imgViewLeft.isHidden = true
        titleLabel.text = nil
        localData = nil
    }
    
    // MARK: -  init
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        config()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        imgViewLeft.image = nil
        imgViewLeft.isHidden = true
        titleLabel.text = nil
        localData = nil
    }
    
    // MARK: -  properties
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    
    var localData:[String:Any]?
    
    // MARK: -  oulet
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgViewLeft: UIImageView!
    @IBOutlet weak var containView: UIView!
}
