//
//  CustomHeader.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 5/17/19.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

class CustomHeader: UIView {

    // MARK: -  function
    func setData(_ titleText: String, _ titleColor: UIColor = UIColor.black, _ colorLine: UIColor = #colorLiteral(red: 0.9360817075, green: 0.4546757936, blue: 0, alpha: 1)) {
        titleLabel.text = titleText
        titleLabel.textColor = titleColor
        separateView.backgroundColor = colorLine
    }
    
    // MARK: -  properties
    
    // MARK: -  oulet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separateView: UIView!
    
}
