//
//  CustomHeader.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 10/4/19.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

class CustomHeaderForAccounting: UIView {

    // MARK: -  function
    func setData(_ titleText: String,
                 _ titleColor: UIColor = UIColor.black,
                 _ colorLine: UIColor = #colorLiteral(red: 0.9360817075, green: 0.4546757936, blue: 0, alpha: 1),
                 _ subText: String = "",
                 _ subColor: UIColor = UIColor.black,
                 _ rightImg: UIImage? = UIImage.init(named: "ic_location")!,
                 _ backgroundColor: UIColor = UIColor.white) {
        
        titleLabel.text = titleText + ((!titleText.isEmpty && !subText.isEmpty) ? ": " : "") + subText
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        titleLabel.textColor = titleColor
        titleLabel.textAlignment = .justified
        titleLabel.textColorChange(listChanges: [subText : subColor])
        
        separateView.backgroundColor = colorLine
        self.backgroundColor = backgroundColor
    }
    
    // MARK: -  properties
    
    // MARK: -  oulet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separateView: UIView!
}
