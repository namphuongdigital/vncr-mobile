//
//  CustomHeaderWithRightInfo.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 10/4/19.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

class CustomHeaderWithRightInfo: UIView {

    // MARK: -  function
    func setData(_ titleText: String,
                 _ titleColor: UIColor = UIColor.black,
                 _ colorLine: UIColor = #colorLiteral(red: 0.9360817075, green: 0.4546757936, blue: 0, alpha: 1),
                 _ rightText: String = "",
                 _ rightColor: UIColor = UIColor.black,
                 _ rightImg: UIImage? = UIImage.init(named: "ic_location")!,
                 _ backgroundColor: UIColor = UIColor.white) {
        
        titleLabel.text = titleText
        titleLabel.textColor = titleColor
        separateView.backgroundColor = colorLine
        rightLabel.text = rightText
        rightLabel.textColor = rightColor
        
        rightImageView.image = rightImg
        self.backgroundColor = backgroundColor
    }
    
    // MARK: -  properties
    
    // MARK: -  oulet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var separateView: UIView!
    @IBOutlet weak var rightLabel: UILabel!
    @IBOutlet weak var rightImageView: UIImageView!
}
