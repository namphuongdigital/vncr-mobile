//
//  ItemMenuCollectionViewCell.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 9/16/19.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//


import UIKit

class ItemMenuCollectionViewCell: UICollectionViewCell {
    
    weak var menuItemModel: MenuItemModel? {
        didSet {
            self.loadData()
        }
    }
    
    let iconSize: CGSize = CGSize(width: 60, height: 60)
        
    @IBOutlet weak var textBagdesLabel: UILabel!
    
    @IBOutlet weak var textBagdesView: UIView!
    
    @IBOutlet weak var textTitleLabel: UILabel!
        
    @IBOutlet weak var imageViewAvatar: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.imageViewAvatar.clipsToBounds = true
        
        self.textBagdesView.clipsToBounds = true
        self.textBagdesView.layer.cornerRadius = self.textBagdesView.frame.height / 2.0
        self.textBagdesView.backgroundColor = UIColor.red
        self.textBagdesLabel.textColor = UIColor.white

        self.layer.masksToBounds = false
        self.layer.shadowOffset = CGSize(width: 2, height: 2)
        self.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        self.layer.shadowOpacity = 0.5
        self.layer.cornerRadius = 5
        self.layer.shadowRadius = 2
        self.backgroundColor = Common.shared.APP_MAIN_COLOR //Common.shared.APP_BLUE_COLOR
    }
    
    func loadData() {

        if let menuItemModel = self.menuItemModel {
            
            self.textBagdesView.isHidden = menuItemModel.badges == 0
            self.textBagdesLabel.text = String.init(format: "%d", menuItemModel.badges)
            //self.imageViewAvatar.setBag(menuItemModel.badges, 2)
            self.textTitleLabel.text = menuItemModel.title
            self.textTitleLabel.textColor = menuItemModel.color
            self.backgroundColor = menuItemModel.backgroundColor

            let d = menuItemModel.imageUrl
            if d.starts(with: "http") {
                self.imageViewAvatar.loadImageUsingCacheWithURLString(d, size: iconSize, placeHolder: #imageLiteral(resourceName: "no_image_available"), true)
            } else {
                if (menuItemModel.keepOriginalColorIcon) {
                    self.imageViewAvatar.image = UIImage(named: d)?.resizeImageWith(newSize: iconSize)
                } else {
                    self.imageViewAvatar.image = UIImage(named: d)?.tint(with: menuItemModel.color).resizeImageWith(newSize: iconSize)
                }
            }
            
        }
        
    }
    
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    

}
