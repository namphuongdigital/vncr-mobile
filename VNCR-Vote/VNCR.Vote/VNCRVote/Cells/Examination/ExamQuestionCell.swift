//
//  ExamQuestionCell.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 1/18/20.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import UIKit

class ExamQuestionCell: UITableViewCell {

    // MARK: -  function
    func setData(data: QuestionItemModel, selectedStatus: Int) {
        
        let labelTopLeft = CustomNormalLabel(frame: .zero)
        labelTopLeft.numberOfLines = 0
        labelTopLeft.lineBreakMode = .byWordWrapping
        labelTopLeft.textAlignment = .left
        labelTopLeft.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        labelTopLeft.text = data.title
        labelTopLeft.setContentHuggingPriority(UILayoutPriority.defaultLow, for:.horizontal)
        
        let stackViewTop = UIStackView(arrangedSubviews: [labelTopLeft])
        stackViewTop.axis = .horizontal
        stackViewTop.distribution = .fill
        stackViewTop.alignment = .fill
        stackViewTop.spacing = 5
        stackViewTop.translatesAutoresizingMaskIntoConstraints = false
        stackViewTop.isLayoutMarginsRelativeArrangement = true
        if #available(iOS 11.0, *) {
            stackViewTop.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 10, trailing: 0)
        } else {
            stackViewTop.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        }
        
        stackValues.addArrangedSubview(stackViewTop)
        
        if (data.answers.count > 0) {
            
            data.answers.sorted(by: { $0.title < $1.title }).forEach { (item) in
                
                var isAdd = true
                if selectedStatus == 1 && !item.isCorrect { isAdd = false }
                
                if isAdd {
                    
                    let labelChildTopLeft = CustomNormalLabel(frame: .zero)
                    labelChildTopLeft.numberOfLines = 0
                    labelChildTopLeft.lineBreakMode = .byWordWrapping
                    labelChildTopLeft.textAlignment = .left
                    labelChildTopLeft.text = item.title
                    labelChildTopLeft.setContentHuggingPriority(UILayoutPriority.init(rawValue: 1000), for:.horizontal)
                    if item.isCorrect {
                        labelChildTopLeft.font = UIFont.systemFont(ofSize: 12, weight: .bold)
                        labelChildTopLeft.textColor = Common.shared.APP_GREEN_COLOR
                    } else {
                        labelChildTopLeft.font = UIFont.systemFont(ofSize: 12, weight: .regular)
                        labelChildTopLeft.textColor = UIColor.darkText
                    }
                    
                    let stackViewChildBot = UIStackView(arrangedSubviews: [labelChildTopLeft])
                    stackViewChildBot.axis = .horizontal
                    stackViewChildBot.distribution = .fill
                    stackViewChildBot.alignment = .fill
                    stackViewChildBot.spacing = 5
                    stackViewChildBot.translatesAutoresizingMaskIntoConstraints = false
                    stackViewChildBot.isLayoutMarginsRelativeArrangement = true
                    if #available(iOS 11.0, *) {
                        stackViewChildBot.directionalLayoutMargins = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 5, trailing: 0)
                    } else {
                        stackViewChildBot.layoutMargins = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
                    }
                    
                    stackValues.addArrangedSubview(stackViewChildBot)
                    
                }
                
            }
            
        }
        
        self.layoutIfNeeded()
    }
    
    // MARK: -  private
    private func config() {
        containView.layer.masksToBounds = false
        containView.layer.shadowOffset = CGSize(width: 2, height: 2)
        containView.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        containView.layer.shadowOpacity = 0.5
        containView.layer.cornerRadius = 5
        containView.layer.shadowRadius = 2
    }
    
    // MARK: -  init
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        config()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        stackValues.arrangedSubviews.forEach({
            $0.removeFromSuperview()
        })
    }
    
    // MARK: -  properties
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    
    // MARK: -  outlets
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var stackValues: UIStackView!
    
}
