//
//  ExamAnswerCell.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 12/24/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import UIKit
import AVFoundation

class ExamAnswerCell: UITableViewCell {

    func setData(data: VoteCandidateModel) {
        lblTitle.text = data.Title
        lblDescription.text = data.Description
        imgCheck.image = data.IsSelected ? #imageLiteral(resourceName: "ic_square_checked") : #imageLiteral(resourceName: "ic_square_unchecked")
        lblTitle.textColor = UIColor.init(data.TitleTextColor)
        lblDescription.textColor = UIColor.init(data.DescriptionTextColor)
        if !data.IsSelected {
            lblTitle.addStrike(text: data.Title)
        } else {
            lblTitle.removetrike()
        }
        imgAvatar.loadImageProgress(url: URL.init(string: data.ImageUrl))
    }
    
    // MARK: -  private
    private func config() {
        loadDefault()
    }
    
    private func loadDefault() {
        lblTitle.text = ""
        lblDescription.text = ""
        imgCheck.image = #imageLiteral(resourceName: "ic_square_unchecked")
        lblTitle.textColor = .darkText
        lblDescription.textColor = .darkGray
        lblTitle.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        lblDescription.font = UIFont.systemFont(ofSize: 13, weight: .regular)
        
        imgAvatar.image = nil
        imgAvatar.isHidden = false
        imgAvatar.layer.masksToBounds = false
        imgAvatar.layer.cornerRadius = 5//imgAvatar.frame.height/2
        imgAvatar.clipsToBounds = true
        imgAvatar.contentMode = .scaleAspectFit
    }
    
    // MARK: -  init
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        config()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        loadDefault()
    }
    
    // MARK: -  properties
    var currentData: AnswerItemModel?
    
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    
    @IBOutlet weak var imgCheck: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgAvatar: UIImageViewProgress!

}
