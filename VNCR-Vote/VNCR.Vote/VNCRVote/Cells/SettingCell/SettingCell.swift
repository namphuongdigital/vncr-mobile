//
//  SettingCell.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 9/29/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    // MARK: -  function
    func setData(data:[String:Any], isIconRounded: Bool = false, _ iconSize: CGSize = CGSize(width: 24, height: 24)) {
        
        localData = data
        
        var keepOriginalColorIcon = false
        if let d = localData?["keepOriginalIconColor"] as? Bool {
            keepOriginalColorIcon = d
        }
        
        var iconColor = UIColor.black
        if let color = localData?["color"] as? UIColor {
            iconColor = color
        }
        
        var textColor = UIColor.black
        if let color = localData?["colorText"] as? UIColor {
            textColor = color
        }
        
        if let d = localData?["icon"] as? String {
            imgViewLeft.isHidden = false
            if d.starts(with: "http") {
                imgViewLeft.loadImageUsingCacheWithURLString(d, size: iconSize, placeHolder: #imageLiteral(resourceName: "ic_filetype_image"), true)
                imgViewLeft.image = imgViewLeft.image?.resizeImageWith(newSize: iconSize)
            } else {
                if (keepOriginalColorIcon) {
                    imgViewLeft.image = UIImage(named: d)?.resizeImageWith(newSize: iconSize)
                } else {
                    imgViewLeft.image = UIImage(named: d)?.resizeImageWith(newSize: iconSize).tint(with: iconColor)
                }
            }
        } else if let d = localData?["icon"] as? UIImage {
            imgViewLeft.isHidden = false
            if (keepOriginalColorIcon) {
                imgViewLeft.image = d.resizeImageWith(newSize: iconSize)
            } else {
                imgViewLeft.image = d.resizeImageWith(newSize: iconSize).tint(with: iconColor)
            }
        }
        
        var iconColorBadge = UIColor.white
        var iconBackgroundColorBadge = UIColor.red
        var iconValueBadge = ""
        
        if let valueBadge = localData?["valueBadge"] as? String {
            iconValueBadge = valueBadge
        }
        
        if let colorBadge = localData?["colorBadge"] as? UIColor, let backgroundColorBadge = localData?["backgroundColorBadge"] as? UIColor {
            iconColorBadge = colorBadge
            iconBackgroundColorBadge = backgroundColorBadge
        }
        
        imgViewLeft.setBagString(iconValueBadge, 2, iconBackgroundColorBadge, iconColorBadge)
        
        if (isIconRounded) {
            imgViewLeft.layer.masksToBounds = false
            imgViewLeft.layer.cornerRadius = imgViewLeft.frame.height/2
            imgViewLeft.clipsToBounds = true
        }
        
        if let d = localData?["hasChild"] as? Bool {
            imgViewRight.isHidden = false
            imgViewRight.image = d ? UIImage(named: "ic_arrow_right")?.resizeImageWith(newSize: CGSize(width: 24, height: 24)).tint(with: UIColor.black) : nil
        }
        
        if let d = localData?["title"] as? String {
            titleLabel.text = d
            titleLabel.textColor = textColor
        }
        
    }
    
    func setAttachmentData(data: VoteDocModel) {
        self.setData(data: ["title": data.FullName, "icon": #imageLiteral(resourceName: "ic_filetype_pdf"), "colorText": UIColor(data.TextColor), "keepOriginalIconColor": true])
    }
    
    // MARK: -  private
    private func config() {
        imgViewLeft.image = nil
        imgViewLeft.isHidden = true
        imgViewRight.image = nil
        imgViewRight.isHidden = true
        titleLabel.text = nil
        localData = nil
    }
    
    // MARK: -  init
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        config()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        imgViewLeft.image = nil
        imgViewLeft.isHidden = true
        imgViewRight.image = nil
        imgViewRight.isHidden = true
        titleLabel.text = nil
        localData = nil
    }
    
    // MARK: -  properties
    static var identifier:String {
        return String(describing: self)
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: Bundle.main)
    }
    
    var localData:[String:Any]?
    
    // MARK: -  oulet
    @IBOutlet weak var mainContentView: UIView!
    @IBOutlet weak var imgViewRight: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imgViewLeft: UIImageView!
    @IBOutlet weak var containView: UIView!
    
}
