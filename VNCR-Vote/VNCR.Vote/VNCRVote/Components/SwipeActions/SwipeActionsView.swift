//
//  ActionsView.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 9/29/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

protocol ActionsViewDelegete: class {
    func actionsView(view:SwipeActionsView, press button:UIButton, identifier:String?)
}

class SwipeActionsView: UIView {
    
    // MARK: -  function
    func setButtons(listdescs:[[String:String]]) {
        
        listButtons.removeAll()
        listStacks.removeAll()
        listStacks.insert(mainStack, at: 0)
        
        for (index,desc) in listdescs.enumerated() {
            let btn = UIButton(type: .custom)
            
            configButton(btn)
            
            if let imageName = desc["image"] {
                btn.setImage(UIImage(named: imageName), for: UIControl.State())
            }
            if let identifier = desc["identifier"] {
                btn.accessibilityIdentifier = identifier
            }
            if let title = desc["title"] {
                btn.setTitle("   \(title)   ", for: UIControl.State())
            }
            
            listButtons.append(btn)
            
            if index < listdescs.count - 2 {
                let stack = UIStackView()
                configStackView(stack)
                stack.accessibilityIdentifier = "\(index)"
                listStacks.append(stack)
            }
        }
        
        for i in 0..<listStacks.count {
            let stack = listStacks.reversed()[i]
            if i == 0 {
                stack.addArrangedSubview(listButtons.reversed()[i + 1])
                stack.addArrangedSubview(listButtons.reversed()[i])
            } else {
                stack.addArrangedSubview(listButtons.reversed()[i+1])
                stack.addArrangedSubview(listStacks.reversed()[i-1])
            }
            stack.bringSubviewToFront(stack.subviews[0])
        }
    }
    
    func releaseReference() {
        self.removeGestureRecognizer(panGesture)
    }
    
    func refresh() {
        _ = listStacks.map { st in
            var temp = CGFloat(0)
            _ = st.subviews.map {
                if $0.isKind(of: UIButton.self) {
                    temp = max(temp,$0.frame.width)
                }
            }
            st.spacing = -temp
        }
        currentSpacing = -getTotalSpacingStack()
    }
    
    // MARK: -  handle action button
    @objc func handleActionButton(_ button:UIButton) {
        guard let d = delegate else {return}
        
        d.actionsView(view: self, press: button, identifier: button.accessibilityIdentifier)
    }
    
    // MARK: - handle gesture
    var beginDragXPoint:CGFloat = 0
    var isCollapse:Bool = false
    @objc func handleGesture(pan:UIPanGestureRecognizer) {
        let translation = pan.translation(in: self)
        switch pan.state {
        case .began:
            beginDragXPoint = translation.x
            let velocity = pan.velocity(in: pan.view)
            isCollapse = velocity.x > 0
            break
        case .ended:
            
            beginDragXPoint = translation.x
            let halfWidth = (getTotalSpacingStack() > self.frame.width ? self.frame.width : getTotalSpacingStack()) /  (isCollapse ? 1.3 : 2)
            if mainStack.frame.width > halfWidth {
                _ = listStacks.map {
                    $0.spacing = 0
                }
                currentSpacing = 0
            } else {
                refresh()
            }
            
            self.setNeedsLayout()
            UIView.animate(withDuration: 0.2, animations: {
                self.layoutIfNeeded()
            })
            break
        case .changed:
            currentSpacing -= translation.x - beginDragXPoint
            if currentSpacing >= 0 {
                currentSpacing = 0
            } else if currentSpacing <= -getTotalSpacingStack() {
                currentSpacing = -getTotalSpacingStack()
            }
            
            for i in 0..<listStacks.count {
                let max = i == 0 ? -CGFloat(0) : -getSpacingStack(listStacks[i])// -widthButton*CGFloat(i)
                let min = max + (i < listStacks.count - 1 ? -getSpacingStack(listStacks[i + 1]) : -getSpacingStack(listStacks[i]) )//-widthButton*CGFloat(i + 1)
                
                var stack:UIStackView?
                let indexStack = listStacks.count - 1 - i
                if indexStack < listStacks.count && indexStack >= 0 {
                    stack = listStacks[indexStack]
                }
                if (currentSpacing < max && currentSpacing >= min) {
                    
                    guard let st = stack else {
                        continue
                    }
                    
                    if isCollapse {
                        if indexStack + 1 < listStacks.count {
                            let stack2 = listStacks[indexStack + 1]
                            stack2.spacing = -getSpacingStack(stack2)
                        }
                    } else {
                        if indexStack - 1 >= 0 {
                            let stack2 = listStacks[indexStack - 1]
                            stack2.spacing = 0
                        }
                    }
                    self.changeSpacing(stack: st, current: currentSpacing, min: min, max: max)
                } else {
                    continue
                }
            }
            
            self.setNeedsLayout()
            beginDragXPoint = translation.x
            break
        case .possible:
            break
        case .cancelled:
            print("calcled")
        case .failed:
            print("failed")
        @unknown default:
            print("unknown")
        }
    }
    
    // MARK: -  private
    private func getTotalSpacingStack()->CGFloat {
        var spacing = CGFloat(0)
        _ = self.listStacks.map { st in
            var temp = CGFloat(0)
            _ = st.subviews.map {
                if $0.isKind(of: UIButton.self) {
                    temp = max(temp,$0.frame.width)
                }
            }
            spacing += temp
        }
        return spacing
    }
    
    private func getSpacingStack(_ stack:UIStackView)->CGFloat {
        var spacing = CGFloat(0)
        _ = stack.subviews.map {
            if $0.isKind(of: UIButton.self) {
                spacing = max(spacing,$0.frame.width)
            }
        }
        return spacing
    }
    
    private func changeSpacing(stack:UIStackView, current:CGFloat, min:CGFloat, max:CGFloat) {
        if !self.isValidSpacing(stack.spacing) {return}
        stack.spacing = current - max
    }
    
    private func isValidSpacing(_ space:CGFloat) -> Bool{
        return space <= 0 && space >= -widthButton
    }
    
    private func configButton(_ btn:UIButton) {
        btn.backgroundColor = UIColor(red:   CGFloat(arc4random()) / CGFloat(UInt32.max),
                                      green: CGFloat(arc4random()) / CGFloat(UInt32.max),
                                      blue:  CGFloat(arc4random()) / CGFloat(UInt32.max),
                                      alpha: 1.0)
        btn.addTarget(self, action: #selector(handleActionButton(_:)), for: .touchUpInside)
    }
    
    private func configStackView(_ st:UIStackView) {
        st.alignment = .fill
        st.distribution = .fill
        st.axis = .horizontal
    }
    
    private func config() {
        setButtons(listdescs: [["image":"1","identifier":"start","title":"Start"],
                               ["image":"2","identifier":"done","title":"Done"],
                               ["image":"2","identifier":"receive","title":"Receive"],
                               ["image":"2","identifier":"receive","title":"Pin"]])
        
        mainStack.spacing = -widthButton
        
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(handleGesture))
        addGestureRecognizer(panGesture)
    }
    
    private func loadNIb() {
        Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNIb()
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNIb()
        config()
    }
    
    // MARK: - outlet
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var mainStack: UIStackView!
    
    // MARK: -  properties
    
    weak var delegate:ActionsViewDelegete?
    
    var listButtons:[UIButton] = []
    var listStacks:[UIStackView] = []
    
    var panGesture:UIPanGestureRecognizer!
    var stack1: UIStackView!
    var stack2: UIStackView!
    var stack3: UIStackView!
    var currentSpacing = CGFloat(0)
    let widthButton = CGFloat(50)
}
