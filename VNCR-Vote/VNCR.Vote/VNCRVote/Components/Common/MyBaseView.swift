//
//  MyBaseView.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 5/21/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import UIKit

class MyBaseView: UIView {
    
    // MARK: -  override
    func setupTexts() {
        // override
    }
    
    func config() {
        setupTexts()
    }
    
    // MARK: -  private
    private func loadNIb() {
        Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNIb()
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNIb()
        config()
    }
    
    // MARK: -  properties
    
    // MARK: - outlet
    @IBOutlet weak var view: UIView!
}
