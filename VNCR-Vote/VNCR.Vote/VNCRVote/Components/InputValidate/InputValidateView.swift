//
//  InputValidateView.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 9/29/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

enum InputValidateViewType {
    case normal
    case password
}

class InputValidateView: UIView {

    
    // MARK: -  function
    func setError(isError:Bool) {
        borderBottom.backgroundColor = !isError ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        //title.textColor = !isError ? #colorLiteral(red: 0.4352941176, green: 0.4431372549, blue: 0.4745098039, alpha: 1) : #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
    }
    
    func setup(type: InputValidateViewType, title:String, placeholder:String) {
        inputType = type
        //self.title.text = title
        //textField.placeholder = placeholder
        textField.attributedPlaceholder = NSAttributedString(string: placeholder,
                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }
    
    func getValue() -> String {
        return textField.text ?? ""
    }
    
    func setValue(text:String, keepError:Bool) {
        textField.text = text
        setError(isError: keepError)
    }
    
    func setColor(color: UIColor) {
        textField.textColor = color
    }
    
    // MARK: -  private
    
    
    private func config() {
        // set up default color, text, placeholder v.vv
        textField.font = UIFont.systemFont(ofSize: 16)
    }
    
    private func loadNIb() {
        Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
        addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        view.topAnchor.constraint(equalTo: topAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    }
    
    private func setUpControl() {
        if inputType == .normal {
            textField.isSecureTextEntry = false
        } else if inputType == .password {
            textField.isSecureTextEntry = true
            let rightButton  = UIButton(type: .custom)
            rightButton.setImage(#imageLiteral(resourceName: "ic_eye").resizeImageWith(newSize: CGSize(width: 30, height: 30)).tint(with: #colorLiteral(red: 0.9360817075, green: 0.4546757936, blue: 0, alpha: 1)), for: .normal)
            rightButton.setImage(#imageLiteral(resourceName: "ic_eye_hidden").resizeImageWith(newSize: CGSize(width: 30, height: 30)).tint(with: #colorLiteral(red: 0.9360817075, green: 0.4546757936, blue: 0, alpha: 1)), for: .selected)
            rightButton.frame = CGRect(x:0, y:0, width:30, height:30)
            rightButton.addTarget(self, action: #selector(self.didTapRightButton), for: .touchUpInside)
            textField.rightViewMode = .always
            textField.rightView = rightButton
        }
    }
    
    @objc func didTapRightButton(_ sender: UIButton) {
        textField.isSecureTextEntry = !textField.isSecureTextEntry
        sender.isSelected = !textField.isSecureTextEntry
    }
    
    // MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        loadNIb()
        config()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNIb()
        config()
    }
    
    // MARK: -  properties
    var inputType:InputValidateViewType = .normal {
        didSet {
            setUpControl()
        }
    }
    
    // MARK: - outlet
    @IBOutlet weak var view: UIView!
    @IBOutlet weak var borderBottom: UIView!
    //@IBOutlet weak var title: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    
}
