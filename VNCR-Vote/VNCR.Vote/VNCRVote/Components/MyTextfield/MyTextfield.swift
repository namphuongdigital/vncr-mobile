//
//  MyTextfield.swift
//  GlobeDr
//
//  Created by Nhan Ba Doan on 5/21/19.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

enum MyTextfieldType {
    case normal
    case password
    case date
}

class MyTextfield: MyBaseView {

    // MARK: -  function
    func setFirstResponse() {
        txtInput.becomeFirstResponder()
    }
    
    func setTypeReturnKey(type:UIReturnKeyType) {
        txtInput.returnKeyType = type
    }
    
    func setDisabled() {
        txtInput.isEnabled = false
    }
    
    func getValue() -> String? {
        return txtInput.text
    }
    
    func getUTCDateString() ->String? {
        if var _ = txtInput.text {
            return selectedDate.toString(dateFormat: "yyyy-MM-dd'T'HH:mm:ss.SSS")
        }
        return nil
    }
    
    func setValue(value:String) {
        txtInput.text = value
    }
    
    func setup(textHolder:String?, title:String, errorString:String, type:MyTextfieldType = .normal) {
        placeHolder = textHolder
        lblError.text = errorString
        lblTitle.text = title
        self.type = type
        setup(type: type)
    }
    
    func showError(_ message:String?) {
//        UIView.transition(with: lblError, duration: 0.24, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.lblError.isHidden = message == nil
//        }, completion: nil)
        lblError.text = message
    }
    
    func setTitleColor(color:UIColor) {
        lblTitle.textColor = color
    }
    
    func setCornerRadius(radius:CGFloat) {
        cornerRadius = radius
        self.layoutSubviews()
    }
    
    // MARK: -  event
    @IBAction func tapButtonShow(_ sender: UIButton) {
        if type == .password {
            txtInput.isSecureTextEntry = !txtInput.isSecureTextEntry
        } else if type == .date {
            txtInput.becomeFirstResponder()
        }
    }
    
    @objc func textfieldDidChange(_ sender:UITextField) {
        onTyping?(sender)
    }
    
    // MARK: -  overridde
    override func config() {
        super.config()
        
//        if isOnlyCornerRadius {
//            vwCornerRadius.makeCorner(radius: 10)
//        } else {
//            vwCornerRadius.makeShadowAndCorner()
//        }
        
        lblError.isHidden = true
        lblError.font = UIFont.systemFont(ofSize: 10, weight: UIFont.Weight.regular)
        lblError.textColor = UIColor.red
        
        lblTitle.font = UIFont.systemFont(ofSize: 15, weight: UIFont.Weight.medium)
        lblTitle.textColor = #colorLiteral(red: 0.1960784314, green: 0.2274509804, blue: 0.2705882353, alpha: 1)
        
        txtInput.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        txtInput.font = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.regular)
        txtInput.attributedPlaceholder = NSAttributedString(string: "", attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.6901960784, green: 0.6901960784, blue: 0.6901960784, alpha: 1)])
        
        txtInput.delegate = self
        
        txtInput.addTarget(self, action: #selector(textfieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }

    override func setupTexts() {
        // setup text here
        lblError.text = lblError.text?.localized()
        lblTitle.text = lblTitle.text?.localized()
        
        if let pl = txtInput.placeholder {
            txtInput.attributedPlaceholder = NSAttributedString(string: pl, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.6901960784, green: 0.6901960784, blue: 0.6901960784, alpha: 1)])
        }
    }
    
    // MARK: -  private
    private func setup(type:MyTextfieldType) {
        switch type {
        case .normal:
            if btnShowText != nil{
                btnShowText.removeFromSuperview()}
            txtInput.isSecureTextEntry = false
        case .password:
            txtInput.isSecureTextEntry = true
        case .date:
            if btnShowText != nil {
                btnShowText.setImage(#imageLiteral(resourceName: "ic_calendar"), for: UIControl.State())
            }
            txtInput.isSecureTextEntry = false
            txtInput.tintColor = UIColor.clear
            showDatePicker()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
//        if cornerRadius > 0 {
//            vwCornerRadius.makeCorner(radius: cornerRadius)
//        }
    }
    
    // MARK: -  clourse
    var onSelectDate:((Date,String)->Void)?
    
    // MARK: -  properties
    var type:MyTextfieldType = .normal
    var isOnlyCornerRadius: Bool = false
    
    var placeHolder:String? = "" {
        didSet {
            if let pl = placeHolder {
                txtInput.attributedPlaceholder = NSAttributedString(string: pl, attributes: [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.6901960784, green: 0.6901960784, blue: 0.6901960784, alpha: 1)])
            }
        }
    }
    
    var isActiveResponse:Bool = false
    fileprivate let datePicker = UIDatePicker()
    var selectedDate:Date = Date()
    var cornerRadius:CGFloat = 0
    
    // MARK: -  closure
    var onTyping:((UITextField)->Void)?
    var onTapReturn:((UITextField)->Void)?
    
    // MARK: -  oulet
    @IBOutlet weak var vwCornerRadius: UIView!
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var btnShowText: UIButton!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
}

extension MyTextfield: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isActiveResponse = true
        if type == .date {
            if var text = txtInput.text {
                text = "\(text)T12:00:00.000"
                if let date = text.replacingOccurrences(of: "/", with: "-").toDate1("dd-MM-yyyy'T'HH:mm:ss.SSS") {
                    datePicker.setDate(date, animated: true)
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        isActiveResponse = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        onTapReturn?(textField)
        return true
    }
}

extension MyTextfield {
    private func showDatePicker(){
        
        //Formate Date
        datePicker.datePickerMode = .date
        let calendar = Calendar(identifier: .gregorian)
        
        var comps = DateComponents()
        comps.year = 20
        let maxDate = calendar.date(byAdding: comps, to: Date())
        comps.year = -105
        let minDate = calendar.date(byAdding: comps, to: Date())
        datePicker.maximumDate = maxDate
        datePicker.minimumDate = minDate
        datePicker.setDate(Date(), animated: true)
        datePicker.locale = NSLocale.init(localeIdentifier: AppHelper.shared.LANGUAGE_OF_DEVICE) as Locale
        
        //ToolBar
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "done".localized().capitalized, style: .plain, target: self, action: #selector(donedatePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "cancel".localized().capitalized, style: .plain, target: self, action: #selector(cancelDatePicker))
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtInput.inputAccessoryView = toolbar
        txtInput.inputView = datePicker
    }
    
    @objc func donedatePicker(){
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        let dateString = formatter.string(from: datePicker.date)
        setValue(value: dateString)
        selectedDate = datePicker.date
        onSelectDate?(datePicker.date,dateString)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
}
