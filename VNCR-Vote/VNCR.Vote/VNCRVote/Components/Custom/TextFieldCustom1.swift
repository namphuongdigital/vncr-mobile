//
//  TextFieldCustom1.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 10/1/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit
import QuartzCore

class TextFieldCustom1: UITextField {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureUI()
    }
    
    private func configureUI() {
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 0.5
        self.layer.cornerRadius = 4.0
    }
}
