//
//  ButtonTemplate.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 9/30/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

@available(iOS 10.0, *)
let kLoginButtonBackgroundColor = UIColor(displayP3Red: 31/255, green: 75/255, blue: 164/255, alpha: 1)
let kLoginButtonTintColor = UIColor.white
let kLoginButtonCornerRadius: CGFloat = 13.0

class ButtonTemplate: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureUI()
    }
    
    private func configureUI() {
        self.backgroundColor = #colorLiteral(red: 0.9360817075, green: 0.4546757936, blue: 0, alpha: 1) // #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6106679137)
        self.layer.cornerRadius = kLoginButtonCornerRadius
        self.tintColor = kLoginButtonTintColor
        self.titleLabel?.font = UIFont(name: "SourceSans-Regular", size: 14)
    }
    
    func initUI(_ backgroundColor: UIColor, _ textColor: UIColor) {
        self.backgroundColor = backgroundColor
        self.tintColor = textColor
    }
    
}
