//
//  VoteManager.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 07/05/2021.
//  Copyright © 2021 Nhan Ba Doan. All rights reserved.
//

import UIKit

class VoteManager: ServiceManager {

    static let shared = VoteManager()
    
    func getList(code: String, completion: @escaping (ResponseModel, VoteModel?) -> Void) {
        
        let api = "/vote/list"
        let postData = NSMutableData(data: ("Code=" + code).data(using: String.Encoding.utf8)!)
        
        taskSendRequest(api: api, postData: postData) { (task) in
            task.continueOnSuccessWith(continuation: { result in
                
                let response = self.parseResponseModel(result)
                var model: VoteModel?
                
                if let obj = self.getSingleDictionary(response) {
                    model = VoteModel.init(json: obj)
                }
                
                completion(response, model)
                
            })
        }
    }
    
    func doSubmit(data: VoteModel, completion: @escaping (ResponseModel, VoteModel?) -> Void) {
        
        let api = "/vote/submit"
        let postData = NSMutableData(data: ("Data=" + Common.shared.convertModelToJson(data)).data(using: String.Encoding.utf8)!)
        
        taskSendRequest(api: api, postData: postData) { (task) in
            task.continueOnSuccessWith(continuation: { result in
                
                let response = self.parseResponseModel(result)
                var model: VoteModel?
                
                if let obj = self.getSingleDictionary(response) {
                    model = VoteModel.init(json: obj)
                }
                
                completion(response, model)
                
            })
        }
    }
    
    func getMultiple(code: String, completion: @escaping (ResponseModel, [VoteModel]?) -> Void) {
        
        let api = "/vote/getMultiple"
        let postData = NSMutableData(data: ("Code=" + code).data(using: String.Encoding.utf8)!)
        
        taskSendRequest(api: api, postData: postData) { (task) in
            task.continueOnSuccessWith(continuation: { result in
                
                let response = self.parseResponseModel(result)
                var list: [VoteModel]?
                
                if let nest = self.getNestedDictionary(response) {
                    list = []
                    for obj in nest {
                        list?.append(VoteModel.init(json: obj))
                    }
                }
                
                completion(response, list)
                
            })
        }
    }
    
    func doSubmitMultiple(code: String, data: [VoteModel], completion: @escaping (ResponseModel, [VoteModel]?) -> Void) {
        
        let api = "/vote/submitMultiple"
        let postData = NSMutableData(data: ("Data=" + Common.shared.convertModelToJson(data).addingPercentEncoding(withAllowedCharacters: CharacterSet.rfc3986Unreserved)!).data(using: String.Encoding.utf8)!)
        postData.append(("&Code=" + code.addingPercentEncoding(withAllowedCharacters: CharacterSet.rfc3986Unreserved)!).data(using: String.Encoding.utf8)!)

        taskSendRequest(api: api, postData: postData) { (task) in
            task.continueOnSuccessWith(continuation: { result in
                
                let response = self.parseResponseModel(result)
                var list: [VoteModel]?
                
                if let nest = self.getNestedDictionary(response) {
                    list = []
                    for obj in nest {
                        list?.append(VoteModel.init(json: obj))
                    }
                }
                
                completion(response, list)
                
            })
        }
    }
    
    func getHelpDocument(completion: @escaping (ResponseModel, String) -> Void) {
        
        let api = "/vote/getHelpDocument"
        
        taskSendRequest(api: api, postData: nil) { (task) in
            task.continueOnSuccessWith(continuation: { result in
                
                let response = self.parseResponseModel(result)
                
                if let obj = self.getAnyDictionary(response) as? String {
                    completion(response, obj)
                    return
                }
                
                completion(response, "")
                
            })
        }
    }

    func getDocs(completion: @escaping (ResponseModel, [VoteDocModel]?) -> Void) {
        
        let api = "/vote/getdocs"
        
        taskSendRequest(api: api, postData: nil) { (task) in
            task.continueOnSuccessWith(continuation: { result in
                
                let response = self.parseResponseModel(result)
                var list: [VoteDocModel]?
                
                if let nest = self.getNestedDictionary(response) {
                    list = []
                    for obj in nest {
                        list?.append(VoteDocModel.init(json: obj))
                    }
                }
                
                completion(response, list)
                
            })
        }
    }

}
