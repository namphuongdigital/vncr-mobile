//
//  AppHelper.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 10/1/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications
import AVFoundation
import SKPhotoBrowser
import Toast_Swift
import MessageUI

class AppHelper: NSObject, MFMailComposeViewControllerDelegate {
    
    //Properties
    var LANGUAGE_OF_DEVICE: String!
    var APP_VERSION: String = ""
    var APP_BUILD: String = ""
    
    var APP_LATEST_VERSION: String = ""
    var APP_LATEST_BUILD: Int = 0
    var APP_RELEASE_NOTE: String = ""
    
    public var progressHUD: MBProgressHUD!

    // MARK: - API & INITIALIZE
    public var APP_DEFAULT_CALL_SERVER_ERROR: String = ""
    public var isServerTest = false
    
    static let shared : AppHelper = {
        
        let instance = AppHelper()
        
        instance.LANGUAGE_OF_DEVICE = Common.shared.getAppLanguage()
        Bundle.setLanguage(lang: instance.LANGUAGE_OF_DEVICE)
        instance.APP_DEFAULT_CALL_SERVER_ERROR = "Some errors occurred during calling to server.".localized()
        
        instance.APP_BUILD = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
        instance.APP_VERSION = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
        
        if UserDefaults.standard.value(forKey: "IS_SERVER_TEST") != nil {
            instance.isServerTest = UserDefaults.standard.value(forKey: "IS_SERVER_TEST") as! Bool
        } else {
            UserDefaults.standard.setValue(instance.isServerTest, forKey: "IS_SERVER_TEST")
        }
                
        return instance
        
    }()
    
    public var API_SERVER_IP: String {
        get {
            let url = "https://api.crew.vn/api"
//            let url = "https://dev.crew.vn/api"
//            if(UserDefaults.standard.value(forKey: "hostAPI") != nil) {
//                url = UserDefaults.standard.value(forKey: "hostAPI") as! String
//            }
            return url
        }
    }
    
    // MARK: FUNCTION
    
    func checkUserStatus() {
        let appSupportVC = UINavigationController(rootViewController: WelcomeController(nibName: "WelcomeController", bundle: Bundle.main))
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let window = appDelegate.window
        if let ww = window {
            ww.rootViewController = appSupportVC
            ww.makeKeyAndVisible()
            UIView.transition(with: ww, duration: 0.5, options: .transitionFlipFromRight, animations: { }, completion: nil)
        }
    }
    
    // ======================== MARK: BEGIN for Common ======================== //
    func setAppLanguage(_ language: String) {
        AppHelper.shared.LANGUAGE_OF_DEVICE = language
        //AppHelper.shared.API_REQUEST_HEADERS.updateValue(language, forKey: Common.shared.X_APP_LANGUAGE)
        UserDefaults.standard.setValue(language, forKey: Common.shared.MYAPP_APP_LANGUAGE)
        Bundle.setLanguage(lang: language)
    }
    
    func getRootViewController() -> UIViewController? {
        var topVC = UIApplication.shared.delegate?.window?!.rootViewController
        while((topVC!.presentedViewController) != nil) {
            topVC = topVC!.presentedViewController
        }
        return topVC
    }
    
    func getMainViewController() -> UIViewController? {
        //return menuController?.mainViewController
        return getRootViewController()
    }
    
    func backToPreviousViewController() {
        if let controller = AppHelper.shared.getMainViewController() as? UINavigationController? {
            controller?.popViewController(animated: true)
        }
    }
    
    func backToRootViewController(_ animated: Bool = false) {
        if let controller = AppHelper.shared.getMainViewController() as? UINavigationController? {
            controller?.popToRootViewController(animated: animated)
        }
    }
    
    func supportsMetadataObjectTypes(_ metadataTypes: [AVMetadataObject.ObjectType]? = nil) throws -> Bool {
        guard let captureDevice = AVCaptureDevice.default(for: .video) else {
            throw NSError(domain: "com.yannickloriot.error", code: -1001, userInfo: nil)
        }
        
        let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
        let output      = AVCaptureMetadataOutput()
        let session     = AVCaptureSession()
        
        session.addInput(deviceInput)
        session.addOutput(output)
        
        var metadataObjectTypes = metadataTypes
        
        if metadataObjectTypes == nil || metadataObjectTypes?.count == 0 {
            // Check the QRCode metadata object type by default
            metadataObjectTypes = [.qr]
        }
        
        for metadataObjectType in metadataObjectTypes! {
            if !output.availableMetadataObjectTypes.contains(where: { $0 == metadataObjectType }) {
                return false
            }
        }
        
        return true
    }
    
    func checkScanPermissions() -> Bool {
        do {
            return try supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            if #available(iOS 10.0, *) {
                                if UIApplication.shared.canOpenURL(settingsURL) {
                                    UIApplication.shared.open(settingsURL, options: [:], completionHandler: { (success) in
                                        print("Open url : \(success)")
                                    })
                                }
                            }
                            else {
                                if UIApplication.shared.canOpenURL(settingsURL) {
                                    UIApplication.shared.openURL(settingsURL)
                                }
                            }
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            AppHelper.shared.getRootViewController()?.present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func setStyleForApp() {
        let bgColor = Common.shared.APP_MAIN_COLOR
        let fgColor = Common.shared.APP_MAIN_TEXT_COLOR
        
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : fgColor]
        UINavigationBar.appearance().tintColor = fgColor
        UINavigationBar.appearance().barTintColor = bgColor
        UINavigationBar.appearance().isTranslucent = false
        
        UITabBar.appearance().isTranslucent = false
        UITabBar.appearance().tintColor = bgColor
        UITabBar.appearance().barTintColor = bgColor
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Common.shared.TAB_UNSELECTED_COLOR, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 10)], for: .normal)
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Common.shared.TAB_SELECTED_COLOR, NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 10)], for: .selected)
    }
    
    func setStypeForPop() {
        UINavigationBar.appearance().tintColor = Common.shared.APP_BLUE_COLOR
        if #available(iOS 13.0, *) {
            UINavigationBar.appearance().barTintColor = UIColor.systemBackground
        } else {
            UINavigationBar.appearance().barTintColor = UIColor.lightGray
        }
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : Common.shared.APP_BLUE_COLOR]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    func openTestFlightBeta() {
        if var url = URL(string: "itms-beta://") {
            url = URL(string: "https://beta.itunes.apple.com/v1/app/1479359111")!
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                        print("Open url : \(success)")
                    })
                }
                else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }
    
    func callPhoneTo(phoneNumber: String) {
        if phoneNumber.isEmpty {
            return
        }
        if let phoneCallURL: URL = URL(string:"tel://\(phoneNumber.replacingOccurrences(of: " ", with: ""))") {
            if #available(iOS 10.0, *) {
                if UIApplication.shared.canOpenURL(phoneCallURL) {
                    UIApplication.shared.open(phoneCallURL, options: [:], completionHandler: { (success) in
                        print("Open url : \(success)")
                    })
                }
            }
            else {
                if UIApplication.shared.canOpenURL(phoneCallURL) {
                    UIApplication.shared.openURL(phoneCallURL)
                }
            }
        }
    }
    
    func sendMailTo(email: String) {
        if email.isEmpty {
            return
        }
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setMessageBody("<p>\("Type text...".localized())</p>", isHTML: true)
            AppHelper.shared.getMainViewController()?.present(mail, animated: true)
        } else {
            AppHelper.shared.showAlert("Your phone not support send mail.".localized())
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    // MARK: - Alert View, Photo Browser, Progress Bars
    func showSuccess(completion: @escaping () -> Void) {
        AppHelper.shared.showAlert("DataSubmitSuccess".localized()) {
            completion()
        }
    }
    
    func showAlert(_ message: String) {
        DispatchQueue.main.async {
            AppHelper.shared.showAlert(Common.shared.APP_NAME, message)
        }
    }
    
    func showAlert(_ message: String, _ isBackPreviousView: Bool = false)  {
        AppHelper.shared.showAlert(message, isBackPreviousView) { }
    }
    
    func showAlert(_ message: String, _ isBackPreviousView: Bool = false, completion: @escaping () -> Void)  {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: Common.shared.APP_NAME, message: message, preferredStyle: UIAlertController.Style.alert)
            // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.cancel, handler: { action in
                if (isBackPreviousView) {
                    AppHelper.shared.backToPreviousViewController()
                }
                completion()
            }))
            // show the alert
            AppHelper.shared.getRootViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    func showAlert(_ title: String, _ message: String)  {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title.isEmpty ? Common.shared.APP_NAME : title, message: message, preferredStyle: UIAlertController.Style.alert)
            // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.cancel, handler: nil))
            // show the alert
            AppHelper.shared.getRootViewController()?.present(alert, animated: true, completion: nil)
        }
    }
    
    func showAlert(_ title: String, _ content: String, _ isAlignLeft: Bool = false, completion: @escaping () -> Void)  {
        DispatchQueue.main.async {
            let alertView = UIAlertController(title: title.isEmpty ? Common.shared.APP_NAME : title, message: content, preferredStyle: UIAlertController.Style.alert)
            
            if isAlignLeft {
                let paragraphStyle = NSMutableParagraphStyle()
                // Here is the key thing!
                paragraphStyle.alignment = .left
                
                let messageText = NSMutableAttributedString(
                    string: content,
                    attributes: [
                        NSAttributedString.Key.paragraphStyle: paragraphStyle,
                        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13, weight: .regular)
                    ]
                )
                alertView.setValue(messageText, forKey: "attributedMessage")
            }
            
            // add the actions (buttons)
            alertView.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.cancel, handler: { action in
                completion()
            }))
            // show the alert
            AppHelper.shared.getRootViewController()?.present(alertView, animated: true, completion: nil)
        }
    }
    
    func showActionAlert(_ title: String?, _ message: String?, _ sourceViewController: UIViewController? = nil, actions: [UIAlertAction]) {
        DispatchQueue.main.async {
            AppHelper.shared.hideMBProgressHUD()
            let alert = UIAlertController(title: (title ?? "").isEmpty ? Common.shared.APP_NAME : title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            // add the actions (buttons)
            if (actions.count == 0) {
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.cancel, handler: nil))
            } else {
                for action in actions {
                    alert.addAction(action)
                }
            }
            
            // show the alert
            var viewController: UIViewController? = sourceViewController
            if (viewController == nil) {
                viewController = AppHelper.shared.getRootViewController()
            }
            viewController?.present(alert, animated: true, completion: nil)
        }
    }
    
    func showActionAlert(_ title: String, _ message: String, buttons: [String], tapBlock:((UIAlertAction,Int) -> Void)?) {
        DispatchQueue.main.async {
            AppHelper.shared.hideMBProgressHUD()
            _ = EZAlertController.alert(title, message: message, buttons: buttons, tapBlock: tapBlock)
        }
    }
    
    func showActionSheet(_ message: String?, _ sourceView: UIView? = nil, actions:[UIAlertAction]) {
        AppHelper.shared.showActionSheet("", message, sourceView, actions: actions)
    }
    
    func showActionSheet(_ title: String?, _ message: String?, _ sourceView: UIView? = nil, actions:[UIAlertAction]) {
        DispatchQueue.main.async {
            if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
                self.showActionAlert((title ?? "").isEmpty ? Common.shared.APP_NAME : title, message, nil, actions: actions)
            } else {
                let _ = EZAlertController.actionSheet((title ?? "").isEmpty ? Common.shared.APP_NAME : title!, message: message ?? "", sourceView: AppHelper.shared.getRootViewController()!.view, actions: actions)
            }
        }
    }
    
    func showActionSheet(_ title:String, message:String, buttons:[String], sourceView:UIView? = nil, tapBlock:((UIAlertAction,Int) -> Void)?) {
        DispatchQueue.main.async {
            AppHelper.shared.hideMBProgressHUD()
            if (UIScreen.main.traitCollection.userInterfaceIdiom == .pad) {
                self.showActionAlert(title, message, buttons: buttons, tapBlock: tapBlock)
            } else {
                EZAlertController.actionSheet(title, message: message, sourceView: sourceView!, buttons: buttons, tapBlock: tapBlock)
            }
        }
    }
    
    func nomalizeFromTimeAndToTime(_ fromTime: Date, _ toTime: Date?) -> String {
        if toTime == nil {
            return "\(fromTime.toString(dateFormat: "longdateformat".localized())) - ?"
        }
        
        let result = Calendar.current.compare(fromTime, to: toTime!, toGranularity: .day)
        if (result == ComparisonResult.orderedSame) {
            return "\(fromTime.toString(dateFormat: "longdateformat".localized())) - \(toTime!.toString(dateFormat: "shorttimeformat".localized()))"
        }
        
        return "\(fromTime.toString(dateFormat: "longdateformat".localized())) - \(toTime!.toString(dateFormat: "longdateformat".localized()))"
    }
    
    func viewPhotoInBrowserFullscreen(_ title: String, _ url: String) {
        let data = PhotoBrowserModel.init()
        data.title = title
        data.url = url
        data.isSelected = true
        AppHelper.shared.viewPhotoInBrowserFullscreen([data])
    }
    
    func viewPhotoInBrowserFullscreen(_ datas: [String: String], _ pageIndex: Int = 0) -> Void {
        var listPhoto = Array<SKPhotoProtocol>()
        for case let item in datas {
            let url: String = item.value
            let name: String = item.key
            if !url.isEmpty && Common.shared.isImageURL(url) {
                let photo = SKPhoto.photoWithImageURL(url)
                photo.caption = name
                listPhoto.append(photo)
            }
        }
        if listPhoto.count > 0 {
            let browser = SKPhotoBrowser(photos: listPhoto)
            browser.initializePageIndex(pageIndex)
            //browser.delegate = self
            AppHelper.shared.getRootViewController()?.present(browser, animated: true, completion: nil)
        }
    }
    
    func viewPhotoInBrowserFullscreen(_ datas: [PhotoBrowserModel]?) -> Void {
        if (datas?.count ?? 0 > 0) {
            var i: Int = 0
            var selectedPageIndex: Int = 0
            var listPhoto = Array<SKPhotoProtocol>()
            for case let item in datas! {
                let name: String = item.title
                let url: String = item.url
                if !url.isEmpty { //&& Common.shared.isImageURL(url) {
                    let photo = SKPhoto.photoWithImageURL(url) //, holder: #imageLiteral(resourceName: "no_image_available")
                    photo.caption = name
                    listPhoto.append(photo)
                    if (item.isSelected) {
                        selectedPageIndex = i
                    }
                    i += 1
                }
            }
            if listPhoto.count > 0 {
                let browser = SKPhotoBrowser(photos: listPhoto)
                browser.initializePageIndex(selectedPageIndex)
                AppHelper.shared.getRootViewController()?.present(browser, animated: true, completion: nil)
            }
        }
    }
    
    func viewPhotoInBrowserFullscreen(image: UIImage, name: String) -> Void {
        var listPhoto = Array<SKPhotoProtocol>()
        let photo = SKPhoto.photoWithImage(image)
        photo.caption = name
        listPhoto.append(photo)
        if listPhoto.count > 0 {
            let browser = SKPhotoBrowser(photos: listPhoto)
            browser.initializePageIndex(0)
            AppHelper.shared.getRootViewController()?.present(browser, animated: true, completion: nil)
        }
    }
    
    private func loadProgressBarHUD() {
        if(AppHelper.shared.getRootViewController()?.navigationController != nil){
            self.progressHUD = MBProgressHUD.showAdded(to: AppHelper.shared.getRootViewController()!.navigationController!.view, animated: true)
        } else {
            self.progressHUD = MBProgressHUD.showAdded(to: AppHelper.shared.getRootViewController()!.view, animated: true)
        }
        self.progressHUD.removeFromSuperViewOnHide = false
        self.progressHUD.hide(animated: false)
    }
    
    func changeMsgMBProgressHUD(_ message: String) {
        DispatchQueue.main.async {
            if self.progressHUD != nil {
                self.progressHUD.label.text = message
            }
        }
    }

    func showMBProgressHUD(_ message: String = "Please wait...".localized(), _ animated: Bool = true) {
        DispatchQueue.main.async {
            if self.progressHUD != nil {
                self.progressHUD.hide(animated: animated)
            }
            self.loadProgressBarHUD()
            self.progressHUD.label.text = message
            self.progressHUD.show(animated: animated)
        }
    }
    
    func hideMBProgressHUD(_ animated: Bool = true) {
        DispatchQueue.main.async {
            if self.progressHUD != nil {
                self.progressHUD.hide(animated: animated)
            }
        }
    }
    
    func loadImageUsingCacheWithURLString(_ URLString: String, size:CGSize? = nil, placeHolder: UIImage = #imageLiteral(resourceName: "no_image_available"), _ loadCached: Bool = true, color: UIColor? = nil, _ onComplete:((UIImage?)->Void)? = nil){
        
        var newImage: UIImage = placeHolder
        
        if let cachedImage = imageCache.object(forKey: NSString(string: URLString)) {
            if loadCached {
                if (color == nil) {
                    newImage = cachedImage
                } else {
                    newImage = cachedImage.tint(with: color!)
                }
                onComplete?(newImage)
                return
            } else {
                imageCache.removeObject(forKey: NSString(string: URLString))
            }
        }
        
        if let url = URL(string: URLString) {
            
            var request = NSMutableURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Common.shared.TIMEOUT_INTERVAL)
            if (!loadCached) {
                request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: Common.shared.TIMEOUT_INTERVAL)
            }
            
            URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) in
                
                if error != nil {
                    if let er = error {
                        print("ERROR LOADING IMAGES FROM URL: \(er)")
                    }
                    return
                }
                if let data = data {
                    if let downloadedImage = UIImage(data: data) {
                        
                        if let s = size {
                            let imgScale = downloadedImage.resizeImageWith(newSize: s)
                            if let imageData = imgScale.jpegData(compressionQuality: 0.7) {
                                if let img = UIImage(data: imageData) {
                                    imageCache.setObject(img, forKey: NSString(string: URLString))
                                    DispatchQueue.main.async {
                                        //self.image = img
                                        if (color == nil) {
                                            newImage = img
                                        } else {
                                            newImage = img.tint(with: color!)
                                        }
                                        onComplete?(newImage)
                                    }
                                }
                            }
                        } else {
                            imageCache.setObject(downloadedImage, forKey: NSString(string: URLString))
                            DispatchQueue.main.async {
                                if (color == nil) {
                                    newImage = downloadedImage
                                } else {
                                    newImage = downloadedImage.tint(with: color!)
                                }
                                onComplete?(newImage)
                            }
                        }
                    }
                }
            }).resume()
        } else {
            onComplete?(placeHolder)
        }
    }
    
    func showToast(_ message : String,
                   _ autoHide: Bool = true,
                   _ duration: TimeInterval = 3.0,
                   isTapToDismissEnabled: Bool = true,
                   position: ToastPosition = .center,
                   view: UIView? = nil) {
        DispatchQueue.main.async {
            ToastManager.shared.isTapToDismissEnabled = isTapToDismissEnabled
            ToastManager.shared.isQueueEnabled = true
            let selfView: UIView = view == nil ? AppHelper.shared.getRootViewController()!.view : view!
            selfView.hideAllToasts()
            AppHelper.shared.getRootViewController()!.view.hideAllToasts()
            if (autoHide) {
                selfView.makeToast(message, duration: duration, position: position) //, style: style
            } else {
                selfView.makeToast(message, duration: duration + 30.0, position: position) //, style: style
            }
        }
    }
    
    func hideToast(_ duration: TimeInterval = 1.5, view: UIView? = nil) {
        DispatchQueue.main.asyncAfter(deadline: .now() + duration, execute: {
            let selfView: UIView = view == nil ? AppHelper.shared.getRootViewController()!.view : view!
            selfView.hideAllToasts()
            AppHelper.shared.getRootViewController()!.view.hideAllToasts()
        })
    }
    
    func generateNotificationFeedback(_ style: UINotificationFeedbackGenerator.FeedbackType) {
        if #available(iOS 10.0, *) {
            let notificationFeedbackGenerator = UINotificationFeedbackGenerator()
            notificationFeedbackGenerator.prepare()
            notificationFeedbackGenerator.notificationOccurred(style)
        }
    }
    
    func generateImpactFeedback(_ style: UIImpactFeedbackGenerator.FeedbackStyle = UIImpactFeedbackGenerator.FeedbackStyle.medium) {
        if #available(iOS 10.0, *) {
            let impactFeedbackgenerator = UIImpactFeedbackGenerator(style: style)
            impactFeedbackgenerator.prepare()
            impactFeedbackgenerator.impactOccurred()
        }
    }
    
    func generateSelectionFeedback() {
        if #available(iOS 10.0, *) {
            let selectionFeedbackGenerator = UISelectionFeedbackGenerator()
            selectionFeedbackGenerator.selectionChanged()
        }
    }
    
    func playSuccess() {
        generateImpactFeedback(.medium)
        AudioServicesPlaySystemSound (1103);//1103//1016
    }
    
    func playError() {
        generateImpactFeedback(.heavy)
        //AudioServicesPlaySystemSound (1073);//1006//1073//1053
    }
    
    // ======================== MARK: END for Common ======================== //
    
}
