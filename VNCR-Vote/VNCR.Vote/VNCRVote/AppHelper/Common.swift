//
//  Common.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 3/17/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation

class Common: NSObject {
    
    static let shared : Common = {
        
        let instance = Common()
        
        return instance
        
    }()
    
    let APP_SELECT_COLOR = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    let APP_MAIN_COLOR = #colorLiteral(red: 0, green: 0.4375008345, blue: 0.5701826215, alpha: 1) //UIColor.init("#2F6180") //231F6E //22206E
    let APP_MAIN_TEXT_COLOR = UIColor.init("#FFFFFF") // #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    let APP_SECOND_TEXT_COLOR = UIColor.init("#EF7400") // #colorLiteral(red: 0.937254902, green: 0.4549019608, blue: 0, alpha: 1)
    let TAB_SELECTED_COLOR = #colorLiteral(red: 0.9882352941, green: 0.6274509804, blue: 0.0431372549, alpha: 1) // #colorLiteral(red: 0.9795131195, green: 0.6284217197, blue: 0, alpha: 1) //UIColor.init("#EF7400")
    let TAB_UNSELECTED_COLOR = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) //UIColor.init("#FFFFFF")
    let APP_BLUE_COLOR = #colorLiteral(red: 0.09803921569, green: 0.462745098, blue: 0.8235294118, alpha: 1)  //UIColor.init("#1976D2")//01579B
    let APP_GREEN_COLOR = #colorLiteral(red: 0.2196078431, green: 0.5568627451, blue: 0.2352941176, alpha: 1) //UIColor.init("#388E3C")//2ECC71
    let APP_ORANGE_COLOR = #colorLiteral(red: 0.9607843137, green: 0.4862745098, blue: 0, alpha: 1) //UIColor.init("#F57C00")//EF7400
    let APP_RED_COLOR = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1) //UIColor.init("#F57C00")//EF7400

    // Define Resource Key
    let APP_NAME =  Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as? String ?? "VNCR Vote"
    let TIMEOUT_INTERVAL = 60.0
    let X_API_ID = "X_API_ID"
    let X_API_KEY = "X_API_KEY"
    let X_API_VERSION = "X_API_VERSION"
    let X_APP_LANGUAGE = "X_APP_LANGUAGE"
    let X_REQUEST_UDID = "X_REQUEST_UDID"
    let X_REQUEST_OS_VERSION = "X_REQUEST_OS_VERSION"
    let X_REQUEST_PLATFORM = "X_REQUEST_PLATFORM"
    let X_REQUEST_DEVICE_MODEL = "X_REQUEST_DEVICE_MODEL"
    let X_REQUEST_DEVICE_NAME = "X_REQUEST_DEVICE_NAME"
    let X_MOBILE_LOCATION = "X_MOBILE_LOCATION"
    let X_MOBILE_SESSION_TOKEN = "X_MOBILE_SESSION_TOKEN"
    let X_MOBILE_PUSH_TOKEN = "X_MOBILE_PUSH_TOKEN"
    let X_APP_BUILD = "X_APP_BUILD"
    let X_APP_VERSION = "X_APP_VERSION"
    let X_APP_REQUEST_NAME = "X_APP_REQUEST_NAME"
    
    //API RETURN KEY
    let X_API_RETURN_KEY_HASHTABLE_DATA = "Data"
    
    let MYAPP_API_SERVER_IP = "MYAPP_API_SERVER_IP"
    let MYAPP_APP_LANGUAGE = "MYAPP_LANGUAGE"
    let MYAPP_USER_REFRESH_TOKEN = "MYAPP_USER_REFRESH_TOKEN"
    let MYAPP_USER_SIGNED_USERNAME = "MYAPP_USER_SIGNED_USERNAME"
    let MYAPP_USER_SIGNED_USERNAME_BIOMETRIC = "MYAPP_USER_SIGNED_USERNAME_BIOMETRIC"
    let MYAPP_ACCOUNTING_HAS_PROMPT_BIOMETRIC = "MYAPP_ACCOUNTING_PROMPT_BIOMETRIC"
    let MYAPP_ACCOUNTING_USER_ID_BIOMETRIC = "MYAPP_ACCOUNTING_USER_ID_BIOMETRIC"
    
    // Define Resource Key
    public var CallServerFailedCode: Int = -9999
    
    public enum ReturnCode: Int
    {
        case Success = 1000,

        DataInputInvalid = 1002,
        DataInputIsRequire = 1003,
        DataNotFound = 1004,
        
        OldVersionAPI = 2001,
        DeviceUUIDNotFound = 2002,
        
        AccountingPasswordInvalid = 3001,
        
        SessionTokenExpired = 9000,
        RefreshTokenExpired = 9009,
        
        RequiredUpdateNewVersion = 9992,
        ServiceIsUpdating = 9993,
        AccessDenied = 9994,
        Unauthorized = 9995,
        BadRequest = 9996,
        RequestInvalid = 9997,
        Unexpected = 9998,
        Error = 9999,
        
        //For Attendance Logs
        LateIn = 1,
        EarlyOut = 2,
        GoBackToWorkEarly = 3
    }
    
    public enum RequestType: Int, CustomStringConvertible {
        case All = 0,
        Absent = 1,
        Overtime = 2,
        Remote = 3
        
        var description: String {
            switch self {
                case .All: return "All"
                case .Absent: return "Absent"
                case .Overtime: return "Overtime"
                case .Remote: return "Remote"
            }
        }
    }
    
    public enum AttendanceMode: Int, CustomStringConvertible {
        case None = 0,
        In = 1,
        Out = 2
        
        var description: String {
            switch self {
                case .None: return "None"
                case .In: return "In"
                case .Out: return "Out"
            }
        }
    }
    
    public enum NotificationStatus: Int, CustomStringConvertible {
        case New = 0,
        Delivered = 1,
        Read = 2,
        Deleted = 3
        
        var description: String {
            switch self {
            case .New: return "New"
            case .Delivered: return "Delivered"
            case .Read: return "Read"
            case .Deleted: return "Deleted"
            }
        }
    }
    
    public enum NotificationActionType: Int, CustomStringConvertible {
        case None = 0,
        Attendance = 1,
        Notification = 2,
        Request = 3,
        Payslip = 4,
        RequestManagement = 5
        
        var description: String {
            switch self {
            case .None: return "None"
            case .Attendance: return "Payslip"
            case .Notification: return "Notification"
            case .Request: return "Request"
            case .Payslip: return "Payslip"
            case .RequestManagement: return "RequestManagement"
            }
        }
    }
    
    public enum PayslipRequestType: Int, CustomStringConvertible {
        case All = 0,
        Salary = 1,
        Timesheet = 2,
        ProductionBonus = 3,
        Warning = 4
        
        var description: String {
            switch self {
            case .All: return "All"
            case .Salary: return "Salary"
            case .Timesheet: return "Timesheet"
            case .ProductionBonus: return "ProductionBonus"
            case .Warning: return "Warning"
            }
        }
    }
    
    public enum EmergencyType: Int, CustomStringConvertible {
        case Fire = 1,
        BombThreat = 2,
        LostStone = 3,
        Intruder = 4
        
        var description: String {
            switch self {
            case .Fire: return "Fire"
            case .BombThreat: return "BombThreat"
            case .LostStone: return "LostStone"
            case .Intruder: return "Intruder"
            }
        }
    }
    
    lazy var currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "en_US")
        formatter.currencyCode = "" //USD
        formatter.currencySymbol = "" //
        formatter.maximumFractionDigits = 2
        formatter.minimumFractionDigits = 2
        return formatter
    }()
    
    func getCurrencyFormatter(_ currencyCode: String = "") -> NumberFormatter {
        let formatter = currencyFormatter
        formatter.currencyCode = currencyCode
        return currencyFormatter
    }
        
    func getAppLanguage() -> String {
        return "vi"
        
//        var language = ""
//        if UserDefaults.standard.value(forKey: Common.shared.MYAPP_APP_LANGUAGE) != nil {
//            language = UserDefaults.standard.value(forKey: Common.shared.MYAPP_APP_LANGUAGE) as! String
//        }
//        
//        if (language.isEmpty) {
//            if let substring = Locale.preferredLanguages.first?.prefix(2) {
//                language = String(substring).lowercased()
//                UserDefaults.standard.setValue(language, forKey: Common.shared.MYAPP_APP_LANGUAGE)
//            }
//        }
//        language = language == "vi" ? "vi" : "en"
//        return language
    }
    
    func isImageURL(_ imgURL: String) -> Bool {
        if imgURL.starts(with: "http://") {
            let imageExtensions = ["png", "jpg", "gif", "jpeg"]
            let url: URL? = NSURL(fileURLWithPath: imgURL) as URL
            let pathExtention = url?.pathExtension
            if imageExtensions.contains(pathExtention!.lowercased()) {
                return true
            }
        }
        return false
    }
    
    func getTypeIconImageFromURL(_ imgURL: String) -> UIImage {
        if imgURL.starts(with: "http://") {
            let url: URL? = NSURL(fileURLWithPath: imgURL) as URL
            let pathExtention = url?.pathExtension
            if ["png", "jpg", "gif", "jpeg"].contains(pathExtention!.lowercased()) {
                return UIImage.init(named: "ic_filetype_image")!
            } else if ["pdf"].contains(pathExtention!) {
                return UIImage.init(named: "ic_filetype_pdf")!
            } else if ["zip", "rar"].contains(pathExtention!) {
                return UIImage.init(named: "ic_filetype_zip")!
            } else if ["xml"].contains(pathExtention!) {
                return UIImage.init(named: "ic_filetype_xml")!
            } else if ["txt"].contains(pathExtention!) {
                return UIImage.init(named: "ic_filetype_text")!
            }
        }
        return UIImage.init(named: "ic_filetype_unknown")!
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String, filename: String, mimetype: String) -> NSData {
        let body = NSMutableData()
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    //open func encode<T>(_ value: T) throws -> Data where T : Encodable
    func convertModelToJson<T>(_ model: T) -> String where T : Encodable {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted // if necessary
        let payload = try! encoder.encode(model)
        let jsonStringPayload = String(data: payload, encoding: .utf8)!
        print(jsonStringPayload)
        return jsonStringPayload
    }
    
    func convertJsonToIntArray(text: String) -> [Int]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [Int]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func nomalizeFromTimeAndToTime(_ fromTime: Date, _ toTime: Date?) -> String {
        if toTime == nil {
            return "\(fromTime.toString(dateFormat: "longdateformat".localized())) - ?"
        }
        
        let result = Calendar.current.compare(fromTime, to: toTime!, toGranularity: .day)
        if (result == ComparisonResult.orderedSame) {
            return "\(fromTime.toString(dateFormat: "longdateformat".localized())) - \(toTime!.toString(dateFormat: "shorttimeformat".localized()))"
        }
        
        return "\(fromTime.toString(dateFormat: "longdateformat".localized())) - \(toTime!.toString(dateFormat: "longdateformat".localized()))"
    }
    
}
