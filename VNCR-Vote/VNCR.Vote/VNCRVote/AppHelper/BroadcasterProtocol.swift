//
//  BroadcasterProtocol.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 5/26/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation

protocol PushNotifyDelegate: NSObjectProtocol {
    func notifyReceiver()
}

protocol QRCodeScannerDelegate: NSObjectProtocol {
    func receiveCode(_ code: String)
}
