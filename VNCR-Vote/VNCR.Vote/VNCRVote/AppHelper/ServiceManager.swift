//
//  ServiceManager.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 05/05/2021.
//  Copyright © 2021 Nhan Ba Doan. All rights reserved.
//

import Foundation
import BoltsSwift
import EZAlertController

class ServiceManager: NSObject {
    
    var dicErrorCount: [String:Int] = [:]
    
    var API_REQUEST_HEADERS : [String : String] {
        get {
            
            //let playerIdForPushToken = "0000000000000000000000000000"
            let loginToken = "0000000000000000000000000000"
            let deviceUUID = UIDevice.current.uuidForKeychain() ?? UIDevice.current.identifierForVendor?.uuidString ?? ""
            let appBuild = Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? ""
//            let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? ""
            
            let requestHeaders = ["Content-Type": "application/x-www-form-urlencoded",
                                  "X_API_ID": "VN_CREW_2017",
                                  "X_API_KEY": "KE4Sc6zqaaHHlpkzStfdpwcmnkvposK6",
                                  "X_REQUEST_API_VERSION": "2.0",
                                  "X_APP_LANGUAGE": "en-US",
                                  "X_TOKEN": loginToken,
                                  "X_REQUEST_UDID": deviceUUID,
                                  "X_REQUEST_OS_VERSION": UIDevice.current.systemVersion,
                                  "X_REQUEST_PLATFORM": "iOS",
                                  "X_LOCATION": "0,0",
                                  "X_REQUEST_DEVICE_TYPE": DeviceTypes.deviceModelName() ?? UIDevice.current.model as String,
                                  "X_REQUEST_DEVICE_NAME": UIDevice.current.name,
                                  "X_REQUEST_DESCRIPTION": "",
                                  "Build": appBuild]
            
            return requestHeaders
            
        }
    }
    
    func taskSendRequest(api: String, postData: NSMutableData?, _ contentType: String = "", completion: @escaping ((Task<AnyObject>) -> Void)) {
        self.callToApi(api, postData, contentType) { (resTaskCompletionSource) in
            completion(resTaskCompletionSource.task)
        }
    }
    
    func callToApi(_ api: String, _ postData: NSMutableData?, _ contentType: String, completion: @escaping ((TaskCompletionSource<AnyObject>) -> Void)) {
        
        var headers = API_REQUEST_HEADERS
        if (!contentType.isEmpty) {
            headers.updateValue(contentType, forKey: "Content-Type")
        }
        
        let url = AppHelper.shared.API_SERVER_IP + api
        print("=============================")
        print("=====API_REQUEST_URL=====")
        print(url)
        print("=====API_REQUEST_HEADERS=====")
        print(headers)
        print("=====API_REQUEST_DATA=====")
        print(postData as Any)
        print("=============================")

        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Common.shared.TIMEOUT_INTERVAL)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        if (postData != nil) {
            request.httpBody = postData! as Data
        }
        
        let session = URLSession.shared
        session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            let taskCompletionSource = TaskCompletionSource<AnyObject>()
            var response = ResponseModel.init()
            
            guard let data = data, error == nil else {
                
                response.RetMessage = error?.localizedDescription ?? "Error"
                response.RetCode = Common.shared.CallServerFailedCode
                
                let countError = self.getValueErrorCount(api) ?? 0
                
                self.setValueErrorCount(api, countError+1)
                
                if countError <= 3 {
                    self.callToApi(api, postData, contentType) { (resTaskCompletionSource) in
                        completion(resTaskCompletionSource)
                    }
                    taskCompletionSource.trySet(result: response as AnyObject)
                    taskCompletionSource.tryCancel()
                    return
                }
                
                DispatchQueue.main.async {
                    EZAlertController.alert(Common.shared.APP_NAME, message: "\(response.RetMessage) Please check your network & try again!", buttons: ["Try again"]) { (action, position) in
                        if position == 0 {
                        }
                        self.callToApi(api, postData, contentType) { (resTaskCompletionSource) in
                            completion(resTaskCompletionSource)
                        }
                    }
                }
                
                taskCompletionSource.trySet(result: response as AnyObject)
                taskCompletionSource.tryCancel()
                //completion(taskCompletionSource)
                
                return
            }
            
            do {
                
                self.setValueErrorCount(api, 0)
                
                let jsonResponse = try JSONSerialization.jsonObject(with: data, options: [])
                if let dicResponse = jsonResponse as? [String: Any] {
                    
                    response.RetCode = dicResponse["ErrorCode"] as! Int? ?? -1
                    response.RetStatus = response.RetCode == 0
                    response.RetMessage = dicResponse["Message"] as! String? ?? ""
                    response.TotalRecord = dicResponse["TotalRecord"] as! Int? ?? 0
                    response.RetData = dicResponse[Common.shared.X_API_RETURN_KEY_HASHTABLE_DATA]
                    print(response)
                    
                    if (response.RetStatus) {
                        taskCompletionSource.trySet(result: response as AnyObject)
                        completion(taskCompletionSource)
                        return
                    } else if response.RetMessage.isEmpty {
                        response.RetMessage = "Some errors occurred during calling to server."
                        DispatchQueue.main.async {
                            AppHelper.shared.showAlert("Some errors occurred during calling to server.")
                        }
                    } else {
                        DispatchQueue.main.async {
                            AppHelper.shared.showAlert(response.RetMessage)
                        }
                    }
                    
                    taskCompletionSource.trySet(result: response as AnyObject)
                    completion(taskCompletionSource)
                    return
                }
                
            } catch let error {
                print("Error", error)
                response.RetMessage = "Some errors occurred during calling to server."
                DispatchQueue.main.async {
                    AppHelper.shared.showAlert("Some errors occurred during calling to server.")
                }
                taskCompletionSource.trySet(result: response as AnyObject)
                completion(taskCompletionSource)
                //taskCompletionSource.trySet(error: error)
                //taskCompletionSource.tryCancel()
            }
            
            completion(taskCompletionSource)
            
        }).resume()
        
    }
    
    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: Data, boundary: String, filename: String, mimetype: String) -> NSMutableData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func createFileKeyAndName(filePathKey: String?, filename: String) -> String {
        return "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n"
    }
    
    func createBodyWithParametersMultipleFiles(parameters: [String: String]?, boundary: String, mimetype: String, files: [String: Data]?) -> NSMutableData {
        let body = NSMutableData();
        
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
        
        body.appendString(string: "--\(boundary)\r\n")
        if let listFiles = files {
            for (key, value) in listFiles {
                body.appendString(string: key)
                body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
                body.append(value)
            }
        }
        
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")
        
        return body
    }
    
    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func createMultiPartForm(_ contentType: String) -> String {
        return "multipart/form-data; boundary=\(contentType)"
    }
    
    func parseResponseModel(_ result: AnyObject) -> ResponseModel {
        
        let response = result as! ResponseModel
        if (response.RetStatus) {
            print("Total record: \(response.TotalRecord)")
        }
        return response
        
    }
    
    func getNestedDictionary(_ response: ResponseModel) -> [[String: Any]]? {
        
        if response.RetStatus && response.RetData != nil {
            
            return response.RetData as? [[String: Any]]
            
        } else {
            
            return nil
        }
        
    }
    
    func getSingleDictionary(_ response: ResponseModel) -> [String: Any]? {
        
        if response.RetStatus && response.RetData != nil {
            
            return response.RetData as? [String: Any]
                        
        } else {
            
            return nil
        }
        
    }
    
    func getStringDictionary(_ response: ResponseModel) -> [String]? {
        
        if response.RetStatus && response.RetData != nil {
            
            return response.RetData as? [String]
            
        } else {
            
            return nil
        }
        
    }
    
    func getAnyDictionary(_ response: ResponseModel) -> Any? {
        
        if response.RetStatus && response.RetData != nil {
            
            return response.RetData
            
        } else {
            
            return nil
        }
        
    }
    
//    func showMessageTryAgainRefreshToken(_ errorMessage: String) {
//        DispatchQueue.main.async {
//            //No internet
//            let alertController = UIAlertController(title: Common.shared.APP_NAME, message:
//                "\(errorMessage)", preferredStyle: UIAlertController.Style.alert)
//            let action = UIAlertAction(title: "Try again", style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
//                AppHelper.shared.doAutoRefreshToken()
//            }
//            alertController.addAction(action)
//            AppHelper.shared.getRootViewController().present(alertController, animated: true, completion: nil)
//        }
//    }
    
    func getValueErrorCount(_ key: String) -> Int? {
        return self.dicErrorCount[key] as Int?
    }
    
    func setValueErrorCount(_ key: String, _ value: Int) {
        self.dicErrorCount[key] = value
    }
}
