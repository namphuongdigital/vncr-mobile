//
//  EncryptionHelper.swift
//  RydiamDemo
//
//  Created by Nhan Bá Đoàn on 6/4/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation
import SwiftyRSA

class EncryptionHelper: NSObject {
    
    static let shared : EncryptionHelper = {
        
        let instance = EncryptionHelper()
        
        return instance
        
    }()
    
    func generateKey() {
        
        var privateKey: PrivateKey?
        var publicKey: PublicKey?
        
        if #available(iOS 10.0, *) {
            guard let keyPair = try? SwiftyRSA.generateRSAKeyPair(sizeInBits: 2048) else {
                print("There was an error!")
                return
            }
            privateKey = keyPair.privateKey
            publicKey = keyPair.publicKey
        } else {
            // Fallback on earlier versions
            let keyToGenerate = String(Date().toUnixTimestamp())
            publicKey = try? PublicKey(pemEncoded: keyToGenerate)
            privateKey = try? PrivateKey(pemEncoded: keyToGenerate)
        }
        
        print("private key: \(String(describing: privateKey))")
        print("public key: \(String(describing: publicKey))")
    }
    
    func doEncryptData(plainText: String) -> String {
        
        guard let publicKey = try? PublicKey(pemNamed: "public") else {
            print("There was an error!")
            return ""
        }
        
        guard let clear = try? ClearMessage(string: plainText, using: .utf8) else {
            print("There was an error!")
            return ""
        }
        
        guard let encrypted = try? clear.encrypted(with: publicKey, padding: .PKCS1) else {
            print("There was an error!")
            return ""
        }
        
        let data = encrypted.data
        let base64String = encrypted.base64String
        
        print("encrypted data: \(data)")
        print("encrypted base64String: \(base64String)")
        
        return base64String
    }
    
    func doDecryptData(encryptedText: String) -> String {
        
        guard let privateKey = try? PrivateKey(pemNamed: "private") else {
            print("There was an error!")
            return ""
        }
        
        guard let encrypted = try? EncryptedMessage(base64Encoded: encryptedText) else {
            print("There was an error!")
            return ""
        }
        
        guard let clear = try? encrypted.decrypted(with: privateKey, padding: .PKCS1) else {
            print("There was an error!")
            return ""
        }
        
        let data = clear.data
        let base64String = clear.base64String
        guard let plainText = try? clear.string(encoding: .utf8) else {
            print("There was an error!")
            return ""
        }

        print("decrypted data: \(data)")
        print("decrypted base64String: \(base64String)")
        print("decrypted plainText: \(plainText)")
        
        return plainText
        
    }
    
}
