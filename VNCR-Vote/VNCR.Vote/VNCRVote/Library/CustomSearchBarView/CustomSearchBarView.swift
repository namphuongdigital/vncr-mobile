//
//  CustomSearchBarView.swift
//  
//
//  Created by Nhan Bá Đoàn on 11/20/19.
//

import UIKit

extension UISearchBar {
    func findTextfield()-> UITextField?{
        for view in self.subviews {
            if view is UITextField {
                return view as? UITextField
            } else {
                for textfield in view.subviews {
                    if textfield is UITextField {
                        return textfield as? UITextField
                    }
                }
            }
        }
        return nil;
    }
    
    func findCancelButton() -> UIButton? {
        for view in self.subviews {
            for subview in view.subviews {
                if let cancelButton = subview as? UIButton {
                    return cancelButton
                }
            }
        }
        return nil
    }
}


class CustomSearchBarView: UISearchBar, UITextFieldDelegate {
    
    var searchBarOriginX: CGFloat = 2.0
    
    final let SearchBarHeight: CGFloat = 44
    final let SearchBarPaddingTop: CGFloat = 2
    override open func awakeFromNib() {
        super.awakeFromNib()
        self.setupUI()
    }
    
    init(searchBarOriginX: CGFloat) {
        super.init(frame: CGRect.zero)
        self.searchBarOriginX = searchBarOriginX
        self.setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupUI(){
        if #available(iOS 11.0, *) {
            self.translatesAutoresizingMaskIntoConstraints = false
            self.heightAnchor.constraint(equalToConstant: SearchBarHeight).isActive = true
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if #available(iOS 11.0, *) {
            self.frame = CGRect(x: searchBarOriginX, y: self.frame.origin.y, width: UIScreen.main.bounds.size.width - searchBarOriginX, height: self.frame.height)
        }
        
        if let textfield = self.findTextfield() {
            textfield.placeholder = "Search...".localized()
            textfield.backgroundColor = UIColor.groupTableViewBackground
            textfield.font = UIFont.systemFont(ofSize: 12)
            textfield.frame = CGRect(x: textfield.frame.origin.x, y: SearchBarPaddingTop, width: textfield.frame.width, height: SearchBarHeight - SearchBarPaddingTop * 2)
            textfield.delegate = self
            textfield.layer.cornerRadius = textfield.frame.size.height / 2.0
            textfield.clipsToBounds = true
            return
        }
        
        
    }
    
    //
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("")
    }
}
