//
//  DepartmentCategoriesPopover.swift
//  
//
//  Created by Nhan Ba Doan on 11/15/17.
//  Copyright © 2017 OneTeam. All rights reserved.
//

import UIKit

class CommonCategoriesPopover: AbstractPopover {

    // singleton
    class var sharedInstance : CommonCategoriesPopover {
        struct Static {
            static let instance : CommonCategoriesPopover = CommonCategoriesPopover()
        }
        return Static.instance
    }
    
    // selected date
    var listCategory: [CommonPopoverModel]?
    
    /// Popover appears
    /// - parameter view: origin view of popover
    /// - parameter baseView: popoverPresentationController's sourceView
    /// - parameter baseViewController: viewController to become the base
    /// - parameter title: title for navigation bar
    /// - parameter dateMode: UIDatePickerMode
    /// - parameter initialDate: initial selected date
    /// - parameter doneAction: action in which user tappend done button
    /// - parameter cancelAction: action in which user tappend cancel button
    /// - parameter clearAction: action in which user tappend clear action. Omissible.
    public class func appearFrom(originView: UIView, baseView: UIView? = nil, baseViewController: UIViewController, title: String?, arrayData:[CommonPopoverModel]?, doneAction: (([CommonPopoverModel])->Void)?, cancelAction: (()->Void)?, clearAction: (()->Void)? = nil, modalStyle: UIModalPresentationStyle, arrowDirection: UIPopoverArrowDirection){
        
        // create navigationController
        guard let navigationController = sharedInstance.configureNavigationController(originView, baseView: baseView, baseViewController: baseViewController, title: title, modalStyle: modalStyle, arrowDirection: arrowDirection) else {
            return
        }
        
        // StringPickerPopoverViewController
        if let contentViewController = navigationController.topViewController as? CommonCategoriesPopoverViewController {
            
            contentViewController.listCategory = arrayData
            
            contentViewController.doneAction = doneAction
            contentViewController.cancleAction = cancelAction
            
            navigationController.popoverPresentationController?.delegate = contentViewController
        }
        
        // presnet popover
        baseViewController.present(navigationController, animated: true, completion: nil)
        
    }
    
    /// storyboardName
    override func storyboardName()->String{
        return "DepartmentCategoriesPopover"
    }

}
