//
//  CalendarPickerPopover.swift
//  
//
//  Created by Nhan Ba Doan on 11/27/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//


import Foundation
import UIKit

public class CalendarPickerPopover: AbstractPopover {
    // singleton
    class var sharedInstance : CalendarPickerPopover {
        struct Static {
            static let instance : CalendarPickerPopover = CalendarPickerPopover()
        }
        return Static.instance
    }
    
    // selected date
    var selectedDate: Date = Date()
    
    /// Popover appears
    /// - parameter view: origin view of popover
    /// - parameter baseView: popoverPresentationController's sourceView
    /// - parameter baseViewController: viewController to become the base
    /// - parameter title: title for navigation bar
    /// - parameter dateMode: UIDatePickerMode
    /// - parameter initialDate: initial selected date
    /// - parameter doneAction: action in which user tappend done button
    /// - parameter cancelAction: action in which user tappend cancel button
    /// - parameter clearAction: action in which user tappend clear action. Omissible.
    public class func appearFrom(originView: UIView, baseView: UIView? = nil, baseViewController: UIViewController, title: String?, dateMode:UIDatePicker.Mode, initialDate:Date, doneAction: ((Date)->Void)?, cancelAction: (()->Void)?, clearAction: (()->Void)? = nil, modalStyle: UIModalPresentationStyle, arrowDirection: UIPopoverArrowDirection){
        
        // create navigationController
        guard let navigationController = sharedInstance.configureNavigationController(originView, baseView: baseView, baseViewController: baseViewController, title: title, modalStyle: modalStyle, arrowDirection: arrowDirection) else {
            return
        }
        
        // StringPickerPopoverViewController
        if let contentViewController = navigationController.topViewController as? CalendarPickerPopoverViewController {
            
            // UIPickerView
            contentViewController.selectedDate = initialDate
            contentViewController.dateMode = dateMode
            
            contentViewController.doneAction = doneAction
            contentViewController.cancleAction = cancelAction

            navigationController.popoverPresentationController?.delegate = contentViewController
        }
        
        // presnet popover
        baseViewController.present(navigationController, animated: true, completion: nil)
        
    }
    
    /// storyboardName
    override func storyboardName()->String{
        return "CalendarPickerPopover"
    }
    
}
