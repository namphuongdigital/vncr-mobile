//
//  AbstractPopover.swift
//  SwiftyPickerPopover
//
//  Created by Y.T. Hoshino on 2016/09/14.
//  Copyright © 2016 Yuta Hoshino. All rights reserved.
//

import Foundation
import UIKit

public class AbstractPopover:NSObject {
    
    /// configure navigationController
    /// - parameter originView: origin view of Popover
    /// - parameter baseView: popoverPresentationController's sourceView
    /// - parameter baseViewController: viewController to become the base
    /// - parameter title: title of navigation bar
    func configureNavigationController(_ originView: UIView, baseView: UIView? = nil, baseViewController: UIViewController, title: String?, modalStyle: UIModalPresentationStyle, arrowDirection: UIPopoverArrowDirection)->UINavigationController?{
        // create ViewController for content
        let bundle = Bundle(for: AbstractPopover.self)
        let storyboard = UIStoryboard(name: self.storyboardName(), bundle: bundle)
        guard let navigationController = storyboard.instantiateInitialViewController() as? UINavigationController else {
            return nil
        }
        
        // define using popover
        navigationController.modalPresentationStyle = modalStyle//.overFullScreen
        
        // origin
        navigationController.popoverPresentationController?.sourceView = originView //?? baseViewController.view
        navigationController.popoverPresentationController?.sourceRect = originView.bounds
        
        // direction of arrow
        navigationController.popoverPresentationController?.permittedArrowDirections = arrowDirection

        // navigationItem's title
        navigationController.topViewController!.navigationItem.title = title
        
        return navigationController
    }
    
    func configureNavigationController(barButtonItem: UIBarButtonItem, baseView: UIView? = nil, baseViewController: UIViewController, title: String?, modalStyle: UIModalPresentationStyle, arrowDirection: UIPopoverArrowDirection)->UINavigationController?{
        // create ViewController for content
        let bundle = Bundle(for: AbstractPopover.self)
        let storyboard = UIStoryboard(name: self.storyboardName(), bundle: bundle)
        guard let navigationController = storyboard.instantiateInitialViewController() as? UINavigationController else {
            return nil
        }
        
        // define using popover
        navigationController.modalPresentationStyle = modalStyle//.overFullScreen
        navigationController.popoverPresentationController?.passthroughViews = nil
        
        navigationController.popoverPresentationController?.barButtonItem = barButtonItem
        // origin
        navigationController.popoverPresentationController?.sourceView = baseView ?? baseViewController.view
        
        // direction of arrow
        navigationController.popoverPresentationController?.permittedArrowDirections = arrowDirection
        
        // navigationItem's title
        navigationController.topViewController!.navigationItem.title = title
        
        return navigationController
    }
    
    /// storyboardName
    func storyboardName()->String{
        return "StringPickerPopover"
    }

}
