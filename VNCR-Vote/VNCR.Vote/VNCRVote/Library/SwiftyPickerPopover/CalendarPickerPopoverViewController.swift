//
//  CalendarPickerPopoverViewController.swift
//  
//
//  Created by Nhan Ba Doan on 11/27/16.
//  Copyright © 2016 OneTeam. All rights reserved.
//

import UIKit

class CalendarPickerPopoverViewController: UIViewController, UIPopoverPresentationControllerDelegate, FSCalendarDataSource, FSCalendarDelegate {
    
    @IBOutlet weak var calendar: FSCalendar!
    
    var doneAction: ((Date)->Void)?
    var cancleAction: (()->Void)?
    var selectedDate = Date()
    var dateMode: UIDatePicker.Mode = .date

    override func viewWillAppear(_ animated: Bool) {
     
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //loadCalendar()
        UINavigationBar.appearance().tintColor = Common.shared.APP_MAIN_TEXT_COLOR
        UINavigationBar.appearance().barTintColor = Common.shared.APP_SECOND_TEXT_COLOR
        UINavigationBar.appearance().titleTextAttributes = [NSAttributedString.Key.foregroundColor : Common.shared.APP_MAIN_TEXT_COLOR]
        UINavigationBar.appearance().isTranslucent = false
        calendar.select(selectedDate, scrollToDate: true)
    }
    
//    func loadCalendar() {
//        calendar = FSCalendar(frame: CGRect(x: 0, y: self.navigationController!.navigationBar.frame.maxY, width: self.view.bounds.width, height: 300))
//        calendar.dataSource = self
//        calendar.delegate = self
//        calendar.backgroundColor = UIColor.white
//        calendar.scopeGesture.isEnabled = true
//        calendar.select(selectedDate, scrollToDate: true)
//        self.view.addSubview(calendar)
//    }
    
    // Update your frame
    func calendar(_ calendar: FSCalendar, boundingRectWillChange bounds: CGRect, animated: Bool) {
        calendar.frame = CGRect(x: 0, y: self.navigationController!.navigationBar.frame.maxY, width: bounds.width, height: bounds.height)
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        doneAction?(date)
        dismiss(animated: true, completion: {})
    }
    
    @IBAction func tappedDone(_ sender: UIButton? = nil) {
        //doneAction?(picker.date)
        dismiss(animated: true, completion: {})
    }
    
    @IBAction func tappedCancel(_ sender: UIButton? = nil) {
        cancleAction?()
        dismiss(animated: true, completion: {})
    }
    
    /// popover dismissed
    func popoverPresentationControllerDidDismissPopover(_ popoverPresentationController: UIPopoverPresentationController) {
        tappedCancel()
    }
    
    /// Popover appears on iPhone
    func adaptivePresentationStyle(for controller: UIPresentationController, traitCollection: UITraitCollection) -> UIModalPresentationStyle {
        return .none
    }
}
