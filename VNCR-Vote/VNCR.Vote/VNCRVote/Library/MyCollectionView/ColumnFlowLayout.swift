//
//  ColumnFlowLayout.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 10/8/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//


class ColumnFlowLayout: UICollectionViewFlowLayout {
    
    let cellsPerRow: Int
    
    init(cellsPerRow: Int, minimumInteritemSpacing: CGFloat = 0, minimumLineSpacing: CGFloat = 0, sectionInset: UIEdgeInsets = .zero) {
        self.cellsPerRow = cellsPerRow
        super.init()
        
        self.minimumInteritemSpacing = minimumInteritemSpacing
        self.minimumLineSpacing = minimumLineSpacing
        self.sectionInset = sectionInset
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else { return }
        var collectionViewSafeAreaInsetsLeft: CGFloat = 0
        var collectionViewSafeAreaInsetsRight: CGFloat = 0
        if #available(iOS 11.0, *) {
            collectionViewSafeAreaInsetsLeft = collectionView.safeAreaInsets.left
            collectionViewSafeAreaInsetsRight = collectionView.safeAreaInsets.right
        }
        let marginsAndInsets = sectionInset.left + sectionInset.right + collectionViewSafeAreaInsetsLeft + collectionViewSafeAreaInsetsRight + minimumInteritemSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        itemSize = CGSize(width: itemWidth, height: itemWidth)
    }
    
    override func invalidationContext(forBoundsChange newBounds: CGRect) -> UICollectionViewLayoutInvalidationContext {
        let context = super.invalidationContext(forBoundsChange: newBounds) as! UICollectionViewFlowLayoutInvalidationContext
        context.invalidateFlowLayoutDelegateMetrics = newBounds.size != collectionView?.bounds.size
        return context
    }
}
