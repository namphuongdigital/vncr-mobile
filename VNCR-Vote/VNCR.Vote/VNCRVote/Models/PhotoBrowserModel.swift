//
//  PhotoBrowserModel.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 06/05/2021.
//  Copyright © 2021 Nhan Ba Doan. All rights reserved.
//

import UIKit

public class PhotoBrowserModel: NSObject {
    
    public var title: String = ""
    public var url: String = ""
    public var isSelected: Bool = false
    
    public override init() {
        super.init()
    }
    
}
