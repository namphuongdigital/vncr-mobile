//
//  NotificationModel.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 4/18/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation

class NotificationModel: NSObject {
    
    var Id: Int = 0
    var ReceiverId: String = ""
    var Title: String = ""
    var Content: String = ""
    var ImageURL: String = ""
    
    var IsHtml: Bool = false
    var ContentHTML: String = ""
    
    var ReadTime: Date? = nil
    var Status: String = ""
    var IsImportant: Bool = false
    
    var CreatedBy: String = ""
    var CreatedDate: Date? = nil
    
    var IsRead: Bool = false

    override init() {
        super.init()
    }
    
    init(json: [String: Any]) {
        
        self.Id = json["Id"] as? Int ?? 0
        self.ReceiverId = json["ReceiverId"] as? String ?? ""
        self.Title = json["Title"] as? String ?? ""
        self.Content = json["Content"] as? String ?? ""
        self.ImageURL = json["ImageURL"] as? String ?? ""

        self.IsHtml = json["IsHtml"] as? Bool ?? false
        self.ContentHTML = json["ContentHTML"] as? String ?? ""

        if let sTime = json["ReadTime"] as? Double? {
            self.ReadTime = (sTime ?? 0) == 0 ? nil : (sTime ?? 0).unixTimestampToDate()
        }
        self.Status = json["Status"] as? String ?? ""
        self.IsImportant = json["IsImportant"] as? Bool ?? false

        self.CreatedBy = json["CreatedBy"] as? String ?? ""
        if let sTime = json["CreatedDate"] as? Double? {
            self.CreatedDate = (sTime ?? 0) == 0 ? nil : (sTime ?? 0).unixTimestampToDate()
        }

        self.IsRead = self.Status != Common.NotificationStatus.New.description
//        if (self.Content.count > 200) {
//            self.Content = String(self.Content.replacingOccurrences(of: "\r\n", with: "").replacingOccurrences(of: "\n", with: "").suffix(200)) + "..." //(self.Content as NSString).substring(to: 200)
//        }
    }
    
}

