//
//  MobileVersionModel.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 5/8/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation

class MobileVersionModel: NSObject {
    
    var Id: Int64 = 0
    var Platform: String = ""
    var Version: String = ""
    var Build: Int = 0
    var IsRequireUpdate: Bool = false
    var UpdatedDate: Date = Date()
    var ReleaseNote: String = ""
    
    override init() {
        super.init()
    }
    
    public init(json: [String: Any]) {
        self.Id = json["Id"] as? Int64 ?? 0
        self.Platform = json["Platform"] as? String ?? ""
        self.Version = json["Version"] as? String ?? AppHelper.shared.APP_VERSION
        self.Build = json["Build"] as? Int ?? Int(AppHelper.shared.APP_BUILD) ?? 0
        self.IsRequireUpdate = json["IsRequireUpdate"] as? Bool ?? false
        if let sTime = json["UpdatedDate"] as? Double? {
            self.UpdatedDate = (sTime ?? 0).unixTimestampToDate()
        }
        self.ReleaseNote = json["ReleaseNote"] as? String ?? ""
    }
}
