//
//  UserDeviceModel.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 10/1/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import Foundation

struct UserDeviceModel {
    
    var EmployeeCode: String
    var DeviceUDID: String
    var DeviceName: String
    var DeviceModel: String
    var OSVersion: String
    var Platform: String
    var LastLogin: Int64
    var IsActivate: Bool?
    var IsMainDevice: Bool?
}

extension UserDeviceModel {
    
    init(json: [String: Any]) {
        
        self.EmployeeCode = json["EmployeeCode"] as? String ?? ""
        self.DeviceUDID = json["DeviceUDID"] as? String ?? ""
        self.DeviceName = json["DeviceName"] as? String ?? ""
        self.DeviceModel = json["DeviceModel"] as? String ?? ""
        self.OSVersion = json["OSVersion"] as? String ?? ""
        self.Platform = json["Platform"] as? String ?? ""
        self.LastLogin = json["LastLogin"] as? Int64 ?? 0
        self.IsActivate = json["IsActivate"] as? Bool? ?? false
        self.IsMainDevice = json["IsMainDevice"] as? Bool? ?? false

    }
    
}
