//
//  ResponseModel.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 10/1/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import Foundation

struct ResponseModel {
    var RetCode: Int = -1
    var RetMessage: String = ""
    var RetStatus: Bool = false
    var TotalRecord: Int = 0
    var RetData: Any? = nil
}
