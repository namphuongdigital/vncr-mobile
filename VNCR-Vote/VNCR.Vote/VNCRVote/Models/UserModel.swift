//
//  UserModel.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 10/1/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import Foundation

struct UserModel {
    
    var EmployeeId: String
    var EmployeeCode: String
    var EmployeeName: String
    var AvatarURL: String
    var Title: String
    var DepartmentName: String
    var DepartmentCode: String
    var Company: String
    var Address: String
    var WorkPhone: String
    var MobilePhone: String
    var Fax: String
    var Email: String
    var Website: String
    var TaxCode: String
    var SocialLinks: [String] = []
    //My Device Info
    var DeviceUDID: String
    var SessionToken: String
    var SessionExpiredDate: Int64
    var RefreshToken: String
    var RefreshExpiredDate: Int64
    var LastLogin: Int64
    var AvailableAnnualLeaveDay: Double
    var AllowAttendanceOnMobile: Bool
    var IsManager: Bool
    var MyDevices: [UserDeviceModel] = []
    var Permissions: [String] = []
    var IsSuperAdmin: Bool = false
    var QRCode: String = ""
    var Vehicles: [CustomDetailModel] = []
    
    init(json: [String: Any]) {
        
        self.EmployeeId = json["EmployeeId"] as? String ?? ""
        self.EmployeeCode = json["EmployeeCode"] as? String ?? ""
        self.EmployeeName = json["EmployeeName"] as? String ?? ""
        self.AvatarURL = json["AvatarURL"] as? String ?? ""
        self.Title = json["Title"] as? String ?? ""
        self.DepartmentCode = json["DepartmentCode"] as? String ?? ""
        self.DepartmentName = json["DepartmentName"] as? String ?? ""
        self.Company = json["Company"] as? String ?? ""
        self.Address = json["Address"] as? String ?? ""
        self.WorkPhone = json["WorkPhone"] as? String ?? ""
        self.MobilePhone = json["MobilePhone"] as? String ?? ""
        self.Fax = json["Fax"] as? String ?? ""
        self.Email = json["Email"] as? String ?? ""
        self.Website = json["Website"] as? String ?? ""
        self.TaxCode = json["TaxCode"] as? String ?? ""
        self.DeviceUDID = json["DeviceUDID"] as? String ?? ""
        self.SessionToken = json["SessionToken"] as? String ?? ""
        self.SessionExpiredDate = json["SessionExpiredDate"] as? Int64 ?? 0
        self.RefreshToken = json["RefreshToken"] as? String ?? ""
        self.RefreshExpiredDate = json["RefreshExpiredDate"] as? Int64 ?? 0
        self.LastLogin = json["LastLogin"] as? Int64 ?? 0
        self.AvailableAnnualLeaveDay = json["AvailableAnnualLeaveDay"] as? Double ?? 0
        self.AllowAttendanceOnMobile = json["AllowAttendanceOnMobile"] as? Bool ?? false
        self.IsManager = json["IsManager"] as? Bool ?? false
        self.IsSuperAdmin = json["IsSuperAdmin"] as? Bool ?? false
        
        if let lst = json["SocialLinks"] as? [String] {
            for case let result in lst {
                self.SocialLinks.append(result)
            }
        }
        
        if let lst = json["MyDevices"] as? [[String: Any]] {
            for case let result in lst {
                let model =  UserDeviceModel.init(json: result)
                self.MyDevices.append(model)
            }
        }
        
        if let lst = json["Permissions"] as? [String] {
            for case let result in lst {
                self.Permissions.append(result)
            }
        }
        
        self.QRCode = json["QRCode"] as? String ?? "\(self.EmployeeCode + "@" + "\(Date().toUnixTimestamp())")"
        
        if let lst = json["Vehicles"] as? [[String: Any]] {
            for case let result in lst {
                let model =  CustomDetailModel.init(json: result)
                self.Vehicles.append(model)
            }
        }
    }
}
