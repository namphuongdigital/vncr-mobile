//
//  FilterModel.swift
//  VNCRVote
//
//  Created by Nhan Ba Doan on 11/26/18.
//  Copyright © 2018 Nhan Ba Doan. All rights reserved.
//

import UIKit

public class FilterModel: NSObject {
    
    public var id: Int = 0
    public var filterName: String = "All"
    public var isSelected: Bool = false
    
    public override init() {
        super.init()
    }
    
}
