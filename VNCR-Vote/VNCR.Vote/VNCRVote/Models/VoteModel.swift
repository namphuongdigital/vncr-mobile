//
//  VoteModel.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 07/05/2021.
//  Copyright © 2021 Nhan Ba Doan. All rights reserved.
//

import UIKit

class VoteModel: Codable {
    
    var Id: Int = 0
    var Title: String = "" //PHIẾU BẦU ĐẠI BIỂU DỰ HỘI NGHỊ ĐẠI BIỂU NLĐ TỔNG CÔNG TY HKVN 2021",
    var TitleTextColor: String = "#000000"
    var Groups: [VoteGroupModel] = []
    var IsVoted: Bool = false
    
    init() { }
    
    init(json: [String: Any]) {
        self.Id = json["Id"] as? Int ?? 0
        self.Title = json["Title"] as? String ?? ""
        self.TitleTextColor = json["TitleTextColor"] as? String ?? ""
        self.IsVoted = json["IsVoted"] as? Bool ?? false
        if let lst = json["Groups"] as? [[String: Any]] {
            for case let result in lst {
                let data = VoteGroupModel.init(json: result)
                Groups.append(data)
            }
        }
    }
}

class VoteGroupModel: Codable {
    var Id: Int = 0 //": 1,
    var Title: String = "" //": "Đại biểu chính thức",
    var TitleTextColor: String = "#000000"
    var Description: String = "" //": "số lượng được phân bổ 15 người, đại biểu tín nhiệm ai thì lựa chọn để lại người đó, lựa chọn ít nhất 01 người, nhiều nhất 15 người.",
    var DescriptionTextColor: String = "#000000" //": "#A9A9A9",
    var Candidates: [VoteCandidateModel] = []
    
    init() { }
    
    init(json: [String: Any]) {
        self.Id = json["Id"] as? Int ?? 0
        self.Title = (json["Title"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        self.Description = (json["Description"] as? String ?? "").trimmingCharacters(in: .whitespacesAndNewlines)
        self.TitleTextColor = json["TitleTextColor"] as? String ?? ""
        self.DescriptionTextColor = json["DescriptionTextColor"] as? String ?? ""
        
        if let lst = json["Candidates"] as? [[String: Any]] {
            for case let result in lst {
                let data = VoteCandidateModel.init(json: result)
                Candidates.append(data)
            }
        }
    }
}

class VoteCandidateModel: Codable {
    var Id: Int = 0 //": 1,
    var Title: String = "" //": "Nguyễn Thị A",
    var Description: String = "" //": "Tiếp viên trưởng",
    var ImageUrl: String = ""
    var TitleTextColor: String = "#000000"
    var DescriptionTextColor: String = "#000000" //": "#A9A9A9",
    var IsSelected: Bool = false
    
    init() { }
    
    init(json: [String: Any]) {
        self.Id = json["Id"] as? Int ?? 0
        self.Title = json["Title"] as? String ?? ""
        self.Description = json["Description"] as? String ?? ""
        self.ImageUrl = json["ImageUrl"] as? String ?? ""
        self.TitleTextColor = json["TitleTextColor"] as? String ?? ""
        self.DescriptionTextColor = json["DescriptionTextColor"] as? String ?? ""
        self.IsSelected = json["IsSelected"] as? Bool ?? false
    }
}

class VoteDocModel: Codable {
    var ID: Int = 0 //": 1,
    var Order: Int = 0 //": 1,
    var FullName: String = ""//": "Mục lục",
    var Link: String = ""//": "https://api.crew.vn/hnnld2021/01.pdf",
    var Note: String = ""//": "",
    var TextColor: String = "#000000"//": "#000000"
    
    init() { }
    
    init(json: [String: Any]) {
        self.ID = json["ID"] as? Int ?? 0
        self.Order = json["Order"] as? Int ?? 0
        self.FullName = json["FullName"] as? String ?? ""
        self.Link = json["Link"] as? String ?? ""
        self.Note = json["Note"] as? String ?? ""
        self.TextColor = json["TextColor"] as? String ?? ""
    }
}
