//
//  CustomDetailModel.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 11/25/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import UIKit

class CustomDetailModel: NSObject {
    
    var Id: String = ""
    var Code: String = ""
    var Name: String = ""
    var ImageURL: String = "" //http://118.69.35.209:85/UploadFiles/API/StockFilter/Shape/Marquise.png"
    var DisplayOrder: Int = 0
    
    override init() {
        super.init()
    }
    
    init (_ code: String, _ name: String) {
        self.Code = code
        self.Name = name
    }
    
    init(json: [String: Any]) {
        
        self.Id = json["Id"] as? String ?? ""
        self.Code = json["Code"] as? String ?? ""
        self.Name = json["Name"] as? String ?? ""
        self.ImageURL = json["ImageURL"] as? String ?? ""
        self.DisplayOrder = json["DisplayOrder"] as? Int ?? 0
        
    }
}
