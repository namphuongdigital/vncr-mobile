//
//  MenuItemModel.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 9/16/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation

class MenuItemModel: NSObject {
    
    var id: Int = 0
    var title: String = ""
    var imageUrl: String = ""
    var color: UIColor = UIColor.white
    var badges: Int = 0
    var action: String = ""
    var keepOriginalColorIcon: Bool = true
    var backgroundColor: UIColor = Common.shared.APP_MAIN_COLOR

    public override init() {
        super.init()
    }
    
    public init(_ id: Int, _ title: String, _ imageUrl: String, _ color: UIColor = UIColor.white, _ keepOriginalColorIcon: Bool = true, _ action: String = "", _ backgroundColor: UIColor = Common.shared.APP_MAIN_COLOR, _ badges: Int = 0) {
        self.id = id
        self.title = title
        self.imageUrl = imageUrl
        self.color = color
        self.action = action
        self.keepOriginalColorIcon = keepOriginalColorIcon
        self.badges = badges
        self.backgroundColor = backgroundColor
    }
    
}
