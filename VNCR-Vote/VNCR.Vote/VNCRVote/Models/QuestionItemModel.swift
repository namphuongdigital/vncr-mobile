//
//  QuestionItemModel.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 12/20/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation

class QuestionItemModel: NSObject {
    
    var id: Int = 0
    var title: String = ""
    var answers: [AnswerItemModel] = []
    var isNoRandomAns: Bool = false

    public override init() {
        super.init()
    }
    
    public init(_ id: Int, _ title: String) {
        self.id = id
        self.title = title
    }
    
    public init(_ id: Int, _ title: String, _ answers: [AnswerItemModel]) {
        self.id = id
        self.title = title
        self.answers = answers
    }
    
    public init(_ title: String, _ answers: [AnswerItemModel], isNoRandomAns: Bool = false) {
        self.title = title
        self.answers = answers
        self.isNoRandomAns = isNoRandomAns
    }
    
    public init(_ title: String) {
        self.title = title
        self.isNoRandomAns = true
    }
    
}

class AnswerItemModel: NSObject {
    
    var id: Int = 0
    var title: String = ""
    var isCorrect: Bool = false
    var isSelected: Bool = false
    
    public override init() {
        super.init()
    }
    
    public init(_ id: Int, _ title: String, _ isCorrect: Bool = false) {
        self.id = id
        self.title = title
        self.isCorrect = isCorrect
    }
    
}
