//
//  CommonPopoverModel.swift
//  VNCRVote
//
//  Created by Nhan Bá Đoàn on 11/2/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation

class CommonPopoverModel: NSObject {
    
    var Id: Int = 0 //145
    var Code: String = "" //AD
    var Name: String = "" //Bộ phận kế toán
    var Description: String = "" //Accounting Department
    var IsSelected: Bool = false
    
    override init() {
        super.init()
    }
    
    public init(_ id: Int, _ code: String, _ name: String, _ isSelected: Bool) {
        self.Id = id
        self.Code = code
        self.Name = name
        self.IsSelected = isSelected
    }
    
    init(json: [String: Any]) {
        
        self.Id = json["Id"] as? Int ?? 0
        self.Code = json["Code"] as? String ?? ""
        self.Name = json["Name"] as? String ?? ""
        self.Description = json["Description"] as? String ?? ""
        
    }
}
