//
//  StoneProductionModel.swift
//  RydiamDemo
//
//  Created by Nhan Bá Đoàn on 5/18/19.
//  Copyright © 2019 Nhan Ba Doan. All rights reserved.
//

import Foundation

struct StoneProductionModel  {
    
    var Order: String = ""
    var StoneId: String = ""
    var StoneBarcode: String = ""
    var Date: String = ""
    var Detail: String = ""
    var ImageURL: String = ""
    var IsEnable: Bool = false
    var IsRequireConfirm: Bool = false
    var RequireConfirmMessage: String = ""

    var MoneyTitleBefore: String
    var MoneyValueBefore: String
    var MoneyColorBefore: String

    var MoneyTitleAfter: String
    var MoneyValueAfter: String
    var MoneyColorAfter: String

    var MoneyTitleFinal: String
    var MoneyValueFinal: String
    var MoneyColorFinal: String

    //Stone Shape Pictures
    var StoneShapePictures: [StonePictureModel]?
    //Stone Receipt Histories
    var StoneReceipts: [StoneReceiptModel]?
    //Stone Direction Pictures
    var StoneDirectionPictures: [StonePictureModel]?
}

extension StoneProductionModel {
    
    init(json: [String: Any]) {
        
        self.StoneId = json["StoneId"] as? String ?? ""
        self.Order = json["Order"] as? String ?? ""
        self.StoneBarcode = json["StoneBarcode"] as? String ?? ""
        self.Date = json["Date"] as? String ?? ""
        self.Detail = json["Detail"] as? String ?? ""
        self.ImageURL = json["ImageURL"] as? String ?? ""
        self.IsEnable = json["IsEnable"] as? Bool ?? false
        self.IsRequireConfirm = json["IsRequireConfirm"] as? Bool ?? false
        self.RequireConfirmMessage = json["RequireConfirmMessage"] as? String ?? ""
        
        self.MoneyTitleBefore = json["MoneyTitleBefore"] as? String ?? ""
        self.MoneyValueBefore = json["MoneyValueBefore"] as? String ?? ""
        self.MoneyColorBefore = json["MoneyColorBefore"] as? String ?? ""

        self.MoneyTitleAfter = json["MoneyTitleAfter"] as? String ?? ""
        self.MoneyValueAfter = json["MoneyValueAfter"] as? String ?? ""
        self.MoneyColorAfter = json["MoneyColorAfter"] as? String ?? ""

        self.MoneyTitleFinal = json["MoneyTitleFinal"] as? String ?? ""
        self.MoneyValueFinal = json["MoneyValueFinal"] as? String ?? ""
        self.MoneyColorFinal = json["MoneyColorFinal"] as? String ?? ""
        
        var stoneShapePictures: [StonePictureModel]? = []
        if let lst = json["StoneShapePictures"] as? [[String: Any]] {
            for case let result in lst {
                let data =  StonePictureModel.init(json: result)
                stoneShapePictures?.append(data)
            }
            self.StoneShapePictures = stoneShapePictures
        }
        
        var stoneDirectionPictures: [StonePictureModel]? = []
        if let lst = json["StoneDirectionPictures"] as? [[String: Any]] {
            for case let result in lst {
                let data =  StonePictureModel.init(json: result)
                stoneDirectionPictures?.append(data)
            }
            self.StoneDirectionPictures = stoneDirectionPictures
        }
        
        var stoneReceipts: [StoneReceiptModel]? = []
        if let lst = json["StoneReceipts"] as? [[String: Any]] {
            for case let result in lst {
                let data =  StoneReceiptModel.init(json: result)
                stoneReceipts?.append(data)
            }
            self.StoneReceipts = stoneReceipts
        }
    }
    
}

class StonePictureModel: NSObject {
    
    var Code: String = ""
    var Name: String = ""
    var ImageURL: String = ""
    
    override init() {
        super.init()
    }
    
    init(json: [String: Any]) {
        
        self.Code = json["Code"] as? String ?? ""
        self.Name = json["Name"] as? String ?? ""
        self.ImageURL = json["ImageURL"] as? String ?? ""
        
    }
    
}

class StoneReceiptModel: NSObject {
    
    var StoneReceiptId: String = "" //dbb29a8f-1167-473e-a694-c388709ec7d3",
    var CurrentStep: String = "" //CB",
    var NextStep: String = "" //HABB-C",
    var WeightIn: String = "" //0.6680",
    var WeightOut: String = "" //0.7670",
    var OutDate: String = "" //1557193775,
    var ReturnDate: String = "" //1557294719,
    var Employee: String = "" //SDD-011",
    var Remark: String = "" //",
    var Status: String = "" //Finished",
    var RemarkAtCenter: String = "" //",
    var Characteristics: String = "" //Black crystal (TA)\nBlack crystal (TA)\nBlack feather (TA)\n",
    var ReceiptDetail: String = "" //SDD-011\nStep: CB -> HABB-C\nWeight: 0.7670 -> 0.6680\nDate: 2019-05-07 08:49 -> 2019-05-08 12:51",
    var QueueDetail: String = "" //Queue\n0.7670 - 2019-05-08 10:32\n0.6870 - 2019-05-08 10:33\n0.6690 - 2019-05-08 11:28\n0.6660 - 2019-05-08 12:37"
    var StoneReceiptQueues: [StoneReceiptQueueModel]? = nil

    override init() {
        super.init()
    }
    
    init(json: [String: Any]) {
        
        self.StoneReceiptId = json["StoneReceiptId"] as? String ?? ""
        self.CurrentStep = json["CurrentStep"] as? String ?? ""
        self.NextStep = json["NextStep"] as? String ?? ""
        self.WeightIn = json["WeightIn"] as? String ?? ""
        self.WeightOut = json["WeightOut"] as? String ?? ""
        self.OutDate = json["OutDate"] as? String ?? ""
        self.ReturnDate = json["ReturnDate"] as? String ?? ""
        self.Employee = json["Employee"] as? String ?? ""
        self.Remark = json["Remark"] as? String ?? ""
        self.Status = json["Status"] as? String ?? ""
        self.RemarkAtCenter = json["RemarkAtCenter"] as? String ?? ""
        self.Characteristics = json["Characteristics"] as? String ?? ""
        self.ReceiptDetail = json["ReceiptDetail"] as? String ?? ""
        self.QueueDetail = json["QueueDetail"] as? String ?? ""
        
        var stoneReceiptQueues: [StoneReceiptQueueModel]? = []
        if let lst = json["StoneReceiptQueues"] as? [[String: Any]] {
            for case let result in lst {
                let data =  StoneReceiptQueueModel.init(json: result)
                stoneReceiptQueues?.append(data)
            }
            self.StoneReceiptQueues = stoneReceiptQueues
        }
        
    }
    
}

class StoneReceiptQueueModel: NSObject {
    
    var StoneReceiptId: String = "" //dbb29a8f-1167-473e-a694-c388709ec7d3",
    var Time: String = "" //1557293843,
    var Carat: String = "" //"0.6660"
    
    override init() {
        super.init()
    }
    
    init(json: [String: Any]) {
        
        self.StoneReceiptId = json["StoneReceiptId"] as? String ?? ""
        self.Time = json["Time"] as? String ?? ""
        self.Carat = json["Carat"] as? String ?? ""
        
    }
    
}
